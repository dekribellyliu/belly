package id.investree.customer;

import id.investree.core.base.BaseApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevCoreCustomerApplication extends BaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevCoreCustomerApplication.class, args);

		initSentry(DevCoreCustomerApplication.class, "customer");
	}
}
