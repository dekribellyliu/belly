package id.investree.customer.controller.external.anchor;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.DefaultData;
import id.investree.core.model.TokenPayload;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.controller.external.bankinformation.BankInformationResponseGenerator;
import id.investree.customer.entity.*;
import id.investree.customer.model.data.BankInformationData;
import id.investree.customer.model.data.PicInformationData;
import id.investree.customer.model.response.AnchorResponse;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.service.bankinformation.BankInformationService;
import id.investree.customer.service.businessprofile.BusinessProfileService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.service.picrole.PicRoleService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.LocalMessageUtils;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/anchor", produces = MediaType.APPLICATION_JSON_VALUE)
public class AnchorController extends BaseController {

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private LocalMessageUtils localMessageUtils;

	@Autowired
	private CustomerInformationService customerInformationService;

	@Autowired
	private CustomerRoleService customerRoleService;

	@Autowired
	private BankInformationService bankInformationService;

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private BusinessProfileService businessProfileService;

	@Autowired
	private LoginDataService loginDataService;

	@Autowired
	private PicRoleService picRoleService;

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	@Autowired
	private BankInformationResponseGenerator bankInformationResponse;


	@GetMapping("/{customerId}")
	public ResponseEntity<ResultResponse> getAnchorData(HttpServletRequest http, @PathVariable("customerId") Long customerId) {

		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
			.orElseThrow(() -> new AppException(localMessageUtils.emptyToken()));

		if (!GlobalConstants.USER_TYPE_BACKOFFICE_ID.equals(Math.toIntExact(tokenPayload.getUserType()))) {
			throw new DataNotFoundException();
		}

		CustomerRole checkCustomerRole = Optional.ofNullable(customerRoleService.findByCustomerIdAndType(customerId, GlobalConstants.USER_CATEGORY_ANCHOR_ID))
			.orElseThrow(() -> new DataNotFoundException("Anchor"));
		Boolean anchorStatus = checkCustomerRole.getStatus().equals(GlobalConstants.BORROWER_STATUS_ACTIVE);

		List<CustomerRole> customerRole = customerRoleService.findByCustomerIdAndType(customerId,
			GlobalConstants.USER_CATEGORY_BORROWER_ID, GlobalConstants.USER_CATEGORY_ANCHOR_ID);

		Boolean isAnchor = customerRole.size() == 1;
		List<Long> customerRoleids = customerRole.stream()
			.map(CustomerRole::getId).collect(Collectors.toList());

		String accessToken = http.getHeader("X-Investree-Token");
		CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.findById(customerId))
			.orElseThrow(() -> new DataNotFoundException("Customer Information"));

		List<UserData> userData = userDataService.findAllByCustomerId(customerId);
		List<BankInformation> bankInformation = bankInformationService.findByCustomerIdAndIsAnchorDisbursement(customerId);
		List<BankInformationData> bankInformationData = bankInformationResponse.generateBankInformations(bankInformation, Constants.BANK_TYPE_ACCOUNT, accessToken);
		List<PicInformationData> picInformationData = generatePicInformations(userData, customerRoleids, accessToken);

		completingDataGlobalParam.getGlobalParam(GlobalConstants.GLOBAL_PARAM_LEGAL_ENTITY);

		AnchorResponse anchorResponse = new AnchorResponse();
		if (GlobalConstants.CATEGORY_INSTITUSI_ID.toString().equals(customerInformation.getUserCategory())) {
			DefaultData legalEntity = null;
			if (!Utilities.isNull(customerInformation.getLegalEntity())) {
				legalEntity = completingDataGlobalParam.getLegalList().stream()
					.filter(v -> v.getId().equals(customerInformation.getLegalEntity()))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
			}

			anchorResponse.setLegalEntity(legalEntity);
		}

		anchorResponse.setAnchorStatus(anchorStatus);
		anchorResponse.setBankInformation(bankInformationData);
		anchorResponse.setCompanyName(customerInformation.getName());
		anchorResponse.setAnchor(isAnchor);
		anchorResponse.setPicInformation(picInformationData);

		return abstractResponseHandler(anchorResponse).getResult(localMessageUtils.dataFetched(), HttpStatus.OK);
	}

	@GetMapping("/pic/borrower-only/{customerId}")
	public ResponseEntity<ResultResponse> findPicBorrowerOnly(HttpServletRequest http, @PathVariable("customerId") Long customerId) {
		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
			.orElseThrow(() -> new AppException(localMessageUtils.emptyToken()));

		if (!GlobalConstants.USER_TYPE_BACKOFFICE_ID.equals(Math.toIntExact(tokenPayload.getUserType()))) {
			throw new DataNotFoundException();
		}

		List<CustomerRole> customerRoles = Optional.ofNullable(customerRoleService.findByCustomerIdAndType(customerId, GlobalConstants.USER_CATEGORY_BORROWER_ID, GlobalConstants.USER_CATEGORY_ANCHOR_ID))
			.orElseThrow(() -> new DataNotFoundException("Anchor"));

		List<Long> borrowerPICs = new ArrayList<>();
		List<Long> anchorPICs = new ArrayList<>();
		for (CustomerRole customerRole : customerRoles) {
			if (GlobalConstants.USER_CATEGORY_ANCHOR_ID.equals(customerRole.getType())) {
				anchorPICs = picRoleService.findByCustomerRoleId(customerRole.getId()).stream().map(PicRole::getLoginId).distinct().collect(Collectors.toList());
			} else if (GlobalConstants.USER_CATEGORY_BORROWER_ID.equals(customerRole.getType())) {
				borrowerPICs = picRoleService.findByCustomerRoleId(customerRole.getId()).stream().map(PicRole::getLoginId).distinct().collect(Collectors.toList());
			}
		}

		List<Long> pureBorrowerPics = new ArrayList<>(borrowerPICs);
		for (int position = 0; position < borrowerPICs.size(); position++) {
			if (anchorPICs.contains(borrowerPICs.get(position))) {
				pureBorrowerPics.remove(position);
			}
		}

		List<PicInformationData> picInformationData = new ArrayList<>();
		String accessToken = http.getHeader("X-Investree-Token");

		for (Long picLoginId : pureBorrowerPics) {
			UserData userData = Optional.ofNullable(userDataService.findByLoginId(picLoginId))
				.orElseThrow(() -> new DataNotFoundException("User Data"));

			LoginDataResponse loginData = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
				.orElseThrow(() -> new DataNotFoundException("Login Data"));
			Boolean emailStatus = GlobalConstants.STATUS_YES.equals(loginData.getEmailStatus());

			completingDataGlobalParam.getGlobalParam(
				GlobalConstants.GLOBAL_PARAM_POSITION,
				GlobalConstants.GLOBAL_PARAM_MOBILE_PREFIX
			);

			DefaultData position = null;
			if (!Utilities.isNull(userData.getCompanyPosition())) {
				position = completingDataGlobalParam.getPositionList().stream()
					.filter(v -> v.getId().equals(Math.toIntExact(userData.getCompanyPosition())))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
			}
			DefaultData mobilePrefix = null;
			if (!Utilities.isNull(loginData.getMobilePrefix())) {
				mobilePrefix = completingDataGlobalParam.getMobilePrefixList().stream()
					.filter(v -> v.getId().equals(Integer.valueOf(loginData.getMobilePrefix())))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
			}

			PicInformationData pic = new PicInformationData();
			pic.setLoginId(userData.getLoginDataId());
			pic.setSaluation(loginData.getSalutation());
			pic.setFullname(loginData.getFullname());
			pic.setUsername(loginData.getUsername());
			pic.setEmailAddress(loginData.getEmailAddress());
			pic.setEmailStatus(emailStatus);
			pic.setMobilePrefix(mobilePrefix);
			pic.setMobileNumber(loginData.getPhoneNumber());
			pic.setPositionWithInstitution(position);
			pic.setIdCardNumber(userData.getIdCardNumber());
			pic.setIdCardFile(userData.getIdCardFile());
			pic.setKtpAddress(userData.getKtpAddress());
			picInformationData.add(pic);
		}

		return abstractResponseHandler(picInformationData)
			.getResult(picInformationData.isEmpty() ? localMessageUtils.picBorrowerEmpty() : localMessageUtils.dataFetched());
	}

	private List<PicInformationData> generatePicInformations(List<UserData> userDatas, List<Long> customerRoleIds, String accessToken) {
		List<PicInformationData> picInformationData = new ArrayList<>();
		for (UserData userData : userDatas) {
			LoginDataResponse loginData = loginDataService.findByLoginId(accessToken, userData.getLoginDataId());
			if (Utilities.isNull(loginData)) {
				throw new DataNotFoundException("Login Data");
			}
			List<PicRole> picRoles = picRoleService.findByCustomerRoleIdAndLoginIdAndRoleIsNotNull(customerRoleIds, userData.getLoginDataId());
			if (Utilities.isNull(loginData.getDeletedAt()) && !picRoles.isEmpty()) {
				picInformationData.add(generatePicInformation(loginData, userData, customerRoleIds));
			}
		}
		return picInformationData;
	}

	private PicInformationData generatePicInformation(LoginDataResponse loginDataResponse, UserData userData,
	                                                  List<Long> customerRoleIds) {

		completingDataGlobalParam.getGlobalParam(
			GlobalConstants.GLOBAL_PARAM_POSITION,
			GlobalConstants.GLOBAL_PARAM_MOBILE_PREFIX,
			Constants.GLOBAL_PARAM_PIC_ROLE
		);

		DefaultData position = null;
		if (!Utilities.isNull(userData.getCompanyPosition())) {
			position = completingDataGlobalParam.getPositionList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(userData.getCompanyPosition())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData mobilePrefix = null;
		if (!Utilities.isNull(loginDataResponse.getMobilePrefix())) {
			mobilePrefix = completingDataGlobalParam.getMobilePrefixList().stream()
				.filter(v -> v.getId().equals(Integer.valueOf(loginDataResponse.getMobilePrefix())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}

		CustomerRole customerRole = customerRoleService.findByCustomerIdAndType(userData.getCustomerId(), GlobalConstants.USER_CATEGORY_ANCHOR_ID);
		Boolean isActive = GlobalConstants.BORROWER_STATUS_ACTIVE.equals(customerRole.getStatus());

		List<PicRole> picRoles = picRoleService.findByCustomerRoleIdAndLoginIdAndRoleIsNotNull(customerRoleIds, userData.getLoginDataId());
		List<DefaultData> roles;
		if (picRoles.size() > 0) {
			List<Integer> picRoleIds = picRoles.stream().map(PicRole::getRole).distinct().collect(Collectors.toList());
			roles = picRoleIds.stream().map(roleId -> completingDataGlobalParam.getPicRoleList().stream()
				.filter(v -> v.getId().equals(roleId))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null)).filter(role -> !Utilities.isNull(role)).collect(Collectors.toList());
		} else {
			roles = null;
		}

		List<PicRole> picRolesForms = picRoleService.findByCustomerRoleIdAndLoginIdAndRoleIsNull(customerRoleIds, userData.getLoginDataId());
		Long picForm = picRolesForms.isEmpty() ? 1L : 2L;

		PicInformationData picInformationData = new PicInformationData();
		picInformationData.setLoginId(userData.getLoginDataId());
		picInformationData.setSaluation(loginDataResponse.getSalutation());
		picInformationData.setFullname(loginDataResponse.getFullname());
		picInformationData.setUsername(loginDataResponse.getUsername());
		picInformationData.setEmailAddress(loginDataResponse.getEmailAddress());
		picInformationData.setActive(isActive);
		picInformationData.setMobilePrefix(mobilePrefix);
		picInformationData.setMobileNumber(loginDataResponse.getPhoneNumber());
		picInformationData.setPositionWithInstitution(position);
		picInformationData.setIdCardNumber(userData.getIdCardNumber());
		picInformationData.setIdCardFile(userData.getIdCardFile());
		picInformationData.setKtpAddress(userData.getKtpAddress());
		picInformationData.setPicRoles(roles);
		picInformationData.setPicFrom(picForm);

		return picInformationData;
	}
}
