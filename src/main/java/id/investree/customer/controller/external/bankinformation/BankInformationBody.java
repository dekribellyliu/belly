package id.investree.customer.controller.external.bankinformation;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.TokenPayload;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.BankInformation;
import id.investree.customer.model.request.BankInformationRequest;
import id.investree.customer.service.bankinformation.BankInformationService;
import id.investree.customer.service.legacy.SyncLegacyService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class BankInformationBody {

	@Autowired
	private BankInformationService bankInformationService;

	@Autowired
	private SyncLegacyService syncLegacyService;

	@Autowired
	private LocalMessageUtils localMessageUtils;

	@Autowired
	private TokenUtils tokenUtils;

	@Transactional(rollbackFor = Exception.class)
	public List<BankInformation> setBankInformation(List<BankInformationRequest> validRequests,Long customerId,
	                                                Integer userType, String accessToken) {
		TokenPayload tokenPayload = tokenUtils.parseToken(accessToken);

		List<BankInformation> responses = new ArrayList<>();
		validRequests.forEach(validRequest -> {

			BankInformation bankInformation;
			if (Utilities.isNull(validRequest.getBankInformationId())) {
				bankInformation = new BankInformation();

				bankInformation.setCreatedBy(tokenPayload.getUserId());
				bankInformation.setCreatedAt(new Date());
			} else {

				bankInformation = bankInformationService.getBankInformationById(validRequest.getBankInformationId(), customerId);

				if (Utilities.isNull(bankInformation)) {
					throw new DataNotFoundException("bank information id " + validRequest.getBankInformationId());
				}

				if (Utilities.isNull(validRequest.isDelete()) || !validRequest.isDelete()) {
					bankInformation.setUpdateBy(tokenPayload.getUserId());
				} else {
					bankInformation.setDeletedAt(new Date());
					bankInformation.setDeletedBy(tokenPayload.getUserId());
				}
			}

			bankInformation.setCustomerId(customerId);
			bankInformation.setBankType(Constants.BANK_TYPE_ACCOUNT);
			bankInformation.setBankAccountNumber(validRequest.getBankAccountNumber());
			bankInformation.setBankAccountCoverFile(validRequest.getBankAccountCoverFile());
			bankInformation.setMasterBankId(validRequest.getMasterBankId());
			bankInformation.setBankAccountHolderName(validRequest.getBankAccountHolderName());
			if (GlobalConstants.USER_CATEGORY_BORROWER_ID.equals(userType)) {
				bankInformation.setUseAsDisbursement(validRequest.getUseAsDisbursement() ? GlobalConstants.STATUS_YES : GlobalConstants.STATUS_NO);
			}
			if (GlobalConstants.USER_CATEGORY_LENDER_ID.equals(userType)) {
				bankInformation.setUseAsWithdrawal(validRequest.getUseAsWithdrawal() ? GlobalConstants.STATUS_YES : GlobalConstants.STATUS_NO);
			}

			BankInformation saveData = Optional.ofNullable(bankInformationService.saveOrUpdate(bankInformation))
				.orElseThrow(() -> new AppException(localMessageUtils.insertFailed("Bank Information")));

			if (Utilities.isNull(saveData.getDeletedBy())) {
				responses.add(saveData);
			}
		});

		return responses;
	}
}
