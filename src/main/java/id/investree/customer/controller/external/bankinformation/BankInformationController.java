package id.investree.customer.controller.external.bankinformation;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.TokenPayload;
import id.investree.core.model.legacy.LegacyModel;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.BankInformation;
import id.investree.customer.entity.CustomerInformation;
import id.investree.customer.entity.UserData;
import id.investree.customer.model.data.BankInformationData;
import id.investree.customer.model.legacy.LegacyModelBuilder;
import id.investree.customer.model.request.BankInformationRequest;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.service.bankinformation.BankInformationService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.legacy.SyncLegacyService;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.*;
import org.apache.logging.log4j.core.config.plugins.validation.constraints.Required;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/bank-information", produces = MediaType.APPLICATION_JSON_VALUE)
public class BankInformationController extends BaseController {

	@Autowired
	private BankInformationFlows flows;

	@Autowired
	private LocalMessageUtils localMessageUtils;

	@Autowired
	private BankInformationService bankInformationService;

	@Autowired
	private CustomerRoleService customerRoleService;

	@Autowired
	private SyncLegacyService syncLegacyService;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private CustomerInformationService customerInformationService;

	@Autowired
	private BankInformationBody bankInformationBody;

	@Autowired
	private BankInformationResponseGenerator bankInformationResponseGenerator;

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private StatusValidation statusValidation;

	@Autowired
	private LoginDataService loginDataService;

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private CustomerUtils customerUtils;

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	@PutMapping(value = "/save-all")
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> addBulkBankInformation(HttpServletRequest http,
	                                                             @RequestParam(value = "customerId", required = false) Long customerId,
	                                                             @RequestParam(value = "userRoleType", defaultValue = "1") Integer userType,
	                                                             @RequestBody List<BankInformationRequest> request) {

		List<BankInformationRequest> validRequests = flows.bankCreationBulk(request, userType);

		if (validRequests.isEmpty()) {
			throw new DataNotFoundException("bank information list");
		}

		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(localMessageUtils.emptyToken());
		}

		String accessToken = http.getHeader("X-Investree-Token");

		if (Utilities.isNull(customerId)) {
			customerId = Long.valueOf(tokenPayload.getCustomerId());
		}

		CustomerInformation customerInformation = customerInformationService.findById(customerId);
		if (Utilities.isNull(customerInformation)) {
			throw new DataNotFoundException();
		}

		statusValidation.statusRestrictionValidation(tokenPayload, userType);

		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(customerId))
			.orElseThrow(() -> new DataNotFoundException("User Data"));

		String emailAddress = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
			.map(LoginDataResponse::getEmailAddress)
			.orElseThrow(() -> new DataNotFoundException("Login Data"));

		List<BankInformation> responses = bankInformationBody.setBankInformation(validRequests, customerId, userType, accessToken);

		List<BankInformationData> bankInformationData = bankInformationResponseGenerator.generateBankInformations(responses, Constants.BANK_TYPE_ACCOUNT, accessToken);

		LegacyModel legacyModel = new LegacyModelBuilder()
			.setBankInformations(responses)
			.setGlobalParam(completingDataGlobalParam)
			.setEmail(emailAddress)
			.build(customerId);
		syncLegacyService.sync(accessToken, legacyModel);

		return abstractResponseHandler(bankInformationData).getResult(localMessageUtils.insertSuccess(), HttpStatus.OK);
	}

	@DeleteMapping(value = "{userRoleType}/{bankId}")
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> deleteById(HttpServletRequest http,
													 @PathVariable("userRoleType") String userRoleType,
													 @Required @PathVariable("bankId") Long id) {

		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(localMessageUtils.emptyToken());
		}

		Integer userTypeIdentification = customerUtils.getUserTypeIdentification(userRoleType);
		statusValidation.statusRestrictionValidation(tokenPayload, userTypeIdentification);

		Long customerId = Long.valueOf(tokenPayload.getCustomerId());
		BankInformation bankInformation = bankInformationService.getBankInformationById(id, customerId);
		if (Utilities.isNull(bankInformation)) {
			throw new DataNotFoundException();
		}

		if (GlobalConstants.STATUS_YES.equalsIgnoreCase(bankInformation.getUseAsDisbursement())
			|| GlobalConstants.STATUS_YES.equalsIgnoreCase(bankInformation.getUseAsWithdrawal())) {
			throw new AppException("can't delete this disbursement/withdrawal account");
		}

		bankInformation.setDeletedBy(tokenPayload.getUserId());
		bankInformation.setDeletedAt(new Date());

		BankInformation deleteData = bankInformationService.saveOrUpdate(bankInformation);

		if (Utilities.isNull(deleteData)) {
			throw new AppException(localMessageUtils.deleteFailed());
		}

		List<BankInformation> bankInformations = bankInformationService.findAllByCustomerId(customerId);
		LegacyModel legacyModel = new LegacyModelBuilder()
			.setBankInformations(bankInformations)
			.setGlobalParam(completingDataGlobalParam)
			.setEmail(tokenPayload.getUserEmail())
			.build(customerId);

		String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);
		syncLegacyService.sync(accessToken, legacyModel);

		return abstractResponseHandler(true).getResult(localMessageUtils.deleteSuccess(), HttpStatus.OK);
	}

	@GetMapping(value = "/{customer-id}")
	public ResponseEntity<ResultResponse> findByCustomerId(HttpServletRequest http, @PathVariable("customer-id") Long customerId) {

		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(localMessageUtils.emptyToken());
		}

		List<BankInformation> bankInformation = bankInformationService.findByCustomerId(customerId);
		if (Utilities.isNull(bankInformation)) {
			throw new DataNotFoundException();
		}

		String accessToken = http.getHeader("X-Investree-Token");
		List<BankInformationData> bankInformationData = bankInformationResponseGenerator.generateBankInformations(bankInformation, Constants.BANK_TYPE_ACCOUNT, accessToken);
		return abstractResponseHandler(bankInformationData).getResult(localMessageUtils.dataFetched(), HttpStatus.OK);
	}
}
