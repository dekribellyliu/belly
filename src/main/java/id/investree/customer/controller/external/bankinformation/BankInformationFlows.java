package id.investree.customer.controller.external.bankinformation;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.utils.Utilities;
import id.investree.customer.model.request.BankInformationRequest;
import id.investree.customer.service.bankinformation.BankInformationService;
import id.investree.customer.utils.BankUtils;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BankInformationFlows {

	@Autowired
	private BankInformationValidator bankInformationValidator;

	@Autowired
	private BankInformationService bankInformationService;

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private BankUtils bankUtils;

	public List<BankInformationRequest> bankCreationBulk(List<BankInformationRequest> requests, Integer userType) {

		if (GlobalConstants.USER_CATEGORY_BORROWER_ID.equals(userType)) {
			long usedAsDisbursementCount = requests.stream().filter(BankInformationRequest::getUseAsDisbursement).count();

			if (usedAsDisbursementCount != Constants.BANK_ACCOUNT_USED_DISBURSEMENT_COUNT) {
				throw new AppException(messageUtils.errorSelectedBankAccountForDisbursement());
			}
		}

		if (GlobalConstants.USER_CATEGORY_LENDER_ID.equals(userType)) {
			long usedAsWithdrawalCount = requests.stream().filter(BankInformationRequest::getUseAsWithdrawal).count();

			if (usedAsWithdrawalCount != Constants.BANK_ACCOUNT_USED_DISBURSEMENT_COUNT) {
				throw new AppException(messageUtils.errorSelectedBankAccountForWithdrawal());
			}
		}

		requests.forEach(this::bankCreation);

		return requests;
	}

	BankInformationRequest bankCreation(BankInformationRequest request) {

		if (Utilities.isNull(request.getCustomerId())) {
			throw new AppException(messageUtils.errorBlank("customer id"));
		}

		if (!Utilities.isEmptyOrBlank(request.getBankAccountCoverFile()) &&
				!Utilities.isValidExtensionFile(request.getBankAccountCoverFile(),
						GlobalConstants.FILE_EXT_PDF, GlobalConstants.FILE_EXT_PNG, GlobalConstants.FILE_EXT_JPEG, GlobalConstants.FILE_EXT_JPG)) {
			throw new AppException(messageUtils.dataNotValid("bank account cover file extension must be pdf, png, jpg or jpeg"));
		}

		if (Utilities.isEmptyOrBlank(request.getBankAccountHolderName())) {
			throw new AppException(messageUtils.errorBlank("back account holder"));
		}

		if (Utilities.isEmptyOrBlank(request.getBankAccountNumber())) {
			throw new AppException(messageUtils.errorBlank("bank number"));
		}

		boolean isBankAccountNumberValid = bankInformationValidator.isBankAccountNumberValid(request.getBankAccountNumber());
		if (!isBankAccountNumberValid) {
			throw new AppException(messageUtils.errorBankAccountNumberShouldContainsNumber());
		}

		if (Utilities.isEmptyOrBlank(request.getBankAccountCoverFile())) {
			throw new AppException(messageUtils.errorBlank("bank account cover file"));
		}

		if (Utilities.isNull(request.getMasterBankId())) {
			throw new AppException(messageUtils.errorBlank("master bank id"));
		}
		if (Constants.BANK_CENTRAL_ASIA_ID.equals(request.getMasterBankId()) && !bankUtils.isValidBcaAccount(request)) {
			throw new AppException(messageUtils.specificBankAcccountNotValid(
					request.getBankAccountHolderName(), request.getBankAccountNumber()));
		}

		return request;
	}
}
