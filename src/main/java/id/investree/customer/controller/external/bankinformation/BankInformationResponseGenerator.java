package id.investree.customer.controller.external.bankinformation;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.DefaultData;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.BankInformation;
import id.investree.customer.entity.CustomerRole;
import id.investree.customer.entity.MrInvarBank;
import id.investree.customer.model.data.BankInformationData;
import id.investree.customer.model.response.MasterBankResponse;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.masterbank.MasterBankService;
import id.investree.customer.service.mrinvarbank.MrInvarBankService;
import id.investree.customer.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class BankInformationResponseGenerator {

	@Autowired
	private MasterBankService masterBankService;

	@Autowired
	private MrInvarBankService mrInvarBankService;

	@Autowired
	private CustomerRoleService customerRoleService;

	public List<BankInformationData> generateBankInformations(List<BankInformation> bankInformations, Integer type, String accessToken) {
		return generateBankInformations(bankInformations, type, accessToken, 0L);
	}

	public List<BankInformationData> generateBankInformations(List<BankInformation> bankInformations, Integer type, String accessToken, Long userTypeIdentification) {
		List<BankInformationData> bankInformationData = new ArrayList<>();

		for (BankInformation bankInformation : bankInformations) {
			if (Constants.BANK_TYPE_VA.equals(type) && Constants.BANK_TYPE_VA.equals(bankInformation.getBankType())) {
				bankInformationData.add(generateBankInformation(bankInformation, accessToken, userTypeIdentification));
			} else if (Constants.BANK_TYPE_ACCOUNT.equals(type) && Constants.BANK_TYPE_ACCOUNT.equals(bankInformation.getBankType())) {
				bankInformationData.add(generateBankInformation(bankInformation, accessToken, userTypeIdentification));
			}
		}
		return bankInformationData;
	}

	public BankInformationData generateBankInformation(BankInformation bankInformation, String accessToken, Long userTypeIdentification) {
		boolean isDisbursement = GlobalConstants.STATUS_YES.equals(bankInformation.getUseAsDisbursement());
		boolean isAnchorDisbursement = GlobalConstants.STATUS_YES.equals(bankInformation.getIsAnchorDisbursement());
		boolean isWithdrawal = GlobalConstants.STATUS_YES.equals(bankInformation.getUseAsWithdrawal());
		String bankTypeValue = Constants.BANK_TYPE_VA.equals(bankInformation.getBankType()) ? "VA" : "Bank Account";
		DefaultData bankType = new DefaultData(bankInformation.getBankType(), bankTypeValue);

		String bin = "";
		if (Constants.BANK_TYPE_VA.equals(bankInformation.getBankType())) {
			bin = Optional.ofNullable(mrInvarBankService.findByBankIdAndType(bankInformation.getMasterBankId(), userTypeIdentification, 3L))
				.map(MrInvarBank::getBankBin)
				.orElseThrow(() -> new DataNotFoundException("Invar Bank"));
		}

		boolean isNewAnchorBank = false;
		if (!Utilities.isNull(bankInformation.getCustomerRoleId())) {
			CustomerRole customerRole = customerRoleService.findById(bankInformation.getCustomerRoleId());
			if (GlobalConstants.USER_CATEGORY_ANCHOR_ID.equals(customerRole.getType()) && bankInformation
				.getIsAnchorDisbursement().equals(GlobalConstants.STATUS_YES)) {
				isNewAnchorBank = true;
			}
		}

		MasterBankResponse masterBankResponse;
		DefaultData masterBank = null;
		if (!Utilities.isNull(bankInformation.getMasterBankId())) {
			masterBankResponse = masterBankService.findById(bankInformation.getMasterBankId(), accessToken);
			masterBank = new DefaultData(Math.toIntExact(masterBankResponse.getId()), masterBankResponse.getName());
		}

		BankInformationData data = new BankInformationData();
		data.setBankInformationId(bankInformation.getId());
		data.setBankType(bankType);
		data.setMasterBank(masterBank);
		data.setBankAccountCoverFile(bankInformation.getBankAccountCoverFile());
		data.setBankAccountNumber(bin + "" + bankInformation.getBankAccountNumber());
		data.setBankAccountHolderName(bankInformation.getBankAccountHolderName());
		data.setVerifiedStatus(bankInformation.getVerifiedStatus());
		data.setUseAsDisbursement(isDisbursement);
		data.setAnchorDisbursement(isAnchorDisbursement);
		data.setUseAsWithdrawal(isWithdrawal);
		data.setNewAnchorBank(isNewAnchorBank);
		data.setDelete(false);

		return data;
	}
}
