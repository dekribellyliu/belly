package id.investree.customer.controller.external.bankinformation;

import id.investree.core.regex.RegexValidator;
import id.investree.core.utils.Utilities;
import org.springframework.stereotype.Component;

@Component
class BankInformationValidator {

	Boolean isBankAccountNumberValid(String bankAccountNumber) {
		if (Utilities.isNull(bankAccountNumber)) {
			return false;
		}
		return RegexValidator.isNumeric(bankAccountNumber);
	}

}
