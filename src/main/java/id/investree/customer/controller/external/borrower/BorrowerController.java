package id.investree.customer.controller.external.borrower;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.TokenPayload;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.controller.external.completing.CompletingDataMapper;
import id.investree.customer.entity.CustomerRole;
import id.investree.customer.entity.UserData;
import id.investree.customer.model.request.UpdateRelationshipManager;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.model.response.UserDataResponse;
import id.investree.customer.service.borrower.BorrowerService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.integration.IntegrationService;
import id.investree.customer.service.loan.LoanService;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.service.userrole.UserRoleService;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.util.Optional;

@RestController
@RequestMapping(value = "/borrower", produces = MediaType.APPLICATION_JSON_VALUE)
public class BorrowerController extends BaseController {

	@Autowired
	private LoginDataService loginDataService;

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private CustomerInformationService customerInformationService;

	@Autowired
	private CustomerRoleService customerRoleService;

	@Autowired
	private BorrowerService borrowerService;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private CompletingDataMapper completingDataMapper;

	@Autowired
	private BorrowerValidator borrowerValidator;

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private IntegrationService integrationService;

	@Autowired
	private LoanService loanService;

	@GetMapping("{customerId}")
	public ResponseEntity<ResultResponse> findByUserId(HttpServletRequest http,
	                                                   @PathVariable("customerId") Long customerId) throws MalformedURLException {

		if (!Optional.ofNullable(tokenUtils.parseToken(http)).isPresent()) {
			throw new AppException(messageUtils.emptyToken());
		}

		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(customerId))
			.orElseThrow(() -> new DataNotFoundException("customer id: " + customerId));

		String accessToken = http.getHeader("X-Investree-Token");
		LoginDataResponse loginData = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
			.orElseThrow(() -> new DataNotFoundException("Login Data"));

		UserDataResponse userDataResponse = Optional.ofNullable(completingDataMapper.generateCompletingData(loginData, GlobalConstants.USER_CATEGORY_BORROWER_ID, userData.getLoginDataId(), accessToken))
			.orElseThrow(() -> new DataNotFoundException("Completing Data"));
		return abstractResponseHandler(userDataResponse).getResult(messageUtils.dataFetched());
	}

	@PutMapping("/update-rm/{borrowerCustomerId}")
	public ResponseEntity<ResultResponse> updateRelationshipManager(HttpServletRequest http,
	                                                                @PathVariable("borrowerCustomerId") Long borrowerCustomerId,
	                                                                @RequestBody UpdateRelationshipManager request) {

		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
			.orElseThrow(() -> new AppException(messageUtils.emptyToken()));

		if (!GlobalConstants.USER_TYPE_BACKOFFICE_ID.equals(tokenPayload.getUserType())) {
			throw new DataNotFoundException();
		}

		CustomerRole customerRole = Optional.ofNullable(
			customerRoleService.findByCustomerIdAndType(borrowerCustomerId, request.getUserRoleType()))
			.orElseThrow(DataNotFoundException::new);

		String accessToken = http.getHeader("X-Investree-Token");

		if (!Utilities.isNull(request.getRmLoginId())) {
			if (!Optional.ofNullable(loginDataService.findByLoginId(accessToken, request.getRmLoginId())).isPresent() ||
				!GlobalConstants.STATUS_YES.equals(loginDataService.findByLoginId(accessToken,
					request.getRmLoginId()).getActive()) || !Optional.ofNullable(userRoleService.
				findByLdIdAndMurId(request.getRmLoginId())).isPresent()) {
				throw new DataNotFoundException();
			}
		}

		customerRole.setRmBy(request.getRmLoginId());
		customerRoleService.saveOrUpdate(customerRole);

		// TODO: [RIO] remove anything connect to loan-app
//		if (GlobalConstants.USER_CATEGORY_BORROWER_ID.equals(request.getUserRoleType())) {
//			loanService.updateRmByBorrower(borrowerCustomerId, request.getRmLoginId());
//		}

		return abstractResponseHandler(true).getResult(messageUtils.updateSuccess());
	}
}
