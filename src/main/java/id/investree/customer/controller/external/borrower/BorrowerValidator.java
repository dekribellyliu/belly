package id.investree.customer.controller.external.borrower;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.customer.model.request.RegistrationStatusRequest;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class BorrowerValidator {

	@Autowired
	private LocalMessageUtils messageUtils;

	public void validateRegistrationStatus(RegistrationStatusRequest request) {
		if (!Optional.ofNullable(request.getStatus()).isPresent() ||
			(!GlobalConstants.USER_CATEGORY_BORROWER_NAME.equalsIgnoreCase(request.getStatus()) &&
				!GlobalConstants.USER_CATEGORY_LENDER_NAME.equalsIgnoreCase(request.getStatus()))) {
			throw new AppException(messageUtils.dataNotValid("status"));
		}

		if (!Optional.ofNullable(request.getUserType()).isPresent()) {
			throw new AppException(messageUtils.dataNotValid("status"));
		}
	}
}
