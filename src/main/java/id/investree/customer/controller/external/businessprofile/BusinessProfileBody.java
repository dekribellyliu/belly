package id.investree.customer.controller.external.businessprofile;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.model.TokenPayload;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.BusinessProfile;
import id.investree.customer.entity.CustomerInformation;
import id.investree.customer.model.request.BusinessProfileRequest;
import id.investree.customer.service.businessprofile.BusinessProfileService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class BusinessProfileBody {

	@Autowired
	private BusinessProfileService businessProfileService;

	@Autowired
	private CustomerInformationService customerInformationService;

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private BusinessProfileValidator validator;

	public BusinessProfile setBusinessBody (BusinessProfileRequest request, CustomerInformation customerInformation, TokenPayload tokenPayload, Integer userType) {

		BusinessProfile businessProfile = businessProfileService.findByCustomerId(customerInformation.getId());

		if (Utilities.isNull(businessProfile)) {
			businessProfile = new BusinessProfile();

			businessProfile.setCreatedBy(tokenPayload.getUserId());
			businessProfile.setCreatedAt(new Date());
		} else {
			businessProfile.setUpdatedBy(tokenPayload.getUserId());
		}

		businessProfile.setCustomerId(customerInformation.getId());
		businessProfile.setDateOfEstablishment(request.getDateOfEstablishment());
		businessProfile.setNumberOfEmployee(request.getNumberOfEmployee());
		businessProfile.setCompanyDescription(request.getCompanyDescription());
		businessProfile.setBusinessNarration(request.getCompanyNarration());
		businessProfile.setCompanyAddress(request.getCompanyAddress());
		businessProfile.setProvince(Utilities.getIdFromDefaultData(request.getProvince()));
		businessProfile.setCity(Utilities.getIdFromDefaultData(request.getCity()));
		businessProfile.setDistrict(Utilities.getIdFromDefaultData(request.getDistrict()));
		businessProfile.setVillage(Utilities.getIdFromDefaultData(request.getVillage()));
		businessProfile.setPostalCode(request.getPostalCode());
		businessProfile.setLandLineNumber(request.getLandLineNumber());
		businessProfile.setMailingAddressStatus(Utilities.isNull(request.getMailingAddressStatus()) ? businessProfile.getMailingAddressStatus() : request.getMailingAddressStatus());

		if (GlobalConstants.USER_TYPE_BACKOFFICE_ID.equals(tokenPayload.getUserType()) &&
				GlobalConstants.CATEGORY_INSTITUSI_ID.equals(Integer.valueOf(customerInformation.getUserCategory()))) {

			businessProfile.setGroupCompany(request.getGroupCompany());
			businessProfile.setGroupDescription(request.getGroupDescription());
			businessProfile.setListOfPayor(request.getListOfPayor());
			businessProfile.setRelationshipWithBank(request.isRelationshipWithBank() ? GlobalConstants.STATUS_YES : GlobalConstants.STATUS_NO);
		}

		return businessProfile;
	}

	public CustomerInformation setCustomerInformation (BusinessProfileRequest request, CustomerInformation customerInformation, TokenPayload tokenPayload) {

		customerInformation.setIndustry(request.getIndustry());
		if (!Utilities.isNull(request.getLegalEntity())) {
			customerInformation.setLegalEntity(request.getLegalEntity());
		}
		customerInformation.setName(request.getCompanyName());
		customerInformation.setUpdatedBy(tokenPayload.getUserId());
		customerInformation.setNationality(Utilities.getIdFromDefaultData(request.getNationality()));

		return customerInformation;
	}
}
