package id.investree.customer.controller.external.businessprofile;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.exception.UnauthorizeException;
import id.investree.core.model.TokenPayload;
import id.investree.core.model.legacy.LegacyModel;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.controller.external.bankinformation.BankInformationBody;
import id.investree.customer.controller.external.bankinformation.BankInformationFlows;
import id.investree.customer.controller.external.bankinformation.BankInformationResponseGenerator;
import id.investree.customer.controller.external.completing.CompletingDataCounterMapper;
import id.investree.customer.controller.external.legalinformation.LegalInformationBody;
import id.investree.customer.controller.external.legalinformation.LegalInformationResponseGenerator;
import id.investree.customer.controller.external.partnershipinformation.PartnershipInformationBody;
import id.investree.customer.controller.external.partnershipinformation.PartnershipInformationResponseGenerator;
import id.investree.customer.controller.external.shareholdersinformation.ShareholderInformationBody;
import id.investree.customer.controller.external.shareholdersinformation.ShareholderInformationResponseGenerator;
import id.investree.customer.controller.external.shareholdersinformation.ShareholdersInformationValidator;
import id.investree.customer.entity.*;
import id.investree.customer.model.data.*;
import id.investree.customer.model.legacy.LegacyModelBuilder;
import id.investree.customer.model.request.*;
import id.investree.customer.model.response.BusinessInfoResponse;
import id.investree.customer.model.response.BusinessInformationResponse;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.service.bankinformation.BankInformationService;
import id.investree.customer.service.businessprofile.BusinessProfileService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.legacy.SyncLegacyService;
import id.investree.customer.service.legalinformation.LegalInformationService;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.service.partnershipinformation.PartnershipInformationService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.LocalMessageUtils;
import id.investree.customer.utils.StatusValidation;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/business-profile", produces = MediaType.APPLICATION_JSON_VALUE)
public class BusinessProfileController extends BaseController {

	@Autowired
	private CustomerRoleService customerRoleService;

	@Autowired
	private BusinessProfileService businessProfileService;

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private BusinessProfileValidator validator;

	@Autowired
	private BankInformationFlows flows;

	@Autowired
	private CustomerInformationService customerInformationService;

	@Autowired
	private BankInformationService bankInformationService;

	@Autowired
	private LegalInformationService legalInformationService;

	@Autowired
	private PartnershipInformationService partnershipInformationService;

	@Autowired
	private SyncLegacyService syncLegacyService;

	@Autowired
	private ShareholderInformationBody shareholderInformationBody;

	@Autowired
	private BusinessProfileBody businessProfileBody;

	@Autowired
	private BankInformationBody bankInformationBody;

	@Autowired
	private LegalInformationBody legalInformationBody;

	@Autowired
	private PartnershipInformationBody partnershipInformationBody;

	@Autowired
	private ShareholdersInformationValidator shareholdersInformationValidator;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private BusinessProfileResponseGenerator businessProfileResponseGenerator;

	@Autowired
	private BankInformationResponseGenerator bankInformationResponseGenerator;

	@Autowired
	private LegalInformationResponseGenerator legalInformationResponseGenerator;

	@Autowired
	private ShareholderInformationResponseGenerator shareholderInformationResponseGenerator;

	@Autowired
	private PartnershipInformationResponseGenerator partnershipInformationResponseGenerator;

	@Autowired
	private CompletingDataCounterMapper counterMapper;

	@Autowired
	private StatusValidation statusValidation;

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private LoginDataService loginDataService;

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	@PostMapping
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> add(@RequestBody BusinessProfileRequest request,
											  @RequestParam(value = "userRoleType", defaultValue = "1") Integer userType,
											  HttpServletRequest http) {

		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(messageUtils.emptyToken());
		}

		statusValidation.statusRestrictionValidation(tokenPayload, userType);

		String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);
		validator.businessValidator(request, accessToken);

		Long customerId;
		if (GlobalConstants.USER_TYPE_BACKOFFICE_ID.equals(tokenPayload.getUserType())) {
			customerId = request.getCustomerId();
		} else {
			customerId = Long.valueOf(tokenPayload.getCustomerId());
		}

		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(customerId))
				.orElseThrow(() -> new DataNotFoundException("User Data"));

		String emailAddress = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
				.map(LoginDataResponse::getEmailAddress)
				.orElseThrow(() -> new DataNotFoundException("Login Data"));

		CustomerInformation customerInformation = customerInformationService.findById(customerId);

		BusinessProfile businessProfileData = businessProfileBody.setBusinessBody(request, customerInformation, tokenPayload, userType);

		BusinessProfile saveData = businessProfileService.saveOrUpdate(businessProfileData);
		if (Utilities.isNull(saveData)) {
			throw new AppException(messageUtils.insertFailed("Business Profile"));
		}

		CustomerInformation customerInformationData = businessProfileBody.setCustomerInformation(request, customerInformation, tokenPayload);

		CustomerInformation saveCustomer = customerInformationService.saveOrUpdate(customerInformationData);
		if (Utilities.isNull(saveCustomer)) {
			businessProfileService.deleteByCustomerId(request.getCustomerId());
			throw new AppException(messageUtils.insertFailed("Customer Information"));
		}

		BusinessInfoData businessInfoData = businessProfileResponseGenerator.generateBusinessInfoData(saveData, saveCustomer, accessToken);
		BusinessInfoResponse businessInfoResponse = new BusinessInfoResponse();
		businessInfoResponse.setBusinessInfo(businessInfoData);
		businessInfoResponse.setCounter(counterMapper.countBusinessProfile(businessInfoData, saveCustomer));

		LegacyModel legacyModel = new LegacyModelBuilder()
				.setCustomerInformation(saveCustomer)
				.setGlobalParam(completingDataGlobalParam)
				.setBusinessProfile(saveData)
				.setEmail(emailAddress)
				.build(customerId);

		syncLegacyService.sync(accessToken, legacyModel);

		return abstractResponseHandler(businessInfoResponse).getResult(messageUtils.insertSuccess());
	}

	@PutMapping("/save-all")
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> saveAll(@RequestBody BusinessInformationRequest requests,
												  @RequestParam(value = "userRoleType", defaultValue = "1") Integer userType,
												  HttpServletRequest http) {
		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
				.orElseThrow(() -> new AppException(messageUtils.emptyToken()));

		String accessToken = http.getHeader("X-Investree-Token");

		if (!GlobalConstants.USER_TYPE_BACKOFFICE_ID.equals(tokenPayload.getUserType())) {
			throw new UnauthorizeException();
		}

		Long customerId = requests.getCustomerId();

		BusinessProfileRequest businessProfileRequest = requests.getBusinessProfile();
		businessProfileRequest.setCustomerId(customerId);
		validator.businessValidator(businessProfileRequest, accessToken);

		CustomerInformation customerInformation = customerInformationService.findById(customerId);

		Long customerRoleId = Optional.ofNullable(customerRoleService.findByCustomerIdAndType(customerId, userType))
				.map(CustomerRole::getId)
				.orElseThrow(() -> new DataNotFoundException("customer role"));

		BusinessProfile businessProfile = businessProfileBody
				.setBusinessBody(businessProfileRequest, customerInformation, tokenPayload, userType);

		BusinessProfile businessProfileSave = Optional.ofNullable(businessProfileService.saveOrUpdate(businessProfile))
				.orElseThrow(() -> new AppException(messageUtils.insertFailed("Business Profile")));

		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(customerId))
				.orElseThrow(() -> new DataNotFoundException("User Data"));

		String emailAddress = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
				.map(LoginDataResponse::getEmailAddress)
				.orElseThrow(() -> new DataNotFoundException("Login Data"));

		CustomerInformation productPreferenceSave;
		List<ShareholdersInformation> shareholdersInformationList = new ArrayList<>();
		if (!GlobalConstants.USER_CATEGORY_LENDER_ID.equals(userType)) {
			UpdateProductPreferenceRequest productPreferenceRequest = requests.getProductPreference();
			customerInformation.setProductPreference(productPreferenceRequest.getProductPreference());
			productPreferenceSave = Optional.ofNullable(customerInformationService.saveOrUpdate(customerInformation))
					.orElseThrow(() -> new AppException(messageUtils.insertFailed("customer information")));

		} else {
			productPreferenceSave = new CustomerInformation();
			productPreferenceSave.setProductPreference(customerInformation.getProductPreference());

			shareholdersInformationValidator.backofficeBulkValidation(requests.getShareholdersInformation());

			shareholdersInformationList = shareholderInformationBody
					.setShareHolder(requests.getShareholdersInformation(), customerId, userType, tokenPayload);
		}

		customerInformation = businessProfileBody.setCustomerInformation(businessProfileRequest, customerInformation, tokenPayload);

		CustomerInformation saveCustomerInformation = customerInformationService.saveOrUpdate(customerInformation);
		if (Utilities.isNull(saveCustomerInformation)) {
			businessProfileService.deleteByCustomerId(customerId);
			throw new AppException(messageUtils.insertFailed("Customer Information"));
		}

		List<BankInformationRequest> bankInformationRequests = requests.getBankInformation();

		List<BankInformation> bankInformationResponseList = new ArrayList<>();
		if (!bankInformationRequests.isEmpty()) {
			bankInformationRequests.forEach(bankInformationRequest -> {
				bankInformationRequest.setCustomerId(customerId);
			});
			List<BankInformationRequest> validRequests = flows.bankCreationBulk(bankInformationRequests, userType);

			bankInformationResponseList = bankInformationBody
					.setBankInformation(validRequests, customerInformation.getId(), userType, accessToken);
		}

		List<LegalInformationDataRequest> legalInformationDataRequests = requests.getLegalInformation();
		List<LegalInformation> legalInformationList = new ArrayList<>();
		if (!legalInformationDataRequests.isEmpty()) {
			legalInformationList = legalInformationBody.setLegalEntity(legalInformationDataRequests, tokenPayload, customerId);
			if (!Optional.ofNullable(legalInformationService.saveAll(legalInformationList)).isPresent()) {
				throw new AppException(messageUtils.insertFailed("legal information"));
			}
		}

		List<ShareholdersInformationData> shareholdersInformationData = shareholderInformationResponseGenerator
				.generateShareHolderInformations(shareholdersInformationList, accessToken);

		BusinessInfoData businessInfoData = businessProfileResponseGenerator
				.generateBusinessInfoData(businessProfileSave, customerInformation, accessToken);

		BankInformationParent bankInformationParent = new BankInformationParent();
		bankInformationParent.setVa(
				bankInformationResponseGenerator.generateBankInformations(
						bankInformationResponseList,
						Constants.BANK_TYPE_VA,
						accessToken,
						Long.valueOf(userType)
				)
		);

		bankInformationParent.setBankAccount(bankInformationResponseGenerator
				.generateBankInformations(bankInformationResponseList, Constants.BANK_TYPE_ACCOUNT, accessToken));

		List<LegalInformationData> legalInformationDataList = legalInformationResponseGenerator
				.generateLegalInformation(customerInformation.getId());
		LegalInfoGroup legalInfoGroup = new LegalInfoGroup(legalInformationDataList);

		BusinessInformationResponse businessInformationResponse = new BusinessInformationResponse();
		businessInformationResponse.setCustomerId(customerId);
		businessInformationResponse.setBusinessInfoData(businessInfoData);
		businessInformationResponse.setProductPreferenceId(productPreferenceSave.getProductPreference());
		businessInformationResponse.setBankInformationData(bankInformationParent);
		businessInformationResponse.setLegalInfoGroup(legalInfoGroup);
		businessInformationResponse.setShareholdersInformation(shareholdersInformationData);

		List<PartnershipInformationRequest> partnershipInformationRequestList = requests.getPartnershipInformation();
		List<PartnershipInformation> partnershipInformation = new ArrayList<>();
		if (!partnershipInformationRequestList.isEmpty()) {
			 partnershipInformation = partnershipInformationBody
					 .setMultiplePartnershipInfo(partnershipInformationRequestList, tokenPayload, customerInformation, customerRoleId);

			List<PartnershipInformationData> partnershipInformationData = partnershipInformationResponseGenerator
					.generatePartnershipInformationList(partnershipInformation);

			businessInformationResponse.setPartnershipInformation(partnershipInformationData);
		}

		List<BankInformation> bankInformations = bankInformationService.findAllByCustomerId(customerId);
		LegacyModel legacyModel = new LegacyModelBuilder()
				.setCustomerInformation(saveCustomerInformation)
				.setGlobalParam(completingDataGlobalParam)
				.setBusinessProfile(businessProfileSave)
				.setBankInformations(bankInformations)
				.setLegalInformations(legalInformationList)
				.setPartnershipInformation(!partnershipInformation.isEmpty() ? partnershipInformation.get(0) : null)
				.setEmail(emailAddress)
				.build(customerId);

		syncLegacyService.sync(accessToken, legacyModel);

		if (!GlobalConstants.USER_CATEGORY_LENDER_ID.equals(userType)) {
			ApuPptCheckRequest apuPptCheckRequest = requests.getApuPptInformation();
			ApuPptCheckData apuPptCheckSave = Optional.ofNullable(businessProfileService.saveBackgroundCheck(apuPptCheckRequest, accessToken))
					.orElseThrow(() -> new AppException(messageUtils.insertFailed("Background check")));

			BackgroundCheckData backgroundCheckData = new BackgroundCheckData();
			backgroundCheckData.setApuPptCheckData(apuPptCheckSave);
			businessInformationResponse.setBackgroundCheckData(backgroundCheckData);
		}

		return abstractResponseHandler(businessInformationResponse).getResult(messageUtils.updateSuccess());
	}
}
