package id.investree.customer.controller.external.businessprofile;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.model.DefaultData;
import id.investree.core.model.TokenPayload;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.BusinessProfile;
import id.investree.customer.entity.CustomerInformation;
import id.investree.customer.model.data.BusinessInfoData;
import id.investree.customer.model.data.Industry;
import id.investree.customer.model.data.KbliData;
import id.investree.customer.model.response.BusinessProfileResponse;
import id.investree.customer.service.industry.IndustryService;
import id.investree.customer.service.postalarea.PostalAreaService;
import id.investree.customer.service.search.SearchService;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BusinessProfileResponseGenerator {

	@Autowired
	private PostalAreaService postalAreaService;

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	@Autowired
	private IndustryService industryService;

	@Autowired
	private SearchService searchService;

	@Autowired
	private TokenUtils tokenUtils;

	public BusinessInfoData generateBusinessInfoData(BusinessProfile businessProfile, CustomerInformation customerInformation,
	                                                 String accessToken) {
		return generateBusinessInfoData(businessProfile, customerInformation, accessToken, false);
	}

	public BusinessInfoData generateBusinessInfoData(BusinessProfile businessProfile, CustomerInformation customerInformation,
	                                                 String accessToken, boolean isRegisterFromApi) {

		TokenPayload tokenPayload = tokenUtils.parseToken(accessToken);


		DefaultData province = isRegisterFromApi ? new DefaultData(null, businessProfile.getProvinceName())
			: postalAreaService.findProvince(accessToken, businessProfile.getProvince());
		DefaultData city = isRegisterFromApi ? new DefaultData(null, businessProfile.getCityName())
			: postalAreaService.findCity(accessToken, businessProfile.getCity());
		DefaultData district = isRegisterFromApi ? new DefaultData(null, businessProfile.getDistrictName())
			: postalAreaService.findDistrict(accessToken, businessProfile.getDistrict());
		DefaultData subDistrict = isRegisterFromApi ? new DefaultData(null, businessProfile.getVillageName())
			: postalAreaService.findVillage(accessToken, businessProfile.getVillage());

		completingDataGlobalParam.getGlobalParam(
			GlobalConstants.GLOBAL_PARAM_NATIONALITY,
			GlobalConstants.GLOBAL_PARAM_LEGAL_ENTITY
		);

		DefaultData country = null;
		if (!Utilities.isNull(customerInformation.getNationality())) {
			country = completingDataGlobalParam.getNationalityList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(customerInformation.getNationality())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}

		BusinessInfoData businessInfoData = new BusinessInfoData();
		if (GlobalConstants.CATEGORY_INSTITUSI_ID.toString().equals(customerInformation.getUserCategory())) {
			DefaultData legalEntity = null;
			if (!Utilities.isNull(customerInformation.getLegalEntity())) {
				legalEntity = completingDataGlobalParam.getLegalList().stream()
					.filter(v -> v.getId().equals(Math.toIntExact(customerInformation.getLegalEntity())))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
			}

			businessInfoData.setLegalEntity(legalEntity);
		}

		Industry industry = null;
		if (!Utilities.isEmptyOrBlank(customerInformation.getIndustry())) {
			KbliData kbliData = searchService.findByCode(customerInformation.getIndustry(), accessToken);
			industry = new Industry();
			industry.setCode(kbliData.getId());
			industry.setName(kbliData.getName());
		}

		businessInfoData.setCompanyName(customerInformation.getName());
		businessInfoData.setCompanyAddress(businessProfile.getCompanyAddress());
		businessInfoData.setCompanyDescription(businessProfile.getCompanyDescription());
		businessInfoData.setCompanyNarration(businessProfile.getBusinessNarration());
		businessInfoData.setDateOfEstablishment(businessProfile.getDateOfEstablishment());
		businessInfoData.setNumberOfEmployee(businessProfile.getNumberOfEmployee());
		businessInfoData.setNationality(country);
		businessInfoData.setProvince(province);
		businessInfoData.setCity(city);
		businessInfoData.setIndustry(industry);
		businessInfoData.setDistrict(district);
		businessInfoData.setVillage(subDistrict);
		businessInfoData.setPostalCode(businessProfile.getPostalCode());
		businessInfoData.setLandLineNumber(businessProfile.getLandLineNumber());
		businessInfoData.setAverageMonthlySales(businessProfile.getAverageMonthlySales());
		businessInfoData.setProfitMargin(businessProfile.getProfitMargin());
		businessInfoData.setMailingAddressStatus(businessProfile.getMailingAddressStatus());

		if (GlobalConstants.USER_TYPE_BACKOFFICE_ID.equals(Math.toIntExact(tokenPayload.getUserType()))) {
			businessInfoData.setBorrowerInitial(customerInformation.getInitial());
			businessInfoData.setGroupCompany(businessProfile.getGroupCompany());
			businessInfoData.setGroupDescription(businessProfile.getGroupDescription());
			businessInfoData.setListOfPayor(businessProfile.getListOfPayor());
			businessInfoData.setRelationshipWithBank(GlobalConstants.STATUS_YES.equalsIgnoreCase(businessProfile.getRelationshipWithBank()));
		}

		return businessInfoData;
	}

	public BusinessProfileResponse generateBusinessProfile(BusinessProfile businessProfile) {
		BusinessProfileResponse businessProfileResponse = new BusinessProfileResponse();
		businessProfileResponse.setAverageMonthlySales(businessProfile.getAverageMonthlySales());
		businessProfileResponse.setProfitMargin(businessProfile.getProfitMargin());
		businessProfileResponse.setOtherIncome(businessProfile.getOtherIncome());
		businessProfileResponse.setGeneralProfitMargin(businessProfile.getGeneralProfitMargin());
		businessProfileResponse.setProfitMarginFromPartner(businessProfile.getProfitMarginFromPartner());
		businessProfileResponse.setTotalNettMargin(businessProfile.getTotalNettMargin());
		businessProfileResponse.setLivingCost(businessProfile.getLivingCost());
		return businessProfileResponse;
	}
}
