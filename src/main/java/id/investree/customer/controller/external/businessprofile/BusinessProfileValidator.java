package id.investree.customer.controller.external.businessprofile;

import id.investree.core.exception.AppException;
import id.investree.core.regex.RegexValidator;
import id.investree.core.utils.Utilities;
import id.investree.customer.model.request.BusinessProfileRequest;
import id.investree.customer.service.postalarea.PostalAreaService;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class BusinessProfileValidator {

	@Autowired
	private PostalAreaService postalAreaService;

	@Autowired
	private LocalMessageUtils messageUtils;

	public void businessValidator(BusinessProfileRequest request, String accessToken) {

		if (Utilities.isNull(request.getCustomerId())) {
			throw new AppException(messageUtils.errorBlank("customer id"));
		}

		if (!Utilities.isEmptyOrBlank(request.getPostalCode()) && !RegexValidator.isNumeric(request.getPostalCode())) {
			throw new AppException(messageUtils.errorBlank("postal code"));
		}

		if (!Utilities.isEmptyOrBlank(request.getLandLineNumber())) {
			if (!RegexValidator.isNumeric(request.getLandLineNumber())) {
				throw new AppException(messageUtils.errorBlank("land line number"));
			} else if (!RegexValidator.isOfficeNumberValid(request.getLandLineNumber())) {
				throw new AppException(messageUtils.lessLandLine());
			}
		}

		if (!Utilities.isNull(request.getDateOfEstablishment()) && request.getDateOfEstablishment().compareTo(new Date()) > 0) {
			throw new AppException(messageUtils.moreDate("Date of establishment"));
		}

		if (!Utilities.isNull(request.getNumberOfEmployee()) && request.getNumberOfEmployee() < 1) {
			throw new AppException(messageUtils.dataNotValid("number of employee"));
		}

		if (Utilities.isNull(request.getProvince()) && !Utilities.isNull(request.getCity())) {
			throw new AppException("Can't update city, please update province first!");
		} else if (!Utilities.isNull(request.getProvince()) && !Utilities.isNull(request.getCity())) {
			postalAreaService.checkCity(accessToken, Utilities.getIdFromDefaultData(request.getCity()), Utilities.getIdFromDefaultData(request.getProvince()));
		}

		if (Utilities.isNull(request.getCity()) && !Utilities.isNull(request.getDistrict())) {
			throw new AppException("Can't update district, please update city first!");
		} else if (!Utilities.isNull(request.getCity()) && !Utilities.isNull(request.getDistrict())) {
			postalAreaService.checkDistrict(accessToken, Utilities.getIdFromDefaultData(request.getDistrict()), Utilities.getIdFromDefaultData(request.getCity()));
		}

		if (Utilities.isNull(request.getDistrict()) && !Utilities.isNull(request.getVillage())) {
			throw new AppException("Can't update village, please update district first!");
		} else if (!Utilities.isNull(request.getDistrict()) && !Utilities.isNull(request.getVillage())) {
			postalAreaService.checkVillage(accessToken, Utilities.getIdFromDefaultData(request.getVillage()), Utilities.getIdFromDefaultData(request.getDistrict()));
		}

	}
}
