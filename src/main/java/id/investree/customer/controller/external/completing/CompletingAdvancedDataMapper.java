package id.investree.customer.controller.external.completing;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.model.TokenPayload;
import id.investree.core.utils.TokenUtils;
import id.investree.customer.controller.external.bankinformation.BankInformationResponseGenerator;
import id.investree.customer.controller.external.businessprofile.BusinessProfileResponseGenerator;
import id.investree.customer.controller.external.customernotes.CustomerNotesResponseGenerator;
import id.investree.customer.controller.external.emergencycontact.EmergencyContactResponseGenerator;
import id.investree.customer.controller.external.financialinformation.FinancialInformationResponseGenerator;
import id.investree.customer.controller.external.legalinformation.LegalInformationResponseGenerator;
import id.investree.customer.controller.external.partnershipinformation.PartnershipInformationResponseGenerator;
import id.investree.customer.controller.external.personalprofile.PersonalProfileResponseGenerator;
import id.investree.customer.controller.external.referraldata.ReferralDataResponseGenerator;
import id.investree.customer.controller.external.salestransaction.SalesTransactionResponseGenerator;
import id.investree.customer.controller.external.shareholdersinformation.ShareholderInformationResponseGenerator;
import id.investree.customer.controller.external.surveyinformation.SurveyInformationResponseGenerator;
import id.investree.customer.entity.*;
import id.investree.customer.model.data.*;
import id.investree.customer.model.response.BusinessProfileResponse;
import id.investree.customer.model.response.DigitalSignatureResponse;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.service.balancesheet.BalanceSheetService;
import id.investree.customer.service.bankinformation.BankInformationService;
import id.investree.customer.service.businessprofile.BusinessProfileService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customernotes.CustomerNotesService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.digitalsignature.DigitalSignatureService;
import id.investree.customer.service.emergencycontact.EmergencyContactService;
import id.investree.customer.service.financialinformation.FinancialInformationService;
import id.investree.customer.service.financialratio.FinancialRatioService;
import id.investree.customer.service.financialstatement.FinancialStatementService;
import id.investree.customer.service.financialtrend.FinancialTrendService;
import id.investree.customer.service.industry.IndustryService;
import id.investree.customer.service.legalinformation.LegalInformationService;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.service.masterbank.MasterBankService;
import id.investree.customer.service.partnershipinformation.PartnershipInformationService;
import id.investree.customer.service.postalarea.PostalAreaService;
import id.investree.customer.service.referraldata.ReferralDataService;
import id.investree.customer.service.salestransaction.SalesTransactionService;
import id.investree.customer.service.shareholdersinformation.ShareholdersInformationService;
import id.investree.customer.service.surveyinformation.SurveyInformationService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.CustomerUtils;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class CompletingAdvancedDataMapper {

	@Autowired
	private BusinessProfileService businessProfileService;

	@Autowired
	private CustomerInformationService customerInformationService;

	@Autowired
	private CustomerRoleService customerRoleService;

	@Autowired
	private BankInformationService bankInformationService;

	@Autowired
	private LegalInformationService legalInformationService;

	@Autowired
	private EmergencyContactService emergencyContactService;

	@Autowired
	private FinancialInformationService financialInformationService;

	@Autowired
	private ShareholdersInformationService shareholdersInformationService;

	@Autowired
	private PostalAreaService postalAreaService;

	@Autowired
	private IndustryService industryService;

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private MasterBankService masterBankService;

	@Autowired
	private SurveyInformationService surveyInformationService;

	@Autowired
	private ReferralDataService referralDataService;

	@Autowired
	private LoginDataService loginDataService;

	@Autowired
	private SalesTransactionService salesTransactionService;

	@Autowired
	private CustomerNotesService customerNotesService;

	@Autowired
	private PartnershipInformationService partnershipInformationService;

	@Autowired
	private FinancialTrendService financialTrendService;

	@Autowired
	private FinancialStatementService financialStatementService;

	@Autowired
	private FinancialRatioService financialRatioService;

	@Autowired
	private BalanceSheetService balanceSheetService;

	@Autowired
	private DigitalSignatureService digitalSignatureService;

	@Autowired
	private CompletingDataFileCounterMapper counterMapper;

	@Autowired
	private CustomerUtils customerUtils;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	@Autowired
	private PersonalProfileResponseGenerator personalProfileResponse;

	@Autowired
	private FinancialInformationResponseGenerator financialInformationResponse;

	@Autowired
	private SalesTransactionResponseGenerator salesTransactionResponse;

	@Autowired
	private EmergencyContactResponseGenerator emergencyContactResponse;

	@Autowired
	private BusinessProfileResponseGenerator businessProfileResponse;

	@Autowired
	private BankInformationResponseGenerator bankInformationResponse;

	@Autowired
	private LegalInformationResponseGenerator legalInformationResponse;

	@Autowired
	private ShareholderInformationResponseGenerator shareholderInformationResponse;

	@Autowired
	private CustomerNotesResponseGenerator customerNotesResponse;

	@Autowired
	private PartnershipInformationResponseGenerator partnershipInformationResponse;

	@Autowired
	private SurveyInformationResponseGenerator surveyInformationResponse;

	@Autowired
	private ReferralDataResponseGenerator referralDataResponse;

	public AdvancedInformationBorrower generateAdvanceBasicDataBorrower(Integer userTypeIdentification, UserData userData, String accessToken) throws MalformedURLException {
		TokenPayload tokenPayload = tokenUtils.parseToken(accessToken);

		AdvancedInformationBorrower advancedBasicData = new AdvancedInformationBorrower();

		CustomerInformation customerInformation = customerInformationService.findById(userData.getCustomerId());

		BusinessProfile businessProfile = businessProfileService.findByCustomerId(userData.getCustomerId());
		PersonalProfileData personalProfileData = personalProfileResponse.generatePersonalProfileData(userData, userTypeIdentification, accessToken);

		CompletingData completingDataPersonalProfile = new CompletingData();
		completingDataPersonalProfile.setField(personalProfileData);
		if (Constants.NATIONALITY_INDONESIA.equals(userData.getNationality())) {
			completingDataPersonalProfile.setCounter(counterMapper.counterPersonalProfile(personalProfileData, customerInformation.getUserCategory(), userTypeIdentification));
		} else {
			completingDataPersonalProfile.setCounter(counterMapper.counterPersonalProfileForeign(personalProfileData));
		}
		advancedBasicData.setPersonalProfile(completingDataPersonalProfile);

		List<FinancialInformation> financialInformationList = financialInformationService.findByCustomerId(userData.getCustomerId());
		FinancialData financialData = new FinancialData();
		financialData.setFinancialStatements(financialInformationResponse.generateFinancialData(financialInformationList, Constants.FINANCIAL_STATEMENT));
		financialData.setEStatements(financialInformationResponse.generateFinancialData(financialInformationList, Constants.FINANCIAL__E_STATEMENT));

		List<SalesTransaction> salesTransactions = salesTransactionService.findByCustomerId(userData.getCustomerId());
		SalesTransactionDataList salesTransactionDataList = new SalesTransactionDataList();
		salesTransactionDataList.setSalesTransactionData(salesTransactionResponse.generateSalesTransactionDataDetails(salesTransactions));

		ProductPreferenceData productPreferenceData = new ProductPreferenceData();
		productPreferenceData.setProductPreferenceId(customerInformation.getProductPreference());

		CompletingData completingDataProductPreference = new CompletingData();
		completingDataProductPreference.setField(productPreferenceData);
		completingDataProductPreference.setCounter(counterMapper.countProductParam(productPreferenceData));
		advancedBasicData.setProductPreference(completingDataProductPreference);

		List<EmergencyContact> emergencyContact = emergencyContactService.internalFindByCustomerId(customerInformation.getId(), accessToken);
		List<EmergencyContactData> emergencyContactDataList = emergencyContactResponse.generateEmergencyContacts(emergencyContact, accessToken);
		CompletingData completingDataEmergencyContact = new CompletingData();
		completingDataEmergencyContact.setField(emergencyContactDataList);
		completingDataEmergencyContact.setCounter(counterMapper.countEmergencyContact(emergencyContactDataList));
		advancedBasicData.setEmergencyContact(completingDataEmergencyContact);

		BusinessInfoData businessInfoData = businessProfileResponse.generateBusinessInfoData(businessProfile, customerInformation, accessToken);
		CompletingData completingDataBusinessProfile = new CompletingData();
		completingDataBusinessProfile.setField(businessInfoData);
		completingDataBusinessProfile.setCounter(counterMapper.countBusinessProfile(businessInfoData, customerInformation));
		advancedBasicData.setBusinessProfile(completingDataBusinessProfile);

		List<BankInformation> bankInformations = bankInformationService.findByCustomerId(customerInformation.getId());
		List<BankInformationData> vaInfo = bankInformationResponse.generateBankInformations(bankInformations, Constants.BANK_TYPE_VA, accessToken, Long.valueOf(userTypeIdentification));
		List<BankInformationData> bankInfo = bankInformationResponse.generateBankInformations(bankInformations, Constants.BANK_TYPE_ACCOUNT, accessToken);
		List<BankInformationData> allBankInfo = Stream.concat(vaInfo.stream(), bankInfo.stream())
			.collect(Collectors.toList());
		BankInformationParent bankInformationParent = new BankInformationParent();
		bankInformationParent.setVa(vaInfo);
		bankInformationParent.setBankAccount(bankInfo);

		CompletingData completingDataBankInfo = new CompletingData();
		completingDataBankInfo.setField(bankInformationParent);
		completingDataBankInfo.setCounter(counterMapper.countBankInformation(allBankInfo));
		advancedBasicData.setBankInformation(completingDataBankInfo);

		List<LegalInformationData> legalInformationDataList = legalInformationResponse.generateLegalInformation(customerInformation.getId());
		LegalInfoGroup legalInfoGroup = new LegalInfoGroup(legalInformationDataList);

		CompletingData completingDataLegalInformation = new CompletingData();
		completingDataLegalInformation.setField(legalInfoGroup);
		if (GlobalConstants.CATEGORY_INSTITUSI_ID.equals(Integer.valueOf(customerInformation.getUserCategory()))) {
			completingDataLegalInformation.setCounter(counterMapper.countLegalInformationInstitusional(legalInformationDataList));
		} else {
			completingDataLegalInformation.setCounter(counterMapper.countLegalInformationIndividual(legalInformationDataList));
		}
		advancedBasicData.setLegalInformation(completingDataLegalInformation);

		if (GlobalConstants.CATEGORY_INSTITUSI_ID.equals(Integer.valueOf(customerInformation.getUserCategory()))) {
			List<ShareholdersInformation> shareHolderInformations = shareholdersInformationService.findByCustomerIdAndNotDeleted(customerInformation.getId());
			List<ShareholdersInformationData> shareholdersInformationDataList = shareholderInformationResponse.generateShareHolderInformations(shareHolderInformations, accessToken);

			CompletingData completingDataShareHolderInformation = new CompletingData();
			completingDataShareHolderInformation.setField(shareholdersInformationDataList);
			completingDataShareHolderInformation.setCounter(counterMapper.countShareHolderInformation(shareHolderInformations));
			advancedBasicData.setShareHolderInformation(completingDataShareHolderInformation);
		} else {
			advancedBasicData.setShareHolderInformation(null);
		}

		if (GlobalConstants.USER_TYPE_BACKOFFICE_ID.equals(Math.toIntExact(tokenPayload.getUserType()))) {

			CompletingData completingDataFinancialData = new CompletingData();
			completingDataFinancialData.setField(financialData);
			completingDataFinancialData.setCounter(counterMapper
				.countFinancialInformation(financialInformationList, Integer.valueOf(customerInformation.getUserCategory())));

			BusinessProfileResponse financialInformation = businessProfileResponse.generateBusinessProfile(businessProfile);
			CompletingData completingDataFinancialInformation = new CompletingData();
			completingDataFinancialInformation.setField(financialInformation);
			completingDataFinancialInformation.setCounter(new CounterData(0, 0));

			if (GlobalConstants.CATEGORY_INDIVIDU_ID.equals(Integer.valueOf(customerInformation.getUserCategory()))) {
				advancedBasicData.setFinancialInformation(completingDataFinancialInformation);

				List<FinancialDataDetail> financialStatementDocuments = financialInformationResponse.generateFinancialData(financialInformationList, Constants.FINANCIAL_STATEMENT);
				List<FinancialDataDetail> eStatementDocuments = financialInformationResponse.generateFinancialData(financialInformationList, Constants.FINANCIAL__E_STATEMENT);

				CompletingData completingDataFinancialStatementDocuments = new CompletingData();
				completingDataFinancialStatementDocuments.setField(financialStatementDocuments);
				completingDataFinancialStatementDocuments.setCounter(counterMapper.countFinancialStatementDocuments(financialStatementDocuments, customerInformation.getUserCategory(), Constants.FINANCIAL_STATEMENT));
				advancedBasicData.setFinancialStatementDocuments(completingDataFinancialStatementDocuments);

				CompletingData completingDataEStatementDocuments = new CompletingData();
				completingDataEStatementDocuments.setField(eStatementDocuments);
				completingDataEStatementDocuments.setCounter(counterMapper.countFinancialStatementDocuments(eStatementDocuments, customerInformation.getUserCategory(), Constants.FINANCIAL__E_STATEMENT));
				advancedBasicData.setEStatementDocuments(completingDataEStatementDocuments);
			} else if (GlobalConstants.CATEGORY_INSTITUSI_ID.equals(Integer.valueOf(customerInformation.getUserCategory()))) {
				advancedBasicData.setFinancialInformation(completingDataFinancialData);
			}

			CompletingData completingDataSalesTransactionDataList = new CompletingData();
			completingDataSalesTransactionDataList.setField(salesTransactionDataList);
			completingDataSalesTransactionDataList.setCounter(counterMapper.countSalesTransaction(salesTransactions));
			advancedBasicData.setSalesTransaction(completingDataSalesTransactionDataList);

			List<SurveyInformation> surveyInformation = surveyInformationService.internalFindAllSurveyByCustomerId(userData.getCustomerId(), accessToken);
			List<SurveyInformationData> surveyInformationDataList = surveyInformationResponse.generateSurveyInformations(surveyInformation, accessToken);

			CompletingData completingDataSurveyInformation = new CompletingData();
			completingDataSurveyInformation.setField(surveyInformationDataList);
			completingDataSurveyInformation.setCounter(counterMapper.countSurveyInformation());
			advancedBasicData.setSurveyInformation(completingDataSurveyInformation);

			List<CustomerNotes> customerNotes = customerNotesService.findByCustomerId(userData.getCustomerId());
			CustomerNotesDataList customerNotesDataList = new CustomerNotesDataList();
			customerNotesDataList.setCustomerNotesData(customerNotesResponse.generateCustomerNotesDataDetails(customerNotes, accessToken));

			CompletingData completingDataCustomerNotesData = new CompletingData();
			completingDataCustomerNotesData.setField(customerNotesDataList);
			completingDataCustomerNotesData.setCounter(counterMapper.countCustomerNotes(customerNotes));
			advancedBasicData.setCustomerNotes(completingDataCustomerNotesData);

			ReferralData referralData = referralDataService.findByReferralUserId(userData.getLoginDataId());
			ReferralInformationData referralInformationData = referralDataResponse.generateReferralInformation(referralData, accessToken);

			CompletingData completingDataReferralData = new CompletingData();
			completingDataReferralData.setField(referralInformationData);
			advancedBasicData.setReferralInformation(completingDataReferralData);

			ApuPptCheckData apuPptCheckData = businessProfileService.checkByCustomerId(customerInformation.getId(), accessToken);
			BackgroundCheckData backgroundCheckData = new BackgroundCheckData();
			backgroundCheckData.setApuPptCheckData(apuPptCheckData);

			CompletingData completingDataBackgroundCheck = new CompletingData();
			completingDataBackgroundCheck.setField(backgroundCheckData);
			advancedBasicData.setBackgroundCheckInformation(completingDataBackgroundCheck);

			List<PartnershipInformation> partnershipInformations =
					partnershipInformationService.findAllByCustomerId(customerInformation.getId());

			CompletingData completingDataPartnershipInformation = new CompletingData();
			if (!partnershipInformations.isEmpty()) {
				List<PartnershipInformationData> partnershipInformationData = partnershipInformationResponse.generatePartnershipInformationList(partnershipInformations);
				completingDataPartnershipInformation.setField(partnershipInformationData);
			} else {
				completingDataPartnershipInformation.setField(partnershipInformations);
			}
			advancedBasicData.setPartnershipInformation(completingDataPartnershipInformation);

			List<FinancialStatement> financialStatement = financialStatementService.findByCustomerId(customerInformation.getId());
			List<FinancialStatementData> financialStatementData = financialInformationResponse.generateFinancialStatement(financialStatement);

			CompletingData completingDataFinancialStatement = new CompletingData();
			completingDataFinancialStatement.setField(financialStatementData);
			advancedBasicData.setFinancialStatement(completingDataFinancialStatement);

			List<FinancialTrend> financialTrend = financialTrendService.findByCustomerId(customerInformation.getId());
			List<FinancialTrendData> financialTrendData = financialInformationResponse.generateFinancialTrend(financialTrend);

			CompletingData completingDataFinancialTrend = new CompletingData();
			completingDataFinancialTrend.setField(financialTrendData);
			advancedBasicData.setFinancialTrend(completingDataFinancialTrend);

			List<BalanceSheet> balanceSheet = balanceSheetService.findByCustomerId(customerInformation.getId());
			List<BalanceSheetData> balanceSheetData = financialInformationResponse.generateBalanceSheet(balanceSheet);

			CompletingData completingDataBalanceSheet = new CompletingData();
			completingDataBalanceSheet.setField(balanceSheetData);
			advancedBasicData.setBalanceSheet(completingDataBalanceSheet);

			List<FinancialRatio> financialRatio = financialRatioService.findByCustomerId(customerInformation.getId());
			List<FinancialRatioData> financialRatioData = financialInformationResponse.generateFinancialRatio(financialRatio);

			CompletingData completingDataFinancialRatio = new CompletingData();
			completingDataFinancialRatio.setField(financialRatioData);
			advancedBasicData.setFinancialRatio(completingDataFinancialRatio);
		} else {
			advancedBasicData.setFinancialInformation(null);
			advancedBasicData.setSalesTransaction(null);
			advancedBasicData.setSurveyInformation(null);
			advancedBasicData.setCustomerNotes(null);
			advancedBasicData.setReferralInformation(null);
			advancedBasicData.setBackgroundCheckInformation(null);
			advancedBasicData.setPartnershipInformation(null);
			advancedBasicData.setFinancialStatement(null);
			advancedBasicData.setFinancialTrend(null);
			advancedBasicData.setBalanceSheet(null);
			advancedBasicData.setFinancialRatio(null);
		}

		return advancedBasicData;
	}

	public AdvancedInformationLender generateAdvanceBasicDataLender(LoginDataResponse loginData, Integer userTypeIdentification, UserData userData, String accessToken) throws MalformedURLException {
		TokenPayload tokenPayload = tokenUtils.parseToken(accessToken);

		AdvancedInformationLender advancedBasicData = new AdvancedInformationLender();

		CustomerInformation customerInformation = customerInformationService.findById(userData.getCustomerId());

		BusinessProfile businessProfile = businessProfileService.findByCustomerId(userData.getCustomerId());
		PersonalProfileData personalProfileData = personalProfileResponse.generatePersonalProfileData(userData, userTypeIdentification, accessToken);

		CompletingData completingDataPersonalProfile = new CompletingData();
		completingDataPersonalProfile.setField(personalProfileData);
		if (Constants.NATIONALITY_INDONESIA.equals(userData.getNationality())) {
			completingDataPersonalProfile.setCounter(counterMapper.counterPersonalProfile(personalProfileData, customerInformation.getUserCategory(), userTypeIdentification));
		} else {
			completingDataPersonalProfile.setCounter(counterMapper.counterPersonalProfileForeign(personalProfileData));
		}
		advancedBasicData.setPersonalProfile(completingDataPersonalProfile);

		List<FinancialInformation> financialInformationList = financialInformationService.findByCustomerId(userData.getCustomerId());
		FinancialData financialData = new FinancialData();
		financialData.setFinancialStatements(financialInformationResponse.generateFinancialData(financialInformationList, Constants.FINANCIAL_STATEMENT));
		financialData.setEStatements(financialInformationResponse.generateFinancialData(financialInformationList, Constants.FINANCIAL__E_STATEMENT));

		List<SalesTransaction> salesTransactions = salesTransactionService.findByCustomerId(userData.getCustomerId());
		SalesTransactionDataList salesTransactionDataList = new SalesTransactionDataList();
		salesTransactionDataList.setSalesTransactionData(salesTransactionResponse.generateSalesTransactionDataDetails(salesTransactions));

		ProductPreferenceData productPreferenceData = new ProductPreferenceData();
		productPreferenceData.setProductPreferenceId(customerInformation.getProductPreference());

		CompletingData completingDataProductPreference = new CompletingData();
		completingDataProductPreference.setField(productPreferenceData);
		completingDataProductPreference.setCounter(counterMapper.countProductParam(productPreferenceData));
		advancedBasicData.setProductPreference(completingDataProductPreference);

		List<EmergencyContact> emergencyContact = emergencyContactService.internalFindByCustomerId(customerInformation.getId(), accessToken);
		List<EmergencyContactData> emergencyContactDataList = emergencyContactResponse.generateEmergencyContacts(emergencyContact, accessToken);
		CompletingData completingDataEmergencyContact = new CompletingData();
		completingDataEmergencyContact.setField(emergencyContactDataList);
		completingDataEmergencyContact.setCounter(counterMapper.countEmergencyContact(emergencyContactDataList));
		advancedBasicData.setEmergencyContact(completingDataEmergencyContact);

		BusinessInfoData businessInfoData = businessProfileResponse.generateBusinessInfoData(businessProfile, customerInformation, accessToken);
		CompletingData completingDataBusinessProfile = new CompletingData();
		completingDataBusinessProfile.setField(businessInfoData);
		completingDataBusinessProfile.setCounter(counterMapper.countBusinessProfile(businessInfoData, customerInformation));
		advancedBasicData.setBusinessProfile(completingDataBusinessProfile);

		List<BankInformation> bankInformations = bankInformationService.findByCustomerId(customerInformation.getId());
		List<BankInformationData> vaInfo = bankInformationResponse.generateBankInformations(bankInformations, Constants.BANK_TYPE_VA, accessToken, Long.valueOf(userTypeIdentification));
		List<BankInformationData> bankInfo = bankInformationResponse.generateBankInformations(bankInformations, Constants.BANK_TYPE_ACCOUNT, accessToken);
		List<BankInformationData> allBankInfo = Stream.concat(vaInfo.stream(), bankInfo.stream())
			.collect(Collectors.toList());
		BankInformationParent bankInformationParent = new BankInformationParent();
		bankInformationParent.setVa(vaInfo);
		bankInformationParent.setBankAccount(bankInfo);

		CompletingData completingDataBankInfo = new CompletingData();
		completingDataBankInfo.setField(bankInformationParent);
		completingDataBankInfo.setCounter(counterMapper.countBankInformation(allBankInfo));
		advancedBasicData.setBankInformation(completingDataBankInfo);

		List<LegalInformationData> legalInformationDataList = legalInformationResponse.generateLegalInformation(customerInformation.getId());
		LegalInfoGroup legalInfoGroup = new LegalInfoGroup(legalInformationDataList);

		CompletingData completingDataLegalInformation = new CompletingData();
		completingDataLegalInformation.setField(legalInfoGroup);
		if (GlobalConstants.CATEGORY_INSTITUSI_ID.equals(Integer.valueOf(customerInformation.getUserCategory()))) {
			completingDataLegalInformation.setCounter(counterMapper.countLegalInformationInstitusional(legalInformationDataList));
		} else {
			completingDataLegalInformation.setCounter(counterMapper.countLegalInformationIndividual(legalInformationDataList));
		}
		advancedBasicData.setLegalInformation(completingDataLegalInformation);

		InvestmentProfileData investmentProfileData = new InvestmentProfileData();
		investmentProfileData.setInvestmentObjective(customerInformation.getInvestmentObjective());
		investmentProfileData.setRiskProfile(customerInformation.getRiskProfile());

		CompletingData completingDataInvetmentProfile = new CompletingData();
		completingDataInvetmentProfile.setField(investmentProfileData);
		completingDataInvetmentProfile.setCounter(counterMapper.countInvestProfile(investmentProfileData));
		advancedBasicData.setInvestmentProfile(completingDataInvetmentProfile);

		DigitalSignatureResponse digitalSignatureResponse = digitalSignatureService.findDigitalSignatureFile(accessToken);
		CompletingData completingDataActivationDocument = new CompletingData();
		completingDataActivationDocument.setField(digitalSignatureResponse);
		completingDataActivationDocument.setCounter(counterMapper.countActivationDocument(digitalSignatureResponse));
		advancedBasicData.setActivationDocument(completingDataActivationDocument);


		if (GlobalConstants.CATEGORY_INSTITUSI_ID.equals(Integer.valueOf(customerInformation.getUserCategory()))) {
			List<ShareholdersInformation> shareHolderInformations = shareholdersInformationService.internalFindByCustomerIdAndNotDeleted(customerInformation.getId(), accessToken);
			List<ShareholdersInformationData> shareholdersInformationDataList = shareholderInformationResponse.generateShareHolderInformations(shareHolderInformations, accessToken);

			CompletingData completingDataShareHolderInformation = new CompletingData();
			completingDataShareHolderInformation.setField(shareholdersInformationDataList);
			completingDataShareHolderInformation.setCounter(counterMapper.countShareHolderInformation(shareHolderInformations));
			advancedBasicData.setShareHolderInformation(completingDataShareHolderInformation);
		} else {
			advancedBasicData.setShareHolderInformation(null);
		}

		if (GlobalConstants.USER_TYPE_BACKOFFICE_ID.equals(Math.toIntExact(tokenPayload.getUserType()))) {

			List<CustomerNotes> customerNotes = customerNotesService.findByCustomerId(userData.getCustomerId());
			CustomerNotesDataList customerNotesDataList = new CustomerNotesDataList();
			customerNotesDataList.setCustomerNotesData(customerNotesResponse.generateCustomerNotesDataDetails(customerNotes, accessToken));

			CompletingData completingDataCustomerNotesData = new CompletingData();
			completingDataCustomerNotesData.setField(customerNotesDataList);
			completingDataCustomerNotesData.setCounter(counterMapper.countCustomerNotes(customerNotes));
			advancedBasicData.setCustomerNotes(completingDataCustomerNotesData);

			ReferralData referralData = referralDataService.findByReferralUserId(userData.getLoginDataId());
			ReferralInformationData referralInformationData = referralDataResponse.generateReferralInformation(referralData, accessToken);

			CompletingData completingDataReferralData = new CompletingData();
			completingDataReferralData.setField(referralInformationData);
			advancedBasicData.setReferralInformation(completingDataReferralData);

			ApuPptCheckData apuPptCheckData = businessProfileService.checkByCustomerId(customerInformation.getId(), accessToken);
			BackgroundCheckData backgroundCheckData = new BackgroundCheckData();
			backgroundCheckData.setApuPptCheckData(apuPptCheckData);

			CompletingData completingDataBackgroundCheck = new CompletingData();
			completingDataBackgroundCheck.setField(backgroundCheckData);
			advancedBasicData.setBackgroundCheckInformation(completingDataBackgroundCheck);

			PartnershipInformation partnershipInformation = partnershipInformationService.findByCustomerId(customerInformation.getId());
		} else {
			advancedBasicData.setCustomerNotes(null);
			advancedBasicData.setReferralInformation(null);
			advancedBasicData.setBackgroundCheckInformation(null);
		}

		return advancedBasicData;
	}

	public AdvancedInformation generateAdvanceBasicData(LoginDataResponse loginData, Integer userTypeIdentification, UserData userData, String accessToken) throws MalformedURLException {

		TokenPayload tokenPayload = tokenUtils.parseToken(accessToken);

		CustomerInformation customerInformation = customerInformationService.findById(userData.getCustomerId());

		BusinessProfile businessProfile = businessProfileService.findByCustomerId(userData.getCustomerId());
		PersonalProfileData personalProfileData = personalProfileResponse.generatePersonalProfileData(userData, userTypeIdentification, accessToken);

		List<FinancialInformation> financialInformationList = financialInformationService.findByCustomerId(userData.getCustomerId());
		FinancialData financialData = new FinancialData();
		financialData.setFinancialStatements(financialInformationResponse.generateFinancialData(financialInformationList, Constants.FINANCIAL_STATEMENT));
		financialData.setEStatements(financialInformationResponse.generateFinancialData(financialInformationList, Constants.FINANCIAL__E_STATEMENT));

		List<SalesTransaction> salesTransactions = salesTransactionService.findByCustomerId(userData.getCustomerId());
		SalesTransactionDataList salesTransactionDataList = new SalesTransactionDataList();
		salesTransactionDataList.setSalesTransactionData(salesTransactionResponse.generateSalesTransactionDataDetails(salesTransactions));

		ProductPreferenceData productPreferenceData = new ProductPreferenceData();
		productPreferenceData.setProductPreferenceId(customerInformation.getProductPreference());

		InvestmentProfileData investmentProfileData = new InvestmentProfileData();
		investmentProfileData.setInvestmentObjective(customerInformation.getInvestmentObjective());
		investmentProfileData.setRiskProfile(customerInformation.getRiskProfile());

		List<EmergencyContact> emergencyContact = emergencyContactService.internalFindByCustomerId(customerInformation.getId(), accessToken);
		List<EmergencyContactData> emergencyContactDataList = emergencyContactResponse.generateEmergencyContacts(emergencyContact, accessToken);
		BusinessInfoData businessInfoData = businessProfileResponse.generateBusinessInfoData(businessProfile, customerInformation, accessToken);

		List<BankInformation> bankInformations = bankInformationService.findByCustomerId(customerInformation.getId());
		List<BankInformationData> vaInfo = bankInformationResponse.generateBankInformations(bankInformations, Constants.BANK_TYPE_VA, accessToken, Long.valueOf(userTypeIdentification));
		List<BankInformationData> bankInfo = bankInformationResponse.generateBankInformations(bankInformations, Constants.BANK_TYPE_ACCOUNT, accessToken);
		List<BankInformationData> allBankInfo = Stream.concat(vaInfo.stream(), bankInfo.stream())
			.collect(Collectors.toList());
		BankInformationParent bankInformationParent = new BankInformationParent();
		bankInformationParent.setVa(vaInfo);
		bankInformationParent.setBankAccount(bankInfo);

		List<LegalInformationData> legalInformationDataList = legalInformationResponse.generateLegalInformation(customerInformation.getId());
		LegalInfoGroup legalInfoGroup = new LegalInfoGroup(legalInformationDataList);

		List<SurveyInformation> surveyInformation = surveyInformationService.internalFindAllSurveyByCustomerId(userData.getCustomerId(), accessToken);
		List<SurveyInformationData> surveyInformationDataList = surveyInformationResponse.generateSurveyInformations(surveyInformation, accessToken);

		List<ShareholdersInformation> shareHolderInformations = shareholdersInformationService.findByCustomerIdAndNotDeleted(customerInformation.getId());
		List<ShareholdersInformationData> shareholdersInformationDataList = shareholderInformationResponse.generateShareHolderInformations(shareHolderInformations, accessToken);

		ReferralData referralData = referralDataService.findByReferralUserId(userData.getLoginDataId());
		ReferralInformationData referralInformationData = referralDataResponse.generateReferralInformation(referralData, accessToken);

		ApuPptCheckData apuPptCheckData = businessProfileService.checkByCustomerId(customerInformation.getId(), accessToken);
		BackgroundCheckData backgroundCheckData = new BackgroundCheckData();
		backgroundCheckData.setApuPptCheckData(apuPptCheckData);

		List<PartnershipInformation> partnershipInformations = partnershipInformationService.findAllByCustomerId(customerInformation.getId());

		List<FinancialStatement> financialStatement = financialStatementService.findByCustomerId(customerInformation.getId());
		List<FinancialStatementData> financialStatementData = financialInformationResponse.generateFinancialStatement(financialStatement);

		List<FinancialTrend> financialTrend = financialTrendService.findByCustomerId(customerInformation.getId());
		List<FinancialTrendData> financialTrendData = financialInformationResponse.generateFinancialTrend(financialTrend);

		List<BalanceSheet> balanceSheet = balanceSheetService.findByCustomerId(customerInformation.getId());
		List<BalanceSheetData> balanceSheetData = financialInformationResponse.generateBalanceSheet(balanceSheet);

		List<FinancialRatio> financialRatio = financialRatioService.findByCustomerId(customerInformation.getId());
		List<FinancialRatioData> financialRatioData = financialInformationResponse.generateFinancialRatio(financialRatio);

		DigitalSignatureResponse digitalSignatureResponse = digitalSignatureService.findDigitalSignatureFile(accessToken);

		CompletingData completingDataPersonalProfile = new CompletingData();
		completingDataPersonalProfile.setField(personalProfileData);
		if (Constants.NATIONALITY_INDONESIA.equals(userData.getNationality())) {
			completingDataPersonalProfile.setCounter(counterMapper.counterPersonalProfile(personalProfileData, customerInformation.getUserCategory(), userTypeIdentification));
		} else {
			completingDataPersonalProfile.setCounter(counterMapper.counterPersonalProfileForeign(personalProfileData));
		}

		CompletingData completingDataBusinessProfile = new CompletingData();
		completingDataBusinessProfile.setField(businessInfoData);
		completingDataBusinessProfile.setCounter(counterMapper.countBusinessProfile(businessInfoData, customerInformation));

		CompletingData completingDataFinancialData = new CompletingData();
		completingDataFinancialData.setField(financialData);
		completingDataFinancialData.setCounter(counterMapper
			.countFinancialInformation(financialInformationList, Integer.valueOf(customerInformation.getUserCategory())));

		CompletingData completingDataLegalInformation = new CompletingData();
		completingDataLegalInformation.setField(legalInfoGroup);
		if (GlobalConstants.CATEGORY_INSTITUSI_ID.equals(Integer.valueOf(customerInformation.getUserCategory()))) {
			completingDataLegalInformation.setCounter(counterMapper.countLegalInformationInstitusional(legalInformationDataList));
		} else {
			completingDataLegalInformation.setCounter(counterMapper.countLegalInformationIndividual(legalInformationDataList));
		}

		CompletingData completingDataBankInfo = new CompletingData();
		completingDataBankInfo.setField(bankInformationParent);
		completingDataBankInfo.setCounter(counterMapper.countBankInformation(allBankInfo));

		CompletingData completingDataSalesTransactionDataList = new CompletingData();
		completingDataSalesTransactionDataList.setField(salesTransactionDataList);
		completingDataSalesTransactionDataList.setCounter(counterMapper.countSalesTransaction(salesTransactions));

		CompletingData completingDataEmergencyContact = new CompletingData();
		completingDataEmergencyContact.setField(emergencyContactDataList);
		completingDataEmergencyContact.setCounter(counterMapper.countEmergencyContact(emergencyContactDataList));

		CompletingData completingDataProductPreference = new CompletingData();
		completingDataProductPreference.setField(productPreferenceData);
		completingDataProductPreference.setCounter(counterMapper.countProductParam(productPreferenceData));

		CompletingData completingDataSurveyInformation = new CompletingData();
		completingDataSurveyInformation.setField(surveyInformationDataList);
		completingDataSurveyInformation.setCounter(counterMapper.countSurveyInformation());

		CompletingData completingDataShareHolderInformation = new CompletingData();
		completingDataShareHolderInformation.setField(shareholdersInformationDataList);
		completingDataShareHolderInformation.setCounter(counterMapper.countShareHolderInformation(shareHolderInformations));

		CompletingData completingDataInvetmentProfile = new CompletingData();
		completingDataInvetmentProfile.setField(investmentProfileData);
		completingDataInvetmentProfile.setCounter(counterMapper.countInvestProfile(investmentProfileData));

		CompletingData completingDataActivationDocument = new CompletingData();
		completingDataActivationDocument.setField(digitalSignatureResponse);
		completingDataActivationDocument.setCounter(counterMapper.countActivationDocument(digitalSignatureResponse));

		AdvancedInformation advancedBasicData = new AdvancedInformation();
		advancedBasicData.setPersonalProfile(completingDataPersonalProfile);
		advancedBasicData.setBusinessProfile(completingDataBusinessProfile);
		advancedBasicData.setLegalInformation(completingDataLegalInformation);
		advancedBasicData.setBankInformation(completingDataBankInfo);
		advancedBasicData.setFinancialInformation(completingDataFinancialData);
		advancedBasicData.setSalesTransaction(completingDataSalesTransactionDataList);
		advancedBasicData.setEmergencyContact(completingDataEmergencyContact);
		advancedBasicData.setProductPreference(completingDataProductPreference);
		advancedBasicData.setSurveyInformation(completingDataSurveyInformation);
		advancedBasicData.setShareHolderInformation(completingDataShareHolderInformation);
		advancedBasicData.setInvestmentProfile(completingDataInvetmentProfile);
		advancedBasicData.setActivationDocument(completingDataActivationDocument);

		if (GlobalConstants.USER_TYPE_BACKOFFICE_ID.equals(Math.toIntExact(tokenPayload.getUserType()))) {

			List<CustomerNotes> customerNotes = customerNotesService.findByCustomerId(userData.getCustomerId());
			CustomerNotesDataList customerNotesDataList = new CustomerNotesDataList();
			customerNotesDataList.setCustomerNotesData(customerNotesResponse.generateCustomerNotesDataDetails(customerNotes, accessToken));

			CompletingData completingDataCustomerNotesData = new CompletingData();
			completingDataCustomerNotesData.setField(customerNotesDataList);
			completingDataCustomerNotesData.setCounter(counterMapper.countCustomerNotes(customerNotes));

			advancedBasicData.setCustomerNotes(completingDataCustomerNotesData);

			CompletingData completingDataReferralData = new CompletingData();
			completingDataReferralData.setField(referralInformationData);

			advancedBasicData.setReferralInformation(completingDataReferralData);

			CompletingData completingDataBackgroundCheck = new CompletingData();
			completingDataBackgroundCheck.setField(backgroundCheckData);

			advancedBasicData.setBackgroundCheckInformation(completingDataBackgroundCheck);

			CompletingData completingDataPartnershipInformation = new CompletingData();
			if (!partnershipInformations.isEmpty()) {
				List<PartnershipInformationData> partnershipInformationData =
						partnershipInformationResponse.generatePartnershipInformationList(partnershipInformations);
				completingDataPartnershipInformation.setField(partnershipInformationData);
			} else {
				completingDataPartnershipInformation.setField(partnershipInformations);
			}

			advancedBasicData.setPartnershipInformation(completingDataPartnershipInformation);

			CompletingData completingDataFinancialStatement = new CompletingData();
			completingDataFinancialStatement.setField(financialStatementData);

			advancedBasicData.setFinancialStatement(completingDataFinancialStatement);

			CompletingData completingDataFinancialTrend = new CompletingData();
			completingDataFinancialTrend.setField(financialTrendData);

			advancedBasicData.setFinancialTrend(completingDataFinancialTrend);

			CompletingData completingDataBalanceSheet = new CompletingData();
			completingDataBalanceSheet.setField(balanceSheetData);

			advancedBasicData.setBalanceSheet(completingDataBalanceSheet);

			CompletingData completingDataFinancialRatio = new CompletingData();
			completingDataFinancialRatio.setField(financialRatioData);

			advancedBasicData.setFinancialRatio(completingDataFinancialRatio);
		}

		return advancedBasicData;
	}
}
