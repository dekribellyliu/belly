package id.investree.customer.controller.external.completing;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.exception.MissingTokenException;
import id.investree.core.model.DefaultData;
import id.investree.core.model.TokenPayload;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.controller.external.customerinfo.CustomerInformationBody;
import id.investree.customer.controller.external.personalprofile.PersonalProfileValidator;
import id.investree.customer.entity.CustomerRole;
import id.investree.customer.entity.UserData;
import id.investree.customer.model.request.RegistrationStatusRequest;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.model.response.UserDataFrontofficeResponse;
import id.investree.customer.model.response.UserDataResponse;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.CustomerUtils;
import id.investree.customer.utils.LocalMessageUtils;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.util.Optional;

@RestController
@RequestMapping(value = "/completing-data", produces = MediaType.APPLICATION_JSON_VALUE)
public class CompletingDataController extends BaseController {

	@Autowired
	private LoginDataService loginDataService;

	@Autowired
	private CustomerRoleService customerRoleService;

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private PersonalProfileValidator personalProfileValidator;

	@Autowired
	private CompletingDataMapper completingDataMapper;

	@Autowired
	private CustomerInformationBody customerInformationBody;

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	@Autowired
	private CustomerUtils customerUtils;

	@GetMapping("{userType}")
	public ResponseEntity<ResultResponse> getCompletingData(HttpServletRequest http, @PathVariable("userType") String userType) throws MalformedURLException {
		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(messageUtils.emptyToken());
		}

		String accessToken = http.getHeader("X-Investree-Token");
		Integer userTypeIdentification = customerUtils.getUserTypeIdentification(userType);

		LoginDataResponse loginData = loginDataService.findByLoginId(accessToken, tokenPayload.getUserId());
		if (Utilities.isNull(loginData)) {
			throw new DataNotFoundException("Login Data");
		}

		UserDataResponse userDataResponse = completingDataMapper.generateCompletingData(loginData, userTypeIdentification, tokenPayload.getUserId(), accessToken);
		if (Utilities.isNull(userDataResponse)) {
			throw new DataNotFoundException("Completing Data");
		}

		CustomerRole customerRole = Optional.ofNullable(customerRoleService.findByCustomerId(Long.valueOf(tokenPayload.getCustomerId())))
			.orElseThrow(DataNotFoundException::new);

		if (GlobalConstants.USER_TYPE_FRONTOFFICE_ID.equals(tokenPayload.getUserType())
			&& GlobalConstants.BORROWER_STATUS_REGISTERED.equals(customerRole.getStatus())) {
			customerRole.setStatus(GlobalConstants.BORROWER_STATUS_COMPLETING_DATA);

			CustomerRole customerRoleSave = customerRoleService.saveOrUpdate(customerRole);

			if (Utilities.isNull(customerRoleSave)) {
				throw new AppException(messageUtils.updateFailed());
			}
		}
		return abstractResponseHandler(userDataResponse).getResult(messageUtils.dataFetched());
	}

	@GetMapping("frontoffice/{userType}")
	public ResponseEntity<ResultResponse> getBasicInfo(HttpServletRequest http, @PathVariable("userType") String userType) throws MalformedURLException {
		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http)).orElseThrow(MissingTokenException::new);

		String accessToken = http.getHeader("X-Investree-Token");
		Integer userTypeIdentification = customerUtils.getUserTypeIdentification(userType);

		LoginDataResponse loginData = loginDataService.findByLoginId(accessToken, tokenPayload.getUserId());
		if (Utilities.isNull(loginData)) {
			throw new DataNotFoundException("Login Data");
		}

		UserDataFrontofficeResponse userDataResponse = completingDataMapper.generateCompletingDataFrontOffice(
			loginData, userTypeIdentification, tokenPayload.getUserId(), accessToken);
		if (Utilities.isNull(userDataResponse)) {
			throw new DataNotFoundException("Completing Data");
		}

		RegistrationStatusRequest registrationStatusRequest = new RegistrationStatusRequest();
		registrationStatusRequest.setUserType(userType);
		registrationStatusRequest.setStatus(GlobalConstants.BORROWER_STATUS_COMPLETING_DATA_NAME);

		customerInformationBody.changeUserStatus(Long.valueOf(tokenPayload.getCustomerId()), registrationStatusRequest,
			accessToken, true);
		return abstractResponseHandler(userDataResponse).getResult(messageUtils.dataFetched());
	}

	@GetMapping("backoffice/{userRoleType}/{customerId}")
	public ResponseEntity<ResultResponse> getCompletingDataBackoffice(HttpServletRequest http,
	                                                                  @PathVariable("userRoleType") String userType,
	                                                                  @PathVariable("customerId") Long customerId) throws MalformedURLException {

		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http)).orElseThrow(MissingTokenException::new);

		String accessToken = http.getHeader("X-Investree-Token");
		Integer userRoleType = customerUtils.getUserTypeIdentification(userType);

		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(customerId))
			.orElseThrow(() -> new DataNotFoundException("customer id: " + customerId));

		LoginDataResponse loginData = loginDataService.findByLoginId(accessToken, userData.getLoginDataId());
		if (Utilities.isNull(loginData)) {
			throw new DataNotFoundException("Login Data");
		}

		Object response = Optional.ofNullable(completingDataMapper.generateCompletingDataBackoffice(
			loginData, userRoleType, accessToken)).orElseThrow(() -> new DataNotFoundException("Completing Data"));
		return abstractResponseHandler(response).getResult(messageUtils.dataFetched());
	}

	@GetMapping("registration-status/{userRoleType}")
	public ResponseEntity<ResultResponse> checkRegistrationStatus(HttpServletRequest http, @PathVariable("userRoleType") String userType) {

		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
			.orElseThrow(MissingTokenException::new);

		Integer roleType = customerUtils.getUserTypeIdentification(userType);
		CustomerRole customerRole = Optional.ofNullable(customerRoleService.findByCustomerIdAndType(Long.valueOf(tokenPayload.getCustomerId()), roleType))
			.orElseThrow(DataNotFoundException::new);

		completingDataGlobalParam.getGlobalParam(GlobalConstants.GLOBAL_PARAM_REGISTRATION_STATUS);

		DefaultData registrationStatus = null;
		if (!Utilities.isNull(customerRole.getStatus())) {
			registrationStatus = completingDataGlobalParam.getRegistrationStatusList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(customerRole.getStatus())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}

		return abstractResponseHandler(registrationStatus).getResult(messageUtils.dataFetched());
	}
}
