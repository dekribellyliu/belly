package id.investree.customer.controller.external.completing;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.*;
import id.investree.customer.model.data.*;
import id.investree.customer.model.response.DigitalSignatureResponse;
import id.investree.customer.utils.Constants;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class CompletingDataFileCounterMapper {

	public CounterData counterPersonalProfile(PersonalProfileData data, String userCategory, Integer userRoleTypes) {
		int filled = 0;
		int total = 0;

		total++;
		if (!Utilities.isEmptyOrBlank(data.getSelfiePicture())) filled++;

		total++;
		if (!Utilities.isEmptyOrBlank(data.getIdCardPicture())) filled++;

		total++;
		if (!Utilities.isEmptyOrBlank(data.getIdCardNumber())) filled++;

		total++;
		if (!Utilities.isEmptyOrBlank(data.getIdCardExpiredDate()) || data.isIdCardLifetime()) {
			filled++;
		}

		total++;
		if (!Utilities.isEmptyOrBlank(data.getAddress())) filled++;

		total++;
		if (!Utilities.isNull(data.getProvince())) filled++;

		total++;
		if (!Utilities.isNull(data.getCity())) filled++;

		total++;
		if (!Utilities.isNull(data.getDistrict())) filled++;

		total++;
		if (!Utilities.isNull(data.getSubDistrict())) filled++;

		total++;
		if (!Utilities.isNull(data.getPostalCode())) filled++;

		total++;
		if (!Utilities.isNull(data.getPlaceOfBirth())) filled++;

		total++;
		if (!Utilities.isNull(data.getDateOfBirth())) filled++;

		total++;
		if (!Utilities.isNull(data.getReligion())) filled++;

		total++;
		if (!Utilities.isNull(data.getEducation())) filled++;

		total++;
		if (!Utilities.isNull(data.getOccupation())) filled++;

		total++;
		if (!Utilities.isNull(data.getMaritalStatus())) filled++;

		if (Optional.ofNullable(data.isSameAsDomicileAddress()).isPresent()) {
			total++;
			if (!Utilities.isNull(data.getDomicileAddress())) filled++;

			total++;
			if (!Utilities.isNull(data.getDomicileProvince())) filled++;

			total++;
			if (!Utilities.isNull(data.getDomicileCity())) filled++;

			total++;
			if (!Utilities.isNull(data.getDomicileDistrict())) filled++;

			total++;
			if (!Utilities.isNull(data.getDomicileSubDistrict())) filled++;

			total++;
			if (!Utilities.isNull(data.getDomicilePostalCode())) filled++;
		}

		if (GlobalConstants.USER_CATEGORY_LENDER_ID.equals(userRoleTypes)
			&& GlobalConstants.CATEGORY_INDIVIDU_ID.equals(Integer.valueOf(userCategory))) {

			total++;
			if (!Utilities.isNull(data.getAnnualIncome())) filled++;

			total++;
			if (!Utilities.isNull(data.getSourceOfFund())) filled++;

			total++;
			if (!Utilities.isNull(data.getMotherMaidenName())) filled++;

			total++;
			if (!Utilities.isNull(data.getFieldOfWork())) filled++;

			total++;
			if (!Utilities.isNull(data.getOccupationAddress())) filled++;

			total++;
			if (!Utilities.isNull(data.getOccupationCity())) filled++;

			total++;
			if (!Utilities.isNull(data.getOccupationPostalCode())) filled++;

			total++;
			if (!Utilities.isNull(data.getOccupationPhoneNumber())) filled++;
		}

		return new CounterData(filled, total);
	}

	public CounterData counterPersonalProfileForeign(PersonalProfileData data) {
		int filled = 0;
		int total = 0;

		total++;
		if (!Utilities.isEmptyOrBlank(data.getSelfiePicture())) filled++;

		total++;
		if (!Utilities.isEmptyOrBlank(data.getIdCardPicture())) filled++;

		total++;
		if (!Utilities.isEmptyOrBlank(data.getIdCardNumber())) filled++;

		total++;
		if (!Utilities.isEmptyOrBlank(data.getIdCardExpiredDate()) || data.isIdCardLifetime()) {
			filled++;
		}

		total++;
		if (!Utilities.isEmptyOrBlank(data.getAddress())) filled++;

		total++;
		if (!Utilities.isNull(data.getPostalCode())) filled++;

		total++;
		if (!Utilities.isNull(data.getPlaceOfBirthExternal())) filled++;

		total++;
		if (!Utilities.isNull(data.getDateOfBirth())) filled++;

		total++;
		if (!Utilities.isNull(data.getReligion())) filled++;

		total++;
		if (!Utilities.isNull(data.getEducation())) filled++;

		total++;
		if (!Utilities.isNull(data.getOccupation())) filled++;

		total++;
		if (!Utilities.isNull(data.getMaritalStatus())) filled++;

		total++;
		if (data.isSameAsDomicileAddress() || !Utilities.isNull(data.getDomicileAddress())) filled++;

		total++;
		if (data.isSameAsDomicileAddress() || !Utilities.isNull(data.getDomicilePostalCode())) filled++;

		return new CounterData(filled, total);
	}

	public CounterData countBusinessProfile(BusinessInfoData data, CustomerInformation customerInformation) {
		int filled = 0;
		int total = 0;

		total++;
		if (!Utilities.isEmptyOrBlank(data.getCompanyName())) filled++;

		total++;
		if (!Utilities.isNull(data.getDateOfEstablishment())) filled++;

		total++;
		if (!Utilities.isNull(data.getNumberOfEmployee())) filled++;

		total++;
		if (!Utilities.isEmptyOrBlank(data.getCompanyDescription())) filled++;

		total++;
		if (!Utilities.isEmptyOrBlank(data.getCompanyAddress())) filled++;

		total++;
		if (!Utilities.isNull(data.getProvince())) filled++;

		total++;
		if (!Utilities.isNull(data.getCity())) filled++;

		total++;
		if (!Utilities.isNull(data.getDistrict())) filled++;

		total++;
		if (!Utilities.isNull(data.getVillage())) filled++;

		total++;
		if (!Utilities.isNull(data.getPostalCode())) filled++;

		total++;
		if (!Utilities.isNull(data.getIndustry())) filled++;

		total++;
		if (!Utilities.isEmptyOrBlank(data.getLandLineNumber())) filled++;

		return new CounterData(filled, total);
	}

	public CounterData countFinancialStatementDocuments(List<FinancialDataDetail> data, String userCategory, Integer documentType) {
		int filled = data.size();
		int total = 0;

		if (GlobalConstants.CATEGORY_INSTITUSI_ID.equals(Integer.valueOf(userCategory))) {
			if (Constants.FINANCIAL_STATEMENT.equals(documentType)) {
				total = 2;
			} else if (Constants.FINANCIAL__E_STATEMENT.equals(documentType)) {
				total = 6;
			}
		}

		return new CounterData(filled, total);
	}

	public CounterData countFinancialInformation(List<FinancialInformation> data, Integer userCategory) {
		int filled = data.size();
		int total =  GlobalConstants.CATEGORY_INSTITUSI_ID.equals(userCategory) ?
			countFinancialStatementDocumentForVerificationData(data,userCategory).getTotal() +
				countFinancialEStatementDocumentForVerificationData(data,userCategory).getTotal() : 0;
		return new CounterData(filled, total);
	}

	public CounterData countFinancialStatementDocumentForVerificationData(List<FinancialInformation> data, Integer userCategory) {
		int filled = 0;
		int total = 2;
		for (FinancialInformation financialInformation : data) {
			if (Constants.FINANCIAL_STATEMENT.equals(financialInformation.getStatementFileType())) {
				filled++;
			}
		}
		return new CounterData(filled, total);
	}

	public CounterData countFinancialEStatementDocumentForVerificationData(List<FinancialInformation> data, Integer userCategory) {
		int filled = 0;
		int total = 6;
		for (FinancialInformation financialInformation : data) {
			if (Constants.FINANCIAL__E_STATEMENT.equals(financialInformation.getStatementFileType())) {
				filled++;
			}
		}
		return new CounterData(filled, total);
	}

	public CounterData countProductParam(ProductPreferenceData data) {
		int filled = 0;
		int total = 0;

		total++;
		if (!Utilities.isNull(data.getProductPreferenceId())) {
			filled++;
		}
		return new CounterData(filled, total);
	}

	public CounterData countLegalInformationInstitusional(List<LegalInformationData> data) {
		AtomicInteger filled = new AtomicInteger();
		int total = 6;

		data.stream()
			.filter(it -> !Utilities.isNull(it.getDocumentType()))
			.forEach(legalInformation -> {
				if (Constants.LEGAL_INFO_NPWP.equals(legalInformation.getDocumentType().getId())
					&& validateLegalInformation(legalInformation)) filled.getAndIncrement();

				if (Constants.LEGAL_INFO_SIUP.equals(legalInformation.getDocumentType().getId())
					&& validateLegalInformation(legalInformation)) filled.getAndIncrement();

				if (Constants.LEGAL_INFO_AKTA_PENDIRIAN.equals(legalInformation.getDocumentType().getId())
					&& validateLegalInformation(legalInformation)) filled.getAndIncrement();

				if (Constants.LEGAL_INFO_AKTA_TERBARU.equals(legalInformation.getDocumentType().getId())
					&& validateLegalInformation(legalInformation)) filled.getAndIncrement();

				if (Constants.LEGAL_INFO_SK_MENKUHAM.equals(legalInformation.getDocumentType().getId())
					&& validateLegalInformation(legalInformation)) filled.getAndIncrement();

				if (Constants.LEGAL_INFO_SK_TDP.equals(legalInformation.getDocumentType().getId())
					&& validateLegalInformation(legalInformation)) filled.getAndIncrement();

			});

		return new CounterData(filled.get(), total);
	}

	public CounterData countLegalInformationIndividual(List<LegalInformationData> data) {
		AtomicInteger filled = new AtomicInteger();
		int total = 1;
		data.stream()
			.filter(it -> !Utilities.isNull(it.getDocumentType()))
			.forEach(legalInformation -> {
				if (Constants.LEGAL_INFO_NPWP.equals(legalInformation.getDocumentType().getId())
					&& validateLegalInformation(legalInformation)) filled.getAndIncrement();

			});
		return new CounterData(filled.get(), total);
	}

	public CounterData countEmergencyContact(List<EmergencyContactData> data) {
		int filled = data.size();
		int total = 1;
		return new CounterData(filled, total);
	}

	public CounterData countBankInformation(List<BankInformationData> data) {
		int filled = data.size();
		int total = 1;
		return new CounterData(filled, total);
	}

	public CounterData countSalesTransaction(List<SalesTransaction> data) {
		int filled = 1;
		int total = 1;
		return new CounterData(filled, total);
	}

	public CounterData countCustomerNotes(List<CustomerNotes> data) {
		int filled = data.size();
		int total = 1;
		return new CounterData(filled, total);
	}

	public CounterData countShareHolderInformation(List<ShareholdersInformation> data) {
		int filled = data.size();
		int total = 1;
		return new CounterData(filled, total);
	}

	public CounterData countSurveyInformation() {
		return new CounterData(0, 0);
	}

	private boolean validateLegalInformation(LegalInformationData data) {
		if (Utilities.isEmptyOrBlank(data.getDocumentFile())) {
			return false;
		} else return !Utilities.isEmptyOrBlank(data.getDocumentNumber());
	}

	public CounterData countInvestProfile(InvestmentProfileData data) {
		int filled = 0;
		int total = 0;

		total++;
		if (!Utilities.isNull(data.getInvestmentObjective())) {
			filled++;
		}

		total++;
		if (!Utilities.isNull(data.getRiskProfile())) {
			filled++;
		}

		return new CounterData(filled, total);
	}

	public CounterData countActivationDocument(DigitalSignatureResponse data) {
		int filled = 0;
		int total = 0;

		total++;
		if (!Utilities.isNull(data.getPmpShariaFile())) {
			filled++;
		}

		total++;
		if (!Utilities.isNull(data.getPmpKonvensionalFile())) {
			filled++;
		}

		total++;
		if (!Utilities.isNull(data.getPoaFile())) {
			filled++;
		}

		return new CounterData(filled, total);
	}
}
