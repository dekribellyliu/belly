package id.investree.customer.controller.external.completing;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.DefaultData;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.controller.external.bankinformation.BankInformationResponseGenerator;
import id.investree.customer.controller.external.businessprofile.BusinessProfileResponseGenerator;
import id.investree.customer.controller.external.customernotes.CustomerNotesResponseGenerator;
import id.investree.customer.controller.external.emergencycontact.EmergencyContactResponseGenerator;
import id.investree.customer.controller.external.financialinformation.FinancialInformationResponseGenerator;
import id.investree.customer.controller.external.legalinformation.LegalInformationResponseGenerator;
import id.investree.customer.controller.external.partnershipinformation.PartnershipInformationResponseGenerator;
import id.investree.customer.controller.external.personalprofile.PersonalProfileResponseGenerator;
import id.investree.customer.controller.external.salestransaction.SalesTransactionResponseGenerator;
import id.investree.customer.controller.external.shareholdersinformation.ShareholderInformationResponseGenerator;
import id.investree.customer.controller.external.surveyinformation.SurveyInformationResponseGenerator;
import id.investree.customer.entity.CustomerInformation;
import id.investree.customer.entity.CustomerRole;
import id.investree.customer.entity.UserData;
import id.investree.customer.model.data.BasicInformation;
import id.investree.customer.model.data.BasicInformationBorrower;
import id.investree.customer.model.data.BasicInformationElementary;
import id.investree.customer.model.data.BasicInformationLender;
import id.investree.customer.model.response.*;
import id.investree.customer.service.balancesheet.BalanceSheetService;
import id.investree.customer.service.bankinformation.BankInformationService;
import id.investree.customer.service.businessprofile.BusinessProfileService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customernotes.CustomerNotesService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.digitalsignature.DigitalSignatureService;
import id.investree.customer.service.emergencycontact.EmergencyContactService;
import id.investree.customer.service.financialinformation.FinancialInformationService;
import id.investree.customer.service.financialratio.FinancialRatioService;
import id.investree.customer.service.financialstatement.FinancialStatementService;
import id.investree.customer.service.financialtrend.FinancialTrendService;
import id.investree.customer.service.industry.IndustryService;
import id.investree.customer.service.legalinformation.LegalInformationService;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.service.masterbank.MasterBankService;
import id.investree.customer.service.partnershipinformation.PartnershipInformationService;
import id.investree.customer.service.postalarea.PostalAreaService;
import id.investree.customer.service.referraldata.ReferralDataService;
import id.investree.customer.service.salestransaction.SalesTransactionService;
import id.investree.customer.service.shareholdersinformation.ShareholdersInformationService;
import id.investree.customer.service.surveyinformation.SurveyInformationService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.CustomerUtils;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class CompletingDataMapper {

	@Autowired
	private BusinessProfileService businessProfileService;

	@Autowired
	private CustomerInformationService customerInformationService;

	@Autowired
	private CustomerRoleService customerRoleService;

	@Autowired
	private BankInformationService bankInformationService;

	@Autowired
	private LegalInformationService legalInformationService;

	@Autowired
	private EmergencyContactService emergencyContactService;

	@Autowired
	private FinancialInformationService financialInformationService;

	@Autowired
	private ShareholdersInformationService shareholdersInformationService;

	@Autowired
	private PostalAreaService postalAreaService;

	@Autowired
	private IndustryService industryService;

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private MasterBankService masterBankService;

	@Autowired
	private SurveyInformationService surveyInformationService;

	@Autowired
	private ReferralDataService referralDataService;

	@Autowired
	private LoginDataService loginDataService;

	@Autowired
	private SalesTransactionService salesTransactionService;

	@Autowired
	private CustomerNotesService customerNotesService;

	@Autowired
	private PartnershipInformationService partnershipInformationService;

	@Autowired
	private FinancialTrendService financialTrendService;

	@Autowired
	private FinancialStatementService financialStatementService;

	@Autowired
	private FinancialRatioService financialRatioService;

	@Autowired
	private BalanceSheetService balanceSheetService;

	@Autowired
	private DigitalSignatureService digitalSignatureService;

	@Autowired
	private CompletingDataFileCounterMapper counterMapper;

	@Autowired
	private CustomerUtils customerUtils;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	@Autowired
	private PersonalProfileResponseGenerator personalProfileResponse;

	@Autowired
	private FinancialInformationResponseGenerator financialInformationResponse;

	@Autowired
	private SalesTransactionResponseGenerator salesTransactionResponse;

	@Autowired
	private EmergencyContactResponseGenerator emergencyContactResponse;

	@Autowired
	private BusinessProfileResponseGenerator businessProfileResponse;

	@Autowired
	private BankInformationResponseGenerator bankInformationResponse;

	@Autowired
	private LegalInformationResponseGenerator legalInformationResponse;

	@Autowired
	private ShareholderInformationResponseGenerator shareholderInformationResponse;

	@Autowired
	private CustomerNotesResponseGenerator customerNotesResponse;

	@Autowired
	private PartnershipInformationResponseGenerator partnershipInformationResponse;

	@Autowired
	private SurveyInformationResponseGenerator surveyInformationResponse;

	@Autowired
	private CompletingAdvancedDataMapper completingAdvancedDataMapper;

	public UserDataResponse generateCompletingData(LoginDataResponse loginData, Integer userTypeIdentification, Long userId, String accessToken) throws MalformedURLException {
		UserData userData = Optional.ofNullable(userDataService.findByLoginId(userId)).orElseThrow(DataNotFoundException::new);
		UserDataResponse userDataResponse = new UserDataResponse();
		userDataResponse.setBasicInfo(generateBasicInformation(loginData, userTypeIdentification, userData, accessToken));
		userDataResponse.setAdvancedInfo(completingAdvancedDataMapper.generateAdvanceBasicData(loginData, userTypeIdentification, userData, accessToken));

		return userDataResponse;
	}

	public UserDataFrontofficeResponse generateCompletingDataFrontOffice(LoginDataResponse loginData, Integer userTypeIdentification, Long userId, String accessToken) throws MalformedURLException {
		UserData userData = Optional.ofNullable(userDataService.findByLoginId(userId)).orElseThrow(DataNotFoundException::new);

		UserDataFrontofficeResponse userDataResponse = new UserDataFrontofficeResponse();
		BasicInformation basicInformation = generateBasicInformation(loginData, userTypeIdentification, userData, accessToken);
		userDataResponse.setBasicInfo(new BasicInformationElementary(basicInformation));
		userDataResponse.setAdvancedInfo(completingAdvancedDataMapper.generateAdvanceBasicData(loginData, userTypeIdentification, userData, accessToken));

		return userDataResponse;
	}

	public Object generateCompletingDataBackoffice(LoginDataResponse loginData, Integer userTypeIdentification, String accessToken) throws MalformedURLException {
		UserData userData = Optional.ofNullable(userDataService.findByLoginId(loginData.getId())).orElseThrow(DataNotFoundException::new);

		if (GlobalConstants.USER_CATEGORY_BORROWER_ID.equals(userTypeIdentification)) {
			UserDataBackofficeBorrowerResponse userDataResponse = new UserDataBackofficeBorrowerResponse();
			BasicInformation basicInformation = generateBasicInformation(loginData, userTypeIdentification, userData, accessToken);
			userDataResponse.setBasicInfo(new BasicInformationBorrower(basicInformation));
			userDataResponse.setAdvancedInfo(completingAdvancedDataMapper.generateAdvanceBasicDataBorrower(userTypeIdentification, userData, accessToken));

			return userDataResponse;
		} else {
			UserDataBackofficeLenderResponse userDataResponse = new UserDataBackofficeLenderResponse();
			BasicInformation basicInformation = generateBasicInformation(loginData, userTypeIdentification, userData, accessToken);
			userDataResponse.setBasicInfo(new BasicInformationLender(basicInformation));
			userDataResponse.setAdvancedInfo(completingAdvancedDataMapper.generateAdvanceBasicDataLender(loginData, userTypeIdentification, userData, accessToken));

			return userDataResponse;
		}
	}

	private BasicInformation generateBasicInformation(LoginDataResponse loginData, Integer userTypeIdentification, UserData userData, String accessToken) {
		CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.findById(userData.getCustomerId()))
			.orElseThrow(() -> new DataNotFoundException("Customer Information"));

		CustomerRole customerRole = Optional.ofNullable(customerRoleService.findByCustomerIdAndType(customerInformation.getId(), userTypeIdentification))
			.orElseThrow(() -> new DataNotFoundException("customer role for customer id: " + customerInformation.getId()));

		Boolean autoWithdrawalStatus = !Utilities.isNull(customerRole.getAutoWithdrawalStatus()) && (customerRole.getAutoWithdrawalStatus().equalsIgnoreCase(Constants.IS_OBJECT_TRUE));
		Boolean feeStatus = !Utilities.isNull(customerRole.getFeeStatus()) && (customerRole.getFeeStatus().equalsIgnoreCase(Constants.IS_OBJECT_TRUE));
		Boolean withHoldingTaxStatus = !Utilities.isNull(customerRole.getWithHoldingTaxStatus()) && (customerRole.getWithHoldingTaxStatus().equalsIgnoreCase(Constants.IS_OBJECT_TRUE));

		DefaultData userCategory = null;
		if (!Utilities.isNull(customerInformation.getUserCategory())) {
			Integer userCategoryId = Integer.parseInt(customerInformation.getUserCategory());
			userCategory = new DefaultData(userCategoryId, GlobalConstants.CATEGORY_INDIVIDU_ID.equals(userCategoryId) ? "Individual" : "Institutional");
		}

		completingDataGlobalParam.getGlobalParam(
			GlobalConstants.GLOBAL_PARAM_NATIONALITY,
			GlobalConstants.GLOBAL_PARAM_POSITION,
			GlobalConstants.GLOBAL_PARAM_DIVISION,
			GlobalConstants.GLOBAL_PARAM_MARITAL_STATUS,
			GlobalConstants.GLOBAL_PARAM_OCCUPATION,
			GlobalConstants.GLOBAL_PARAM_REGISTRATION_STATUS,
			GlobalConstants.GLOBAL_PARAM_EDUCATION,
			GlobalConstants.GLOBAL_PARAM_MOBILE_PREFIX
		);

		DefaultData country = null;
		if (!Utilities.isNull(userData.getNationality())) {
			country = completingDataGlobalParam.getNationalityList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(userData.getNationality())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData position = null;
		if (!Utilities.isNull(userData.getCompanyPosition())) {
			position = completingDataGlobalParam.getPositionList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(userData.getCompanyPosition())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData division = null;
		if (!Utilities.isNull(userData.getCompanyDivision())) {
			division = completingDataGlobalParam.getDivisionList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(userData.getCompanyDivision())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData maritalStatus = null;
		if (!Utilities.isNull(userData.getMaritalStatus())) {
			maritalStatus = completingDataGlobalParam.getMaritalStatusList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(userData.getMaritalStatus())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData occupation = null;
		if (!Utilities.isNull(userData.getOccupation())) {
			occupation = completingDataGlobalParam.getOccupationList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(userData.getOccupation())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData registrationStatus = null;
		if (!Utilities.isNull(customerRole.getStatus())) {
			registrationStatus = completingDataGlobalParam.getRegistrationStatusList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(customerRole.getStatus())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData education = null;
		if (!Utilities.isNull(userData.getEducation())) {
			education = completingDataGlobalParam.getEducationList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(userData.getEducation())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData mobilePrefix = null;
		if (!Utilities.isNull(loginData.getMobilePrefix())) {
			mobilePrefix = completingDataGlobalParam.getMobilePrefixList().stream()
				.filter(v -> v.getId().equals(Integer.valueOf(loginData.getMobilePrefix())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}

		List<CustomerRole> customerRoles = customerRoleService.findAllByCustomerId(customerInformation.getId());
		List<Integer> customerRoleIds = customerRoles.stream().map(CustomerRole::getType).distinct().collect(Collectors.toList());
		List<DefaultData> roles = customerRoleIds.stream().map(roleId -> {
			if (GlobalConstants.USER_CATEGORY_BORROWER_ID.equals(roleId)) {
				return new DefaultData(roleId, GlobalConstants.USER_CATEGORY_BORROWER_NAME);
			} else if (GlobalConstants.USER_CATEGORY_LENDER_ID.equals(roleId)) {
				return new DefaultData(roleId, GlobalConstants.USER_CATEGORY_LENDER_NAME);
			} else if (GlobalConstants.USER_CATEGORY_ANCHOR_ID.equals(roleId)) {
				return new DefaultData(roleId, GlobalConstants.USER_CATEGORY_ANCHOR_NAME);
			}
			return null;
		}).filter(role -> !Utilities.isNull(role)).collect(Collectors.toList());

		Boolean emailVerificationStatus = GlobalConstants.STATUS_YES.equals(loginData.getEmailStatus());

		BasicInformation basicInformation = new BasicInformation();
		basicInformation.setCustomerId(customerInformation.getId());
		basicInformation.setRmBy(customerRole.getRmBy());
		basicInformation.setUserCategory(userCategory);
		basicInformation.setProfilePicture(loginData.getPicture());
		basicInformation.setSalutation(loginData.getSalutation());
		basicInformation.setFullName(loginData.getFullname());
		basicInformation.setUsername(loginData.getUsername());
		basicInformation.setEmailAddress(loginData.getEmailAddress());
		basicInformation.setEmailStatus(emailVerificationStatus);
		basicInformation.setMobilePrefix(mobilePrefix);
		basicInformation.setMobileNumber(loginData.getPhoneNumber());
		basicInformation.setCustomerIdentificationNumber(customerInformation.getCustomerNumber());
		basicInformation.setAge(customerUtils.getAge(userData.getDateOfBirth()));
		basicInformation.setMaritalStatus(maritalStatus);
		basicInformation.setEducation(education);
		basicInformation.setOccupation(occupation);
		basicInformation.setNationality(country);
		basicInformation.setStatus(registrationStatus);
		basicInformation.setInstallment(customerRole.getInstallment());
		basicInformation.setLsfBullet(customerRole.getLsfBullet());
		basicInformation.setFeeStatus(feeStatus);
		basicInformation.setAutoWithdrawalStatus(autoWithdrawalStatus);
		basicInformation.setWithHoldingTaxStatus(withHoldingTaxStatus);
		basicInformation.setPositionWithInstitution(position);
		basicInformation.setDivisionWithInstitution(division);
		basicInformation.setUserRoleTypes(roles);
		basicInformation.setReferralCode(loginData.getReferralCode());
		basicInformation.setAgreeSubscribe(loginData.getAgreeSubscribe().equalsIgnoreCase(GlobalConstants.STATUS_YES));
		basicInformation.setBorrowerInitial(customerInformation.getInitial());

		return basicInformation;
	}
}
