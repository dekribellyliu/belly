package id.investree.customer.controller.external.customerinfo;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.TokenPayload;
import id.investree.core.model.legacy.LegacyModel;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.*;
import id.investree.customer.model.data.VirtualAccountData;
import id.investree.customer.model.legacy.LegacyModelBuilder;
import id.investree.customer.model.request.LoginDataRequest;
import id.investree.customer.model.request.RegistrationStatusRequest;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.service.bankinformation.BankInformationService;
import id.investree.customer.service.borrower.BorrowerService;
import id.investree.customer.service.ciflist.CifListService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customernotification.CustomerNotificationService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.integration.IntegrationService;
import id.investree.customer.service.legacy.SyncLegacyService;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.service.mrinvarbank.MrInvarBankService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.LocalMessageUtils;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class CustomerInformationBody {

	private CustomerRoleService customerRoleService;
	private CustomerInformationService customerInformationService;
	private CustomerNotificationService customerNotificationService;
	private UserDataService userDataService;
	private LoginDataService loginDataService;
	private BankInformationService bankInformationService;
	private MrInvarBankService mrInvarBankService;
	private IntegrationService integrationService;
	private CifListService cifListService;
	private SyncLegacyService syncLegacyService;
	private TokenUtils tokenUtils;
	private LocalMessageUtils messageUtils;
	private StoreDataGlobalParam completingDataGlobalParam;

	public CustomerInformationBody(CustomerRoleService customerRoleService,
								   CustomerInformationService customerInformationService,
								   CustomerNotificationService customerNotificationService,
								   UserDataService userDataService,
								   LoginDataService loginDataService,
								   BorrowerService borrowerService,
								   BankInformationService bankInformationService,
								   MrInvarBankService mrInvarBankService,
								   IntegrationService integrationService,
								   CifListService cifListService,
								   SyncLegacyService syncLegacyService,
								   TokenUtils tokenUtils,
								   LocalMessageUtils messageUtils,
								   StoreDataGlobalParam completingDataGlobalParam) {
		this.customerRoleService = customerRoleService;
		this.customerInformationService = customerInformationService;
		this.customerNotificationService = customerNotificationService;
		this.userDataService = userDataService;
		this.loginDataService = loginDataService;
		this.bankInformationService = bankInformationService;
		this.mrInvarBankService = mrInvarBankService;
		this.integrationService = integrationService;
		this.cifListService = cifListService;
		this.syncLegacyService = syncLegacyService;
		this.tokenUtils = tokenUtils;
		this.messageUtils = messageUtils;
		this.completingDataGlobalParam = completingDataGlobalParam;
	}

	@Transactional(rollbackFor = Exception.class)
	public void changeUserStatus(Long customerId, RegistrationStatusRequest request, String accessToken, boolean isCompletingData) {
		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(accessToken)).orElseThrow(() -> new AppException(messageUtils.emptyToken()));
		Integer userType = GlobalConstants.USER_CATEGORY_BORROWER_NAME.equalsIgnoreCase(request.getUserType()) ? GlobalConstants.USER_CATEGORY_BORROWER_ID : GlobalConstants.USER_CATEGORY_LENDER_ID;
		CustomerRole customerRole = Optional.ofNullable(customerRoleService.findByCustomerIdAndType(customerId, userType)).orElseThrow(DataNotFoundException::new);
		CustomerInformation customerInformation = customerInformationService.findById(customerId);
		UserData userData = userDataService.findByCustomerId(customerId);

		String emailAddress = isCompletingData ? null : Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
				.map(LoginDataResponse::getEmailAddress)
				.orElseThrow(() -> new DataNotFoundException("Login Data"));

		Optional.ofNullable(request.getStatus()).ifPresent(v -> {
			long selectedStatus = customerRole.getStatus();
			if (GlobalConstants.BORROWER_STATUS_REGISTERED_NAME.equalsIgnoreCase(v)) {
				selectedStatus = GlobalConstants.BORROWER_STATUS_REGISTERED;

			} else if (GlobalConstants.BORROWER_STATUS_COMPLETING_DATA_NAME.equalsIgnoreCase(v)
					&& GlobalConstants.BORROWER_STATUS_REGISTERED.equals(customerRole.getStatus())) {
				selectedStatus = GlobalConstants.BORROWER_STATUS_COMPLETING_DATA;

			} else if (GlobalConstants.BORROWER_STATUS_ACTIVE_NAME.equalsIgnoreCase(v)) {
				selectedStatus = GlobalConstants.BORROWER_STATUS_ACTIVE;

				createVa(accessToken, customerInformation, customerRole.getId(), userType);
				createCif(accessToken, userData, customerInformation, customerRole, userType);
				createUserInitial(customerInformation);

				customerNotificationService.sendEmail(accessToken, "brw-completing-data", customerId);

				customerRole.setActiveAt(new Date());
				customerRole.setActiveBy(tokenPayload.getUserId());
				if (null == customerRole.getFillFinishAt()) {
					customerRole.setFillFinishAt(new Date());
				}
				if (!Optional.ofNullable(customerRoleService.saveOrUpdate(customerRole)).isPresent()) {
					throw new AppException("activation failed");
				}

				if (!Optional.ofNullable(customerInformationService.saveOrUpdate(customerInformation)).isPresent()) {
					throw new AppException("initial failed");
				}

			} else if (GlobalConstants.BORROWER_STATUS_REJECTED_NAME.equalsIgnoreCase(v)) {
				selectedStatus = GlobalConstants.BORROWER_STATUS_REJECTED;

			} else if (GlobalConstants.BORROWER_STATUS_INACTIVE_NAME.equalsIgnoreCase(v)) {
				selectedStatus = GlobalConstants.BORROWER_STATUS_INACTIVE;

			}

			customerRole.setStatus(selectedStatus);
			customerRole.setUpdatedBy(tokenPayload.getUserId());
			if (!Optional.ofNullable(customerRoleService.saveOrUpdate(customerRole)).isPresent()) {
				throw new AppException(messageUtils.updateFailed());
			}

			LoginDataRequest loginDataRequest = new LoginDataRequest();
			loginDataRequest.setLoginAttempt(0);
			loginDataRequest.setActive(true);
			loginDataService.updateLoginData(accessToken, userData.getLoginDataId(), loginDataRequest);

			CustomerRole customerRoleData = customerRoleService.findByCustomerIdAndType(customerId, userType);
			CustomerInformation customerInfoData = customerInformationService.findById(customerInformation.getId());
			List<BankInformation> bankInformations = bankInformationService.findByCustomerId(customerId);
			CifList cifList = cifListService.findByCustomerId(customerId);
			LegacyModel legacyModel = new LegacyModelBuilder()
					.setCustomerRole(customerRoleData)
					.setGlobalParam(completingDataGlobalParam)
					.setCustomerInformation(customerInfoData)
					.setBankInformations(bankInformations)
					.setCifList(cifList)
					.setEmail(emailAddress)
					.build(customerId);
			syncLegacyService.sync(accessToken, legacyModel);
		});
	}

	public void createUserInitial(CustomerInformation customerInformation) {
		String userInitial;
		do {
			userInitial = RandomStringUtils.randomAlphabetic(4).toUpperCase();
			if (Utilities.isNull(customerInformationService.findByInitial(userInitial))) {
				customerInformation.setInitial(userInitial);
				break;
			}
		} while (!Utilities.isNull(customerInformationService.findByInitial(userInitial.toUpperCase())));
	}

	private Long createCif(String accessToken, UserData userData,
						   CustomerInformation customerInformation, CustomerRole customerRole, Integer userType) {

		Long userId = tokenUtils.parseToken(accessToken).getUserId();
		if (Utilities.isNull(customerInformation.getCustomerNumber())) {

			CifList cifSaveResult = generateCustomerIdentifier(customerInformation.getId(), userData.getLoginDataId());

			SimpleDateFormat formatMMYY = new SimpleDateFormat("MMyy");
			String sequenceId = String.format("%06d", cifSaveResult.getId());
			String domicileCity = "";
			if (!Utilities.isNull(userData.getDomicileCity())) {
				domicileCity = String.valueOf(userData.getDomicileCity());
			}

			String cifNumber = userType
					+ "." + customerInformation.getUserCategory()
					+ "." + domicileCity
					+ "." + formatMMYY.format(customerRole.getRegisteredAt())
					+ "." + sequenceId;

			customerInformation.setCustomerNumber(cifNumber);
			customerInformation.setUpdatedBy(userId);
			customerInformation.setUpdatedAt(new Date());

			if (!Optional.ofNullable(customerInformationService.saveOrUpdate(customerInformation)).isPresent()) {
				throw new AppException(messageUtils.dataNotValid("user number"));
			}

			return cifSaveResult.getId();
		}

		return 0L;
	}

	@Transactional(rollbackFor = Exception.class)
	public CifList generateCustomerIdentifier(Long customerId, Long userId) {
		return Optional.ofNullable(cifListService.findByCustomerId(customerId))
				.orElseGet(() -> {
					CifList cif = new CifList();
					cif.setCustomerId(customerId);
					cif.setCreatedAt(new Date());
					cif.setCreatedBy(userId);
					return cifListService.save(cif);
				});
	}

	private void createVa(String accessToken, CustomerInformation customerInformation, Long customerRoleId, Integer userType) {
		if (Optional.ofNullable(bankInformationService.findByCustomerIdAndCustomerRoleId(customerInformation.getId(), customerRoleId))
				.isPresent()) {
			return;
		}

		List<MrInvarBank> mrInvarBanks = GlobalConstants.USER_CATEGORY_LENDER_ID.equals(userType) ?
				mrInvarBankService.findById(Constants.getInvarBankLender()) :
				mrInvarBankService.findById(Constants.getInvarBankBorrower());

		VirtualAccountData virtualAccountData = new VirtualAccountData();
		virtualAccountData.setAccessToken(accessToken);
		virtualAccountData.setCustomerInformation(customerInformation);
		virtualAccountData.setCustomerRoleId(customerRoleId);
		virtualAccountData.setUserType(userType);
		integrationService.createBankVirtualAccount(virtualAccountData, mrInvarBanks);
	}
}
