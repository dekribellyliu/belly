package id.investree.customer.controller.external.customerinfo;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.exception.UnauthorizeException;
import id.investree.core.model.DefaultData;
import id.investree.core.model.TokenPayload;
import id.investree.core.model.legacy.LegacyModel;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.controller.external.bankinformation.BankInformationResponseGenerator;
import id.investree.customer.entity.*;
import id.investree.customer.model.data.BankInformationData;
import id.investree.customer.model.data.CustomerPreferenceData;
import id.investree.customer.model.data.InvestmentProfileData;
import id.investree.customer.model.legacy.LegacyModelBuilder;
import id.investree.customer.model.request.*;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.model.response.RegisterStatusResponse;
import id.investree.customer.model.response.UpdateProductPreferenceResponse;
import id.investree.customer.model.response.VirtualAccountResponse;
import id.investree.customer.service.bankinformation.BankInformationService;
import id.investree.customer.service.borrower.BorrowerService;
import id.investree.customer.service.businessprofile.BusinessProfileService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customerproductlist.CustomerProductListService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.integration.IntegrationService;
import id.investree.customer.service.legacy.SyncLegacyService;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.service.mailchimp.MailchimpService;
import id.investree.customer.service.mrinvarbank.MrInvarBankService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.*;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/customer-information", produces = MediaType.APPLICATION_JSON_VALUE)
public class CustomerInformationController extends BaseController {

    private CustomerRoleService customerRoleService;
    private CustomerInformationService customerInformationService;
    private BusinessProfileService businessProfileService;
    private CustomerInformationValidator validator;
    private UserDataService userDataService;
    private LoginDataService loginDataService;
    private BorrowerService borrowerService;
    private IntegrationService integrationService;
    private MrInvarBankService mrInvarBankService;
    private BankInformationService bankInformationService;
    private BankInformationResponseGenerator bankInformationResponseGenerator;
    private LocalMessageUtils messageUtils;
    private TokenUtils tokenUtils;
    private CustomerInformationBody customerInformationBody;
    private StoreDataGlobalParam completingDataGlobalParam;
    private StatusValidation statusValidation;
    private CustomerUtils customerUtils;
    private CustomerProductListService customerProductListService;
    private SyncLegacyService syncLegacyService;
    private MailchimpService mailchimpService;

    public CustomerInformationController(CustomerRoleService customerRoleService,
                                         CustomerInformationService customerInformationService,
                                         BusinessProfileService businessProfileService,
                                         CustomerInformationValidator validator,
                                         UserDataService userDataService,
                                         LoginDataService loginDataService,
                                         BorrowerService borrowerService,
                                         IntegrationService integrationService,
                                         MrInvarBankService mrInvarBankService,
                                         BankInformationService bankInformationService,
                                         BankInformationResponseGenerator bankInformationResponseGenerator,
                                         LocalMessageUtils messageUtils,
                                         TokenUtils tokenUtils,
                                         CustomerInformationBody customerInformationBody,
                                         StoreDataGlobalParam completingDataGlobalParam,
                                         StatusValidation statusValidation,
                                         CustomerUtils customerUtils,
                                         CustomerProductListService customerProductListService,
                                         SyncLegacyService syncLegacyService,
                                         MailchimpService mailchimpService) {
        this.customerRoleService = customerRoleService;
        this.customerInformationService = customerInformationService;
        this.businessProfileService = businessProfileService;
        this.validator = validator;
        this.userDataService = userDataService;
        this.loginDataService = loginDataService;
        this.borrowerService = borrowerService;
        this.integrationService = integrationService;
        this.mrInvarBankService = mrInvarBankService;
        this.bankInformationService = bankInformationService;
        this.bankInformationResponseGenerator = bankInformationResponseGenerator;
        this.messageUtils = messageUtils;
        this.tokenUtils = tokenUtils;
        this.customerInformationBody = customerInformationBody;
        this.completingDataGlobalParam = completingDataGlobalParam;
        this.statusValidation = statusValidation;
        this.customerUtils = customerUtils;
        this.customerProductListService = customerProductListService;
        this.syncLegacyService = syncLegacyService;
        this.mailchimpService = mailchimpService;
    }

    @PostMapping("/product-preference/{userRoleType}/{customerId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<ResultResponse> createProductPreference(HttpServletRequest http,
                                                                  @PathVariable("customerId") Long customerId,
                                                                  @PathVariable("userRoleType") String userRoleType,
                                                                  @RequestBody CreateProductPrefRequest request) {
        String accessToken = http.getHeader("X-Investree-Token");
        TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(accessToken))
                .orElseThrow(() -> new AppException(messageUtils.emptyToken()));

        if (!Long.valueOf(tokenPayload.getCustomerId()).equals(customerId)) {
            throw new AppException(messageUtils.dataNotValid("customer id"));
        }

        String emptyField = validator.isCreateProductPreferenceValid(request);
        if (!Utilities.isEmptyOrBlank(emptyField)) {
            throw new AppException(messageUtils.dataNotValid(emptyField));
        }

        LoginDataResponse loginDataResponse = loginDataService.findByLoginId(accessToken, tokenPayload.getUserId());

        CustomerInformation customerInformationData = Optional.ofNullable(customerInformationService.findById(customerId))
                .orElse(new CustomerInformation());

        customerInformationData.setProductPreference(request.getProductPreference());
        customerInformationData.setUserCategory(String.valueOf(request.getUserCategory()));
        customerInformationData.setUpdatedBy(tokenPayload.getUserId());

        if (GlobalConstants.CATEGORY_INSTITUSI_ID.equals(request.getUserCategory())) {
            customerInformationData.setName(request.getCompanyName());
            customerInformationData.setLegalEntity(request.getLegalEntity());
        }

        CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.saveOrUpdate(customerInformationData))
                .orElseThrow(() -> new AppException(messageUtils.insertFailed("Customer Information")));

        List<CustomerRole> customerRoles = customerRoleService.findAllByCustomerId(customerId);
        for (CustomerRole customerRole : customerRoles) {
            customerRole.setStatus(GlobalConstants.BORROWER_STATUS_REGISTERED);
            customerRole.setRegisteredAt(new Date());
            Long custId = Optional.ofNullable(customerRoleService.saveOrUpdate(customerRole))
                    .map(CustomerRole::getCustomerInformationId)
                    .orElseThrow(() -> new AppException(messageUtils.insertFailed("Customer Role")));
        }

        Integer userTypeIdentification = customerUtils.getUserTypeIdentification(userRoleType);
        CustomerRole customerRole = Optional.ofNullable(customerRoleService
                .findByCustomerIdAndType(customerId, userTypeIdentification))
                .orElseThrow(DataNotFoundException::new);

        List<Integer> productSelections = null;
        if (Constants.USER_PREFERENCE_OSF.equals(request.getProductSelection())) {
            productSelections = Collections.singletonList(Constants.OSF_PRODUCT);
        } else if (Constants.USER_PREFERENCE_PROJECT_FINANCING.equals(request.getProductSelection())) {
            productSelections = Constants.getGroupProductFinancing();
        } else if (Constants.USER_PREFERENCE_WCTL.equals(request.getProductSelection())) {
            productSelections = Constants.getGroupProductWctl();
        }

        for (Integer productSelection : Objects.requireNonNull(productSelections)) {
            CustomerProductList customerProductList = new CustomerProductList();
            customerProductList.setCustomerRoleId(customerRole.getId());
            customerProductList.setType(Long.valueOf(productSelection));
            customerProductList.setCreatedBy(tokenPayload.getUserId());

            CustomerProductList saveCustomerProductList = Optional.ofNullable(customerProductListService
                    .saveOrUpdate(customerProductList))
                    .orElseThrow(() -> new AppException(messageUtils.insertFailed("Customer Product List")));
        }

        completingDataGlobalParam.getGlobalParam(GlobalConstants.GLOBAL_PARAM_REGISTRATION_STATUS);

        RegisterStatusResponse registerStatusResponse = new RegisterStatusResponse();
        customerRoles.forEach(customerRoleStatus -> {
            DefaultData registerStatus = null;
            if (!Utilities.isNull(customerRoleStatus.getStatus())) {
                registerStatus = completingDataGlobalParam.getRegistrationStatusList().stream()
                        .filter(v -> v.getId().equals(Math.toIntExact(customerRoleStatus.getStatus())))
                        .findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
            }

            if (customerRoleStatus.getType().equals(GlobalConstants.USER_CATEGORY_LENDER_ID)) {
                registerStatusResponse.setLender(registerStatus);
            }

            if (customerRoleStatus.getType().equals(GlobalConstants.USER_CATEGORY_BORROWER_ID)) {
                registerStatusResponse.setBorrower(registerStatus);
            }

            if (customerRoleStatus.getType().equals(GlobalConstants.USER_CATEGORY_ANCHOR_ID)) {
                registerStatusResponse.setAnchor(registerStatus);
            }
        });

        UpdateProductPreferenceResponse response = new UpdateProductPreferenceResponse();
        if (GlobalConstants.CATEGORY_INDIVIDU_ID.equals(Integer.valueOf(customerInformation.getUserCategory()))) {
            response.setUserPreferenceStatus(
                    !Utilities.isEmptyOrBlank(customerInformation.getUserCategory())
                            && !customerInformation.getUserCategory().equals("-")
                            && !Utilities.isNull(customerInformation.getProductPreference()));

        } else if (GlobalConstants.CATEGORY_INSTITUSI_ID.equals(Integer.valueOf(customerInformation.getUserCategory()))) {
            response.setUserPreferenceStatus(
                    !Utilities.isEmptyOrBlank(customerInformation.getUserCategory())
                            && !customerInformation.getUserCategory().equals("-")
                            && !Utilities.isNull(customerInformation.getProductPreference())
                            && !Utilities.isEmptyOrBlank(customerInformation.getName())
                            && !Utilities.isNull(customerInformation.getLegalEntity()));
        }
        response.setRegistrationStatus(registerStatusResponse);
        response.setProductPreference(customerInformation.getProductPreference());
        response.setUserCategory(request.getUserCategory());

        LegacyModel legacyModel = new LegacyModelBuilder()
                .setCustomerInformation(customerInformation)
                .setGlobalParam(completingDataGlobalParam)
                .setCustomerRole(customerRole)
                .setProductSelection(Long.valueOf(request.getProductSelection()))
                .setEmail(tokenPayload.getUserEmail())
                .build(customerId);
        syncLegacyService.sync(accessToken, legacyModel);

        if (GlobalConstants.STATUS_YES.equalsIgnoreCase(loginDataResponse.getAgreeSubscribe())) {
            String mailchimpUserType = "Business Borrower";
            if (GlobalConstants.CATEGORY_INDIVIDU_ID.equals(request.getUserCategory())) {
                mailchimpUserType = "Employee Borrower";
            }
            mailchimpService.subscribe(loginDataResponse.getEmailAddress(), loginDataResponse.getFullname(), mailchimpUserType);
        }

        return abstractResponseHandler(response).getResult(messageUtils.insertSuccess());
    }

    @GetMapping("/product-preference/{userRoleType}")
    public ResponseEntity<ResultResponse> findPreferenceByCustomerId(HttpServletRequest http,
                                                                     @PathVariable("userRoleType") String userRoleType) {
        if (!Optional.ofNullable(tokenUtils.parseToken(http)).isPresent()) {
            throw new AppException(messageUtils.emptyToken());
        }

        Integer customerId = Optional.ofNullable(tokenUtils.parseToken(http).getCustomerId())
                .orElseThrow(() -> new AppException(messageUtils.emptyToken()));
        CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.findById((long) customerId))
                .orElseThrow(() -> new DataNotFoundException("Customer Information"));

        CustomerPreferenceData preferenceData = new CustomerPreferenceData();

        if (Optional.ofNullable(customerInformation.getUserCategory()).isPresent()) {
            String userCategoryName = GlobalConstants.CATEGORY_INDIVIDU_ID.toString().equals(customerInformation.getUserCategory())
                    ? GlobalConstants.CATEGORY_INDIVIDU_NAME
                    : GlobalConstants.CATEGORY_INSTITUSI_NAME;
            preferenceData.setUserCategory(new DefaultData(Integer.parseInt(customerInformation.getUserCategory()), userCategoryName));
        }

        if (Optional.ofNullable(customerInformation.getProductPreference()).isPresent()) {
            String preferenceName = GlobalConstants.USER_PREFERENCE_SHARIA.equals(customerInformation.getProductPreference())
                    ? Constants.USER_PREFERENCE_SHARIA_NAME
                    : Constants.USER_PREFERENCE_CONVENTIONAL_NAME;
            preferenceData.setProductPreference(new DefaultData(customerInformation.getProductPreference(), preferenceName));
        }


        Integer userTypeIdentification = customerUtils.getUserTypeIdentification(userRoleType);
        CustomerRole customerRole = customerRoleService.findByCustomerIdAndType((long) customerId, userTypeIdentification);
        List<CustomerProductList> productList = Optional.ofNullable(customerProductListService.findByCustomerRoleIds(customerRole.getId()))
                .orElse(new ArrayList<>());
        List<Integer> productListId = productList.stream().map(CustomerProductList::getType).map(Long::intValue)
                .collect(Collectors.toList());

        DefaultData productSelection = null;
        if (productListId.containsAll(Constants.getGroupProductFinancing())) {
            productSelection = new DefaultData(Constants.USER_PREFERENCE_PROJECT_FINANCING, Constants.USER_PREFERENCE_PROJECT_FINANCING_NAME);
        } else if (productListId.containsAll(Constants.getGroupProductWctl())) {
            productSelection = new DefaultData(Constants.USER_PREFERENCE_WCTL, Constants.USER_PREFERENCE_WCTL_NAME);
        } else if (productListId.contains(Constants.OSF_PRODUCT)) {
            productSelection = new DefaultData(Constants.USER_PREFERENCE_OSF, Constants.USER_PREFERENCE_OSF_NAME);
        }

        preferenceData.setProductSelection(productSelection);

        completingDataGlobalParam.getGlobalParam(GlobalConstants.GLOBAL_PARAM_LEGAL_ENTITY);

        if (GlobalConstants.CATEGORY_INSTITUSI_ID.toString().equals(customerInformation.getUserCategory())) {
            DefaultData legalEntity = null;
            if (null != customerInformation.getLegalEntity()) {
                legalEntity = completingDataGlobalParam.getLegalList().stream()
                        .filter(v -> v.getId().equals(Math.toIntExact(customerInformation.getLegalEntity())))
                        .findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElseThrow(null);
            }
            preferenceData.setLegalEntity(legalEntity);
            preferenceData.setCompanyName(customerInformation.getName());
        }

        return abstractResponseHandler(preferenceData).getResult(messageUtils.dataFetched());
    }

    @PutMapping("/product-preference/{userRoleType}/{customerId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<ResultResponse> updateProductPreference(HttpServletRequest http,
                                                                  @PathVariable("userRoleType") String userRoleType,
                                                                  @PathVariable("customerId") Long customerId,
                                                                  @RequestBody UpdateProductPreferenceRequest request) {
        TokenPayload tokenPayload = tokenUtils.parseToken(http);
        if (Utilities.isNull(tokenPayload)) {
            throw new AppException(messageUtils.emptyToken());
        }

        Integer userTypeIdentification = customerUtils.getUserTypeIdentification(userRoleType);
        statusValidation.statusRestrictionValidation(tokenPayload, userTypeIdentification);

        if (!Long.valueOf(tokenPayload.getCustomerId()).equals(customerId)) {
            throw new AppException(messageUtils.dataNotValid("customer id"));
        }

        String emptyField = validator.isUpdateProductPreferenceValid(request);
        if (!Utilities.isEmptyOrBlank(emptyField)) {
            throw new AppException(messageUtils.dataNotValid(emptyField));
        }

        CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.findById(customerId))
                .orElseThrow(() -> new AppException(emptyField));

        customerInformation.setProductPreference(request.getProductPreference());
        CustomerInformation saveData = Optional.ofNullable(customerInformationService.saveOrUpdate(customerInformation))
                .orElseThrow(() -> new AppException(messageUtils.insertFailed("customer information")));

        LegacyModel legacyModel = new LegacyModelBuilder()
                .setCustomerInformation(saveData)
                .setGlobalParam(completingDataGlobalParam)
                .setEmail(tokenPayload.getUserEmail())
                .build(customerId);

        String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);
        syncLegacyService.sync(accessToken, legacyModel);

        return abstractResponseHandler(true).getResult(messageUtils.updateSuccess());
    }

    @PostMapping("/investment-profile")
    public ResponseEntity<ResultResponse> createInvestmentProfile(HttpServletRequest http,
                                                                  @RequestBody InvestmentProfileRequest request) {
        TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
                .orElseThrow(() -> new AppException(messageUtils.emptyToken()));

        CustomerRole checkingStatus = customerRoleService.findByCustomerIdAndType(Long.valueOf(tokenPayload.getCustomerId()), tokenPayload.getUserType());
        if (Constants.STATUS_ACTIVE.equals(checkingStatus.getStatus()) || Constants.STATUS_PENDING_VERIFICATION.equals(checkingStatus.getStatus()) || Constants.STATUS_INACTIVE.equals(checkingStatus.getStatus())) {
            throw new AppException(messageUtils.statusRestricted());
        }

        CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.findById(Long.valueOf(tokenPayload.getCustomerId())))
                .map(v -> {
                    v.setInvestmentObjective(request.getInvestmentObjective());
                    v.setRiskProfile(request.getRiskProfile());
                    v.setUpdatedBy(tokenPayload.getUserId());

                    return v;
                }).orElseThrow(DataNotFoundException::new);

        CustomerInformation saveData = customerInformationService.saveOrUpdate(customerInformation);
        if (!Optional.ofNullable(saveData).isPresent()) {
            throw new AppException(messageUtils.insertFailed("investment profile"));
        }

        return abstractResponseHandler(saveData).getResult(messageUtils.insertSuccess());
    }

    @PutMapping("/investment-profile/{customerId}")
    public ResponseEntity<ResultResponse> updateInvestmentProfile(HttpServletRequest http,
                                                                  @PathVariable("customerId") Long customerId,
                                                                  @RequestBody InvestmentProfileBulkRequest request) {
        TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
                .orElseThrow(() -> new AppException(messageUtils.emptyToken()));

        CustomerRole checkingStatus = customerRoleService.findByCustomerIdAndType(Long.valueOf(tokenPayload.getCustomerId()), tokenPayload.getUserType());
        if (Constants.STATUS_ACTIVE.equals(checkingStatus.getStatus()) || Constants.STATUS_PENDING_VERIFICATION.equals(checkingStatus.getStatus()) || Constants.STATUS_INACTIVE.equals(checkingStatus.getStatus())) {
            throw new AppException(messageUtils.statusRestricted());
        }

        String accessToken = http.getHeader("X-Investree-Token");

        CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.findById(customerId))
                .map(v -> {
                    if (!Utilities.isNull(request.getProductPreference())) {
                        v.setProductPreference(request.getProductPreference().getProductPreference());
                    }

                    if (!Utilities.isNull(request.getPersonalData())) {
                        v.setAnnualIncome(request.getPersonalData().getAnnualIncome());
                        v.setSourceOfFund(request.getPersonalData().getSourceOfFund());
                    }

                    if (!Utilities.isNull(request.getInvestmentProfile())) {
                        v.setInvestmentObjective(request.getInvestmentProfile().getInvestmentObjective());
                        v.setRiskProfile(request.getInvestmentProfile().getRiskProfile());
                    }

                    v.setUpdatedBy(tokenPayload.getUserId());

                    v = Optional.ofNullable(customerInformationService.saveOrUpdate(v))
                            .orElseThrow(() -> new AppException(messageUtils.updateFailed()));

                    return v;
                }).orElseThrow(DataNotFoundException::new);

        if (!Utilities.isNull(request.getApuPptCheck())) {
            ApuPptCheckRequest apuPptCheckRequest = new ApuPptCheckRequest();
            apuPptCheckRequest.setCustomerId(customerId);
            apuPptCheckRequest.setCheckingDate(request.getApuPptCheck().getCheckingDate());
            apuPptCheckRequest.setCheckingFile(request.getApuPptCheck().getCheckingFile());
            apuPptCheckRequest.setCheckingResult(request.getApuPptCheck().getCheckingResult());

            if (!Optional.ofNullable(businessProfileService.saveBackgroundCheck(apuPptCheckRequest, accessToken)).isPresent()) {
                throw new AppException(messageUtils.updateFailed());
            }
        }

        InvestmentProfileData investmentProfileResponse = new InvestmentProfileData();
        investmentProfileResponse.setInvestmentObjective(customerInformation.getInvestmentObjective());
        investmentProfileResponse.setRiskProfile(customerInformation.getRiskProfile());

        return abstractResponseHandler(GlobalConstants.USER_TYPE_FRONTOFFICE_ID.equals(tokenPayload.getUserType()) ? investmentProfileResponse : true)
                .getResult(messageUtils.updateSuccess());
    }

    @PutMapping("/resend-va/{userRoleType}/{customerId}/{bankId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<ResultResponse> resendVa(HttpServletRequest http,
                                                   @PathVariable("userRoleType") String userRoleType,
                                                   @PathVariable("customerId") Long customerId,
                                                   @PathVariable("bankId") Long bankId) {

        TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
                .orElseThrow(() -> new AppException(messageUtils.emptyToken()));

        String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);
        LoginDataResponse loginDataResponse = Optional.ofNullable(loginDataService.findByLoginId(accessToken, tokenPayload.getUserId()))
                .orElseThrow(() -> new DataNotFoundException("user"));

        CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.findById(customerId))
                .orElseThrow(() -> new DataNotFoundException("customer"));

        BankInformation bankInformation = Optional.ofNullable(bankInformationService.findByCustomerIdAndBankId(customerId, bankId))
                .orElseThrow(() -> new DataNotFoundException("bank"));

        Long userTypeId = Long.valueOf(customerUtils.getUserTypeIdentification(userRoleType));
        String bin = Optional.ofNullable(mrInvarBankService.findByBankIdAndType(bankInformation.getMasterBankId(),
                userTypeId, Constants.INVAR_BANK_TYPE_ALL))
                .map(MrInvarBank::getBankBin)
                .orElseThrow(() -> new DataNotFoundException("Invar Bank"));

        RecreateVirtualAccountRequest recreateVirtualAccountRequest = new RecreateVirtualAccountRequest();
        recreateVirtualAccountRequest.setAccountName(loginDataResponse.getFullname());
        recreateVirtualAccountRequest.setBin(bin);
        recreateVirtualAccountRequest.setVaNumber(bankInformation.getBankAccountNumber());

        VirtualAccountResponse response = integrationService.resendCreateVa(accessToken, recreateVirtualAccountRequest);
        if (!response.getStatus().equalsIgnoreCase("SUCCESS")) {
            throw new AppException("resend va failed");
        }

        bankInformation.setVerifiedStatus("SUCCESS".equalsIgnoreCase(response.getStatus()) ? "Verified" : "Unverified");
        if (!Optional.ofNullable(bankInformationService.saveOrUpdate(bankInformation)).isPresent()) {
            throw new AppException(messageUtils.updateFailed());
        }

        BankInformationData bankInformationData = bankInformationResponseGenerator.generateBankInformation(bankInformation, accessToken, userTypeId);

        List<BankInformation> bankInformations = bankInformationService.findByCustomerId(customerId);
        LegacyModel legacyModel = new LegacyModelBuilder()
                .setBankInformations(bankInformations)
                .setGlobalParam(completingDataGlobalParam)
                .setEmail(tokenPayload.getUserEmail())
                .build(customerId);

        syncLegacyService.sync(accessToken, legacyModel);

        return abstractResponseHandler(bankInformationData).getResult(messageUtils.updateSuccess());
    }

    @PutMapping("/change-status/{customerId}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity<ResultResponse> updateRegistrationStatus(HttpServletRequest http,
                                                                   @PathVariable("customerId") Long customerId,
                                                                   @RequestBody RegistrationStatusRequest request) {

        String accessToken = http.getHeader("X-Investree-Token");
        TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(accessToken))
                .orElseThrow(UnauthorizeException::new);

        if (!GlobalConstants.USER_TYPE_BACKOFFICE_ID.equals(tokenPayload.getUserType())) {
            throw new DataNotFoundException();
        }

        customerInformationBody.changeUserStatus(customerId, request, accessToken, false);
        return abstractResponseHandler(true).getResult(messageUtils.updateSuccess());
    }
}
