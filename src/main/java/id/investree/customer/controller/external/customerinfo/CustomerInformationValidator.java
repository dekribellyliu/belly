package id.investree.customer.controller.external.customerinfo;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.utils.Utilities;
import id.investree.customer.model.request.CreateProductPrefRequest;
import id.investree.customer.model.request.UpdateProductPreferenceRequest;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CustomerInformationValidator {

	@Autowired
	private CustomerRoleService customerRoleService;

	@Autowired
	private LocalMessageUtils messageUtils;

	String isCreateProductPreferenceValid(CreateProductPrefRequest request) {
		if (Utilities.isNull(request.getUserCategory())) {
			return "user category";
		}

		if (!GlobalConstants.CATEGORY_INDIVIDU_ID.equals(request.getUserCategory()) &&
			!GlobalConstants.CATEGORY_INSTITUSI_ID.equals(request.getUserCategory())) {
			return "user category";
		}

		if (Utilities.isNull(request.getProductPreference())) {
			return "product preference";
		}

		if (Utilities.isNull(request.getProductSelection())) {
			return "product selection";
		}

		if (GlobalConstants.CATEGORY_INSTITUSI_ID.equals(request.getUserCategory())) {
			if (Utilities.isEmptyOrBlank(request.getCompanyName())) {
				return "company name";
			} else if (Utilities.isNull(request.getLegalEntity())) {
				return "legal entity";
			} else if (Constants.USER_PREFERENCE_OSF.equals(request.getProductSelection())) {
				return "product selection institution";
			}
		} else {
			if (!Constants.USER_PREFERENCE_OSF.equals(request.getProductSelection())) {
				return "product selection individual";
			}
		}

		return "";
	}

	String isUpdateProductPreferenceValid(UpdateProductPreferenceRequest request) {
		if (Utilities.isNull(request.getProductPreference())) {
			return "product preference";
		}
		return "";
	}

	public Boolean isUserTypeValid(int userType) {
		return GlobalConstants.USER_CATEGORY_LENDER_ID.equals(userType)
			|| GlobalConstants.USER_CATEGORY_BORROWER_ID.equals(userType)
			|| GlobalConstants.USER_CATEGORY_ANCHOR_ID.equals(userType)
			|| GlobalConstants.USER_CATEGORY_PARTNER_ID.equals(userType);
	}
}
