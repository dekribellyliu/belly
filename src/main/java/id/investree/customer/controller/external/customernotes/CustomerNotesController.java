package id.investree.customer.controller.external.customernotes;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.TokenPayload;
import id.investree.core.model.paginate.PaginationData;
import id.investree.core.utils.TokenUtils;
import id.investree.customer.model.data.CustomerNotesData;
import id.investree.customer.service.customernotes.CustomerNotesService;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/customer-notes", produces = MediaType.APPLICATION_JSON_VALUE)
public class CustomerNotesController extends BaseController {

	@Autowired
	private CustomerNotesService customerNotesService;

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private CustomerNotesResponseGenerator customerNotesResponseGenerator;

	@GetMapping("{customerId}")
	public ResponseEntity<ResultResponse> getAllNotes(HttpServletRequest http,
													  @PathVariable("customerId") Long customerId,
													  @RequestParam(value = "limit", defaultValue = "5") Integer limit,
													  @RequestParam("page") Integer page) {

		String accessToken = http.getHeader("X-Investree-Token");
		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
				.orElseThrow(() -> new AppException(messageUtils.emptyToken()));
		if (!GlobalConstants.USER_TYPE_BACKOFFICE_ID.equals(tokenPayload.getUserType())) {
			throw new AppException(messageUtils.dataNotValid("token not valid"));
		}
		List<CustomerNotesData> customerNotesList = customerNotesResponseGenerator.generateCustomerNotesDataDetails
				(customerNotesService.findByCustomerId(customerId), accessToken);

		if (customerNotesList.isEmpty()) {
			throw new DataNotFoundException();
		}

		int originalSize = customerNotesList.size();
		int totalPages = originalSize / limit;
		int residualSize = originalSize % limit;
		if (residualSize > 0) {
			totalPages += 1;
		}

		int start = (page - 1) * limit;
		int end = (page * limit);

		end = Math.min(end, originalSize);

		customerNotesList = customerNotesList.subList(start, end);

		PaginationData<CustomerNotesData> response = new PaginationData<>(originalSize, totalPages, page, limit, customerNotesList);

		return abstractResponseHandler(response).getResult(messageUtils.dataFetched());
	}
}
