package id.investree.customer.controller.external.customernotes;

import id.investree.core.model.TokenPayload;
import id.investree.core.utils.TokenUtils;
import id.investree.customer.entity.CustomerNotes;
import id.investree.customer.model.data.CustomerNotesData;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.service.logindata.LoginDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomerNotesResponseGenerator {

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private LoginDataService loginDataService;

	public List<CustomerNotesData> generateCustomerNotesDataDetails(List<CustomerNotes> customerNotes, String accessToken) {
		List<CustomerNotesData> customerNotesData = new ArrayList<>();
		for (CustomerNotes customerNote : customerNotes) {
			customerNotesData.add(generateCustomerNotesDataDetail(customerNote, accessToken));
		}
		return customerNotesData;
	}

	public CustomerNotesData generateCustomerNotesDataDetail(CustomerNotes customerNotes, String accessToken) {
		TokenPayload tokenPayload = tokenUtils.parseToken(accessToken);
		LoginDataResponse loginData = loginDataService.findByLoginId(accessToken, customerNotes.getCreatedBy());
		CustomerNotesData data = new CustomerNotesData();
		data.setId(customerNotes.getId());
		data.setName(loginData.getFullname());
		data.setRemarks(customerNotes.getNote());
		data.setCreatedAt(customerNotes.getCreatedAt());
		data.setCreatedBy(tokenPayload.getUserId().equals(loginData.getId()));
		data.setUpdatedAt(customerNotes.getUpdateAt());

		return data;
	}
}
