package id.investree.customer.controller.external.emergencycontact;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.exception.UnauthorizeException;
import id.investree.core.model.TokenPayload;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.CustomerInformation;
import id.investree.customer.entity.EmergencyContact;
import id.investree.customer.model.data.EmergencyContactData;
import id.investree.customer.model.request.EmergencyContactBulkRequest;
import id.investree.customer.model.request.EmergencyContactRequest;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.emergencycontact.EmergencyContactService;
import id.investree.customer.utils.CustomerUtils;
import id.investree.customer.utils.LocalMessageUtils;
import id.investree.customer.utils.StatusValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/emergency-contact", produces = MediaType.APPLICATION_JSON_VALUE)
public class EmergencyContactController extends BaseController {

	@Autowired
	private CustomerRoleService customerRoleService;

	@Autowired
	private EmergencyContactService emergencyContactService;

	@Autowired
	private CustomerInformationService customerInformationService;

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private EmergencyContactValidator emergencyContactValidator;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private EmergencyContactResponseGenerator emergencyContactResponseGenerator;

	@Autowired
	private StatusValidation statusValidation;

	@Autowired
	private CustomerUtils customerUtils;

	@PostMapping("{userRoleType}")
	public ResponseEntity<ResultResponse> add(HttpServletRequest http,
	                                          @PathVariable("userRoleType") String userRoleType,
	                                          @RequestBody EmergencyContactRequest request) {


		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
			.orElseThrow(() -> new AppException(messageUtils.emptyToken()));

		Integer userTypeIdentification = customerUtils.getUserTypeIdentification(userRoleType);
		statusValidation.statusRestrictionValidation(tokenPayload, userTypeIdentification);

		String accessToken = http.getHeader("X-Investree-Token");

		Long custId = Optional.ofNullable(customerInformationService.findById(Long.valueOf(tokenPayload.getCustomerId())))
			.map(CustomerInformation::getId)
			.orElseThrow(DataNotFoundException::new);

		request.setCustomerId(custId);
		emergencyContactValidator.contactValidator(request, accessToken);

		EmergencyContact emergencyContact = new EmergencyContact();
		emergencyContact.setCustomerId(custId);
		emergencyContact.setRelationship(request.getRelationship());
		emergencyContact.setFullname(request.getFullName());
		emergencyContact.setMobilePrefix(Utilities.getIdFromDefaultData(request.getMobilePrefix()));
		emergencyContact.setMobileNumber(request.getMobileNumber());
		emergencyContact.setEmailAddress(request.getEmailAddress());
		emergencyContact.setAddress(request.getAddress());
		emergencyContact.setProvince(Utilities.getIdFromDefaultData(request.getProvince()));
		emergencyContact.setCity(Utilities.getIdFromDefaultData(request.getCity()));
		emergencyContact.setDistrict(Utilities.getIdFromDefaultData(request.getDistrict()));
		emergencyContact.setVillage(Utilities.getIdFromDefaultData(request.getVillage()));
		emergencyContact.setPostalCode(request.getPostalCode());
		emergencyContact.setIdentityCardUrl(request.getIdentityCardUrl());
		emergencyContact.setIdentityCardNumber(request.getIdentityCardNumber());
		emergencyContact.setIdentityCardExpired(request.getIdentityExpiryDate());
		emergencyContact.setCreatedBy(tokenPayload.getUserId());
		EmergencyContact saveData = Optional.ofNullable(emergencyContactService.saveOrUpdate(emergencyContact))
			.orElseThrow(() -> new AppException(messageUtils.insertFailed("Emergency Contact")));

		EmergencyContactData emergencyContactResponse = emergencyContactResponseGenerator.generateEmergencyContact(saveData, accessToken);

		return abstractResponseHandler(emergencyContactResponse).getResult(messageUtils.insertSuccess());
	}

	@DeleteMapping("{userRoleType}/{emergencyId}")
	public ResponseEntity<ResultResponse> delete(@PathVariable("userRoleType") String userRoleType,
	                                             @PathVariable("emergencyId") Long emergencyId,
	                                             HttpServletRequest http) {

		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
			.orElseThrow(() -> new AppException(messageUtils.emptyToken()));

		Integer userTypeIdentification = customerUtils.getUserTypeIdentification(userRoleType);
		statusValidation.statusRestrictionValidation(tokenPayload, userTypeIdentification);

		EmergencyContact contactData = Optional.ofNullable(emergencyContactService.findByIdAndCustomer(emergencyId, Long.valueOf(tokenPayload.getCustomerId())))
			.orElseThrow(DataNotFoundException::new);

		contactData.setDeleteBy(tokenPayload.getUserId());
		contactData.setDeletedAt(new Date());

		EmergencyContact emergencyContact = Optional.ofNullable(emergencyContactService.saveOrUpdate(contactData))
			.orElseThrow(() -> new AppException(messageUtils.deleteFailed()));

		return abstractResponseHandler(true).getResult(messageUtils.deleteSuccess());
	}

	@PutMapping("/{userRoleType}/{emergencyId}")
	public ResponseEntity<ResultResponse> update(@PathVariable("userRoleType") String userRoleType,
	                                             @PathVariable("emergencyId") Long emergencyId,
	                                             @RequestBody EmergencyContactRequest request, HttpServletRequest http) {

		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
			.orElseThrow(() -> new AppException(messageUtils.emptyToken()));

		Integer userTypeIdentification = customerUtils.getUserTypeIdentification(userRoleType);
		statusValidation.statusRestrictionValidation(tokenPayload, userTypeIdentification);

		String accessToken = http.getHeader("X-Investree-Token");

		EmergencyContact emergencyContact = Optional.ofNullable(emergencyContactService.findByIdAndCustomer(emergencyId, Long.valueOf(tokenPayload.getCustomerId())))
			.orElseThrow(DataNotFoundException::new);

		request.setCustomerId(Long.valueOf(tokenPayload.getCustomerId()));
		emergencyContactValidator.contactValidator(request, accessToken);

		emergencyContact.setRelationship(request.getRelationship());
		emergencyContact.setFullname(request.getFullName());
		emergencyContact.setMobilePrefix(Utilities.getIdFromDefaultData(request.getMobilePrefix()));
		emergencyContact.setMobileNumber(request.getMobileNumber());
		emergencyContact.setEmailAddress(request.getEmailAddress());
		emergencyContact.setAddress(request.getAddress());
		emergencyContact.setProvince(Utilities.getIdFromDefaultData(request.getProvince()));
		emergencyContact.setCity(Utilities.getIdFromDefaultData(request.getCity()));
		emergencyContact.setDistrict(Utilities.getIdFromDefaultData(request.getDistrict()));
		emergencyContact.setVillage(Utilities.getIdFromDefaultData(request.getVillage()));
		emergencyContact.setPostalCode(request.getPostalCode());
		emergencyContact.setIdentityCardUrl(request.getIdentityCardUrl());
		emergencyContact.setIdentityCardNumber(request.getIdentityCardNumber());
		emergencyContact.setIdentityCardExpired(request.getIdentityExpiryDate());
		emergencyContact.setUpdateBy(tokenPayload.getUserId());
		EmergencyContact saveData = Optional.ofNullable(emergencyContactService.saveOrUpdate(emergencyContact))
			.orElseThrow(() -> new AppException(messageUtils.updateFailed()));

		EmergencyContactData emergencyContactResponse = emergencyContactResponseGenerator.generateEmergencyContact(saveData, accessToken);

		return abstractResponseHandler(emergencyContactResponse).getResult(messageUtils.updateSuccess());
	}

	@PostMapping("/all")
	public ResponseEntity<ResultResponse> addBulk(@RequestBody EmergencyContactBulkRequest requests, HttpServletRequest http) {

		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
			.orElseThrow(() -> new AppException(messageUtils.emptyToken()));

		if (!GlobalConstants.USER_TYPE_BACKOFFICE_ID.equals(tokenPayload.getUserType())) {
			throw new UnauthorizeException();
		}

		String accessToken = http.getHeader("X-Investree-Token");

		Long customerId = requests.getCustomerId();

		List<EmergencyContact> emergencyContacts = new ArrayList<>();

		for (EmergencyContactRequest request : requests.getData()) {
			request.setCustomerId(customerId);

			emergencyContactValidator.contactValidator(request, accessToken);

			EmergencyContact emergencyContactData = emergencyContactService.findByIdentityCardNumberAndCustomerIdAndDeletedIsNull(request.getIdentityCardNumber(), customerId);

			if (Utilities.isNull(emergencyContactData) && !request.isDelete()) {
				emergencyContactData = new EmergencyContact();
				emergencyContactData.setCreatedBy(tokenPayload.getUserId());
			} else if (!Utilities.isNull(emergencyContactData) && request.isDelete()) {
				emergencyContactData.setDeleteBy(tokenPayload.getUserId());
				emergencyContactData.setDeletedAt(new Date());
			} else {
				emergencyContactData.setUpdateBy(tokenPayload.getUserId());
			}

			emergencyContactData.setCustomerId(customerId);
			emergencyContactData.setRelationship(request.getRelationship());
			emergencyContactData.setFullname(request.getFullName());
			emergencyContactData.setMobilePrefix(Utilities.getIdFromDefaultData(request.getMobilePrefix()));
			emergencyContactData.setMobileNumber(request.getMobileNumber());
			emergencyContactData.setEmailAddress(request.getEmailAddress());
			emergencyContactData.setAddress(request.getAddress());
			emergencyContactData.setProvince(Utilities.getIdFromDefaultData(request.getProvince()));
			emergencyContactData.setCity(Utilities.getIdFromDefaultData(request.getCity()));
			emergencyContactData.setDistrict(Utilities.getIdFromDefaultData(request.getDistrict()));
			emergencyContactData.setVillage(Utilities.getIdFromDefaultData(request.getVillage()));
			emergencyContactData.setPostalCode(request.getPostalCode());
			emergencyContactData.setIdentityCardUrl(request.getIdentityCardUrl());
			emergencyContactData.setIdentityCardNumber(request.getIdentityCardNumber());
			emergencyContactData.setIdentityCardExpired(request.getIdentityExpiryDate());

			emergencyContacts.add(emergencyContactData);
		}

		emergencyContacts = Optional.ofNullable(emergencyContactService.saveAll(emergencyContacts))
			.orElseThrow(() -> new AppException(messageUtils.insertFailed("emergency contact bulk")));

		emergencyContacts = emergencyContacts.stream().filter(v -> Utilities.isNull(v.getDeletedAt())).collect(Collectors.toList());

		List<EmergencyContactData> emergencyContactDataList = emergencyContactResponseGenerator.generateEmergencyContacts(emergencyContacts, accessToken);

		return abstractResponseHandler(emergencyContactDataList).getResult(messageUtils.insertSuccess());
	}
}
