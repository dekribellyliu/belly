package id.investree.customer.controller.external.emergencycontact;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.model.DefaultData;
import id.investree.core.utils.DateUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.EmergencyContact;
import id.investree.customer.model.data.EmergencyContactData;
import id.investree.customer.service.postalarea.PostalAreaService;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EmergencyContactResponseGenerator {

	@Autowired
	private PostalAreaService postalAreaService;

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	public List<EmergencyContactData> generateEmergencyContacts(List<EmergencyContact> emergencyContacts, String accessToken) {
		List<EmergencyContactData> emergencyContactData = new ArrayList<>();
		for (EmergencyContact emergencyContact : emergencyContacts) {
			if (Utilities.isNull(emergencyContact.getDeleteBy())) {
				emergencyContactData.add(generateEmergencyContact(emergencyContact, accessToken));
			}
		}
		return emergencyContactData;
	}

	public EmergencyContactData generateEmergencyContact(EmergencyContact emergencyContact, String accessToken) {
		DefaultData province = postalAreaService.findProvince(accessToken, emergencyContact.getProvince());
		DefaultData city = postalAreaService.findCity(accessToken, emergencyContact.getCity());
		DefaultData district = postalAreaService.findDistrict(accessToken, emergencyContact.getDistrict());
		DefaultData subDistrict = postalAreaService.findVillage(accessToken, emergencyContact.getVillage());

		completingDataGlobalParam.getGlobalParam(
			GlobalConstants.GLOBAL_PARAM_RELATIONSHIP,
			GlobalConstants.GLOBAL_PARAM_MOBILE_PREFIX
		);

		DefaultData relationship = null;
		if (!Utilities.isNull(emergencyContact.getRelationship())) {
			relationship = completingDataGlobalParam.getRelationshipList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(emergencyContact.getRelationship())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData mobilePrefix = null;
		if (!Utilities.isNull(emergencyContact.getMobilePrefix())) {
			mobilePrefix = completingDataGlobalParam.getMobilePrefixList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(emergencyContact.getMobilePrefix())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}

		EmergencyContactData emergencyContactData = new EmergencyContactData();
		emergencyContactData.setEmergencyId(emergencyContact.getId());
		emergencyContactData.setRelationship(relationship);
		emergencyContactData.setFullName(emergencyContact.getFullname());
		emergencyContactData.setMobilePrefix(mobilePrefix);
		emergencyContactData.setMobileNumber(emergencyContact.getMobileNumber());
		emergencyContactData.setEmailAddress(emergencyContact.getEmailAddress());
		emergencyContactData.setAddress(emergencyContact.getAddress());
		emergencyContactData.setProvince(province);
		emergencyContactData.setCity(city);
		emergencyContactData.setDistrict(district);
		emergencyContactData.setVillage(subDistrict);
		emergencyContactData.setPostalCode(emergencyContact.getPostalCode());
		emergencyContactData.setIdentityCardUrl(emergencyContact.getIdentityCardUrl());
		emergencyContactData.setIdentityCardNumber(emergencyContact.getIdentityCardNumber());
		emergencyContactData.setIdentityExpiryDate(emergencyContact.getIdentityCardExpired());
		emergencyContactData.setIdCardLifetime(GlobalConstants.DEFAULT_LIFETIME_DATE.equals(DateUtils.convertToString(emergencyContact.getIdentityCardExpired(), DateUtils.FORMAT_DATE)));

		return emergencyContactData;
	}
}
