package id.investree.customer.controller.external.emergencycontact;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.regex.RegexValidator;
import id.investree.core.utils.Utilities;
import id.investree.customer.model.request.EmergencyContactRequest;
import id.investree.customer.service.postalarea.PostalAreaService;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class EmergencyContactValidator {

	@Autowired
	LocalMessageUtils localMessageUtils;

	@Autowired
	private PostalAreaService postalAreaService;

	public void contactValidator(EmergencyContactRequest request, String accessToken) {

		if (Utilities.isNull(request.getCustomerId())) {
			throw new AppException(localMessageUtils.errorBlank("customer id"));
		}

		if (!Utilities.isNull(request.getMobileNumber()) && !RegexValidator.isMobileNumberValid(request.getMobileNumber())) {
			throw new AppException(localMessageUtils.mobileNumberLength());
		}

		if (!Utilities.isEmptyOrBlank(request.getPostalCode()) && !RegexValidator.isNumeric(request.getPostalCode())) {
			throw new AppException(localMessageUtils.errorNumberOnly("postal code"));
		}

		if (!Utilities.isEmptyOrBlank(request.getPostalCode()) && request.getPostalCode().length() != 5) {
			throw new AppException(localMessageUtils.dataNotValid("postal code length"));
		}

		if (!Utilities.isNull(request.getIdentityCardNumber())
			&& !RegexValidator.isKtpNumberValid(request.getIdentityCardNumber())) {
			throw new AppException(localMessageUtils.identityCardNumberLength());
		}

		if (!Utilities.isEmptyOrBlank(request.getIdentityCardUrl()) &&
			!Utilities.isValidExtensionFile(request.getIdentityCardUrl(),
					GlobalConstants.FILE_EXT_JPG, GlobalConstants.FILE_EXT_JPEG, GlobalConstants.FILE_EXT_PNG, GlobalConstants.FILE_EXT_PDF)) {
			throw new AppException(localMessageUtils.dataNotValid("identity card file extension must be pdf, png, jpg or jpeg"));
		}

		if (!Utilities.isNull(request.getIdentityExpiryDate())
			&& request.getIdentityExpiryDate().compareTo(new Date()) < 0) {
			throw new AppException(localMessageUtils.identityCardExpiry());
		}

		if (Utilities.isNull(request.isDelete())) {
			throw new AppException(localMessageUtils.dataNotValid("delete status"));
		}

		if (Utilities.isNull(request.getProvince()) && !Utilities.isNull(request.getCity())) {
			throw new AppException("Can't update city, please update province first!");
		} else if (!Utilities.isNull(request.getProvince()) && !Utilities.isNull(request.getCity())) {
			postalAreaService.checkCity(accessToken, Utilities.getIdFromDefaultData(request.getCity()), Utilities.getIdFromDefaultData(request.getProvince()));
		}

		if (Utilities.isNull(request.getCity()) && !Utilities.isNull(request.getDistrict())) {
			throw new AppException("Can't update district, please update city first!");
		} else if (!Utilities.isNull(request.getCity()) && !Utilities.isNull(request.getDistrict())) {
			postalAreaService.checkDistrict(accessToken, Utilities.getIdFromDefaultData(request.getDistrict()), Utilities.getIdFromDefaultData(request.getCity()));
		}

		if (Utilities.isNull(request.getDistrict()) && !Utilities.isNull(request.getVillage())) {
			throw new AppException("Can't update village, please update district first!");
		} else if (!Utilities.isNull(request.getDistrict()) && !Utilities.isNull(request.getVillage())) {
			postalAreaService.checkVillage(accessToken, Utilities.getIdFromDefaultData(request.getVillage()), Utilities.getIdFromDefaultData(request.getDistrict()));
		}
	}
}
