package id.investree.customer.controller.external.financialinformation;

import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.FinancialInformation;
import id.investree.customer.entity.FinancialStatement;
import id.investree.customer.entity.FinancialTrend;
import id.investree.customer.model.legacy.LegacyModelBuilder;
import id.investree.customer.model.request.FinancialInformationListRequest;
import id.investree.customer.model.request.FinancialStatementRequest;
import id.investree.customer.model.request.FinancialTrendRequest;
import id.investree.customer.service.financialinformation.FinancialInformationService;
import id.investree.customer.service.financialstatement.FinancialStatementService;
import id.investree.customer.service.financialtrend.FinancialTrendService;
import id.investree.customer.service.legacy.SyncLegacyService;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class FinancialInformationBody {

	@Autowired
	private FinancialStatementService financialStatementService;

	@Autowired
	private FinancialTrendService financialTrendService;

	@Autowired
	private FinancialInformationService financialInformationService;

	@Autowired
	private FinancialInformationValidator financialInformationValidator;

	@Autowired
	private LocalMessageUtils localMessageUtils;

	@Autowired
	private SyncLegacyService syncLegacyService;

	public List<FinancialInformation> setFinancialStatementDocument(List<FinancialInformationListRequest> requests,
	                                                          Long customerId, Long userId, LegacyModelBuilder legacyModelBuilder) {

		List<FinancialInformation> financialInformationList = new ArrayList<>();
		requests.forEach(financialStatement -> {

			financialStatement.setCustomerId(customerId);
			financialInformationValidator.partOfFinancialValidator(financialStatement);

			FinancialInformation financialInformation;
			FinancialInformation findLatestFinancialInformation;
			boolean isLatestFinancialInformation = false;
			if (Utilities.isNull(financialStatement.getStatementId())) {
				financialInformation = new FinancialInformation();
				financialInformation.setCreatedBy(userId);
			} else {
				financialInformation = Optional.ofNullable(financialInformationService.findByIdAndCustomerId(
					financialStatement.getStatementId(), customerId))
					.orElseThrow(DataNotFoundException::new);

				if (!financialStatement.isDelete()) {
					financialInformation.setUpdatedBy(userId);
				} else {
					findLatestFinancialInformation = financialInformationService
						.findFirstByCustomerIdAndStatementFileTypeOrderByStatementFileDateAndUpdatedAt(
							customerId,
							financialStatement.getStatementFileType()
						);
					isLatestFinancialInformation = financialInformation.getId().equals(findLatestFinancialInformation.getId());

					financialInformation.setDeleteBy(userId);
					financialInformation.setDeletedAt(new Date());
				}
			}

			financialInformation.setCustomerId(customerId);
			financialInformation.setStatementFileType(financialStatement.getStatementFileType());
			financialInformation.setStatementFileDate(financialStatement.getStatementFileAt());
			financialInformation.setStatementUrl(financialStatement.getStatementUrl());

			FinancialInformation saveData = Optional.ofNullable(financialInformationService.saveOrUpdate(financialInformation))
				.orElseThrow(() -> new AppException(localMessageUtils.updateFailed()));

			if (Utilities.isNull(saveData.getDeletedAt())) {
				financialInformationList.add(saveData);
			}

			if (Utilities.isNull(financialStatement.getStatementId())) {
				findLatestFinancialInformation = financialInformationService
					.findFirstByCustomerIdAndStatementFileTypeOrderByStatementFileDateAndCreatedAt(
						customerId,
						financialStatement.getStatementFileType()
					);

				if (findLatestFinancialInformation.getId().equals(saveData.getId())) {
					legacyModelBuilder.setFinancialStatementUrl(findLatestFinancialInformation.getStatementUrl());
				}
			} else if (!financialStatement.isDelete()) {
				findLatestFinancialInformation = financialInformationService
					.findFirstByCustomerIdAndStatementFileTypeOrderByStatementFileDateAndUpdatedAt(
						customerId,
						financialStatement.getStatementFileType()
					);

				legacyModelBuilder.setFinancialStatementUrl(findLatestFinancialInformation.getStatementUrl());
			} else {
				boolean isEmptyFinancialByCustomerId = financialInformationService.findByCustomerIdAndStatementFileType(
					customerId, financialStatement.getStatementFileType()).isEmpty();
				if (isLatestFinancialInformation && !isEmptyFinancialByCustomerId) {
					FinancialInformation checkLatestFinancialInformation = financialInformationService
						.findFirstByCustomerIdAndStatementFileTypeOrderByStatementFileDateAndUpdatedAt(
							customerId,
							financialStatement.getStatementFileType()
						);
					financialInformation.setStatementUrl(checkLatestFinancialInformation.getStatementUrl());
				} else if (isLatestFinancialInformation) {
					financialStatement.setStatementUrl("");
				}
				legacyModelBuilder.setFinancialStatementUrl(financialInformation.getStatementUrl());
			}
		});

		return financialInformationList;
	}

	public List<FinancialInformation> setFinancialEStatementDocument(List<FinancialInformationListRequest> requests,
	                                                          Long customerId, Long userId, LegacyModelBuilder legacyModelBuilder) {

		List<FinancialInformation> financialInformationList = new ArrayList<>();
		requests.forEach(financialStatement -> {

			financialStatement.setCustomerId(customerId);
			financialInformationValidator.partOfFinancialValidator(financialStatement);

			FinancialInformation financialInformation;
			FinancialInformation findLatestFinancialInformation;
			boolean isLatestFinancialInformation = false;
			if (Utilities.isNull(financialStatement.getStatementId())) {
				financialInformation = new FinancialInformation();
				financialInformation.setCreatedBy(userId);
			} else {
				financialInformation = Optional.ofNullable(financialInformationService.findByIdAndCustomerId(
					financialStatement.getStatementId(), customerId))
					.orElseThrow(DataNotFoundException::new);

				if (!financialStatement.isDelete()) {
					financialInformation.setUpdatedBy(userId);
				} else {
					findLatestFinancialInformation = financialInformationService
						.findFirstByCustomerIdAndStatementFileTypeOrderByStatementFileDateAndUpdatedAt(
							customerId,
							financialStatement.getStatementFileType()
						);
					isLatestFinancialInformation = financialInformation.getId().equals(findLatestFinancialInformation.getId());

					financialInformation.setDeleteBy(userId);
					financialInformation.setDeletedAt(new Date());
				}
			}

			financialInformation.setCustomerId(customerId);
			financialInformation.setStatementFileType(financialStatement.getStatementFileType());
			financialInformation.setStatementFileDate(financialStatement.getStatementFileAt());
			financialInformation.setStatementUrl(financialStatement.getStatementUrl());

			FinancialInformation saveData = Optional.ofNullable(financialInformationService.saveOrUpdate(financialInformation))
				.orElseThrow(() -> new AppException(localMessageUtils.updateFailed()));

			if (Utilities.isNull(saveData.getDeletedAt())) {
				financialInformationList.add(saveData);
			}

			if (Utilities.isNull(financialStatement.getStatementId())) {
				findLatestFinancialInformation = financialInformationService
					.findFirstByCustomerIdAndStatementFileTypeOrderByStatementFileDateAndCreatedAt(
						customerId,
						financialStatement.getStatementFileType()
					);

				if (findLatestFinancialInformation.getId().equals(saveData.getId())) {
					legacyModelBuilder.setFinancialEStatementUrl(findLatestFinancialInformation.getStatementUrl());
				}
			} else if (!financialStatement.isDelete()) {
				findLatestFinancialInformation = financialInformationService
					.findFirstByCustomerIdAndStatementFileTypeOrderByStatementFileDateAndUpdatedAt(
						customerId,
						financialStatement.getStatementFileType()
					);

				legacyModelBuilder.setFinancialEStatementUrl(findLatestFinancialInformation.getStatementUrl());
			} else {
				boolean isEmptyFinancialByCustomerId = financialInformationService.findByCustomerIdAndStatementFileType(
					customerId, financialStatement.getStatementFileType()).isEmpty();
				if (isLatestFinancialInformation && !isEmptyFinancialByCustomerId) {
					FinancialInformation checkLatestFinancialInformation = financialInformationService
						.findFirstByCustomerIdAndStatementFileTypeOrderByStatementFileDateAndUpdatedAt(
							customerId,
							financialStatement.getStatementFileType()
						);
					financialInformation.setStatementUrl(checkLatestFinancialInformation.getStatementUrl());
				} else if (isLatestFinancialInformation) {
					financialStatement.setStatementUrl("");
				}
				legacyModelBuilder.setFinancialEStatementUrl(financialInformation.getStatementUrl());
			}
		});

		return financialInformationList;
	}

	public List<FinancialStatement> setFinancialStatement(List<FinancialStatementRequest> request, Long customerId, Long userId) {
		List<FinancialStatement> financialStatementList = new ArrayList<>();
		request.forEach(financialStatementRequest -> {

			FinancialStatement financialStatement;

			if (Utilities.isNull(financialStatementRequest.getFinancialStatementId())) {

				if (Optional.ofNullable(financialStatementService.findByCustomerIdAndYearTo(customerId, financialStatementRequest.getYearTo())).isPresent()) {
					throw new AppException("data exist");
				}

				financialStatement = new FinancialStatement();

				financialStatement.setCreatedBy(userId);
				financialStatement.setCreatedAt(new Date());
			} else {
				financialStatement = Optional.ofNullable(financialStatementService.findByIdAndCustomerId(financialStatementRequest.getFinancialStatementId(), customerId))
					.orElseThrow(DataNotFoundException::new);

				financialStatement.setUpdateBy(userId);
			}

			financialStatement.setCustomerId(customerId);
			financialStatement.setYearTo(financialStatementRequest.getYearTo());
			financialStatement.setFiscalYear(financialStatementRequest.getFiscalYear());
			financialStatement.setSales(financialStatementRequest.getSales());
			financialStatement.setCogs(financialStatementRequest.getCogs());
			financialStatement.setGrossProfit(financialStatementRequest.getGrossProfit());
			financialStatement.setSga(financialStatementRequest.getSga());
			financialStatement.setDepreciation(financialStatementRequest.getDepreciation());
			financialStatement.setOperatingProfit(financialStatementRequest.getOperatingProfit());
			financialStatement.setInterestExpense(financialStatementRequest.getInterestExpense());
			financialStatement.setOtherExpense(financialStatementRequest.getOtherExpense());
			financialStatement.setOtherIncome(financialStatementRequest.getOtherIncome());
			financialStatement.setProfitBeforeTax(financialStatementRequest.getProfitBeforeTax());
			financialStatement.setTax(financialStatementRequest.getTax());
			financialStatement.setProfitAfterTax(financialStatementRequest.getProfitAfterTax());
			financialStatement.setExistingFacility(financialStatementRequest.getInstallment());

			FinancialStatement saveData = Optional.ofNullable(financialStatementService.saveOrUpdate(financialStatement))
				.orElseThrow(() -> new AppException(localMessageUtils.updateFailed()));

			financialStatementList.add(saveData);
		});

		return financialStatementList;
	}

	public List<FinancialTrend> setFinancialTrend(List<FinancialTrendRequest> request, Long customerId, Long userId) {
		List<FinancialTrend> financialTrendList = new ArrayList<>();
		request.forEach(financialTrendRequest -> {

			FinancialTrend financialTrend;
			if (Utilities.isNull(financialTrendRequest.getFinancialTrendId())) {
				if (Optional.ofNullable(financialTrendService.findByCustomerIdAndTrendPeriod(customerId, financialTrendRequest.getTrendPeriod())).isPresent()) {
					throw new AppException("data exist");
				}

				financialTrend = new FinancialTrend();
				financialTrend.setCreatedBy(userId);
				financialTrend.setCreatedAt(new Date());
			} else {
				financialTrend = Optional.ofNullable(financialTrendService.findByIdAndCustomerId(financialTrendRequest.getFinancialTrendId(), customerId))
					.orElseThrow(DataNotFoundException::new);

				financialTrend.setUpdateBy(userId);
			}

			financialTrend.setCustomerId(customerId);
			financialTrend.setTrendPeriod(financialTrendRequest.getTrendPeriod());
			financialTrend.setSales(financialTrendRequest.getSales());
			financialTrend.setCogs(financialTrendRequest.getCogs());
			financialTrend.setGrossProfit(financialTrendRequest.getGrossProfit());
			financialTrend.setSga(financialTrendRequest.getSga());
			financialTrend.setOperatingProfit(financialTrendRequest.getOperatingProfit());
			financialTrend.setExistingFacility(financialTrendRequest.getInstallment());
			financialTrend.setProfitBeforeTax(financialTrendRequest.getProfitBeforeTax());
			financialTrend.setProfitAfterTax(financialTrendRequest.getProfitAfterTax());

			FinancialTrend saveData = Optional.ofNullable(financialTrendService.saveOrUpdate(financialTrend))
				.orElseThrow(() -> new AppException(localMessageUtils.updateFailed()));

			financialTrendList.add(saveData);
		});

		return financialTrendList;
	}
}
