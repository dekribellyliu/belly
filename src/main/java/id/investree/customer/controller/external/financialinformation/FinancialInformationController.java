package id.investree.customer.controller.external.financialinformation;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.exception.UnauthorizeException;
import id.investree.core.model.TokenPayload;
import id.investree.core.model.legacy.LegacyModel;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.controller.external.customernotes.CustomerNotesResponseGenerator;
import id.investree.customer.controller.external.salestransaction.SalesTransactionBody;
import id.investree.customer.controller.external.salestransaction.SalesTransactionResponseGenerator;
import id.investree.customer.controller.external.salestransaction.SalesTransactionValidator;
import id.investree.customer.entity.*;
import id.investree.customer.model.data.*;
import id.investree.customer.model.legacy.LegacyModelBuilder;
import id.investree.customer.model.request.*;
import id.investree.customer.model.response.BusinessProfileResponse;
import id.investree.customer.model.response.FinancialInfoInstResponse;
import id.investree.customer.model.response.FinancialInformationResponse;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.service.balancesheet.BalanceSheetService;
import id.investree.customer.service.businessprofile.BusinessProfileService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customernotes.CustomerNotesService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.financialinformation.FinancialInformationService;
import id.investree.customer.service.financialratio.FinancialRatioService;
import id.investree.customer.service.financialstatement.FinancialStatementService;
import id.investree.customer.service.financialtrend.FinancialTrendService;
import id.investree.customer.service.legacy.SyncLegacyService;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.service.salestransaction.SalesTransactionService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/financial-information", produces = MediaType.APPLICATION_JSON_VALUE)
public class FinancialInformationController extends BaseController {

	@Autowired
	private CustomerRoleService customerRoleService;

	@Autowired
	private FinancialInformationService financialInformationService;

	@Autowired
	private CustomerInformationService customerInformationService;

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private FinancialInformationValidator financialInformationValidator;

	@Autowired
	private BusinessProfileService businessProfileService;

	@Autowired
	private SalesTransactionValidator salesTransactionValidator;

	@Autowired
	private SalesTransactionService salesTransactionService;

	@Autowired
	private CustomerNotesService customerNotesService;

	@Autowired
	private LoginDataService loginDataService;

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private FinancialTrendService financialTrendService;

	@Autowired
	private FinancialStatementService financialStatementService;

	@Autowired
	private FinancialRatioService financialRatioService;

	@Autowired
	private BalanceSheetService balanceSheetService;

	@Autowired
	private FinancialInformationBody financialInformationBody;

	@Autowired
	private SalesTransactionBody salesTransactionBody;

	@Autowired
	private FinancialInformationResponseGenerator financialInformationResponseGenerator;

	@Autowired
	private SalesTransactionResponseGenerator salesTransactionResponseGenerator;

	@Autowired
	private CustomerNotesResponseGenerator customerNotesResponseGenerator;

	@Autowired
	private StatusValidation statusValidation;

	@Autowired
	private CustomerUtils customerUtils;

	@Autowired
	private SyncLegacyService syncLegacyService;

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	@PostMapping("{userRoleType}")
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> add(HttpServletRequest http,
	                                          @PathVariable("userRoleType") String userRoleType,
	                                          @RequestBody FinancialInformationRequest request) {

		financialInformationValidator.financialValidator(request);
		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
			.orElseThrow(() -> new AppException(messageUtils.emptyToken()));

		String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);

		Integer userTypeIdentification = customerUtils.getUserTypeIdentification(userRoleType);
		statusValidation.statusRestrictionValidation(tokenPayload, userTypeIdentification);

		Long custId;
		if (GlobalConstants.USER_TYPE_FRONTOFFICE_ID.equals(tokenPayload.getUserType())) {
			custId = Long.valueOf(tokenPayload.getCustomerId());
		} else {
			custId = Optional.ofNullable(customerInformationService.findById(request.getCustomerId()))
				.map(CustomerInformation::getId)
				.orElseThrow(DataNotFoundException::new);
		}

		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(custId))
			.orElseThrow(() -> new DataNotFoundException("User Data"));

		String emailAddress = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
			.map(LoginDataResponse::getEmailAddress)
			.orElseThrow(() -> new DataNotFoundException("Login Data"));

		Integer StatementFileType = request.getStatementFileType();

		FinancialInformation financialInformation = new FinancialInformation();
		financialInformation.setCustomerId(custId);
		financialInformation.setStatementFileType(StatementFileType);
		financialInformation.setStatementUrl(request.getStatementUrl());
		financialInformation.setStatementFileDate(request.getStatementFileDate());
		financialInformation.setCreatedBy(tokenPayload.getUserId());
		FinancialInformation saveData = Optional.ofNullable(financialInformationService.saveOrUpdate(financialInformation))
			.orElseThrow(() -> new AppException(messageUtils.insertFailed("Financial Information")));

		FinancialInformation findLatestFinancialInformation = financialInformationService
			.findFirstByCustomerIdAndStatementFileTypeOrderByStatementFileDateAndCreatedAt(
				custId,
				StatementFileType
			);

		LegacyModelBuilder builder = new LegacyModelBuilder();
		if (findLatestFinancialInformation.getId().equals(saveData.getId())) {
			if (StatementFileType.equals(Constants.FINANCIAL_STATEMENT)) {
				builder.setFinancialStatementUrl(findLatestFinancialInformation.getStatementUrl());
			} else if (StatementFileType.equals(Constants.FINANCIAL__E_STATEMENT)) {
				 builder.setFinancialEStatementUrl(findLatestFinancialInformation.getStatementUrl());
			}
			LegacyModel legacyModel = builder
				.setEmail(emailAddress)
				.setGlobalParam(completingDataGlobalParam)
				.build(custId);
			syncLegacyService.sync(accessToken, legacyModel);
		}

		return abstractResponseHandler(saveData).getResult(messageUtils.insertSuccess());
	}

	@DeleteMapping("{userRoleType}/{financialId}")
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> delete(@PathVariable("userRoleType") String userRoleType,
	                                             @PathVariable("financialId") Long financialId, HttpServletRequest http) {

		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
			.orElseThrow(() -> new AppException(messageUtils.emptyToken()));

		Integer userTypeIdentification = customerUtils.getUserTypeIdentification(userRoleType);
		statusValidation.statusRestrictionValidation(tokenPayload, userTypeIdentification);

		FinancialInformation financialData = Optional.ofNullable(financialInformationService
			.findByIdAndCustomerId(financialId, Long.valueOf(tokenPayload.getCustomerId())))
			.orElseThrow(DataNotFoundException::new);

		Integer statementFileType = financialData.getStatementFileType();

		FinancialInformation findLatestFinancialInformation = financialInformationService
			.findFirstByCustomerIdAndStatementFileTypeOrderByStatementFileDateAndUpdatedAt(
				Long.valueOf(tokenPayload.getCustomerId()),
				statementFileType
			);

		financialData.setDeleteBy(tokenPayload.getUserId());
		financialData.setDeletedAt(new Date());

		boolean isFindLatestFinancialInformation = financialId.equals(findLatestFinancialInformation.getId());

		FinancialInformation isDeleted = financialInformationService.saveOrUpdate(financialData);

		if (Utilities.isNull(isDeleted)) {
			throw new AppException(messageUtils.deleteFailed());
		}

		boolean isFinancialInfoNotEmpty = !financialInformationService.findByCustomerIdAndStatementFileType(
			Long.valueOf(tokenPayload.getCustomerId()), statementFileType).isEmpty();

		if (isFindLatestFinancialInformation) {
			if (isFinancialInfoNotEmpty) {
				FinancialInformation checkLatestFinancialInformation = financialInformationService
					.findFirstByCustomerIdAndStatementFileTypeOrderByStatementFileDateAndUpdatedAt(
						Long.valueOf(tokenPayload.getCustomerId()),
						statementFileType
					);
				financialData.setStatementUrl(checkLatestFinancialInformation.getStatementUrl());
			} else {
				financialData.setStatementUrl("");
			}

			LegacyModelBuilder builder = new LegacyModelBuilder();
			if (statementFileType.equals(Constants.FINANCIAL_STATEMENT)) {
				builder.setFinancialStatementUrl(financialData.getStatementUrl());
			} else if (statementFileType.equals(Constants.FINANCIAL__E_STATEMENT)) {
				builder.setFinancialEStatementUrl(financialData.getStatementUrl());
			}
			LegacyModel legacyModel = builder
				.setEmail(tokenPayload.getUserEmail())
				.setGlobalParam(completingDataGlobalParam)
				.build(Long.valueOf(tokenPayload.getCustomerId()));
			syncLegacyService.sync(http.getHeader(GlobalConstants.HEADER_TOKEN), legacyModel);
		}

		return abstractResponseHandler(isDeleted).getResult(messageUtils.deleteSuccess());
	}

	@PutMapping("/{financialId}")
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> update(@PathVariable("financialId") Long financialId,
	                                             @RequestBody FinancialInformationRequest request,
	                                             HttpServletRequest http) {

		financialInformationValidator.financialValidator(request);
		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
			.orElseThrow(() -> new AppException(messageUtils.emptyToken()));

		Long customerId = Long.valueOf(tokenPayload.getCustomerId());

		String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);

		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(customerId))
			.orElseThrow(() -> new DataNotFoundException("User Data"));

		String emailAddress = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
			.map(LoginDataResponse::getEmailAddress)
			.orElseThrow(() -> new DataNotFoundException("Login Data"));

		statusValidation.statusRestrictionValidation(tokenPayload, GlobalConstants.USER_CATEGORY_BORROWER_ID);

		FinancialInformation financialInformation = Optional.ofNullable(financialInformationService.findByIdAndCustomerId(financialId, customerId))
			.orElseThrow(DataNotFoundException::new);

		financialInformation.setStatementFileType(request.getStatementFileType());
		financialInformation.setStatementUrl(request.getStatementUrl());
		financialInformation.setStatementFileDate(request.getStatementFileDate());
		financialInformation.setUpdatedAt(new Date());
		financialInformation.setUpdatedBy(tokenPayload.getUserId());

		FinancialInformation updateData = Optional.ofNullable(financialInformationService.saveOrUpdate(financialInformation))
			.orElseThrow(() -> new AppException(messageUtils.updateFailed()));

		FinancialInformation findLatestFinancialInformation = financialInformationService
			.findFirstByCustomerIdAndStatementFileTypeOrderByStatementFileDateAndUpdatedAt(
				customerId,
				request.getStatementFileType()
			);

		LegacyModelBuilder builder = new LegacyModelBuilder();
		if (request.getStatementFileType().equals(Constants.FINANCIAL_STATEMENT)) {
			builder.setFinancialStatementUrl(findLatestFinancialInformation.getStatementUrl());
		} else if (request.getStatementFileType().equals(Constants.FINANCIAL__E_STATEMENT)) {
			builder.setFinancialEStatementUrl(findLatestFinancialInformation.getStatementUrl());
		}

		LegacyModel legacyModel = builder
			.setEmail(emailAddress)
			.setGlobalParam(completingDataGlobalParam)
			.build(customerId);
		syncLegacyService.sync(accessToken, legacyModel);

		return abstractResponseHandler(updateData).getResult(messageUtils.updateSuccess());
	}

	@PutMapping("/save-all/{customerId}")
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> createBorrowerDetail(HttpServletRequest http,
	                                                           @RequestBody FinancialInformationDetailRequest financialInformationDetailRequest,
	                                                           @PathVariable("customerId") Long customerId) {

		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
			.orElseThrow(() -> new AppException(messageUtils.emptyToken()));

		if (!GlobalConstants.USER_TYPE_BACKOFFICE_ID.equals(tokenPayload.getUserType())) {
			throw new UnauthorizeException();
		}

		String accessToken = http.getHeader("X-Investree-Token");

		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(customerId))
			.orElseThrow(() -> new DataNotFoundException("User Data"));

		String emailAddress = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
			.map(LoginDataResponse::getEmailAddress)
			.orElseThrow(() -> new DataNotFoundException("Login Data"));

		if (!financialInformationDetailRequest.getFinancialStatement().isEmpty()) {
			if (financialInformationDetailRequest.getFinancialStatement().stream().filter(v -> Utilities.isNull(v.getFinancialStatementId())).count() > 3) {
				throw new AppException("exceed financial statement. you cant add more financial statement");
			}

			for (FinancialStatementRequest request : financialInformationDetailRequest.getFinancialStatement()) {
				if (Utilities.isNull(request.getFinancialStatementId()) && financialStatementService.findByCustomerId(customerId).size() > 3) {
					throw new AppException("exceed financial statement. you cant add more financial statement");
				}
			}
		}

		if (!financialInformationDetailRequest.getFinancialTrend().isEmpty()) {
			if (financialInformationDetailRequest.getFinancialTrend().stream().filter(v -> Utilities.isNull(v.getFinancialTrendId())).count() > 2) {
				throw new AppException("exceed financial trends. you cant add more financial trend");
			}
			for (FinancialTrendRequest financialTrendRequest : financialInformationDetailRequest.getFinancialTrend()) {
				if (Utilities.isNull(financialTrendRequest.getFinancialTrendId()) && (financialTrendService.findByCustomerId(customerId).size() > 2)) {
					throw new AppException("exceed financial trends. you cant add more financial trend");
				}
			}
		}

		if (!financialInformationDetailRequest.getSalesTransactionData().isEmpty()) {
			if (financialInformationDetailRequest.getSalesTransactionData().stream().filter(v -> Utilities.isNull(v.getSalesTransactionId())).count() > 12) {
				throw new AppException("Sales transaction maximum add until 12 months");
			}
			for (SalesTransactionRequest request : financialInformationDetailRequest.getSalesTransactionData()) {
				if (Utilities.isNull(request.getSalesTransactionId()) && salesTransactionService.countCustomerId(customerId) > 12) {
					throw new AppException("Sales transaction maximum add until 12 months");
				}
			}
		}

		LegacyModelBuilder legacyModelBuilder = new LegacyModelBuilder();
		BusinessProfileResponse businessProfileResponse = new BusinessProfileResponse();
		BusinessProfileRequest businessProfileRequest = financialInformationDetailRequest.getFinancialInformation();

		BusinessProfile businessProfile = Optional.ofNullable(businessProfileService.findByCustomerId(customerId))
			.map(v -> {
				v.setUpdatedBy(tokenPayload.getUserId());
				return v;
			})
			.orElseGet(() -> {
				BusinessProfile newBusinessProfile = new BusinessProfile();
				newBusinessProfile.setCreatedBy(tokenPayload.getUserId());
				return newBusinessProfile;
			});

		businessProfile.setCustomerId(customerId);
		businessProfile.setProfitMargin(businessProfileRequest.getProfitMargin());
		businessProfile.setAverageMonthlySales(businessProfileRequest.getAverageMonthlySales());
		businessProfile.setOtherIncome(businessProfileRequest.getOtherIncome());
		businessProfile.setGeneralProfitMargin(businessProfileRequest.getGeneralProfitMargin());
		businessProfile.setProfitMarginFromPartner(businessProfileRequest.getProfitMarginFromPartner());
		businessProfile.setTotalNettMargin(businessProfileRequest.getProfitMarginFromPartner());
		businessProfile.setLivingCost(businessProfileRequest.getLivingCost());
		businessProfile.setUpdatedBy(tokenPayload.getUserId());
		businessProfile.setUpdatedAt(new Date());
		BusinessProfile businessProfileSaveData = Optional.ofNullable(businessProfileService.saveOrUpdate(businessProfile))
			.orElseThrow(() -> new AppException(messageUtils.insertFailed("Business Profile")));

		businessProfileResponse.setAverageMonthlySales(businessProfileSaveData.getAverageMonthlySales());
		businessProfileResponse.setProfitMargin(businessProfileSaveData.getProfitMargin());
		businessProfileResponse.setOtherIncome(businessProfileSaveData.getOtherIncome());
		businessProfileResponse.setGeneralProfitMargin(businessProfileSaveData.getGeneralProfitMargin());
		businessProfileResponse.setProfitMarginFromPartner(businessProfileSaveData.getProfitMarginFromPartner());
		businessProfileResponse.setTotalNettMargin(businessProfileSaveData.getTotalNettMargin());
		businessProfileResponse.setLivingCost(businessProfileSaveData.getLivingCost());
		businessProfileResponse.setUpdatedAt(new Date());
		businessProfileResponse.setUpdatedBy(tokenPayload.getUserId());

		List<FinancialInformation> eStatement;
		List<FinancialDataDetail> estatementDataDetail = new ArrayList<>();
		if (!financialInformationDetailRequest.getEStatementDocuments().isEmpty()) {
			eStatement = financialInformationBody.setFinancialEStatementDocument(
				financialInformationDetailRequest.getEStatementDocuments(),
				customerId,
				tokenPayload.getUserId(),
				legacyModelBuilder);
			estatementDataDetail = financialInformationResponseGenerator.generateFinancialDataDetails(eStatement);
		}

		List<FinancialInformation> financialStatement;
		List<FinancialDataDetail> financialDataDetail = new ArrayList<>();
		if (!financialInformationDetailRequest.getFinancialStatementDocuments().isEmpty()) {
			financialStatement = financialInformationBody.setFinancialStatementDocument(
				financialInformationDetailRequest.getFinancialStatementDocuments(),
				customerId,
				tokenPayload.getUserId(),
				legacyModelBuilder);
			financialDataDetail = financialInformationResponseGenerator.generateFinancialDataDetails(financialStatement);
		}

		List<FinancialStatement> financialStatementResponses;
		List<FinancialStatementData> financialStatementDataList = new ArrayList<>();
		if (!financialInformationDetailRequest.getFinancialStatement().isEmpty()) {
			financialStatementResponses = financialInformationBody.setFinancialStatement(
				financialInformationDetailRequest.getFinancialStatement(),
				customerId,
				tokenPayload.getUserId());
			financialStatementDataList = financialInformationResponseGenerator.generateFinancialStatement(financialStatementResponses);
		}

		List<FinancialTrend> financialTrendResponses;
		List<FinancialTrendData> financialTrendDataList = new ArrayList<>();
		if (!financialInformationDetailRequest.getFinancialTrend().isEmpty()) {
			financialTrendResponses = financialInformationBody.setFinancialTrend(
				financialInformationDetailRequest.getFinancialTrend(),
				customerId,
				tokenPayload.getUserId());
			financialTrendDataList = financialInformationResponseGenerator.generateFinancialTrend(financialTrendResponses);
		}

		List<SalesTransaction> salesTransactionResponses;
		List<SalesTransactionData> salesTransactionDataList = new ArrayList<>();
		if (!financialInformationDetailRequest.getSalesTransactionData().isEmpty()) {
			salesTransactionResponses = salesTransactionBody.setSalesTransaction(
				financialInformationDetailRequest.getSalesTransactionData(),
				customerId,
				tokenPayload.getUserId());
			salesTransactionDataList = salesTransactionResponseGenerator.generateSalesTransactionDataDetails(salesTransactionResponses);
		}

		List<CustomerNotesData> customerNotesDataList = new ArrayList<>();
		if (!financialInformationDetailRequest.getCustomerNotesData().isEmpty()) {
			List<CustomerNotesRequest> customerNotesData = financialInformationDetailRequest.getCustomerNotesData();
			List<CustomerNotes> customerNotesResponses = new ArrayList<>();
			customerNotesData.forEach(customerNotesRequest -> {

				CustomerNotes customerNotes = Optional.ofNullable(customerNotesService.findByCustomerIdAndCreatedBy(customerId, tokenPayload.getUserId()))
					.map(v -> {
						v.setUpdateBy(tokenPayload.getUserId());
						return v;
					}).orElseGet(() -> {
						CustomerNotes notes = new CustomerNotes();
						notes.setCreatedBy(tokenPayload.getUserId());
						notes.setCreatedAt(new Date());
						return notes;
					});

				customerNotes.setCustomerId(customerId);
				customerNotes.setCustomerType(customerNotesRequest.getCustomerType());
				String remarks = Optional.ofNullable(customerNotesRequest.getRemarks())
					.orElseThrow(() -> new AppException(messageUtils.errorBlank("remarks")));
				customerNotes.setNote(remarks);

				CustomerNotes customerNotesSaveData = Optional.ofNullable(customerNotesService.saveOrUpdate(customerNotes))
					.orElseThrow(() -> new AppException(messageUtils.insertFailed("Customer Notes")));

				customerNotesResponses.add(customerNotesSaveData);
			});
			customerNotesDataList = customerNotesResponseGenerator.generateCustomerNotesDataDetails(customerNotesResponses, accessToken);
		}

		FinancialInformationResponse financialInformationResponse = new FinancialInformationResponse();
		financialInformationResponse.setEStatementDocuments(estatementDataDetail);
		financialInformationResponse.setFinancialStatementDocuments(financialDataDetail);
		financialInformationResponse.setSalesTransactionData(salesTransactionDataList);
		financialInformationResponse.setCustomerNotesData(customerNotesDataList);
		financialInformationResponse.setFinancialStatement(financialStatementDataList);
		financialInformationResponse.setFinancialTrend(financialTrendDataList);
		financialInformationResponse.setFinancialInformation(businessProfileResponse);

		List<FinancialStatement> allFinancialStatementData = financialStatementService.findByCustomerId(customerId);
		List<FinancialTrend> allFinancialTrendData = financialTrendService.findByCustomerId(customerId);
		List<FinancialRatio> allFinancialRatioData = financialRatioService.findByCustomerId(customerId);
		List<SalesTransaction> allSalesTransactionData = salesTransactionService.findByCustomerId(customerId);

		LegacyModel legacyModel = legacyModelBuilder
			.setEmail(emailAddress)
			.setGlobalParam(completingDataGlobalParam)
			.setBusinessProfile(businessProfile)
			.setFinancialStatements(allFinancialStatementData)
			.setFinancialTrends(allFinancialTrendData)
			.setFinancialRatios(allFinancialRatioData)
			.setSalesTransaction(allSalesTransactionData)
			.build(customerId);
		syncLegacyService.sync(accessToken, legacyModel);

		return abstractResponseHandler(financialInformationResponse).getResult(messageUtils.insertSuccess(), HttpStatus.OK);
	}

	@PutMapping("/institutional/save")
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> saveInstitutional(HttpServletRequest http, @RequestBody FinancialInformationInstRequest request) {

		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
			.orElseThrow(() -> new AppException(messageUtils.emptyToken()));

		String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);

		if (!GlobalConstants.USER_TYPE_BACKOFFICE_ID.equals(tokenPayload.getUserType())) {
			throw new UnauthorizeException();
		}

		CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.findById(request.getCustomerId()))
			.orElseThrow(DataNotFoundException::new);
		Long customerId = customerInformation.getId();

		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(customerId))
			.orElseThrow(() -> new DataNotFoundException("User Data"));

		String emailAddress = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
			.map(LoginDataResponse::getEmailAddress)
			.orElseThrow(() -> new DataNotFoundException("Login Data"));

		if (!GlobalConstants.CATEGORY_INSTITUSI_ID.equals(Integer.valueOf(customerInformation.getUserCategory()))) {
			throw new UnauthorizeException();
		}

		if (!request.getFinancialStatementDetail().isEmpty()) {
			if (request.getFinancialStatementDetail().stream().filter(v -> Utilities.isNull(v.getFinancialStatementId())).count() > 3) {
				throw new AppException("exceed financial statement. you cant add more financial statement");
			}

			for (FinancialStatementRequest financialStatementRequest : request.getFinancialStatementDetail()) {

				if (Utilities.isNull(financialStatementRequest.getFinancialStatementId()) && (financialStatementService.findByCustomerId(request.getCustomerId()).size() > 3)) {
					throw new AppException("exceed financial statement. you cant add more financial statement");
				}
			}
		}

		if (!request.getFinancialTrend().isEmpty()) {
			if (request.getFinancialTrend().stream().filter(v -> Utilities.isNull(v.getFinancialTrendId())).count() > 2) {
				throw new AppException("exceed financial trends. you cant add more financial trend");
			}
			for (FinancialTrendRequest financialTrendRequest : request.getFinancialTrend()) {

				if (Utilities.isNull(financialTrendRequest.getFinancialTrendId()) && (financialTrendService.findByCustomerId(request.getCustomerId()).size() > 2)) {
					throw new AppException("exceed financial trends. you cant add more financial trend");
				}
			}
		}

		if (!request.getBalanceSheet().isEmpty()) {
			if (request.getBalanceSheet().stream().filter(v -> Utilities.isNull(v.getBalanceSheetId())).count() > 3) {
				throw new AppException("exceed balance sheet. you cant add more balance sheet");
			}

			for (BalanceSheetRequest balanceSheetRequest : request.getBalanceSheet()) {
				if (Utilities.isNull(balanceSheetRequest.getBalanceSheetId()) && (balanceSheetService.findByCustomerId(request.getCustomerId()).size() > 3)) {
					throw new AppException("exceed balance sheet. you cant add more balance sheet");
				}
			}
		}

		if (!request.getFinancialRatio().isEmpty()) {
			if (request.getFinancialRatio().stream().filter(v -> Utilities.isNull(v.getFinancialRatioId())).count() > 3) {
				throw new AppException("exceed financial ratio. you cant add more financial ratio");
			}

			for (FinancialRatioRequest financialRatioRequest : request.getFinancialRatio()) {

				if (Utilities.isNull(financialRatioRequest.getFinancialRatioId()) && (financialRatioService.findByCustomerId(request.getCustomerId()).size() > 3)) {
					throw new AppException("exceed financial ratio. you cant add more financial ratio");
				}
			}
		}

		if (!request.getSalesTransaction().isEmpty()) {
			if (request.getSalesTransaction().stream().filter(v -> Utilities.isNull(v.getSalesTransactionId())).count() > 12) {
				throw new AppException("Sales transaction maximum add until 12 months");
			}

			for (SalesTransactionRequest salesTransactionRequest : request.getSalesTransaction()) {
				if (Utilities.isNull(salesTransactionRequest.getSalesTransactionId()) && salesTransactionService.countCustomerId(request.getCustomerId()) > 12) {
					throw new AppException("Sales transaction maximum add until 12 months");
				}
			}
		}

		LegacyModelBuilder legacyModelBuilder = new LegacyModelBuilder();

		BusinessProfile businessProfile = businessProfileService.findByCustomerId(request.getCustomerId());
		if (Utilities.isNull(businessProfile)) {
			businessProfile = new BusinessProfile();

			businessProfile.setCustomerId(request.getCustomerId());
			businessProfile.setCreatedBy(tokenPayload.getUserId());
			businessProfile.setCreatedAt(new Date());
		} else {
			businessProfile.setUpdatedBy(tokenPayload.getUserId());
		}

		businessProfile.setCustomerId(request.getCustomerId());
		businessProfile.setAverageMonthlySales(request.getAverageMonthlySales());
		businessProfile.setProfitMargin(request.getProfitMargin());


		if (!Optional.ofNullable(businessProfileService.saveOrUpdate(businessProfile)).isPresent()) {
			throw new AppException(messageUtils.updateFailed());
		}

		List<FinancialInformation> financialInformationList = financialInformationBody.setFinancialStatementDocument(
			request.getFinancialStatement(), request.getCustomerId(), tokenPayload.getUserId(), legacyModelBuilder);
		List<Long> financialInformationIdList = financialInformationList.stream().map(FinancialInformation::getId).distinct().collect(Collectors.toList());

		List<FinancialInformation> bankStatementList = financialInformationBody.setFinancialEStatementDocument(
			request.getBankStatement(), request.getCustomerId(), tokenPayload.getUserId(), legacyModelBuilder);

		List<Long> bankStatementIdList = bankStatementList.stream().map(FinancialInformation::getId).distinct().collect(Collectors.toList());

		List<FinancialStatement> financialStatementList = financialInformationBody.setFinancialStatement(request.getFinancialStatementDetail(), request.getCustomerId(), tokenPayload.getUserId());
		List<Long> financialStatementIdList = financialStatementList.stream().map(FinancialStatement::getId).distinct().collect(Collectors.toList());

		List<FinancialTrend> financialTrendList = financialInformationBody.setFinancialTrend(request.getFinancialTrend(), request.getCustomerId(), tokenPayload.getUserId());
		List<Long> financialTrendIdList = financialTrendList.stream().map(FinancialTrend::getId).distinct().collect(Collectors.toList());

		List<BalanceSheet> balanceSheetList = new ArrayList<>();
		request.getBalanceSheet().forEach(balanceSheetRequest -> {
			BalanceSheet balanceSheet;
			if (Utilities.isNull(balanceSheetRequest.getBalanceSheetId())) {
				balanceSheet = new BalanceSheet();

				balanceSheet.setCreatedBy(tokenPayload.getUserId());
				balanceSheet.setCreatedAt(new Date());
			} else {
				balanceSheet = Optional.ofNullable(balanceSheetService.findByIdAndCustomerId(balanceSheetRequest.getBalanceSheetId(), request.getCustomerId()))
					.orElseThrow(DataNotFoundException::new);

				balanceSheet.setUpdateBy(tokenPayload.getUserId());
			}

			balanceSheet.setCustomerId(request.getCustomerId());
			balanceSheet.setYearTo(balanceSheetRequest.getYearTo());
			balanceSheet.setAccReceive(balanceSheetRequest.getAccReceive());
			balanceSheet.setInvestory(balanceSheetRequest.getInvestory());
			balanceSheet.setAccPayable(balanceSheetRequest.getAccPayable());
			balanceSheet.setBankDebt(balanceSheetRequest.getBankDebt());
			balanceSheet.setCurrentAssets(balanceSheetRequest.getCurrentAssets());
			balanceSheet.setCurrentLiabilities(balanceSheetRequest.getCurrentLiabilities());
			balanceSheet.setTotalLiabilities(balanceSheetRequest.getTotalLiabilities());
			balanceSheet.setEquity(balanceSheetRequest.getEquity());

			BalanceSheet saveData = Optional.ofNullable(balanceSheetService.saveOrUpdate(balanceSheet))
				.orElseThrow(() -> new AppException(messageUtils.updateFailed()));

			balanceSheetList.add(saveData);
		});
		List<Long> balanceSheetIdList = balanceSheetList.stream().map(BalanceSheet::getId).distinct().collect(Collectors.toList());

		List<FinancialRatio> financialRatioList = new ArrayList<>();
		request.getFinancialRatio().forEach(financialRatioRequest -> {
			FinancialRatio financialRatio;
			if (Utilities.isNull(financialRatioRequest.getFinancialRatioId())) {
				financialRatio = new FinancialRatio();

				financialRatio.setCreatedBy(tokenPayload.getUserId());
				financialRatio.setCreatedAt(new Date());
			} else {
				financialRatio = Optional.ofNullable(financialRatioService.findByIdAndCustomerId(financialRatioRequest.getFinancialRatioId(), request.getCustomerId()))
					.orElseThrow(DataNotFoundException::new);

				financialRatio.setUpdateBy(tokenPayload.getUserId());
			}

			financialRatio.setCustomerId(request.getCustomerId());
			financialRatio.setYearTo(financialRatioRequest.getYearTo());
			financialRatio.setGpm(financialRatioRequest.getGpm());
			financialRatio.setNpm(financialRatioRequest.getNpm());
			financialRatio.setArdoh(financialRatioRequest.getArdoh());
			financialRatio.setInvdoh(financialRatioRequest.getInvdoh());
			financialRatio.setApdoh(financialRatioRequest.getApdoh());
			financialRatio.setCashCycle(financialRatioRequest.getCashCycle());
			financialRatio.setCashRatio(financialRatioRequest.getCashRatio());
			financialRatio.setEbitda(financialRatioRequest.getEbitda());
			financialRatio.setLeverage(financialRatioRequest.getLeverage());
			financialRatio.setWiNeeds(financialRatioRequest.getWiNeeds());
			financialRatio.setTie(financialRatioRequest.getTie());
			financialRatio.setDscr(financialRatioRequest.getDscr());

			FinancialRatio saveData = Optional.ofNullable(financialRatioService.saveOrUpdate(financialRatio))
				.orElseThrow(() -> new AppException(messageUtils.updateFailed()));

			financialRatioList.add(saveData);
		});
		List<Long> financialRatioIdList = financialRatioList.stream().map(FinancialRatio::getId).distinct().collect(Collectors.toList());

		List<SalesTransaction> salesTransactionResponses = salesTransactionBody.setSalesTransaction(request.getSalesTransaction(), request.getCustomerId(), tokenPayload.getUserId());
		List<Long> salesTransactionIdList = salesTransactionResponses.stream().map(SalesTransaction::getId).distinct().collect(Collectors.toList());

		CustomerNotes customerNotesSave = new CustomerNotes();
		if (!Utilities.isNull(request.getCustomerNotes()) && !Utilities.isEmptyOrBlank(request.getCustomerNotes().getRemarks())) {
			CustomerNotes customerNotes = Optional.ofNullable(customerNotesService.findByCustomerIdAndCreatedBy(request.getCustomerId(), tokenPayload.getUserId()))
				.map(v -> {
					v.setUpdateBy(tokenPayload.getUserId());
					return v;
				}).orElseGet(() -> {
					CustomerNotes notes = new CustomerNotes();
					notes.setCreatedBy(tokenPayload.getUserId());
					notes.setCreatedAt(new Date());

					return notes;
				});

			customerNotes.setCustomerId(request.getCustomerId());
			customerNotes.setCustomerType(request.getCustomerNotes().getCustomerType());
			customerNotes.setNote(Utilities.isEmptyOrBlank(request.getCustomerNotes().getRemarks()) ? customerNotes.getNote() : request.getCustomerNotes().getRemarks());

			customerNotesSave = Optional.ofNullable(customerNotesService.saveOrUpdate(customerNotes))
				.orElseThrow(() -> new AppException(messageUtils.updateFailed()));
		}

		FinancialInfoInstResponse financialInfoInstResponse = new FinancialInfoInstResponse();
		financialInfoInstResponse.setFinancialStatements(financialInformationIdList);
		financialInfoInstResponse.setEStatements(bankStatementIdList);
		financialInfoInstResponse.setFinancialStatement(financialStatementIdList);
		financialInfoInstResponse.setFinancialTrend(financialTrendIdList);
		financialInfoInstResponse.setBalanceSheet(balanceSheetIdList);
		financialInfoInstResponse.setFinancialRatio(financialRatioIdList);
		financialInfoInstResponse.setSalesTransactionData(salesTransactionIdList);
		financialInfoInstResponse.setCustomerNotes(Utilities.isNull(customerNotesSave.getId()) ? null : customerNotesResponseGenerator.generateCustomerNotesDataDetail(customerNotesSave, accessToken));

		List<FinancialStatement> allFinancialStatementData = financialStatementService.findByCustomerId(customerId);
		List<FinancialTrend> allFinancialTrendData = financialTrendService.findByCustomerId(customerId);
		List<FinancialRatio> allFinancialRatioData = financialRatioService.findByCustomerId(customerId);
		List<BalanceSheet> allBalanceSheetData = balanceSheetService.findByCustomerId(customerId);
		List<SalesTransaction> allSalesTransactionData = salesTransactionService.findByCustomerId(customerId);

		LegacyModel legacyModel = legacyModelBuilder
			.setEmail(emailAddress)
			.setGlobalParam(completingDataGlobalParam)
			.setBusinessProfile(businessProfile)
			.setFinancialStatements(allFinancialStatementData)
			.setFinancialTrends(allFinancialTrendData)
			.setFinancialRatios(allFinancialRatioData)
			.setBalanceSheets(allBalanceSheetData)
			.setSalesTransaction(allSalesTransactionData)
			.build(customerInformation.getId());
		syncLegacyService.sync(accessToken, legacyModel);

		return abstractResponseHandler(financialInfoInstResponse).getResult(messageUtils.updateSuccess());
	}
}
