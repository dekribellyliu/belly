package id.investree.customer.controller.external.financialinformation;

import id.investree.core.utils.Utilities;
import id.investree.customer.entity.*;
import id.investree.customer.model.data.*;
import id.investree.customer.utils.Constants;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class FinancialInformationResponseGenerator {

	public List<FinancialDataDetail> generateFinancialData(List<FinancialInformation> data, Integer type) {
		return data.stream()
				.filter(it -> (type.equals(it.getStatementFileType()) || Constants.FINANCIAL_ALL_STATEMENT.equals(type)) && Utilities.isNull(it.getDeleteBy()))
				.map(this::convertToFinancialDetail).collect(Collectors.toList());
	}

	public FinancialDataDetail convertToFinancialDetail(FinancialInformation financialInformation) {
		FinancialDataDetail detail = new FinancialDataDetail();
		detail.setStatementId(financialInformation.getId());
		detail.setStatementFileAt(financialInformation.getStatementFileDate());
		detail.setStatementFileType(financialInformation.getStatementFileType());
		detail.setStatementUrl(financialInformation.getStatementUrl());
		detail.setDelete(false);
		return detail;
	}

	public List<FinancialDataDetail> generateFinancialDataDetails(List<FinancialInformation> financialInformations) {
		List<FinancialDataDetail> financialDataDetail = new ArrayList<>();
		for (FinancialInformation financialInformation : financialInformations) {
			financialDataDetail.add(convertToFinancialDetail(financialInformation));
		}
		return financialDataDetail;
	}

	public List<FinancialStatementData> generateFinancialStatement(List<FinancialStatement> financialStatements) {
		List<FinancialStatementData> financialStatementDataList = new ArrayList<>();
		financialStatements.forEach(financialStatement -> {
			FinancialStatementData financialStatementData = new FinancialStatementData();

			financialStatementData.setFinancialStatementId(financialStatement.getId());
			financialStatementData.setYearTo(financialStatement.getYearTo());
			financialStatementData.setFiscalYear(financialStatement.getFiscalYear());
			financialStatementData.setSales(financialStatement.getSales());
			financialStatementData.setCogs(financialStatement.getCogs());
			financialStatementData.setGrossProfit(financialStatement.getGrossProfit());
			financialStatementData.setSga(financialStatement.getSga());
			financialStatementData.setDepreciation(financialStatement.getDepreciation());
			financialStatementData.setInstallment(financialStatement.getExistingFacility());
			financialStatementData.setOperatingProfit(financialStatement.getOperatingProfit());
			financialStatementData.setInterestExpense(financialStatement.getInterestExpense());
			financialStatementData.setOtherIncome(financialStatement.getOtherIncome());
			financialStatementData.setOtherExpense(financialStatement.getOtherExpense());
			financialStatementData.setProfitBeforeTax(financialStatement.getProfitBeforeTax());
			financialStatementData.setTax(financialStatement.getTax());
			financialStatementData.setProfitAfterTax(financialStatement.getProfitAfterTax());

			financialStatementDataList.add(financialStatementData);
		});

		return financialStatementDataList;
	}

	public List<FinancialTrendData> generateFinancialTrend(List<FinancialTrend> financialTrends) {
		List<FinancialTrendData> financialTrendDataList = new ArrayList<>();
		financialTrends.forEach(financialTrend -> {
			FinancialTrendData financialTrendData = new FinancialTrendData();

			financialTrendData.setFinancialTrendId(financialTrend.getId());
			financialTrendData.setTrendPeriod(financialTrend.getTrendPeriod());
			financialTrendData.setSales(financialTrend.getSales());
			financialTrendData.setCogs(financialTrend.getCogs());
			financialTrendData.setGrossProfit(financialTrend.getGrossProfit());
			financialTrendData.setSga(financialTrend.getSga());
			financialTrendData.setOperatingProfit(financialTrend.getOperatingProfit());
			financialTrendData.setInstallment(financialTrend.getExistingFacility());

			financialTrendDataList.add(financialTrendData);
		});

		return financialTrendDataList;
	}

	public List<BalanceSheetData> generateBalanceSheet(List<BalanceSheet> balanceSheets) {
		List<BalanceSheetData> balanceSheetDataList = new ArrayList<>();
		balanceSheets.forEach(balanceSheet -> {
			BalanceSheetData balanceSheetData = new BalanceSheetData();

			balanceSheetData.setBalanceSheetId(balanceSheet.getId());
			balanceSheetData.setYearTo(balanceSheet.getYearTo());
			balanceSheetData.setAccReceive(balanceSheet.getAccReceive());
			balanceSheetData.setInvestory(balanceSheet.getInvestory());
			balanceSheetData.setAccPayable(balanceSheet.getAccPayable());
			balanceSheetData.setBankDebt(balanceSheet.getBankDebt());
			balanceSheetData.setCurrentAssets(balanceSheet.getCurrentAssets());
			balanceSheetData.setCurrentLiabilities(balanceSheet.getCurrentLiabilities());
			balanceSheetData.setTotalLiabilities(balanceSheet.getTotalLiabilities());
			balanceSheetData.setEquity(balanceSheet.getEquity());

			balanceSheetDataList.add(balanceSheetData);
		});

		return balanceSheetDataList;
	}

	public List<FinancialRatioData> generateFinancialRatio(List<FinancialRatio> financialRatios) {
		List<FinancialRatioData> financialRatioDataList = new ArrayList<>();
		financialRatios.forEach(financialRatio -> {
			FinancialRatioData financialRatioData = new FinancialRatioData();

			financialRatioData.setFinancialRatioId(financialRatio.getId());
			financialRatioData.setYearTo(financialRatio.getYearTo());
			financialRatioData.setGpm(financialRatio.getGpm());
			financialRatioData.setNpm(financialRatio.getNpm());
			financialRatioData.setArdoh(financialRatio.getArdoh());
			financialRatioData.setInvdoh(financialRatio.getInvdoh());
			financialRatioData.setApdoh(financialRatio.getApdoh());
			financialRatioData.setCashCycle(financialRatio.getCashCycle());
			financialRatioData.setCashRatio(financialRatio.getCashRatio());
			financialRatioData.setEbitda(financialRatio.getEbitda());
			financialRatioData.setLeverage(financialRatio.getLeverage());
			financialRatioData.setWiNeeds(financialRatio.getWiNeeds());
			financialRatioData.setTie(financialRatio.getTie());
			financialRatioData.setDscr(financialRatio.getDscr());

			financialRatioDataList.add(financialRatioData);
		});

		return financialRatioDataList;
	}
}
