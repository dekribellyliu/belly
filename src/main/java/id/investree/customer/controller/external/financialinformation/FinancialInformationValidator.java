package id.investree.customer.controller.external.financialinformation;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.utils.DateUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.model.request.FinancialInformationListRequest;
import id.investree.customer.model.request.FinancialInformationRequest;
import id.investree.customer.service.balancesheet.BalanceSheetService;
import id.investree.customer.service.financialratio.FinancialRatioService;
import id.investree.customer.service.financialstatement.FinancialStatementService;
import id.investree.customer.service.financialtrend.FinancialTrendService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;

@Component
public class FinancialInformationValidator {

	@Autowired
	private FinancialTrendService financialTrendService;

	@Autowired
	private FinancialStatementService financialStatementService;

	@Autowired
	private FinancialRatioService financialRatioService;

	@Autowired
	private BalanceSheetService balanceSheetService;

	@Autowired
	private LocalMessageUtils localMessageUtils;

	public void financialValidator(FinancialInformationRequest request) {

		if (Utilities.isNull(request.getCustomerId())) {
			throw new AppException(localMessageUtils.errorBlank("customer id"));
		}

		if (Utilities.isNull(request.getStatementFileType())) {
			throw new AppException(localMessageUtils.errorBlank("Statement FileType"));
		}


		if (Constants.FINANCIAL_STATEMENT.equals(request.getStatementFileType()) && Utilities.isEmptyOrBlank(request.getStatementUrl())) {
			throw new AppException(localMessageUtils.errorBlank("Financial statement cannot be blank"));
		}

		if (Constants.FINANCIAL__E_STATEMENT.equals(request.getStatementFileType()) && Utilities.isEmptyOrBlank(request.getStatementUrl())) {
			throw new AppException(localMessageUtils.errorBlank("E-statement cannot be blank"));
		}

		if (!Utilities.isEmptyOrBlank(request.getStatementUrl())
				&& !Utilities.isValidExtensionFile(request.getStatementUrl(),
				GlobalConstants.FILE_EXT_JPG, GlobalConstants.FILE_EXT_JPEG, GlobalConstants.FILE_EXT_PNG, GlobalConstants.FILE_EXT_PDF)) {
			throw new AppException(localMessageUtils.dataNotValid("Statement file extension must be pdf, png, jpg or jpeg"));
		}

		if (!Constants.FINANCIAL_STATEMENT.equals(request.getStatementFileType()) && !Constants.FINANCIAL__E_STATEMENT.equals(request.getStatementFileType())) {
			throw new AppException(localMessageUtils.dataNotValid("Statement File Type"));
		}

		if (!Utilities.isNull(request.getStatementFileDate()) && DateUtils.isDateMoreThanToday(request.getStatementFileDate())) {
			throw new AppException(localMessageUtils.statementDate());
		}

		if (!Utilities.isNull(request.getStatementFileDate()) && !dateAfterSixYearsAgo(request.getStatementFileDate()) ) {
			throw new AppException(localMessageUtils.statementDateMinimunYear());
		}
	}

	public void partOfFinancialValidator(FinancialInformationListRequest request) {
		FinancialInformationRequest financialInformationRequest = new FinancialInformationRequest();
		financialInformationRequest.setCustomerId(request.getCustomerId());
		financialInformationRequest.setStatementFileType(request.getStatementFileType());
		financialInformationRequest.setStatementFileDate(request.getStatementFileAt());
		financialInformationRequest.setStatementUrl(request.getStatementUrl());

		financialValidator(financialInformationRequest);
	}

	private boolean dateAfterSixYearsAgo(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -60);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		Date dateAfterSixYearsAgo = cal.getTime();

		return date.after(dateAfterSixYearsAgo) || date.equals(dateAfterSixYearsAgo);
	}
}
