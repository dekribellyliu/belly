package id.investree.customer.controller.external.legalinformation;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.model.TokenPayload;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.LegalInformation;
import id.investree.customer.model.request.LegalInformationDataRequest;
import id.investree.customer.service.legalinformation.LegalInformationService;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class LegalInformationBody {

	@Autowired
	private LegalInformationValidator validator;

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private LegalInformationService service;

	public List<LegalInformation> setLegalEntity (List<LegalInformationDataRequest> requests, TokenPayload tokenPayload, Long customerId) {
		List<LegalInformation> legalInformations = new ArrayList<>();

		for (LegalInformationDataRequest request : requests) {
			validator.validateLegalInformation(customerId, tokenPayload, request);
			if (Utilities.isNull(request.getDocumentType())) {
				throw new AppException(messageUtils.dataNotValid("document type"));
			}

			LegalInformation legalInformation;
			if (GlobalConstants.USER_TYPE_BACKOFFICE_ID.equals(tokenPayload.getUserType())) {
				legalInformation = service.findByCustomerIdAndType(customerId, request.getDocumentType().getId());
				legalInformation.setCustomerId(customerId);
			} else {
				legalInformation = service.findByCustomerIdAndType(Long.valueOf(tokenPayload.getCustomerId()), request.getDocumentType().getId());
				legalInformation.setCustomerId(Long.valueOf(tokenPayload.getCustomerId()));
			}

			legalInformation.setDocumentType(request.getDocumentType().getId());
			legalInformation.setDocumentFile(request.getDocumentFile());
			legalInformation.setDocumentNumber(request.getDocumentNumber());
			legalInformation.setDocumentExpired(request.getDocumentExpiredDate());
			legalInformation.setCreatedBy(tokenPayload.getUserId());
			legalInformation.setUpdateBy(tokenPayload.getUserId());

			legalInformations.add(legalInformation);
		}

		return legalInformations;
	}
}
