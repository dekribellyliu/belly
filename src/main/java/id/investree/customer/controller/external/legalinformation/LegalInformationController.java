package id.investree.customer.controller.external.legalinformation;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.TokenPayload;
import id.investree.core.model.legacy.LegacyModel;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.controller.external.completing.CompletingDataFileCounterMapper;
import id.investree.customer.entity.LegalInformation;
import id.investree.customer.entity.UserData;
import id.investree.customer.model.data.LegalInformationData;
import id.investree.customer.model.legacy.LegacyModelBuilder;
import id.investree.customer.model.request.LegalInformationRequest;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.legacy.SyncLegacyService;
import id.investree.customer.service.legalinformation.LegalInformationService;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.CustomerUtils;
import id.investree.customer.utils.LocalMessageUtils;
import id.investree.customer.utils.StatusValidation;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/legal-information", produces = MediaType.APPLICATION_JSON_VALUE)
public class LegalInformationController extends BaseController {

	@Autowired
	private CustomerRoleService customerRoleService;

	@Autowired
	private LegalInformationService service;

	@Autowired
	private CustomerInformationService customerInformationService;

	@Autowired
	private LegalInformationService legalInformationService;

	@Autowired
	private SyncLegacyService syncLegacyService;

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private LegalInformationValidator validator;

	@Autowired
	private LegalInformationBody legalInformationBody;

	@Autowired
	private StatusValidation statusValidation;

	@Autowired
	private CustomerUtils customerUtils;

	@Autowired
	private CompletingDataFileCounterMapper counterMapper;

	@Autowired
	private LegalInformationResponseGenerator responseGenerator;

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private LoginDataService loginDataService;

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	@PostMapping("{userRoleType}")
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> save(HttpServletRequest http,
	                                           @PathVariable("userRoleType") String userRoleType,
	                                           @RequestBody LegalInformationRequest requests) {

		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(messageUtils.emptyToken());
		}

		Integer userTypeIdentification = customerUtils.getUserTypeIdentification(userRoleType);
		statusValidation.statusRestrictionValidation(tokenPayload, userTypeIdentification);

		String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);
		Long customerId = requests.getCustomerId();
		if (Utilities.isNull(customerId)) {
			throw new AppException(messageUtils.errorBlank("customer id"));
		}

		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(customerId))
			.orElseThrow(() -> new DataNotFoundException("User Data"));

		String emailAddress = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
			.map(LoginDataResponse::getEmailAddress)
			.orElseThrow(() -> new DataNotFoundException("Login Data"));

		Integer userCategory = Optional.ofNullable(customerInformationService.findById(customerId))
			.map(v -> Integer.valueOf(v.getUserCategory()))
			.orElseThrow(DataNotFoundException::new);

		List<LegalInformation> legalInformations = legalInformationBody.setLegalEntity(requests.getData(), tokenPayload, customerId);
		legalInformations = service.saveAllAndFlush(legalInformations);
		if (legalInformations.isEmpty() || Utilities.isNull(legalInformations)) {
			throw new AppException(messageUtils.insertFailed("legal information"));
		}

		LegacyModel legacyModel = new LegacyModelBuilder()
			.setLegalInformations(legalInformations)
			.setGlobalParam(completingDataGlobalParam)
			.setEmail(emailAddress)
			.build(customerId);
		syncLegacyService.sync(accessToken, legacyModel);

		List<LegalInformationData> responseLegalInfo = responseGenerator.generateLegalInformation(customerId);

		return GlobalConstants.CATEGORY_INSTITUSI_ID.equals(userCategory) ?
			abstractResponseHandler(counterMapper.countLegalInformationInstitusional(responseLegalInfo)).getResult(messageUtils.insertSuccess()) :
			abstractResponseHandler(counterMapper.countLegalInformationIndividual(responseLegalInfo)).getResult(messageUtils.insertSuccess());
	}
}
