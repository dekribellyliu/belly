package id.investree.customer.controller.external.legalinformation;

import id.investree.core.model.DefaultData;
import id.investree.customer.entity.LegalInformation;
import id.investree.customer.model.data.LegalInformationData;
import id.investree.customer.service.legalinformation.LegalInformationService;
import id.investree.customer.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class LegalInformationResponseGenerator {

	@Autowired
	private LegalInformationService legalInformationService;

	public List<LegalInformationData> generateLegalInformation(Long customerId) {
		List<LegalInformation> legalInformations = legalInformationService.findByCustomerId(customerId);
		List<LegalInformationData> legalInformationData = iterateLegalInfo(legalInformations);

		return legalInformationData;
	}

	public List<LegalInformationData> generatePartnerLegalInformation(Long customerId) {
		List<LegalInformation> legalInformations = legalInformationService.findByCustomerId(customerId);
		List<LegalInformationData> legalInformationData = new ArrayList<>();
		if (!legalInformations.isEmpty()) {
			legalInformationData = iterateLegalInfo(legalInformations);
		}

		return legalInformationData;
	}

	private List<LegalInformationData> iterateLegalInfo(List<LegalInformation> legalInformations) {
		List<LegalInformationData> legalInformationData = new ArrayList<>();
		for (Integer legalInfotype : Constants.getLegalInfoTypes()) {
			LegalInformationData legalInformation = legalInformations.stream().filter(v -> v.getDocumentType().equals(legalInfotype)).findFirst().map(v -> {
				LegalInformationData data = new LegalInformationData();
				data.setDocumentType(generateLegalInfoFileType(v.getDocumentType()));
				data.setDocumentFile(v.getDocumentFile());
				data.setDocumentNumber(v.getDocumentNumber());
				data.setDocumentRegistered(v.getDocumentRegistered());
				data.setDocumentExpiredDate(v.getDocumentExpired());

				return data;
			}).orElseGet(() -> {
				LegalInformationData data = new LegalInformationData();
				data.setDocumentType(generateLegalInfoFileType(legalInfotype));
				data.setDocumentFile(null);
				data.setDocumentNumber(null);
				data.setDocumentRegistered(null);
				data.setDocumentExpiredDate(null);

				return data;
			});

			legalInformationData.add(legalInformation);
		}

		return legalInformationData;
	}

	private DefaultData generateLegalInfoFileType(Integer fileType) {
		if (Constants.LEGAL_INFO_NPWP.equals(fileType)) {
			return new DefaultData(Constants.LEGAL_INFO_NPWP, Constants.LEGAL_INFO_NPWP_NAME);

		} else if (Constants.LEGAL_INFO_SIUP.equals(fileType)) {
			return new DefaultData(Constants.LEGAL_INFO_SIUP, Constants.LEGAL_INFO_SIUP_NAME);

		} else if (Constants.LEGAL_INFO_AKTA_PENDIRIAN.equals(fileType)) {
			return new DefaultData(Constants.LEGAL_INFO_AKTA_PENDIRIAN, Constants.LEGAL_INFO_AKTA_PENDIRIAN_NAME);

		} else if (Constants.LEGAL_INFO_AKTA_TERBARU.equals(fileType)) {
			return new DefaultData(Constants.LEGAL_INFO_AKTA_TERBARU, Constants.LEGAL_INFO_AKTA_TERBARU_NAME);

		} else if (Constants.LEGAL_INFO_SK_MENKUHAM.equals(fileType)) {
			return new DefaultData(Constants.LEGAL_INFO_SK_MENKUHAM, Constants.LEGAL_INFO_SK_MENKUHAM_NAME);

		} else if (Constants.LEGAL_INFO_SK_TDP.equals(fileType)) {
			return new DefaultData(Constants.LEGAL_INFO_SK_TDP, Constants.LEGAL_INFO_SK_TDP_NAME);

		} else if (Constants.LEGAL_INFO_SK_SKDU.equals(fileType)) {
			return new DefaultData(Constants.LEGAL_INFO_SK_SKDU, Constants.LEGAL_INFO_SK_SKDU_NAME);

		} else if (Constants.LEGAL_INFO_DGT.equals(fileType)) {
			return new DefaultData(Constants.LEGAL_INFO_DGT, Constants.LEGAL_INFO_DGT_NAME);

		} else if (Constants.LEGAL_INFO_ARTICLE_ASSOCIATION.equals(fileType)) {
			return new DefaultData(Constants.LEGAL_INFO_ARTICLE_ASSOCIATION, Constants.LEGAL_INFO_ARTICLE_ASSOCIATION_NAME);

		} else if (Constants.LEGAL_INFO_CERTIFICATE_INCUMBENCY.equals(fileType)) {
			return new DefaultData(Constants.LEGAL_INFO_CERTIFICATE_INCUMBENCY, Constants.LEGAL_INFO_CERTIFICATE_INCUMBENCY_NAME);

		}

		return null;
	}
}
