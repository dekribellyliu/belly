package id.investree.customer.controller.external.legalinformation;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataExistException;
import id.investree.core.model.TokenPayload;
import id.investree.core.regex.RegexValidator;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.LegalInformation;
import id.investree.customer.model.request.LegalInformationDataRequest;
import id.investree.customer.service.legalinformation.LegalInformationService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class LegalInformationValidator {

	@Autowired
	private LegalInformationService legalInformationService;

	@Autowired
	private LocalMessageUtils messageUtils;

	public void validateLegalInformation(Long customerId, TokenPayload tokenPayload, LegalInformationDataRequest request) {
		if (Utilities.isNull(request.getDocumentType())) {
			throw new AppException(messageUtils.dataNotValid("document type"));
		}

		if (!Constants.getLegalInfoTypes().contains(request.getDocumentType().getId())) {
			throw new AppException(messageUtils.dataNotValid("document type"));
		}

		if (!Utilities.isEmptyOrBlank(request.getDocumentFile())
				&& !Utilities.isValidExtensionFile(request.getDocumentFile(),
				GlobalConstants.FILE_EXT_PDF, GlobalConstants.FILE_EXT_PNG, GlobalConstants.FILE_EXT_JPEG, GlobalConstants.FILE_EXT_JPG)) {
			throw new AppException(messageUtils.dataNotValid(request.getDocumentType().getName() + " document file extension must be pdf, png, jpg or jpeg"));
		}

		if (!Utilities.isEmptyOrBlank(request.getDocumentNumber()) && Constants.LEGAL_INFO_NPWP.equals(request.getDocumentType().getId())) {


			if (!Utilities.isNull(request.getDocumentExpiredDate())) {
				throw new AppException(messageUtils.errorDocumentNotHaveExpireDate(Constants.LEGAL_INFO_NPWP_NAME));
			}

			if (!RegexValidator.isNumeric(request.getDocumentNumber())) {
				throw new AppException(messageUtils.errorNumberOnly(request.getDocumentType().getName()));
			}

			if (!RegexValidator.isNpwpNumberValid(request.getDocumentNumber())) {
				throw new AppException(messageUtils.errorNpwpNotValidDigit());
			}

			List<LegalInformation> legalInformation = legalInformationService.findByTypeAndDocNumber(request.getDocumentType().getId(), request.getDocumentNumber());
			if (!legalInformation.isEmpty()) {
				legalInformation.forEach(v -> {
					if (!customerId.equals(v.getCustomerId())) {
						throw new DataExistException(Constants.LEGAL_INFO_NPWP_NAME);
					}
				});
			}
		}

		if (Constants.LEGAL_INFO_SK_SKDU.equals(request.getDocumentType().getId()) && !Utilities.isNull(request.getDocumentExpiredDate())) {
			throw new AppException(messageUtils.errorDocumentNotHaveExpireDate(Constants.LEGAL_INFO_SK_SKDU_NAME));
		}
	}
}