package id.investree.customer.controller.external.lender;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.integration.notification.NotificationService;
import id.investree.core.utils.TokenUtils;
import id.investree.customer.controller.external.completing.CompletingDataMapper;
import id.investree.customer.entity.UserData;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.model.response.UserDataResponse;
import id.investree.customer.service.borrower.BorrowerService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.integration.IntegrationService;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.util.Optional;

@RestController
@RequestMapping(value = "/lender", produces = MediaType.APPLICATION_JSON_VALUE)
public class LenderController extends BaseController {

	@Autowired
	private LoginDataService loginDataService;

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private CustomerInformationService customerInformationService;

	@Autowired
	private CustomerRoleService customerRoleService;

	@Autowired
	private BorrowerService borrowerService;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private CompletingDataMapper completingDataMapper;

	@Autowired
	private LocalMessageUtils messageUtils;
	@Autowired
	private NotificationService notificationService;

	@Value("${app.name}")
	private String applicationName;

	@Autowired
	private IntegrationService integrationService;

	@GetMapping("{customerId}")
	public ResponseEntity<ResultResponse> findByUserId(HttpServletRequest http,
	                                                   @PathVariable("customerId") Long customerId) throws MalformedURLException {

		if (!Optional.ofNullable(tokenUtils.parseToken(http)).isPresent()) {
			throw new AppException(messageUtils.emptyToken());
		}

		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(customerId))
			.orElseThrow(() -> new DataNotFoundException("customer id: " + customerId));

		String accessToken = http.getHeader("X-Investree-Token");
		LoginDataResponse loginData = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
			.orElseThrow(() -> new DataNotFoundException("Login Data"));

		UserDataResponse userDataResponse = Optional.ofNullable(completingDataMapper.generateCompletingData(loginData, GlobalConstants.USER_CATEGORY_LENDER_ID, userData.getLoginDataId(), accessToken))
			.orElseThrow(() -> new DataNotFoundException("Completing Data"));

		return abstractResponseHandler(userDataResponse).getResult(messageUtils.dataFetched());
	}

}
