package id.investree.customer.controller.external.partnershipinformation;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.TokenPayload;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.CustomerInformation;
import id.investree.customer.entity.CustomerProductList;
import id.investree.customer.entity.PartnershipInformation;
import id.investree.customer.model.request.PartnershipInformationRequest;
import id.investree.customer.service.customerproductlist.CustomerProductListService;
import id.investree.customer.service.partnershipinformation.PartnershipInformationService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class PartnershipInformationBody {

	private final PartnershipInformationService partnershipInformationService;
	private final CustomerProductListService customerProductListService;
	private final LocalMessageUtils messageUtils;

	@Autowired
	public PartnershipInformationBody(
			PartnershipInformationService partnershipInformationService,
			CustomerProductListService customerProductListService,
			LocalMessageUtils messageUtils) {
		this.customerProductListService = customerProductListService;
		this.partnershipInformationService = partnershipInformationService;
		this.messageUtils = messageUtils;
	}

	public PartnershipInformation setPartnershipInfo(PartnershipInformationRequest request, TokenPayload tokenPayload,
													 Long customerId, Long customerRoleId) {
		PartnershipInformation partnershipInformation = partnershipInformationService.findByCustomerId(customerId);
		if (Utilities.isNull(partnershipInformation)) {
			partnershipInformation = new PartnershipInformation();

			partnershipInformation.setCreatedBy(tokenPayload.getUserId());
			partnershipInformation.setCreatedAt(new Date());
		} else {
			partnershipInformation.setUpdateBy(tokenPayload.getUserId());
		}

		List<CustomerProductList> productList = Optional.ofNullable(customerProductListService.findByCustomerRoleIds(customerRoleId))
				.orElse(new ArrayList<>());

		List<Integer> productListId = productList.stream().map(CustomerProductList::getType).map(Long::intValue)
				.collect(Collectors.toList());

		Long partnerId = request.getPartnerId();
		if (productListId.containsAll(Constants.getGroupProductWctl())) {
			partnerId = Constants.USER_PREFERENCE_WCTL.longValue();
		}

		partnershipInformation.setCustomerId(customerId);
		Optional.ofNullable(request.getType()).ifPresent(partnershipInformation::setType);
		Optional.ofNullable(partnerId).ifPresent(partnershipInformation::setPartnerId);
		Optional.ofNullable(request.getName()).ifPresent(partnershipInformation::setName);
		Optional.ofNullable(request.getCategory()).ifPresent(partnershipInformation::setCategory);
		Optional.ofNullable(request.getStartOfRelation()).ifPresent(partnershipInformation::setStartOfRelation);
		Optional.ofNullable(request.getSeller()).ifPresent(partnershipInformation::setSellerId);
		Optional.ofNullable(request.getSellerLink()).ifPresent(partnershipInformation::setSellerLink);
		Optional.ofNullable(request.getSellerLocation()).ifPresent(partnershipInformation::setSellerLocation);
		Optional.ofNullable(request.getBuildingLocOwnership()).ifPresent(partnershipInformation::setBuildingLocOwnership);
		Optional.ofNullable(request.getLengthOfOwnership()).ifPresent(partnershipInformation::setLengthOfOwnership);
		Optional.ofNullable(request.getInternalRating()).ifPresent(partnershipInformation::setInternalRating);
		Optional.ofNullable(request.getExternalRating()).ifPresent(partnershipInformation::setExternalRating);
		Optional.ofNullable(request.getInitialLoanAmount()).ifPresent(partnershipInformation::setInitialLoanAmount);
		Optional.ofNullable(request.getInitialLoanTenor()).ifPresent(partnershipInformation::setInitialLoanTenor);
		Optional.ofNullable(request.getMaxAmount()).ifPresent(partnershipInformation::setMaxAmount);
		Optional.ofNullable(request.getMaxTenor()).ifPresent(partnershipInformation::setMaxTenor);

		return partnershipInformation;
	}

	@Transactional
	public List<PartnershipInformation> setMultiplePartnershipInfo(List<PartnershipInformationRequest> requests, TokenPayload tokenPayload,
																   CustomerInformation customerInformation, Long customerRoleId) {

		List<PartnershipInformation> partnershipInformationList = new ArrayList<>();
		List<PartnershipInformation> partnershipInformations = partnershipInformationService.findAllByCustomerId(customerInformation.getId());

		for (PartnershipInformationRequest request : requests) {
			PartnershipInformation partnershipInformation;
			if (!Optional.ofNullable(request.getPartnershipInfoId()).isPresent()) {
				partnershipInformation = partnershipInformations.stream()
						.filter(v -> v.getPartnerId().equals(request.getPartnerId()))
						.findFirst().orElse(null);
				if (Optional.ofNullable(partnershipInformation).isPresent()) {
					throw new AppException(messageUtils.updateFailed());
				}

				partnershipInformation = new PartnershipInformation();
				partnershipInformation.setCreatedBy(tokenPayload.getUserId());
				partnershipInformation.setCreatedAt(new Date());
			} else if (Optional.ofNullable(customerInformation.getUserCategory()).isPresent()
					&& GlobalConstants.CATEGORY_INSTITUSI_ID.equals(Integer.valueOf(customerInformation.getUserCategory()))) {
				List<PartnershipInformation> filteredPartnershipInformations = partnershipInformations.stream()
						.filter(v -> !Utilities.isNull(v.getPartnerId()))
						.collect(Collectors.toList());

				if (!filteredPartnershipInformations.isEmpty()) {
					boolean isPartnerPresent = filteredPartnershipInformations.stream()
							.filter(v -> v.getPartnerId().equals(request.getPartnerId()))
							.anyMatch(v -> v.getId().equals(request.getPartnershipInfoId()));

					if (!isPartnerPresent) {
						throw new AppException(messageUtils.updateFailed());
					}
				}
				partnershipInformation =
						Optional.ofNullable(partnershipInformationService.findByIdAndCustomerId(request.getPartnershipInfoId(), customerInformation.getId()))
								.orElseThrow(() -> new DataNotFoundException("partnership info"));

				partnershipInformation.setUpdateBy(tokenPayload.getUserId());

			} else {
				partnershipInformation =
						Optional.ofNullable(partnershipInformationService.findByIdAndCustomerId(request.getPartnershipInfoId(), customerInformation.getId()))
								.orElseThrow(() -> new DataNotFoundException("partnership info"));

				partnershipInformation.setUpdateBy(tokenPayload.getUserId());
			}

			List<CustomerProductList> productList = Optional.ofNullable(customerProductListService.findByCustomerRoleIds(customerRoleId))
					.orElse(new ArrayList<>());

			List<Integer> productListId = productList.stream().map(CustomerProductList::getType).map(Long::intValue)
					.collect(Collectors.toList());

			Long partnerId = request.getPartnerId();
			if (productListId.containsAll(Constants.getGroupProductWctl())) {
				partnerId = Constants.USER_PREFERENCE_WCTL.longValue();
			}

			partnershipInformation.setCustomerId(customerInformation.getId());
			Optional.ofNullable(request.getType()).ifPresent(partnershipInformation::setType);
			Optional.ofNullable(partnerId).ifPresent(partnershipInformation::setPartnerId);
			Optional.ofNullable(request.getName()).ifPresent(partnershipInformation::setName);
			Optional.ofNullable(request.getCategory()).ifPresent(partnershipInformation::setCategory);
			Optional.ofNullable(request.getStartOfRelation()).ifPresent(partnershipInformation::setStartOfRelation);
			Optional.ofNullable(request.getSeller()).ifPresent(partnershipInformation::setSellerId);
			Optional.ofNullable(request.getSellerLink()).ifPresent(partnershipInformation::setSellerLink);
			Optional.ofNullable(request.getSellerLocation()).ifPresent(partnershipInformation::setSellerLocation);
			Optional.ofNullable(request.getBuildingLocOwnership()).ifPresent(partnershipInformation::setBuildingLocOwnership);
			Optional.ofNullable(request.getLengthOfOwnership()).ifPresent(partnershipInformation::setLengthOfOwnership);
			Optional.ofNullable(request.getInternalRating()).ifPresent(partnershipInformation::setInternalRating);
			Optional.ofNullable(request.getExternalRating()).ifPresent(partnershipInformation::setExternalRating);
			Optional.ofNullable(request.getInitialLoanAmount()).ifPresent(partnershipInformation::setInitialLoanAmount);
			Optional.ofNullable(request.getInitialLoanTenor()).ifPresent(partnershipInformation::setInitialLoanTenor);
			Optional.ofNullable(request.getMaxAmount()).ifPresent(partnershipInformation::setMaxAmount);
			Optional.ofNullable(request.getMaxTenor()).ifPresent(partnershipInformation::setMaxTenor);

			PartnershipInformation partnershipInformationSave = Optional.ofNullable(partnershipInformationService.saveOrUpdate(partnershipInformation))
					.orElseThrow(() -> new AppException(messageUtils.insertFailed("partnership info")));

			partnershipInformationList.add(partnershipInformationSave);
		}

		return partnershipInformationList;
	}
}
