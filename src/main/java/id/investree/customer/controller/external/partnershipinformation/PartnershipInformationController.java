package id.investree.customer.controller.external.partnershipinformation;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.exception.MissingTokenException;
import id.investree.core.model.TokenPayload;
import id.investree.core.utils.TokenUtils;
import id.investree.customer.entity.PartnershipInformation;
import id.investree.customer.model.data.TransactionHistoryData;
import id.investree.customer.model.request.TransactionHistoryRequest;
import id.investree.customer.model.response.PaginationDataResponse;
import id.investree.customer.service.integration.IntegrationService;
import id.investree.customer.service.partnershipinformation.PartnershipInformationService;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@RestController
@RequestMapping(value = "partnership-information", produces = MediaType.APPLICATION_JSON_VALUE)
public class PartnershipInformationController extends BaseController {

	@Autowired
	private PartnershipInformationService partnershipInformationService;

	@Autowired
	private IntegrationService integrationService;

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private TokenUtils tokenUtils;

	@GetMapping("transaction-history/{customerId}")
	public ResponseEntity<ResultResponse> getTransactionData(HttpServletRequest http,
															 @PathVariable(value = "customerId") Long customerId,
															 @RequestParam(value = "page", defaultValue = "1") Integer page,
															 @RequestParam(value = "size", defaultValue = "10") Integer size,
															 @RequestParam(value = "sort", defaultValue = "asc") String sort,
															 @RequestParam(value = "buyerName", required = false, defaultValue = "") String buyerName) {

		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
				.orElseThrow(MissingTokenException::new);
		if (!GlobalConstants.USER_TYPE_BACKOFFICE_ID.equals(tokenPayload.getUserType())) {
			throw new DataNotFoundException();
		}

		PartnershipInformation partnershipInformation = Optional.ofNullable(partnershipInformationService.findByCustomerId(customerId))
				.orElseThrow(DataNotFoundException::new);

		TransactionHistoryRequest request = new TransactionHistoryRequest()
				.setPartnershipId(partnershipInformation.getId().toString())
				.setPage(page)
				.setSize(size)
				.setSort(sort)
				.setBuyerName(buyerName)
				.build();

		PaginationDataResponse<TransactionHistoryData> transactionDataList = integrationService.getHistoryData(request);
		if (transactionDataList.getItems().isEmpty()) {
			throw new DataNotFoundException();
		}

		for (TransactionHistoryData data : transactionDataList.getItems()) {
			data.setPartnerName(partnershipInformation.getName());
		}

		return abstractResponseHandler(transactionDataList).getResult(messageUtils.dataFetched());
	}
}
