package id.investree.customer.controller.external.partnershipinformation;

import id.investree.customer.entity.PartnershipInformation;
import id.investree.customer.model.data.PartnershipInformationData;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PartnershipInformationResponseGenerator {

	/**
	 *
	 * terkait dengan FE yang sudah mengkonsumsi response multiple partnership maka single partnership sudah tidak digunakan lagi
	 *
	 */
	@Deprecated
	public PartnershipInformationData generatePartnershipInformation(PartnershipInformation partnershipInformation) {
		PartnershipInformationData partnershipInformationData = new PartnershipInformationData();

		partnershipInformationData.setPartnershipInfoId(partnershipInformation.getId());
		partnershipInformationData.setCustomerId(partnershipInformation.getCustomerId());
		partnershipInformationData.setType(partnershipInformation.getType());
		partnershipInformationData.setPartnerId(partnershipInformation.getPartnerId());
		partnershipInformationData.setName(partnershipInformation.getName());
		partnershipInformationData.setCategory(partnershipInformation.getCategory());
		partnershipInformationData.setStartOfRelation(partnershipInformation.getStartOfRelation());
		partnershipInformationData.setSeller(partnershipInformation.getSellerId());
		partnershipInformationData.setSellerLink(partnershipInformation.getSellerLink());
		partnershipInformationData.setSellerLocation(partnershipInformation.getSellerLocation());
		partnershipInformationData.setBuildingLocOwnership(partnershipInformation.getBuildingLocOwnership());
		partnershipInformationData.setLengthOfOwnership(partnershipInformation.getLengthOfOwnership());
		partnershipInformationData.setInternalRating(partnershipInformation.getInternalRating());
		partnershipInformationData.setExternalRating(partnershipInformation.getExternalRating());
		partnershipInformationData.setInitialLoanAmount(partnershipInformation.getInitialLoanAmount());
		partnershipInformationData.setInitialLoanTenor(partnershipInformation.getInitialLoanTenor());
		partnershipInformationData.setMaxAmount(partnershipInformation.getMaxAmount());
		partnershipInformationData.setMaxTenor(partnershipInformation.getMaxTenor());

		return partnershipInformationData;
	}

	public List<PartnershipInformationData> generatePartnershipInformationList(List<PartnershipInformation> partnershipInformations) {
		List<PartnershipInformationData> partnershipInformationDataList = new ArrayList<>();

		for (PartnershipInformation partnershipInformation : partnershipInformations) {
			PartnershipInformationData partnershipInformationData = new PartnershipInformationData();
			partnershipInformationData.setPartnershipInfoId(partnershipInformation.getId());
			partnershipInformationData.setCustomerId(partnershipInformation.getCustomerId());
			partnershipInformationData.setType(partnershipInformation.getType());
			partnershipInformationData.setPartnerId(partnershipInformation.getPartnerId());
			partnershipInformationData.setName(partnershipInformation.getName());
			partnershipInformationData.setCategory(partnershipInformation.getCategory());
			partnershipInformationData.setStartOfRelation(partnershipInformation.getStartOfRelation());
			partnershipInformationData.setSeller(partnershipInformation.getSellerId());
			partnershipInformationData.setSellerLink(partnershipInformation.getSellerLink());
			partnershipInformationData.setSellerLocation(partnershipInformation.getSellerLocation());
			partnershipInformationData.setBuildingLocOwnership(partnershipInformation.getBuildingLocOwnership());
			partnershipInformationData.setLengthOfOwnership(partnershipInformation.getLengthOfOwnership());
			partnershipInformationData.setInternalRating(partnershipInformation.getInternalRating());
			partnershipInformationData.setExternalRating(partnershipInformation.getExternalRating());
			partnershipInformationData.setInitialLoanAmount(partnershipInformation.getInitialLoanAmount());
			partnershipInformationData.setInitialLoanTenor(partnershipInformation.getInitialLoanTenor());
			partnershipInformationData.setMaxAmount(partnershipInformation.getMaxAmount());
			partnershipInformationData.setMaxTenor(partnershipInformation.getMaxTenor());

			partnershipInformationDataList.add(partnershipInformationData);
		}


		return partnershipInformationDataList;
	}
}
