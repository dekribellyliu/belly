package id.investree.customer.controller.external.personalprofile;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.UserData;
import id.investree.customer.model.request.PersonalProfileIdentificationRequest;
import id.investree.customer.model.request.UpdateUserDataRequest;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Optional;

@Component
public class PersonalProfileBody {

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private PersonalProfileValidator personalProfileValidator;

	public UserData userData(UpdateUserDataRequest request, Long customerId, Long userId, Long nationality) throws ParseException {

		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(customerId))
			.orElseThrow(DataNotFoundException::new);

		String validator = personalProfileValidator.validate(request, customerId, nationality);

		if (!Utilities.isNull(validator)) {
			throw new AppException(validator);
		}

		if (GlobalConstants.USER_TYPE_BACKOFFICE_ID.equals(Math.toIntExact(userId))) {
			userData.setNationality(Utilities.isNull(request.getNationality()) ? null : request.getNationality());
			userData.setCompanyPosition(Utilities.isNull(request.getPositionWithInstitution()) ? null : request.getPositionWithInstitution());
			userData.setCompanyDivision(Utilities.isNull(request.getDivisionWithInstitution()) ? null : request.getDivisionWithInstitution());
		}

		userData.setSelfieFile(Utilities.isEmptyOrBlank(request.getSelfiePicture()) ? null : request.getSelfiePicture());
		userData.setIdCardFile(Utilities.isEmptyOrBlank(request.getIdCardPicture()) ? null : request.getIdCardPicture());
		userData.setSelfieKtpFile(Utilities.isEmptyOrBlank(request.getSelfieKtpPicture()) ? null : request.getSelfieKtpPicture());
		userData.setSignatureFile(Utilities.isEmptyOrBlank(request.getSignaturePicture()) ? null : request.getSignaturePicture());
		userData.setIdCardNumber(Utilities.isEmptyOrBlank(request.getIdCardNumber()) ? null : request.getIdCardNumber());
		userData.setKtpAddress(Utilities.isEmptyOrBlank(request.getAddress()) ? null : request.getAddress());
		userData.setKtpProvince(Utilities.isNull(request.getProvince()) ? null : request.getProvince());
		userData.setKtpCity(Utilities.isNull(request.getCity()) ? null : request.getCity());
		userData.setKtpDistrict(Utilities.isNull(request.getDistrict()) ? null : request.getDistrict());
		userData.setKtpVillage(Utilities.isNull(request.getSubDistrict()) ? null : request.getSubDistrict());
		userData.setKtpPostalCode(Utilities.isEmptyOrBlank(request.getPostalCode()) ? null : request.getPostalCode());
		userData.setPlaceOfBirth(Utilities.isNull(request.getPlaceOfBirth()) ? null : request.getPlaceOfBirth());
		userData.setDateOfBirth(Utilities.isNull(request.getDateOfBirth()) ? null : request.getDateOfBirth());
		userData.setReligion(Utilities.isNull(request.getReligion()) ? null : request.getReligion());
		userData.setIdCardExpired(Utilities.isNull(request.getIdCardExpiredDate()) ? null : request.getIdCardExpiredDate());
		userData.setUpdatedBy(userId);
		userData.setSameAddress(Utilities.isNull(request.getSameAsDomicileAddress()) ? null : request.getSameAsDomicileAddress() ? GlobalConstants.STATUS_YES : GlobalConstants.STATUS_NO);
		userData.setMaritalStatus(Utilities.isNull(request.getMaritalStatus()) ? null : request.getMaritalStatus());
		userData.setEducation(Utilities.isNull(request.getEducation()) ? null : request.getEducation());
		userData.setOccupation(Utilities.isNull(request.getOccupation()) ? null : request.getOccupation());
		userData.setMotherName(Utilities.isEmptyOrBlank(request.getMotherMaidenName()) ? null : request.getMotherMaidenName());

		if (Utilities.isEmptyOrBlank(request.getPlaceOfBirthExternal())) {
			userData.setPlaceOfBirthExternal(null);
		} else {
			userData.setPlaceOfBirth(Constants.EXT_POB_ID);
			userData.setPlaceOfBirthExternal(Utilities.isEmptyOrBlank(request.getPlaceOfBirthExternal()) ? null : request.getPlaceOfBirthExternal());
		}

		if (GlobalConstants.STATUS_YES.equals(userData.getSameAddress())) {
			userData.setDomicileAddress(Utilities.isEmptyOrBlank(request.getAddress()) ? null : request.getAddress());
			userData.setDomicileProvince(Utilities.isNull(request.getProvince()) ? null : request.getProvince());
			userData.setDomicileCity(Utilities.isNull(request.getCity()) ? null : request.getCity());
			userData.setDomicileDistrict(Utilities.isNull(request.getDistrict()) ? null : request.getDistrict());
			userData.setDomicileVillage(Utilities.isNull(request.getSubDistrict()) ? null : request.getSubDistrict());
			userData.setDomicilePostalCode(Utilities.isEmptyOrBlank(request.getPostalCode()) ? null : request.getPostalCode());
		} else {
			userData.setDomicileAddress(Utilities.isEmptyOrBlank(request.getDomicileAddress()) ? null : request.getDomicileAddress());
			userData.setDomicileProvince(request.getDomicileProvince());
			userData.setDomicileCity(request.getDomicileCity());
			userData.setDomicileDistrict(request.getDomicileDistrict());
			userData.setDomicileVillage(request.getDomicileSubDistrict());
			userData.setDomicileProvinceName(request.getDomicileProvinceName());
			userData.setDomicileCityName(request.getDomicileCityName());
			userData.setDomicileDistrictName(request.getDomicileDistrictName());
			userData.setDomicileVillageName(request.getDomicileSubDistrictName());

			userData.setDomicilePostalCode(Utilities.isEmptyOrBlank(request.getDomicilePostalCode()) ? null : request.getDomicilePostalCode());
		}

		UserData userDataResponse = userDataService.saveOrUpdate(userData);
		if (Utilities.isNull(userDataResponse)) {
			throw new AppException(messageUtils.insertFailed("User Data"));
		}
		return userDataResponse;
	}

	public UserData setIdentification(PersonalProfileIdentificationRequest request, Long customerId, Long userId, Long nationality) throws ParseException {
		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(customerId))
			.orElseThrow(DataNotFoundException::new);

		String validator = personalProfileValidator.validatePersonalProfileIdentification(request, customerId, nationality);

		if (!Utilities.isNull(validator)) {
			throw new AppException(validator);
		}

		userData.setSelfieFile(request.getSelfiePicture());
		userData.setIdCardFile(request.getIdCardPicture());
		userData.setIdCardNumber(request.getIdCardNumber());
		userData.setIdCardExpired(request.getIdCardExpiredDate());
		userData.setKtpAddress(request.getAddress());
		userData.setKtpProvince(request.getProvince());
		userData.setKtpCity(request.getCity());
		userData.setKtpDistrict(request.getDistrict());
		userData.setKtpVillage(request.getSubDistrict());
		userData.setKtpPostalCode(request.getPostalCode());
		userData.setUpdatedBy(userId);

		if (GlobalConstants.STATUS_YES.equals(userData.getSameAddress())) {
			userData.setDomicileAddress(request.getAddress());
			userData.setDomicileProvince(request.getProvince());
			userData.setDomicileCity(request.getCity());
			userData.setDomicileDistrict(request.getDistrict());
			userData.setDomicileVillage(request.getSubDistrict());
			userData.setDomicilePostalCode(request.getPostalCode());
		}
		return userData;
	}

	public UserData setPersonalProfile(UpdateUserDataRequest request, Long customerId, Long userId, Long nationality) throws ParseException {

		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(customerId))
			.orElseThrow(DataNotFoundException::new);

		String validator = personalProfileValidator.validate(request, customerId, nationality);

		if (!Utilities.isNull(validator)) {
			throw new AppException(validator);
		}

		userData.setPlaceOfBirth(request.getPlaceOfBirth());
		userData.setDateOfBirth(request.getDateOfBirth());
		userData.setReligion(request.getReligion());
		userData.setSameAddress(request.getSameAsDomicileAddress() ? GlobalConstants.STATUS_YES : GlobalConstants.STATUS_NO);
		userData.setMaritalStatus(request.getMaritalStatus());
		userData.setEducation(request.getEducation());
		userData.setOccupation(request.getOccupation());
		userData.setMotherName(request.getMotherMaidenName());

		if (!Utilities.isEmptyOrBlank(request.getPlaceOfBirthExternal())) {
			userData.setPlaceOfBirth(Constants.EXT_POB_ID);
			userData.setPlaceOfBirthExternal(request.getPlaceOfBirthExternal());
		}

		if (request.getSameAsDomicileAddress()) {
			userData.setDomicileAddress(userData.getKtpAddress());
			userData.setDomicileProvince(userData.getKtpProvince());
			userData.setDomicileCity(userData.getKtpCity());
			userData.setDomicileDistrict(userData.getKtpDistrict());
			userData.setDomicileVillage(userData.getKtpVillage());
			userData.setDomicilePostalCode(userData.getKtpPostalCode());
		} else {
			userData.setDomicileAddress(request.getDomicileAddress());
			userData.setDomicileProvince(request.getDomicileProvince());
			userData.setDomicileCity(request.getDomicileCity());
			userData.setDomicileDistrict(request.getDomicileDistrict());
			userData.setDomicileVillage(request.getDomicileSubDistrict());
			userData.setDomicileProvinceName(request.getDomicileProvinceName());
			userData.setDomicileCityName(request.getDomicileCityName());
			userData.setDomicileDistrictName(request.getDomicileDistrictName());
			userData.setDomicileVillageName(request.getDomicileSubDistrictName());
			userData.setDomicilePostalCode(request.getDomicilePostalCode());
		}

		userData.setUpdatedBy(userId);

		return userData;
	}
}
