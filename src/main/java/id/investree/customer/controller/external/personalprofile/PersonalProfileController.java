package id.investree.customer.controller.external.personalprofile;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.DefaultData;
import id.investree.core.model.TokenPayload;
import id.investree.core.model.legacy.LegacyModel;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.controller.external.bankinformation.BankInformationBody;
import id.investree.customer.controller.external.bankinformation.BankInformationFlows;
import id.investree.customer.controller.external.completing.CompletingDataCounterMapper;
import id.investree.customer.controller.external.emergencycontact.EmergencyContactValidator;
import id.investree.customer.controller.external.legalinformation.LegalInformationBody;
import id.investree.customer.entity.*;
import id.investree.customer.model.data.PersonalProfileData;
import id.investree.customer.model.legacy.LegacyModelBuilder;
import id.investree.customer.model.request.*;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.model.response.PersonalProfileResponse;
import id.investree.customer.service.businessprofile.BusinessProfileService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.emergencycontact.EmergencyContactService;
import id.investree.customer.service.legacy.SyncLegacyService;
import id.investree.customer.service.legalinformation.LegalInformationService;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

@RestController
@RequestMapping(value = "/personal-profile", produces = MediaType.APPLICATION_JSON_VALUE)
public class PersonalProfileController extends BaseController {

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private BusinessProfileService businessProfileService;

	@Autowired
	private CustomerInformationService customerInformationService;

	@Autowired
	private EmergencyContactService emergencyContactService;

	@Autowired
	private LegalInformationService legalInformationService;

	@Autowired
	private SyncLegacyService syncLegacyService;

	@Autowired
	private EmergencyContactValidator emergencyContactValidator;

	@Autowired
	private BankInformationBody bankInformationBody;

	@Autowired
	private PersonalProfileBody personalProfileBody;

	@Autowired
	private BankInformationFlows flows;

	@Autowired
	private LegalInformationBody legalInformationBody;

	@Autowired
	private LoginDataService loginDataService;

	@Autowired
	private CustomerRoleService customerRoleService;

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private PersonalProfileValidator personalProfileValidator;

	@Autowired
	private CompletingDataCounterMapper counterMapper;

	@Autowired
	private PersonalProfileResponseGenerator personalProfileResponseGenerator;

	@Autowired
	private StatusValidation statusValidation;

	@Autowired
	private CustomerUtils customerUtils;

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	@PutMapping("{customerId}")
	public ResponseEntity<ResultResponse> savePersonalProfile(HttpServletRequest http,
	                                                          @PathVariable("customerId") Long customerId,
	                                                          @RequestParam(value = "userRoleType", defaultValue = "1") Integer userType,
	                                                          @RequestBody UpdateUserDataRequest request) throws ParseException, MalformedURLException {

		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(messageUtils.emptyToken());
		}

		String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);
		if (GlobalConstants.USER_TYPE_FRONTOFFICE_ID.equals(tokenPayload.getUserType())
			&& !Long.valueOf(tokenPayload.getCustomerId()).equals((customerId))) {
			throw new AppException(messageUtils.updateFailed());
		}

		statusValidation.statusRestrictionValidation(tokenPayload, userType);

		UserData userData = personalProfileBody.userData(request, customerId, tokenPayload.getUserId(), request.getNationality());

		String emailAddress = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
			.map(LoginDataResponse::getEmailAddress)
			.orElseThrow(() -> new DataNotFoundException("Login Data"));

		if (GlobalConstants.USER_CATEGORY_LENDER_ID.equals(userType)) {
			BusinessProfile businessProfile = Optional.ofNullable(businessProfileService.findByCustomerId(customerId))
				.map(v -> {
					v.setCustomerId(customerId);
					v.setCompanyAddress(request.getOccupationAddress());
					v.setCity(request.getOccupationCity());
					v.setPostalCode(request.getOccupationPostalCode());
					v.setLandLineNumber(request.getOccupationPhoneNumber());
					v.setMobilePrefix(request.getOccupationMobilePrefix());
					if (checkEmail(request.getMailingAddressStatus())) {
						v.setMailingAddressStatus(request.getMailingAddressStatus());
					}
					v.setUpdatedBy(tokenPayload.getUserId());
					return v;
				})
				.orElseGet(() -> {
					BusinessProfile occupationalInfo = new BusinessProfile();
					occupationalInfo.setCustomerId(customerId);
					occupationalInfo.setCompanyAddress(request.getOccupationAddress());
					occupationalInfo.setCity(request.getOccupationCity());
					occupationalInfo.setPostalCode(request.getOccupationPostalCode());
					occupationalInfo.setLandLineNumber(request.getOccupationPhoneNumber());
					occupationalInfo.setMobilePrefix(request.getOccupationMobilePrefix());
					if (checkEmail(request.getMailingAddressStatus())) {
						occupationalInfo.setMailingAddressStatus(request.getMailingAddressStatus());
					}
					occupationalInfo.setCreatedAt(new Date());
					occupationalInfo.setCreatedBy(tokenPayload.getUserId());

					return occupationalInfo;
				});

			BusinessProfile businessProfileSaved = businessProfileService.saveOrUpdate(businessProfile);
			if (null == businessProfileSaved) {
				throw new AppException(messageUtils.updateFailed());
			}

			CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.findById(customerId))
				.map(v -> {
					v.setAnnualIncome(request.getAnnualIncome());
					v.setSourceOfFund(request.getSourceOfFund());
					v.setUpdatedBy(tokenPayload.getUserId());

					return v;
				})
				.orElseThrow(DataNotFoundException::new);

			CustomerInformation customerInformationSaved = customerInformationService.saveOrUpdate(customerInformation);
			if (null == customerInformationSaved) {
				throw new AppException(messageUtils.updateFailed());
			}

			LegacyModel legacyModel = new LegacyModelBuilder()
				.setCustomerInformation(customerInformationSaved)
				.setGlobalParam(completingDataGlobalParam)
				.setBusinessProfile(businessProfileSaved)
				.setEmail(emailAddress)
				.build(customerId);

			syncLegacyService.sync(accessToken, legacyModel);
		}

		UserData userDataResponse = Optional.ofNullable(userDataService.findByCustomerId(customerId))
			.orElseThrow(DataNotFoundException::new);

		PersonalProfileData personalProfileData = personalProfileResponseGenerator.generatePersonalProfileData(userDataResponse, userType, accessToken);
		return abstractResponseHandler(personalProfileData).getResult(messageUtils.dataFetched());
	}

	@PutMapping("/identification/{userRoleType}")
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> saveLenderIdentification(HttpServletRequest http,
	                                                               @PathVariable("userRoleType") String userRoleType,
	                                                               @RequestBody PersonalProfileIdentificationRequest request)
		throws ParseException, MalformedURLException {
		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(messageUtils.emptyToken());
		}

		String accessToken = http.getHeader("X-Investree-Token");

		if (!GlobalConstants.USER_TYPE_FRONTOFFICE_ID.equals(tokenPayload.getUserType())) {
			throw new AppException(messageUtils.updateFailed());
		}

		Integer userTypeIdentification = customerUtils.getUserTypeIdentification(userRoleType);
		statusValidation.statusRestrictionValidation(tokenPayload, userTypeIdentification);

		UserData userData = personalProfileBody.setIdentification(request, Long.valueOf(tokenPayload.getCustomerId()),
			tokenPayload.getUserId(), request.getNationality());

		UserData userDataSave = Optional.ofNullable(userDataService.saveOrUpdate(userData))
			.orElseThrow(() -> new AppException(messageUtils.updateFailed()));

		String emailAddress = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
			.map(LoginDataResponse::getEmailAddress)
			.orElseThrow(() -> new DataNotFoundException("Login Data"));

		BusinessProfile businessProfile = Optional.ofNullable(businessProfileService.findByCustomerId(Long.valueOf(tokenPayload.getCustomerId())))
			.map(v -> {
				if (checkEmail(request.getMailingAddressStatus())) {
					v.setMailingAddressStatus(request.getMailingAddressStatus());
				}
				v.setUpdatedBy(tokenPayload.getUserId());
				return v;
			}).orElseGet(() -> {
				BusinessProfile occupationalInfo = new BusinessProfile();
				if (checkEmail(request.getMailingAddressStatus())) {
					occupationalInfo.setMailingAddressStatus(request.getMailingAddressStatus());
				}
				occupationalInfo.setCreatedAt(new Date());
				occupationalInfo.setCreatedBy(tokenPayload.getUserId());

				return occupationalInfo;
			});

		BusinessProfile businessProfileSaved = businessProfileService.saveOrUpdate(businessProfile);
		if (null == businessProfileSaved) {
			throw new AppException(messageUtils.updateFailed());
		}

		if (Utilities.isNull(userDataService.findByCustomerId(Long.valueOf(tokenPayload.getCustomerId())))) {
			throw new DataNotFoundException();
		}

		CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.findById(Long.valueOf(tokenPayload.getCustomerId())))
			.orElseThrow(DataNotFoundException::new);

		PersonalProfileData personalProfileData = personalProfileResponseGenerator.generatePersonalProfileData(userData,
			userTypeIdentification, accessToken);

		PersonalProfileResponse personalProfileResponse = new PersonalProfileResponse();
		personalProfileResponse.setPersonalProfile(personalProfileData);
		if (Constants.NATIONALITY_INDONESIA.equals(userData.getNationality())) {
			personalProfileResponse.setCounter(counterMapper.counterPersonalProfile(personalProfileData,
				customerInformation.getUserCategory(), userTypeIdentification));
		} else {
			personalProfileResponse.setCounter(counterMapper.counterPersonalProfileForeign(personalProfileData));
		}

		LegacyModel legacyModel = new LegacyModelBuilder()
			.setUserData(userDataSave)
			.setGlobalParam(completingDataGlobalParam)
			.setBusinessProfile(businessProfileSaved)
			.setEmail(emailAddress)
			.build(userData.getCustomerId());

		syncLegacyService.sync(accessToken, legacyModel);

		return abstractResponseHandler(personalProfileResponse).getResult(messageUtils.insertSuccess());
	}

	@PutMapping("/personal-data/{userRoleType}")
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> saveLenderPersonalData(HttpServletRequest http,
	                                                             @PathVariable("userRoleType") String userRoleType,
	                                                             @RequestBody UpdateUserDataRequest request)
		throws ParseException, MalformedURLException {
		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(messageUtils.emptyToken());
		}

		String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);

		if (!GlobalConstants.USER_TYPE_FRONTOFFICE_ID.equals(tokenPayload.getUserType())) {
			throw new AppException(messageUtils.updateFailed());
		}

		Integer userTypeIdentification = customerUtils.getUserTypeIdentification(userRoleType);
		statusValidation.statusRestrictionValidation(tokenPayload, userTypeIdentification);

		UserData userData = personalProfileBody.setPersonalProfile(request, Long.valueOf(tokenPayload.getCustomerId()),
			tokenPayload.getUserId(), request.getNationality());

		UserData userDataSave = Optional.ofNullable(userDataService.saveOrUpdate(userData))
			.orElseThrow(() -> new AppException(messageUtils.updateFailed()));

		String emailAddress = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
			.map(LoginDataResponse::getEmailAddress)
			.orElseThrow(() -> new DataNotFoundException("Login Data"));

		BusinessProfile businessProfile = Optional.ofNullable(businessProfileService.findByCustomerId(Long.valueOf(tokenPayload.getCustomerId())))
			.map(v -> {
				if (checkEmail(request.getMailingAddressStatus())) {
					v.setMailingAddressStatus(request.getMailingAddressStatus());
				}
				v.setUpdatedBy(tokenPayload.getUserId());
				return v;
			}).orElseGet(() -> {
				BusinessProfile occupationalInfo = new BusinessProfile();
				if (checkEmail(request.getMailingAddressStatus())) {
					occupationalInfo.setMailingAddressStatus(request.getMailingAddressStatus());
				}
				occupationalInfo.setCreatedAt(new Date());
				occupationalInfo.setCreatedBy(tokenPayload.getUserId());

				return occupationalInfo;
			});

		BusinessProfile businessProfileSaved = businessProfileService.saveOrUpdate(businessProfile);
		if (null == businessProfileSaved) {
			throw new AppException(messageUtils.updateFailed());
		}

		CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.findById(Long.valueOf(tokenPayload.getCustomerId())))
			.orElseThrow(DataNotFoundException::new);

		PersonalProfileData personalProfileData = personalProfileResponseGenerator.generatePersonalProfileData(userData,
			userTypeIdentification, accessToken);

		PersonalProfileResponse personalProfileResponse = new PersonalProfileResponse();
		personalProfileResponse.setPersonalProfile(personalProfileData);
		if (Constants.NATIONALITY_INDONESIA.equals(userData.getNationality())) {
			personalProfileResponse.setCounter(counterMapper.counterPersonalProfile(personalProfileData,
				customerInformation.getUserCategory(), userTypeIdentification));
		} else {
			personalProfileResponse.setCounter(counterMapper.counterPersonalProfileForeign(personalProfileData));
		}

		LegacyModel legacyModel = new LegacyModelBuilder()
			.setUserData(userDataSave)
			.setGlobalParam(completingDataGlobalParam)
			.setBusinessProfile(businessProfileSaved)
			.setEmail(emailAddress)
			.build(userData.getCustomerId());

		syncLegacyService.sync(accessToken, legacyModel);

		return abstractResponseHandler(personalProfileResponse)
			.getResult(messageUtils.insertSuccess());
	}

	@PutMapping("/occupational-information")
	public ResponseEntity<ResultResponse> saveLenderOccupationalInformation(HttpServletRequest http,
	                                                                        @RequestBody UpdateUserDataRequest request)
		throws ParseException, MalformedURLException {
		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(messageUtils.emptyToken());
		}

		if (!GlobalConstants.USER_TYPE_FRONTOFFICE_ID.equals(tokenPayload.getUserType())) {
			throw new AppException(messageUtils.updateFailed());
		}

		statusValidation.statusRestrictionValidation(tokenPayload, GlobalConstants.USER_CATEGORY_LENDER_ID);

		BusinessProfile businessProfile = Optional.ofNullable(businessProfileService.findByCustomerId(
			Long.valueOf(tokenPayload.getCustomerId())))
			.map(v -> {
				v.setCustomerId(Long.valueOf(tokenPayload.getCustomerId()));
				v.setCompanyAddress(request.getOccupationAddress());
				v.setCity(request.getOccupationCity());
				v.setPostalCode(request.getOccupationPostalCode());
				v.setLandLineNumber(request.getOccupationPhoneNumber());
				v.setMobilePrefix(request.getOccupationMobilePrefix());
				if (checkEmail(request.getMailingAddressStatus())) {
					v.setMailingAddressStatus(request.getMailingAddressStatus());
				}
				v.setUpdatedBy(tokenPayload.getUserId());
				return v;
			})
			.orElseGet(() -> {
				BusinessProfile occupationalInfo = new BusinessProfile();
				occupationalInfo.setCustomerId(Long.valueOf(tokenPayload.getCustomerId()));
				occupationalInfo.setCompanyAddress(request.getOccupationAddress());
				occupationalInfo.setCity(request.getOccupationCity());
				occupationalInfo.setPostalCode(request.getOccupationPostalCode());
				occupationalInfo.setLandLineNumber(request.getOccupationPhoneNumber());
				occupationalInfo.setMobilePrefix(request.getOccupationMobilePrefix());
				if (checkEmail(request.getMailingAddressStatus())) {
					occupationalInfo.setMailingAddressStatus(request.getMailingAddressStatus());
				}
				occupationalInfo.setCreatedAt(new Date());
				occupationalInfo.setCreatedBy(tokenPayload.getUserId());

				return occupationalInfo;
			});

		if (!Optional.ofNullable(businessProfileService.saveOrUpdate(businessProfile)).isPresent()) {
			throw new AppException(messageUtils.updateFailed());
		}

		String accessToken = http.getHeader("X-Investree-Token");

		UserData userDataResponse = Optional.ofNullable(userDataService.findByCustomerId(Long.valueOf(tokenPayload.getCustomerId())))
			.orElseThrow(DataNotFoundException::new);

		CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.findById(Long.valueOf(tokenPayload.getCustomerId())))
			.orElseThrow(DataNotFoundException::new);

		PersonalProfileData personalProfileData = personalProfileResponseGenerator.generatePersonalProfileData(userDataResponse,
			GlobalConstants.USER_CATEGORY_LENDER_ID, accessToken);

		PersonalProfileResponse personalProfileResponse = new PersonalProfileResponse();
		personalProfileResponse.setPersonalProfile(personalProfileData);
		if (Constants.NATIONALITY_INDONESIA.equals(userDataResponse.getNationality())) {
			personalProfileResponse.setCounter(counterMapper.counterPersonalProfile(personalProfileData,
				customerInformation.getUserCategory(), GlobalConstants.USER_CATEGORY_LENDER_ID));
		} else {
			personalProfileResponse.setCounter(counterMapper.counterPersonalProfileForeign(personalProfileData));
		}

		return abstractResponseHandler(personalProfileResponse)
			.getResult(messageUtils.insertSuccess());
	}

	@PutMapping("/income-information")
	public ResponseEntity<ResultResponse> saveLenderIncomeInformation(HttpServletRequest http,
	                                                                  @RequestBody UpdateUserDataRequest request)
		throws ParseException, MalformedURLException {
		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(messageUtils.emptyToken());
		}

		if (!GlobalConstants.USER_TYPE_FRONTOFFICE_ID.equals(tokenPayload.getUserType())) {
			throw new AppException(messageUtils.updateFailed());
		}

		statusValidation.statusRestrictionValidation(tokenPayload, GlobalConstants.USER_CATEGORY_LENDER_ID);

		CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.findById(
			Long.valueOf(tokenPayload.getCustomerId())))
			.map(v -> {
				v.setAnnualIncome(request.getAnnualIncome());
				v.setSourceOfFund(request.getSourceOfFund());

				v.setUpdatedBy(tokenPayload.getUserId());

				return v;
			})
			.orElseThrow(DataNotFoundException::new);

		if (!Optional.ofNullable(customerInformationService.saveOrUpdate(customerInformation)).isPresent()) {
			throw new AppException(messageUtils.updateFailed());
		}

		String accessToken = http.getHeader("X-Investree-Token");

		UserData userDataResponse = Optional.ofNullable(userDataService.findByCustomerId(Long.valueOf(tokenPayload.getCustomerId())))
			.orElseThrow(DataNotFoundException::new);

		PersonalProfileData personalProfileData = personalProfileResponseGenerator.generatePersonalProfileData(userDataResponse,
			GlobalConstants.USER_CATEGORY_LENDER_ID, accessToken);

		PersonalProfileResponse personalProfileResponse = new PersonalProfileResponse();
		personalProfileResponse.setPersonalProfile(personalProfileData);
		if (Constants.NATIONALITY_INDONESIA.equals(userDataResponse.getNationality())) {
			personalProfileResponse.setCounter(counterMapper.counterPersonalProfile(personalProfileData,
				customerInformation.getUserCategory(), GlobalConstants.USER_CATEGORY_LENDER_ID));
		} else {
			personalProfileResponse.setCounter(counterMapper.counterPersonalProfileForeign(personalProfileData));
		}

		return abstractResponseHandler(personalProfileResponse)
			.getResult(messageUtils.insertSuccess());
	}

	@PutMapping("/save-all/{customerId}")
	public ResponseEntity<ResultResponse> updatePersonalProfile(HttpServletRequest http,
	                                                            @PathVariable(value = "customerId") Long customerId,
	                                                            @RequestParam(value = "userRoleType", defaultValue = "1") Integer userType,
	                                                            @RequestBody UpdatePersonalDataRequest request) throws ParseException {
		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
			.orElseThrow(() -> new AppException(messageUtils.emptyToken()));

		String accessToken = http.getHeader("X-Investree-Token");

		String userCategory = Optional.ofNullable(customerInformationService.findById(customerId))
			.map(CustomerInformation::getUserCategory)
			.orElseThrow(DataNotFoundException::new);

		UserData userData = personalProfileBody.userData(request.getPersonalData(), customerId,
			tokenPayload.getUserId(), request.getPersonalData().getNationality());

		LoginDataResponse loginDataResponse = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
			.orElseThrow(() -> new DataNotFoundException("Login Data"));

		if (!Utilities.isNull(request.getBasicData())) {
			LoginDataRequest loginDataRequest = new LoginDataRequest();
			loginDataRequest.setSalutation(Utilities.isEmptyOrBlank(request.getBasicData().getSalutation()) ?
				loginDataResponse.getSalutation() : request.getBasicData().getSalutation());
			loginDataRequest.setUsername(Utilities.isEmptyOrBlank(request.getBasicData().getUsername()) ?
				loginDataResponse.getUsername() : request.getBasicData().getUsername());
			loginDataRequest.setFullname(Utilities.isEmptyOrBlank(request.getBasicData().getFullname()) ?
				loginDataResponse.getFullname() : request.getBasicData().getFullname());
			loginDataRequest.setEmailAddress(Utilities.isEmptyOrBlank(request.getBasicData().getEmailAddress()) ?
				loginDataResponse.getEmailAddress() : request.getBasicData().getEmailAddress());
			loginDataRequest.setActive(GlobalConstants.STATUS_YES.equalsIgnoreCase(loginDataResponse.getActive()));
			loginDataRequest.setAgreePrivacy(GlobalConstants.STATUS_YES.equalsIgnoreCase(loginDataResponse.getAgreePrivacy()));
			loginDataRequest.setAgreeSubscribe(GlobalConstants.STATUS_YES.equalsIgnoreCase(loginDataResponse.getAgreeSubscribe()));
			loginDataRequest.setEmailStatus(loginDataResponse.getEmailStatus());

			loginDataService.updateLoginData(accessToken, userData.getLoginDataId(), loginDataRequest);
		}

		if (GlobalConstants.USER_CATEGORY_LENDER_ID.equals(userType)) {
			BusinessProfile businessProfile = Optional.ofNullable(businessProfileService.findByCustomerId(customerId))
				.map(v -> {
					v.setCustomerId(customerId);
					v.setCompanyAddress(request.getPersonalData().getOccupationAddress());
					v.setCity(request.getPersonalData().getOccupationCity());
					v.setPostalCode(request.getPersonalData().getOccupationPostalCode());
					if (checkEmail(request.getPersonalData().getMailingAddressStatus())) {
						v.setMailingAddressStatus(request.getPersonalData().getMailingAddressStatus());
					}
					v.setUpdatedBy(tokenPayload.getUserId());
					return v;
				})
				.orElseGet(() -> {
					BusinessProfile occupationalInfo = new BusinessProfile();
					occupationalInfo.setCustomerId(customerId);
					occupationalInfo.setCompanyAddress(request.getPersonalData().getOccupationAddress());
					occupationalInfo.setCity(request.getPersonalData().getOccupationCity());
					occupationalInfo.setPostalCode(request.getPersonalData().getOccupationPostalCode());
					if (checkEmail(request.getPersonalData().getMailingAddressStatus())) {
						occupationalInfo.setMailingAddressStatus(request.getPersonalData().getMailingAddressStatus());
					}
					occupationalInfo.setCreatedAt(new Date());
					occupationalInfo.setCreatedBy(tokenPayload.getUserId());
					return occupationalInfo;
				});
			if (!Optional.ofNullable(businessProfileService.saveOrUpdate(businessProfile)).isPresent()) {
				throw new AppException(messageUtils.updateFailed());
			}
		}

		if (!GlobalConstants.CATEGORY_INSTITUSI_ID.toString().equalsIgnoreCase(userCategory)) {
			request.getBankInformation().forEach(bankInformationRequest -> {
				bankInformationRequest.setCustomerId(customerId);
			});
			List<BankInformationRequest> validRequests = flows.bankCreationBulk(request.getBankInformation(), userType);

			List<BankInformation> bankResponse = bankInformationBody.setBankInformation(validRequests, customerId, userType, accessToken);

			List<EmergencyContact> emergencyContacts = new ArrayList<>();

			for (EmergencyContactRequest emergencyContactRequest : request.getEmergencyContact()) {
				emergencyContactRequest.setCustomerId(customerId);

				emergencyContactValidator.contactValidator(emergencyContactRequest, accessToken);

				EmergencyContact emergencyContactData = emergencyContactService
					.findByIdentityCardNumberAndCustomerIdAndDeletedIsNull(emergencyContactRequest.getIdentityCardNumber(), customerId);

				if (Utilities.isNull(emergencyContactData) && !emergencyContactRequest.isDelete()) {
					emergencyContactData = new EmergencyContact();
					emergencyContactData.setCreatedBy(tokenPayload.getUserId());
				} else if (emergencyContactRequest.isDelete()) {
					emergencyContactData.setDeleteBy(tokenPayload.getUserId());
					emergencyContactData.setDeletedAt(new Date());
				} else {
					emergencyContactData.setUpdateBy(tokenPayload.getUserId());
				}

				emergencyContactData.setCustomerId(customerId);
				emergencyContactData.setRelationship(emergencyContactRequest.getRelationship());
				emergencyContactData.setFullname(emergencyContactRequest.getFullName());
				if (emergencyContactRequest.getMobilePrefix() instanceof DefaultData) {
					emergencyContactData.setMobilePrefix(((DefaultData) emergencyContactRequest.getMobilePrefix()).getId().longValue());
				} else if (emergencyContactRequest.getMobilePrefix() instanceof Long) {
					emergencyContactData.setMobilePrefix((Long) emergencyContactRequest.getMobilePrefix());
				}
				emergencyContactData.setMobileNumber(emergencyContactRequest.getMobileNumber());
				emergencyContactData.setEmailAddress(emergencyContactRequest.getEmailAddress());
				emergencyContactData.setAddress(emergencyContactRequest.getAddress());
				emergencyContactData.setProvince(Utilities.getIdFromDefaultData(emergencyContactRequest.getProvince()));
				emergencyContactData.setCity(Utilities.getIdFromDefaultData(emergencyContactRequest.getCity()));
				emergencyContactData.setDistrict(Utilities.getIdFromDefaultData(emergencyContactRequest.getDistrict()));
				emergencyContactData.setVillage(Utilities.getIdFromDefaultData(emergencyContactRequest.getVillage()));
				emergencyContactData.setPostalCode(emergencyContactRequest.getPostalCode());
				emergencyContactData.setIdentityCardUrl(emergencyContactRequest.getIdentityCardUrl());
				emergencyContactData.setIdentityCardNumber(emergencyContactRequest.getIdentityCardNumber());
				emergencyContactData.setIdentityCardExpired(emergencyContactRequest.getIdentityExpiryDate());

				emergencyContacts.add(emergencyContactData);
			}

			if (!Optional.ofNullable(emergencyContactService.saveAll(emergencyContacts)).isPresent()) {
				throw new AppException(messageUtils.insertFailed("emergency contact bulk"));
			}
		}

		List<LegalInformation> legalInformations = legalInformationBody.setLegalEntity(request.getLegalInformation(), tokenPayload, customerId);

		if (!Optional.ofNullable(legalInformationService.saveAll(legalInformations)).isPresent()) {
			throw new AppException(messageUtils.updateFailed());
		}

		return abstractResponseHandler(true).getResult(messageUtils.updateSuccess());
	}

	private Boolean checkEmail(Integer mailingAddressStatus) {
		if (!Utilities.isNull(mailingAddressStatus)) {
			int arr[] = {1, 2, 3};
			return IntStream.of(arr).anyMatch(value -> value == mailingAddressStatus);
		} else {
			return false;
		}
	}
}
