package id.investree.customer.controller.external.personalprofile;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.model.DefaultData;
import id.investree.core.utils.DateUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.*;
import id.investree.customer.model.data.PersonalProfileData;
import id.investree.customer.service.businessprofile.BusinessProfileService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.legalinformation.LegalInformationService;
import id.investree.customer.service.postalarea.PostalAreaService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URL;

@Component
public class PersonalProfileResponseGenerator {

	private PostalAreaService postalAreaService;
	private CustomerInformationService customerInformationService;
	private CustomerRoleService customerRoleService;
	private BusinessProfileService businessProfileService;
	private LegalInformationService legalInformationService;
	private StoreDataGlobalParam completingDataGlobalParam;

	@Autowired
	public PersonalProfileResponseGenerator(PostalAreaService postalAreaService,
											CustomerInformationService customerInformationService,
											CustomerRoleService customerRoleService,
											BusinessProfileService businessProfileService,
											LegalInformationService legalInformationService,
											StoreDataGlobalParam completingDataGlobalParam) {
		this.postalAreaService = postalAreaService;
		this.customerInformationService = customerInformationService;
		this.customerRoleService = customerRoleService;
		this.businessProfileService = businessProfileService;
		this.legalInformationService = legalInformationService;
		this.completingDataGlobalParam = completingDataGlobalParam;
	}

	public PersonalProfileData generatePersonalProfileData(UserData userData, Integer userTypeIdentification, String accessToken) throws MalformedURLException {

		CustomerInformation customerInformation = customerInformationService.findById(userData.getCustomerId());
		CustomerRole customerRole = customerRoleService.findByCustomerId(userData.getCustomerId());
		BusinessProfile businessProfile = businessProfileService.findByCustomerId(userData.getCustomerId());

		String expireDate = DateUtils.convertToString(userData.getIdCardExpired(), "yyyy-MM-dd");
		String dateOfBirth = DateUtils.convertToString(userData.getDateOfBirth(), "yyyy-MM-dd");

		DefaultData province = postalAreaService.findProvince(accessToken, userData.getKtpProvince());
		DefaultData city = postalAreaService.findCity(accessToken, userData.getKtpCity());
		DefaultData occupationCity = postalAreaService.findCity(accessToken, businessProfile.getCity());
		DefaultData district = postalAreaService.findDistrict(accessToken, userData.getKtpDistrict());
		DefaultData subDistrict = postalAreaService.findVillage(accessToken, userData.getKtpVillage());
		DefaultData domicileProvince = postalAreaService.findProvince(accessToken, userData.getDomicileProvince());
		DefaultData domicileCity = postalAreaService.findCity(accessToken, userData.getDomicileCity());
		DefaultData domicileDistrict = postalAreaService.findDistrict(accessToken, userData.getDomicileDistrict());
		DefaultData domicileSubDistrict = postalAreaService.findVillage(accessToken, userData.getDomicileVillage());

		DefaultData placeOfBirth;
		if (!Utilities.isNull(userData.getPlaceOfBirth()) && Constants.EXT_POB_ID.equals(userData.getPlaceOfBirth())) {
			placeOfBirth = new DefaultData(Math.toIntExact(Constants.EXT_POB_ID), userData.getPlaceOfBirthExternal());
		} else {
			placeOfBirth = postalAreaService.findCity(accessToken, userData.getPlaceOfBirth());
		}

		LegalInformation legalInformation = legalInformationService.findByCustomerIdAndType(userData.getCustomerId(), Constants.LEGAL_INFO_NPWP);

		String npwpFileName;
		if (!Utilities.isNull(legalInformation.getDocumentFile()) && legalInformation.getDocumentFile().startsWith("http")) {
			URL url = new URL(legalInformation.getDocumentFile());
			npwpFileName = FilenameUtils.getName(url.getPath());
		} else {
			npwpFileName = legalInformation.getDocumentFile();
		}

		String signatureFileName;
		if (!Utilities.isNull(userData.getSignatureFile()) && userData.getSignatureFile().startsWith("http")) {
			URL url = new URL(userData.getSignatureFile());
			signatureFileName = FilenameUtils.getName(url.getPath());
		} else {
			signatureFileName = userData.getSignatureFile();
		}

		String idCardFileName;
		if (!Utilities.isNull(userData.getIdCardFile()) && userData.getIdCardFile().startsWith("http")) {
			URL url = new URL(userData.getIdCardFile());
			idCardFileName = FilenameUtils.getName(url.getPath());
		} else {
			idCardFileName = userData.getIdCardFile();
		}

		String selfieFileName;
		if (!Utilities.isNull(userData.getSelfieFile()) && userData.getSelfieFile().startsWith("http")) {
			URL url = new URL(userData.getSelfieFile());
			selfieFileName = FilenameUtils.getName(url.getPath());
		} else {
			selfieFileName = userData.getSelfieFile();
		}

		completingDataGlobalParam.getGlobalParam(
				GlobalConstants.GLOBAL_PARAM_MARITAL_STATUS,
				GlobalConstants.GLOBAL_PARAM_OCCUPATION,
				GlobalConstants.GLOBAL_PARAM_EDUCATION,
				GlobalConstants.GLOBAL_PARAM_RELIGION,
				GlobalConstants.GLOBAL_PARAM_MOBILE_PREFIX,
				Constants.GLOBAL_PARAM_INCOME_LEVEL,
				Constants.GLOBAL_PARAM_SOURCE_OF_FUND
		);

		DefaultData maritalStatus = null;
		if (!Utilities.isNull(userData.getMaritalStatus())) {
			maritalStatus = completingDataGlobalParam.getMaritalStatusList().stream()
					.filter(v -> v.getId().equals(Math.toIntExact(userData.getMaritalStatus())))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData occupation = null;
		if (!Utilities.isNull(userData.getOccupation())) {
			occupation = completingDataGlobalParam.getOccupationList().stream()
					.filter(v -> v.getId().equals(Math.toIntExact(userData.getOccupation())))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData education = null;
		if (!Utilities.isNull(userData.getEducation())) {
			education = completingDataGlobalParam.getEducationList().stream()
					.filter(v -> v.getId().equals(Math.toIntExact(userData.getEducation())))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData religion = null;
		if (!Utilities.isNull(userData.getReligion())) {
			religion = completingDataGlobalParam.getReligionList().stream()
					.filter(v -> v.getId().equals(Math.toIntExact(userData.getReligion())))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData annualIncome = null;
		if (!Utilities.isNull(customerInformation.getAnnualIncome())) {
			annualIncome = completingDataGlobalParam.getIncomeLevelList().stream()
					.filter(v -> v.getId().equals(Math.toIntExact(customerInformation.getAnnualIncome())))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData sourceOfFund = null;
		if (!Utilities.isNull(customerInformation.getSourceOfFund())) {
			sourceOfFund = completingDataGlobalParam.getSourceOfFundList().stream()
					.filter(v -> v.getId().equals(Math.toIntExact(customerInformation.getSourceOfFund())))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}

		DefaultData mobilePrefix = null;
		if (!Utilities.isNull(businessProfile.getMobilePrefix())) {
			mobilePrefix = completingDataGlobalParam.getMobilePrefixList().stream()
					.filter(v -> v.getId().equals(Integer.valueOf(businessProfile.getMobilePrefix())))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}

		PersonalProfileData personalProfileData = new PersonalProfileData();
		personalProfileData.setSelfiePicture(userData.getSelfieFile());
		personalProfileData.setSelfieFileName(selfieFileName);
		personalProfileData.setIdCardPicture(userData.getIdCardFile());
		personalProfileData.setIdCardNumber(userData.getIdCardNumber());
		personalProfileData.setIdCardFileName(idCardFileName);
		personalProfileData.setSelfieKtpPicture(userData.getSelfieKtpFile());
		personalProfileData.setSignaturePicture(userData.getSignatureFile());
		personalProfileData.setSignatureFileName(signatureFileName);
		personalProfileData.setNpwpFile(legalInformation.getDocumentFile());
		personalProfileData.setNpwpFileName(npwpFileName);
		personalProfileData.setIdCardNumber(userData.getIdCardNumber());
		personalProfileData.setIdCardExpiredDate(expireDate);
		personalProfileData.setIdCardLifetime(GlobalConstants.DEFAULT_LIFETIME_DATE.equals(expireDate));
		personalProfileData.setSameAsDomicileAddress(GlobalConstants.STATUS_YES.equals(userData.getSameAddress()));
		personalProfileData.setAddress(userData.getKtpAddress());
		personalProfileData.setMaritalStatus(maritalStatus);
		personalProfileData.setProvince(province);
		personalProfileData.setCity(city);
		personalProfileData.setDistrict(district);
		personalProfileData.setSubDistrict(subDistrict);
		personalProfileData.setPostalCode(userData.getKtpPostalCode());
		personalProfileData.setPlaceOfBirth(placeOfBirth);
		personalProfileData.setPostalCode(userData.getKtpPostalCode());
		personalProfileData.setDateOfBirth(dateOfBirth);
		personalProfileData.setReligion(religion);
		personalProfileData.setEducation(education);
		personalProfileData.setOccupation(occupation);
		personalProfileData.setDomicileAddress(userData.getDomicileAddress());
		personalProfileData.setDomicileProvince(domicileProvince);
		personalProfileData.setDomicileCity(domicileCity);
		personalProfileData.setDomicileDistrict(domicileDistrict);
		personalProfileData.setDomicileSubDistrict(domicileSubDistrict);
		personalProfileData.setDomicilePostalCode(userData.getDomicilePostalCode());
		personalProfileData.setPoaFile(customerRole.getPoaFile());
		personalProfileData.setMotherMaidenName(userData.getMotherName());
		personalProfileData.setFieldOfWork(userData.getFieldOfWork());

		if (GlobalConstants.USER_CATEGORY_LENDER_ID.equals(userTypeIdentification) && Constants.NATIONALITY_INDONESIA.equals(userData.getNationality())) {
			personalProfileData.setAnnualIncome(annualIncome);
			personalProfileData.setSourceOfFund(sourceOfFund);
			personalProfileData.setOccupationAddress(businessProfile.getCompanyAddress());
			personalProfileData.setOccupationCity(occupationCity);
			personalProfileData.setOccupationPostalCode(businessProfile.getPostalCode());
			personalProfileData.setOccupationPhoneNumber(businessProfile.getLandLineNumber());
			personalProfileData.setOccupationMobilePrefix(mobilePrefix);
			personalProfileData.setMailingAddressStatus(businessProfile.getMailingAddressStatus());
		}

		return personalProfileData;
	}

	public PersonalProfileData generateBasicPersonalProfileData(UserData userData, CustomerInformation customerInformation, String accessToken) throws MalformedURLException {

		String expireDate = DateUtils.convertToString(userData.getIdCardExpired(), "yyyy-MM-dd");
		String dateOfBirth = DateUtils.convertToString(userData.getDateOfBirth(), "yyyy-MM-dd");

		DefaultData province = postalAreaService.findProvince(accessToken, userData.getKtpProvince());
		DefaultData city = postalAreaService.findCity(accessToken, userData.getKtpCity());
		DefaultData district = postalAreaService.findDistrict(accessToken, userData.getKtpDistrict());
		DefaultData subDistrict = postalAreaService.findVillage(accessToken, userData.getKtpVillage());

		DefaultData placeOfBirth = new DefaultData();
		if (!Constants.EXT_POB_ID.equals(userData.getPlaceOfBirth())) {
			placeOfBirth = postalAreaService.findCity(accessToken, userData.getPlaceOfBirth());
		}

		DefaultData domicileProvince = postalAreaService.findProvince(accessToken, userData.getDomicileProvince());
		DefaultData domicileCity = postalAreaService.findCity(accessToken, userData.getDomicileCity());
		DefaultData domicileDistrict = postalAreaService.findDistrict(accessToken, userData.getDomicileDistrict());
		DefaultData domicileSubDistrict = postalAreaService.findVillage(accessToken, userData.getDomicileVillage());

		LegalInformation legalInformation = legalInformationService.findByCustomerIdAndType(userData.getCustomerId(), Constants.LEGAL_INFO_NPWP);

		String npwpFileName;
		if (!Utilities.isNull(legalInformation.getDocumentFile()) && legalInformation.getDocumentFile().startsWith("http")) {
			URL url = new URL(legalInformation.getDocumentFile());
			npwpFileName = FilenameUtils.getName(url.getPath());
		} else {
			npwpFileName = legalInformation.getDocumentFile();
		}

		String signatureFileName;
		if (!Utilities.isNull(userData.getSignatureFile()) && userData.getSignatureFile().startsWith("http")) {
			URL url = new URL(userData.getSignatureFile());
			signatureFileName = FilenameUtils.getName(url.getPath());
		} else {
			signatureFileName = userData.getSignatureFile();
		}

		String idCardFileName;
		if (!Utilities.isNull(userData.getIdCardFile()) && userData.getIdCardFile().startsWith("http")) {
			URL url = new URL(userData.getIdCardFile());
			idCardFileName = FilenameUtils.getName(url.getPath());
		} else {
			idCardFileName = userData.getIdCardFile();
		}

		String selfieFileName;
		if (!Utilities.isNull(userData.getSelfieFile()) && userData.getSelfieFile().startsWith("http")) {
			URL url = new URL(userData.getSelfieFile());
			selfieFileName = FilenameUtils.getName(url.getPath());
		} else {
			selfieFileName = userData.getSelfieFile();
		}

		completingDataGlobalParam.getGlobalParam(
				GlobalConstants.GLOBAL_PARAM_MARITAL_STATUS,
				GlobalConstants.GLOBAL_PARAM_OCCUPATION,
				GlobalConstants.GLOBAL_PARAM_EDUCATION,
				GlobalConstants.GLOBAL_PARAM_RELIGION
		);

		DefaultData maritalStatus = null;
		if (!Utilities.isNull(userData.getMaritalStatus())) {
			maritalStatus = completingDataGlobalParam.getMaritalStatusList().stream()
					.filter(v -> v.getId().equals(Math.toIntExact(userData.getMaritalStatus())))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData occupation = null;
		if (!Utilities.isNull(userData.getOccupation())) {
			occupation = completingDataGlobalParam.getOccupationList().stream()
					.filter(v -> v.getId().equals(Math.toIntExact(userData.getOccupation())))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData education = null;
		if (!Utilities.isNull(userData.getEducation())) {
			education = completingDataGlobalParam.getEducationList().stream()
					.filter(v -> v.getId().equals(Math.toIntExact(userData.getEducation())))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData religion = null;
		if (!Utilities.isNull(userData.getReligion())) {
			religion = completingDataGlobalParam.getReligionList().stream()
					.filter(v -> v.getId().equals(Math.toIntExact(userData.getReligion())))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}

		PersonalProfileData personalProfileData = new PersonalProfileData();
		personalProfileData.setSelfiePicture(userData.getSelfieFile());
		personalProfileData.setSelfieFileName(selfieFileName);
		personalProfileData.setIdCardPicture(userData.getIdCardFile());
		personalProfileData.setIdCardNumber(userData.getIdCardNumber());
		personalProfileData.setIdCardFileName(idCardFileName);
		personalProfileData.setSelfieKtpPicture(userData.getSelfieKtpFile());
		personalProfileData.setSignaturePicture(userData.getSignatureFile());
		personalProfileData.setSignatureFileName(signatureFileName);
		personalProfileData.setNpwpFile(legalInformation.getDocumentFile());
		personalProfileData.setNpwpFileName(npwpFileName);
		personalProfileData.setIdCardNumber(userData.getIdCardNumber());
		personalProfileData.setIdCardExpiredDate(expireDate);
		personalProfileData.setIdCardLifetime(GlobalConstants.DEFAULT_LIFETIME_DATE.equals(expireDate));
		personalProfileData.setSameAsDomicileAddress(GlobalConstants.STATUS_YES.equals(userData.getSameAddress()));
		personalProfileData.setAddress(userData.getKtpAddress());
		personalProfileData.setMaritalStatus(maritalStatus);
		personalProfileData.setProvince(province);
		personalProfileData.setCity(city);
		personalProfileData.setDistrict(district);
		personalProfileData.setSubDistrict(subDistrict);
		personalProfileData.setPostalCode(userData.getKtpPostalCode());
		personalProfileData.setPlaceOfBirth(placeOfBirth);
		personalProfileData.setPostalCode(userData.getKtpPostalCode());
		personalProfileData.setDateOfBirth(dateOfBirth);
		personalProfileData.setReligion(religion);
		personalProfileData.setEducation(education);
		personalProfileData.setOccupation(occupation);
		personalProfileData.setDomicileAddress(userData.getDomicileAddress());
		personalProfileData.setDomicileProvince(domicileProvince);
		personalProfileData.setDomicileCity(domicileCity);
		personalProfileData.setDomicileDistrict(domicileDistrict);
		personalProfileData.setDomicileSubDistrict(domicileSubDistrict);
		personalProfileData.setDomicilePostalCode(userData.getDomicilePostalCode());
		personalProfileData.setMotherMaidenName(userData.getMotherName());
		personalProfileData.setFieldOfWork(userData.getFieldOfWork());

		if (!Utilities.isEmptyOrBlank(userData.getPlaceOfBirthExternal())) {
			personalProfileData.setPlaceOfBirth(new DefaultData(Math.toIntExact(Constants.EXT_POB_ID), userData.getPlaceOfBirthExternal()));
			personalProfileData.setPlaceOfBirthExternal(userData.getPlaceOfBirthExternal());
		}

		return personalProfileData;
	}
}
