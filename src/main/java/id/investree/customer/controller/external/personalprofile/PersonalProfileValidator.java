package id.investree.customer.controller.external.personalprofile;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.regex.RegexValidator;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.UserData;
import id.investree.customer.model.request.PersonalProfileIdentificationRequest;
import id.investree.customer.model.request.UpdateUserDataRequest;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.CustomerUtils;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Date;

@Component
public class PersonalProfileValidator {

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private CustomerUtils customerUtils;

	@Autowired
	private UserDataService userDataService;

	public String validatePersonalProfileIdentification(PersonalProfileIdentificationRequest request, Long customerId, Long nationality) throws ParseException {

		if (!Utilities.isEmptyOrBlank(request.getSelfiePicture()) &&
				!Utilities.isValidExtensionFile(request.getSelfiePicture(), GlobalConstants.FILE_EXT_JPG, GlobalConstants.FILE_EXT_JPEG, GlobalConstants.FILE_EXT_PNG)) {
			return messageUtils.dataNotValid("selfie file extension must be png, jpg or jpeg");
		}

		if (!Utilities.isEmptyOrBlank(request.getIdCardPicture()) &&
				!Utilities.isValidExtensionFile(request.getIdCardPicture(), GlobalConstants.FILE_EXT_JPG, GlobalConstants.FILE_EXT_JPEG, GlobalConstants.FILE_EXT_PNG, GlobalConstants.FILE_EXT_PDF)) {
			return messageUtils.dataNotValid("id card file extension must be pdf, png, jpg or jpeg");
		}

		if (Constants.NATIONALITY_INDONESIA.equals(nationality)) {
			if (!Utilities.isEmptyOrBlank(request.getIdCardNumber())
				&& request.getIdCardNumber().length() != Constants.IDENTITY_CARD_NUMBER_LENGTH) {
				return messageUtils.identityCardNumberLength();
			}
		}

		if (!Utilities.isEmptyOrBlank(request.getIdCardNumber())
			&& !RegexValidator.isNumeric(request.getIdCardNumber())) {
			return messageUtils.errorNumberOnly("id card");
		}

		if (!Utilities.isEmptyOrBlank(request.getIdCardNumber())) {
			UserData duplicateData = userDataService.findByIdCardNumber(request.getIdCardNumber());
			if (!Utilities.isNull(duplicateData) && !customerId.equals(duplicateData.getCustomerId())) {
				return messageUtils.identityCardNumberDuplicate();
			}
		}

		if (!Utilities.isNull(request.getIdCardExpiredDate())) {
			if (request.getIdCardExpiredDate().before(new Date())) {
				return messageUtils.identityCardExpiry();
			}
		}
		return null;
	}

	public String validate(UpdateUserDataRequest request, Long customerId, Long nationality) throws ParseException {

		if (Constants.NATIONALITY_INDONESIA.equals(nationality)) {
			if (!Utilities.isEmptyOrBlank(request.getIdCardNumber())
				&& request.getIdCardNumber().length() != Constants.IDENTITY_CARD_NUMBER_LENGTH) {
				return messageUtils.identityCardNumberLength();
			}
		}

		if (!Utilities.isEmptyOrBlank(request.getSelfiePicture()) &&
				!Utilities.isValidExtensionFile(request.getSelfiePicture(), GlobalConstants.FILE_EXT_JPG, GlobalConstants.FILE_EXT_JPEG, GlobalConstants.FILE_EXT_PNG)) {
			return messageUtils.dataNotValid("selfie file extension must be png, jpg or jpeg");
		}

		if (!Utilities.isEmptyOrBlank(request.getIdCardPicture()) &&
				!Utilities.isValidExtensionFile(request.getIdCardPicture(),
						GlobalConstants.FILE_EXT_JPG, GlobalConstants.FILE_EXT_JPEG, GlobalConstants.FILE_EXT_PNG, GlobalConstants.FILE_EXT_PDF)) {
			return messageUtils.dataNotValid("id card file extension must be pdf, png, jpg or jpeg");
		}

		if (!Utilities.isEmptyOrBlank(request.getIdCardNumber())
			&& !RegexValidator.isNumeric(request.getIdCardNumber())) {
			return messageUtils.errorNumberOnly("id card");
		}

		if (!Utilities.isEmptyOrBlank(request.getIdCardNumber())) {
			UserData duplicateData = userDataService.findByIdCardNumber(request.getIdCardNumber());
			if (!Utilities.isNull(duplicateData) && !customerId.equals(duplicateData.getCustomerId())) {
				return messageUtils.identityCardNumberDuplicate();
			}
		}

		if (!Utilities.isNull(request.getIdCardExpiredDate())) {
			if (request.getIdCardExpiredDate().before(new Date())) {
				return messageUtils.identityCardExpiry();
			}
		}

		if (!Utilities.isNull(request.getDateOfBirth()) && !customerUtils.isDateAfterSeventeenYearsAgo(request.getDateOfBirth())) {
			return messageUtils.dateMustBeOverSeventeenYears("dateOfBirth");
		}

		return null;
	}

}