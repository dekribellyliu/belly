package id.investree.customer.controller.external.referraldata;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.model.DefaultData;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.ReferralData;
import id.investree.customer.model.data.ReferralInformationData;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ReferralDataResponseGenerator {

	@Autowired
	private LoginDataService loginDataService;

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	public ReferralInformationData generateReferralInformation(ReferralData referralData, String accessToken) {

		ReferralInformationData referralInformationData = new ReferralInformationData();

		if (!Utilities.isNull(referralData)) {

			referralInformationData.setReferralId(referralData.getId());
			referralInformationData.setReferrerCode(referralData.getReferrerCode());

			if (!Utilities.isNull(referralData.getReferrerUserId())) {
				LoginDataResponse loginDataResponse = loginDataService.findByLoginId(accessToken, referralData.getReferrerUserId());
				referralInformationData.setReferrerName(loginDataResponse.getFullname());

			} else {
				referralInformationData.setReferrerName(null);
			}

			completingDataGlobalParam.getGlobalParam(GlobalConstants.GLOBAL_PARAM_KNOW_INVESTREE_FROM);

			referralInformationData.setExpiredDate(referralData.getReferralExpiredDate());
			if (!Utilities.isNull(referralData.getKnowInvestreeFrom())) {
				DefaultData kif = completingDataGlobalParam.getKifList().stream()
					.filter(v -> v.getId().equals(Math.toIntExact(referralData.getKnowInvestreeFrom())))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
				referralInformationData.setKnowInvestreeFrom(kif);
			}
		}

		return referralInformationData;
	}
}
