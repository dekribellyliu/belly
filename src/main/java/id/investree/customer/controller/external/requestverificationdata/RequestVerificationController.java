package id.investree.customer.controller.external.requestverificationdata;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.DefaultData;
import id.investree.core.model.TokenPayload;
import id.investree.core.model.legacy.LegacyModel;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.CustomerInformation;
import id.investree.customer.entity.CustomerRole;
import id.investree.customer.model.legacy.LegacyModelBuilder;
import id.investree.customer.model.response.CustomerRoleResponse;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.model.response.RegisterStatusResponse;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customernotification.CustomerNotificationService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.legacy.SyncLegacyService;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.utils.LocalMessageUtils;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Optional;

@RestController
@RequestMapping(value = "/request-verification-data", produces = MediaType.APPLICATION_JSON_VALUE)
public class RequestVerificationController extends BaseController {

	private LocalMessageUtils messageUtils;
	private TokenUtils tokenUtils;
	private RequestVerificationValidator validator;
	private LoginDataService loginDataService;
	private CustomerRoleService customerRoleService;
	private StoreDataGlobalParam completingDataGlobalParam;
	private CustomerNotificationService customerNotificationService;
	private CustomerInformationService customerInformationService;
	private SyncLegacyService syncLegacyService;

	public RequestVerificationController(LocalMessageUtils messageUtils,
										 TokenUtils tokenUtils,
										 RequestVerificationValidator validator,
										 LoginDataService loginDataService,
										 CustomerRoleService customerRoleService,
										 StoreDataGlobalParam completingDataGlobalParam,
										 CustomerNotificationService customerNotificationService,
										 CustomerInformationService customerInformationService,
										 SyncLegacyService syncLegacyService) {
		this.messageUtils = messageUtils;
		this.tokenUtils = tokenUtils;
		this.validator = validator;
		this.loginDataService = loginDataService;
		this.customerRoleService = customerRoleService;
		this.completingDataGlobalParam = completingDataGlobalParam;
		this.customerNotificationService = customerNotificationService;
		this.customerInformationService = customerInformationService;
		this.syncLegacyService = syncLegacyService;
	}

	@PutMapping
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> updateDataVerificationStatus(HttpServletRequest http,
																	   @RequestParam(value = "userRoleType", defaultValue = "1") Integer userTypeIdentification) {

		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(messageUtils.emptyToken());
		}

		String accessToken = http.getHeader("X-Investree-Token");
		LoginDataResponse loginData = loginDataService.findByLoginId(accessToken, tokenPayload.getUserId());
		if (Utilities.isNull(loginData)) {
			throw new DataNotFoundException("Login Data");
		} else if ((Utilities.isNull(loginData.getEmailStatus()) || !GlobalConstants.STATUS_YES.equals(loginData.getEmailStatus()))) {
			throw new AppException("Email is not verified");
		}

		validator.checkData(userTypeIdentification, Math.toIntExact(tokenPayload.getCustomerId()), accessToken);

		CustomerInformation customerInformation = Optional.ofNullable(customerInformationService
				.findById(Long.valueOf(tokenPayload.getCustomerId())))
				.orElseThrow(() -> new DataNotFoundException("Customer Information"));

		CustomerRole customerRole = customerRoleService.findByCustomerIdAndType(Long.valueOf(tokenPayload.getCustomerId()), userTypeIdentification);
		customerRole.setStatus(GlobalConstants.BORROWER_STATUS_PENDING_VERIFICATION);
		customerRole.setUpdatedBy(tokenPayload.getUserId());
		if (null == customerRole.getFillFinishAt()) {
			customerRole.setFillFinishAt(new Date());
		}
		CustomerRole saveData = customerRoleService.saveOrUpdate(customerRole);

		if (Utilities.isNull(saveData)) {
			throw new AppException(messageUtils.updateFailed());
		}

		completingDataGlobalParam.getGlobalParam(GlobalConstants.GLOBAL_PARAM_REGISTRATION_STATUS);

		RegisterStatusResponse registerStatusResponse = new RegisterStatusResponse();
		DefaultData registerStatus = null;
		if (!Utilities.isNull(customerRole.getStatus())) {
			registerStatus = completingDataGlobalParam.getRegistrationStatusList().stream()
					.filter(v -> v.getId().equals(Math.toIntExact(customerRole.getStatus())))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		if (customerRole.getType().equals(Math.toIntExact(GlobalConstants.BORROWER_STATUS_PENDING_VERIFICATION))) {
			registerStatusResponse.setLender(registerStatus);
		}
		if (customerRole.getType().equals(Math.toIntExact(GlobalConstants.USER_CATEGORY_BORROWER_ID))) {
			registerStatusResponse.setBorrower(registerStatus);
		}

		if (customerRole.getType().equals(Math.toIntExact(GlobalConstants.USER_CATEGORY_ANCHOR_ID))) {
			registerStatusResponse.setAnchor(registerStatus);
		}

		CustomerRoleResponse customerRoleResponse = new CustomerRoleResponse(saveData, registerStatusResponse);

		customerNotificationService.sendNotificationVerificationData(accessToken,
				customerInformation.getName(), customerInformation.getUserCategory());

		LegacyModel legacyModel = new LegacyModelBuilder()
				.setCustomerRole(customerRole)
				.setGlobalParam(completingDataGlobalParam)
				.setEmail(tokenPayload.getUserEmail())
				.build(Long.valueOf(tokenPayload.getCustomerId()));

		syncLegacyService.sync(accessToken, legacyModel);

		return abstractResponseHandler(customerRoleResponse).getResult(messageUtils.insertSuccess());
	}
}
