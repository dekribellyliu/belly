package id.investree.customer.controller.external.requestverificationdata;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.DefaultData;
import id.investree.core.utils.DateUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.controller.external.completing.CompletingDataFileCounterMapper;
import id.investree.customer.entity.*;
import id.investree.customer.model.data.*;
import id.investree.customer.service.bankinformation.BankInformationService;
import id.investree.customer.service.businessprofile.BusinessProfileService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.emergencycontact.EmergencyContactService;
import id.investree.customer.service.financialinformation.FinancialInformationService;
import id.investree.customer.service.legalinformation.LegalInformationService;
import id.investree.customer.service.mrinvarbank.MrInvarBankService;
import id.investree.customer.service.postalarea.PostalAreaService;
import id.investree.customer.service.shareholdersinformation.ShareholdersInformationService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.LocalMessageUtils;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class RequestVerificationValidator {

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private CustomerInformationService customerInformationService;

	@Autowired
	private BusinessProfileService businessProfileService;

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private LegalInformationService legalInformationService;

	@Autowired
	private EmergencyContactService emergencyContactService;

	@Autowired
	private FinancialInformationService financialInformationService;

	@Autowired
	private BankInformationService bankInformationService;

	@Autowired
	private ShareholdersInformationService shareholdersInformationService;

	@Autowired
	private CompletingDataFileCounterMapper counterMapper;

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	@Autowired
	private PostalAreaService postalAreaService;

	@Autowired
	private CustomerRoleService customerRoleService;

	@Autowired
	private MrInvarBankService mrInvarBankService;

	public void checkData(Integer userTypeIdentification, Integer customerId, String accessToken) {

		CustomerInformation customerData = customerInformationService.findById(Long.valueOf(customerId));
		UserData userData = userDataService.findByCustomerId(Long.valueOf(customerId));
		BusinessProfile businessData = businessProfileService.findByCustomerId(Long.valueOf(customerId));
		List<BankInformation> bankData = bankInformationService.findByCustomerId(Long.valueOf(customerId));
		List<ShareholdersInformation> shareholderData = shareholdersInformationService.findByCustomerIdAndNotDeleted(Long.valueOf(customerId));
		List<EmergencyContact> emergencyData = emergencyContactService.findByCustomerId(Long.valueOf(customerId));

		CustomerRole customerRole = customerRoleService.findByCustomerIdAndType(Long.valueOf(customerId), userTypeIdentification);

		if (Optional.ofNullable(customerRole).isPresent()) {
			if (GlobalConstants.BORROWER_STATUS_ACTIVE.equals(customerRole.getStatus())) {
				throw new AppException("Borrower with status active can not request data verification");
			} else if (GlobalConstants.BORROWER_STATUS_INACTIVE.equals(customerRole.getStatus())) {
				throw new AppException("Borrower with status inactive can not request data verification");
			} else if (GlobalConstants.BORROWER_STATUS_PENDING_VERIFICATION.equals(customerRole.getStatus())) {
				throw new AppException("Borrower with status pending verification can not request data verification");
			}
		}

		if (Utilities.isNull(businessData.getCustomerId())) {
			throw new AppException(messageUtils.emptySection("BusinessProfile"));
		}

		PersonalProfileData personalProfileData = checkPersonalProfileData(userData);
		CounterData personalProfileCounter = counterMapper.counterPersonalProfile(personalProfileData, customerData.getUserCategory(), userTypeIdentification);
		if (personalProfileCounter.getFilled() < personalProfileCounter.getTotal()) {
			throw new AppException(messageUtils.notCompletedSection("Personal Profile"));
		}

		if (!GlobalConstants.USER_CATEGORY_LENDER_ID.equals(userTypeIdentification)) {
			BusinessInfoData businessInfoData = checkBusinessProfile(businessData, customerData);
			CounterData businessInfoCounter = counterMapper.countBusinessProfile(businessInfoData, customerData);
			System.out.println(businessInfoCounter.getFilled());
			if (businessInfoCounter.getFilled() < businessInfoCounter.getTotal()) {
				throw new AppException(messageUtils.notCompletedSection("Business Profile"));
			}
		}

		if (Constants.NATIONALITY_INDONESIA.equals(userData.getNationality())) {
			List<LegalInformationData> legalInformationDataList = generateLegalInformation(customerData.getId());
			CounterData legalInformationCounter;
			if (GlobalConstants.CATEGORY_INSTITUSI_ID.equals(Integer.valueOf(customerData.getUserCategory()))) {
				legalInformationCounter = counterMapper.countLegalInformationInstitusional(legalInformationDataList);
			} else {
				legalInformationCounter = counterMapper.countLegalInformationIndividual(legalInformationDataList);
			}
			if (legalInformationCounter.getFilled() < legalInformationCounter.getTotal()) {
				throw new AppException(messageUtils.notCompletedSection("Legal Information"));
			}
		}

		List<BankInformationData> bankInfoData = generateBankInformations(bankData, Constants.BANK_TYPE_ALL, Long.valueOf(userTypeIdentification));
		CounterData bankInformationCounter = counterMapper.countBankInformation(bankInfoData);
		if (bankInformationCounter.getFilled() < bankInformationCounter.getTotal()) {
			throw new AppException(messageUtils.emptySection("BankInformation"));
		}

		List<FinancialInformation> financialInformationData = financialInformationService.findByCustomerId(userData.getCustomerId());
		CounterData financialInformationCounter = counterMapper
			.countFinancialStatementDocumentForVerificationData(financialInformationData, Integer.valueOf(customerData.getUserCategory()));
		CounterData financialEStatementCounter = counterMapper.countFinancialEStatementDocumentForVerificationData(financialInformationData,
			Integer.valueOf(customerData.getUserCategory()));
		if (GlobalConstants.CATEGORY_INSTITUSI_ID.equals(Integer.valueOf(customerData.getUserCategory()))) {
			if (financialEStatementCounter.getFilled() < financialEStatementCounter.getTotal()) {
				throw new AppException("Please provide the last " + financialEStatementCounter.getTotal() + " months of your E-statements");
			}
			if (financialInformationCounter.getFilled() < financialInformationCounter.getTotal()) {
				throw new AppException("Please provide the last " + financialInformationCounter.getTotal() + " years of your Financial Statements");
			}
		}

		if (GlobalConstants.CATEGORY_INDIVIDU_ID.equals(Integer.valueOf(customerData.getUserCategory()))) {
			List<EmergencyContactData> emergencyContactData = generateEmergencyContacts(emergencyData, accessToken);
			CounterData emergencyContactCounter = counterMapper.countEmergencyContact(emergencyContactData);
			if (emergencyContactCounter.getFilled() < emergencyContactCounter.getTotal()) {
				throw new AppException(messageUtils.emptySection("EmergencyContact"));
			}
		} else if (GlobalConstants.CATEGORY_INSTITUSI_ID.equals(Integer.valueOf(customerData.getUserCategory()))) {
			CounterData shareholdersInformationCounter = counterMapper.countShareHolderInformation(shareholderData);
			if (shareholdersInformationCounter.getFilled() < shareholdersInformationCounter.getTotal()) {
				throw new AppException(messageUtils.emptySection("ShareholdersInformation"));
			}
		}
	}

	private PersonalProfileData checkPersonalProfileData(UserData userData) {

		String expireDate = DateUtils.convertToString(userData.getIdCardExpired(), "yyyy-MM-dd");
		String dateOfBirth = DateUtils.convertToString(userData.getDateOfBirth(), "yyyy-MM-dd");

		DefaultData defaultData;
		PersonalProfileData personalProfileData = new PersonalProfileData();
		personalProfileData.setSelfiePicture(userData.getSelfieFile());
		personalProfileData.setIdCardPicture(userData.getIdCardFile());
		personalProfileData.setIdCardNumber(userData.getIdCardNumber());
		personalProfileData.setIdCardExpiredDate(expireDate);
		personalProfileData.setIdCardLifetime(GlobalConstants.DEFAULT_LIFETIME_DATE.equals(expireDate));
		personalProfileData.setSameAsDomicileAddress(GlobalConstants.STATUS_YES.equals(userData.getSameAddress()));
		personalProfileData.setAddress(userData.getKtpAddress());
		if (!Utilities.isNull(userData.getMaritalStatus())) {
			defaultData = new DefaultData();
			defaultData.setName(userData.getMaritalStatus().toString());
			personalProfileData.setMaritalStatus(defaultData);
		}
		if (!Utilities.isNull(userData.getKtpProvince())) {
			defaultData = new DefaultData();
			defaultData.setName(userData.getKtpProvince().toString());
			personalProfileData.setProvince(defaultData);
		}
		if (!Utilities.isNull(userData.getKtpCity())) {
			defaultData = new DefaultData();
			defaultData.setName(userData.getKtpCity().toString());
			personalProfileData.setCity(defaultData);
		}
		if (!Utilities.isNull(userData.getKtpDistrict())) {
			defaultData = new DefaultData();
			defaultData.setName(userData.getKtpDistrict().toString());
			personalProfileData.setDistrict(defaultData);
		}
		if (!Utilities.isNull(userData.getKtpVillage())) {
			defaultData = new DefaultData();
			defaultData.setName(userData.getKtpVillage().toString());
			personalProfileData.setSubDistrict(defaultData);
		}
		personalProfileData.setPostalCode(userData.getKtpPostalCode());
		if (!Utilities.isNull(userData.getPlaceOfBirth())) {
			defaultData = new DefaultData();
			defaultData.setName(userData.getPlaceOfBirth().toString());
			personalProfileData.setPlaceOfBirth(defaultData);
		}
		personalProfileData.setPostalCode(userData.getKtpPostalCode());
		personalProfileData.setDateOfBirth(dateOfBirth);
		if (!Utilities.isNull(userData.getReligion())) {
			defaultData = new DefaultData();
			defaultData.setName(userData.getReligion().toString());
			personalProfileData.setReligion(defaultData);
		}
		if (!Utilities.isNull(userData.getEducation())) {
			defaultData = new DefaultData();
			defaultData.setName(userData.getEducation().toString());
			personalProfileData.setEducation(defaultData);
		}
		if (!Utilities.isNull(userData.getOccupation())) {
			defaultData = new DefaultData();
			defaultData.setName(userData.getOccupation().toString());
			personalProfileData.setOccupation(defaultData);
		}
		personalProfileData.setDomicileAddress(userData.getDomicileAddress());
		if (!Utilities.isNull(userData.getDomicileProvince())) {
			defaultData = new DefaultData();
			defaultData.setName(userData.getDomicileProvince().toString());
			personalProfileData.setDomicileProvince(defaultData);
		}
		if (!Utilities.isNull(userData.getDomicileCity())) {
			defaultData = new DefaultData();
			defaultData.setName(userData.getDomicileCity().toString());
			personalProfileData.setDomicileCity(defaultData);
		}
		if (!Utilities.isNull(userData.getDomicileDistrict())) {
			defaultData = new DefaultData();
			defaultData.setName(userData.getDomicileDistrict().toString());
			personalProfileData.setDomicileDistrict(defaultData);
		}
		if (!Utilities.isNull(userData.getDomicileVillage())) {
			defaultData = new DefaultData();
			defaultData.setName(userData.getDomicileDistrict().toString());
			personalProfileData.setDomicileSubDistrict(defaultData);
		}
		personalProfileData.setDomicilePostalCode(userData.getDomicilePostalCode());

		return personalProfileData;
	}

	private BusinessInfoData checkBusinessProfile(BusinessProfile businessProfile, CustomerInformation customerInformation) {

		BusinessInfoData businessInfoData = new BusinessInfoData();
		DefaultData defaultData;
		if (GlobalConstants.CATEGORY_INSTITUSI_ID.toString().equals(customerInformation.getUserCategory())) {
			if (!Utilities.isNull(customerInformation.getLegalEntity())) {
				defaultData = new DefaultData();
				defaultData.setName(customerInformation.getLegalEntity().toString());
				businessInfoData.setLegalEntity(defaultData);
			}
		}
		businessInfoData.setCompanyName(customerInformation.getName());
		businessInfoData.setCompanyAddress(businessProfile.getCompanyAddress());
		businessInfoData.setCompanyDescription(businessProfile.getCompanyDescription());
		businessInfoData.setDateOfEstablishment(businessProfile.getDateOfEstablishment());
		businessInfoData.setNumberOfEmployee(businessProfile.getNumberOfEmployee());
		if (!Utilities.isNull(businessProfile.getProvince())) {
			defaultData = new DefaultData();
			defaultData.setName(businessProfile.getProvince().toString());
			businessInfoData.setProvince(defaultData);
		}
		if (!Utilities.isNull(businessProfile.getCity())) {
			defaultData = new DefaultData();
			defaultData.setName(businessProfile.getCity().toString());
			businessInfoData.setCity(defaultData);
		}
		if (!Utilities.isNull(customerInformation.getIndustry())) {
			Industry industryName = new Industry();
			industryName.setCode(customerInformation.getIndustry());
			businessInfoData.setIndustry(industryName);
		}
		if (!Utilities.isNull(businessProfile.getDistrict())) {
			defaultData = new DefaultData();
			defaultData.setName(businessProfile.getDistrict().toString());
			businessInfoData.setDistrict(defaultData);
		}
		if (!Utilities.isNull(businessProfile.getVillage())) {
			defaultData = new DefaultData();
			defaultData.setName(businessProfile.getDistrict().toString());
			businessInfoData.setVillage(defaultData);
		}
		businessInfoData.setPostalCode(businessProfile.getPostalCode());
		businessInfoData.setLandLineNumber(businessProfile.getLandLineNumber());
		return businessInfoData;
	}

	private List<LegalInformationData> generateLegalInformation(Long customerId) {
		List<LegalInformation> legalInformations = legalInformationService.findByCustomerId(customerId);
		List<LegalInformationData> legalInformationData = new ArrayList<>();
		for (LegalInformation legalInformation : legalInformations) {
			LegalInformationData data = new LegalInformationData();
			data.setDocumentType(generateLegalInfoFileType(legalInformation.getDocumentType()));
			data.setDocumentFile(legalInformation.getDocumentFile());
			data.setDocumentNumber(legalInformation.getDocumentNumber());
			data.setDocumentExpiredDate(legalInformation.getDocumentExpired());

			legalInformationData.add(data);
		}
		return legalInformationData;
	}

	private DefaultData generateLegalInfoFileType(Integer fileType) {
		if (Constants.LEGAL_INFO_NPWP.equals(fileType)) {
			return new DefaultData(Constants.LEGAL_INFO_NPWP, Constants.LEGAL_INFO_NPWP_NAME);

		} else if (Constants.LEGAL_INFO_SIUP.equals(fileType)) {
			return new DefaultData(Constants.LEGAL_INFO_SIUP, Constants.LEGAL_INFO_SIUP_NAME);

		} else if (Constants.LEGAL_INFO_AKTA_PENDIRIAN.equals(fileType)) {
			return new DefaultData(Constants.LEGAL_INFO_AKTA_PENDIRIAN, Constants.LEGAL_INFO_AKTA_PENDIRIAN_NAME);

		} else if (Constants.LEGAL_INFO_AKTA_TERBARU.equals(fileType)) {
			return new DefaultData(Constants.LEGAL_INFO_AKTA_TERBARU, Constants.LEGAL_INFO_AKTA_TERBARU_NAME);

		} else if (Constants.LEGAL_INFO_SK_MENKUHAM.equals(fileType)) {
			return new DefaultData(Constants.LEGAL_INFO_SK_MENKUHAM, Constants.LEGAL_INFO_SK_MENKUHAM_NAME);

		} else if (Constants.LEGAL_INFO_SK_TDP.equals(fileType)) {
			return new DefaultData(Constants.LEGAL_INFO_SK_TDP, Constants.LEGAL_INFO_SK_TDP_NAME);

		} else if (Constants.LEGAL_INFO_SK_SKDU.equals(fileType)) {
			return new DefaultData(Constants.LEGAL_INFO_SK_SKDU, Constants.LEGAL_INFO_SK_SKDU_NAME);
		}
		return null;
	}

	private List<BankInformationData> generateBankInformations(List<BankInformation> bankInformations, Integer type, Long userCategory) {
		List<BankInformationData> bankInformationData = new ArrayList<>();
		for (BankInformation bankInformation : bankInformations) {
			if (type.equals(bankInformation.getBankType()) || Constants.BANK_TYPE_ALL.equals(type)) {
				bankInformationData.add(generateBankInformation(bankInformation, userCategory));
			}
		}
		return bankInformationData;
	}

	private BankInformationData generateBankInformation(BankInformation bankInformation, Long userCategory) {
		boolean isDisbursement = GlobalConstants.STATUS_YES.equals(bankInformation.getUseAsDisbursement());
		String bankTypeValue = Constants.BANK_TYPE_VA.equals(bankInformation.getBankType()) ? "VA" : "Bank Account";
		DefaultData bankType = new DefaultData(bankInformation.getBankType(), bankTypeValue);
		DefaultData masterBank = new DefaultData();

		String bin = "";
		if (Constants.BANK_TYPE_VA.equals(bankInformation.getBankType())) {
			bin = Optional.ofNullable(mrInvarBankService.findByBankIdAndType(bankInformation.getMasterBankId(), userCategory, 3L))
				.map(MrInvarBank::getBankBin)
				.orElseThrow(() -> new DataNotFoundException("Invar Bank"));
		}

		BankInformationData data = new BankInformationData();
		data.setBankInformationId(bankInformation.getId());
		data.setBankType(bankType);
		masterBank.setName(bankInformation.getMasterBankId().toString());
		data.setMasterBank(masterBank);
		data.setBankAccountCoverFile(bankInformation.getBankAccountCoverFile());
		data.setBankAccountNumber(bin + "" + bankInformation.getBankAccountNumber());
		data.setBankAccountHolderName(bankInformation.getBankAccountHolderName());
		data.setUseAsDisbursement(isDisbursement);

		return data;
	}

	private List<FinancialDataDetail> generateFinancialData(List<FinancialInformation> data, Integer type) {
		return data.stream()
			.filter(it -> (type.equals(it.getStatementFileType()) || Constants.FINANCIAL_ALL_STATEMENT.equals(type)) && Utilities.isNull(it.getDeleteBy()))
			.map(this::convertToFinancialDetail).collect(Collectors.toList());
	}

	private FinancialDataDetail convertToFinancialDetail(FinancialInformation financialInformation) {
		FinancialDataDetail detail = new FinancialDataDetail();
		detail.setStatementId(financialInformation.getId());
		detail.setStatementFileAt(financialInformation.getStatementFileDate());
		detail.setStatementFileType(financialInformation.getStatementFileType());
		detail.setStatementUrl(financialInformation.getStatementUrl());
		return detail;
	}

	private List<EmergencyContactData> generateEmergencyContacts(List<EmergencyContact> emergencyContacts, String accessToken) {
		List<EmergencyContactData> emergencyContactData = new ArrayList<>();
		for (EmergencyContact emergencyContact : emergencyContacts) {
			if (Utilities.isNull(emergencyContact.getDeleteBy())) {
				emergencyContactData.add(generateEmergencyContact(emergencyContact, accessToken));
			}
		}
		return emergencyContactData;
	}

	private EmergencyContactData generateEmergencyContact(EmergencyContact emergencyContact, String accessToken) {

		EmergencyContactData emergencyContactData = new EmergencyContactData();
		emergencyContactData.setEmergencyId(emergencyContact.getId());

		completingDataGlobalParam.getGlobalParam(
			GlobalConstants.GLOBAL_PARAM_RELATIONSHIP,
			GlobalConstants.GLOBAL_PARAM_MOBILE_PREFIX
		);

		DefaultData relationship;
		if (!Utilities.isNull(emergencyContact.getRelationship())) {
			relationship = completingDataGlobalParam.getRelationshipList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(emergencyContact.getRelationship())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
			emergencyContactData.setRelationship(relationship);
		}
		emergencyContactData.setFullName(emergencyContact.getFullname());

		DefaultData mobilePrefix;
		if (!Utilities.isNull(emergencyContact.getMobilePrefix())) {
			mobilePrefix = completingDataGlobalParam.getMobilePrefixList().stream()
				.filter(v -> v.getId().equals(Integer.valueOf(Math.toIntExact(emergencyContact.getMobilePrefix()))))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
			emergencyContactData.setMobilePrefix(mobilePrefix);
		}

		emergencyContactData.setMobileNumber(emergencyContact.getMobileNumber());
		emergencyContactData.setEmailAddress(emergencyContact.getEmailAddress());
		emergencyContactData.setAddress(emergencyContact.getAddress());

		DefaultData province = null;
		if (!Utilities.isNull(emergencyContact.getProvince())) {
			province = postalAreaService.findProvince(accessToken, emergencyContact.getProvince());
			emergencyContactData.setProvince(province);
		}

		DefaultData city = null;
		if (!Utilities.isNull(emergencyContact.getCity())) {
			city = postalAreaService.findCity(accessToken, emergencyContact.getCity());
			emergencyContactData.setCity(city);
		}

		DefaultData distict = null;
		if (!Utilities.isNull(emergencyContact.getDistrict())) {
			distict = postalAreaService.findDistrict(accessToken, emergencyContact.getDistrict());
			emergencyContactData.setDistrict(distict);
		}

		DefaultData village = null;
		if (!Utilities.isNull(emergencyContact.getVillage())) {
			village = postalAreaService.findVillage(accessToken, emergencyContact.getVillage());
			emergencyContactData.setVillage(village);
		}

		emergencyContactData.setPostalCode(emergencyContact.getPostalCode());
		emergencyContactData.setIdentityCardUrl(emergencyContact.getIdentityCardUrl());
		emergencyContactData.setIdentityCardNumber(emergencyContact.getIdentityCardNumber());
		emergencyContactData.setIdentityExpiryDate(emergencyContact.getIdentityCardExpired());

		return emergencyContactData;
	}

}