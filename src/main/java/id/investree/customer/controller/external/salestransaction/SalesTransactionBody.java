package id.investree.customer.controller.external.salestransaction;

import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.SalesTransaction;
import id.investree.customer.model.request.SalesTransactionRequest;
import id.investree.customer.service.salestransaction.SalesTransactionService;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class SalesTransactionBody {

	@Autowired
	private SalesTransactionService salesTransactionService;

	@Autowired
	private LocalMessageUtils messageUtils;

	public List<SalesTransaction> setSalesTransaction(List<SalesTransactionRequest> requests, Long customerId, Long userId) {
		List<SalesTransaction> salesTransactionResponses = new ArrayList<>();
		requests.forEach(salesTransactionDataReq -> {

			SalesTransaction salesTransaction ;

			if(!Optional.ofNullable(salesTransactionDataReq.getSalesTransactionId()).isPresent()) {
				salesTransaction = new SalesTransaction();

				salesTransaction.setCreatedBy(userId);
				salesTransaction.setCreatedAt(new Date());
			} else {

				salesTransaction = Optional.ofNullable(salesTransactionService.findById(salesTransactionDataReq.getSalesTransactionId()))
						.orElseThrow(() -> new DataNotFoundException("Sales Transaction Id" + salesTransactionDataReq.getSalesTransactionId()));

				if (!salesTransactionDataReq.isDelete()) {
					salesTransaction.setUpdateBy(userId);
				} else {
					salesTransaction.setDeletedAt(new Date());
					salesTransaction.setDeletedBy(userId);
				}
			}

			salesTransaction.setCustomerId(customerId);
			salesTransaction.setAmount(salesTransactionDataReq.getAmount());
			salesTransaction.setTransaction(salesTransactionDataReq.getTransaction());
			salesTransaction.setDate(salesTransactionDataReq.getDates());

			SalesTransaction salesTransactionSaveData = Optional.ofNullable(salesTransactionService.saveOrUpdate(salesTransaction))
					.orElseThrow(() -> new AppException(messageUtils.insertFailed("Sales transaction")));

			if (Utilities.isNull(salesTransactionSaveData.getDeletedAt())) {
				salesTransactionResponses.add(salesTransactionSaveData);
			}

		});

		return salesTransactionResponses;
	}
}
