package id.investree.customer.controller.external.salestransaction;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.TokenPayload;
import id.investree.core.utils.TokenUtils;
import id.investree.customer.entity.SalesTransaction;
import id.investree.customer.service.salestransaction.SalesTransactionService;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Optional;

@RestController
@RequestMapping(value = "/sales-transaction", produces = MediaType.APPLICATION_JSON_VALUE)
public class SalesTransactionController extends BaseController {

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private SalesTransactionService salesTransactionService;

	@DeleteMapping
	public ResponseEntity<ResultResponse> delete(HttpServletRequest http, @RequestParam Long salesTransactionId) {

		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if(!Optional.ofNullable(tokenPayload).isPresent()) {
			throw new AppException(messageUtils.emptyToken());
		}

		SalesTransaction salesTransaction = Optional.ofNullable(salesTransactionService.findByIdAndCustomerId(salesTransactionId, Long.valueOf(tokenPayload.getCustomerId())))
			.orElseThrow(DataNotFoundException::new);

		salesTransaction.setDeletedBy(tokenPayload.getUserId());
		salesTransaction.setDeletedAt(new Date());
		SalesTransaction delete = salesTransactionService.saveOrUpdate(salesTransaction);
		if(!Optional.ofNullable(delete).isPresent()) {
			throw new AppException(messageUtils.deleteFailed());
		}

		return abstractResponseHandler(delete).getResult(messageUtils.deleteSuccess());
	}

}
