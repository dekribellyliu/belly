package id.investree.customer.controller.external.salestransaction;

import id.investree.core.utils.Utilities;
import id.investree.customer.entity.SalesTransaction;
import id.investree.customer.model.data.SalesTransactionData;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SalesTransactionResponseGenerator {

	public List<SalesTransactionData> generateSalesTransactionDataDetails(List<SalesTransaction> salesTransactions) {
		List<SalesTransactionData> salesTransactionData = new ArrayList<>();
		for (SalesTransaction salesTransaction : salesTransactions) {
			salesTransactionData.add(generateSalesTransactionDataDetail(salesTransaction));
		}
		return salesTransactionData;
	}

	public SalesTransactionData generateSalesTransactionDataDetail(SalesTransaction salesTransaction) {
		SalesTransactionData data = new SalesTransactionData();
		data.setSalesTransactionId(salesTransaction.getId());
		data.setAmount(salesTransaction.getAmount());
		data.setTransaction(salesTransaction.getTransaction());
		data.setDates(salesTransaction.getDate());
		Boolean isDelete = !Utilities.isNull(salesTransaction.getDeletedBy());
		data.setDelete(isDelete);
		return data;
	}
}
