package id.investree.customer.controller.external.salestransaction;

import id.investree.core.exception.AppException;
import id.investree.core.utils.Utilities;
import id.investree.customer.model.request.SalesTransactionRequest;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SalesTransactionValidator {

    @Autowired
    private LocalMessageUtils localMessageUtils;

    public void salesValidator(SalesTransactionRequest request) {

        if (Utilities.isNull(request.getAmount())) {
            throw new AppException(localMessageUtils.errorBlank("amount"));
        }

        if (Utilities.isNull(request.getTransaction())) {
            throw new AppException(localMessageUtils.errorBlank("transaction"));
        }

        if (Utilities.isNull(request.getDates()) ) {
            throw new AppException(localMessageUtils.errorBlank("date"));
        }
    }
}
