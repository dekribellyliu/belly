package id.investree.customer.controller.external.shareholdersinformation;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.TokenPayload;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.ShareholdersInformation;
import id.investree.customer.model.request.ShareholdersInformationRequest;
import id.investree.customer.service.shareholdersinformation.ShareholdersInformationService;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class ShareholderInformationBody {

	@Autowired
	private ShareholdersInformationValidator shareholdersInformationValidator;

	@Autowired
	private ShareholdersInformationService shareholdersInformationService;

	@Autowired
	private LocalMessageUtils messageUtils;

	@Transactional(rollbackFor = Exception.class)
	public List<ShareholdersInformation> setShareHolder(List<ShareholdersInformationRequest> requests,
														Long customerId, Integer userType, TokenPayload tokenPayload) {
		List<ShareholdersInformation> shareholdersInformationList = new ArrayList<>();
		for (ShareholdersInformationRequest request : requests) {
			request.setCustomerId(customerId);
			if (GlobalConstants.USER_TYPE_FRONTOFFICE_ID.equals(tokenPayload.getUserType())) {
				shareholdersInformationValidator.shareholderValidator(request);
			}

			ShareholdersInformation shareholderInformation;
			if (Utilities.isNull(request.getShareHolderId()) && !request.isDelete()) {
				shareholderInformation = new ShareholdersInformation();

				shareholderInformation.setCreatedBy(tokenPayload.getUserId());
			} else {
				shareholderInformation = Optional.ofNullable(shareholdersInformationService.findById(request.getShareHolderId()))
						.orElseThrow(() -> new DataNotFoundException("shareholder Id: " + request.getShareHolderId()));

				if(!request.isDelete()) {
					shareholderInformation.setUpdatedBy(tokenPayload.getUserId());
				} else {
					shareholderInformation.setDeletedBy(tokenPayload.getUserId());
					shareholderInformation.setDeletedAt(new Date());
				}
			}

			shareholderInformation.setCustomerId(customerId);
			shareholderInformation.setPosition(request.getPosition());
			shareholderInformation.setFullName(request.getFullName());
			shareholderInformation.setMobilePrefix(request.getMobilePrefix());
			shareholderInformation.setMobileNumber(request.getMobileNumber());
			shareholderInformation.setEmailAddress(request.getEmailAddress());
			shareholderInformation.setStockOwnership(request.getStockOwnership());
			shareholderInformation.setDob(request.getDob());
			shareholderInformation.setIdentificationCardUrl(request.getIdentificationCardUrl());
			shareholderInformation.setIdentificationCardNumber(request.getIdentificationCardNumber());
			shareholderInformation.setIdentificationCardExpiryDate(request.getIdentificationCardExpiryDate());
			shareholderInformation.setSelfieUrl(request.getSelfieUrl());
			if (GlobalConstants.USER_CATEGORY_BORROWER_ID.equals(userType)) {
				shareholderInformation.setTaxCardUrl(request.getTaxCardUrl());
				shareholderInformation.setTaxCardNumber(request.getTaxCardNumber());
				shareholderInformation.setIsLss(request.isLss() ? GlobalConstants.STATUS_YES : GlobalConstants.STATUS_NO);
				shareholderInformation.setIsPgs(request.isPgs() ? GlobalConstants.STATUS_YES : GlobalConstants.STATUS_NO);
				shareholderInformation.setIsTss(request.isTss() ? GlobalConstants.STATUS_YES : GlobalConstants.STATUS_NO);
			}

			if (GlobalConstants.USER_CATEGORY_BORROWER_ID.equals(userType) &&
					(!Utilities.isNull(request.isPgs()) && request.isPgs())) {
				shareholdersInformationValidator.personalGuaranteeValidator(request);

				shareholderInformation.setPgNumber(request.getPgNumber());
				shareholderInformation.setPgAmount(request.getPgAmount());
				shareholderInformation.setPgSignedDate(request.getPgSignedDate());
				shareholderInformation.setPgType(request.getPgType());
				shareholderInformation.setPgFile(request.getPgFile());
			}

			shareholderInformation.setApuPptDate(Utilities.isNull(request.getApuPptCheck().getCheckingDate()) ? null : request.getApuPptCheck().getCheckingDate());
			shareholderInformation.setApuPptFile(Utilities.isEmptyOrBlank(request.getApuPptCheck().getCheckingFile()) ? null : request.getApuPptCheck().getCheckingFile());
			shareholderInformation.setApuPptResult(Utilities.isNull(request.getApuPptCheck().getCheckingResult()) ? null : String.valueOf(Utilities.getIdFromDefaultData(request.getApuPptCheck().getCheckingResult())));


			ShareholdersInformation shareholdersInformationSave = Optional.ofNullable(shareholdersInformationService.saveOrUpdate(shareholderInformation))
					.orElseThrow(() -> new AppException(messageUtils.updateFailed()));

			if (Utilities.isNull(shareholdersInformationSave.getDeletedAt())) {
				shareholdersInformationList.add(shareholdersInformationSave);
			}
		}

		return shareholdersInformationList;
	}
}
