package id.investree.customer.controller.external.shareholdersinformation;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.model.DefaultData;
import id.investree.core.regex.RegexValidator;
import id.investree.core.utils.DateUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.ShareholdersInformation;
import id.investree.customer.model.data.ApuPptCheckData;
import id.investree.customer.model.data.ShareholdersInformationData;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ShareholderInformationResponseGenerator {

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	public ShareholdersInformationData generateShareHolderInformation(ShareholdersInformation data, String accessToken) {
		String expireDate = DateUtils.convertToString(data.getIdentificationCardExpiryDate(), "yyyy-MM-dd");

		completingDataGlobalParam.getGlobalParam(
			GlobalConstants.GLOBAL_PARAM_POSITION,
			GlobalConstants.GLOBAL_PARAM_MOBILE_PREFIX,
			Constants.GLOBAL_PARAM_APPT_RESULT
		);

		DefaultData position = null;
		if (!Utilities.isNull(data.getPosition())) {
			position = completingDataGlobalParam.getPositionList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(data.getPosition())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData mobilePrefix = null;
		if (!Utilities.isNull(data.getMobilePrefix())) {
			mobilePrefix = completingDataGlobalParam.getMobilePrefixList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(data.getMobilePrefix())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData checkingResult = null;
		if (!Utilities.isNull(data.getApuPptResult())) {
			if (RegexValidator.isNumeric(data.getApuPptResult())) {
				checkingResult = completingDataGlobalParam.getApptResultList().stream()
					.filter(v -> v.getId().equals(Integer.valueOf(data.getApuPptResult())))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
			}
		}
		ShareholdersInformationData shareholdersInformationData = new ShareholdersInformationData();
		shareholdersInformationData.setId(data.getId());
		shareholdersInformationData.setPosition(position);
		shareholdersInformationData.setFullName(data.getFullName());
		shareholdersInformationData.setMobilePrefix(mobilePrefix);
		shareholdersInformationData.setMobileNumber(data.getMobileNumber());
		shareholdersInformationData.setEmailAddress(data.getEmailAddress());
		shareholdersInformationData.setStockOwnership(data.getStockOwnership());
		shareholdersInformationData.setDob(data.getDob());
		shareholdersInformationData.setIdentificationCardUrl(data.getIdentificationCardUrl());
		shareholdersInformationData.setIdentificationCardNumber(data.getIdentificationCardNumber());
		shareholdersInformationData.setIdentificationCardExpiryDate(data.getIdentificationCardExpiryDate());
		shareholdersInformationData.setIdCardExpiryLifetime(GlobalConstants.DEFAULT_LIFETIME_DATE.equalsIgnoreCase(expireDate));
		shareholdersInformationData.setSelfieUrl(data.getSelfieUrl());
		shareholdersInformationData.setTaxCardUrl(data.getTaxCardUrl());
		shareholdersInformationData.setTaxCardNumber(data.getTaxCardNumber());
		shareholdersInformationData.setIsLss(GlobalConstants.STATUS_YES.equals(data.getIsLss()));
		shareholdersInformationData.setIsPgs(GlobalConstants.STATUS_YES.equals(data.getIsPgs()));
		shareholdersInformationData.setIsTss(GlobalConstants.STATUS_YES.equals(data.getIsTss()));

		ApuPptCheckData apuPptCheckData = new ApuPptCheckData();
		apuPptCheckData.setCheckingDate(data.getApuPptDate());
		apuPptCheckData.setCheckingFile(data.getApuPptFile());
		apuPptCheckData.setCheckingResult(checkingResult);

		shareholdersInformationData.setApuPptCheck(apuPptCheckData);

		if (GlobalConstants.STATUS_YES.equals(data.getIsPgs())) {
			shareholdersInformationData.setPgNumber(data.getPgNumber());
			shareholdersInformationData.setPgAmount(data.getPgAmount());
			shareholdersInformationData.setPgSignedDate(data.getPgSignedDate());
			shareholdersInformationData.setPgType(data.getPgType());
			shareholdersInformationData.setPgFile(data.getPgFile());
		}

		return shareholdersInformationData;
	}

	public List<ShareholdersInformationData> generateShareHolderInformations(List<ShareholdersInformation> shareHolderInformations, String accessToken) {
		List<ShareholdersInformationData> shareholdersInformationDataList = new ArrayList<>();
		shareHolderInformations.forEach(data -> {

			ShareholdersInformationData shareholdersInformationData = generateShareHolderInformation(data, accessToken);

			shareholdersInformationDataList.add(shareholdersInformationData);
		});
		return shareholdersInformationDataList;
	}
}
