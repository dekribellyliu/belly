package id.investree.customer.controller.external.shareholdersinformation;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.TokenPayload;
import id.investree.core.model.legacy.LegacyModel;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.CustomerInformation;
import id.investree.customer.entity.CustomerRole;
import id.investree.customer.entity.ShareholdersInformation;
import id.investree.customer.entity.UserData;
import id.investree.customer.model.data.ShareholdersInformationData;
import id.investree.customer.model.legacy.LegacyModelBuilder;
import id.investree.customer.model.request.ShareholderInformationBulkRequest;
import id.investree.customer.model.request.ShareholdersInformationRequest;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.legacy.SyncLegacyService;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.service.shareholdersinformation.ShareholdersInformationService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/shareholders-information", produces = MediaType.APPLICATION_JSON_VALUE)
public class ShareholdersInformationController extends BaseController {

	@Autowired
	private CustomerRoleService customerRoleService;

	@Autowired
	private ShareholdersInformationService shareholdersInformationService;

	@Autowired
	private CustomerInformationService customerInformationService;

	@Autowired
	private SyncLegacyService syncLegacyService;

	@Autowired
	private ShareholderInformationBody shareholderInformationBody;

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private ShareholdersInformationValidator shareholdersInformationValidator;

	@Autowired
	private ShareholderInformationResponseGenerator shareholderInformationResponseGenerator;

	@Autowired
	private StatusValidation statusValidation;

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private LoginDataService loginDataService;

	@Autowired
	private CustomerUtils customerUtils;

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	@PostMapping
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> add(@RequestBody ShareholdersInformationRequest request,
	                                          @RequestParam(value = "userRoleType", defaultValue = "1") Integer userType,
	                                          HttpServletRequest http) {

		shareholdersInformationValidator.shareholderValidator(request);

		String accessToken = http.getHeader("X-Investree-Token");

		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(messageUtils.emptyToken());
		}

		statusValidation.statusRestrictionValidation(tokenPayload, userType);

		CustomerInformation customerInformation = customerInformationService.findById(request.getCustomerId());
		if (Utilities.isNull(customerInformation)) {
			throw new DataNotFoundException();
		}

		Long custId;
		if (GlobalConstants.USER_TYPE_FRONTOFFICE_ID.equals(tokenPayload.getUserType())) {
			custId = Long.valueOf(tokenPayload.getCustomerId());
		} else {
			custId = customerInformation.getId();
		}

		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(custId))
			.orElseThrow(() -> new DataNotFoundException("User Data"));

		String emailAddress = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
			.map(LoginDataResponse::getEmailAddress)
			.orElseThrow(() -> new DataNotFoundException("Login Data"));

		ShareholdersInformation shareholderInformation = new ShareholdersInformation();
		shareholderInformation.setCustomerId(custId);
		shareholderInformation.setPosition(request.getPosition());
		shareholderInformation.setFullName(request.getFullName());
		shareholderInformation.setMobilePrefix(request.getMobilePrefix());
		shareholderInformation.setMobileNumber(request.getMobileNumber());
		shareholderInformation.setEmailAddress(request.getEmailAddress());
		shareholderInformation.setStockOwnership(request.getStockOwnership());
		shareholderInformation.setDob(request.getDob());
		shareholderInformation.setIdentificationCardUrl(request.getIdentificationCardUrl());
		shareholderInformation.setIdentificationCardNumber(request.getIdentificationCardNumber());
		shareholderInformation.setIdentificationCardExpiryDate(request.getIdentificationCardExpiryDate());
		shareholderInformation.setSelfieUrl(request.getSelfieUrl());
		if (GlobalConstants.USER_CATEGORY_BORROWER_ID.equals(userType)) {
			shareholderInformation.setTaxCardUrl(request.getTaxCardUrl());
			shareholderInformation.setTaxCardNumber(request.getTaxCardNumber());
			shareholderInformation.setIsLss(request.isLss() ? GlobalConstants.STATUS_YES : GlobalConstants.STATUS_NO);
			shareholderInformation.setIsPgs(request.isPgs() ? GlobalConstants.STATUS_YES : GlobalConstants.STATUS_NO);
			shareholderInformation.setIsTss(request.isTss() ? GlobalConstants.STATUS_YES : GlobalConstants.STATUS_NO);
		}
		shareholderInformation.setCreatedBy(tokenPayload.getUserId());
		ShareholdersInformation saveData = shareholdersInformationService.saveOrUpdate(shareholderInformation);
		if (Utilities.isNull(saveData)) {
			throw new AppException(messageUtils.insertFailed("Shareholders Information"));
		}

		List<ShareholdersInformation> allDataByCustomerId = shareholdersInformationService.findByCustomerIdAndNotDeleted(custId);
		LegacyModel legacyModel = new LegacyModelBuilder()
			.setShareholdersInformations(allDataByCustomerId)
			.setGlobalParam(completingDataGlobalParam)
			.setEmail(emailAddress)
			.build(custId);
		syncLegacyService.sync(accessToken, legacyModel);

		ShareholdersInformationData shareholdersInformationData = shareholderInformationResponseGenerator.generateShareHolderInformation(saveData, accessToken);

		return abstractResponseHandler(shareholdersInformationData).getResult(messageUtils.insertSuccess());
	}

	@DeleteMapping("{userRoleType}/{shareholderId}")
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> delete(@PathVariable("userRoleType") String userRoleType,
												 @PathVariable("shareholderId") Long shareholderId, HttpServletRequest http) {

		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(messageUtils.emptyToken());
		}

		Integer userTypeIdentification = customerUtils.getUserTypeIdentification(userRoleType);
		statusValidation.statusRestrictionValidation(tokenPayload, userTypeIdentification);

		ShareholdersInformation shareholderInformation = shareholdersInformationService.findByIdAndCustomerId(shareholderId, Long.valueOf(tokenPayload.getCustomerId()));
		if (Utilities.isNull(shareholderInformation)) {
			throw new DataNotFoundException();
		}
		shareholderInformation.setDeletedBy(tokenPayload.getUserId());
		shareholderInformation.setDeletedAt(new Date());
		ShareholdersInformation deleteData = shareholdersInformationService.saveOrUpdate(shareholderInformation);
		if (Utilities.isNull(deleteData)) {
			throw new AppException(messageUtils.deleteFailed());
		}

		List<ShareholdersInformation> allDataByCustomerId = shareholdersInformationService.findByCustomerIdAndDeleted(Long.valueOf(tokenPayload.getCustomerId()));
		LegacyModel legacyModel = new LegacyModelBuilder()
			.setShareholdersInformations(allDataByCustomerId)
			.setGlobalParam(completingDataGlobalParam)
			.setEmail(tokenPayload.getUserEmail())
			.build(Long.valueOf(tokenPayload.getCustomerId()));
		String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);
		syncLegacyService.sync(accessToken, legacyModel);

		return abstractResponseHandler(true).getResult(messageUtils.deleteSuccess());
	}

	@PutMapping("/{shareholderId}")
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> update(@PathVariable("shareholderId") Long shareholderId,
	                                             @RequestParam(value = "userRoleType", defaultValue = "1") Integer userRoleType,
	                                             @RequestBody ShareholdersInformationRequest request,
	                                             HttpServletRequest http) {

		shareholdersInformationValidator.shareholderValidator(request);

		String accessToken = http.getHeader("X-Investree-Token");

		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(messageUtils.emptyToken());
		}

		statusValidation.statusRestrictionValidation(tokenPayload, userRoleType);

		CustomerInformation customerInformation = customerInformationService.findById(request.getCustomerId());
		if (Utilities.isNull(customerInformation)) {
			throw new DataNotFoundException();
		}

		Long custId;
		if (GlobalConstants.USER_TYPE_FRONTOFFICE_ID.equals(tokenPayload.getUserType())) {
			custId = Long.valueOf(tokenPayload.getCustomerId());
		} else {
			custId = customerInformation.getId();
		}

		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(custId))
			.orElseThrow(() -> new DataNotFoundException("User Data"));

		String emailAddress = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
			.map(LoginDataResponse::getEmailAddress)
			.orElseThrow(() -> new DataNotFoundException("Login Data"));

		CustomerRole checkingStatus = Optional.ofNullable(customerRoleService.findByCustomerIdAndType(custId, userRoleType))
			.orElseThrow(DataNotFoundException::new);
		if (Constants.STATUS_ACTIVE.equals(checkingStatus.getStatus()) || Constants.STATUS_PENDING_VERIFICATION.equals(checkingStatus.getStatus()) || Constants.STATUS_INACTIVE.equals(checkingStatus.getStatus())) {
			throw new AppException(messageUtils.statusRestricted());
		}

		ShareholdersInformation shareholderInformation = shareholdersInformationService.findByIdAndCustomerId(shareholderId, custId);
		if (Utilities.isNull(shareholderInformation)) {
			throw new DataNotFoundException();
		}

		shareholderInformation.setPosition(request.getPosition());
		shareholderInformation.setFullName(request.getFullName());
		shareholderInformation.setMobilePrefix(request.getMobilePrefix());
		shareholderInformation.setMobileNumber(request.getMobileNumber());
		shareholderInformation.setEmailAddress(request.getEmailAddress());
		shareholderInformation.setStockOwnership(request.getStockOwnership());
		shareholderInformation.setDob(request.getDob());
		shareholderInformation.setIdentificationCardUrl(request.getIdentificationCardUrl());
		shareholderInformation.setIdentificationCardNumber(request.getIdentificationCardNumber());
		shareholderInformation.setIdentificationCardExpiryDate(request.getIdentificationCardExpiryDate());
		shareholderInformation.setSelfieUrl(request.getSelfieUrl());
		if (GlobalConstants.USER_CATEGORY_BORROWER_ID.equals(userRoleType)) {
			shareholderInformation.setTaxCardUrl(request.getTaxCardUrl());
			shareholderInformation.setTaxCardNumber(request.getTaxCardNumber());
			shareholderInformation.setIsLss(request.isLss() ? GlobalConstants.STATUS_YES : GlobalConstants.STATUS_NO);
			shareholderInformation.setIsPgs(request.isPgs() ? GlobalConstants.STATUS_YES : GlobalConstants.STATUS_NO);
			shareholderInformation.setIsTss(request.isTss() ? GlobalConstants.STATUS_YES : GlobalConstants.STATUS_NO);
		}
		shareholderInformation.setUpdatedBy(tokenPayload.getUserId());
		ShareholdersInformation saveData = shareholdersInformationService.saveOrUpdate(shareholderInformation);
		if (Utilities.isNull(saveData)) {
			throw new AppException(messageUtils.updateFailed());
		}

		List<ShareholdersInformation> allDataByCustomerId = shareholdersInformationService.findByCustomerIdAndNotDeleted(custId);
		LegacyModel legacyModel = new LegacyModelBuilder()
			.setShareholdersInformations(allDataByCustomerId)
			.setGlobalParam(completingDataGlobalParam)
			.setEmail(emailAddress)
			.build(custId);
		 syncLegacyService.sync(accessToken, legacyModel);

		ShareholdersInformationData shareholdersInformationData = shareholderInformationResponseGenerator.generateShareHolderInformation(saveData, accessToken);
		return abstractResponseHandler(shareholdersInformationData).getResult(messageUtils.updateSuccess());
	}

	@PostMapping("/save-all")
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> saveAll(@RequestBody ShareholderInformationBulkRequest requests,
	                                              @RequestParam(value = "userRoleType", defaultValue = "1") Integer userRoleType,
	                                              HttpServletRequest http) {
		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
			.orElseThrow(() -> new AppException(messageUtils.emptyToken()));

		String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);

		CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.findById(requests.getCustomerId()))
			.orElseThrow(DataNotFoundException::new);

		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(customerInformation.getId()))
			.orElseThrow(() -> new DataNotFoundException("User Data"));

		String emailAddress = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
			.map(LoginDataResponse::getEmailAddress)
			.orElseThrow(() -> new DataNotFoundException("Login Data"));

		//TODO: need to add userRoleType
		//statusValidation.statusRestrictionValidation(tokenPayload);

		requests.getData().forEach(data -> {
			data.setCustomerId(requests.getCustomerId());
		});

		shareholdersInformationValidator.backofficeBulkValidation(requests.getData());

		List<ShareholdersInformation> shareholdersInformationList = shareholderInformationBody
			.setShareHolder(requests.getData(), requests.getCustomerId(), userRoleType, tokenPayload);

		List<ShareholdersInformation> allDataByCustomerId = shareholdersInformationService.findAllByCustomerId(requests.getCustomerId());
		LegacyModel legacyModel = new LegacyModelBuilder()
				.setShareholdersInformations(allDataByCustomerId)
			.setGlobalParam(completingDataGlobalParam)
			.setEmail(emailAddress)
				.build(requests.getCustomerId());
		syncLegacyService.sync(accessToken, legacyModel);

		return abstractResponseHandler(shareholdersInformationList).getResult(messageUtils.updateSuccess());
	}

}
