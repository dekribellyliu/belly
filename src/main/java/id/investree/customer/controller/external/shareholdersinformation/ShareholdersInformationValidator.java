package id.investree.customer.controller.external.shareholdersinformation;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.regex.RegexValidator;
import id.investree.core.utils.DateUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.ShareholdersInformation;
import id.investree.customer.model.request.ShareholdersInformationRequest;
import id.investree.customer.service.shareholdersinformation.ShareholdersInformationService;
import id.investree.customer.utils.CustomerUtils;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ShareholdersInformationValidator {

	@Autowired
	private ShareholdersInformationService shareholdersInformationService;

	@Autowired
	private CustomerUtils customerUtils;

	@Autowired
	private LocalMessageUtils localMessageUtils;

	public void shareholderValidator(ShareholdersInformationRequest request) {

		if (Utilities.isNull(request.getCustomerId())) {
			throw new AppException(localMessageUtils.errorBlank("customer id"));
		}

		if (Utilities.isNull(request.getPosition())) {
			throw new AppException(localMessageUtils.errorBlank("position"));
		}

		if (Utilities.isNull(request.getFullName()) || Utilities.isEmptyOrBlank(request.getFullName())) {
			throw new AppException(localMessageUtils.errorBlank("full name"));
		} else if (!RegexValidator.isContainsAlphaSpaceOnly(request.getFullName())) {
			throw new AppException(localMessageUtils.nameOnlyAlphabet());
		}

		if (Utilities.isNull(request.getMobileNumber()) || Utilities.isEmptyOrBlank(request.getMobileNumber())) {
			throw new AppException(localMessageUtils.errorBlank("mobile number"));
		} else if (!RegexValidator.isNumeric(request.getMobileNumber())) {
			throw new AppException(localMessageUtils.errorNumberOnly("mobile number"));
		}

		if (Utilities.isNull(request.getEmailAddress()) || Utilities.isEmptyOrBlank(request.getEmailAddress())) {
			throw new AppException(localMessageUtils.errorBlank("email"));
		}

		Optional.ofNullable(request.getDob()).ifPresent(v -> {
			if (DateUtils.isDateMoreThanToday(v)) {
				throw new AppException(localMessageUtils.moreDate("Date of Birth"));
			}
			if (!customerUtils.isDateAfterSeventeenYearsAgo(v)) {
				throw new AppException(localMessageUtils.dateMustBeOverSeventeenYears("Date of Birth"));
			}
		});

		stockOwnershipValidation(request);

		if (Utilities.isNull(request.getIdentificationCardUrl())
			|| Utilities.isEmptyOrBlank(request.getIdentificationCardUrl())) {
			throw new AppException(localMessageUtils.errorBlank("identification url"));
		}

		if (!Utilities.isEmptyOrBlank(request.getIdentificationCardUrl())
			&& !Utilities.isValidExtensionFile(request.getIdentificationCardUrl(),
			GlobalConstants.FILE_EXT_PDF, GlobalConstants.FILE_EXT_PNG, GlobalConstants.FILE_EXT_JPEG, GlobalConstants.FILE_EXT_JPG)) {
			throw new AppException(localMessageUtils.dataNotValid("id card file extension must be pdf, png, jpg or jpeg"));
		}

		if (Utilities.isNull(request.getIdentificationCardNumber()) || Utilities.isEmptyOrBlank(request.getIdentificationCardNumber())) {
			throw new AppException(localMessageUtils.errorBlank("identification card number"));
		} else if (!RegexValidator.isKtpNumberValid(request.getIdentificationCardNumber())) {
			throw new AppException(localMessageUtils.identityCardNumberLength());
		}

		if (Utilities.isNull(request.getIdentificationCardExpiryDate())) {
			throw new AppException(localMessageUtils.errorBlank("identification card expiry date"));
		} else if (DateUtils.isDateLessThanToday(request.getIdentificationCardExpiryDate())) {
			throw new AppException(localMessageUtils.lessDate("Identification Expiry Date"));
		}

		if (!Utilities.isNull(request.getSelfieUrl()) && Utilities.isEmptyOrBlank(request.getSelfieUrl())) {
			throw new AppException(localMessageUtils.errorBlank("selfie url"));
		}

		if (Utilities.isNull(request.getTaxCardNumber()) || Utilities.isEmptyOrBlank(request.getTaxCardNumber())) {
			throw new AppException(localMessageUtils.errorBlank("tax card number"));
		} else if (!RegexValidator.isNpwpNumberValid(request.getTaxCardNumber())) {
			throw new AppException(localMessageUtils.errorNpwpNotValidDigit());
		}
	}

	void personalGuaranteeValidator(ShareholdersInformationRequest request) {
		if (!Utilities.isNull(request.getPgSignedDate())
			&& DateUtils.isDateMoreThanToday(request.getPgSignedDate())) {
			throw new AppException(localMessageUtils.moreDate("personal guarantee signed date"));
		}

		if (!Utilities.isEmptyOrBlank(request.getPgFile()) && !Utilities.isValidExtensionFile(request.getPgFile(), GlobalConstants.FILE_EXT_PDF)) {
			throw new AppException(localMessageUtils.dataNotValid("personal guarantee fileextension must be pdf"));
		}

	}

	public void backofficeBulkValidation(List<ShareholdersInformationRequest> requests) {
		float countOfStockOwnership = 0F;
		for (ShareholdersInformationRequest request : requests) {
			if (!request.isDelete()) {
				if (!Utilities.isNull(request.getDob()) && DateUtils.isDateMoreThanToday(request.getDob())) {
					throw new AppException(localMessageUtils.moreDate("Date of Birth"));
				}

				Optional.ofNullable(request.getDob()).ifPresent(v -> {
					if (DateUtils.isDateMoreThanToday(v)) {
						throw new AppException(localMessageUtils.moreDate("Date of Birth"));
					}
					if (!customerUtils.isDateAfterSeventeenYearsAgo(v)) {
						throw new AppException(localMessageUtils.dateMustBeOverSeventeenYears("Date of Birth"));
					}
				});

				if (!Utilities.isEmptyOrBlank(request.getIdentificationCardUrl())
					&& !Utilities.isValidExtensionFile(request.getIdentificationCardUrl(), GlobalConstants.FILE_EXT_PDF, GlobalConstants.FILE_EXT_PNG, GlobalConstants.FILE_EXT_JPEG, GlobalConstants.FILE_EXT_JPG)) {
					throw new AppException(localMessageUtils.dataNotValid("id card file extension must be pdf, png, jpg or jpeg"));
				}

				if (!Utilities.isEmptyOrBlank(request.getIdentificationCardNumber())
					&& !RegexValidator.isKtpNumberValid(request.getIdentificationCardNumber())) {
					throw new AppException(localMessageUtils.identityCardNumberLength());
				}

				if (!Utilities.isNull(request.getIdentificationCardExpiryDate())
					&& DateUtils.isDateLessThanToday(request.getIdentificationCardExpiryDate())) {
					throw new AppException(localMessageUtils.lessDate("Identification Expiry Date"));
				}

				if (!Utilities.isEmptyOrBlank(request.getTaxCardNumber())
					&& !RegexValidator.isNpwpNumberValid(request.getTaxCardNumber())) {
					throw new AppException(localMessageUtils.errorNpwpNotValidDigit());
				}

				if ((!Utilities.isNull(request.getStockOwnership()) && request.getStockOwnership() > 100)
					|| (!Utilities.isNull(request.getStockOwnership())
					&& (countOfStockOwnership + request.getStockOwnership()) > 100)) {
					throw new AppException(localMessageUtils.dataNotValid("stock ownership can't more than 100%"));
				}

				if (!Utilities.isNull(request.getStockOwnership())) {
					countOfStockOwnership = countOfStockOwnership + request.getStockOwnership();
				}

				if (!Utilities.isEmptyOrBlank(request.getApuPptCheck().getCheckingFile()) &&
					!Utilities.isValidExtensionFile(request.getApuPptCheck().getCheckingFile(), GlobalConstants.FILE_EXT_PDF)) {
					throw new AppException(localMessageUtils.dataNotValid("apu ppt checking file"));
				}

				if (!Utilities.isNull(request.getApuPptCheck().getCheckingDate())
					&& DateUtils.isDateMoreThanToday(request.getApuPptCheck().getCheckingDate())) {
					throw new AppException(localMessageUtils.moreDate("apu ppt checking date"));
				}
			}

		}
	}

	private void stockOwnershipValidation(ShareholdersInformationRequest request) {
		if (!Utilities.isNull(request.getShareHolderId()) && !request.isDelete()) {
			Float currentStock = Optional
				.ofNullable(shareholdersInformationService.findById(request.getShareHolderId()))
				.map(v -> v.getStockOwnership()).orElseThrow(() -> new DataNotFoundException("stock"));

			if (!Utilities.isNull(request.getStockOwnership()) && !currentStock.equals(request.getStockOwnership())) {
				checkStockOwnership(request.getStockOwnership(), request.getCustomerId(), request.getShareHolderId());
			}
		} else if (!Utilities.isNull(request.getStockOwnership()) && !request.isDelete()) {
			checkStockOwnership(request.getStockOwnership(), request.getCustomerId(), request.getShareHolderId());
		}
	}

	private void checkStockOwnership(Float requestStockOwnership, Long customerId, Long shareHolderId) {
		if (requestStockOwnership > 100) {
			throw new AppException(localMessageUtils.dataNotValid("stock ownership more than 100%"));
		} else {
			List<ShareholdersInformation> shareholdersInformations = shareholdersInformationService
				.findByCustomerIdAndNotDeleted(customerId);

			float countOfStockOwnership = 0F;
			for (ShareholdersInformation shareholdersInformation : shareholdersInformations) {
				if (!Utilities.isNull(shareholdersInformation.getStockOwnership())
					&& !shareholdersInformation.getId().equals(shareHolderId)) {
					countOfStockOwnership = countOfStockOwnership + shareholdersInformation.getStockOwnership();
				}
			}
			if ((countOfStockOwnership + requestStockOwnership) > 100) {
				throw new AppException(localMessageUtils.dataNotValid("stock ownership can't more than 100%"));
			}
		}
	}
}
