package id.investree.customer.controller.external.surveyinformation;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.DefaultData;
import id.investree.core.model.TokenPayload;
import id.investree.core.model.legacy.LegacyModel;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.SurveyInformation;
import id.investree.customer.entity.UserData;
import id.investree.customer.model.data.SurveyInformationData;
import id.investree.customer.model.legacy.LegacyModelBuilder;
import id.investree.customer.model.request.SurveyInformationDataRequest;
import id.investree.customer.model.request.SurveyInformationRequest;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.service.legacy.SyncLegacyService;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.service.surveyinformation.SurveyInformationService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.LocalMessageUtils;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/survey-information", produces = MediaType.APPLICATION_JSON_VALUE)
public class SurveyInformationController extends BaseController {

	@Autowired
	private SurveyInformationService service;

	@Autowired
	private SyncLegacyService syncLegacyService;

	@Autowired
	private LocalMessageUtils localMessageUtils;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private SurveyInformationValidator surveyValidator;

	@Autowired
	private SurveyInformationResponseGenerator surveyInformationResponseGenerator;

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private LoginDataService loginDataService;

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	@PostMapping
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> save(HttpServletRequest http, @RequestBody SurveyInformationRequest requests) {

		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(localMessageUtils.emptyToken());
		}

		String accessToken = http.getHeader("X-Investree-Token");

		Long customerId = requests.getCustomerId();

		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(customerId))
			.orElseThrow(() -> new DataNotFoundException("User Data"));

		String emailAddress = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
			.map(LoginDataResponse::getEmailAddress)
			.orElseThrow(() -> new DataNotFoundException("Login Data"));

		List<SurveyInformation> surveyInformations = new ArrayList<>();

		for (SurveyInformationDataRequest request: requests.getData()) {

			surveyValidator.surveyValidator(request);
			SurveyInformation surveyInformation;

			if(Utilities.isNull(request.getSurveyInformationId()) && !request.isDelete()) {
				surveyInformation = new SurveyInformation();

				surveyInformation.setCreatedBy(tokenPayload.getUserId());
				surveyInformation.setCreatedAt(new Date());
			} else {
				surveyInformation = service.findByIdAndCustomerId(request.getSurveyInformationId(), customerId);

				if (Utilities.isNull(surveyInformation)) {
					throw new DataNotFoundException("survey id = "+request.getSurveyInformationId());
				}

				if(!request.isDelete()) {
					surveyInformation.setUpdateBy(tokenPayload.getUserId());
				} else {
					surveyInformation.setDeletedBy(tokenPayload.getUserId());
					surveyInformation.setDeletedAt(new Date());
				}
			}

			surveyInformation.setCustomerId(customerId);
			surveyInformation.setLoginId(Utilities.isNull(request.getSurveyBy()) ? null : request.getSurveyBy());
			surveyInformation.setSurveyDate(Utilities.isNull(request.getSurveyAt()) ? null : request.getSurveyAt());
			surveyInformation.setBorrowerPosition(Utilities.isNull(request.getBorrowerPosition()) ? null : request.getBorrowerPosition());
			surveyInformation.setBorrowerName(Utilities.isEmptyOrBlank(request.getBorrowerName()) ? null : request.getBorrowerName());
			surveyInformation.setNumberOfEmployees(Utilities.isNull(request.getNumberOfEmployee()) ? null : request.getNumberOfEmployee());
			surveyInformation.setOfficeStatus(Utilities.isNull(request.getOfficeStatus()) ? null : request.getOfficeStatus());
			surveyInformation.setLengthOfStay(Utilities.isNull(request.getLengthOfStay()) ? null : request.getLengthOfStay());

			String surveyFilename = null;
			if (!Utilities.isNull(request.getFilename())) {
				final StringBuilder builder = new StringBuilder();
				for (int i = 0; i < request.getFilename().size(); i++) {
					builder.append(request.getFilename().get(i));
					if (i != request.getFilename().size() - 1) {
						builder.append(",");
					}
				}
				surveyFilename = builder.toString();
			}

			surveyInformation.setFilename(surveyFilename);
			surveyInformation.setResultDescription(Utilities.isEmptyOrBlank(request.getResultDescription()) ? null : request.getResultDescription());
			surveyInformation.setAddress(Utilities.isEmptyOrBlank(request.getAddress()) ? null : request.getAddress());

			surveyInformations.add(surveyInformation);
		}

		surveyInformations = service.saveAll(surveyInformations);
		if (Utilities.isNull(surveyInformations)) {
			throw new AppException(localMessageUtils.insertFailed("survey information"));
		}

		List<SurveyInformationData> surveyInformationDataList = surveyInformationResponseGenerator.generateSurveyInformations(surveyInformations, accessToken);

		SurveyInformation findLastUpdatedSurvey = service.findLastUpdateByCustomerId(customerId);
		DefaultData surveyOfficeStatus = new DefaultData();
		if (!Optional.ofNullable(findLastUpdatedSurvey).isPresent()) {
			findLastUpdatedSurvey = new SurveyInformation();
			findLastUpdatedSurvey.setDeletedAt(new Date());
		} else if (Optional.ofNullable(findLastUpdatedSurvey.getOfficeStatus()).isPresent()){
			surveyOfficeStatus = service.getSurveyOfficeStatus(findLastUpdatedSurvey.getOfficeStatus(), accessToken);
		}
		LegacyModel legacyModel = new LegacyModelBuilder()
				.setSurveyInformation(findLastUpdatedSurvey, completingDataGlobalParam)
			.setGlobalParam(completingDataGlobalParam)
			.setOfficeStatusName(surveyOfficeStatus.getName())
				.setEmail(emailAddress)
				.build(customerId);
		syncLegacyService.sync(accessToken, legacyModel);

		return abstractResponseHandler(surveyInformationDataList).getResult(localMessageUtils.insertSuccess(), HttpStatus.OK);
	}
}
