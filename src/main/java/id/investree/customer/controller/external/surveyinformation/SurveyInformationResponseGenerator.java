package id.investree.customer.controller.external.surveyinformation;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.model.DefaultData;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.SurveyInformation;
import id.investree.customer.model.data.SurveyInformationData;
import id.investree.customer.service.surveyinformation.SurveyInformationService;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class SurveyInformationResponseGenerator {

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	@Autowired
	private SurveyInformationService surveyInformationService;

	public List<SurveyInformationData> generateSurveyInformations(List<SurveyInformation> surveyInformations, String accessToken) {
		List<SurveyInformationData> surveyInformationDataList = new ArrayList<>();
		for (SurveyInformation surveyInformation : surveyInformations) {
			SurveyInformationData surveyInformationData = new SurveyInformationData();
			surveyInformationData.setSurveyInformationId(surveyInformation.getId());
			surveyInformationData.setSurveyBy(surveyInformation.getLoginId());
			surveyInformationData.setSurveyAt(surveyInformation.getSurveyDate());

			completingDataGlobalParam.getGlobalParam(GlobalConstants.GLOBAL_PARAM_POSITION);

			DefaultData position = null;
			if (!Utilities.isNull(surveyInformation.getBorrowerPosition())) {
				position = completingDataGlobalParam.getPositionList().stream()
					.filter(v -> v.getId().equals(Math.toIntExact(surveyInformation.getBorrowerPosition())))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
			}
			surveyInformationData.setBorrowerPosition(position);

			surveyInformationData.setBorrowerName(surveyInformation.getBorrowerName());
			surveyInformationData.setNumberOfEmployee(surveyInformation.getNumberOfEmployees());

			if (!Utilities.isNull(surveyInformation.getOfficeStatus())) {
				DefaultData surveyOfficeStatusResponse = surveyInformationService.getSurveyOfficeStatus(surveyInformation.getOfficeStatus(), accessToken);
				surveyInformationData.setOfficeStatus(surveyOfficeStatusResponse);
			}

			surveyInformationData.setLengthOfStay(surveyInformation.getLengthOfStay());

			List<String> surveyFilename = Optional.ofNullable(surveyInformation.getFilename())
				.map(v -> {
					List<String> filenames = new ArrayList<>();
					for (String fileName : v.split(",")) {
						filenames.add(fileName.replace(",", ""));
					}
					return filenames;
				})
				.orElse(new ArrayList<>());

			surveyInformationData.setFilename(surveyFilename);

			surveyInformationData.setResultDescription(surveyInformation.getResultDescription());
			surveyInformationData.setAddress(surveyInformation.getAddress());

			if (!Optional.ofNullable(surveyInformation.getDeletedAt()).isPresent()) {
				surveyInformationDataList.add(surveyInformationData);
			}
		}

		return surveyInformationDataList;
	}
}
