package id.investree.customer.controller.external.surveyinformation;

import id.investree.core.exception.AppException;
import id.investree.core.utils.Utilities;
import id.investree.customer.model.request.SurveyInformationDataRequest;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class SurveyInformationValidator {

	@Autowired
	private LocalMessageUtils localMessageUtils;

	public void surveyValidator(SurveyInformationDataRequest request) {

		if (!Utilities.isNull(request.getSurveyAt()) && request.getSurveyAt().compareTo(new Date()) > 0) {
			throw new AppException(localMessageUtils.moreDate("survey date"));
		}

		if (!request.getFilename().isEmpty()) {
			request.getFilename().forEach(filename -> {
				if (Utilities.isEmptyOrBlank(filename)) {
					throw new AppException(localMessageUtils.errorBlank("filename"));

				} else if (!Utilities.isEmptyOrBlank(filename) && Utilities.isValidExtensionFile(filename, Constants.FILE_EXT_GIF)) {
					throw new AppException(localMessageUtils.dataNotValid("filename extension must be doc, docx, xls, xlsx, pdf, png, jpg or jpeg"));
				}
			});
		}

		if (!Utilities.isNull(request.getNumberOfEmployee()) && request.getNumberOfEmployee() <= 0) {
			throw new AppException(localMessageUtils.dataNotValid("number of employee"));
		}

		if (!Utilities.isNull(request.getLengthOfStay()) && Float.parseFloat(request.getLengthOfStay()) <= 0) {
			throw new AppException(localMessageUtils.dataNotValid("length of stay"));
		}
	}
}
