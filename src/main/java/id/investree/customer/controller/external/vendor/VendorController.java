package id.investree.customer.controller.external.vendor;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.DefaultData;
import id.investree.core.model.TokenPayload;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.controller.external.bankinformation.BankInformationResponseGenerator;
import id.investree.customer.controller.external.completing.CompletingDataMapper;
import id.investree.customer.entity.*;
import id.investree.customer.model.data.BankInformationData;
import id.investree.customer.model.data.BankInformationParent;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.model.response.VendorDetailResponse;
import id.investree.customer.model.response.VendorResponse;
import id.investree.customer.service.bankinformation.BankInformationService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.loan.LoanService;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.service.picrole.PicRoleService;
import id.investree.customer.service.postalarea.PostalAreaService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.LocalMessageUtils;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/vendor", produces = MediaType.APPLICATION_JSON_VALUE)
public class VendorController extends BaseController {

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private CustomerInformationService customerInformationService;

	@Autowired
	private CustomerRoleService customerRoleService;

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private PicRoleService picRoleService;

	@Autowired
	private PostalAreaService postalAreaService;

	@Autowired
	private CompletingDataMapper mapper;

	@Autowired
	private BankInformationService bankInformationService;

	@Autowired
	private LoanService loanService;

	@Autowired
	private LoginDataService loginDataService;

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	@Autowired
	private BankInformationResponseGenerator bankInformationResponse;

	@GetMapping("/detail")
	public ResponseEntity<ResultResponse> getVendorDetail(
		@RequestParam(value = "customerId") Long customerId,
		@RequestParam(value = "anchorId") Long anchorId, HttpServletRequest http) {

		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
			.orElseThrow(() -> new AppException(messageUtils.emptyToken()));

		CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.findById(customerId))
			.orElseThrow(() -> new AppException(messageUtils.dataNotValid("customer Information")));

		CustomerRole customerRole = Optional.ofNullable(customerRoleService.findByCustomerIdAndType(customerId, GlobalConstants.USER_CATEGORY_BORROWER_ID))
			.orElseThrow(() -> new AppException(messageUtils.dataNotValid("customer Role")));

		List<PicRole> picRoles = Optional.ofNullable(picRoleService.findByCustomerRoleId(customerRole.getId()))
			.orElseThrow(() -> new AppException(messageUtils.dataNotValid("Pic Job Role")));

		if (picRoles.isEmpty()) {
			throw new DataNotFoundException("Pic Job Role");
		}

		UserData userData = Optional.ofNullable(userDataService.findByLoginId(picRoles.get(0).getLoginId()))
			.orElseThrow(() -> new AppException(messageUtils.dataNotValid("customer Role")));

		String accessToken = http.getHeader("X-Investree-Token");
		DefaultData city = postalAreaService.findCity(accessToken, userData.getKtpCity());

		PicRole picRole = picRoles.get(0);
		LoginDataResponse loginDataResponse = loginDataService.findByLoginId(http.getHeader(GlobalConstants.HEADER_TOKEN),
			picRole.getLoginId());

		List<BankInformation> bankInformations = bankInformationService.findByCustomerId(customerInformation.getId());
		List<BankInformationData> vaInfo = bankInformationResponse.generateBankInformations(
			bankInformations, Constants.BANK_TYPE_VA, accessToken, Long.valueOf(GlobalConstants.USER_CATEGORY_BORROWER_ID));
		BankInformationParent bankInformationParent = new BankInformationParent();
		bankInformationParent.setVa(vaInfo);

		DefaultData legalEntity = null;

		LoginDataResponse loginData = loginDataService.findByLoginId(accessToken, tokenPayload.getUserId());
		if (Utilities.isNull(loginData)) {
			throw new DataNotFoundException("Login Data");
		}

		completingDataGlobalParam.getGlobalParam(
			GlobalConstants.GLOBAL_PARAM_LEGAL_ENTITY,
			GlobalConstants.GLOBAL_PARAM_MOBILE_PREFIX
		);

		if (!Utilities.isNull(customerInformation.getLegalEntity())) {
			legalEntity = completingDataGlobalParam.getLegalList().stream()
				.filter(v -> v.getId().equals(customerInformation.getLegalEntity()))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData mobilePrefix = null;
		if (!Utilities.isNull(loginData.getMobilePrefix())) {
			mobilePrefix = completingDataGlobalParam.getMobilePrefixList().stream()
				.filter(v -> v.getId().equals(Integer.valueOf(loginData.getMobilePrefix())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}

		VendorDetailResponse response = new VendorDetailResponse();
		response.setAnchorId(anchorId);
		response.setLegalEntity(legalEntity);
		response.setContactName(loginDataResponse.getFullname());
		response.setEmail(loginDataResponse.getEmailAddress());
		response.setCompanyName(customerInformation.getName());
		response.setMobilePrefix(mobilePrefix);
		response.setMobileNumber(loginData.getPhoneNumber());
		response.setCity(city);
		response.setPoaFile(customerRole.getPoaFile());
		response.setBankInformation(bankInformationParent);

		return abstractResponseHandler(response).getResult(messageUtils.dataFetched(), HttpStatus.OK);
	}

	@GetMapping("/detail/{vendorId}/{anchorId}")
	public ResponseEntity<ResultResponse> getVendor(HttpServletRequest http,
	                                                @PathVariable Long vendorId,
	                                                @PathVariable Long anchorId) {
		String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);

		CustomerInformation vendorInformation = Optional.ofNullable(customerInformationService.findById(vendorId))
			.orElseThrow(() -> new DataNotFoundException("vendor user data"));

		CustomerInformation anchorInformation = Optional.ofNullable(customerInformationService.findById(anchorId))
			.orElseThrow(() -> new DataNotFoundException("anchor user data"));

		VendorResponse vendorResponse = new VendorResponse();
		vendorResponse.setAnchorName(anchorInformation.getName());
		vendorResponse.setVendorName(vendorInformation.getName());

		CustomerRole customerRole = Optional.ofNullable(customerRoleService.findByCustomerIdAndType(vendorId, GlobalConstants.USER_CATEGORY_BORROWER_ID))
			.orElseThrow(() -> new AppException(messageUtils.dataNotValid("customer Role")));

		List<PicRole> picRoles = Optional.ofNullable(picRoleService.findByCustomerRoleId(customerRole.getId()))
			.orElseThrow(() -> new AppException(messageUtils.dataNotValid("Pic Job Role")));

		UserData userData = Optional.ofNullable(userDataService.findByLoginId(picRoles.get(0).getLoginId()))
			.orElseThrow(() -> new AppException(messageUtils.dataNotValid("customer Role")));

		DefaultData city = postalAreaService.findCity(accessToken, userData.getKtpCity());
		vendorResponse.setVendorCity(city.getName());

		List<PicRole> roles = picRoleService.findByCustomerRoleId(customerRole.getId());
		if (roles.isEmpty()) {
			throw new DataNotFoundException("pic data");
		}

		PicRole picRole = roles.get(0);
		LoginDataResponse loginDataResponse = loginDataService.findByLoginId(http.getHeader(GlobalConstants.HEADER_TOKEN),
			picRole.getLoginId());

		vendorResponse.setPicEmail(loginDataResponse.getEmailAddress());
		vendorResponse.setPicName(loginDataResponse.getFullname());

		List<BankInformation> bankInformation = bankInformationService.findByCustomerId(vendorInformation.getId());
		List<BankInformationData> vaInfo = bankInformationResponse.generateBankInformations(
			bankInformation, Constants.BANK_TYPE_VA, accessToken, Long.valueOf(GlobalConstants.USER_CATEGORY_BORROWER_ID));
		vendorResponse.setVirtualAccounts(vaInfo);

		// TODO: [RIO] remove anything connect to loan-app
//		VendorApplicationResponse vendorListResponse = loanService.getVendor(vendorId, anchorId);
//		vendorResponse.setDigitalSignatureFile(vendorListResponse.getDsFilename());
//		vendorResponse.setDigitalSignatureStatus(vendorListResponse.getDsStatus());
//		vendorResponse.setRelationshipStart(vendorListResponse.getRelationshipStart());
//		vendorResponse.setAffiliate(vendorListResponse.getAffiliate());
//		vendorResponse.setStatus(vendorListResponse.getStatus());

		return abstractResponseHandler(vendorResponse).getResult(messageUtils.dataFetched());
	}
}
