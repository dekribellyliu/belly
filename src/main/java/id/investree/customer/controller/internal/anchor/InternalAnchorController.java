package id.investree.customer.controller.internal.anchor;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.TokenPayload;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.controller.internal.completing.InternalCompletingDataMapper;
import id.investree.customer.entity.*;
import id.investree.customer.model.data.CompletingInitialData;
import id.investree.customer.model.request.InternalAnchorRequest;
import id.investree.customer.model.request.PicAnchorRequest;
import id.investree.customer.model.request.PicRolesRequest;
import id.investree.customer.service.bankinformation.BankInformationService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.picrole.PicRoleService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/internal", produces = MediaType.APPLICATION_JSON_VALUE)
public class InternalAnchorController extends BaseController {

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private CustomerInformationService customerInformationService;

	@Autowired
	private BankInformationService bankInformationService;

	@Autowired
	private CustomerRoleService customerRoleService;

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private PicRoleService picRoleService;

	@Autowired
	private InternalCompletingDataMapper completingDataMapper;

	@PostMapping("/anchor")
	public ResponseEntity<ResultResponse> addAnchor(HttpServletRequest http,
	                                                @RequestBody InternalAnchorRequest request) {

		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
			.orElseThrow(() -> new AppException(messageUtils.emptyToken()));

		CustomerInformation customerInformation = new CustomerInformation();
		if (Optional.ofNullable(request.getCustomerId()).isPresent()) {
			customerInformation = customerInformationService.findById(request.getCustomerId());
		} else {
			customerInformation.setUserCategory(String.valueOf(GlobalConstants.CATEGORY_INSTITUSI_ID));
			customerInformation.setLegalEntity(request.getLegalEntity());
			customerInformation.setName(request.getCompanyName());
			customerInformation.setCreatedAt(new Date());
			customerInformation.setCreatedBy(tokenPayload.getUserId());
			customerInformation = customerInformationService.saveOrUpdate(customerInformation);
		}

		CustomerRole customerRole = customerRoleService.findByCustomerIdAndType(customerInformation.getId(),
			GlobalConstants.USER_CATEGORY_ANCHOR_ID);
		if (!Optional.ofNullable(customerRole).isPresent()) {
			customerRole = new CustomerRole();
			customerRole.setCreatedAt(new Date());
			customerRole.setCreatedBy(tokenPayload.getUserId());
			customerRole.setCustomerInformationId(customerInformation.getId());
			customerRole.setType(GlobalConstants.USER_CATEGORY_ANCHOR_ID);
			customerRole.setStatus(GlobalConstants.BORROWER_STATUS_ACTIVE);
			customerRole.setRegisteredAt(new Date());
			customerRole = Optional.ofNullable(customerRoleService.saveOrUpdate(customerRole)).orElseThrow(() -> new AppException(messageUtils.insertFailed("Customer Role")));

		}

		List<BankInformation> bankInformations = bankInformationService.findByCustomerIdAndIsAnchorDisbursement(request.getCustomerId());
		for (BankInformation bankInformation : bankInformations) {
			bankInformation.setIsAnchorDisbursement(GlobalConstants.STATUS_NO);
			bankInformationService.saveOrUpdate(bankInformation);
		}

		BankInformation bankInformation;
		if (Optional.ofNullable(request.getBankId()).isPresent()) {
			bankInformation = bankInformationService.getBankInformationById(request.getBankId(), customerInformation.getId());
			if (Utilities.isNull(bankInformation)) {
				throw new AppException(messageUtils.dataNotValid("Bank Information"));
			}
			CustomerRole customerRoleBorrower = Optional.ofNullable(customerRoleService.findByCustomerIdAndType(customerInformation.getId(),
				GlobalConstants.USER_CATEGORY_BORROWER_ID)).orElseThrow(() -> new DataNotFoundException("Customer Role"));
			bankInformation.setCustomerRoleId(customerRoleBorrower.getId());
		} else {
			bankInformation = new BankInformation();
			bankInformation.setCustomerId(customerInformation.getId());
			bankInformation.setCustomerRoleId(customerRole.getId());
			bankInformation.setBankType(Constants.BANK_TYPE_ACCOUNT);
			bankInformation.setMasterBankId(request.getMasterBankId());
			bankInformation.setBankAccountHolderName(request.getBankAccountHolderName());
			bankInformation.setBankAccountNumber(request.getBankAccountNumber());
		}
		bankInformation.setIsAnchorDisbursement(GlobalConstants.STATUS_YES);

		if (!Utilities.isEmptyOrBlank(bankInformation.getBankAccountHolderName())
			&& !Utilities.isEmptyOrBlank(bankInformation.getBankAccountNumber())
			&& !Utilities.isNull(bankInformation.getMasterBankId())) {
			bankInformationService.saveOrUpdate(bankInformation);
		}

		for (PicAnchorRequest pic : request.getPic()) {
			List<PicRole> roles = picRoleService.findByCustomerRoleIdAndLoginId(customerRole.getId(), pic.getLoginId());
			for (PicRole picRole : roles) {
				picRoleService.deleteById(picRole.getId());
			}

			UserData userData = new UserData();
			if (Constants.EXISTING_BORROWER.equals(pic.getPicFrom())) {
				userData = userDataService.findByLoginId(pic.getLoginId());
			} else {
				userData.setLoginDataId(pic.getLoginId());
			}
			userData.setCompanyPosition(pic.getPosition());
			userData.setIdCardFile(pic.getKtpFile());
			userData.setIdCardNumber(pic.getKtpNumber());
			userData.setKtpAddress(pic.getAddress());
			userData.setCustomerId(customerInformation.getId());
			userData.setNationality(pic.getNationality());
			userData = Optional.ofNullable(userDataService.saveOrUpdate(userData)).orElseThrow(() -> new AppException(messageUtils.insertFailed("User Data")));

			for (PicRolesRequest role : pic.getRoles()) {
				PicRole picRole = completingDataMapper.generatePicRole(role.getRole(), customerRole.getId(),
					pic.getLoginId(), tokenPayload.getUserId(), true);
				roles.add(picRole);
			}
			picRoleService.saveAllRole(roles);
		}

		CompletingInitialData response = new CompletingInitialData();
		response.setCustomerId(customerInformation.getId());
		return abstractResponseHandler(response).getResult(messageUtils.insertSuccess());
	}

	@PutMapping("/anchor")
	public ResponseEntity<ResultResponse> editAnchor(HttpServletRequest http, @RequestBody InternalAnchorRequest request) {

		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
			.orElseThrow(() -> new AppException(messageUtils.emptyToken()));

		CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.findById(request.getCustomerId())).orElseThrow(() -> new AppException(messageUtils.dataNotValid("customer Information")));
		customerInformation.setLegalEntity(request.getLegalEntity());
		customerInformation.setName(request.getCompanyName());
		customerInformationService.saveOrUpdate(customerInformation);

		CustomerRole customerRole = Optional.ofNullable(customerRoleService.findByCustomerIdAndType(customerInformation.getId(),
			GlobalConstants.USER_CATEGORY_ANCHOR_ID)).orElseThrow(() -> new AppException(messageUtils.dataNotValid("Customer Role")));

		customerRole.setUpdatedAt(new Date());
		Long status = request.getAnchorStatus() ? GlobalConstants.BORROWER_STATUS_ACTIVE : GlobalConstants.BORROWER_STATUS_INACTIVE;
		customerRole.setStatus(status);
		CustomerRole saveCustomerRole = Optional.ofNullable(customerRoleService.saveOrUpdate(customerRole))
			.orElseThrow(() -> new AppException(messageUtils.insertFailed("Customer Role")));

		BankInformation bankInformation;
		if (Optional.ofNullable(request.getBankId()).isPresent()) {
			bankInformation = bankInformationService.getBankInformationById(request.getBankId(), customerInformation.getId());
			if (Utilities.isNull(bankInformation)) {
				throw new AppException(messageUtils.dataNotValid("Bank Information"));
			}

			List<BankInformation> bankInformations = bankInformationService.findByCustomerIdAndIsAnchorDisbursement(request.getCustomerId());
			for (BankInformation bank : bankInformations) {
				bank.setIsAnchorDisbursement(GlobalConstants.STATUS_NO);
				bankInformationService.saveOrUpdate(bank);
			}

			CustomerRole customerRoleBorrower = Optional.ofNullable(customerRoleService.findByCustomerIdAndType(customerInformation.getId(),
				GlobalConstants.USER_CATEGORY_BORROWER_ID)).orElseThrow(() -> new DataNotFoundException("Customer Role"));
			bankInformation.setCustomerRoleId(customerRoleBorrower.getId());
		} else {
			bankInformation = new BankInformation();
			bankInformation.setCustomerId(customerInformation.getId());
			bankInformation.setCustomerRoleId(customerRole.getId());
			bankInformation.setBankType(Constants.BANK_TYPE_ACCOUNT);
			bankInformation.setMasterBankId(request.getMasterBankId());
			bankInformation.setBankAccountHolderName(request.getBankAccountHolderName());
			bankInformation.setBankAccountNumber(request.getBankAccountNumber());
		}
		bankInformation.setIsAnchorDisbursement(GlobalConstants.STATUS_YES);

		if (!Utilities.isEmptyOrBlank(bankInformation.getBankAccountHolderName())
			&& !Utilities.isEmptyOrBlank(bankInformation.getBankAccountNumber())
			&& !Utilities.isNull(bankInformation.getMasterBankId())) {
			bankInformationService.saveOrUpdate(bankInformation);
		}

		for (PicAnchorRequest pic : request.getPic()) {
			List<PicRole> roles = picRoleService.findByCustomerRoleIdAndLoginId(customerRole.getId(), pic.getLoginId());
			for (PicRole picRole : roles) {
				if (pic.getIsDelete()) {
					picRole.setDeletedBy(tokenPayload.getUserId());
					picRole.setDeletedAt(new Date());
					PicRole deletePicRole = picRoleService.saveOrUpdate(picRole);
				} else {
					picRoleService.deleteById(picRole.getId());
				}
			}

			UserData userData = new UserData();

			if (Optional.ofNullable(userDataService.findByLoginId(pic.getLoginId())).isPresent()) {
				userData = userDataService.findByLoginId(pic.getLoginId());
			} else if (Optional.ofNullable(pic.getChangeTo()).isPresent()) {
				userData = userDataService.findByLoginId(pic.getChangeTo().getLoginId());
			} else {
				userData.setLoginDataId(pic.getLoginId());
			}

			userData.setCompanyPosition(pic.getPosition());
			userData.setIdCardFile(pic.getKtpFile());
			userData.setIdCardNumber(pic.getKtpNumber());
			userData.setKtpAddress(pic.getAddress());
			userData.setCustomerId(customerInformation.getId());
			userData.setNationality(pic.getNationality());
			UserData saveUserData = Optional.ofNullable(userDataService.saveOrUpdate(userData))
				.orElseThrow(() -> new AppException(messageUtils.insertFailed("User Data")));

			Long statusPic = pic.getStatus() ? GlobalConstants.BORROWER_STATUS_ACTIVE : GlobalConstants.BORROWER_STATUS_INACTIVE;
			CustomerRole customerRolePic = Optional.ofNullable(customerRoleService.findByCustomerIdAndType(saveUserData.getCustomerId(), GlobalConstants.USER_CATEGORY_ANCHOR_ID))
				.map(v -> {
					v.setStatus(statusPic);
					v.setUpdatedAt(new Date());
					return v;
				}).orElseThrow(() -> new AppException(messageUtils.insertFailed("Customer role")));

			if (!Optional.ofNullable(customerRoleService.saveOrUpdate(customerRolePic)).isPresent()) {
				throw new AppException(messageUtils.updateFailed());
			}

			roles = new ArrayList<>();
			for (PicRolesRequest role : pic.getRoles()) {
				if (!pic.getIsDelete()) {
					PicRole picRole = completingDataMapper.generatePicRole(role.getRole(), customerRole.getId(),
						userData.getLoginDataId(), tokenPayload.getUserId(), pic.getStatus());
					roles.add(picRole);
				}
			}
			picRoleService.saveAllRole(roles);
		}

		CompletingInitialData response = new CompletingInitialData();
		response.setCustomerId(customerInformation.getId());
		return abstractResponseHandler(response).getResult(messageUtils.insertSuccess());
	}

}
