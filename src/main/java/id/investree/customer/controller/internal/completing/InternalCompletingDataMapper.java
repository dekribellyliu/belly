package id.investree.customer.controller.internal.completing;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.DefaultData;
import id.investree.core.utils.DateUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.*;
import id.investree.customer.model.data.BasicInformation;
import id.investree.customer.model.data.BusinessInfoData;
import id.investree.customer.model.data.PersonalProfileData;
import id.investree.customer.model.data.ProductPreferenceData;
import id.investree.customer.model.data.internal.InternalAdvancedInformation;
import id.investree.customer.model.response.InternalUserDataResponse;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.model.response.RegisterStatusResponse;
import id.investree.customer.service.businessprofile.BusinessProfileService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.legalinformation.LegalInformationService;
import id.investree.customer.service.postalarea.PostalAreaService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.CustomerUtils;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class InternalCompletingDataMapper {

	private BusinessProfileService businessProfileService;
	private CustomerInformationService customerInformationService;
	private UserDataService userDataService;
	private PostalAreaService postalAreaService;
	private CustomerRoleService customerRoleService;
	private LegalInformationService legalInformationService;
	private StoreDataGlobalParam completingDataGlobalParam;
	private CustomerUtils customerUtils;

	@Autowired
	public InternalCompletingDataMapper(BusinessProfileService businessProfileService,
	                                     CustomerInformationService customerInformationService,UserDataService userDataService,
	                                     PostalAreaService postalAreaService,CustomerRoleService customerRoleService,
	                                     LegalInformationService legalInformationService,StoreDataGlobalParam completingDataGlobalParam,
	                                     CustomerUtils customerUtils) {

		this.businessProfileService = businessProfileService;
		this.customerInformationService = customerInformationService;
		this.userDataService = userDataService;
		this.postalAreaService = postalAreaService;
		this.customerRoleService = customerRoleService;
		this.legalInformationService = legalInformationService;
		this.completingDataGlobalParam = completingDataGlobalParam;
		this.customerUtils = customerUtils;
	}


	public InternalUserDataResponse generateCompletingData(HttpServletRequest http, LoginDataResponse loginData, Long userId) throws MalformedURLException {
		UserData userData = userDataService.findByLoginId(userId);
		if (Utilities.isNull(userData)) {
			return null;
		}

		String accessToken = http.getHeader("X-Investree-Token");
		InternalUserDataResponse userDataResponse = new InternalUserDataResponse();
		userDataResponse.setLoginId(userData.getLoginDataId());
		userDataResponse.setBasicInfo(generateBasicInformation(loginData, userData));
		userDataResponse.setAdvancedInfo(generateAdvancedInformation(userData, accessToken));
		return userDataResponse;
	}

	private BasicInformation generateBasicInformation(LoginDataResponse loginData, UserData userData) {
		CustomerInformation customerInformation = customerInformationService.findById(userData.getCustomerId());
		if (Utilities.isNull(customerInformation)) {
			throw new DataNotFoundException("Customer Information");
		}

		Map<String, Long> params = new HashMap<>();

		DefaultData userCategory = null;
		if (!Utilities.isNull(customerInformation.getUserCategory())) {
			Integer userCategoryId = Integer.parseInt(customerInformation.getUserCategory());
			userCategory = new DefaultData(
				userCategoryId,
				GlobalConstants.CATEGORY_INDIVIDU_ID.equals(userCategoryId) ? "Individual" : "Institutional");
		}

		completingDataGlobalParam.getGlobalParam(
			GlobalConstants.GLOBAL_PARAM_NATIONALITY,
			GlobalConstants.GLOBAL_PARAM_POSITION,
			GlobalConstants.GLOBAL_PARAM_DIVISION,
			GlobalConstants.GLOBAL_PARAM_MARITAL_STATUS,
			GlobalConstants.GLOBAL_PARAM_OCCUPATION,
			GlobalConstants.GLOBAL_PARAM_EDUCATION,
			GlobalConstants.GLOBAL_PARAM_MOBILE_PREFIX,
			GlobalConstants.GLOBAL_PARAM_REGISTRATION_STATUS
		);

		DefaultData country = null;
		if (!Utilities.isNull(userData.getNationality())) {
			country = completingDataGlobalParam.getNationalityList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(userData.getNationality())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData position = null;
		if (!Utilities.isNull(userData.getCompanyPosition())) {
			position = completingDataGlobalParam.getPositionList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(userData.getCompanyPosition())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData division = null;
		if (!Utilities.isNull(userData.getCompanyDivision())) {
			division = completingDataGlobalParam.getDivisionList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(userData.getCompanyDivision())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData maritalStatus = null;
		if (!Utilities.isNull(userData.getMaritalStatus())) {
			maritalStatus = completingDataGlobalParam.getMaritalStatusList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(userData.getMaritalStatus())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData occupation = null;
		if (!Utilities.isNull(userData.getOccupation())) {
			occupation = completingDataGlobalParam.getOccupationList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(userData.getOccupation())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData education = null;
		if (!Utilities.isNull(userData.getEducation())) {
			education = completingDataGlobalParam.getEducationList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(userData.getEducation())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}

		DefaultData mobilePrefix = null;
		if (!Utilities.isNull(loginData.getMobilePrefix())) {
			mobilePrefix = completingDataGlobalParam.getMobilePrefixList().stream()
				.filter(v -> v.getId().equals(Integer.valueOf(loginData.getMobilePrefix())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}

		BasicInformation basicInformation = new BasicInformation();
		basicInformation.setCustomerId(customerInformation.getId());
		basicInformation.setUserCategory(userCategory);
		basicInformation.setProfilePicture(loginData.getPicture());
		basicInformation.setSalutation(loginData.getSalutation());
		basicInformation.setFullName(loginData.getFullname());
		basicInformation.setUsername(loginData.getUsername());
		basicInformation.setEmailAddress(loginData.getEmailAddress());
		basicInformation.setMobilePrefix(mobilePrefix);
		basicInformation.setMobileNumber(loginData.getPhoneNumber());
		basicInformation.setCustomerIdentificationNumber(loginData.getUserNumber());
		basicInformation.setAge(customerUtils.getAge(userData.getDateOfBirth()));
		basicInformation.setMaritalStatus(maritalStatus);
		basicInformation.setEducation(education);
		basicInformation.setOccupation(occupation);
		basicInformation.setPositionWithInstitution(position);
		basicInformation.setDivisionWithInstitution(division);
		basicInformation.setNationality(country);

		List<CustomerRole> customerRoles = customerRoleService.findAllByCustomerId(customerInformation.getId());
		List<Integer> customerRoleIds = customerRoles.stream().map(CustomerRole::getType).distinct().collect(Collectors.toList());
		List<DefaultData> roles = customerRoleIds.stream().map(roleId -> {
			if (GlobalConstants.USER_CATEGORY_BORROWER_ID.equals(roleId)) {
				return new DefaultData(roleId, GlobalConstants.USER_CATEGORY_BORROWER_NAME);
			} else if (GlobalConstants.USER_CATEGORY_LENDER_ID.equals(roleId)) {
				return new DefaultData(roleId, GlobalConstants.USER_CATEGORY_LENDER_NAME);
			} else if (GlobalConstants.USER_CATEGORY_ANCHOR_ID.equals(roleId)) {
				return new DefaultData(roleId, GlobalConstants.USER_CATEGORY_ANCHOR_NAME);
			} else if (GlobalConstants.USER_CATEGORY_PARTNER_ID.equals(roleId)) {
				return new DefaultData(roleId, GlobalConstants.USER_CATEGORY_PARTNER_NAME);
			}
			return null;
		}).filter(role -> !Utilities.isNull(role)).collect(Collectors.toList());

		completingDataGlobalParam.getGlobalParam(GlobalConstants.GLOBAL_PARAM_REGISTRATION_STATUS);

		RegisterStatusResponse registerStatusResponse = new RegisterStatusResponse();
		customerRoles.forEach(customerRoleStatus -> {
			DefaultData registerStatus = null;
			if (!Utilities.isNull(customerRoleStatus.getStatus())) {
				registerStatus = completingDataGlobalParam.getRegistrationStatusList().stream()
					.filter(v -> v.getId().equals(Math.toIntExact(customerRoleStatus.getStatus())))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
			}

			if (customerRoleStatus.getType().equals(GlobalConstants.USER_CATEGORY_LENDER_ID)) {
				registerStatusResponse.setLender(registerStatus);
			}

			if (customerRoleStatus.getType().equals(GlobalConstants.USER_CATEGORY_BORROWER_ID)) {
				registerStatusResponse.setBorrower(registerStatus);
			}

			if (customerRoleStatus.getType().equals(GlobalConstants.USER_CATEGORY_ANCHOR_ID)) {
				registerStatusResponse.setAnchor(registerStatus);
			}

			if (customerRoleStatus.getType().equals(GlobalConstants.USER_CATEGORY_PARTNER_ID)) {
				registerStatusResponse.setPartner(registerStatus);
			}
		});

		basicInformation.setUserRoleTypes(roles);
		basicInformation.setRegistrationStatus(registerStatusResponse);

		return basicInformation;
	}

	private InternalAdvancedInformation generateAdvancedInformation(UserData userData, String accessToken) throws MalformedURLException {
		CustomerInformation customerInformation = customerInformationService.findById(userData.getCustomerId());
		BusinessProfile businessProfile = businessProfileService.findByCustomerId(userData.getCustomerId());
		PersonalProfileData personalProfileData = generatePersonalProfileData(userData, accessToken);

		ProductPreferenceData productPreferenceData = new ProductPreferenceData();
		productPreferenceData.setProductPreferenceId(customerInformation.getProductPreference());

		InternalAdvancedInformation advancedInformation = new InternalAdvancedInformation();
		advancedInformation.setBusinessProfile(generateBusinessInfoData(businessProfile, customerInformation, accessToken));
		advancedInformation.setProductPreference(productPreferenceData);
		advancedInformation.setPersonalProfile(personalProfileData);

		return advancedInformation;
	}

	private BusinessInfoData generateBusinessInfoData(BusinessProfile businessProfile, CustomerInformation customerInformation, String accessToken) {
		DefaultData province = postalAreaService.findProvince(accessToken, businessProfile.getProvince());
		DefaultData city = postalAreaService.findCity(accessToken, businessProfile.getCity());
		DefaultData district = postalAreaService.findDistrict(accessToken, businessProfile.getDistrict());
		DefaultData subDistrict = postalAreaService.findVillage(accessToken, businessProfile.getVillage());

		completingDataGlobalParam.getGlobalParam(GlobalConstants.GLOBAL_PARAM_LEGAL_ENTITY);

		BusinessInfoData businessInfoData = new BusinessInfoData();
		if (GlobalConstants.CATEGORY_INSTITUSI_ID.toString().equals(customerInformation.getUserCategory())) {
			DefaultData legalEntity = null;
			if (!Utilities.isNull(customerInformation.getLegalEntity())) {
				legalEntity = completingDataGlobalParam.getLegalList().stream()
					.filter(v -> v.getId().equals(Math.toIntExact(customerInformation.getLegalEntity())))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
			}

			businessInfoData.setLegalEntity(legalEntity);
		}
		businessInfoData.setCompanyName(customerInformation.getName());
		businessInfoData.setCompanyAddress(businessProfile.getCompanyAddress());
		businessInfoData.setCompanyDescription(businessProfile.getCompanyDescription());
		businessInfoData.setDateOfEstablishment(businessProfile.getDateOfEstablishment());
		businessInfoData.setNumberOfEmployee(businessProfile.getNumberOfEmployee());
		businessInfoData.setProvince(province);
		businessInfoData.setCity(city);
		businessInfoData.setDistrict(district);
		businessInfoData.setVillage(subDistrict);
		businessInfoData.setPostalCode(businessProfile.getPostalCode());
		businessInfoData.setLandLineNumber(businessProfile.getLandLineNumber());
		return businessInfoData;
	}

	private PersonalProfileData generatePersonalProfileData(UserData userData, String accessToken) throws MalformedURLException {
		String expireDate = DateUtils.convertToString(userData.getIdCardExpired(), "yyyy-MM-dd");
		String dateOfBirth = DateUtils.convertToString(userData.getDateOfBirth(), "yyyy-MM-dd");

		DefaultData province = postalAreaService.findProvince(accessToken, userData.getKtpProvince());
		DefaultData city = postalAreaService.findCity(accessToken, userData.getKtpCity());
		DefaultData district = postalAreaService.findDistrict(accessToken, userData.getKtpDistrict());
		DefaultData subDistrict = postalAreaService.findVillage(accessToken, userData.getKtpVillage());
		DefaultData domicileProvince = postalAreaService.findProvince(accessToken, userData.getDomicileProvince());
		DefaultData domicileCity = postalAreaService.findCity(accessToken, userData.getDomicileCity());
		DefaultData domicileDistrict = postalAreaService.findDistrict(accessToken, userData.getDomicileDistrict());
		DefaultData domicileSubDistrict = postalAreaService.findVillage(accessToken, userData.getDomicileVillage());

		DefaultData placeOfBirth;
		if (!Utilities.isNull(userData.getPlaceOfBirth()) && Constants.EXT_POB_ID.equals(userData.getPlaceOfBirth())) {
			placeOfBirth = new DefaultData(Math.toIntExact(Constants.EXT_POB_ID), userData.getPlaceOfBirthExternal());
		} else {
			placeOfBirth = postalAreaService.findCity(accessToken, userData.getPlaceOfBirth());
		}

		LegalInformation legalInformation = legalInformationService.findByCustomerIdAndType(userData.getCustomerId(), Constants.LEGAL_INFO_NPWP);

		String npwpFileName;
		if (!Utilities.isNull(legalInformation.getDocumentFile()) && legalInformation.getDocumentFile().startsWith("http")) {
			URL url = new URL(legalInformation.getDocumentFile());
			npwpFileName = FilenameUtils.getName(url.getPath());
		} else {
			npwpFileName = legalInformation.getDocumentFile();
		}

		String signatureFileName;
		if (!Utilities.isNull(userData.getSignatureFile()) && userData.getSignatureFile().startsWith("http")) {
			URL url = new URL(userData.getSignatureFile());
			signatureFileName = FilenameUtils.getName(url.getPath());
		} else {
			signatureFileName = userData.getSignatureFile();
		}

		String idCardFileName;
		if (!Utilities.isNull(userData.getIdCardFile()) && userData.getIdCardFile().startsWith("http")) {
			URL url = new URL(userData.getIdCardFile());
			idCardFileName = FilenameUtils.getName(url.getPath());
		} else {
			idCardFileName = userData.getIdCardFile();
		}

		String selfieFileName;
		if (!Utilities.isNull(userData.getSelfieFile()) && userData.getSelfieFile().startsWith("http")) {
			URL url = new URL(userData.getSelfieFile());
			selfieFileName = FilenameUtils.getName(url.getPath());
		} else {
			selfieFileName = userData.getSelfieFile();
		}

		completingDataGlobalParam.getGlobalParam(
			GlobalConstants.GLOBAL_PARAM_OCCUPATION,
			GlobalConstants.GLOBAL_PARAM_EDUCATION,
			GlobalConstants.GLOBAL_PARAM_RELIGION
		);

		DefaultData occupation = null;
		if (!Utilities.isNull(userData.getOccupation())) {
			occupation = completingDataGlobalParam.getOccupationList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(userData.getOccupation())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData education = null;
		if (!Utilities.isNull(userData.getEducation())) {
			education = completingDataGlobalParam.getEducationList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(userData.getEducation())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		DefaultData religion = null;
		if (!Utilities.isNull(userData.getReligion())) {
			religion = completingDataGlobalParam.getReligionList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(userData.getReligion())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}

		PersonalProfileData personalProfileData = new PersonalProfileData();
		personalProfileData.setSelfiePicture(userData.getSelfieFile());
		personalProfileData.setSelfieFileName(selfieFileName);
		personalProfileData.setIdCardPicture(userData.getIdCardFile());
		personalProfileData.setIdCardNumber(userData.getIdCardNumber());
		personalProfileData.setIdCardFileName(idCardFileName);
		personalProfileData.setSignaturePicture(userData.getSignatureFile());
		personalProfileData.setSignatureFileName(signatureFileName);
		personalProfileData.setNpwpFile(legalInformation.getDocumentFile());
		personalProfileData.setNpwpFileName(npwpFileName);
		personalProfileData.setIdCardExpiredDate(expireDate);
		personalProfileData.setIdCardLifetime(GlobalConstants.DEFAULT_LIFETIME_DATE.equals(expireDate));
		personalProfileData.setSameAsDomicileAddress(GlobalConstants.STATUS_YES.equals(userData.getSameAddress()));
		personalProfileData.setAddress(userData.getKtpAddress());
		personalProfileData.setProvince(province);
		personalProfileData.setCity(city);
		personalProfileData.setDistrict(district);
		personalProfileData.setSubDistrict(subDistrict);
		personalProfileData.setPostalCode(userData.getKtpPostalCode());
		personalProfileData.setPlaceOfBirth(placeOfBirth);
		personalProfileData.setPostalCode(userData.getKtpPostalCode());
		personalProfileData.setDateOfBirth(dateOfBirth);
		personalProfileData.setReligion(religion);
		personalProfileData.setEducation(education);
		personalProfileData.setOccupation(occupation);
		personalProfileData.setDomicileAddress(userData.getDomicileAddress());
		personalProfileData.setDomicileProvince(domicileProvince);
		personalProfileData.setDomicileCity(domicileCity);
		personalProfileData.setDomicileDistrict(domicileDistrict);
		personalProfileData.setDomicileSubDistrict(domicileSubDistrict);
		return personalProfileData;
	}

	public PicRole generatePicRole(Integer role, Long customerRoleId, Long loginId, Long userId, Boolean status) {
		PicRole picRole = new PicRole();
		picRole.setCustomerRoleId(customerRoleId);
		picRole.setLoginId(loginId);
		picRole.setRole(role);
		picRole.setStatus(status ? GlobalConstants.STATUS_YES : GlobalConstants.STATUS_NO);
		picRole.setCreatedAt(new Date());
		picRole.setCreatedBy(userId);

		return picRole;
	}
}
