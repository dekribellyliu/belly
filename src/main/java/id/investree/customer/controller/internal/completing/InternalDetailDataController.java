package id.investree.customer.controller.internal.completing;


import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataExistException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.DefaultData;
import id.investree.core.model.TokenPayload;
import id.investree.core.model.legacy.LegacyModel;
import id.investree.core.utils.DateUtils;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.controller.external.bankinformation.BankInformationResponseGenerator;
import id.investree.customer.controller.external.businessprofile.BusinessProfileResponseGenerator;
import id.investree.customer.controller.external.customerinfo.CustomerInformationBody;
import id.investree.customer.controller.external.customerinfo.CustomerInformationValidator;
import id.investree.customer.controller.external.legalinformation.LegalInformationResponseGenerator;
import id.investree.customer.controller.external.partnershipinformation.PartnershipInformationBody;
import id.investree.customer.controller.external.partnershipinformation.PartnershipInformationResponseGenerator;
import id.investree.customer.controller.external.personalprofile.PersonalProfileResponseGenerator;
import id.investree.customer.controller.external.referraldata.ReferralDataResponseGenerator;
import id.investree.customer.entity.*;
import id.investree.customer.model.data.*;
import id.investree.customer.model.data.internal.InternalBorrowerData;
import id.investree.customer.model.enums.PartnershipTypeEnum;
import id.investree.customer.model.legacy.LegacyModelBuilder;
import id.investree.customer.model.request.*;
import id.investree.customer.model.response.EcommercePartnershipResponse;
import id.investree.customer.model.response.InternalUserDataResponse;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.model.response.RegisterStatusResponse;
import id.investree.customer.model.response.internal.PartnersResponse;
import id.investree.customer.service.bankinformation.BankInformationService;
import id.investree.customer.service.businessprofile.BusinessProfileService;
import id.investree.customer.service.ciflist.CifListService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customernotification.CustomerNotificationService;
import id.investree.customer.service.customerproductlist.CustomerProductListService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.financialtrend.FinancialTrendService;
import id.investree.customer.service.legacy.SyncLegacyService;
import id.investree.customer.service.legalinformation.LegalInformationService;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.service.mrinvarbank.MrInvarBankService;
import id.investree.customer.service.partnershipinformation.PartnershipInformationService;
import id.investree.customer.service.picrole.PicRoleService;
import id.investree.customer.service.postalarea.PostalAreaService;
import id.investree.customer.service.referraldata.ReferralDataService;
import id.investree.customer.service.salestransaction.SalesTransactionService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.LocalMessageUtils;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping(value = "/internal", produces = MediaType.APPLICATION_JSON_VALUE)
public class InternalDetailDataController extends BaseController {

	private TokenUtils tokenUtils;
	private LocalMessageUtils messageUtils;
	private BankInformationService bankInformationService;
	private CustomerInformationBody customerInformationBody;
	private LoginDataService loginDataService;
	private UserDataService userDataService;
	private CifListService cifListService;
	private CustomerProductListService customerProductListService;
	private BusinessProfileService businessProfileService;
	private SalesTransactionService salesTransactionService;
	private ReferralDataService referralDataService;
	private MrInvarBankService invarBankService;
	private CustomerInformationService customerInformationService;
	private CustomerNotificationService notificationService;
	private InternalCompletingDataMapper completingDataMapper;
	private ReferralDataResponseGenerator referralDataResponseGenerator;
	private CustomerInformationValidator customerInformationValidator;
	private CustomerRoleService customerRoleService;
	private LegalInformationService legalInformationService;
	private InternalDetailDataValidator internalDetailDataValidator;
	private FinancialTrendService financialTrendService;
	private InternalDetailDataFlow internalDetailDataFlow;
	private PicRoleService picRoleService;
	private PartnershipInformationService partnershipInformationService;
	private PostalAreaService postalAreaService;
	private SyncLegacyService syncLegacyService;
	private StoreDataGlobalParam completingDataGlobalParam;
	private BusinessProfileResponseGenerator businessProfileResponseGenerator;
	private BankInformationResponseGenerator bankInformationResponseGenerator;
	private LegalInformationResponseGenerator legalInformationResponseGenerator;
	private PersonalProfileResponseGenerator personalProfileResponseGenerator;
	private PartnershipInformationBody partnershipInformationBody;
	private PartnershipInformationResponseGenerator partnershipInformationResponseGenerator;

	@Autowired
	public InternalDetailDataController(TokenUtils tokenUtils, LocalMessageUtils messageUtils, BankInformationService bankInformationService,
										CustomerInformationBody customerInformationBody, LoginDataService loginDataService,
										UserDataService userDataService, CifListService cifListService, CustomerProductListService customerProductListService,
										BusinessProfileService businessProfileService, SalesTransactionService salesTransactionService,
										ReferralDataService referralDataService, MrInvarBankService invarBankService,
										CustomerInformationService customerInformationService, CustomerNotificationService notificationService,
										InternalCompletingDataMapper completingDataMapper, ReferralDataResponseGenerator referralDataResponseGenerator,
										CustomerInformationValidator customerInformationValidator, CustomerRoleService customerRoleService,
										LegalInformationService legalInformationService, InternalDetailDataValidator internalDetailDataValidator,
										FinancialTrendService financialTrendService, InternalDetailDataFlow internalDetailDataFlow,
										PicRoleService picRoleService, PartnershipInformationService partnershipInformationService,
										PostalAreaService postalAreaService, SyncLegacyService syncLegacyService,
										StoreDataGlobalParam completingDataGlobalParam, BusinessProfileResponseGenerator businessProfileResponseGenerator,
										BankInformationResponseGenerator bankInformationResponseGenerator, LegalInformationResponseGenerator legalInformationResponseGenerator,
										PersonalProfileResponseGenerator personalProfileResponseGenerator, PartnershipInformationBody partnershipInformationBody,
										PartnershipInformationResponseGenerator partnershipInformationResponseGenerator) {

		this.tokenUtils = tokenUtils;
		this.messageUtils = messageUtils;
		this.bankInformationService = bankInformationService;
		this.customerInformationBody = customerInformationBody;
		this.loginDataService = loginDataService;
		this.userDataService = userDataService;
		this.cifListService = cifListService;
		this.customerProductListService = customerProductListService;
		this.businessProfileService = businessProfileService;
		this.salesTransactionService = salesTransactionService;
		this.referralDataService = referralDataService;
		this.invarBankService = invarBankService;
		this.customerInformationService = customerInformationService;
		this.notificationService = notificationService;
		this.completingDataMapper = completingDataMapper;
		this.referralDataResponseGenerator = referralDataResponseGenerator;
		this.customerInformationValidator = customerInformationValidator;
		this.customerRoleService = customerRoleService;
		this.legalInformationService = legalInformationService;
		this.internalDetailDataValidator = internalDetailDataValidator;
		this.financialTrendService = financialTrendService;
		this.internalDetailDataFlow = internalDetailDataFlow;
		this.picRoleService = picRoleService;
		this.partnershipInformationService = partnershipInformationService;
		this.postalAreaService = postalAreaService;
		this.syncLegacyService = syncLegacyService;
		this.completingDataGlobalParam = completingDataGlobalParam;
		this.businessProfileResponseGenerator = businessProfileResponseGenerator;
		this.bankInformationResponseGenerator = bankInformationResponseGenerator;
		this.legalInformationResponseGenerator = legalInformationResponseGenerator;
		this.personalProfileResponseGenerator = personalProfileResponseGenerator;
		this.partnershipInformationBody = partnershipInformationBody;
		this.partnershipInformationResponseGenerator = partnershipInformationResponseGenerator;

	}


	@PostMapping("/completing-data/initial")
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> createBasicData(HttpServletRequest http,
														  @RequestBody InitialDataRequest request) {

		String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);
		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
				.orElseThrow(() -> new AppException(messageUtils.emptyToken()));

		completingDataGlobalParam.getGlobalParam(
				GlobalConstants.GLOBAL_PARAM_MOBILE_PREFIX,
				GlobalConstants.GLOBAL_PARAM_REGISTRATION_STATUS);

		boolean isUserTypeValid = customerInformationValidator.isUserTypeValid(request.getUserType());
		if (!isUserTypeValid) {
			throw new AppException("user type");
		}

		UserData userData = userDataService.findByLoginId(request.getLoginDataId());
		if (!request.isRegisterFromApi()
				&& (!GlobalConstants.USER_CATEGORY_ANCHOR_ID.equals(request.getUserType())
				&& !Utilities.isNull(userData))) {
			throw new DataExistException("for user id: " + request.getLoginDataId());
		}

		Integer productPreference = null;
		if (request.isRegisterFromRetailPartner()) {
			request.setUserCategory(String.valueOf(GlobalConstants.CATEGORY_INDIVIDU_ID));
			productPreference = GlobalConstants.USER_PREFERENCE_ALL;
		}

		if (GlobalConstants.USER_CATEGORY_PARTNER_ID.equals(request.getUserType()) && !Utilities.isNull(request.getEcommercePartnershipRequest())) {
			request.setUserCategory(String.valueOf(GlobalConstants.CATEGORY_INSTITUSI_ID));
		}

		CustomerInformation customerInformation = new CustomerInformation();
		Optional.ofNullable(request.getUserCategory()).ifPresent(customerInformation::setUserCategory);
		Optional.ofNullable(request.getNationality()).ifPresent(customerInformation::setNationality);
		Optional.ofNullable(request.getCompanyName()).ifPresent(customerInformation::setName);
		Optional.ofNullable(request.getBpdNumber()).ifPresent(customerInformation::setCustomerNumber);
		Optional.ofNullable(productPreference).ifPresent(customerInformation::setProductPreference);

		if (request.isRegisterFromApi()) {
			customerInformation.setProductPreference(GlobalConstants.USER_PREFERENCE_CONVENTIONAL);
			customerInformationBody.createUserInitial(customerInformation);
		}

		if (!Utilities.isNull(request.getCustomerId())) {
			customerInformation = customerInformationService.findById(request.getCustomerId());
		} else {
			if (GlobalConstants.USER_CATEGORY_ANCHOR_ID.equals(request.getUserType())) {
				customerInformation.setLegalEntity(request.getLegalEntity());
				customerInformation.setName(request.getCompanyName());
			}
			if (request.isRegisterFromApi()) {
				customerInformation.setUserCategory(request.getUserCategory());
			}
			customerInformation.setCreatedAt(Utilities.isNull(request.getCiCreatedAt()) ? new Date() : request.getCiCreatedAt());
			customerInformation.setCreatedBy(request.getLoginDataId());
			customerInformation = customerInformationService.saveOrUpdate(customerInformation);
		}
		Long customerId = customerInformation.getId();

		if (GlobalConstants.USER_CATEGORY_ANCHOR_ID.equals(request.getUserType())) {
			BankInformation bankInformation = new BankInformation();
			if (!Utilities.isNull(request.getBankId())) {
				bankInformation = bankInformationService.getBankInformationById(request.getBankId(), customerId);
				if (Utilities.isNull(bankInformation)) {
					throw new AppException(messageUtils.dataNotValid("Bank Information"));
				}
				bankInformation.setIsAnchorDisbursement(GlobalConstants.STATUS_YES);
			} else {
				bankInformation.setMasterBankId(request.getBankName());
				bankInformation.setBankAccountHolderName(request.getBankAccountHolder());
				bankInformation.setBankAccountNumber(request.getBankNumber());
				bankInformation.setIsAnchorDisbursement(GlobalConstants.STATUS_YES);
			}
			bankInformationService.saveOrUpdate(bankInformation);
		}

		userData = new UserData();
		if (GlobalConstants.USER_CATEGORY_ANCHOR_ID.equals(request.getUserType())) {
			userData.setCompanyPosition(request.getPosition());
			userData.setIdCardFile(request.getKtpFile());
			userData.setIdCardNumber(request.getKtpNumber());
			userData.setKtpAddress(request.getAddress());
		}
		userData.setLoginDataId(request.getLoginDataId());
		userData.setCustomerId(customerId);
		userData.setNationality(request.getNationality());
		Optional.ofNullable(request.getPosition()).ifPresent(userData::setCompanyPosition);
		Optional.ofNullable(request.getPlaceOfBirth()).ifPresent(userData::setPlaceOfBirth);
		Optional.ofNullable(request.getDateOfBirth()).ifPresent(userData::setDateOfBirth);
		Optional.ofNullable(request.getKtpNumber()).ifPresent(userData::setIdCardNumber);
		Optional.ofNullable(request.getKtpExpired()).ifPresent(userData::setIdCardExpired);
		Optional.ofNullable(request.getAddress()).ifPresent(userData::setKtpAddress);
		Optional.ofNullable(request.getPostalCode()).ifPresent(userData::setKtpPostalCode);
		Optional.ofNullable(request.getDomicileAddress()).ifPresent(userData::setDomicileAddress);
		Optional.ofNullable(request.getDomicileProvinceName()).ifPresent(userData::setDomicileProvinceName);
		Optional.ofNullable(request.getDomicileCityName()).ifPresent(userData::setDomicileCityName);
		Optional.ofNullable(request.getDomicileDistrictName()).ifPresent(userData::setDomicileDistrictName);
		Optional.ofNullable(request.getDomicileSubDistrictName()).ifPresent(userData::setDomicileVillageName);
		Optional.ofNullable(request.getDomicilePostalCode()).ifPresent(userData::setDomicilePostalCode);

		if (!Utilities.isEmptyOrBlank(request.getPlaceOfBirthExternal())) {
			userData.setPlaceOfBirth(Constants.EXT_POB_ID);
			userData.setPlaceOfBirthExternal(request.getPlaceOfBirthExternal());
		}

		if (request.isRegisterFromApi()) {
			Optional.ofNullable(request.getProvinceName()).ifPresent(userData::setPartnerProvince);
			Optional.ofNullable(request.getCityName()).ifPresent(userData::setPartnerCity);
			Optional.ofNullable(request.getDistrictName()).ifPresent(userData::setPartnerDistrict);
			Optional.ofNullable(request.getSubDistrictName()).ifPresent(userData::setPartnerVillage);
			Optional.ofNullable(request.getPostalCode()).ifPresent(userData::setPartnerPostalCode);
		}

		userData = Optional.ofNullable(userDataService.saveOrUpdate(userData)).orElseThrow(() ->
				new AppException(messageUtils.insertFailed("User Data")));

		List<Integer> listRoles = new ArrayList<>();
		CustomerRole customerRole = customerRoleService.findByCustomerIdAndType(customerId, request.getUserType());
		if (!Optional.ofNullable(customerRole).isPresent()) {
			Long customerStatus;
			Date registerDate = null;
			if (request.isRegisterFromApi() && PartnershipTypeEnum.ECOMMERCE_PARTNERSHIP.equals(request.getPartnershipType())) {
				customerStatus = GlobalConstants.BORROWER_STATUS_ACTIVE;
				registerDate = new Date();
			} else if (request.isRegisterFromRetailPartner() ||
					(request.isRegisterFromApi() && PartnershipTypeEnum.BUSINESS_PARTNERSHIP.equals(request.getPartnershipType()))) {
				customerStatus = GlobalConstants.BORROWER_STATUS_REGISTERED;
				registerDate = new Date();
			} else {
				customerStatus = Constants.STATUS_PRE_REGISTERED;
			}

			customerRole = new CustomerRole();
			customerRole.setCreatedAt(new Date());
			customerRole.setCreatedBy(request.getLoginDataId());
			customerRole.setCustomerInformationId(customerInformation.getId());
			customerRole.setType(request.getUserType());
			customerRole.setStatus(customerStatus);
			customerRole.setRegisteredAt(registerDate);
			customerRole = customerRoleService.saveOrUpdate(customerRole);

			if (Utilities.isNull(customerRole)) {
				throw new AppException(messageUtils.insertFailed("Customer Role"));
			}
			listRoles.add(customerRole.getType());
		}
		Long customerRoleId = customerRole.getId();

		if (!Utilities.isNull(request.getRoles())) {
			List<PicRole> roles = new ArrayList<>();
			for (PicRolesRequest role : request.getRoles()) {
				PicRole picRole = completingDataMapper.generatePicRole(role.getRole(), customerRoleId, request.getLoginDataId(), tokenPayload.getUserId(), true);
				roles.add(picRole);
			}
			picRoleService.saveAllRole(roles);
		} else {
			PicRole picRole = completingDataMapper.generatePicRole(null, customerRoleId, request.getLoginDataId(), tokenPayload.getUserId(), true);
			picRoleService.saveOrUpdate(picRole);
		}

		PartnershipInformation partnershipInformation = null;
		List<Integer> productSelections = null;
		Long customerProduct = null;
		if (request.isRegisterFromRetailPartner()) {
			EcommercePartnershipResponse response = partnershipInformationService.findEcommercePartnershipByCustomerId(
				Long.valueOf(tokenPayload.getCustomerId()),accessToken);
			PartnershipInformationRequest partnershipInformationRequest = request.getPartnershipInformationRequest();
			partnershipInformationRequest.setPartnerId(response.getId());
			partnershipInformationRequest.setName(response.getName());
			partnershipInformationRequest.setType(Constants.PARTNERSHIP_ECOMMERCE_TYPE);

			partnershipInformation = partnershipInformationBody
					.setPartnershipInfo(partnershipInformationRequest, tokenPayload, customerId, customerRoleId);

			PartnershipInformation partnershipInformationSave = Optional.ofNullable(partnershipInformationService.saveOrUpdate(partnershipInformation))
					.orElseThrow(() -> new AppException(messageUtils.insertFailed("partnership info")));

			productSelections = Collections.singletonList(Constants.OSF_PRODUCT);
			customerProduct = Long.valueOf(Constants.USER_PREFERENCE_OSF);

			for (Integer productSelection : Objects.requireNonNull(productSelections)) {
				CustomerProductList customerProductList = new CustomerProductList();
				customerProductList.setCustomerRoleId(customerRole.getId());
				customerProductList.setType(Long.valueOf(productSelection));
				customerProductList.setCreatedBy(tokenPayload.getUserId());

				if (!Optional.ofNullable(customerProductListService
						.saveOrUpdate(customerProductList)).isPresent()) {
					throw new AppException(messageUtils.insertFailed("Customer Product List"));
				}
			}
		}

		if (request.isRegisterFromApi()) {
			BusinessProfile businessProfile = new BusinessProfile();
			businessProfile.setCustomerId(customerId);
			Optional.ofNullable(request.getPhoneNumber()).ifPresent(businessProfile::setLandLineNumber);
			Optional.ofNullable(request.getBusinessDescription()).ifPresent(businessProfile::setCompanyDescription);
			Optional.ofNullable(request.getBusinessDateOfEstablished()).ifPresent(businessProfile::setDateOfEstablishment);
			Optional.ofNullable(request.getBusinessAddress()).ifPresent(businessProfile::setCompanyAddress);
			Optional.ofNullable(request.getBusinessPostalCode()).ifPresent(businessProfile::setPostalCode);
			Optional.ofNullable(request.getBusinessProvinceName()).ifPresent(businessProfile::setProvinceName);
			Optional.ofNullable(request.getBusinessCityName()).ifPresent(businessProfile::setCityName);
			Optional.ofNullable(request.getBusinessDistrictName()).ifPresent(businessProfile::setDistrictName);
			Optional.ofNullable(request.getBusinessSubDistrictName()).ifPresent(businessProfile::setVillageName);
			businessProfileService.saveOrUpdate(businessProfile);

			LegalInformation legalInformation = new LegalInformation();
			legalInformation.setCustomerId(customerId);
			Optional.ofNullable(request.getLegalInfoDocNumber()).ifPresent(legalInformation::setDocumentNumber);
			Optional.ofNullable(request.getLegalInfoDocFileType()).ifPresent(v -> {
				legalInformation.setDocumentType(v);
				legalInformationService.save(legalInformation);
			});

			Optional.ofNullable(request.getSellerLink()).ifPresent(v -> {
				PartnershipInformation sellerLinkInfo = new PartnershipInformation();
				sellerLinkInfo.setCustomerId(customerId);
				sellerLinkInfo.setSellerLink(v);
				partnershipInformationService.saveOrUpdate(sellerLinkInfo);
			});

			BankInformation bankInformation = new BankInformation();
			bankInformation.setCustomerId(customerId);
			bankInformation.setCustomerRoleId(customerRoleId);
			bankInformation.setUseAsDisbursement(GlobalConstants.STATUS_YES);
			bankInformation.setBankType(Constants.BANK_TYPE_ACCOUNT);
			Optional.ofNullable(request.getBankMasterId()).ifPresent(bankInformation::setMasterBankId);
			Optional.ofNullable(request.getBankAccountName()).ifPresent(bankInformation::setBankAccountHolderName);
			Optional.ofNullable(request.getBankAccountNumber()).ifPresent(bankInformation::setBankAccountNumber);
			bankInformationService.saveOrUpdate(bankInformation);

			Optional.ofNullable(request.getVaNumber()).ifPresent(vaNumber -> {
				invarBankService.findById(Constants.INVAR_BANK_ID_DANAMON_SYARIAH, Constants.INVAR_BANK_ID_DANAMON_BORROWER).forEach(invarBank -> {
					BankInformation bankVa = new BankInformation();
					bankVa.setCustomerId(customerId);
					bankVa.setCustomerRoleId(customerRoleId);
					bankVa.setBankType(Constants.BANK_TYPE_VA);
					bankVa.setMasterBankId(invarBank.getBankId());
					bankVa.setBankAccountNumber(vaNumber);
					bankVa.setVerifiedStatus(Constants.VA_STATUS_VERIFIED);
					Optional.ofNullable(request.getBankAccountName()).ifPresent(bankVa::setBankAccountHolderName);
					bankInformationService.saveOrUpdate(bankVa);
				});
			});

			Optional.ofNullable(request.getCashflowHistory()).ifPresent(cashflows -> {
				for (int i = 0; i < cashflows.size(); i++) {
					try {
						SalesTransaction salesTransaction = new SalesTransaction();
						salesTransaction.setCustomerId(customerId);
						salesTransaction.setAmount(cashflows.get(i).getValue());

						if (null != request.getTransactionHistory() && null != request.getTransactionHistory().get(i)) {
							salesTransaction.setTransaction(request.getTransactionHistory().get(i) * 1.0);
						}

						if (!Utilities.isNull(cashflows.get(i).getMonth()) && !Utilities.isNull(cashflows.get(i).getYear())) {
							Date date = DateUtils.convertToDate(String.format("%d-%d-01", cashflows.get(i).getYear(), cashflows.get(i).getMonth()), "yyyy-MM-dd");
							salesTransaction.setDate(date);
						}

						salesTransactionService.saveOrUpdate(salesTransaction);
					} catch (ParseException e) {
						throw new AppException(e.getMessage());
					}

				}
			});

			Optional.ofNullable(request.getReferral()).ifPresent(referralCode -> {
				ReferralData referralData = new ReferralData();
				referralData.setReferralUserId(request.getLoginDataId());
				referralData.setReferrerCode(referralCode);
				referralDataService.saveOrUpdate(referralData);
			});

			FinancialTrend financialTrend = new FinancialTrend();
			financialTrend.setCustomerId(customerId);
			financialTrend.setTrendPeriod("1 to 2");
			Optional.ofNullable(request.getProfitMargin()).ifPresent(financialTrend::setGrossProfit);
			financialTrendService.saveOrUpdate(financialTrend);

			if (!Utilities.isEmptyOrBlank(request.getBpdNumber())) {
				CifList cifList = cifListService.findById(Long.valueOf(request.getBpdNumber()));
				if (Optional.ofNullable(cifList).isPresent()) {
					throw new AppException("ci number exist");
				}
				cifList = new CifList();
				cifList.setId(Long.valueOf(request.getBpdNumber()));
				cifList.setCustomerId(customerId);
				cifList.setCreatedAt(new Date());
				cifList.setCreatedBy(tokenPayload.getUserId());
				if (!Optional.ofNullable(cifListService.saveByQuery(cifList)).isPresent()) {
					throw new AppException("failed save bpd number");
				}
			}

			for (Integer productSelection : Objects.requireNonNull(request.getProductSelections())) {
				CustomerProductList customerProductList = new CustomerProductList();
				customerProductList.setCustomerRoleId(customerRole.getId());
				customerProductList.setType(Long.valueOf(productSelection));
				customerProductList.setCreatedBy(tokenPayload.getUserId());

				if (!Optional.ofNullable(customerProductListService
						.saveOrUpdate(customerProductList)).isPresent()) {
					throw new AppException(messageUtils.insertFailed("Customer Product List"));
				}
			}

			if (null != request.getOtpCreatedAt()) {
				Optional.ofNullable(request.getPhoneNumber()).ifPresent(v -> {
					AtomicReference<String> mobilePrefix = new AtomicReference<>("");
					Optional.ofNullable(request.getMobilePrefix()).ifPresent(prefixName -> {
						mobilePrefix.set(completingDataGlobalParam.getMobilePrefixList().stream()
								.filter(val -> val.getId().equals(Integer.valueOf(prefixName)))
								.findFirst().map(DefaultData::getName).orElse(null));
					});
					String phoneNumber = mobilePrefix + v;

					InitialDataRequest otpRequest = new InitialDataRequest();
					otpRequest.setLoginDataId(request.getLoginDataId());
					otpRequest.setPhoneNumber(phoneNumber);
					otpRequest.setOtpCreatedAt(request.getOtpCreatedAt());
					notificationService.setOtpPartners(accessToken, otpRequest);
				});
			}
		}

		if (GlobalConstants.USER_CATEGORY_PARTNER_ID.equals(request.getUserType()) && !Utilities.isNull(request.getEcommercePartnershipRequest())) {
			EcommercePartnershipRequest ecommercePartnershipRequest = request.getEcommercePartnershipRequest();
			ecommercePartnershipRequest.setCustomerId(customerId);
			partnershipInformationService.createEcommercePartnershipData(ecommercePartnershipRequest, http.getHeader(GlobalConstants.HEADER_TOKEN));
		}

		RegisterStatusResponse registrationStatus = generateInitialRegistrationStatus(request.getUserType());

		CompletingInitialData response = new CompletingInitialData();
		response.setCustomerId(customerId);
		response.setUserCategory(customerInformation.getUserCategory());
		response.setProductPreference(customerInformation.getProductPreference());
		response.setUserRoleTypes(listRoles);
		response.setNationality(userData.getNationality());
		response.setRegistrationStatus(registrationStatus);

		if (request.isRegisterFromApi()) {
			response.setBorrowerNumber(customerInformation.getCustomerNumber());
		} else {
			LegacyModel legacyModel = new LegacyModelBuilder()
					.setGlobalParam(completingDataGlobalParam)
					.setUserData(userData)
					.setCustomerInformation(customerInformation)
					.setCustomerRole(customerRole)
					.setPartnershipInformation(partnershipInformation)
					.setProductSelection(customerProduct)
					.setEmail(tokenPayload.getUserEmail())
					.build(customerInformation.getId());

			syncLegacyService.sync(accessToken, legacyModel);
		}
		return abstractResponseHandler(response).getResult(messageUtils.insertSuccess());
	}

	private RegisterStatusResponse generateInitialRegistrationStatus(Integer userType) {
		RegisterStatusResponse registerStatusResponse = new RegisterStatusResponse();
		DefaultData defaultData = completingDataGlobalParam.getRegistrationStatusList()
				.stream().filter(predicate -> predicate.getId() == 1)
				.findFirst().orElse(new DefaultData());

		if (GlobalConstants.USER_CATEGORY_BORROWER_ID.equals(userType)) {
			registerStatusResponse.setBorrower(defaultData);

		} else if (GlobalConstants.USER_CATEGORY_LENDER_ID.equals(userType)) {
			registerStatusResponse.setLender(defaultData);

		} else if (GlobalConstants.USER_CATEGORY_ANCHOR_ID.equals(userType)) {
			registerStatusResponse.setAnchor(defaultData);

		} else if (GlobalConstants.USER_CATEGORY_PARTNER_ID.equals(userType)) {
			registerStatusResponse.setPartner(defaultData);

		}
		return registerStatusResponse;
	}

	@PutMapping("/completing-data/update")
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> updateUserData(@RequestBody InitialDataRequest request, HttpServletRequest http) {

		String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);
		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(messageUtils.emptyToken());
		}

		UserData userData = userDataService.findByLoginId(request.getLoginDataId());
		Optional.ofNullable(request.getPosition()).ifPresent(userData::setCompanyPosition);
		Optional.ofNullable(request.getDateOfBirth()).ifPresent(userData::setDateOfBirth);
		Optional.ofNullable(request.getPlaceOfBirth()).ifPresent(userData::setPlaceOfBirth);
		Optional.ofNullable(request.getKtpNumber()).ifPresent(userData::setIdCardNumber);
		Optional.ofNullable(request.getKtpExpired()).ifPresent(userData::setIdCardExpired);
		Optional.ofNullable(request.getAddress()).ifPresent(userData::setKtpAddress);
		Optional.ofNullable(request.getPostalCode()).ifPresent(userData::setKtpPostalCode);
		Optional.ofNullable(request.getDomicileAddress()).ifPresent(userData::setDomicileAddress);
		Optional.ofNullable(request.getDomicileProvinceName()).ifPresent(userData::setDomicileProvinceName);
		Optional.ofNullable(request.getDomicileCityName()).ifPresent(userData::setDomicileCityName);
		Optional.ofNullable(request.getDomicileDistrictName()).ifPresent(userData::setDomicileDistrictName);
		Optional.ofNullable(request.getDomicileSubDistrictName()).ifPresent(userData::setDomicileVillageName);
		Optional.ofNullable(request.getDomicilePostalCode()).ifPresent(userData::setDomicilePostalCode);

		if (!Utilities.isEmptyOrBlank(request.getPlaceOfBirthExternal())) {
			userData.setPlaceOfBirth(Constants.EXT_POB_ID);
			userData.setPlaceOfBirthExternal(request.getPlaceOfBirthExternal());
		}

		if (request.isRegisterFromApi()) {
			Optional.ofNullable(request.getProvinceName()).ifPresent(userData::setPartnerProvince);
			Optional.ofNullable(request.getCityName()).ifPresent(userData::setPartnerCity);
			Optional.ofNullable(request.getDistrictName()).ifPresent(userData::setPartnerDistrict);
			Optional.ofNullable(request.getSubDistrictName()).ifPresent(userData::setPartnerVillage);
			Optional.ofNullable(request.getPostalCode()).ifPresent(userData::setPartnerPostalCode);
		}

		userData = Optional.ofNullable(userDataService.saveOrUpdate(userData))
				.orElseThrow(() -> new AppException(messageUtils.updateSuccess()));

		CustomerInformation customerInformation = customerInformationService.findById(userData.getCustomerId());
		Optional.ofNullable(request.getUserCategory()).ifPresent(customerInformation::setUserCategory);
		Optional.ofNullable(request.getNationality()).ifPresent(customerInformation::setNationality);
		Optional.ofNullable(request.getCompanyName()).ifPresent(customerInformation::setName);
		Optional.ofNullable(request.getBpdNumber()).ifPresent(customerInformation::setCustomerNumber);

		if (request.isRegisterFromApi()) {
			customerInformation.setProductPreference(GlobalConstants.USER_PREFERENCE_CONVENTIONAL);
		}

		customerInformation = Optional.ofNullable(customerInformationService.saveOrUpdate(customerInformation)).orElseThrow(() -> new AppException(messageUtils.updateSuccess()));

		if (request.isRegisterFromApi()) {
			Long customerId = userData.getCustomerId();
			Optional.ofNullable(businessProfileService.findByCustomerId(customerId)).ifPresent(businessProfile -> {
				businessProfile.setCustomerId(customerId);
				Optional.ofNullable(request.getPhoneNumber()).ifPresent(businessProfile::setLandLineNumber);
				Optional.ofNullable(request.getBusinessDescription()).ifPresent(businessProfile::setCompanyDescription);
				Optional.ofNullable(request.getBusinessDateOfEstablished()).ifPresent(businessProfile::setDateOfEstablishment);
				Optional.ofNullable(request.getBusinessAddress()).ifPresent(businessProfile::setCompanyAddress);
				Optional.ofNullable(request.getBusinessPostalCode()).ifPresent(businessProfile::setPostalCode);
				Optional.ofNullable(request.getBusinessProvinceName()).ifPresent(businessProfile::setProvinceName);
				Optional.ofNullable(request.getBusinessCityName()).ifPresent(businessProfile::setCityName);
				Optional.ofNullable(request.getBusinessDistrictName()).ifPresent(businessProfile::setDistrictName);
				Optional.ofNullable(request.getBusinessSubDistrictName()).ifPresent(businessProfile::setVillageName);
				businessProfileService.saveOrUpdate(businessProfile);
			});

			LegalInformation legalInformation = legalInformationService.findByCustomerIdAndType(customerId, request.getLegalInfoDocFileType());
			legalInformation.setCustomerId(customerId);
			Optional.ofNullable(request.getLegalInfoDocNumber()).ifPresent(legalInformation::setDocumentNumber);
			Optional.ofNullable(request.getLegalInfoDocFileType()).ifPresent(v -> {
				legalInformation.setDocumentType(v);
				legalInformationService.save(legalInformation);
			});

			Optional.ofNullable(request.getSellerLink()).ifPresent(v -> {
				PartnershipInformation partnershipInformation = partnershipInformationService.findByCustomerId(customerId);
				if (null == partnershipInformation) {
					partnershipInformation = new PartnershipInformation();
					partnershipInformation.setCustomerId(customerId);
					partnershipInformation.setSellerLink(v);
				}
				partnershipInformationService.saveOrUpdate(partnershipInformation);
			});

			Long customerRoleId = Optional.ofNullable(customerRoleService.findByCustomerId(customerId))
					.map(CustomerRole::getId)
					.orElse(null);

			BankInformation bankInformation = bankInformationService.findByCustomerIdAndIsUseAsDisbursement(customerId);
			if (null == bankInformation) {
				bankInformation = new BankInformation();
				bankInformation.setCustomerId(customerId);
				bankInformation.setCustomerRoleId(customerRoleId);
				bankInformation.setBankType(Constants.BANK_TYPE_ACCOUNT);
			}
			Optional.ofNullable(request.getBankMasterId()).ifPresent(bankInformation::setMasterBankId);
			Optional.ofNullable(request.getBankAccountName()).ifPresent(bankInformation::setBankAccountHolderName);
			Optional.ofNullable(request.getBankAccountNumber()).ifPresent(bankInformation::setBankAccountNumber);
			bankInformationService.saveOrUpdate(bankInformation);

			Optional.ofNullable(request.getVaNumber()).ifPresent(vaNumber -> {
				invarBankService.findById(Constants.INVAR_BANK_ID_DANAMON_SYARIAH, Constants.INVAR_BANK_ID_DANAMON_BORROWER).forEach(invarBank -> {
					BankInformation bankVa = bankInformationService.findByCustomerIdAndBankId(customerId, invarBank.getBankId());
					if (null == bankVa) {
						bankVa = new BankInformation();
						bankVa.setCustomerId(customerId);
						bankVa.setBankType(Constants.BANK_TYPE_VA);
						bankVa.setMasterBankId(invarBank.getBankId());
					}
					bankVa.setCustomerRoleId(customerRoleId);
					bankVa.setBankAccountNumber(vaNumber);
					bankVa.setVerifiedStatus(Constants.VA_STATUS_VERIFIED);
					Optional.ofNullable(request.getBankAccountName()).ifPresent(bankVa::setBankAccountHolderName);
					bankInformationService.saveOrUpdate(bankVa);
				});
			});

			Optional.ofNullable(request.getCashflowHistory()).ifPresent(cashflows -> {
				for (int i = 0; i < cashflows.size(); i++) {
					try {
						SalesTransaction salesTransaction = new SalesTransaction();
						salesTransaction.setCustomerId(customerId);
						salesTransaction.setAmount(cashflows.get(i).getValue());

						if (null != request.getTransactionHistory() && null != request.getTransactionHistory().get(i)) {
							salesTransaction.setTransaction(request.getTransactionHistory().get(i) * 1.0);
						}

						if (!Utilities.isNull(cashflows.get(i).getMonth()) && !Utilities.isNull(cashflows.get(i).getYear())) {
							Date date = DateUtils.convertToDate(String.format("%d-%d-01", cashflows.get(i).getYear(), cashflows.get(i).getMonth()), "yyyy-MM-dd");
							salesTransaction.setDate(date);
						}

						salesTransactionService.saveOrUpdate(salesTransaction);
					} catch (ParseException e) {
						throw new AppException(e.getMessage());
					}

				}
			});

			Optional.ofNullable(request.getReferral()).ifPresent(referralCode -> {
				ReferralData referralData = Optional.ofNullable(referralDataService.findByReferralUserId(request.getLoginDataId()))
						.orElse(new ReferralData());
				referralData.setReferralUserId(request.getLoginDataId());
				referralData.setReferrerCode(referralCode);
				referralDataService.saveOrUpdate(referralData);
			});

			List<FinancialTrend> financialTrends = financialTrendService.findByCustomerId(customerId);
			for (FinancialTrend financialTrend : financialTrends) {
				Optional.ofNullable(request.getProfitMargin()).ifPresent(financialTrend::setGrossProfit);
				financialTrendService.saveOrUpdate(financialTrend);
			}

			for (Integer productSelection : Objects.requireNonNull(request.getProductSelections())) {
				CustomerProductList customerProductList = new CustomerProductList();
				customerProductList.setCustomerRoleId(customerRoleId);
				customerProductList.setType(Long.valueOf(productSelection));
				customerProductList.setCreatedBy(tokenPayload.getUserId());

				if (!Optional.ofNullable(customerProductListService
						.saveOrUpdate(customerProductList)).isPresent()) {
					throw new AppException(messageUtils.insertFailed("Customer Product List"));
				}
			}

			completingDataGlobalParam.getGlobalParam(GlobalConstants.GLOBAL_PARAM_MOBILE_PREFIX);

			if (null != request.getOtpCreatedAt()) {
				Optional.ofNullable(request.getPhoneNumber()).ifPresent(v -> {
					AtomicReference<String> mobilePrefix = new AtomicReference<>("");
					Optional.ofNullable(request.getMobilePrefix()).ifPresent(prefixName -> {
						mobilePrefix.set(completingDataGlobalParam.getMobilePrefixList().stream()
								.filter(val -> val.getId().equals(Integer.valueOf(prefixName)))
								.findFirst().map(DefaultData::getName).orElse(null));
					});
					String phoneNumber = mobilePrefix + v;

					InitialDataRequest otpRequest = new InitialDataRequest();
					otpRequest.setLoginDataId(request.getLoginDataId());
					otpRequest.setPhoneNumber(phoneNumber);
					otpRequest.setOtpCreatedAt(request.getOtpCreatedAt());
					notificationService.setOtpPartners(accessToken, otpRequest);
				});
			}
		}

		CifList cifList = cifListService.findByCustomerId(customerInformation.getId());
		if (null == cifList) {
			throw new DataNotFoundException("cif");
		}

		CompletingInitialData response = new CompletingInitialData();
		response.setCustomerId(customerInformation.getId());
		response.setBorrowerNumber(cifList.getId().

				toString());

		return

				abstractResponseHandler(response).

						getResult(messageUtils.updateSuccess());
	}

	@GetMapping("/completing-data")
	public ResponseEntity<ResultResponse> getCompletingData(HttpServletRequest http) throws
			MalformedURLException {
		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(messageUtils.emptyToken());
		}

		String accessToken = http.getHeader("X-Investree-Token");
		LoginDataResponse loginData = loginDataService.findByLoginId(accessToken, tokenPayload.getUserId());
		if (Utilities.isNull(loginData)) {
			throw new DataNotFoundException("Login Data");
		}

		InternalUserDataResponse userDataResponse = completingDataMapper.generateCompletingData(http, loginData, tokenPayload.getUserId());
		if (Utilities.isNull(userDataResponse)) {
			throw new DataNotFoundException("Completing Data");
		}
		return abstractResponseHandler(userDataResponse).getResult(messageUtils.dataFetched());
	}

	@GetMapping("/completing-data/{customerId}")
	public ResponseEntity<ResultResponse> getCompletingData(@PathVariable("customerId") Long customerId,
															HttpServletRequest http) throws MalformedURLException {
		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(messageUtils.emptyToken());
		}

		UserData userData = userDataService.findByCustomerId(customerId);
		if (Utilities.isNull(userData)) {
			throw new DataNotFoundException("customer id: " + customerId);
		}

		String accessToken = http.getHeader("X-Investree-Token");
		LoginDataResponse loginData = loginDataService.findByLoginId(accessToken, userData.getLoginDataId());
		if (Utilities.isNull(loginData)) {
			throw new DataNotFoundException("Login Data");
		}

		InternalUserDataResponse userDataResponse = completingDataMapper.generateCompletingData(http, loginData, userData.getLoginDataId());
		if (Utilities.isNull(userDataResponse)) {
			throw new DataNotFoundException("Completing Data");
		}
		return abstractResponseHandler(userDataResponse).getResult(messageUtils.dataFetched());
	}

	@GetMapping("/borrower/{customerId}")
	public ResponseEntity<ResultResponse> getBorrowerCompletingData(@PathVariable("customerId") Long
																			customerId,
																	HttpServletRequest http) throws MalformedURLException {

		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
				.orElseThrow(() -> new AppException(messageUtils.emptyToken()));

		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(customerId))
				.orElseThrow(() -> new DataNotFoundException("customer id:" + customerId));

		String accessToken = http.getHeader("X-Investree-Token");
		LoginDataResponse loginDataResponse = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
				.orElseThrow(() -> new DataNotFoundException("Login Data"));

		InternalBorrowerData internalBorrowerData = generateInternalBorrowerData(accessToken, userData, loginDataResponse);

		return abstractResponseHandler(internalBorrowerData).getResult(messageUtils.dataFetched());
	}

	private InternalBorrowerData generateInternalBorrowerData(String accessToken, UserData userData,
															  LoginDataResponse loginDataResponse) throws MalformedURLException {
		DefaultData province = postalAreaService.findProvince(accessToken, userData.getKtpProvince());
		DefaultData city = postalAreaService.findCity(accessToken, userData.getKtpCity());
		DefaultData district = postalAreaService.findDistrict(accessToken, userData.getKtpDistrict());
		DefaultData subDistrict = postalAreaService.findVillage(accessToken, userData.getKtpVillage());

		DefaultData placeOfBirth;
		if (!Utilities.isNull(userData.getPlaceOfBirth()) && Constants.EXT_POB_ID.equals(userData.getPlaceOfBirth())) {
			placeOfBirth = new DefaultData(Math.toIntExact(Constants.EXT_POB_ID), userData.getPlaceOfBirthExternal());
		} else {
			placeOfBirth = postalAreaService.findCity(accessToken, userData.getPlaceOfBirth());
		}

		String gender = Utilities.isNull(loginDataResponse.getGender()) ? null
				: (loginDataResponse.getGender().equals(Constants.USER_FEMALE_GENDER_TYPE) ? Constants.USER_FEMALE_GENDER
				: Constants.USER_MALE_GENDER);

		String dateOfBirth = DateUtils.convertToString(userData.getDateOfBirth(), "yyyy-MM-dd");

		LegalInformation legalInformation = legalInformationService.findByCustomerIdAndType(userData.getCustomerId(), Constants.LEGAL_INFO_NPWP);

		String npwpFileName;
		if (!Utilities.isNull(legalInformation.getDocumentFile()) && legalInformation.getDocumentFile().startsWith("http")) {
			URL url = new URL(legalInformation.getDocumentFile());
			npwpFileName = FilenameUtils.getName(url.getPath());
		} else {
			npwpFileName = legalInformation.getDocumentFile();
		}

		String signatureFileName;
		if (!Utilities.isNull(userData.getSignatureFile()) && userData.getSignatureFile().startsWith("http")) {
			URL url = new URL(userData.getSignatureFile());
			signatureFileName = FilenameUtils.getName(url.getPath());
		} else {
			signatureFileName = userData.getSignatureFile();
		}

		String idCardFileName;
		if (!Utilities.isNull(userData.getIdCardFile()) && userData.getIdCardFile().startsWith("http")) {
			URL url = new URL(userData.getIdCardFile());
			idCardFileName = FilenameUtils.getName(url.getPath());
		} else {
			idCardFileName = userData.getIdCardFile();
		}

		String selfieFileName;
		if (!Utilities.isNull(userData.getSelfieKtpFile()) && userData.getSelfieKtpFile().startsWith("http")) {
			URL url = new URL(userData.getSelfieFile());
			selfieFileName = FilenameUtils.getName(url.getPath());
		} else {
			selfieFileName = userData.getSelfieFile();
		}

		completingDataGlobalParam.getGlobalParam(GlobalConstants.GLOBAL_PARAM_MOBILE_PREFIX);

		DefaultData mobilePrefix = null;
		if (!Utilities.isNull(loginDataResponse.getMobilePrefix())) {
			mobilePrefix = completingDataGlobalParam.getMobilePrefixList().stream()
					.filter(v -> v.getId().equals(Integer.valueOf(loginDataResponse.getMobilePrefix())))
					.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}

		InternalBorrowerData internalBorrowerData = new InternalBorrowerData();
		internalBorrowerData.setFullName(loginDataResponse.getFullname());
		internalBorrowerData.setEmailAddress(loginDataResponse.getEmailAddress());
		internalBorrowerData.setAddress(userData.getKtpAddress());
		internalBorrowerData.setProvince(province);
		internalBorrowerData.setCity(city);
		internalBorrowerData.setDistrict(district);
		internalBorrowerData.setVillage(subDistrict);
		internalBorrowerData.setMobilePrefix(mobilePrefix);
		internalBorrowerData.setMobileNumber(loginDataResponse.getPhoneNumber());
		internalBorrowerData.setGender(gender);
		internalBorrowerData.setPlaceOfBirth(placeOfBirth);
		internalBorrowerData.setDateOfBirth(dateOfBirth);
		internalBorrowerData.setSelfiePicture(userData.getSelfieFile());
		internalBorrowerData.setSelfieFileName(selfieFileName);
		internalBorrowerData.setSignaturePicture(userData.getSignatureFile());
		internalBorrowerData.setSignatureFileName(signatureFileName);
		internalBorrowerData.setDocumentNumber(legalInformation.getDocumentNumber());
		internalBorrowerData.setNpwpFile(legalInformation.getDocumentFile());
		internalBorrowerData.setNpwpFileName(npwpFileName);
		internalBorrowerData.setIdCardNumber(userData.getIdCardNumber());
		internalBorrowerData.setIdCardPicture(userData.getIdCardFile());
		internalBorrowerData.setIdCardFileName(idCardFileName);

		if (!Utilities.isNull(userData.getPlaceOfBirth()) && Constants.EXT_POB_ID.equals(userData.getPlaceOfBirth())) {
			internalBorrowerData.setPlaceOfBirthExternal(userData.getPlaceOfBirthExternal());
		}

		return internalBorrowerData;
	}

	@PutMapping("/completing-data/pic")
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> updateUserDataCompany(HttpServletRequest http,
																@RequestParam(value = "customerId", required = false) Long customerId,
																@RequestParam(value = "userRoleType", defaultValue = "1") Integer userType,
																@RequestBody UpdateUserDataRequest request) {

		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(messageUtils.emptyToken());
		}
		String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);
		UserData userData;
		CustomerRole customerRole;
		if (Utilities.isNull(customerId)) {
			userData = userDataService.findByLoginId(tokenPayload.getUserId());
			customerRole = customerRoleService.findByCustomerId(Long.valueOf(tokenPayload.getCustomerId()));
			customerId = userData.getCustomerId();
		} else {
			userData = userDataService.findByCustomerId(customerId);
			customerRole = customerRoleService.findByCustomerId(customerId);
		}

		if (Utilities.isNull(userData)) {
			throw new DataNotFoundException("for user id: " + tokenPayload.getUserId());
		}

		internalDetailDataValidator.validatePic(userData, request, userType);
		userData = internalDetailDataFlow.generateUserDataByUserType(userData, request, tokenPayload);

		UserData userDataResponse = userDataService.saveOrUpdate(userData);
		if (Utilities.isNull(userDataResponse)) {
			throw new AppException(messageUtils.insertFailed("User Data"));
		}

		String emailAddress = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userDataResponse.getLoginDataId()))
				.map(LoginDataResponse::getEmailAddress)
				.orElseThrow(() -> new DataNotFoundException("Login Data"));

		if (GlobalConstants.USER_CATEGORY_LENDER_ID.equals(userType)) {
			BusinessProfile businessProfile = Optional.ofNullable(businessProfileService.findByCustomerId(customerId))
					.map(v -> {
						v.setMailingAddressStatus(request.getMailingAddressStatus());

						return v;
					})
					.orElseGet(() -> {
						BusinessProfile mailingAddress = new BusinessProfile();

						mailingAddress.setMailingAddressStatus(request.getMailingAddressStatus());
						return mailingAddress;
					});
		}

		customerRole.setStatus(Utilities.isNull(request.getStatus()) ? customerRole.getStatus() : request.getStatus());
		customerRole.setPoaFile(Utilities.isNull(request.getPoaFile()) ? customerRole.getPoaFile() : request.getPoaFile());
		CustomerRole customerRoleResponse = Optional.ofNullable(customerRoleService.saveOrUpdate(customerRole))
				.orElseThrow(() -> new AppException(messageUtils.insertFailed("Poa update")));

		LegacyModel legacyModel = new LegacyModelBuilder()
				.setCustomerRole(customerRoleResponse)
				.setGlobalParam(completingDataGlobalParam)
				.setUserData(userDataResponse)
				.setEmail(emailAddress)
				.build(customerId);

		syncLegacyService.sync(accessToken, legacyModel);

		return abstractResponseHandler(userData).getResult(messageUtils.updateSuccess());
	}

	@PutMapping("/referral-data")
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> updateReferralInfo(HttpServletRequest
																	 http, @RequestBody ReferralCodeRequest request) {

		String accessToken = http.getHeader("X-Investree-Token");

		ReferralData referralData;
		Long customerId = null;
		if (Optional.ofNullable(request.getReferralUserId()).isPresent()) {
			customerId = Optional.ofNullable(userDataService.findByLoginId(request.getReferralUserId()))
					.map(UserData::getCustomerId)
					.orElseThrow(() -> new DataNotFoundException("referral id"));

			referralData = Optional.ofNullable(referralDataService.findByReferralUserId(request.getReferralUserId()))
					.orElse(new ReferralData());

			referralData.setReferrerCode(Utilities.isEmptyOrBlank(request.getReferrerCode()) ? referralData.getReferrerCode() : request.getReferrerCode());
			referralData.setReferralUserId(request.getReferralUserId());
			referralData.setReferrerUserId(Utilities.isNull(request.getReferrerUserId()) ? referralData.getReferrerUserId() : request.getReferrerUserId());
			referralData.setReferralExpiredDate(Utilities.isNull(request.getExpiredDate()) ? null : Date.from(ZonedDateTime.now(ZoneId.of("Asia/Jakarta")).plusMonths(6).toInstant()));
			referralData.setKnowInvestreeFrom(request.getKnowInvestreeFrom());
		} else {
			referralData = new ReferralData();

			referralData.setReferrerCode(request.getReferrerCode());
			referralData.setReferralUserId(request.getReferralUserId());
			referralData.setReferrerUserId(request.getReferrerUserId());
			referralData.setReferralExpiredDate(Utilities.isNull(request.getExpiredDate()) ? null : Date.from(ZonedDateTime.now(ZoneId.of("Asia/Jakarta")).plusMonths(6).toInstant()));
			referralData.setKnowInvestreeFrom(request.getKnowInvestreeFrom());
		}

		ReferralData referralDataSave = Optional.ofNullable(referralDataService.saveOrUpdate(referralData)).orElseThrow(() -> new AppException(messageUtils.updateFailed()));
		ReferralInformationData referralInformationData = referralDataResponseGenerator.generateReferralInformation(referralDataSave, accessToken);

		if (!Utilities.isNull(customerId)) {
			UserData userData = Optional.ofNullable(userDataService.findByCustomerId(customerId))
					.orElseThrow(() -> new DataNotFoundException("User Data"));

			LegacyModel legacyModel = new LegacyModelBuilder()
					.setReferralData(referralDataSave)
					.setGlobalParam(completingDataGlobalParam)
					.setEmail(request.getEmailAddress())
					.build(customerId);

			syncLegacyService.sync(accessToken, legacyModel);
		}

		return abstractResponseHandler(referralInformationData).getResult(messageUtils.updateSuccess());
	}

	@GetMapping("/check-referrer/{referrerCode}")
	public ResponseEntity<ResultResponse> checkReferrer(@PathVariable("referrerCode") String
																referrerCode, HttpServletRequest http) {

		ReferralData referralData = referralDataService.findByReferrerCode(referrerCode);

		return abstractResponseHandler(referralData).getResult(messageUtils.dataFetched());
	}

	@GetMapping("/check-borrower-status/{customerId}")
	public ResponseEntity<ResultResponse> checkBorrowerStatus(@PathVariable("customerId") Long customerId,
															  HttpServletRequest http) throws MalformedURLException {

		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(messageUtils.emptyToken());
		}

		CustomerRole customerRole = customerRoleService.findByCustomerIdAndType(customerId, GlobalConstants.USER_CATEGORY_BORROWER_ID);
		if (Constants.STATUS_ACTIVE.equals(customerRole.getStatus()) || Constants.STATUS_PENDING_VERIFICATION.equals(customerRole.getStatus()) || Constants.STATUS_INACTIVE.equals(customerRole.getStatus())) {
			throw new AppException(messageUtils.statusRestricted());
		}

		return abstractResponseHandler(true).getResult(messageUtils.dataFetched());
	}

	@GetMapping("userdata/{loginId}")
	public ResponseEntity<ResultResponse> getUserDataByLogin(HttpServletRequest http,
															 @PathVariable Long loginId) throws MalformedURLException {

		String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);

		UserData userData = Optional.ofNullable(userDataService.findByLoginId(loginId))
				.orElseThrow(DataNotFoundException::new);

		CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.findById(userData.getCustomerId()))
				.orElseThrow(DataNotFoundException::new);

		List<PartnershipInformation> partnershipInformationList = partnershipInformationService.findAllByCustomerId(userData.getCustomerId());

		String borrowerNumber = Optional.ofNullable(cifListService.findByCustomerId(customerInformation.getId()))
				.map(v -> v.getId().toString()).orElse(null);

		PersonalProfileData personalProfileData = personalProfileResponseGenerator.generateBasicPersonalProfileData(userData, customerInformation, accessToken);

		BusinessProfile businessProfile = Optional.ofNullable(businessProfileService.findByCustomerId(customerInformation.getId()))
				.orElseThrow(DataNotFoundException::new);

		BusinessInfoData businessInfoData = businessProfileResponseGenerator.generateBusinessInfoData(businessProfile, customerInformation, accessToken);

		List<BankInformation> bankInformation = Optional.ofNullable(bankInformationService.findByCustomerId(customerInformation.getId()))
				.orElseThrow(DataNotFoundException::new);

		BankInformationData bankInformationData;
		if (!bankInformation.isEmpty()) {
			bankInformationData = bankInformationResponseGenerator.generateBankInformation(bankInformation.get(0),
					accessToken, Long.valueOf(GlobalConstants.USER_CATEGORY_BORROWER_ID));

		} else {
			bankInformationData = new BankInformationData();
		}

		List<PartnershipInformationData> partnershipInformationDataList = new ArrayList<>();
		if (!partnershipInformationList.isEmpty()) {
			partnershipInformationDataList = partnershipInformationResponseGenerator.generatePartnershipInformationList(partnershipInformationList);
		}

		List<LegalInformationData> legalInformationData = legalInformationResponseGenerator.generatePartnerLegalInformation(customerInformation.getId());

		Long customerRoleStatus = Optional.ofNullable(customerRoleService.findByCustomerIdAndType(customerInformation.getId(), GlobalConstants.USER_CATEGORY_BORROWER_ID))
				.map(CustomerRole::getStatus).orElse(null);

		PartnersResponse partnersResponse = new PartnersResponse.PartnersResponseBuilder()
				.setProductPreference(customerInformation.getProductPreference())
				.setCustomerId(userData.getCustomerId())
				.setBorrowerNumber(borrowerNumber)
				.setUserData(userData, completingDataGlobalParam)
				.setPersonalProfileData(personalProfileData)
				.setBusinessInfoData(businessInfoData)
				.setBankInformationData(bankInformationData)
				.setCrStatus(customerRoleStatus)
				.setLegalInformationDataList(legalInformationData)
				.setCustomerInformation(customerInformation)
				.setPartnershipInformation(partnershipInformationDataList)
				.build();

		return abstractResponseHandler(partnersResponse).getResult(messageUtils.dataFetched());
	}

	@PostMapping("/sync-legacy/{customerId}")
	public ResponseEntity<ResultResponse> syncLegacy(HttpServletRequest http,
													 @PathVariable("customerId") Long customerId) {
		String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);
		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(customerId))
				.orElseThrow(() -> new DataNotFoundException("user by customer id:" + customerId));

		CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.findById(customerId))
				.orElseThrow(() -> new DataNotFoundException("customer id:" + customerId));

		CustomerRole customerRole = Optional.ofNullable(customerRoleService.findById(customerId))
				.orElseThrow(() -> new DataNotFoundException("role by customer id:" + customerId));

		String emailAddress = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
				.map(LoginDataResponse::getEmailAddress)
				.orElseThrow(() -> new DataNotFoundException("Login Data"));

		LegacyModel legacyModel = new LegacyModelBuilder()
				.setUserData(userData)
				.setGlobalParam(completingDataGlobalParam)
				.setCustomerInformation(customerInformation)
				.setCustomerRole(customerRole)
				.setEmail(emailAddress)
				.build(customerInformation.getId());

		syncLegacyService.sync(accessToken, legacyModel);
		return abstractResponseHandler(true).getResult(messageUtils.dataFetched());
	}

	private void deleteBasicData(Long customerId) {
		userDataService.hardDeleteByCustomerId(customerId);
		customerInformationService.hardDelete(customerId);
		customerRoleService.hardDeleteByCustomerId(customerId);
	}
}