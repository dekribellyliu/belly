package id.investree.customer.controller.internal.completing;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.model.TokenPayload;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.UserData;
import id.investree.customer.model.request.UpdateUserDataRequest;
import id.investree.customer.utils.Constants;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class InternalDetailDataFlow {

	/**
	 * Frontoffice has a function to check nullable field. If field is not null, then field should be save to database
	 * Backoffice will save whatever it given from request
	 */
	public UserData generateUserDataByUserType(UserData userData, UpdateUserDataRequest request, TokenPayload tokenPayload) {
		if (GlobalConstants.USER_TYPE_FRONTOFFICE_ID.equals(tokenPayload.getUserType())) {
			return generateUserDataByFrontoffice(userData, request, tokenPayload);
		} else {
			return generateUserDataByBackoffice(userData, request, tokenPayload);
		}
	}

	private UserData generateUserDataByBackoffice(UserData userData, UpdateUserDataRequest request, TokenPayload tokenPayload) {
		userData.setSelfieFile(request.getSelfiePicture());
		userData.setIdCardFile(request.getIdCardPicture());
		userData.setIdCardNumber(request.getIdCardNumber());
		userData.setKtpAddress(request.getAddress());
		userData.setKtpProvince(request.getProvince());
		userData.setKtpCity(request.getCity());
		userData.setKtpDistrict(request.getDistrict());
		userData.setKtpVillage(request.getSubDistrict());
		userData.setKtpPostalCode(request.getPostalCode());
		userData.setPlaceOfBirth(request.getPlaceOfBirth());
		userData.setDateOfBirth(request.getDateOfBirth());
		userData.setReligion(request.getReligion());
		userData.setIdCardExpired(request.getIdCardExpiredDate());
		userData.setUpdatedBy(tokenPayload.getUserId());
		userData.setSameAddress(Utilities.isNull(request.getSameAsDomicileAddress()) ? userData.getSameAddress() : request.getSameAsDomicileAddress() ? GlobalConstants.STATUS_YES : GlobalConstants.STATUS_NO);
		userData.setMaritalStatus(request.getMaritalStatus());
		userData.setEducation(request.getEducation());
		userData.setOccupation(request.getOccupation());

		if (!Utilities.isEmptyOrBlank(request.getPlaceOfBirthExternal())) {
			userData.setPlaceOfBirth(Constants.EXT_POB_ID);
			userData.setPlaceOfBirthExternal(request.getPlaceOfBirthExternal());
		}

		if (GlobalConstants.STATUS_YES.equals(userData.getSameAddress())) {
			userData.setDomicileAddress(request.getAddress());
			userData.setDomicileProvince(request.getProvince());
			userData.setDomicileCity(request.getCity());
			userData.setDomicileDistrict(request.getDistrict());
			userData.setDomicileVillage(request.getSubDistrict());
			userData.setDomicilePostalCode(request.getPostalCode());
		} else {
			userData.setDomicileAddress(request.getDomicileAddress());
			userData.setDomicileProvince(request.getDomicileProvince());
			userData.setDomicileCity(request.getDomicileCity());
			userData.setDomicileDistrict(request.getDomicileDistrict());
			userData.setDomicileVillage(request.getDomicileSubDistrict());
			userData.setDomicileProvinceName(request.getDomicileProvinceName());
			userData.setDomicileCityName(request.getDomicileCityName());
			userData.setDomicileDistrictName(request.getDomicileDistrictName());
			userData.setDomicileVillageName(request.getDomicileSubDistrictName());

			userData.setDomicilePostalCode(request.getDomicilePostalCode());
		}
		return userData;
	}

	private UserData generateUserDataByFrontoffice(UserData userData, UpdateUserDataRequest request, TokenPayload tokenPayload) {
		Optional.ofNullable(request.getNationality()).ifPresent(userData::setNationality);
		Optional.ofNullable(request.getPositionWithInstitution()).ifPresent(userData::setCompanyPosition);
		Optional.ofNullable(request.getDivisionWithInstitution()).ifPresent(userData::setCompanyDivision);
		Optional.ofNullable(request.getSelfiePicture()).ifPresent(userData::setSelfieFile);
		Optional.ofNullable(request.getIdCardPicture()).ifPresent(userData::setIdCardFile);
		Optional.ofNullable(request.getIdCardNumber()).ifPresent(userData::setIdCardNumber);
		Optional.ofNullable(request.getAddress()).ifPresent(userData::setKtpAddress);
		Optional.ofNullable(request.getProvince()).ifPresent(userData::setKtpProvince);
		Optional.ofNullable(request.getCity()).ifPresent(userData::setKtpCity);
		Optional.ofNullable(request.getDistrict()).ifPresent(userData::setKtpDistrict);
		Optional.ofNullable(request.getSubDistrict()).ifPresent(userData::setKtpVillage);
		Optional.ofNullable(request.getPostalCode()).ifPresent(userData::setKtpPostalCode);
		Optional.ofNullable(request.getPlaceOfBirth()).ifPresent(userData::setPlaceOfBirth);
		Optional.ofNullable(request.getDateOfBirth()).ifPresent(userData::setDateOfBirth);
		Optional.ofNullable(request.getReligion()).ifPresent(userData::setReligion);
		Optional.ofNullable(request.getIdCardExpiredDate()).ifPresent(userData::setIdCardExpired);
		Optional.ofNullable(tokenPayload.getUserId()).ifPresent(userData::setUpdatedBy);
		Optional.ofNullable(request.getMaritalStatus()).ifPresent(userData::setMaritalStatus);
		Optional.ofNullable(request.getEducation()).ifPresent(userData::setEducation);
		Optional.ofNullable(request.getOccupation()).ifPresent(userData::setOccupation);
		userData.setSameAddress(Utilities.isNull(request.getSameAsDomicileAddress()) ? userData.getSameAddress() : request.getSameAsDomicileAddress() ? GlobalConstants.STATUS_YES : GlobalConstants.STATUS_NO);

		if (!Utilities.isEmptyOrBlank(request.getPlaceOfBirthExternal())) {
			userData.setPlaceOfBirth(Constants.EXT_POB_ID);
			userData.setPlaceOfBirthExternal(request.getPlaceOfBirthExternal());
		}

		if (GlobalConstants.STATUS_YES.equals(userData.getSameAddress())) {
			Optional.ofNullable(request.getAddress()).ifPresent(userData::setDomicileAddress);
			Optional.ofNullable(request.getProvince()).ifPresent(userData::setDomicileProvince);
			Optional.ofNullable(request.getCity()).ifPresent(userData::setDomicileCity);
			Optional.ofNullable(request.getDistrict()).ifPresent(userData::setDomicileDistrict);
			Optional.ofNullable(request.getSubDistrict()).ifPresent(userData::setDomicileVillage);
			Optional.ofNullable(request.getPostalCode()).ifPresent(userData::setDomicilePostalCode);
		} else {
			Optional.ofNullable(request.getDomicileAddress()).ifPresent(userData::setDomicileAddress);
			Optional.ofNullable(request.getDomicileProvince()).ifPresent(userData::setDomicileProvince);
			Optional.ofNullable(request.getDomicileCity()).ifPresent(userData::setDomicileCity);
			Optional.ofNullable(request.getDomicileDistrict()).ifPresent(userData::setDomicileDistrict);
			Optional.ofNullable(request.getDomicileSubDistrict()).ifPresent(userData::setDomicileVillage);
			Optional.ofNullable(request.getDomicileProvinceName()).ifPresent(userData::setDomicileProvinceName);
			Optional.ofNullable(request.getDomicileCityName()).ifPresent(userData::setDomicileCityName);
			Optional.ofNullable(request.getDomicileDistrictName()).ifPresent(userData::setDomicileDistrictName);
			Optional.ofNullable(request.getDomicileSubDistrictName()).ifPresent(userData::setDomicileVillageName);
			Optional.ofNullable(request.getDomicilePostalCode()).ifPresent(userData::setDomicilePostalCode);
		}
		return userData;
	}
}
