package id.investree.customer.controller.internal.completing;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.regex.RegexValidator;
import id.investree.core.utils.DateUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.UserData;
import id.investree.customer.model.request.UpdateUserDataRequest;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InternalDetailDataValidator {

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private LocalMessageUtils messageUtils;

	void validatePic(UserData userData, UpdateUserDataRequest request, Integer userType) {
		if (GlobalConstants.USER_CATEGORY_LENDER_ID.equals(userType) && Utilities.isNull(request.getMailingAddressStatus())) {
			throw new AppException("please select one mailing address");
		}

		if (!Utilities.isEmptyOrBlank(request.getIdCardNumber())) {
			if (!RegexValidator.isKtpNumberValid(request.getIdCardNumber())) {
				throw new AppException(messageUtils.identityCardNumberLength());
			}

			if (!DateUtils.isDateMoreThanToday(request.getIdCardExpiredDate())) {
				throw new AppException(messageUtils.identityCardExpiry());
			}

			UserData ktpOwner = userDataService.findByIdCardNumber(request.getIdCardNumber());
			if (!Utilities.isNull(ktpOwner) && !userData.getId().equals(ktpOwner.getId())) {
				throw new AppException(messageUtils.identityCardNumberDuplicate());
			}
		}

		if (!Utilities.isEmptyOrBlank(request.getPostalCode())) {
			if (!RegexValidator.validateCustomLengthValidation(request.getPostalCode(), 5, 5)) {
				throw new AppException(messageUtils.errorPostalCode());
			}
		}
	}
}
