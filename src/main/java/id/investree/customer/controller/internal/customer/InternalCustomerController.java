package id.investree.customer.controller.internal.customer;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.exception.AppException;
import id.investree.core.model.TokenPayload;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.CustomerInformation;
import id.investree.customer.entity.LegalInformation;
import id.investree.customer.entity.UserData;
import id.investree.customer.model.response.internal.GetDataForPrivyRegisterResponse;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.legalinformation.LegalInformationService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;

@RestController
@RequestMapping(value = "/internal/customer-info", produces = MediaType.APPLICATION_JSON_VALUE)
public class InternalCustomerController extends BaseController {

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private CustomerInformationService customerInformationService;

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private LegalInformationService legalInformationService;

	@Autowired
	private LocalMessageUtils messageUtils;

	@GetMapping
	public ResponseEntity<ResultResponse> getCustomerInfo(HttpServletRequest http) throws MalformedURLException {
		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(messageUtils.emptyToken());
		}

		CustomerInformation customerInformation = customerInformationService.findById(Long.valueOf(tokenPayload.getCustomerId()));
		return abstractResponseHandler(customerInformation).getResult(messageUtils.dataFetched());
	}

	@GetMapping("{customerId}")
	public ResponseEntity<ResultResponse> getCustomerInfo(HttpServletRequest http,
	                                                      @PathVariable(value = "customerId") Long customerId) {
		TokenPayload tokenPayload = tokenUtils.parseToken(http);
		if (Utilities.isNull(tokenPayload)) {
			throw new AppException(messageUtils.emptyToken());
		}

		CustomerInformation customerInformation = customerInformationService.findById(customerId);
		return abstractResponseHandler(customerInformation).getResult(messageUtils.dataFetched());
	}

	@GetMapping("/privy/register/{customerId}")
	public ResponseEntity<ResultResponse> getDataForPrivyRegistration(@PathVariable("customerId") Long customerId) {

		UserData userData = userDataService.findByCustomerId(customerId);

		LegalInformation legalInformation = legalInformationService.findByCustomerIdAndType(customerId, Constants.LEGAL_INFO_NPWP);

		GetDataForPrivyRegisterResponse getData = new GetDataForPrivyRegisterResponse();
		getData.setLegalInformation(legalInformation);
		getData.setUserData(userData);

		return abstractResponseHandler(getData).getResult(messageUtils.dataFetched());
	}
}
