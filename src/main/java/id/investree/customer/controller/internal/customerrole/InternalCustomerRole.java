package id.investree.customer.controller.internal.customerrole;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.utils.TokenUtils;
import id.investree.customer.entity.CustomerRole;
import id.investree.customer.repository.UserDataRepository;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.LocalMessageUtils;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/internal/customer-role", produces = MediaType.APPLICATION_JSON_VALUE)
public class InternalCustomerRole extends BaseController {

	@Autowired
	private CustomerRoleService customerRoleService;

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private UserDataRepository userDataRepository;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	@GetMapping("{customerId}")
	public ResponseEntity<ResultResponse> findByCustomerId(@PathVariable("customerId") Long customerId,
														   @RequestParam(value = "userRoleType", required = false) Integer userRoleType) {

		CustomerRole customerRole = Optional.ofNullable(userRoleType)
				.map(v -> customerRoleService.findByCustomerIdAndType(customerId, v))
				.orElseGet(() -> customerRoleService.findByCustomerId(customerId));

		return abstractResponseHandler(customerRole).getResult(messageUtils.dataFetched());
	}

	@GetMapping("/all/{customerId}")
	public ResponseEntity<ResultResponse> findAllByCustomerId(@PathVariable("customerId") Long customerId) {

		List<CustomerRole> customerRoles = Optional.ofNullable(customerRoleService.findAllByCustomerId(customerId))
			.orElseThrow(() -> new DataNotFoundException("Customer Role"));

		return abstractResponseHandler(customerRoles).getResult(messageUtils.dataFetched());
	}
}
