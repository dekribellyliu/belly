package id.investree.customer.controller.internal.ecommercepartnership;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.DefaultData;
import id.investree.core.model.TokenPayload;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.CustomerRole;
import id.investree.customer.entity.UserData;
import id.investree.customer.model.response.EcommercePartnershipResponse;
import id.investree.customer.model.response.RegisterStatusResponse;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.partnershipinformation.PartnershipInformationService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.LocalMessageUtils;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@RestController
@RequestMapping(value = "/internal/ecommerce", produces = MediaType.APPLICATION_JSON_VALUE)
public class InternalEcommercePartnershipController extends BaseController {

	private TokenUtils tokenUtils;
	private LocalMessageUtils messageUtils;
	private UserDataService userDataService;
	private PartnershipInformationService partnershipInformationService;
	private CustomerRoleService customerRoleService;
	private StoreDataGlobalParam completingDataGlobalParam;

	@Autowired
	public InternalEcommercePartnershipController(TokenUtils tokenUtils,LocalMessageUtils messageUtils,
	                                              UserDataService userDataService,
	                                              PartnershipInformationService partnershipInformationService,
	                                              CustomerRoleService customerRoleService,
	                                              StoreDataGlobalParam completingDataGlobalParam) {

		this.tokenUtils = tokenUtils;
		this.messageUtils = messageUtils;
		this.userDataService = userDataService;
		this.partnershipInformationService = partnershipInformationService;
		this.customerRoleService = customerRoleService;
		this.completingDataGlobalParam = completingDataGlobalParam;
	}

	@GetMapping
	public ResponseEntity<ResultResponse> findEcommerceData(HttpServletRequest http) {
		String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);
		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(accessToken))
			.orElseThrow(() -> new AppException(messageUtils.emptyToken()));

		Long customerId = Optional.ofNullable(userDataService.findByLoginId(tokenPayload.getUserId()))
			.map(UserData::getCustomerId)
			.orElseThrow(() -> new DataNotFoundException("customer id"));

		CustomerRole customerRole = customerRoleService.findByCustomerIdAndType(customerId, GlobalConstants.USER_CATEGORY_PARTNER_ID);
		RegisterStatusResponse registerStatusResponse = new RegisterStatusResponse();
		DefaultData registerStatus = null;
		if (!Utilities.isNull(customerRole)) {
			registerStatus = completingDataGlobalParam.getRegistrationStatusList().stream()
				.filter(v -> v.getId().equals(Math.toIntExact(customerRole.getStatus())))
				.findFirst().map(v -> new DefaultData(v.getId(), v.getName())).orElse(null);
		}
		registerStatusResponse.setPartner(registerStatus);

		EcommercePartnershipResponse response = partnershipInformationService.findEcommercePartnershipByCustomerId(customerId, accessToken);
		response.setRegisterStatusResponse(registerStatusResponse);

		return abstractResponseHandler(response).getResult(messageUtils.dataFetched());

	}
}
