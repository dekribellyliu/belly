package id.investree.customer.controller.internal.partnerintegration;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.DefaultData;
import id.investree.core.model.TokenPayload;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.controller.external.bankinformation.BankInformationResponseGenerator;
import id.investree.customer.controller.external.businessprofile.BusinessProfileResponseGenerator;
import id.investree.customer.controller.external.legalinformation.LegalInformationResponseGenerator;
import id.investree.customer.controller.external.partnershipinformation.PartnershipInformationResponseGenerator;
import id.investree.customer.controller.external.personalprofile.PersonalProfileResponseGenerator;
import id.investree.customer.entity.*;
import id.investree.customer.model.data.BankInformationData;
import id.investree.customer.model.data.BusinessInfoData;
import id.investree.customer.model.data.LegalInformationData;
import id.investree.customer.model.data.PersonalProfileData;
import id.investree.customer.model.request.LegalInformationDataRequest;
import id.investree.customer.model.request.PartnersCustomerCompletingRequest;
import id.investree.customer.model.response.DefaultListResponse;
import id.investree.customer.model.response.internal.PartnersResponse;
import id.investree.customer.service.bankinformation.BankInformationService;
import id.investree.customer.service.businessprofile.BusinessProfileService;
import id.investree.customer.service.ciflist.CifListService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.legalinformation.LegalInformationService;
import id.investree.customer.service.mrinvarbank.MrInvarBankService;
import id.investree.customer.service.partnershipinformation.PartnershipInformationService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.LocalMessageUtils;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/internal/partner", produces = MediaType.APPLICATION_JSON_VALUE)
public class InternalPartnerRegistrationController extends BaseController {

	private TokenUtils tokenUtils;
	private LocalMessageUtils messageUtils;
	private BankInformationService bankInformationService;
	private UserDataService userDataService;
	private BusinessProfileService businessProfileService;
	private CustomerInformationService customerInformationService;
	private CustomerRoleService customerRoleService;
	private LegalInformationService legalInformationService;
	private CifListService cifListService;
	private MrInvarBankService invarBankService;
	private PartnershipInformationService partnershipInformationService;
	private BusinessProfileResponseGenerator businessProfileResponseGenerator;
	private BankInformationResponseGenerator bankInformationResponseGenerator;
	private LegalInformationResponseGenerator legalInformationResponseGenerator;
	private PersonalProfileResponseGenerator personalProfileResponseGenerator;
	private StoreDataGlobalParam completingDataGlobalParam;
	private PartnershipInformationResponseGenerator partnershipInformationResponseGenerator;

	@Autowired
	public InternalPartnerRegistrationController(
		TokenUtils tokenUtils,
		LocalMessageUtils messageUtils,
		BankInformationService bankInformationService,
		UserDataService userDataService,
		BusinessProfileService businessProfileService,
		CustomerInformationService customerInformationService,
		CustomerRoleService customerRoleService,
		LegalInformationService legalInformationService,
		CifListService cifListService,
		MrInvarBankService invarBankService,
		PartnershipInformationService partnershipInformationService,
		BusinessProfileResponseGenerator businessProfileResponseGenerator,
		BankInformationResponseGenerator bankInformationResponseGenerator,
		LegalInformationResponseGenerator legalInformationResponseGenerator,
		PersonalProfileResponseGenerator personalProfileResponseGenerator,
		StoreDataGlobalParam completingDataGlobalParam,
		PartnershipInformationResponseGenerator partnershipInformationResponseGenerator
	) {
		this.tokenUtils = tokenUtils;
		this.messageUtils = messageUtils;
		this.bankInformationService = bankInformationService;
		this.userDataService = userDataService;
		this.businessProfileService = businessProfileService;
		this.customerInformationService = customerInformationService;
		this.customerRoleService = customerRoleService;
		this.legalInformationService = legalInformationService;
		this.cifListService = cifListService;
		this.invarBankService = invarBankService;
		this.partnershipInformationService = partnershipInformationService;
		this.businessProfileResponseGenerator = businessProfileResponseGenerator;
		this.bankInformationResponseGenerator = bankInformationResponseGenerator;
		this.legalInformationResponseGenerator = legalInformationResponseGenerator;
		this.personalProfileResponseGenerator = personalProfileResponseGenerator;
		this.completingDataGlobalParam = completingDataGlobalParam;
		this.partnershipInformationResponseGenerator = partnershipInformationResponseGenerator;
	}

	@PostMapping("completing-data/{customerId}")
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> partnerCompletingData(HttpServletRequest http,
																@PathVariable("customerId") Long customerId,
																@RequestBody PartnersCustomerCompletingRequest request) throws MalformedURLException {

		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
				.orElseThrow(() -> new AppException(messageUtils.emptyToken()));

		CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.findById(customerId))
				.orElseThrow(DataNotFoundException::new);

		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(customerId))
				.orElseThrow(DataNotFoundException::new);

		CifList cifList = Optional.ofNullable(cifListService.findByCustomerId(customerId))
				.orElseThrow(() -> new DataNotFoundException("borrower number"));

		CustomerRole customerRole = Optional.ofNullable(customerRoleService.findByCustomerId(customerId))
				.orElseThrow(DataNotFoundException::new);
		customerRole.setStatus(GlobalConstants.BORROWER_STATUS_ACTIVE);
		customerRole.setActiveAt(new Date());
		customerRole.setActiveBy(tokenPayload.getUserId());
		customerRole.setUpdatedAt(new Date());
		customerRole.setUpdatedBy(tokenPayload.getUserId());

		CustomerRole customerRoleSave = Optional.ofNullable(customerRoleService.saveOrUpdate(customerRole))
				.orElseThrow(() -> new AppException(messageUtils.insertFailed("partner completing data in update status")));

		PersonalProfileData personalProfileData = new PersonalProfileData();
		if (Optional.ofNullable(request.getUserData()).isPresent()) {
			Optional.ofNullable(request.getUserData().getDomicileAddress()).ifPresent(userData::setDomicileAddress);
			Optional.ofNullable(request.getUserData().getDomicileProvince()).ifPresent(userData::setDomicileProvince);
			Optional.ofNullable(request.getUserData().getDomicileCity()).ifPresent(userData::setDomicileCity);
			Optional.ofNullable(request.getUserData().getDomicileDistrict()).ifPresent(userData::setDomicileDistrict);
			Optional.ofNullable(request.getUserData().getDomicileSubDistrict()).ifPresent(userData::setDomicileVillage);
			Optional.ofNullable(request.getUserData().getDomicileProvinceName()).ifPresent(userData::setDomicileProvinceName);
			Optional.ofNullable(request.getUserData().getDomicileCityName()).ifPresent(userData::setDomicileCityName);
			Optional.ofNullable(request.getUserData().getDomicileDistrictName()).ifPresent(userData::setDomicileDistrictName);
			Optional.ofNullable(request.getUserData().getDomicileSubDistrictName()).ifPresent(userData::setDomicileVillageName);
			Optional.ofNullable(request.getUserData().getDomicilePostalCode()).ifPresent(userData::setDomicilePostalCode);
			Optional.ofNullable(request.getUserData().getFillFinishAt()).ifPresent(userData::setFillFinishAt);

			UserData userDataSave = Optional.ofNullable(userDataService.saveOrUpdate(userData))
					.orElseThrow(() -> new AppException(messageUtils.insertFailed("partner completing data in user data")));

			personalProfileData =
					personalProfileResponseGenerator.generateBasicPersonalProfileData(userDataSave, customerInformation, http.getHeader(GlobalConstants.HEADER_TOKEN));
		}

		String ciNumber = generateCiNumber(
				GlobalConstants.USER_CATEGORY_BORROWER_ID,
				userData.getDomicileCity(),
				customerRoleSave.getRegisteredAt(),
				customerInformation.getUserCategory(),
				cifList.getId());

		Optional.ofNullable(request.getProductPrefRequest()).ifPresent(v -> {
			Optional.ofNullable(v.getCompanyName()).ifPresent(customerInformation::setName);
			Optional.ofNullable(v.getLegalEntity()).ifPresent(customerInformation::setLegalEntity);
			Optional.ofNullable(v.getProductPreference()).ifPresent(customerInformation::setProductPreference);
			Optional.ofNullable(request.getBusinessProfile()).ifPresent(businessProfile -> customerInformation.setIndustry(businessProfile.getIndustry()));
			customerInformation.setCustomerNumber(ciNumber);
			customerInformation.setUpdatedAt(new Date());
			customerInformation.setUpdatedBy(tokenPayload.getUserId());

			if (!Optional.ofNullable(customerInformationService.saveOrUpdate(customerInformation)).isPresent()) {
				throw new AppException(messageUtils.insertFailed("partner completing data in customer info"));
			}
		});

		BusinessInfoData businessInfoData = new BusinessInfoData();
		if (Optional.ofNullable(request.getBusinessProfile()).isPresent()) {
			BusinessProfile businessProfile = Optional.ofNullable(businessProfileService.findByCustomerId(customerId))
					.orElse(new BusinessProfile());
			businessProfile.setCustomerId(customerId);
			Optional.ofNullable(request.getBusinessProfile().getDateOfEstablishment()).ifPresent(businessProfile::setDateOfEstablishment);
			Optional.ofNullable(request.getBusinessProfile().getNumberOfEmployee()).ifPresent(businessProfile::setNumberOfEmployee);
			Optional.ofNullable(request.getBusinessProfile().getCompanyDescription()).ifPresent(businessProfile::setCompanyDescription);
			Optional.ofNullable(request.getBusinessProfile().getCompanyAddress()).ifPresent(businessProfile::setCompanyAddress);
			Optional.ofNullable(request.getBusinessProfile().getPostalCode()).ifPresent(businessProfile::setPostalCode);
			Optional.ofNullable(request.getBusinessProfile().getMobilePrefix()).ifPresent(businessProfile::setMobilePrefix);
			Optional.ofNullable(request.getBusinessProfile().getLandLineNumber()).ifPresent(businessProfile::setLandLineNumber);
			Optional.ofNullable(request.getBusinessProfile().getProvince()).ifPresent(val -> {
				if (val instanceof String) {
					businessProfile.setProvinceName(val.toString());
				}
			});
			Optional.ofNullable(request.getBusinessProfile().getCity()).ifPresent(val -> {
				if (val instanceof String) {
					businessProfile.setCityName(val.toString());
				}
			});
			Optional.ofNullable(request.getBusinessProfile().getDistrict()).ifPresent(val -> {
				if (val instanceof String) {
					businessProfile.setDistrictName(val.toString());
				}
			});
			Optional.ofNullable(request.getBusinessProfile().getVillage()).ifPresent(val -> {
				if (val instanceof String) {
					businessProfile.setVillageName(val.toString());
				}
			});

			BusinessProfile businessProfileSave = Optional.ofNullable(businessProfileService.saveOrUpdate(businessProfile))
					.orElseThrow(() -> new AppException(messageUtils.insertFailed("partner completing data in customer info")));

			businessInfoData = businessProfileResponseGenerator.generateBusinessInfoData(businessProfileSave, customerInformation,
					http.getHeader(GlobalConstants.HEADER_TOKEN), true);
		}

		List<BankInformation> bankInformationList = Optional.ofNullable(bankInformationService.findByCustomerIdAndType(customerId, Constants.BANK_TYPE_ACCOUNT))
				.orElse(new ArrayList<>());

		BankInformationData bankInformationData;
		if (bankInformationList.isEmpty() && Optional.ofNullable(request.getBankInformation()).isPresent()) {
			BankInformation bankInformation = new BankInformation();
			bankInformation.setCustomerId(customerId);
			bankInformation.setBankType(Constants.BANK_TYPE_ACCOUNT);
			bankInformation.setUseAsDisbursement(GlobalConstants.STATUS_YES);
			Optional.ofNullable(request.getBankInformation().getLegacyId()).ifPresent(bankInformation::setLegacyId);
			Optional.ofNullable(request.getBankInformation().getMasterBankId()).ifPresent(bankInformation::setMasterBankId);
			Optional.ofNullable(request.getBankInformation().getBankAccountNumber()).ifPresent(bankInformation::setBankAccountNumber);
			Optional.ofNullable(request.getBankInformation().getBankAccountHolderName()).ifPresent(bankInformation::setBankAccountHolderName);

			BankInformation bankInformationSave = Optional.ofNullable(bankInformationService.saveOrUpdate(bankInformation))
					.orElseThrow(() -> new AppException(messageUtils.insertFailed("partner completing data in bank info")));

			bankInformationData = bankInformationResponseGenerator.generateBankInformation(bankInformationSave,
					http.getHeader(GlobalConstants.HEADER_TOKEN), Long.valueOf(GlobalConstants.USER_CATEGORY_BORROWER_ID));

		} else if (Optional.ofNullable(request.getBankInformation()).isPresent()) {
			List<BankInformation> bankInformationSaveList = new ArrayList<>();
			for (BankInformation bankInformation : bankInformationList) {
				bankInformation.setUseAsDisbursement(GlobalConstants.STATUS_YES);
				Optional.ofNullable(request.getBankInformation().getLegacyId()).ifPresent(bankInformation::setLegacyId);
				Optional.ofNullable(request.getBankInformation().getMasterBankId()).ifPresent(bankInformation::setMasterBankId);
				Optional.ofNullable(request.getBankInformation().getBankAccountNumber()).ifPresent(bankInformation::setBankAccountNumber);
				Optional.ofNullable(request.getBankInformation().getBankAccountHolderName()).ifPresent(bankInformation::setBankAccountHolderName);

				BankInformation bankInformationSave = Optional.ofNullable(bankInformationService.saveOrUpdate(bankInformation))
						.orElseThrow(() -> new AppException(messageUtils.insertFailed("partner completing data in bank info")));

				bankInformationSaveList.add(bankInformationSave);
			}
			bankInformationData = bankInformationResponseGenerator.generateBankInformation(bankInformationSaveList.get(0),
					http.getHeader(GlobalConstants.HEADER_TOKEN), Long.valueOf(GlobalConstants.USER_CATEGORY_BORROWER_ID));
		} else {
			bankInformationData = bankInformationResponseGenerator.generateBankInformation(bankInformationList.get(0),
					http.getHeader(GlobalConstants.HEADER_TOKEN), Long.valueOf(GlobalConstants.USER_CATEGORY_BORROWER_ID));
		}

		Optional.ofNullable(request.getPartnershipInformation()).ifPresent(v -> {
			PartnershipInformation partnershipInformation =
					Optional.ofNullable(partnershipInformationService.findByCustomerIdAndPartnerId(v.getCustomerId(), v.getPartnerId()))
					.orElse(new PartnershipInformation());

			DefaultListResponse<DefaultData> jointVentureList =
					partnershipInformationService.jointVentureList(http.getHeader(GlobalConstants.HEADER_TOKEN));

			Optional.ofNullable(v.getCustomerId()).ifPresent(partnershipInformation::setCustomerId);
			Optional.ofNullable(v.getLegacyId()).ifPresent(partnershipInformation::setLegacyId);
			Optional.ofNullable(v.getType()).ifPresent(partnershipInformation::setType);
			Optional.ofNullable(v.getPartnerId()).ifPresent(partnerId -> {
				String partnerName = jointVentureList.getData().stream()
						.filter(partnerData -> partnerData.getId().equals(Math.toIntExact(v.getPartnerId())))
						.findFirst().map(DefaultData::getName).orElse(null);
				partnershipInformation.setPartnerId(partnerId);
				partnershipInformation.setName(partnerName);
			});
			partnershipInformation.setStartOfRelation(Utilities.isNull(partnershipInformation.getStartOfRelation()) ? new Date() :
					partnershipInformation.getStartOfRelation());
			partnershipInformation.setCreatedBy(tokenPayload.getUserId());
			partnershipInformation.setUpdateBy(tokenPayload.getUserId());

			if (!Optional.ofNullable(partnershipInformationService.saveOrUpdate(partnershipInformation)).isPresent()) {
				throw new AppException(messageUtils.insertFailed("partner completing data in partnership info"));
			}
		});

		List<LegalInformation> legalInformations = new ArrayList<>();

		if (Optional.ofNullable(request.getLegalInformation()).isPresent()
				&& !request.getLegalInformation().getData().isEmpty()) {
			for (LegalInformationDataRequest legalInformationDataRequest : request.getLegalInformation().getData()) {
				if (Utilities.isNull(legalInformationDataRequest.getDocumentType())) {
					throw new AppException(messageUtils.dataNotValid("document type"));
				}

				LegalInformation legalInformation =
						Optional.ofNullable(legalInformationService.findByCustomerIdAndType(customerId, legalInformationDataRequest.getDocumentType().getId()))
								.orElse(new LegalInformation());
				legalInformation.setCustomerId(customerId);
				legalInformation.setDocumentType(legalInformationDataRequest.getDocumentType().getId());
				Optional.ofNullable(legalInformationDataRequest.getDocumentFile()).ifPresent(legalInformation::setDocumentFile);
				Optional.ofNullable(legalInformationDataRequest.getDocumentNumber()).ifPresent(legalInformation::setDocumentNumber);
				Optional.ofNullable(legalInformationDataRequest.getDocumentRegisterDate()).ifPresent(legalInformation::setDocumentRegistered);
				Optional.ofNullable(legalInformationDataRequest.getDocumentExpiredDate()).ifPresent(legalInformation::setDocumentExpired);
				legalInformation.setCreatedBy(tokenPayload.getUserId());
				legalInformation.setUpdateBy(tokenPayload.getUserId());

				legalInformations.add(legalInformation);
			}

			if (!Optional.ofNullable(legalInformationService.saveAll(legalInformations)).isPresent()) {
				throw new AppException(messageUtils.insertFailed("partner completing data in legal info"));
			}
		}

		List<LegalInformationData> legalInformationData = legalInformationResponseGenerator.generatePartnerLegalInformation(customerId);

		Optional.ofNullable(request.getVaNumber()).ifPresent(vaNumber -> {
			invarBankService.findById(Constants.INVAR_BANK_ID_DANAMON_SYARIAH, Constants.INVAR_BANK_ID_DANAMON_BORROWER).forEach(invarBank -> {
				BankInformation bankVa = Optional.ofNullable(bankInformationService.
						findByCustomerIdAndTypeAndMasterBankId(customerId, Constants.BANK_TYPE_VA, invarBank.getBankId())).orElse(new BankInformation());
				bankVa.setCustomerId(customerId);
				bankVa.setCustomerRoleId(customerRoleSave.getId());
				bankVa.setBankType(Constants.BANK_TYPE_VA);
				bankVa.setMasterBankId(invarBank.getBankId());
				bankVa.setBankAccountNumber(vaNumber);

				if (Utilities.isEmptyOrBlank(request.getVaStatus()) || GlobalConstants.STATUS_YES.equalsIgnoreCase(request.getVaStatus())) {
					bankVa.setVerifiedStatus(Constants.VA_STATUS_VERIFIED);

				} else if (GlobalConstants.STATUS_NO.equalsIgnoreCase(request.getVaStatus()) ||
						GlobalConstants.STATUS_DEFAULT.equalsIgnoreCase(request.getVaStatus())) {
					bankVa.setVerifiedStatus(Constants.VA_STATUS_UNVERIFIED);

				}
				Optional.ofNullable(request.getBankInformation().getLegacyId()).ifPresent(bankVa::setLegacyId);
				Optional.ofNullable(request.getBankInformation().getBankAccountHolderName()).ifPresent(bankVa::setBankAccountHolderName);

				if (!Optional.ofNullable(bankInformationService.saveOrUpdate(bankVa)).isPresent()) {
					throw new AppException(messageUtils.updateFailed());
				}
			});
		});

		String borrowerNumber = Optional.ofNullable(cifListService.findByCustomerId(customerId))
				.map(v -> v.getId().toString()).orElse(null);

		PartnersResponse partnersResponse = new PartnersResponse.PartnersResponseBuilder()
				.setProductPreference(customerInformation.getProductPreference())
				.setCustomerId(customerId)
				.setBorrowerNumber(borrowerNumber)
				.setUserData(userData, completingDataGlobalParam)
				.setPersonalProfileData(personalProfileData)
				.setBusinessInfoData(businessInfoData)
				.setBankInformationData(bankInformationData)
				.setLegalInformationDataList(legalInformationData).build();

		return abstractResponseHandler(partnersResponse).getResult(messageUtils.insertSuccess());
	}

	@GetMapping("customer-info/{customerId}")
	public ResponseEntity<ResultResponse> getCustomerInfo(@PathVariable(value = "customerId") Long customerId) {

		CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.findById(customerId))
				.orElseThrow(() -> new DataNotFoundException("customer information"));

		List<PartnershipInformation> partnershipInformation = Optional.ofNullable(partnershipInformationService.findAllByCustomerId(customerId))
				.orElseThrow(() -> new DataNotFoundException("partnership information"));

		CifList cifList = Optional.ofNullable(cifListService.findByCustomerId(customerId))
				.orElseThrow(() -> new DataNotFoundException("borrower number"));

		CustomerRole customerRole = Optional.ofNullable(customerRoleService.findByCustomerIdAndType(customerId, GlobalConstants.USER_CATEGORY_BORROWER_ID))
				.orElseThrow(() -> new DataNotFoundException("customer role"));

		PartnersResponse partnersResponse = new PartnersResponse.PartnersResponseBuilder()
				.setCustomerId(customerInformation.getId())
				.setBorrowerNumber(cifList.getId().toString())
				.setCustomerInformation(customerInformation)
				.setPartnershipInformation(
					partnershipInformationResponseGenerator.generatePartnershipInformationList(partnershipInformation
				))
				.setCrStatus(customerRole.getStatus())
				.build();

		return abstractResponseHandler(partnersResponse).getResult(messageUtils.dataFetched());
	}

	private String generateCiNumber(Integer userRoleType, Long domicile, Date registeredAt,
									String userCategory, Long borrowerNumber) {
		SimpleDateFormat formatMMYY = new SimpleDateFormat("MMyy");
		String sequenceId = String.format("%06d", borrowerNumber);
		String domicileCity = "";
		if (!Utilities.isNull(domicile)) {
			domicileCity = String.valueOf(domicile);
		}

		return userRoleType
				+ "." + userCategory
				+ "." + domicileCity
				+ "." + formatMMYY.format(registeredAt)
				+ "." + sequenceId;
	}
}
