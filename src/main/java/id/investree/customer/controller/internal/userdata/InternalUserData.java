package id.investree.customer.controller.internal.userdata;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.CustomerProductList;
import id.investree.customer.entity.UserData;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.model.response.internal.UserDataWithProductResponse;
import id.investree.customer.service.customerproductlist.CustomerProductListService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/internal/user-data", produces = MediaType.APPLICATION_JSON_VALUE)
public class InternalUserData extends BaseController {

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private LoginDataService loginDataService;

	@Autowired
	private CustomerProductListService customerProductListService;

	@Autowired
	private CustomerRoleService customerRoleService;

	@Autowired
	private LocalMessageUtils messageUtils;

	@GetMapping("{customerId}")
	public ResponseEntity<ResultResponse> findUserData(@PathVariable("customerId") Long customerId) {
		UserData userData = userDataService.findByCustomerId(customerId);
		return abstractResponseHandler(userData).getResult(messageUtils.dataFetched());
	}

	@GetMapping
	public ResponseEntity<ResultResponse> findByKtp(@RequestParam(value = "ktp", required = false) String ktp,
	                                                @RequestParam(value = "loginId", required = false) Long loginId) {
		UserData userData = null;
		if (!Utilities.isEmptyOrBlank(ktp)) {
			userData = userDataService.findByIdCardNumber(ktp);
		} else if (!Utilities.isNull(loginId)) {
			userData = userDataService.findByLoginId(loginId);
		} else {
			throw new AppException(messageUtils.dataNotValid("params not found"));
		}

		List<CustomerProductList> productLists = null;
		if (null != userData && null != userData.getCustomerId()) {
			List<Long> customerRoleIds = new ArrayList<>();
			Optional.ofNullable(customerRoleService.findAllByCustomerId(userData.getCustomerId()))
				.ifPresent(cr -> cr.forEach(v -> customerRoleIds.add(v.getId())));

			Long[] customerIdInArray = new Long[customerRoleIds.size()];
			customerIdInArray = customerRoleIds.toArray(customerIdInArray);
			productLists = customerProductListService.findByCustomerRoleIds(customerIdInArray);
		}

		UserDataWithProductResponse response = new UserDataWithProductResponse();
		response.setUserData(userData);
		response.setProducts(productLists);
		return abstractResponseHandler(response).getResult(messageUtils.dataFetched());
	}

	@GetMapping("/login-data/{customerId}")
	public ResponseEntity<ResultResponse> findLoginDataByCustomerId(HttpServletRequest http,
	                                                                @PathVariable("customerId") Long customerId) {
		String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);
		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(customerId))
			.orElseThrow(() -> new DataNotFoundException("user data"));

		LoginDataResponse loginDataResponse = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
			.orElseThrow(() -> new DataNotFoundException("login data"));

		return abstractResponseHandler(loginDataResponse).getResult(messageUtils.dataFetched());
	}
}
