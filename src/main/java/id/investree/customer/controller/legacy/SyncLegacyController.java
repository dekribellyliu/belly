package id.investree.customer.controller.legacy;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.*;
import id.investree.customer.model.request.ShareholderInformationSyncLegacyRequest;
import id.investree.customer.model.request.SyncLegacyRequest;
import id.investree.customer.model.response.ShareholderInformationSyncLegacyResponse;
import id.investree.customer.service.bankinformation.BankInformationService;
import id.investree.customer.service.businessprofile.BusinessProfileService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.partnershipinformation.PartnershipInformationService;
import id.investree.customer.service.shareholdersinformation.ShareholdersInformationService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.LocalMessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/legacy", produces = MediaType.APPLICATION_JSON_VALUE)
public class SyncLegacyController extends BaseController {

	@Autowired
	private CustomerInformationService customerInformationService;

	@Autowired
	private ShareholdersInformationService shareholdersInformationService;

	@Autowired
	private BusinessProfileService businessProfileService;

	@Autowired
	private BankInformationService bankInformationService;

	@Autowired
	private PartnershipInformationService partnershipInformationService;

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private LocalMessageUtils localMessageUtils;

	@PostMapping(value = "/save/{customerId}")
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<ResultResponse> saveLegacy(@PathVariable(value = "customerId") Long customerId,
	                                                 @RequestBody SyncLegacyRequest request) {

		CustomerInformation customerInformation = Optional.ofNullable(customerInformationService.findById(customerId))
			.orElseThrow(() -> new DataNotFoundException("customer information"));

		Optional.ofNullable(request.getCustomerInfo_Initial()).ifPresent(v -> {
			customerInformation.setInitial(v);
			customerInformationService.saveOrUpdate(customerInformation);
		});

		Optional.ofNullable(userDataService.findByCustomerId(customerId)).ifPresent(userData -> saveUserData(userData, request));
		Optional.ofNullable(bankInformationService.findByCustomerIdAndIsUseAsDisbursement(customerId)).ifPresent(bankInformation -> saveBankInformation(bankInformation, request));
		Optional.ofNullable(partnershipInformationService.findByCustomerId(customerId)).ifPresent(partnershipInformation -> savePartnershipInformation(partnershipInformation, request));
		Optional.ofNullable(businessProfileService.findByCustomerId(customerId)).ifPresent(businessProfile -> saveBusinessProfile(businessProfile, request));

		if (!Utilities.isNull(request.getShareholderInformation())) {
			ShareholdersInformation shareholdersInformation = shareholdersInformationService.findByIdOrCreate(request.getShareholderInformation().getShareholderInfo_id());
			ShareholdersInformation save = saveShareholderInformation(shareholdersInformation, request.getShareholderInformation(), customerId);

			ShareholderInformationSyncLegacyResponse response = new ShareholderInformationSyncLegacyResponse();
			response.setShi_id(save.getId());
			return abstractResponseHandler(response).getResult(localMessageUtils.dataFetched(), HttpStatus.OK);
		}

		return abstractResponseHandler(true).getResult(localMessageUtils.insertSuccess());
	}

	private void saveUserData(UserData userData, SyncLegacyRequest request) {
		Optional.ofNullable(request.getUserData_domicileAddress()).ifPresent(userData::setDomicileAddress);
		Optional.ofNullable(request.getUserData_domicileProvince()).ifPresent(userData::setDomicileProvince);
		Optional.ofNullable(request.getUserData_domicileCity()).ifPresent(userData::setDomicileCity);
		Optional.ofNullable(request.getUserData_domicileDistrict()).ifPresent(userData::setDomicileDistrict);
		Optional.ofNullable(request.getUserData_domicileVillage()).ifPresent(userData::setDomicileVillage);
		Optional.ofNullable(request.getUserData_domicilePostalCode()).ifPresent(userData::setDomicilePostalCode);
		Optional.ofNullable(request.getUserData_dateOfBirth()).ifPresent(userData::setDateOfBirth);
		Optional.ofNullable(request.getUserData_idCardExpired()).ifPresent(userData::setIdCardExpired);
		userDataService.saveOrUpdate(userData);
	}

	private void saveBankInformation(BankInformation bankInformation, SyncLegacyRequest request) {
		Optional.ofNullable(request.getBankInfo_masterBankId()).ifPresent(bankInformation::setMasterBankId);
		Optional.ofNullable(request.getBankInfo_bankAccountHolderName()).ifPresent(bankInformation::setBankAccountHolderName);
		Optional.ofNullable(request.getBankInfo_bankAccountNumber()).ifPresent(bankInformation::setBankAccountNumber);
		bankInformationService.saveOrUpdate(bankInformation);
	}

	private void savePartnershipInformation(PartnershipInformation partnershipInformation, SyncLegacyRequest request) {
		Optional.ofNullable(request.getPartnershipInfo_buildingLocOwnership()).ifPresent(partnershipInformation::setBuildingLocOwnership);
		Optional.ofNullable(request.getPartnershipInfo_internalRating()).ifPresent(partnershipInformation::setInternalRating);
		Optional.ofNullable(request.getPartnershipInfo_externalRating()).ifPresent(partnershipInformation::setExternalRating);
		partnershipInformationService.saveOrUpdate(partnershipInformation);
	}

	private void saveBusinessProfile(BusinessProfile businessProfile, SyncLegacyRequest request) {
		Optional.ofNullable(request.getBusinessData_Description()).ifPresent(businessProfile::setCompanyDescription);
		Optional.ofNullable(request.getBusinessData_numberOfEmployee()).ifPresent(businessProfile::setNumberOfEmployee);
		Optional.ofNullable(request.getBusinessData_doe()).ifPresent(businessProfile::setDateOfEstablishment);
		businessProfileService.saveOrUpdate(businessProfile);
	}

	private ShareholdersInformation saveShareholderInformation(ShareholdersInformation shareholdersInformation,
	                                                           ShareholderInformationSyncLegacyRequest request,
	                                                           Long customerId) {

		shareholdersInformation.setCustomerId(customerId);
		Optional.ofNullable(request.getShareholderInfo_fullName()).ifPresent(shareholdersInformation::setFullName);
		Optional.ofNullable(request.getShareholderInfo_position()).ifPresent(shareholdersInformation::setPosition);
		Optional.ofNullable(request.getShareholderInfo_mobilePrefix()).ifPresent(shareholdersInformation::setMobilePrefix);
		Optional.ofNullable(request.getShareholderInfo_mobileNumber()).ifPresent(shareholdersInformation::setMobileNumber);
		Optional.ofNullable(request.getShareholderInfo_dob()).ifPresent(shareholdersInformation::setDob);
		Optional.ofNullable(request.getShareholderInfo_identificationCardUrl()).ifPresent(shareholdersInformation::setIdentificationCardUrl);
		Optional.ofNullable(request.getShareholderInfo_identificationCardNumber()).ifPresent(shareholdersInformation::setIdentificationCardNumber);
		Optional.ofNullable(request.getShareholderInfo_taxCardUrl()).ifPresent(shareholdersInformation::setTaxCardUrl);
		Optional.ofNullable(request.getShareholderInfo_taxCardNumber()).ifPresent(shareholdersInformation::setTaxCardNumber);
		Optional.ofNullable(request.getShareholderInfo_stockOwnership()).ifPresent(shareholdersInformation::setStockOwnership);
		Optional.ofNullable(request.getShareholderInfo_isLss()).ifPresent(shareholdersInformation::setIsLss);
		Optional.ofNullable(request.getShareholderInfo_isPgs()).ifPresent(shareholdersInformation::setIsPgs);
		Optional.ofNullable(request.getShareholderInfo_pgFile()).ifPresent(shareholdersInformation::setPgFile);
		Optional.ofNullable(request.getShareholderInfo_pgNumber()).ifPresent(shareholdersInformation::setPgNumber);
		Optional.ofNullable(request.getShareholderInfo_pgSignedDate()).ifPresent(shareholdersInformation::setPgSignedDate);
		Optional.ofNullable(request.getShareholderInfo_pgAmount()).ifPresent(shareholdersInformation::setPgAmount);
		Optional.ofNullable(request.getShareholderInfo_emailAddress()).ifPresent(shareholdersInformation::setEmailAddress);
		Optional.ofNullable(request.getShareholderInfo_identificationCardExpiryDate()).ifPresent(shareholdersInformation::setIdentificationCardExpiryDate);
		Optional.ofNullable(request.getShareholderInfo_deletedAt()).ifPresent(shareholdersInformation::setDeletedAt);
		return shareholdersInformationService.saveOrUpdate(shareholdersInformation);
	}
}
