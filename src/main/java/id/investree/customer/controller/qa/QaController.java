package id.investree.customer.controller.qa;

import id.investree.core.base.BaseController;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.MissingTokenException;
import id.investree.core.exception.UnauthorizeException;
import id.investree.core.model.TokenPayload;
import id.investree.core.model.legacy.LegacyModel;
import id.investree.core.utils.TokenUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.BankInformation;
import id.investree.customer.model.legacy.LegacyModelBuilder;
import id.investree.customer.service.bankinformation.BankInformationService;
import id.investree.customer.service.legacy.SyncLegacyService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.LocalMessageUtils;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class QaController extends BaseController {


	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private BankInformationService bankInformationService;

	@Autowired
	private LocalMessageUtils messageUtils;

	@Autowired
	private SyncLegacyService syncLegacyService;

	@Autowired
	private HttpServletResponse httpServletResponse;

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	@Transactional(rollbackFor = Exception.class)
	@PutMapping("unverify-va/{userType}/{customerId}")
	public ResponseEntity<ResultResponse> unverifyVaStatus(HttpServletRequest http,
														   @PathVariable("userType") String userRoleType,
														   @PathVariable("customerId") Long customerId) throws IOException {

		if (GlobalConstants.PRODUCTION.equals(Utilities.getEnvironment())) {
			httpServletResponse.sendError(HttpStatus.NOT_FOUND.value(), "Not Found");
		}

		TokenPayload tokenPayload = Optional.ofNullable(tokenUtils.parseToken(http))
				.orElseThrow(MissingTokenException::new);

		if (!GlobalConstants.USER_TYPE_BACKOFFICE_ID.equals(tokenPayload.getUserType()) ||
				GlobalConstants.USER_CATEGORY_LENDER_NAME.equalsIgnoreCase(userRoleType)) {
			throw new UnauthorizeException();
		}

		List<BankInformation> bankInformation = bankInformationService.findByCustomerIdAndType(customerId, Constants.BANK_TYPE_VA);

		if (!bankInformation.isEmpty()) {
			bankInformation.forEach(v -> {
				v.setVerifiedStatus(Constants.VA_STATUS_UNVERIFIED);
				v.setUpdatedAt(new Date());
				v.setUpdateBy(tokenPayload.getUserId());

				if (!Optional.ofNullable(bankInformationService.saveOrUpdate(v)).isPresent()) {
					throw new AppException(messageUtils.updateFailed());
				}
			});
		}

		LegacyModel legacyModel = new LegacyModelBuilder()
				.setBankInformations(bankInformation)
			.setGlobalParam(completingDataGlobalParam)
			.build(customerId);
		syncLegacyService.sync(http.getHeader(GlobalConstants.HEADER_TOKEN), legacyModel);

		return abstractResponseHandler(true).getResult(messageUtils.updateSuccess());
	}
}
