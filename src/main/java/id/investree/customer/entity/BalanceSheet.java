package id.investree.customer.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "balance_sheet")
public class BalanceSheet {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "bs_id")
	private Long id;

	@Column(name = "bs_ci_id")
	private Long customerId;

	@Column(name = "bs_year_to")
	private Long yearTo;

	@Column(name = "bs_acc_receiv")
	private Double accReceive;

	@Column(name = "bs_investory")
	private Double investory;

	@Column(name = "bs_acc_payable")
	private Double accPayable;

	@Column(name = "bs_bank_debt")
	private Double bankDebt;

	@Column(name = "bs_current_assets")
	private Double currentAssets;

	@Column(name = "bs_current_liabilites")
	private Double currentLiabilities;

	@Column(name = "bs_total_liabilites")
	private Double totalLiabilities;

	@Column(name = "bs_equity")
	private Double equity;

	@Column(name = "bs_created_by")
	private Long createdBy;

	@Column(name = "bs_created_at")
	@CreationTimestamp
	private Date createdAt;

	@Column(name = "bs_update_by")
	private Long updateBy;

	@Column(name = "bs_update_at")
	@UpdateTimestamp
	private Date updateAt;

}
