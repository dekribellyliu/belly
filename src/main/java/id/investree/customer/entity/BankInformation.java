package id.investree.customer.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "bank_information")
public class BankInformation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "bi_id")
	private Long id;

	@Column(name = "bi_ci_id")
	private Long customerId;

	@Column(name = "bi_legacy_id")
	private Long legacyId;

	@Column(name = "bi_type")
	private Integer bankType;

	@Column(name = "bi_bank_account_cover_file")
	private String bankAccountCoverFile;

	@Column(name = "bi_bank_id")
	private Long masterBankId;

	@Column(name = "bi_bank_account_number")
	private String bankAccountNumber;

	@Column(name = "bi_bank_account_holder")
	private String bankAccountHolderName;

	@Column(name = "bi_use_as_disbursement")
	private String useAsDisbursement;

	@Column(name = "bi_withdrawal")
	private String useAsWithdrawal;
	
	@Column(name= "bi_anchor_disbursement")
	private String isAnchorDisbursement;

	@Column(name = "bi_created_by")
	private Long createdBy;

	@Column(name = "bi_update_by")
	private Long updateBy;

	@Column(name = "bi_deleted_by")
	private Long deletedBy;

	@Column(name = "bi_created_at")
	@CreationTimestamp
	private Date createdAt;

	@Column(name = "bi_update_at")
	@UpdateTimestamp
	private Date updatedAt;

	@Column(name = "bi_deleted_at")
	private Date deletedAt;

	@Column(name = "bi_fill_finish_at")
	private Date fillFinishAt;

	@Column(name = "bi_status")
	private String verifiedStatus;

	@Column(name = "bi_cr_id")
	private Long customerRoleId;

	@Column(name = "bi_mib_id")
	private Long invarBank;

}