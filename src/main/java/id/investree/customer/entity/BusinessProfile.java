package id.investree.customer.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "business_data")
public class BusinessProfile {

	@Id
	@Column(name = "bd_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	private Long id;

	@JsonIgnore
	@Column(name = "bd_ci_id")
	private Long customerId;

	@Column(name = "bd_doe")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	private Date dateOfEstablishment;

	@Column(name = "bd_noe")
	private Integer numberOfEmployee;

	@Column(name = "bd_description")
	private String companyDescription;

	@Column(name = "bd_address")
	private String companyAddress;

	@Column(name = "bd_province")
	private Long province;

	@Column(name = "bd_province_txt")
	private String provinceName;

	@Column(name = "bd_city")
	private Long city;

	@Column(name = "bd_city_txt")
	private String cityName;

	@Column(name = "bd_district")
	private Long district;

	@Column(name = "bd_district_txt")
	private String districtName;

	@Column(name = "bd_village")
	private Long village;

	@Column(name = "bd_village_txt")
	private String villageName;

	@Column(name = "bd_postal_code")
	private String postalCode;

	@Column(name = "bd_land_line_number")
	private String landLineNumber;

	@Column(name = "bd_mobile_prefix")
	private String mobilePrefix;

	@JsonIgnore
	@Column(name = "bd_created_at")
	@CreationTimestamp
	private Date createdAt;

	@JsonIgnore
	@Column(name = "bd_created_by")
	private Long createdBy;

	@JsonIgnore
	@Column(name = "bd_update_by")
	private Long updatedBy;

	@JsonIgnore
	@Column(name = "bd_update_at")
	@UpdateTimestamp
	private Date updatedAt;

	@JsonIgnore
	@Column(name = "bd_fill_finish_at")
	private Date fillFinishAt;

	@Column(name = "bd_group_company")
	private String groupCompany;

	@Column(name = "bd_group_description")
	private String groupDescription;

	@Column(name = "bd_lop")
	private String listOfPayor;

	@Column(name = "bd_lrwb")
	private String relationshipWithBank;

	@Column(name = "bd_ams")
	private Double averageMonthlySales;

	@Column(name = "bd_profit_margin")
	private Double profitMargin;

	@Column(name = "bd_mailing_address_source")
	private Integer mailingAddressStatus;

	@Column(name = "bd_other_income")
	private Double otherIncome;

	@Column(name = "bd_gpm")
	private Double generalProfitMargin;

	@Column(name = "bd_pmfp")
	private Double profitMarginFromPartner;

	@Column(name = "bd_tnm")
	private Double totalNettMargin;

	@Column(name = "bd_living_cost")
	private Double livingCost;

	@Column(name = "bd_bomn")
	private String businessNarration;

}
