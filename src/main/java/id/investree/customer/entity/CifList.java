package id.investree.customer.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "cif_list")
public class CifList {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cl_id")
	private Long id;

	@Column(name = "cl_ci_id")
	private Long customerId;

	@Column(name = "cl_created_by")
	private Long createdBy;

	@Column(name = "cl_created_at")
	@CreationTimestamp
	private Date createdAt;
}
