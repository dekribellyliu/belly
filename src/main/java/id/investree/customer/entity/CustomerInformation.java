package id.investree.customer.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "customer_information")
public class CustomerInformation {

	@Id
	@Column(name = "ci_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "ci_prod_pref")
	private Integer productPreference;

	@Column(name = "ci_user_category")
	private String userCategory;

	@Column(name = "ci_name")
	private String name;

	@Column(name = "ci_nationality")
	private Long nationality;

	@Column(name = "ci_legal_entity")
	private Integer legalEntity;

	@Column(name = "ci_kbli_code")
	private String industry;

	@Column(name = "ci_annual_income")
	private Long annualIncome;

	@Column(name = "ci_source_of_fund")
	private Long sourceOfFund;

	@Column(name = "ci_investment_objective")
	private Long investmentObjective;

	@Column(name = "ci_risk_profile")
	private Long riskProfile;

	@JsonIgnore
	@Column(name = "ci_created_by")
	private Long createdBy;

	@JsonIgnore
	@Column(name = "ci_created_at")
	@CreationTimestamp
	private Date createdAt;

	@JsonIgnore
	@Column(name = "ci_update_by")
	private Long updatedBy;

	@JsonIgnore
	@Column(name = "ci_update_at")
	@UpdateTimestamp
	private Date updatedAt;

	@Column(name = "ci_initial")
	private String initial;

	@Column(name = "ci_number")
	private String customerNumber;

}
