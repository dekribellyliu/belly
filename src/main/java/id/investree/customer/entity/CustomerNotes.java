package id.investree.customer.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "customer_notes")
public class CustomerNotes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cn_id")
    private Long id;

    @Column(name = "cn_ci_id")
    @JsonIgnore
    private Long customerId;

    @Column(name = "cn_mct_id")
    @JsonIgnore
    private Long customerType;

    @Column(name = "cn_note")
    @JsonIgnore
    private String note;

    @Column(name = "cn_created_by")
    private Long createdBy;

    @Column(name = "cn_created_at")
    @CreationTimestamp
    private Date createdAt;

    @Column(name = "cn_update_by")
    @JsonIgnore
    private Long updateBy;

    @Column(name = "cn_update_at")
    @JsonIgnore
    @UpdateTimestamp
    private Date updateAt;

}
