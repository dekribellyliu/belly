package id.investree.customer.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "customer_product_list")
public class CustomerProductList {

	@Id
	@Column(name = "cpl_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "cpl_cr_id")
	private Long customerRoleId;

	@Column(name = "cpl_legacy_id")
	private Long legacyId;

	@Column(name = "cpl_type")
	private Long type;

	@JsonIgnore
	@Column(name = "cpl_created_by")
	private Long createdBy;

	@JsonIgnore
	@Column(name = "cpl_created_at")
	@CreationTimestamp
	private Date createdAt;

	@JsonIgnore
	@Column(name = "cpl_update_by")
	private Long customerProductList_updatedBy;

	@JsonIgnore
	@Column(name = "cpl_update_at")
	@UpdateTimestamp
	private Date customerProductList_updatedAt;

	@JsonIgnore
	@Column(name = "cpl_deleted_by")
	private Long deletedBy;

	@JsonIgnore
	@Column(name = "cpl_deleted_at")
	private Date deletedAt;

}
