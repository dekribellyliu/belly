package id.investree.customer.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "customer_role")
public class CustomerRole {

	@Id
	@Column(name = "cr_id")
	@JsonIgnore
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "cr_ci_id")
	private Long customerInformationId;

	@Column(name = "cr_type")
	private Integer type;

	@Column(name = "cr_status")
	private Long status;

	@Column(name = "cr_created_by")
	private Long createdBy;

	@Column(name = "cr_created_at")
	@CreationTimestamp
	private Date createdAt;

	@Column(name = "cr_update_by")
	private Long updatedBy;

	@Column(name = "cr_update_at")
	@UpdateTimestamp
	private Date updatedAt;

	@Column(name = "cr_registered_at")
	private Date registeredAt;

	@Column(name = "cr_poa_file")
	private String poaFile;

	@Column(name = "cr_rm_by")
	private Long rmBy;

	@Column(name = "cr_active_at")
	private Date activeAt;

	@Column(name = "cr_active_by")
	private Long activeBy;

	@Column(name = "cr_service_fee_bullet_repayment")
	private Double lsfBullet;

	@Column(name = "cr_service_fee_installment")
	private Double installment;

	@Column(name = "cr_service_fee_status")
	private String feeStatus;

	@Column(name = "cr_auto_withdrawal_status")
	private String autoWithdrawalStatus;

	@Column(name = "cr_withholding_tax_status")
	private String withHoldingTaxStatus;

	@Column(name = "cr_number")
	private String customerNumber;

	@Column(name = "cr_fill_finish_at")
	private Date fillFinishAt;

}
