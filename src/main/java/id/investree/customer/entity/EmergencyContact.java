package id.investree.customer.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "emergency_contact")
public class EmergencyContact {

	@Id
	@Column(name = "ec_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "ec_ci_id")
	private Long customerId;

	@Column(name = "ec_relationship")
	private Long relationship;

	@Column(name = "ec_fullname")
	private String fullname;

	@Column(name = "ec_mobile_prefix")
	private Long mobilePrefix;

	@Column(name = "ec_mobile_number")
	private String mobileNumber;

	@Column(name = "ec_email_address")
	private String emailAddress;

	@Column(name = "ec_address")
	private String address;

	@Column(name = "ec_province")
	private Long province;

	@Column(name = "ec_city")
	private Long city;

	@Column(name = "ec_district")
	private Long district;

	@Column(name = "ec_village")
	private Long village;

	@Column(name = "ec_postal_code")
	private String postalCode;

	@Column(name = "ec_identity_card_url")
	private String identityCardUrl;

	@Column(name = "ec_identity_card_number")
	private String identityCardNumber;

	@Column(name = "ec_identity_card_expired")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	private Date identityCardExpired;

	@Column(name = "ec_created_by")
	private Long createdBy;

	@Column(name = "ec_update_by")
	private Long updateBy;

	@Column(name = "ec_update_at")
	@UpdateTimestamp
	private Date updateAt;
	
	@Column(name = "ec_deleted_at")
	private Date deletedAt;

	@Column(name = "ec_deleted_by")
	private Long deleteBy;

}
