package id.investree.customer.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "financial_information")
public class FinancialInformation {

	@Id
	@Column(name = "fi_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "fi_ci_id")
	@JsonIgnore
	private Long customerId;

	@Column(name = "fi_financial_statement_file_type")
	private Integer statementFileType;

	@Column(name = "fi_financial_statement_url")
	private String statementUrl;

	@Column(name = "fi_financial_statement_file_at")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	private Date statementFileDate;

	@JsonIgnore
	@Column(name = "fi_created_at")
	@CreationTimestamp
	private Date createdAt;

	@Column(name = "fi_created_by")
	@JsonIgnore
	private Long createdBy;

	@Column(name = "fi_update_by")
	@JsonIgnore
	private Long updatedBy;

	@Column(name = "fi_update_at")
	@JsonIgnore
	@UpdateTimestamp
	private Date updatedAt;

	@Column(name = "fi_deleted_at")
	@JsonIgnore
	private Date deletedAt;

	@Column(name = "fi_deleted_by")
	@JsonIgnore
	private Long deleteBy;

}
