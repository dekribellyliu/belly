package id.investree.customer.entity;

import lombok.Data;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "financial_ratio")
public class FinancialRatio {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "fr_id")
	private Long id;

	@Column(name = "fr_ci_id")
	private Long customerId;

	@Column(name = "fr_year_to")
	private Long yearTo;

	@Column(name = "fr_gpm")
	private Double gpm;

	@Column(name = "fr_npm")
	private Double npm;

	@Column(name = "fr_ardoh")
	private Double ardoh;

	@Column(name = "fr_invdoh")
	private Double invdoh;

	@Column(name = "fr_apdoh")
	private Double apdoh;

	@Column(name = "fr_cash_cycle")
	private Double cashCycle;

	@Column(name = "fr_cash_ratio")
	private Double cashRatio;

	@Column(name = "fr_ebitda")
	private Double ebitda;

	@Column(name = "fr_leverage")
	private Double leverage;

	@Column(name = "fr_wi_needs")
	private Double wiNeeds;

	@Column(name = "fr_tie")
	private Double tie;

	@Column(name = "fr_dscr")
	private Double dscr;

	@Column(name = "fr_created_by")
	private Long createdBy;

	@Column(name = "fr_created_at")
	private Date createdAt;

	@Column(name = "fr_update_by")
	private Long updateBy;

	@Column(name = "fr_update_at")
	@UpdateTimestamp
	private Date updateAt;

}
