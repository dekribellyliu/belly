package id.investree.customer.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "financial_statement")
public class FinancialStatement {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "fs_id")
	private Long id;

	@Column(name = "fs_ci_id")
	private Long customerId;

	@Column(name = "fs_year_to")
	private Long yearTo;

	@Column(name = "fs_fiscal_year")
	private Date fiscalYear;

	@Column(name = "fs_sales")
	private Double sales;

	@Column(name = "fs_cogs")
	private Double cogs;

	@Column(name = "fs_gross_profit")
	private Double grossProfit;

	@Column(name = "fs_sga")
	private Double sga;

	@Column(name = "fs_depreciation")
	private Double depreciation;

	@Column(name = "fs_operating_profit")
	private Double operatingProfit;

	@Column(name = "fs_interest_expense")
	private Double interestExpense;

	@Column(name = "fs_other_income")
	private Double otherIncome;

	@Column(name = "fs_other_expense")
	private Double otherExpense;

	@Column(name = "fs_profit_before_tax")
	private Double profitBeforeTax;

	@Column(name = "fs_tax")
	private Double tax;

	@Column(name = "fs_profit_after_tax")
	private Double profitAfterTax;

	@Column(name = "fs_created_by")
	private Long createdBy;

	@Column(name = "fs_created_at")
	@CreationTimestamp
	private Date createdAt;

	@Column(name = "fs_update_by")
	private Long updateBy;

	@Column(name = "fs_update_at")
	@UpdateTimestamp
	private Date updateAt;

	@Column(name = "fs_efi")
	private Double existingFacility;

}
