package id.investree.customer.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "financial_trend")
public class FinancialTrend {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ft_id")
	private Long id;

	@Column(name = "ft_ci_id")
	private Long customerId;

	@Column(name = "ft_trend_period")
	private String trendPeriod;

	@Column(name = "ft_sales")
	private Double sales;

	@Column(name = "ft_cogs")
	private Double cogs;

	@Column(name = "ft_gross_profit")
	private Double grossProfit;

	@Column(name = "ft_sga")
	private Double sga;

	@Column(name = "ft_operating_profit")
	private Double operatingProfit;

	@Column(name = "ft_profit_before_tax")
	private Double profitBeforeTax;

	@Column(name = "ft_profit_after_tax")
	private Double profitAfterTax;

	@Column(name = "ft_created_by")
	private Long createdBy;

	@Column(name = "ft_created_at")
	@CreationTimestamp
	private Date createdAt;

	@Column(name = "ft_update_by")
	private Long updateBy;

	@Column(name = "ft_update_at")
	@UpdateTimestamp
	private Date updateAt;

	@Column(name = "ft_efi")
	private Double existingFacility;

}
