package id.investree.customer.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Data
@Entity
@Table(name = "legal_information")
public class LegalInformation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "li_id")
	private Long id;

	@Column(name = "li_ci_id")
	private Long customerId;

	@Column(name = "li_doc_type")
	private Integer documentType;

	@Column(name = "li_doc_file")
	private String documentFile;

	@Column(name = "li_doc_number")
	private String documentNumber;

	@Temporal(TemporalType.DATE)
	@Column(name = "li_doc_expired")
	private Date documentExpired;

	@Temporal(TemporalType.DATE)
	@Column(name = "li_doc_registered")
	private Date documentRegistered;

	@Column(name = "li_created_by")
	private Long createdBy;

	@Column(name = "li_update_by")
	private Long updateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "li_created_at")
	@CreationTimestamp
	private Date createdAt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "li_update_at")
	@UpdateTimestamp
	private Date updatedAt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "li_fill_finish_at")
	private Date fillFinishAt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "li_deleted_at")
	private Date deletedAt;

	@Column(name = "li_deleted_by")
	private Long deletedBy;

}
