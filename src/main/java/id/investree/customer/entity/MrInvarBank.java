package id.investree.customer.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "mr_invar_bank")
public class MrInvarBank {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "mib_id")
	private Long id;

	@Column(name = "mib_mb_id")
	private Long bankId;

	@Column(name = "mib_bank_name")
	private String bankName;

	@Column(name = "mib_bank_account_name")
	private String bankAccountName;

	@Column(name = "mib_bank_no")
	private String bankNo;

	@Column(name = "mib_bank_bin")
	private String bankBin;

	@Column(name = "mib_bank_va")
	private String bankVa;

	@Column(name = "mib_type")
	private Long type;

}
