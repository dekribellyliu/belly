package id.investree.customer.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "partnership_info")
public class PartnershipInformation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pi_id")
	private Long id;

	@Column(name = "pi_ci_id")
	private Long customerId;

	@Column(name = "pi_legacy_id")
	private Long legacyId;

	@Column(name = "pi_type")
	private Long type;

	@Column(name = "pi_partner_id")
	private Long partnerId;

	@Column(name = "pi_name")
	private String name;

	@Column(name = "pi_product_category")
	private Long category;

	@Column(name = "pi_sor")
	private Date startOfRelation;

	@Column(name = "pi_seller_id")
	private String sellerId;

	@Column(name = "pi_seller_link")
	private String sellerLink;

	@Column(name = "pi_seller_location")
	private Long sellerLocation;

	@Column(name = "pi_blo")
	private Long buildingLocOwnership;

	@Column(name = "pi_loo")
	private String lengthOfOwnership;

	@Column(name = "pi_iecr")
	private String internalRating;

	@Column(name = "pi_eecr")
	private String externalRating;

	@Column(name = "pi_initial_loan_amount ")
	private Double initialLoanAmount;

	@Column(name = "pi_initial_loan_tenor ")
	private Long initialLoanTenor;

	@Column(name = "pi_max_amount ")
	private Double maxAmount;

	@Column(name = "pi_max_tenor ")
	private Long maxTenor;

	@Column(name = "pi_created_by")
	private Long createdBy;

	@Column(name = "pi_created_at")
	@CreationTimestamp
	private Date createdAt;

	@Column(name = "pi_update_by")
	private Long updateBy;

	@Column(name = "pi_update_at")
	@UpdateTimestamp
	private Date updateAt;

}
