package id.investree.customer.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "pic_job_role")
public class PicRole {

	@Id
	@Column(name = "pjr_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	private Long id;

	@Column(name = "pjr_cr_id")
	private Long customerRoleId;

	@Column(name = "pjr_ld_id")
	private Long loginId;

	@Column(name = "pjr_role")
	private Integer role;
	
	@Column(name = "pjr_status")
	private String status;

	@Column(name = "pjr_created_by")
	private Long createdBy;

	@Column(name = "pjr_created_at")
	@CreationTimestamp
	private Date createdAt;

	@Column(name = "pjr_update_by")
	private Long updateBy;

	@Column(name = "pjr_update_at")
	@UpdateTimestamp
	private Date updateAt;

	@Column(name = "pjr_deleted_by")
	private Long deletedBy;

	@Column(name = "pjr_deleted_at")
	private Date deletedAt;

}
