package id.investree.customer.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "referral_data")
public class ReferralData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "rfd_id")
	private Long id;

	@Column(name = "rfd_referral_user_id")
	private Long referralUserId;

	@Column(name = "rfd_referrer_user_id")
	private Long referrerUserId;

	@Column(name = "rfd_referrer_code")
	private String referrerCode;

	@Column(name = "rfd_referral_expired_date")
	private Date referralExpiredDate;

	@Column(name = "rfd_kif")
	private Long knowInvestreeFrom;

}
