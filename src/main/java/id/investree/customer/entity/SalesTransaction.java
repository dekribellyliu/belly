package id.investree.customer.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "sales_transaction")
public class SalesTransaction {

    @Id
    @Column(name = "st_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "st_ci_id")
    @JsonIgnore
    private Long customerId;

    @Column(name = "st_date")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
    private Date date;

    @Column(name = "st_amount")
    private Double amount;

    @Column(name = "st_transaction")
    private Double transaction;

    @Column(name = "st_created_by")
    @JsonIgnore
    private Long createdBy;

    @Column(name = "st_created_at")
    @JsonIgnore
    @CreationTimestamp
    private Date createdAt;

    @Column(name = "st_update_by")
    @JsonIgnore
    private Long updateBy;

    @Column(name = "st_update_at")
    @JsonIgnore
    @UpdateTimestamp
    private Date updateAt;

    @Column(name = "st_deleted_at")
    @JsonIgnore
    private Date deletedAt;

    @Column(name = "st_deleted_by")
    @JsonIgnore
    private Long deletedBy;

}
