package id.investree.customer.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Data
@Entity
@Table(name = "shareholder_information")
public class ShareholdersInformation {

	@Id
	@Column(name = "shi_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "shi_ci_id")
	private Long customerId;

	@Column(name = "shi_position")
	private Long position;

	@Column(name = "shi_fullname")
	private String fullName;

	@Column(name = "shi_mobile_prefix")
	private Long mobilePrefix;

	@Column(name = "shi_mobile_number")
	private String mobileNumber;

	@Column(name = "shi_email_address")
	private String emailAddress;

	@Column(name = "shi_stock_ownership_rate")
	private Float stockOwnership;

	@Column(name="shi_dob")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	private Date dob;

	@Column(name = "shi_identity_card_file")
	private String identificationCardUrl;

	@Column(name = "shi_identity_card_number")
	private String identificationCardNumber;

	@Column(name = "shi_identity_card_expired")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	private Date identificationCardExpiryDate;

	@Column(name = "shi_selfie_file")
	private String selfieUrl;

	@Column(name = "shi_tax_card_file")
	private String taxCardUrl;

	@Column(name = "shi_tax_card_number")
	private String taxCardNumber;

	@Column(name = "shi_is_lss")
	private String isLss;

	@Column(name = "shi_is_pgs")
	private String isPgs;

	@Column(name = "shi_is_tss")
	private String isTss;

	@Column(name = "shi_created_at")
	@CreationTimestamp
	private Date createdAt;

	@Column(name = "shi_created_by")
	private Long createdBy;

	@Column(name = "shi_update_by")
	private Long updatedBy;

	@Column(name = "shi_update_at")
	@UpdateTimestamp
	private Date updatedAt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "shi_fill_finish_at")
	private Date fillFinishAt;

	@Column(name = "shi_deleted_by")
	private Long deletedBy;

	@Column(name = "shi_deleted_at")
	private Date deletedAt;

	@Column(name = "shi_appt_cd")
	private Date apuPptDate;

	@Column(name = "shi_appt_cf")
	private String apuPptFile;

	@Column(name = "shi_appt_cr")
	private String apuPptResult;

	@Column(name = "shi_pg_no")
	private String pgNumber;

	@Column(name = "shi_pg_amount")
	private Long pgAmount;

	@Column(name = "shi_pg_sd")
	private Date pgSignedDate;

	@Column(name = "shi_pg_type")
	private String pgType;

	@Column(name = "shi_pg_file")
	private String pgFile;

}
