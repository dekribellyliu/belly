package id.investree.customer.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "survey_info")
public class SurveyInformation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "svi_id")
	private Long id;

	@Column(name = "svi_ci_id")
	private Long customerId;

	@Column(name = "svi_survey_by")
	private Long loginId;

	@Column(name = "svi_survey_date")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	private Date surveyDate;

	@Column(name = "svi_br_position")
	private Long borrowerPosition;

	@Column(name = "svi_br_name")
	private String borrowerName;

	@Column(name = "svi_number_of_employees")
	private Long numberOfEmployees;

	@Column(name = "svi_msos_id")
	private Long officeStatus;

	@Column(name = "svi_length_of_stay")
	private String lengthOfStay;

	@Column(name = "svi_file")
	private String filename;

	@Column(name = "svi_result_desc")
	private String resultDescription;

	@Column(name = "svi_address")
	private String address;

	@Column(name = "svi_created_by")
	private Long createdBy;

	@Column(name = "svi_created_at")
	@CreationTimestamp
	private Date createdAt;

	@Column(name = "svi_update_by")
	private Long updateBy;

	@Column(name = "svi_update_at")
	@UpdateTimestamp
	private Date updateAt;

	@Column(name = "svi_deleted_by")
	private Long deletedBy;

	@Column(name = "svi_deleted_at")
	private Date deletedAt;

}
