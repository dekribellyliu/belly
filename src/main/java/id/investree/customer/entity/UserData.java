package id.investree.customer.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "user_data")
public class UserData {
	@Id
	@Column(name = "ud_id")
	@JsonIgnore
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "ud_ld_id")
	private Long loginDataId;

	@Column(name = "ud_ci_id")
	private Long customerId;

	@Column(name = "ud_marital_status")
	private Long maritalStatus;

	@Column(name = "ud_company_position")
	private Long companyPosition;

	@Column(name = "ud_company_division")
	private Long companyDivision;

	@Column(name = "ud_selfie_file")
	private String selfieFile;

	@Column(name = "ud_identity_card_file")
	private String idCardFile;

	@Column(name = "ud_selfie_with_ktp")
	private String selfieKtpFile;

	@Column(name = "ud_signature_file")
	private String signatureFile;

	@Column(name = "ud_identity_card_number")
	private String idCardNumber;

	@Column(name = "ud_identity_card_expired")
	private Date idCardExpired;

	@Column(name = "ud_nationality")
	private Long nationality;

	@Column(name = "ud_same_address")
	private String sameAddress;

	@Column(name = "ud_ktp_address")
	private String ktpAddress;

	@Column(name = "ud_ktp_province")
	private Long ktpProvince;

	@Column(name = "ud_ktp_city")
	private Long ktpCity;

	@Column(name = "ud_ktp_district")
	private Long ktpDistrict;

	@Column(name = "ud_ktp_village")
	private Long ktpVillage;

	@Column(name = "ud_ktp_postal_code")
	private String ktpPostalCode;

	@Column(name = "ud_pob")
	private Long placeOfBirth;

	@Column(name = "ud_ext_pob")
	private String placeOfBirthExternal;

	@Column(name = "ud_dob")
	private Date dateOfBirth;

	@Column(name = "ud_religion")
	private Long religion;

	@Column(name = "ud_education")
	private Long education;

	@Column(name = "ud_occupation")
	private Long occupation;

	@Column(name = "ud_field_of_work")
	private Long fieldOfWork;

	@Column(name = "ud_domicile_address")
	private String domicileAddress;

	@Column(name = "ud_domicile_province")
	private Long domicileProvince;

	@Column(name = "ud_domicile_province_txt")
	private String domicileProvinceName;

	@Column(name = "ud_domicile_city")
	private Long domicileCity;

	@Column(name = "ud_domicile_city_txt")
	private String domicileCityName;

	@Column(name = "ud_domicile_district")
	private Long domicileDistrict;

	@Column(name = "ud_domicile_district_txt")
	private String domicileDistrictName;

	@Column(name = "ud_domicile_village")
	private Long domicileVillage;

	@Column(name = "ud_domicile_village_txt")
	private String domicileVillageName;

	@Column(name = "ud_domicile_postal_code")
	private String domicilePostalCode;

	@Column(name = "ud_partner_province")
	private String partnerProvince;

	@Column(name = "ud_partner_city")
	private String partnerCity;

	@Column(name = "ud_partner_district")
	private String partnerDistrict;

	@Column(name = "ud_partner_village")
	private String partnerVillage;

	@Column(name = "ud_partner_postal_code")
	private String partnerPostalCode;

	@Column(name = "ud_mother_maiden_name")
	private String motherName;

	@Column(name = "ud_created_by")
	private Long createdBy;

	@Column(name = "ud_created_at")
	@CreationTimestamp
	private Date createdAt;

	@Column(name = "ud_update_by")
	private Long updatedBy;

	@Column(name = "ud_update_at")
	@UpdateTimestamp
	private Date updatedAt;

	@Column(name = "ud_fill_finish_at")
	private Date fillFinishAt;

}
