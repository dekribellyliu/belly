package id.investree.customer.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "user_role")
public class UserRole {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ur_id")
	private Long id;

	@Column(name = "ur_ld_id")
	private Long ldId;

	@Column(name = "ur_mur_id")
	private Long murId;

	@Column(name = "ur_active")
	private String active;

	@Column(name = "ur_available")
	private String available;

}
