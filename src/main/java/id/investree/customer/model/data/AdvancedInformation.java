package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AdvancedInformation {
	@JsonProperty("personalProfile")
	private CompletingData personalProfile;

	@JsonProperty("businessProfile")
	private CompletingData businessProfile;

	@JsonProperty("legalInformation")
	private CompletingData legalInformation;

	@JsonProperty("bankInformation")
	private CompletingData bankInformation;

	@JsonProperty("financialInformation")
	private CompletingData financialInformation;

	@JsonProperty("financialStatementDocuments")
	private CompletingData financialStatementDocuments;

	@JsonProperty("eStatementDocuments")
	private CompletingData eStatementDocuments;

	@JsonProperty("salesTransaction")
	private CompletingData salesTransaction;

	@JsonProperty("customerNotes")
	private CompletingData customerNotes;

	@JsonProperty("emergencyContact")
	private CompletingData emergencyContact;

	@JsonProperty("productPreference")
	private CompletingData productPreference;

	@JsonProperty("surveyInformation")
	private CompletingData surveyInformation;

	@JsonProperty("shareHolderInformation")
	private CompletingData shareHolderInformation;

	@JsonProperty("referralInformation")
	private CompletingData referralInformation;

	@JsonProperty("backgroundCheckInformation")
	private CompletingData backgroundCheckInformation;

	@JsonProperty("partnershipInformation")
	private CompletingData partnershipInformation;

	@JsonProperty("financialStatement")
	private CompletingData financialStatement;

	@JsonProperty("financialTrend")
	private CompletingData financialTrend;

	@JsonProperty("balanceSheet")
	private CompletingData balanceSheet;

	@JsonProperty("financialRatio")
	private CompletingData financialRatio;

	@JsonProperty("investmentProfile")
	private CompletingData investmentProfile;

	@JsonProperty("activationDocument")
	private CompletingData activationDocument;

}