package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class AdvancedInformationBorrower extends AdvancedInformationElementary {
	@JsonProperty("financialInformation")
	private CompletingData financialInformation;

	@JsonProperty("salesTransaction")
	private CompletingData salesTransaction;

	@JsonProperty("surveyInformation")
	private CompletingData surveyInformation;

	@JsonProperty("partnershipInformation")
	private CompletingData partnershipInformation;

	@JsonProperty("financialStatement")
	private CompletingData financialStatement;

	@JsonProperty("financialTrend")
	private CompletingData financialTrend;

	@JsonProperty("balanceSheet")
	private CompletingData balanceSheet;

	@JsonProperty("financialRatio")
	private CompletingData financialRatio;

	@JsonProperty("financialStatementDocuments")
	private CompletingData financialStatementDocuments;

	@JsonProperty("eStatementDocuments")
	private CompletingData eStatementDocuments;
}
