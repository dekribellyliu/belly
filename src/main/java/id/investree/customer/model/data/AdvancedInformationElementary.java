package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AdvancedInformationElementary {
	@JsonProperty("personalProfile")
	private CompletingData personalProfile;

	@JsonProperty("businessProfile")
	private CompletingData businessProfile;

	@JsonProperty("legalInformation")
	private CompletingData legalInformation;

	@JsonProperty("bankInformation")
	private CompletingData bankInformation;

	@JsonProperty("customerNotes")
	private CompletingData customerNotes;

	@JsonProperty("emergencyContact")
	private CompletingData emergencyContact;

	@JsonProperty("shareHolderInformation")
	private CompletingData shareHolderInformation;

	@JsonProperty("productPreference")
	private CompletingData productPreference;

	@JsonProperty("referralInformation")
	private CompletingData referralInformation;

	@JsonProperty("backgroundCheckInformation")
	private CompletingData backgroundCheckInformation;
}
