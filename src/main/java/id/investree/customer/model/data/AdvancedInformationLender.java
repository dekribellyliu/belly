package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class AdvancedInformationLender extends AdvancedInformationElementary {

	@JsonProperty("investmentProfile")
	private CompletingData investmentProfile;

	@JsonProperty("activationDocument")
	private CompletingData activationDocument;
}
