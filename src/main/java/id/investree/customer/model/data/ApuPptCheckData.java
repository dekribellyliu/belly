package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.DefaultData;
import lombok.Data;

import java.util.Date;

@Data
public class ApuPptCheckData {

	@JsonProperty("apuPptId")
	private Long apuPptId;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	@JsonProperty("checkingDate")
	private Date checkingDate;

	@JsonProperty("checkingFile")
	private String checkingFile;

	@JsonProperty("checkingResult")
	private DefaultData checkingResult;

	public ApuPptCheckData() {}

	public ApuPptCheckData(Long apuPptId, Date checkingDate, String checkingFile, DefaultData checkingResult) {
		this.apuPptId = apuPptId;
		this.checkingDate = checkingDate;
		this.checkingFile = checkingFile;
		this.checkingResult = checkingResult;
	}
}
