package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BackgroundCheckData {
	@JsonProperty("apuPptCheckData")
	private ApuPptCheckData apuPptCheckData;
}
