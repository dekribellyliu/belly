package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BalanceSheetData {
	@JsonProperty("balanceSheetId")
	private Long balanceSheetId;

	@JsonProperty("yearTo")
	private Long yearTo;

	@JsonProperty("accReceive")
	private Double accReceive;

	@JsonProperty("investory")
	private Double investory;

	@JsonProperty("accPayable")
	private Double accPayable;

	@JsonProperty("bankDebt")
	private Double bankDebt;

	@JsonProperty("currentAssets")
	private Double currentAssets;

	@JsonProperty("currentLiabilities")
	private Double currentLiabilities;

	@JsonProperty("totalLiabilities")
	private Double totalLiabilities;

	@JsonProperty("equity")
	private Double equity;
}
