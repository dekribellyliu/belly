package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.DefaultData;
import lombok.Data;

@Data
public class BankInformationData {
	@JsonProperty("bankInformationId")
	private Long bankInformationId;

	@JsonProperty("bankType")
	private DefaultData bankType;

	@JsonProperty("masterBank")
	private DefaultData masterBank;

	@JsonProperty("bankAccountCoverFile")
	private String bankAccountCoverFile;

	@JsonProperty("bankAccountNumber")
	private String bankAccountNumber;

	@JsonProperty("bankAccountHolderName")
	private String bankAccountHolderName;

	@JsonProperty("verifiedStatus")
	private String verifiedStatus;

	@JsonProperty("useAsDisbursement")
	private boolean useAsDisbursement;

	@JsonProperty("anchorDisbursement")
	private boolean isAnchorDisbursement;

	@JsonProperty("useAsWithdrawal")
	private boolean useAsWithdrawal;

	@JsonProperty("newAnchorBank")
	private boolean isNewAnchorBank;

	@JsonProperty("isDelete")
	private boolean isDelete;
}