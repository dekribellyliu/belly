package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class BankInformationParent {
	@JsonProperty("va")
	private List<BankInformationData> va;

	@JsonProperty("bankAccount")
	private List<BankInformationData> bankAccount;
}
