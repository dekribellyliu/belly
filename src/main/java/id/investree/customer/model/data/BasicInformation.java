package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.DefaultData;
import id.investree.customer.model.response.RegisterStatusResponse;
import lombok.Data;

import java.util.List;

@Data
public class BasicInformation {
	@JsonProperty("customerId")
	private Long customerId;

	@JsonProperty("rmBy")
	private Long rmBy;

	@JsonProperty("userCategory")
	private DefaultData userCategory;

	@JsonProperty("profilePicture")
	private String profilePicture;

	@JsonProperty("salutation")
	private String salutation;

	@JsonProperty("fullName")
	private String fullName;

	@JsonProperty("username")
	private String username;

	@JsonProperty("emailAddress")
	private String emailAddress;

	@JsonProperty("emailStatus")
	private Boolean emailStatus;

	@JsonProperty("mobilePrefix")
	private DefaultData mobilePrefix;

	@JsonProperty("mobileNumber")
	private String mobileNumber;

	@JsonProperty("customerIdentificationNumber")
	private String customerIdentificationNumber;

	@JsonProperty("maritalStatus")
	private DefaultData maritalStatus;

	@JsonProperty("age")
	private Integer age;

	@JsonProperty("status")
	private DefaultData status;

	@JsonProperty("education")
	private DefaultData education;

	@JsonProperty("occupation")
	private DefaultData occupation;

	@JsonProperty("nationality")
	private DefaultData nationality;

	@JsonProperty("positionWithInstitution")
	private DefaultData positionWithInstitution;

	@JsonProperty("divisionWithInstitution")
	private DefaultData divisionWithInstitution;

	@JsonProperty("userRoleTypes")
	private List<DefaultData> userRoleTypes;

	@JsonProperty("referralCode")
	private String referralCode;

	@JsonProperty("agreeSubscribe")
	private Boolean agreeSubscribe;

	@JsonProperty("lsfBullet")
	private Double lsfBullet;

	@JsonProperty("installment")
	private Double installment;

	@JsonProperty("feeStatus")
	private Boolean feeStatus;

	@JsonProperty("autoWithdrawalStatus")
	private Boolean autoWithdrawalStatus;

	@JsonProperty("withHoldingTaxStatus")
	private Boolean withHoldingTaxStatus;

	@JsonProperty("registrationStatus")
	private RegisterStatusResponse registrationStatus;

	@JsonProperty("borrowerInitial")
	private String borrowerInitial;
}
