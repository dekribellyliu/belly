package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.DefaultData;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class BasicInformationBorrower extends BasicInformationElementary{
	@JsonProperty("customerId")
	private Long customerId;

	@JsonProperty("rmBy")
	private Long rmBy;

	@JsonProperty("userCategory")
	private DefaultData userCategory;

	@JsonProperty("profilePicture")
	private String profilePicture;

	@JsonProperty("emailStatus")
	private Boolean emailStatus;

	@JsonProperty("mobilePrefix")
	private DefaultData mobilePrefix;

	@JsonProperty("mobileNumber")
	private String mobileNumber;

	@JsonProperty("age")
	private Integer age;

	@JsonProperty("status")
	private DefaultData status;

	@JsonProperty("userRoleTypes")
	private List<DefaultData> userRoleTypes;

	@JsonProperty("agreeSubscribe")
	private Boolean agreeSubscribe;

	@JsonProperty("borrowerInitial")
	private String borrowerInitial;


	public BasicInformationBorrower(BasicInformation basicInformation) {
		super(basicInformation);
		customerId = basicInformation.getCustomerId();
		rmBy = basicInformation.getRmBy();
		userCategory = basicInformation.getUserCategory();
		profilePicture = basicInformation.getProfilePicture();
		emailStatus = basicInformation.getEmailStatus();
		mobilePrefix = basicInformation.getMobilePrefix();
		mobileNumber = basicInformation.getMobileNumber();
		age = basicInformation.getAge();
		status = basicInformation.getStatus();
		userRoleTypes = basicInformation.getUserRoleTypes();
		agreeSubscribe = basicInformation.getAgreeSubscribe();
		borrowerInitial = basicInformation.getBorrowerInitial();
	}
}
