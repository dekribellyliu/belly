package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.DefaultData;
import lombok.Data;

@Data
public class BasicInformationElementary {
	@JsonProperty("salutation")
	private String salutation;

	@JsonProperty("fullName")
	private String fullName;

	@JsonProperty("username")
	private String username;

	@JsonProperty("emailAddress")
	private String emailAddress;

	@JsonProperty("mobileNumber")
	private String mobileNumber;

	@JsonProperty("customerIdentificationNumber")
	private String customerIdentificationNumber;

	@JsonProperty("emailStatus")
	private Boolean emailStatus;

	@JsonProperty("registrationStatus")
	private DefaultData registrationStatus;

	@JsonProperty("mobilePrefix")
	private DefaultData mobilePrefix;

	@JsonProperty("nationality")
	private DefaultData nationality;

	@JsonProperty("positionWithInstitution")
	private DefaultData positionWithInstitution;

	@JsonProperty("divisionWithInstitution")
	private DefaultData divisionWithInstitution;

	public BasicInformationElementary(BasicInformation basicInformation) {
		salutation = basicInformation.getSalutation();
		fullName = basicInformation.getFullName();
		username = basicInformation.getUsername();
		emailAddress = basicInformation.getEmailAddress();
		mobileNumber = basicInformation.getMobileNumber();
		customerIdentificationNumber = basicInformation.getCustomerIdentificationNumber();
		nationality = basicInformation.getNationality();
		positionWithInstitution = basicInformation.getPositionWithInstitution();
		divisionWithInstitution = basicInformation.getDivisionWithInstitution();
		mobilePrefix = basicInformation.getMobilePrefix();
		emailStatus = basicInformation.getEmailStatus();
		registrationStatus = basicInformation.getStatus();
	}
}

