package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.DefaultData;
import id.investree.customer.model.response.RegisterStatusResponse;
import lombok.Data;

import java.util.List;

@Data
public class BasicInformationFrontofficeBorrower {

	@JsonProperty("customerId")
	private Long customerId;

	@JsonProperty("customerIdentificationNumber")
	private String customerIdentificationNumber;

	@JsonProperty("userCategory")
	private DefaultData userCategory;

	@JsonProperty("profilePicture")
	private String profilePicture;

	@JsonProperty("salutation")
	private String salutation;

	@JsonProperty("nationality")
	private DefaultData nationality;

	@JsonProperty("emailAddress")
	private String emailAddress;

	@JsonProperty("emailStatus")
	private Boolean emailStatus;

	@JsonProperty("username")
	private String username;

	@JsonProperty("fullName")
	private String fullName;

	@JsonProperty("mobilePrefix")
	private DefaultData mobilePrefix;

	@JsonProperty("mobileNumber")
	private String mobileNumber;

	@JsonProperty("maritalStatus")
	private DefaultData maritalStatus;

	@JsonProperty("age")
	private Integer age;

	@JsonProperty("status")
	private DefaultData status;

	@JsonProperty("education")
	private DefaultData education;

	@JsonProperty("occupation")
	private DefaultData occupation;

	@JsonProperty("positionWithInstitution")
	private DefaultData positionWithInstitution;

	@JsonProperty("divisionWithInstitution")
	private DefaultData divisionWithInstitution;

	@JsonProperty("userRoleTypes")
	private List<DefaultData> userRoleTypes;

	@JsonProperty("registrationStatus")
	private RegisterStatusResponse registrationStatus;

	@JsonProperty("referralCode")
	private String referralCode;

	@JsonProperty("withHoldingTaxStatus")
	private Boolean withHoldingTaxStatus;

	@JsonProperty("lsfBullet")
	private Double lsfBullet;

	@JsonProperty("agreeSubscribe")
	private Boolean agreeSubscribe;

	@JsonProperty("feeStatus")
	private Boolean feeStatus;

	@JsonProperty("installment")
	private Double installment;

	@JsonProperty("autoWithdrawalStatus")
	private Boolean autoWithdrawalStatus;
}
