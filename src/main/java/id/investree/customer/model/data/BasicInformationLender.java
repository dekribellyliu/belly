package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.DefaultData;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;


@Data
@EqualsAndHashCode(callSuper = true)
public class BasicInformationLender extends BasicInformationElementary {

	@JsonProperty("customerId")
	private Long customerId;

	@JsonProperty("userCategory")
	private DefaultData userCategory;

	@JsonProperty("profilePicture")
	private String profilePicture;

	@JsonProperty("emailStatus")
	private Boolean emailStatus;

	@JsonProperty("mobilePrefix")
	private DefaultData mobilePrefix;

	@JsonProperty("mobileNumber")
	private String mobileNumber;

	@JsonProperty("age")
	private Integer age;

	@JsonProperty("status")
	private DefaultData status;

	@JsonProperty("userRoleTypes")
	private List<DefaultData> userRoleTypes;

	@JsonProperty("referralCode")
	private String referralCode;

	@JsonProperty("agreeSubscribe")
	private Boolean agreeSubscribe;

	@JsonProperty("lsfBullet")
	private Double lsfBullet;

	@JsonProperty("installment")
	private Double installment;

	@JsonProperty("feeStatus")
	private Boolean feeStatus;

	@JsonProperty("autoWithdrawalStatus")
	private Boolean autoWithdrawalStatus;

	@JsonProperty("withHoldingTaxStatus")
	private Boolean withHoldingTaxStatus;

	public BasicInformationLender(BasicInformation basicInformation) {
		super(basicInformation);
		customerId = basicInformation.getCustomerId();
		userCategory = basicInformation.getUserCategory();
		profilePicture = basicInformation.getProfilePicture();
		emailStatus = basicInformation.getEmailStatus();
		mobilePrefix = basicInformation.getMobilePrefix();
		mobileNumber = basicInformation.getMobileNumber();
		age = basicInformation.getAge();
		status = basicInformation.getStatus();
		userRoleTypes = basicInformation.getUserRoleTypes();
		agreeSubscribe = basicInformation.getAgreeSubscribe();
		referralCode = basicInformation.getReferralCode();
		lsfBullet = basicInformation.getLsfBullet();
		installment = basicInformation.getInstallment();
		feeStatus = basicInformation.getFeeStatus();
		autoWithdrawalStatus = basicInformation.getAutoWithdrawalStatus();
		withHoldingTaxStatus = basicInformation.getWithHoldingTaxStatus();
	}
}
