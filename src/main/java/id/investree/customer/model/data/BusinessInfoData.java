package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.DefaultData;
import lombok.Data;

import java.util.Date;

@Data
public class BusinessInfoData {
	@JsonProperty("legalEntity")
	private DefaultData legalEntity;

	@JsonProperty("companyName")
	private String companyName;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	@JsonProperty("dateOfEstablishment")
	private Date dateOfEstablishment;

	@JsonProperty("numberOfEmployee")
	private Integer numberOfEmployee;

	@JsonProperty("companyDescription")
	private String companyDescription;

	@JsonProperty("companyNarration")
	private String companyNarration;

	@JsonProperty("companyAddress")
	private String companyAddress;

	@JsonProperty("nationality")
	private DefaultData nationality;

	@JsonProperty("province")
	private DefaultData province;

	@JsonProperty("city")
	private DefaultData city;

	@JsonProperty("district")
	private DefaultData district;

	@JsonProperty("village")
	private DefaultData village;

	@JsonProperty("industry")
	private Industry industry;

	@JsonProperty("postalCode")
	private String postalCode;

	@JsonProperty("landLineNumber")
	private String landLineNumber;

	@JsonProperty("groupCompany")
	private String groupCompany;

	@JsonProperty("groupDescription")
	private String groupDescription;

	@JsonProperty("listOfPayor")
	private String listOfPayor;

	@JsonProperty("relationshipWithBank")
	private boolean relationshipWithBank;

	@JsonProperty("averageMonthlySales")
	private Double averageMonthlySales;

	@JsonProperty("profitMargin")
	private Double profitMargin;

	@JsonProperty("mailingAddressStatus")
	private Integer mailingAddressStatus;

	@JsonProperty("borrowerInitial")
	private String borrowerInitial;

}
