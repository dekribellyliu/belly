package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CompletingData {
	@JsonProperty("field")
	private Object field;

	@JsonProperty("counter")
	private CounterData counter;
}
