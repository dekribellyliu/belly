package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.customer.model.response.RegisterStatusResponse;
import lombok.Data;

import java.util.List;

@Data
public class CompletingInitialData {

	@JsonProperty("customerId")
	private Long customerId;

	@JsonProperty("userCategory")
	private String userCategory;

	@JsonProperty("productPreference")
	private Integer productPreference;

	@JsonProperty("userRoleTypes")
	private List<Integer> userRoleTypes;

	@JsonProperty("nationality")
	private Long nationality;

	@JsonProperty("borrowerNumber")
	private String borrowerNumber;

	@JsonProperty("registrationStatus")
	private RegisterStatusResponse registrationStatus;
}
