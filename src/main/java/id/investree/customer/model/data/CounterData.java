package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CounterData {
	@JsonProperty("filled")
	private Integer filled = 0;

	@JsonProperty("total")
	private Integer total = 0;

	public CounterData(Integer filled, Integer total) {
		this.filled = filled;
		this.total = total;
	}
}
