package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class CustomerNotesDataList {
	@JsonProperty("customerNotesData")
	private List<CustomerNotesData> customerNotesData;
}

