package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.DefaultData;
import lombok.Data;

@Data
public class CustomerPreferenceData {
	@JsonProperty("companyName")
	private String companyName;

	@JsonProperty("legalEntity")
	private DefaultData legalEntity;

	@JsonProperty("userCategory")
	private DefaultData userCategory;

	@JsonProperty("productPreference")
	private DefaultData productPreference;

	@JsonProperty("productSelection")
	private DefaultData productSelection;
}