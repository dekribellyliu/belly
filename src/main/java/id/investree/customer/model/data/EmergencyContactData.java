package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.DefaultData;
import lombok.Data;

import java.util.Date;

@Data
public class EmergencyContactData {
	@JsonProperty("emergencyId")
	private Long emergencyId;

	@JsonProperty("relationship")
	private DefaultData relationship;

	@JsonProperty("fullName")
	private String fullName;

	@JsonProperty("mobilePrefix")
	private DefaultData mobilePrefix;

	@JsonProperty("mobileNumber")
	private String mobileNumber;

	@JsonProperty("emailAddress")
	private String emailAddress;

	@JsonProperty("address")
	private String address;

	@JsonProperty("province")
	private DefaultData province;

	@JsonProperty("city")
	private DefaultData city;

	@JsonProperty("district")
	private DefaultData district;

	@JsonProperty("village")
	private DefaultData village;

	@JsonProperty("postalCode")
	private String postalCode;

	@JsonProperty("identityCardUrl")
	private String identityCardUrl;

	@JsonProperty("identityCardNumber")
	private String identityCardNumber;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	@JsonProperty("identityExpiryDate")
	private Date identityExpiryDate;

	@JsonProperty("idCardLifetime")
	private Boolean idCardLifetime;
}
