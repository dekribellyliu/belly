package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class FinancialData {
	@JsonProperty("financialStatements")
	private List<FinancialDataDetail> financialStatements;

	@JsonProperty("eStatements")
	private List<FinancialDataDetail> eStatements;
}
