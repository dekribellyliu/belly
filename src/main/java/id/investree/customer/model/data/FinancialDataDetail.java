package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class FinancialDataDetail {
	@JsonProperty("statementId")
	private Long statementId;

	@JsonProperty("statementFileType")
	private Integer statementFileType;

	@JsonProperty("statementUrl")
	private String statementUrl;

	@JsonFormat(pattern="yyyy-MM-dd", timezone = "Asia/Jakarta")

	@JsonProperty("statementFileAt")
	private Date statementFileAt;

	@JsonProperty("isDelete")
	private boolean isDelete;
}
