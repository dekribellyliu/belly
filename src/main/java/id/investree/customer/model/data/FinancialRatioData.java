package id.investree.customer.model.data;

import lombok.Data;

@Data
public class FinancialRatioData {
	private Long financialRatioId;
	private Long yearTo;
	private Double gpm;
	private Double npm;
	private Double ardoh;
	private Double invdoh;
	private Double apdoh;
	private Double cashCycle;
	private Double cashRatio;
	private Double ebitda;
	private Double leverage;
	private Double wiNeeds;
	private Double tie;
	private Double dscr;
}
