package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class InvestmentProfileData {
	@JsonProperty("investmentObjective")
	private Long investmentObjective;
	@JsonProperty("riskProfile")
	private Long riskProfile;
}
