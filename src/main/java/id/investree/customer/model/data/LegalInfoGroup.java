package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.utils.Utilities;
import id.investree.customer.utils.Constants;
import lombok.Data;

import java.util.List;

@Data
public class LegalInfoGroup {
	@JsonProperty("npwp")
	private LegalInformationData npwp;

	@JsonProperty("siup")
	private LegalInformationData siup;

	@JsonProperty("aktaPendirian")
	private LegalInformationData aktaPendirian;

	@JsonProperty("aktaTerbaru")
	private LegalInformationData aktaTerbaru;

	@JsonProperty("skMenkumham")
	private LegalInformationData skMenkumham;

	@JsonProperty("tdp")
	private LegalInformationData tdp;

	@JsonProperty("skdu")
	private LegalInformationData skdu;

	@JsonProperty("dgt")
	private LegalInformationData dgt;

	@JsonProperty("articleOfAssociation")
	private LegalInformationData articleOfAssociation;

	@JsonProperty("certificateOfIncumbency")
	private LegalInformationData certificateOfIncumbency;

	public LegalInfoGroup(List<LegalInformationData> legalInformationDataList) {
		legalInformationDataList.stream()
			.filter(it -> !Utilities.isNull(it.getDocumentType()))
			.forEach(legalInformationData -> {
				if (Constants.LEGAL_INFO_NPWP.equals(legalInformationData.getDocumentType().getId())) {
					this.npwp = legalInformationData;

				} else if (Constants.LEGAL_INFO_SIUP.equals(legalInformationData.getDocumentType().getId())) {
					this.siup = legalInformationData;

				} else if (Constants.LEGAL_INFO_AKTA_PENDIRIAN.equals(legalInformationData.getDocumentType().getId())) {
					this.aktaPendirian = legalInformationData;

				} else if (Constants.LEGAL_INFO_AKTA_TERBARU.equals(legalInformationData.getDocumentType().getId())) {
					this.aktaTerbaru = legalInformationData;

				} else if (Constants.LEGAL_INFO_SK_MENKUHAM.equals(legalInformationData.getDocumentType().getId())) {
					this.skMenkumham = legalInformationData;

				} else if (Constants.LEGAL_INFO_SK_TDP.equals(legalInformationData.getDocumentType().getId())) {
					this.tdp = legalInformationData;

				} else if (Constants.LEGAL_INFO_SK_SKDU.equals(legalInformationData.getDocumentType().getId())) {
					this.skdu = legalInformationData;

				} else if (Constants.LEGAL_INFO_DGT.equals(legalInformationData.getDocumentType().getId())) {
					this.dgt = legalInformationData;

				} else if (Constants.LEGAL_INFO_ARTICLE_ASSOCIATION.equals(legalInformationData.getDocumentType().getId())) {
					this.articleOfAssociation = legalInformationData;

				} else if (Constants.LEGAL_INFO_CERTIFICATE_INCUMBENCY.equals(legalInformationData.getDocumentType().getId())) {
					this.certificateOfIncumbency = legalInformationData;
				}
			});
	}
}
