package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.DefaultData;
import lombok.Data;

import java.util.Date;

@Data
public class LegalInformationData {
	@JsonProperty("documentType")
	private DefaultData documentType;

	@JsonProperty("documentFile")
	private String documentFile;

	@JsonProperty("documentNumber")
	private String documentNumber;

	@JsonProperty("documentRegistered")
	private Date documentRegistered;

	@JsonProperty("documentExpiredDate")
	private Date documentExpiredDate;
}
