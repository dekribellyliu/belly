package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MailchimpVarsData {

    @JsonProperty("FNAME")
    private String fullname;

    @JsonProperty("FTYPE")
    private String type;
}
