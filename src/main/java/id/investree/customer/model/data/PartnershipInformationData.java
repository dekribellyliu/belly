package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class PartnershipInformationData {
	@JsonProperty("partnershipInfoId")
	private Long partnershipInfoId;

	@JsonProperty("customerId")
	private Long customerId;

	@JsonProperty("type")
	private Long type;

	@JsonProperty("partnerId")
	private Long partnerId;

	@JsonProperty("name")
	private String name;

	@JsonProperty("category")
	private Long category;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	@JsonProperty("startOfRelation")
	private Date startOfRelation;

	@JsonProperty("seller")
	private String seller;

	@JsonProperty("sellerLink")
	private String sellerLink;

	@JsonProperty("sellerLocation")
	private Long sellerLocation;

	@JsonProperty("buildingLocOwnership")
	private Long buildingLocOwnership;

	@JsonProperty("lengthOfOwnership")
	private String lengthOfOwnership;

	@JsonProperty("internalRating")
	private String internalRating;

	@JsonProperty("externalRating")
	private String externalRating;

	@JsonProperty("initialLoanAmount")
	private Double initialLoanAmount;

	@JsonProperty("initialLoanTenor ")
	private Long initialLoanTenor;

	@JsonProperty("maxAmount")
	private Double maxAmount;

	@JsonProperty("maxTenor")
	private Long maxTenor;
}
