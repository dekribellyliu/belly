package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.DefaultData;
import lombok.Data;

@Data
public class PersonalProfileData {
	@JsonProperty("selfiePicture")
	private String selfiePicture;
	@JsonProperty("selfieFileName")
	private String selfieFileName;
	@JsonProperty("idCardPicture")
	private String idCardPicture;
	@JsonProperty("idCardFileName")
	private String idCardFileName;
	@JsonProperty("selfieKtpPicture")
	private String selfieKtpPicture;
	@JsonProperty("signaturePicture")
	private String signaturePicture;
	@JsonProperty("signatureFileName")
	private String signatureFileName;
	@JsonProperty("idCardNumber")
	private String idCardNumber;
	@JsonProperty("idCardExpiredDate")
	private String idCardExpiredDate;
	@JsonProperty("idCardLifetime")
	private boolean isIdCardLifetime;
	@JsonProperty("sameAsDomicileAddress")
	private boolean isSameAsDomicileAddress;
	@JsonProperty("address")
	private String address;
	@JsonProperty("maritalStatus")
	private DefaultData maritalStatus;
	@JsonProperty("province")
	private DefaultData province;
	@JsonProperty("city")
	private DefaultData city;
	@JsonProperty("district")
	private DefaultData district;
	@JsonProperty("subDistrict")
	private DefaultData subDistrict;
	@JsonProperty("postalCode")
	private String postalCode;
	@JsonProperty("placeOfBirth")
	private DefaultData placeOfBirth;
	@JsonProperty("placeOfBirthExternal")
	private String placeOfBirthExternal;
	@JsonProperty("dateOfBirth")
	private String dateOfBirth;
	@JsonProperty("religion")
	private DefaultData religion;
	@JsonProperty("education")
	private DefaultData education;
	@JsonProperty("occupation")
	private DefaultData occupation;
	@JsonProperty("domicileAddress")
	private String domicileAddress;
	@JsonProperty("domicileProvince")
	private DefaultData domicileProvince;
	@JsonProperty("domicileCity")
	private DefaultData domicileCity;
	@JsonProperty("domicileDistrict")
	private DefaultData domicileDistrict;
	@JsonProperty("domicileSubDistrict")
	private DefaultData domicileSubDistrict;
	@JsonProperty("domicilePostalCode")
	private String domicilePostalCode;
	@JsonProperty("poaFile")
	private String poaFile;
	@JsonProperty("motherMaidenName")
	private String motherMaidenName;
	@JsonProperty("fieldOfWork")
	private Long fieldOfWork;
	@JsonProperty("npwpFile")
	private String npwpFile;
	@JsonProperty("npwpFileName")
	private String npwpFileName;

	@JsonProperty("occupationAddress")
	private String occupationAddress;
	@JsonProperty("occupationCity")
	private DefaultData occupationCity;
	@JsonProperty("occupationPostalCode")
	private String occupationPostalCode;
	@JsonProperty("occupationMobilePrefix")
	private DefaultData occupationMobilePrefix;
	@JsonProperty("occupationPhoneNumber")
	private String occupationPhoneNumber;
	@JsonProperty("mailingAddressStatus")
	private Integer mailingAddressStatus;

	@JsonProperty("annualIncome")
	private DefaultData annualIncome;
	@JsonProperty("sourceOfFund")
	private DefaultData sourceOfFund;
}
