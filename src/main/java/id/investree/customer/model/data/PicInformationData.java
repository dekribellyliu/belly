package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.DefaultData;
import lombok.Data;

import java.util.List;

@Data
public class PicInformationData {
	@JsonProperty("loginId")
	private Long loginId;

	@JsonProperty("saluation")
	private String saluation;

	@JsonProperty("fullname")
	private String fullname;

	@JsonProperty("username")
	private String username;

	@JsonProperty("emailAddress")
	private String emailAddress;

	@JsonProperty("emailStatus")
	private Boolean emailStatus;

	@JsonProperty("mobilePrefix")
	private DefaultData mobilePrefix;

	@JsonProperty("mobileNumber")
	private String mobileNumber;

	@JsonProperty("positionWithInstitution")
	private DefaultData positionWithInstitution;

	@JsonProperty("idCardNumber")
	private String idCardNumber;

	@JsonProperty("idCardFile")
	private String idCardFile;

	@JsonProperty("ktpAddress")
	private String ktpAddress;

	@JsonProperty("active")
	private Boolean active;

	@JsonProperty("picFrom")
	private Long picFrom;

	@JsonProperty("picRoles")
	private List<DefaultData> picRoles;
}
