package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ProductPreferenceData {
	@JsonProperty("productPreferenceId")
	private Integer productPreferenceId;
}
