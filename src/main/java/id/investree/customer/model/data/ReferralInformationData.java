package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.DefaultData;
import lombok.Data;

import java.util.Date;

@Data
public class ReferralInformationData {
	@JsonProperty("referralId")
	private Long referralId;

	@JsonProperty("referrerCode")
	private String referrerCode;

	@JsonProperty("referrerName")
	private String referrerName;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	@JsonProperty("expiredDate")
	private Date expiredDate;

	@JsonProperty("knowInvestreeFrom")
	private DefaultData knowInvestreeFrom;
}
