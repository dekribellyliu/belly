package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.DefaultData;
import lombok.Data;

import java.util.Date;

@Data
public class ShareholdersInformationData {
	@JsonProperty("id")
	private Long id;

	@JsonProperty("position")
	private DefaultData position;

	@JsonProperty("fullName")
	private String fullName;

	@JsonProperty("mobilePrefix")
	private DefaultData mobilePrefix;

	@JsonProperty("mobileNumber")
	private String mobileNumber;

	@JsonProperty("emailAddress")
	private String emailAddress;

	@JsonProperty("stockOwnership")
	private Float stockOwnership;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")

	@JsonProperty("dob")
	private Date dob;

	@JsonProperty("identificationCardUrl")
	private String identificationCardUrl;

	@JsonProperty("identificationCardNumber")
	private String identificationCardNumber;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")

	@JsonProperty("identificationCardExpiryDate")
	private Date identificationCardExpiryDate;

	@JsonProperty("idCardExpiryLifetime")
	private Boolean idCardExpiryLifetime;

	@JsonProperty("selfieUrl")
	private String selfieUrl;

	@JsonProperty("taxCardUrl")
	private String taxCardUrl;

	@JsonProperty("taxCardNumber")
	private String taxCardNumber;
	@JsonProperty("isLss")
	private Boolean isLss;

	@JsonProperty("isPgs")
	private Boolean isPgs;

	@JsonProperty("isTss")
	private Boolean isTss;

	@JsonProperty("apuPptCheck")
	private ApuPptCheckData apuPptCheck;

	@JsonProperty("pgNumber")
	private String pgNumber;

	@JsonProperty("pgAmount")
	private Long pgAmount;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")

	@JsonProperty("pgSignedDate")
	private Date pgSignedDate;

	@JsonProperty("pgType")
	private String pgType;

	@JsonProperty("pgFile")
	private String pgFile;
}
