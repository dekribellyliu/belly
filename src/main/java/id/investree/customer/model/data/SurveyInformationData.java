package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.DefaultData;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class SurveyInformationData {
	@JsonProperty("surveyInformationId")
	private Long surveyInformationId;

	@JsonProperty("surveyBy")
	private Long surveyBy;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")

	@JsonProperty("surveyAt")
	private Date surveyAt;

	@JsonProperty("borrowerPosition")
	private DefaultData borrowerPosition;

	@JsonProperty("borrowerName")
	private String borrowerName;

	@JsonProperty("numberOfEmployee")
	private Long numberOfEmployee;

	@JsonProperty("officeStatus")
	private DefaultData officeStatus;

	@JsonProperty("lengthOfStay")
	private String lengthOfStay;

	@JsonProperty("filename")
	private List<String> filename;

	@JsonProperty("resultDescription")
	private String resultDescription;

	@JsonProperty("address")
	private String address;
}
