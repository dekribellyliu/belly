package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class TransactionHistoryData {

	@JsonProperty("partnerName")
	private String partnerName;

	@JsonProperty("transactionHistoryId")
	private Long id;

	@JsonProperty("partnershipId")
	private Long partnershipId;

	@JsonProperty("documentNumber")
	private String documentNumber;

	@JsonProperty("documentDate")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	private Date documentDate;

	@JsonProperty("amount")
	private Double amount;

	@JsonProperty("status")
	private String status;

	@JsonProperty("buyerName")
	private String buyerName;

	@JsonProperty("termOfPayment")
	private Integer termOfPayment;
}
