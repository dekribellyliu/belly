package id.investree.customer.model.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.customer.entity.CustomerInformation;
import lombok.Data;

@Data
public class VirtualAccountData {

	@JsonProperty("accessToken")
	private String accessToken;

	@JsonProperty("customerInformation")
	private CustomerInformation customerInformation;;

	@JsonProperty("userType")
	private Integer userType;

	@JsonProperty("customerRoleId")
	private Long customerRoleId;
}
