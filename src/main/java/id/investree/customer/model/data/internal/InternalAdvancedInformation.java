package id.investree.customer.model.data.internal;

import id.investree.customer.model.data.BusinessInfoData;
import id.investree.customer.model.data.PersonalProfileData;
import id.investree.customer.model.data.ProductPreferenceData;

public class InternalAdvancedInformation {

	private BusinessInfoData businessProfile;
	private ProductPreferenceData productPreference;
	private PersonalProfileData personalProfile;

	public BusinessInfoData getBusinessProfile() {
		return businessProfile;
	}

	public void setBusinessProfile(BusinessInfoData businessProfile) {
		this.businessProfile = businessProfile;
	}

	public ProductPreferenceData getProductPreference() {
		return productPreference;
	}

	public void setProductPreference(ProductPreferenceData productPreference) {
		this.productPreference = productPreference;
	}

	public PersonalProfileData getPersonalProfile() {
		return personalProfile;
	}

	public void setPersonalProfile(PersonalProfileData personalProfile) {
		this.personalProfile = personalProfile;
	}
}
