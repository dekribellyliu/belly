package id.investree.customer.model.data.internal;

import id.investree.core.model.DefaultData;

public class InternalBorrowerData {

	private String fullName;
	private String emailAddress;
	private String address;
	private String gender;
	private DefaultData province;
	private DefaultData city;
	private DefaultData district;
	private DefaultData village;
	private String postalCode;
	private DefaultData mobilePrefix;
	private String mobileNumber;
	private DefaultData placeOfBirth;
	private String dateOfBirth;
	private String placeOfBirthExternal;
	private String selfiePicture;
	private String selfieFileName;
	private String signaturePicture;
	private String signatureFileName;
	private String documentNumber;
	private String npwpFile;
	private String npwpFileName;
	private String idCardNumber;
	private String idCardPicture;
	private String idCardFileName;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public DefaultData getProvince() {
		return province;
	}

	public void setProvince(DefaultData province) {
		this.province = province;
	}

	public DefaultData getCity() {
		return city;
	}

	public void setCity(DefaultData city) {
		this.city = city;
	}

	public DefaultData getDistrict() {
		return district;
	}

	public void setDistrict(DefaultData district) {
		this.district = district;
	}

	public DefaultData getVillage() {
		return village;
	}

	public void setVillage(DefaultData village) {
		this.village = village;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public DefaultData getMobilePrefix() {
		return mobilePrefix;
	}

	public void setMobilePrefix(DefaultData mobilePrefix) {
		this.mobilePrefix = mobilePrefix;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public DefaultData getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(DefaultData placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getPlaceOfBirthExternal() {
		return placeOfBirthExternal;
	}

	public void setPlaceOfBirthExternal(String placeOfBirthExternal) {
		this.placeOfBirthExternal = placeOfBirthExternal;
	}

	public String getSelfiePicture() {
		return selfiePicture;
	}

	public void setSelfiePicture(String selfiePicture) {
		this.selfiePicture = selfiePicture;
	}

	public String getSelfieFileName() {
		return selfieFileName;
	}

	public void setSelfieFileName(String selfieFileName) {
		this.selfieFileName = selfieFileName;
	}

	public String getSignaturePicture() {
		return signaturePicture;
	}

	public void setSignaturePicture(String signaturePicture) {
		this.signaturePicture = signaturePicture;
	}

	public String getSignatureFileName() {
		return signatureFileName;
	}

	public void setSignatureFileName(String signatureFileName) {
		this.signatureFileName = signatureFileName;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getNpwpFile() {
		return npwpFile;
	}

	public void setNpwpFile(String npwpFile) {
		this.npwpFile = npwpFile;
	}

	public String getNpwpFileName() {
		return npwpFileName;
	}

	public void setNpwpFileName(String npwpFileName) {
		this.npwpFileName = npwpFileName;
	}

	public String getIdCardNumber() {
		return idCardNumber;
	}

	public void setIdCardNumber(String idCardNumber) {
		this.idCardNumber = idCardNumber;
	}

	public String getIdCardPicture() {
		return idCardPicture;
	}

	public void setIdCardPicture(String idCardPicture) {
		this.idCardPicture = idCardPicture;
	}

	public String getIdCardFileName() {
		return idCardFileName;
	}

	public void setIdCardFileName(String idCardFileName) {
		this.idCardFileName = idCardFileName;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
}
