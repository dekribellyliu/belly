package id.investree.customer.model.enums;

public enum PartnershipTypeEnum {
	ECOMMERCE_PARTNERSHIP, BUSINESS_PARTNERSHIP
}
