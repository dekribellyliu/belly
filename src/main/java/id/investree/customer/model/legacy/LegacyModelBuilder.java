package id.investree.customer.model.legacy;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.model.DefaultData;
import id.investree.core.model.legacy.LegacyBalanceSheetModel;
import id.investree.core.model.legacy.LegacyBankInformationModel;
import id.investree.core.model.legacy.LegacyFinancialRatioModel;
import id.investree.core.model.legacy.LegacyFinancialStatementModel;
import id.investree.core.model.legacy.LegacyFinancialTrendModel;
import id.investree.core.model.legacy.LegacyLegalInfoModel;
import id.investree.core.model.legacy.LegacyModel;
import id.investree.core.model.legacy.LegacySalesTransaction;
import id.investree.core.model.legacy.LegacyShareholderInfoModel;
import id.investree.customer.entity.BalanceSheet;
import id.investree.customer.entity.BankInformation;
import id.investree.customer.entity.BusinessProfile;
import id.investree.customer.entity.CifList;
import id.investree.customer.entity.CustomerInformation;
import id.investree.customer.entity.CustomerRole;
import id.investree.customer.entity.FinancialRatio;
import id.investree.customer.entity.FinancialStatement;
import id.investree.customer.entity.FinancialTrend;
import id.investree.customer.entity.LegalInformation;
import id.investree.customer.entity.PartnershipInformation;
import id.investree.customer.entity.ReferralData;
import id.investree.customer.entity.SalesTransaction;
import id.investree.customer.entity.ShareholdersInformation;
import id.investree.customer.entity.SurveyInformation;
import id.investree.customer.entity.UserData;
import id.investree.customer.utils.StoreDataGlobalParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LegacyModelBuilder {

	private String email;
	private Long productSelection;
	private CustomerInformation customerInformation;
	private CustomerRole customerRole;
	private UserData userData;
	private BusinessProfile businessProfile;
	private String financialStatementUrl;
	private String financialEStatementUrl;
	private SurveyInformation surveyInformation;
	private String officeStatusName;
	private ReferralData referralData;
	private List<BalanceSheet> balanceSheets;
	private List<BankInformation> bankInformations;
	private List<FinancialRatio> financialRatios;
	private List<FinancialStatement> financialStatements;
	private List<FinancialTrend> financialTrends;
	private List<LegalInformation> legalInformations;
	private List<ShareholdersInformation> shareholdersInformations;
	private PartnershipInformation partnershipInformation;
	private List<SalesTransaction> salesTransactions;
	private StoreDataGlobalParam storeDataGlobalParam;
	private CifList cifList;

	public LegacyModelBuilder setEmail(String email) {
		this.email = email;
		return this;
	}

	public LegacyModelBuilder setOfficeStatusName(String officeStatusName) {
		this.officeStatusName = officeStatusName;
		return this;
	}

	public LegacyModelBuilder setProductSelection(Long productSelection) {
		this.productSelection = productSelection;
		return this;
	}

	public LegacyModelBuilder setCustomerInformation(CustomerInformation customerInformation) {
		this.customerInformation = customerInformation;
		return this;
	}

	public LegacyModelBuilder setCustomerRole(CustomerRole customerRole) {
		this.customerRole = customerRole;
		return this;
	}

	public LegacyModelBuilder setUserData(UserData userData) {
		this.userData = userData;
		return this;
	}

	public LegacyModelBuilder setBusinessProfile(BusinessProfile businessProfile) {
		this.businessProfile = businessProfile;
		return this;
	}

	public LegacyModelBuilder setFinancialStatementUrl(String financialStatementUrl) {
		this.financialStatementUrl = financialStatementUrl;
		return this;
	}

	public LegacyModelBuilder setFinancialEStatementUrl(String financialEStatementUrl) {
		this.financialEStatementUrl = financialEStatementUrl;
		return this;
	}

	public LegacyModelBuilder setReferralData(ReferralData referralData) {
		this.referralData = referralData;
		return this;
	}

	public LegacyModelBuilder setGlobalParam(StoreDataGlobalParam storeDataGlobalParam) {
		this.storeDataGlobalParam = storeDataGlobalParam;
		return this;
	}

	public LegacyModelBuilder setBalanceSheets(List<BalanceSheet> balanceSheets) {
		this.balanceSheets = balanceSheets;
		return this;
	}

	public LegacyModelBuilder setBankInformations(List<BankInformation> bankInformations) {
		this.bankInformations = bankInformations;
		return this;
	}

	public LegacyModelBuilder setFinancialRatios(List<FinancialRatio> financialRatios) {
		this.financialRatios = financialRatios;
		return this;
	}

	public LegacyModelBuilder setFinancialStatements(List<FinancialStatement> financialStatements) {
		this.financialStatements = financialStatements;
		return this;
	}

	public LegacyModelBuilder setFinancialTrends(List<FinancialTrend> financialTrends) {
		this.financialTrends = financialTrends;
		return this;
	}

	public LegacyModelBuilder setLegalInformations(List<LegalInformation> legalInformations) {
		this.legalInformations = legalInformations;
		return this;
	}

	public LegacyModelBuilder setShareholdersInformations(List<ShareholdersInformation> shareholdersInformations) {
		this.shareholdersInformations = shareholdersInformations;
		return this;
	}

	public LegacyModelBuilder setSurveyInformation(SurveyInformation surveyInformation, StoreDataGlobalParam storeDataGlobalParam) {
		this.surveyInformation = surveyInformation;
		this.storeDataGlobalParam = storeDataGlobalParam;
		return this;
	}

	public LegacyModelBuilder setPartnershipInformation(PartnershipInformation partnershipInformation) {
		this.partnershipInformation = partnershipInformation;
		return this;
	}

	public LegacyModelBuilder setSalesTransaction(List<SalesTransaction> salesTransactions) {
		this.salesTransactions = salesTransactions;
		return this;
	}

	public LegacyModelBuilder setCifList(CifList cifList) {
		this.cifList = cifList;
		return this;
	}

	public LegacyModel build(Long customerId) {
		LegacyModel legacyModel = new LegacyModel();

		legacyModel.setEmailValidation(email);
		legacyModel.setCustomerInfo_Id(customerId);

		Optional.ofNullable(productSelection).ifPresent(legacyModel::setCustomerProductList_type);
		Optional.ofNullable(customerInformation).ifPresent(v -> {
			Optional.ofNullable(v.getUserCategory()).ifPresent(legacyModel::setCustomerInfo_UserCategory);
			Optional.ofNullable(v.getProductPreference()).ifPresent(legacyModel::setCustomerInfo_ProductPreference);
			Optional.ofNullable(v.getLegalEntity()).ifPresent(legacyModel::setCustomerInfo_LegalEntity);
			Optional.ofNullable(v.getCustomerNumber()).ifPresent(legacyModel::setCustomerInfo_Number);
			Optional.ofNullable(v.getInitial()).ifPresent(legacyModel::setCustomerInfo_Initial);
			Optional.ofNullable(v.getIndustry()).ifPresent(legacyModel::setCustomerInfo_KbliCode);
			Optional.ofNullable(v.getName()).ifPresent(legacyModel::setCustomerInfo_Name);
		});

		Optional.ofNullable(customerRole).ifPresent(v -> {
			Optional.ofNullable(v.getCustomerInformationId()).ifPresent(legacyModel::setCustomerRole_customerInformationId);
			Optional.ofNullable(v.getType()).ifPresent(legacyModel::setCustomerRole_type);
			Optional.ofNullable(v.getCreatedBy()).ifPresent(legacyModel::setCustomerRole_createdBy);
			Optional.ofNullable(v.getCreatedAt()).ifPresent(legacyModel::setCustomerRole_createdAt);
			Optional.ofNullable(v.getUpdatedBy()).ifPresent(legacyModel::setCustomerRole_updatedBy);
			Optional.ofNullable(v.getUpdatedAt()).ifPresent(legacyModel::setCustomerRole_updatedAt);
			Optional.ofNullable(v.getStatus()).ifPresent(legacyModel::setCustomerRole_status);
			Optional.ofNullable(v.getRegisteredAt()).ifPresent(legacyModel::setCustomerRole_registeredAt);
			Optional.ofNullable(v.getPoaFile()).ifPresent(legacyModel::setCustomerRole_poaFile);
		});

		Optional.ofNullable(userData).ifPresent(v -> {
			Optional.ofNullable(v.getId()).ifPresent(legacyModel::setUserData_id);
			Optional.ofNullable(v.getLoginDataId()).ifPresent(legacyModel::setUserData_loginDataId);
			Optional.ofNullable(v.getCustomerId()).ifPresent(legacyModel::setUserData_customerId);
			Optional.ofNullable(v.getCompanyDivision()).ifPresent(legacyModel::setUserData_companyDivision);
			Optional.ofNullable(v.getCompanyPosition()).ifPresent(legacyModel::setUserData_companyPosition);
			Optional.ofNullable(v.getSelfieFile()).ifPresent(legacyModel::setUserData_selfieFile);
			Optional.ofNullable(v.getIdCardFile()).ifPresent(legacyModel::setUserData_idCardFile);
			Optional.ofNullable(v.getIdCardNumber()).ifPresent(legacyModel::setUserData_idCardNumber);
			Optional.ofNullable(v.getIdCardExpired()).ifPresent(legacyModel::setUserData_idCardExpired);
			Optional.ofNullable(v.getNationality()).ifPresent(legacyModel::setUserData_nationality);
			Optional.ofNullable(v.getSameAddress()).ifPresent(legacyModel::setUserData_sameAddress);
			Optional.ofNullable(v.getKtpAddress()).ifPresent(legacyModel::setUserData_ktpAddress);
			Optional.ofNullable(v.getKtpProvince()).ifPresent(legacyModel::setUserData_ktpProvince);
			Optional.ofNullable(v.getKtpCity()).ifPresent(legacyModel::setUserData_ktpCity);
			Optional.ofNullable(v.getKtpDistrict()).ifPresent(legacyModel::setUserData_ktpDistrict);
			Optional.ofNullable(v.getKtpVillage()).ifPresent(legacyModel::setUserData_ktpVillage);
			Optional.ofNullable(v.getKtpPostalCode()).ifPresent(legacyModel::setUserData_ktpPostalCode);
			Optional.ofNullable(v.getPlaceOfBirth()).ifPresent(legacyModel::setUserData_placeOfBirth);
			Optional.ofNullable(v.getPlaceOfBirthExternal()).ifPresent(legacyModel::setUserData_placeOfBirthExternal);
			Optional.ofNullable(v.getDateOfBirth()).ifPresent(legacyModel::setUserData_dateOfBirth);
			Optional.ofNullable(v.getReligion()).ifPresent(legacyModel::setUserData_religion);
			Optional.ofNullable(v.getEducation()).ifPresent(legacyModel::setUserData_education);
			Optional.ofNullable(v.getOccupation()).ifPresent(legacyModel::setUserData_occupation);
			Optional.ofNullable(v.getDomicileAddress()).ifPresent(legacyModel::setUserData_domicileAddress);
			Optional.ofNullable(v.getDomicileProvince()).ifPresent(legacyModel::setUserData_domicileProvince);
			Optional.ofNullable(v.getDomicileCity()).ifPresent(legacyModel::setUserData_domicileCity);
			Optional.ofNullable(v.getDomicileDistrict()).ifPresent(legacyModel::setUserData_domicileDistrict);
			Optional.ofNullable(v.getDomicileVillage()).ifPresent(legacyModel::setUserData_domicileVillage);
			Optional.ofNullable(v.getDomicilePostalCode()).ifPresent(legacyModel::setUserData_domicilePostalCode);
			Optional.ofNullable(v.getCreatedBy()).ifPresent(legacyModel::setUserData_createdBy);
			Optional.ofNullable(v.getCreatedAt()).ifPresent(legacyModel::setUserData_createdAt);
			Optional.ofNullable(v.getUpdatedBy()).ifPresent(legacyModel::setUserData_updatedBy);
			Optional.ofNullable(v.getUpdatedAt()).ifPresent(legacyModel::setUserData_updatedAt);
			Optional.ofNullable(v.getMaritalStatus()).ifPresent(legacyModel::setUserData_maritalStatus);
		});

		Optional.ofNullable(businessProfile).ifPresent(v -> {
			Optional.ofNullable(v.getCustomerId()).ifPresent(legacyModel::setBusinessData_ciId);
			Optional.ofNullable(v.getDateOfEstablishment()).ifPresent(legacyModel::setBusinessData_doe);
			Optional.ofNullable(v.getNumberOfEmployee()).ifPresent(val -> {
				legacyModel.setBusinessData_noe(Long.valueOf(val));
			});
			Optional.ofNullable(v.getCompanyDescription()).ifPresent(legacyModel::setBusinessData_description);
			Optional.ofNullable(v.getCompanyAddress()).ifPresent(legacyModel::setBusinessData_address);
			Optional.ofNullable(v.getProvince()).ifPresent(legacyModel::setBusinessData_province);
			Optional.ofNullable(v.getCity()).ifPresent(legacyModel::setBusinessData_city);
			Optional.ofNullable(v.getDistrict()).ifPresent(legacyModel::setBusinessData_district);
			Optional.ofNullable(v.getVillage()).ifPresent(legacyModel::setBusinessData_village);
			Optional.ofNullable(v.getPostalCode()).ifPresent(legacyModel::setBusinessData_postalCode);
			Optional.ofNullable(v.getLandLineNumber()).ifPresent(legacyModel::setBusinessData_landLine);
			Optional.ofNullable(v.getCreatedAt()).ifPresent(legacyModel::setBusinessData_createdAt);
			Optional.ofNullable(v.getCreatedBy()).ifPresent(legacyModel::setBusinessData_createdBy);
			Optional.ofNullable(v.getUpdatedAt()).ifPresent(legacyModel::setBusinessData_updateAt);
			Optional.ofNullable(v.getUpdatedBy()).ifPresent(legacyModel::setBusinessData_updateBy);
			Optional.ofNullable(v.getFillFinishAt()).ifPresent(legacyModel::setBusinessData_fillFinishAt);
			Optional.ofNullable(v.getGroupCompany()).ifPresent(legacyModel::setBusinessData_groupCompany);
			Optional.ofNullable(v.getGroupDescription()).ifPresent(legacyModel::setBusinessData_groupDescription);
			Optional.ofNullable(v.getListOfPayor()).ifPresent(legacyModel::setBusinessData_lop);
			Optional.ofNullable(v.getRelationshipWithBank()).ifPresent(legacyModel::setBusinessData_lrwb);
			Optional.ofNullable(v.getAverageMonthlySales()).ifPresent(legacyModel::setBusinessData_ams);
			Optional.ofNullable(v.getProfitMargin()).ifPresent(legacyModel::setBusinessData_profitMargin);
		});

		Optional.ofNullable(financialStatementUrl).ifPresent(legacyModel::setFinancialInformation_financialStatementUrl);
		Optional.ofNullable(financialEStatementUrl).ifPresent(legacyModel::setFinancialInformation_financialEStatementUrl);

		Optional.ofNullable(balanceSheets).ifPresent(balanceSheet -> {
			List<LegacyBalanceSheetModel> balanceSheetModelList = new ArrayList<>();
			balanceSheet.forEach(v -> {
				LegacyBalanceSheetModel balanceSheetModel = new LegacyBalanceSheetModel();
				Optional.ofNullable(v.getId()).ifPresent(balanceSheetModel::setBalanceSheet_id);
				Optional.ofNullable(v.getCustomerId()).ifPresent(balanceSheetModel::setBalanceSheet_customerId);
				Optional.ofNullable(v.getYearTo()).ifPresent(balanceSheetModel::setBalanceSheet_yearTo);
				Optional.ofNullable(v.getAccReceive()).ifPresent(balanceSheetModel::setBalanceSheet_accReceive);
				Optional.ofNullable(v.getInvestory()).ifPresent(balanceSheetModel::setBalanceSheet_investory);
				Optional.ofNullable(v.getAccPayable()).ifPresent(balanceSheetModel::setBalanceSheet_accPayable);
				Optional.ofNullable(v.getBankDebt()).ifPresent(balanceSheetModel::setBalanceSheet_bankDebt);
				Optional.ofNullable(v.getCurrentAssets()).ifPresent(balanceSheetModel::setBalanceSheet_currentAssets);
				Optional.ofNullable(v.getCurrentLiabilities()).ifPresent(balanceSheetModel::setBalanceSheet_currentLiabilities);
				Optional.ofNullable(v.getTotalLiabilities()).ifPresent(balanceSheetModel::setBalanceSheet_totalLiabilities);
				Optional.ofNullable(v.getEquity()).ifPresent(balanceSheetModel::setBalanceSheet_equity);
				Optional.ofNullable(v.getCreatedBy()).ifPresent(balanceSheetModel::setBalanceSheet_createdBy);
				Optional.ofNullable(v.getCreatedAt()).ifPresent(balanceSheetModel::setBalanceSheet_createdAt);
				Optional.ofNullable(v.getUpdateBy()).ifPresent(balanceSheetModel::setBalanceSheet_updateBy);
				Optional.ofNullable(v.getUpdateAt()).ifPresent(balanceSheetModel::setBalanceSheet_updateAt);

				balanceSheetModelList.add(balanceSheetModel);
			});
			legacyModel.setBalanceSheet(balanceSheetModelList);
		});

		Optional.ofNullable(bankInformations).ifPresent(bankInfo -> {
			List<LegacyBankInformationModel> bankInformationList = new ArrayList<>();
			bankInfo.forEach(v -> {
				LegacyBankInformationModel bankInformation = new LegacyBankInformationModel();
				Optional.ofNullable(v.getId()).ifPresent(bankInformation::setBankInfo_Id);
				Optional.ofNullable(v.getCustomerId()).ifPresent(bankInformation::setBankInfo_CustomerId);
				Optional.ofNullable(v.getBankType()).ifPresent(bankInformation::setBankInfo_type);
				Optional.ofNullable(v.getBankAccountCoverFile()).ifPresent(bankInformation::setBankInfo_accountCoverFile);
				Optional.ofNullable(v.getMasterBankId()).ifPresent(bankInformation::setBankInfo_masterBankId);
				Optional.ofNullable(v.getBankAccountHolderName()).ifPresent(bankInformation::setBankInfo_accountHolderName);
				Optional.ofNullable(v.getBankAccountNumber()).ifPresent(bankInformation::setBankInfo_accountNumber);
				Optional.ofNullable(v.getUseAsDisbursement()).ifPresent(bankInformation::setBankInfo_useAsDisbursement);
				Optional.ofNullable(v.getCreatedBy()).ifPresent(bankInformation::setBankInfo_createdBy);
				Optional.ofNullable(v.getCreatedAt()).ifPresent(bankInformation::setBankInfo_createdAt);
				Optional.ofNullable(v.getUpdateBy()).ifPresent(bankInformation::setBankInfo_updateBy);
				Optional.ofNullable(v.getUpdatedAt()).ifPresent(bankInformation::setBankInfo_updatedAt);
				Optional.ofNullable(v.getFillFinishAt()).ifPresent(bankInformation::setBankInfo_fillFinishAt);
				Optional.ofNullable(v.getDeletedBy()).ifPresent(bankInformation::setBankInfo_deletedBy);
				Optional.ofNullable(v.getDeletedAt()).ifPresent(bankInformation::setBankInfo_deletedAt);
				Optional.ofNullable(v.getVerifiedStatus()).ifPresent(bankInformation::setBankInfo_verifiedStatus);
				Optional.ofNullable(v.getInvarBank()).ifPresent(bankInformation::setBankInfo_mibId);
				Optional.ofNullable(v.getIsAnchorDisbursement()).ifPresent(bankInformation::setBankInfo_isAnchorDisbursement);

				bankInformationList.add(bankInformation);
			});
			legacyModel.setBankInformation(bankInformationList);
		});

		Optional.ofNullable(financialRatios).ifPresent(financialRatio -> {
			List<LegacyFinancialRatioModel> financialRatioList = new ArrayList<>();
			financialRatio.forEach(v -> {
				LegacyFinancialRatioModel financialRatioModel = new LegacyFinancialRatioModel();
				Optional.ofNullable(v.getId()).ifPresent(financialRatioModel::setFinancialRatio_id);
				Optional.ofNullable(v.getCustomerId()).ifPresent(financialRatioModel::setFinancialRatio_customerId);
				Optional.ofNullable(v.getYearTo()).ifPresent(financialRatioModel::setFinancialRatio_yearTo);
				Optional.ofNullable(v.getGpm()).ifPresent(financialRatioModel::setFinancialRatio_gpm);
				Optional.ofNullable(v.getNpm()).ifPresent(financialRatioModel::setFinancialRatio_npm);
				Optional.ofNullable(v.getArdoh()).ifPresent(financialRatioModel::setFinancialRatio_ardoh);
				Optional.ofNullable(v.getInvdoh()).ifPresent(financialRatioModel::setFinancialRatio_invdoh);
				Optional.ofNullable(v.getApdoh()).ifPresent(financialRatioModel::setFinancialRatio_apdoh);
				Optional.ofNullable(v.getCashCycle()).ifPresent(financialRatioModel::setFinancialRatio_cashCycle);
				Optional.ofNullable(v.getCashRatio()).ifPresent(financialRatioModel::setFinancialRatio_cashRatio);
				Optional.ofNullable(v.getEbitda()).ifPresent(financialRatioModel::setFinancialRatio_ebitda);
				Optional.ofNullable(v.getLeverage()).ifPresent(financialRatioModel::setFinancialRatio_leverage);
				Optional.ofNullable(v.getWiNeeds()).ifPresent(financialRatioModel::setFinancialRatio_wiNeeds);
				Optional.ofNullable(v.getTie()).ifPresent(financialRatioModel::setFinancialRatio_tie);
				Optional.ofNullable(v.getCreatedBy()).ifPresent(financialRatioModel::setFinancialRatio_createdBy);
				Optional.ofNullable(v.getCreatedAt()).ifPresent(financialRatioModel::setFinancialRatio_createdAt);
				Optional.ofNullable(v.getUpdateBy()).ifPresent(financialRatioModel::setFinancialRatio_updateBy);
				Optional.ofNullable(v.getUpdateAt()).ifPresent(financialRatioModel::setFinancialRatio_updateAt);

				financialRatioList.add(financialRatioModel);
			});

			legacyModel.setFinancialRatio(financialRatioList);
		});

		Optional.ofNullable(financialStatements).ifPresent(financialStatement -> {
			List<LegacyFinancialStatementModel> financialStatementList = new ArrayList<>();
			financialStatement.forEach(v -> {
				LegacyFinancialStatementModel financialStatementModel = new LegacyFinancialStatementModel();
				Optional.ofNullable(v.getId()).ifPresent(financialStatementModel::setFinancialStatement_id);
				Optional.ofNullable(v.getCustomerId()).ifPresent(financialStatementModel::setFinancialStatement_customerId);
				Optional.ofNullable(v.getYearTo()).ifPresent(financialStatementModel::setFinancialStatement_yearTo);
				Optional.ofNullable(v.getFiscalYear()).ifPresent(financialStatementModel::setFinancialStatement_fiscalYear);
				Optional.ofNullable(v.getSales()).ifPresent(financialStatementModel::setFinancialStatement_sales);
				Optional.ofNullable(v.getCogs()).ifPresent(financialStatementModel::setFinancialStatement_cogs);
				Optional.ofNullable(v.getGrossProfit()).ifPresent(financialStatementModel::setFinancialStatement_grossProfit);
				Optional.ofNullable(v.getSga()).ifPresent(financialStatementModel::setFinancialStatement_sga);
				Optional.ofNullable(v.getDepreciation()).ifPresent(financialStatementModel::setFinancialStatement_depreciation);
				Optional.ofNullable(v.getOperatingProfit()).ifPresent(financialStatementModel::setFinancialStatement_operatingProfit);
				Optional.ofNullable(v.getInterestExpense()).ifPresent(financialStatementModel::setFinancialStatement_interestExpense);
				Optional.ofNullable(v.getOtherIncome()).ifPresent(financialStatementModel::setFinancialStatement_otherIncome);
				Optional.ofNullable(v.getOtherExpense()).ifPresent(financialStatementModel::setFinancialStatement_otherExpense);
				Optional.ofNullable(v.getProfitBeforeTax()).ifPresent(financialStatementModel::setFinancialStatement_profitBeforeTax);
				Optional.ofNullable(v.getTax()).ifPresent(financialStatementModel::setFinancialStatement_tax);
				Optional.ofNullable(v.getProfitAfterTax()).ifPresent(financialStatementModel::setFinancialStatement_profitAfterTax);
				Optional.ofNullable(v.getCreatedAt()).ifPresent(financialStatementModel::setFinancialStatement_createdAt);
				Optional.ofNullable(v.getCreatedBy()).ifPresent(financialStatementModel::setFinancialStatement_createdBy);
				Optional.ofNullable(v.getUpdateAt()).ifPresent(financialStatementModel::setFinancialStatement_updateAt);
				Optional.ofNullable(v.getUpdateBy()).ifPresent(financialStatementModel::setFinancialStatement_updateBy);
				Optional.ofNullable(v.getExistingFacility()).ifPresent(financialStatementModel::setFinancialStatement_existingFacility);

				financialStatementList.add(financialStatementModel);
			});
			legacyModel.setFinancialStatement(financialStatementList);
		});

		Optional.ofNullable(financialTrends).ifPresent(financialTrend -> {
			List<LegacyFinancialTrendModel> financialTrendList = new ArrayList<>();
			financialTrend.forEach(v -> {
				LegacyFinancialTrendModel financialTrendModel = new LegacyFinancialTrendModel();
				Optional.ofNullable(v.getId()).ifPresent(financialTrendModel::setFinancialTrend_id);
				Optional.ofNullable(v.getCustomerId()).ifPresent(financialTrendModel::setFinancialTrend_customerId);
				Optional.ofNullable(v.getTrendPeriod()).ifPresent(financialTrendModel::setFinancialTrend_trendPeriod);
				Optional.ofNullable(v.getSales()).ifPresent(financialTrendModel::setFinancialTrend_sales);
				Optional.ofNullable(v.getCogs()).ifPresent(financialTrendModel::setFinancialTrend_cogs);
				Optional.ofNullable(v.getGrossProfit()).ifPresent(financialTrendModel::setFinancialTrend_grossProfit);
				Optional.ofNullable(v.getSga()).ifPresent(financialTrendModel::setFinancialTrend_sga);
				Optional.ofNullable(v.getOperatingProfit()).ifPresent(financialTrendModel::setFinancialTrend_operatingProfit);
				Optional.ofNullable(v.getCreatedAt()).ifPresent(financialTrendModel::setFinancialTrend_createdAt);
				Optional.ofNullable(v.getCreatedBy()).ifPresent(financialTrendModel::setFinancialTrend_createdBy);
				Optional.ofNullable(v.getUpdateAt()).ifPresent(financialTrendModel::setFinancialTrend_updateAt);
				Optional.ofNullable(v.getUpdateBy()).ifPresent(financialTrendModel::setFinancialTrend_updateBy);
				Optional.ofNullable(v.getExistingFacility()).ifPresent(financialTrendModel::setFinancialTrend_existingFacility);
				Optional.ofNullable(v.getProfitBeforeTax()).ifPresent(financialTrendModel::setFinancialTrend_profitBeforeTax);
				Optional.ofNullable(v.getProfitAfterTax()).ifPresent(financialTrendModel::setFinancialTrend_profitAfterTax);

				financialTrendList.add(financialTrendModel);
			});
			legacyModel.setFinancialTrend(financialTrendList);
		});

		Optional.ofNullable(legalInformations).ifPresent(legalInfo -> {
			List<LegacyLegalInfoModel> legalInformationList = new ArrayList<>();
			legalInfo.forEach(v -> {
				LegacyLegalInfoModel legacyLegalInfoModel = new LegacyLegalInfoModel();
				Optional.ofNullable(v.getId()).ifPresent(legacyLegalInfoModel::setLegalInfo_Id);
				Optional.ofNullable(v.getCustomerId()).ifPresent(legacyLegalInfoModel::setLegalInfo_ciId);
				Optional.ofNullable(v.getDocumentType()).ifPresent(val -> legacyLegalInfoModel.setLegalInfo_docType(Long.valueOf(val)));
				Optional.ofNullable(v.getDocumentFile()).ifPresent(legacyLegalInfoModel::setLegalInfo_docFile);
				Optional.ofNullable(v.getDocumentNumber()).ifPresent(legacyLegalInfoModel::setLegalInfo_docNumber);
				Optional.ofNullable(v.getDocumentExpired()).ifPresent(legacyLegalInfoModel::setLegalInfo_docExpired);
				Optional.ofNullable(v.getCreatedAt()).ifPresent(legacyLegalInfoModel::setLegalInfo_createdAt);
				Optional.ofNullable(v.getCreatedBy()).ifPresent(legacyLegalInfoModel::setLegalInfo_createdBy);
				Optional.ofNullable(v.getUpdatedAt()).ifPresent(legacyLegalInfoModel::setLegalInfo_updateAt);
				Optional.ofNullable(v.getUpdateBy()).ifPresent(legacyLegalInfoModel::setLegalInfo_updateBy);
				Optional.ofNullable(v.getFillFinishAt()).ifPresent(legacyLegalInfoModel::setLegalInfo_fillFinishAt);
				Optional.ofNullable(v.getDeletedAt()).ifPresent(legacyLegalInfoModel::setLegalInfo_deletedAt);
				Optional.ofNullable(v.getDeletedBy()).ifPresent(legacyLegalInfoModel::setLegalInfo_deletedBy);

				legalInformationList.add(legacyLegalInfoModel);
			});
			legacyModel.setLegalInformations(legalInformationList);
		});

		Optional.ofNullable(shareholdersInformations).ifPresent(shareHolder -> {
			List<LegacyShareholderInfoModel> shareholderInfoList = new ArrayList<>();
			shareHolder.forEach(v -> {
				LegacyShareholderInfoModel shareholderInfo = new LegacyShareholderInfoModel();
				Optional.ofNullable(v.getId()).ifPresent(shareholderInfo::setShareholderInfo_id);
				Optional.ofNullable(v.getCustomerId()).ifPresent(shareholderInfo::setShareholderInfo_customerId);
				Optional.ofNullable(v.getPosition()).ifPresent(shareholderInfo::setShareholderInfo_position);
				Optional.ofNullable(v.getFullName()).ifPresent(shareholderInfo::setShareholderInfo_fullName);
				Optional.ofNullable(v.getMobilePrefix()).ifPresent(shareholderInfo::setShareholderInfo_mobilePrefix);
				Optional.ofNullable(v.getMobileNumber()).ifPresent(shareholderInfo::setShareholderInfo_mobileNumber);
				Optional.ofNullable(v.getEmailAddress()).ifPresent(shareholderInfo::setShareholderInfo_emailAddress);
				Optional.ofNullable(v.getStockOwnership()).ifPresent(shareholderInfo::setShareholderInfo_stockOwnership);
				Optional.ofNullable(v.getDob()).ifPresent(shareholderInfo::setShareholderInfo_dob);
				Optional.ofNullable(v.getIdentificationCardUrl()).ifPresent(shareholderInfo::setShareholderInfo_identificationCardUrl);
				Optional.ofNullable(v.getIdentificationCardNumber()).ifPresent(shareholderInfo::setShareholderInfo_identificationCardNumber);
				Optional.ofNullable(v.getIdentificationCardExpiryDate()).ifPresent(shareholderInfo::setShareholderInfo_identificationCardExpiryDate);
				Optional.ofNullable(v.getSelfieUrl()).ifPresent(shareholderInfo::setShareholderInfo_selfieUrl);
				Optional.ofNullable(v.getTaxCardUrl()).ifPresent(shareholderInfo::setShareholderInfo_taxCardUrl);
				Optional.ofNullable(v.getTaxCardNumber()).ifPresent(shareholderInfo::setShareholderInfo_taxCardNumber);
				Optional.ofNullable(v.getCreatedBy()).ifPresent(shareholderInfo::setShareholderInfo_createdBy);
				Optional.ofNullable(v.getCreatedAt()).ifPresent(shareholderInfo::setShareholderInfo_createdAt);
				Optional.ofNullable(v.getUpdatedBy()).ifPresent(shareholderInfo::setShareholderInfo_updatedBy);
				Optional.ofNullable(v.getUpdatedAt()).ifPresent(shareholderInfo::setShareholderInfo_updatedAt);
				Optional.ofNullable(v.getFillFinishAt()).ifPresent(shareholderInfo::setShareholderInfo_fillFinishAt);
				Optional.ofNullable(v.getDeletedBy()).ifPresent(shareholderInfo::setShareholderInfo_deletedBy);
				Optional.ofNullable(v.getDeletedAt()).ifPresent(shareholderInfo::setShareholderInfo_deletedAt);
				Optional.ofNullable(v.getIsLss()).ifPresent(shareholderInfo::setShareholderInfo_isLss);
				Optional.ofNullable(v.getIsPgs()).ifPresent(shareholderInfo::setShareholderInfo_isPgs);
				Optional.ofNullable(v.getIsTss()).ifPresent(shareholderInfo::setShareholderInfo_isTss);
				Optional.ofNullable(v.getApuPptDate()).ifPresent(shareholderInfo::setShareholderInfo_apuPptDate);
				Optional.ofNullable(v.getApuPptFile()).ifPresent(shareholderInfo::setShareholderInfo_apuPptFile);
				Optional.ofNullable(v.getApuPptResult()).ifPresent(shareholderInfo::setShareholderInfo_apuPptResult);
				Optional.ofNullable(v.getPgNumber()).ifPresent(shareholderInfo::setShareholderInfo_pgNumber);
				Optional.ofNullable(v.getPgAmount()).ifPresent(shareholderInfo::setShareholderInfo_pgAmount);
				Optional.ofNullable(v.getPgSignedDate()).ifPresent(shareholderInfo::setShareholderInfo_pgSignedDate);
				Optional.ofNullable(v.getPgFile()).ifPresent(shareholderInfo::setShareholderInfo_pgFile);
				Optional.ofNullable(v.getPgType()).ifPresent(shareholderInfo::setShareholderInfo_pgType);

				shareholderInfoList.add(shareholderInfo);
			});
			legacyModel.setShareholderInformation(shareholderInfoList);
		});

		storeDataGlobalParam.getGlobalParam(GlobalConstants.GLOBAL_PARAM_POSITION);

		Optional.ofNullable(surveyInformation).ifPresent(surveyInformation -> {
			Optional.ofNullable(surveyInformation.getId()).ifPresent(legacyModel::setSurveyInfo_id);
			Optional.ofNullable(surveyInformation.getCustomerId()).ifPresent(legacyModel::setSurveyInfo_customerId);
			Optional.ofNullable(surveyInformation.getSurveyDate()).ifPresent(legacyModel::setSurveyInfo_surveyDate);
			Optional.ofNullable(surveyInformation.getBorrowerPosition()).ifPresent(v -> {
				String position = storeDataGlobalParam.getPositionList().stream()
					.filter(val -> val.getId().equals(Math.toIntExact(v)))
					.findFirst().map(DefaultData::getName).orElse(null);
				legacyModel.setSurveyInfo_borrowerPosition(position);
			});
			Optional.ofNullable(surveyInformation.getBorrowerName()).ifPresent(legacyModel::setSurveyInfo_borrowerName);
			Optional.ofNullable(surveyInformation.getNumberOfEmployees()).ifPresent(legacyModel::setSurveyInfo_numberOfEmployees);
			Optional.ofNullable(surveyInformation.getOfficeStatus()).ifPresent(legacyModel::setSurveyInfo_officeStatus);
			Optional.ofNullable(officeStatusName).ifPresent(legacyModel::setSurveyInfo_officeStatusName);
			Optional.ofNullable(surveyInformation.getLengthOfStay()).ifPresent(legacyModel::setSurveyInfo_lengthOfStay);
			Optional.ofNullable(surveyInformation.getFilename()).ifPresent(legacyModel::setSurveyInfo_filename);
			Optional.ofNullable(surveyInformation.getResultDescription()).ifPresent(legacyModel::setSurveyInfo_resultDescription);
			Optional.ofNullable(surveyInformation.getAddress()).ifPresent(legacyModel::setSurveyInfo_address);
			Optional.ofNullable(surveyInformation.getCreatedBy()).ifPresent(legacyModel::setSurveyInfo_createdBy);
			Optional.ofNullable(surveyInformation.getCreatedAt()).ifPresent(legacyModel::setSurveyInfo_createdAt);
			Optional.ofNullable(surveyInformation.getUpdateBy()).ifPresent(legacyModel::setSurveyInfo_updateBy);
			Optional.ofNullable(surveyInformation.getUpdateAt()).ifPresent(legacyModel::setSurveyInfo_updateAt);
			Optional.ofNullable(surveyInformation.getDeletedBy()).ifPresent(legacyModel::setSurveyInfo_deletedBy);
			Optional.ofNullable(surveyInformation.getDeletedAt()).ifPresent(legacyModel::setSurveyInfo_deletedAt);
		});

		Optional.ofNullable(partnershipInformation).ifPresent(partnershipInformation -> {
			Optional.ofNullable(partnershipInformation.getId()).ifPresent(legacyModel::setPartnershipInfo_Id);
			Optional.ofNullable(partnershipInformation.getCustomerId()).ifPresent(legacyModel::setPartnershipInfo_customerId);
			Optional.ofNullable(partnershipInformation.getType()).ifPresent(legacyModel::setPartnershipInfo_type);
			Optional.ofNullable(partnershipInformation.getPartnerId()).ifPresent(legacyModel::setPartnershipInfo_partnerId);
			Optional.ofNullable(partnershipInformation.getName()).ifPresent(legacyModel::setPartnershipInfo_name);
			Optional.ofNullable(partnershipInformation.getCategory()).ifPresent(legacyModel::setPartnershipInfo_category);
			Optional.ofNullable(partnershipInformation.getStartOfRelation()).ifPresent(legacyModel::setPartnershipInfo_startOfRelation);
			Optional.ofNullable(partnershipInformation.getSellerId()).ifPresent(legacyModel::setSellerId);
			Optional.ofNullable(partnershipInformation.getSellerLink()).ifPresent(legacyModel::setPartnershipInfo_sellerLink);
			Optional.ofNullable(partnershipInformation.getSellerLocation()).ifPresent(legacyModel::setPartnershipInfo_sellerLocation);
			Optional.ofNullable(partnershipInformation.getBuildingLocOwnership()).ifPresent(legacyModel::setPartnershipInfo_buildingLocOwnership);
			Optional.ofNullable(partnershipInformation.getLengthOfOwnership()).ifPresent(legacyModel::setPartnershipInfo_lengthOfOwnership);
			Optional.ofNullable(partnershipInformation.getInternalRating()).ifPresent(legacyModel::setPartnershipInfo_internalRating);
			Optional.ofNullable(partnershipInformation.getExternalRating()).ifPresent(legacyModel::setPartnershipInfo_externalRating);
			Optional.ofNullable(partnershipInformation.getCreatedBy()).ifPresent(legacyModel::setPartnershipInfo_createdBy);
			Optional.ofNullable(partnershipInformation.getCreatedAt()).ifPresent(legacyModel::setPartnershipInfo_createdAt);
			Optional.ofNullable(partnershipInformation.getUpdateBy()).ifPresent(legacyModel::setPartnershipInfo_updateBy);
			Optional.ofNullable(partnershipInformation.getUpdateAt()).ifPresent(legacyModel::setPartnershipInfo_updateAt);
			Optional.ofNullable(partnershipInformation.getInitialLoanAmount()).ifPresent(legacyModel::setPartnershipInfo_initialLoanAmount);
			Optional.ofNullable(partnershipInformation.getInitialLoanTenor()).ifPresent(legacyModel::setPartnershipInfo_initialLoanTenor);
			Optional.ofNullable(partnershipInformation.getMaxAmount()).ifPresent(legacyModel::setPartnershipInfo_maxAmount);
			Optional.ofNullable(partnershipInformation.getMaxTenor()).ifPresent(legacyModel::setPartnershipInfo_maxTenor);
		});

		Optional.ofNullable(salesTransactions).ifPresent(salesTransaction -> {
			List<LegacySalesTransaction> salesTransactionsList = new ArrayList<>();
			salesTransaction.forEach(v -> {
				LegacySalesTransaction legacySalesTransaction = new LegacySalesTransaction();
				Optional.ofNullable(v.getDate()).ifPresent(legacySalesTransaction::setSalesTransaction_date);
				Optional.ofNullable(v.getAmount()).ifPresent(legacySalesTransaction::setSalesTransaction_amount);
				Optional.ofNullable(v.getTransaction()).ifPresent(legacySalesTransaction::setSalesTransaction_transaction);

				salesTransactionsList.add(legacySalesTransaction);
			});
			legacyModel.setSalesTransactions(salesTransactionsList);
		});

		storeDataGlobalParam.getGlobalParam(GlobalConstants.GLOBAL_PARAM_KNOW_INVESTREE_FROM);

		Optional.ofNullable(referralData).ifPresent(reffData -> {
			List<DefaultData> kifList = Optional.ofNullable(storeDataGlobalParam)
				.map(StoreDataGlobalParam::getKifList)
				.orElse(new ArrayList<>());

			Optional.ofNullable(reffData.getKnowInvestreeFrom()).ifPresent(kifId -> {
				for (DefaultData kif : kifList) {
					if (kifId.equals(Long.valueOf(kif.getId()))) {
						legacyModel.setReferralData_kif(kif.getName());
					}
				}
			});

			Optional.ofNullable(reffData.getReferrerCode()).ifPresent(legacyModel::setReferralData_referrerCode);
			Optional.ofNullable(reffData.getReferralExpiredDate()).ifPresent(legacyModel::setReferralData_referralExpiredDate);
		});

		Optional.ofNullable(cifList).ifPresent(v -> legacyModel.setCifList_ClId(cifList.getId()));

		return legacyModel;
	}
}
