package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class AnchorPicChangesRequest{
	@JsonProperty("loginId")
	private Long loginId;

	@JsonProperty("salutation")
	private String salutation;

	@JsonProperty("fullname")
	private String fullname;

	@JsonProperty("username")
	private String username;

	@JsonProperty("email")
	private String email;

	@JsonProperty("mobilePrefix")
	private String mobilePrefix;

	@JsonProperty("phoneNumber")
	private String phoneNumber;

	@JsonProperty("status")
	private Boolean status;

	@JsonProperty("resetPassword")
	private Boolean resetPassword;

	@JsonProperty("nationality")
	private Long nationality;

	@JsonProperty("position")
	private Long position;

	@JsonProperty("ktpFile")
	private String ktpFile;

	@JsonProperty("ktpNumber")
	private String ktpNumber;

	@JsonProperty("picFrom")
	private Long picFrom;

	@JsonProperty("address")
	private String address;

	@JsonProperty("roles")
	private List<PicRolesRequest> roles;
}
