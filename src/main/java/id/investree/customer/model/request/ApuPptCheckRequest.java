package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class ApuPptCheckRequest {

	@JsonProperty("apuPptId")
	private Long apuPptId;

	@JsonProperty("customerId")
	private Long customerId;

	@JsonProperty("checkingDate")
	private Date checkingDate;

	@JsonProperty("checkingFile")
	private String checkingFile;

	@JsonProperty("checkingResult")
	private Object checkingResult;
}
