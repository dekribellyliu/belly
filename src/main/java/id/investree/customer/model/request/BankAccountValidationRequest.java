package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BankAccountValidationRequest {
	@JsonProperty("accountNumber")
	private String accountNumber;

	@JsonProperty("accountName")
	private String accountName;

	public BankAccountValidationRequest(String accountName, String accountNumber) {
		this.accountName = accountName;
		this.accountNumber = accountNumber;
	}
}
