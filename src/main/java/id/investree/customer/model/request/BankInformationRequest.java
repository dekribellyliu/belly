package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BankInformationRequest {
	@JsonProperty("bankInformationId")
	private Long bankInformationId;

	@JsonProperty("customerId")
	private Long customerId;

	@JsonProperty(value = "legacyId")
	private Long legacyId;

	@JsonProperty("masterBankId")
	private Long masterBankId;

	@JsonProperty("bankAccountCoverFile")
	private String bankAccountCoverFile;

	@JsonProperty("bankAccountNumber")
	private String bankAccountNumber;

	@JsonProperty("bankAccountHolderName")
	private String bankAccountHolderName;

	@JsonProperty("useAsDisbursement")
	private Boolean useAsDisbursement;

	@JsonProperty("useAsWithdrawal")
	private Boolean useAsWithdrawal;

	@JsonProperty("isDelete")
	private boolean isDelete;
}
