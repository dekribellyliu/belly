package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class BusinessInformationRequest {
	@JsonProperty("customerId")
	private Long customerId;

	@JsonProperty("businessProfile")
	private BusinessProfileRequest businessProfile;

	@JsonProperty("productPreference")
	private UpdateProductPreferenceRequest productPreference;

	@JsonProperty("apuPptInformation")
	private ApuPptCheckRequest apuPptInformation;

	@JsonProperty("bankInformation")
	private List<BankInformationRequest> bankInformation;

	@JsonProperty("legalInformation")
	private List<LegalInformationDataRequest> legalInformation;

	@JsonProperty("partnershipInformation")
	private List<PartnershipInformationRequest> partnershipInformation;

	@JsonProperty("shareholdersInformation")
	private List<ShareholdersInformationRequest> shareholdersInformation;
}
