package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class BusinessProfileRequest {
	@JsonProperty("customerId")
	private Long customerId;

	@JsonProperty("companyName")
	private String companyName;

	@JsonProperty("legalEntity")
	private Integer legalEntity;

	@JsonProperty("industry")
	private String industry;

	@JsonProperty("dateOfEstablishment")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	private Date dateOfEstablishment;

	@JsonProperty("numberOfEmployee")
	private Integer numberOfEmployee;

	@JsonProperty("companyDescription")
	private String companyDescription;

	@JsonProperty("companyNarration")
	private String companyNarration;

	@JsonProperty("companyAddress")
	private String companyAddress;

	@JsonProperty("nationality")
	private Object nationality;

	@JsonProperty("province")
	private Object province;

	@JsonProperty("city")
	private Object city;

	@JsonProperty("district")
	private Object district;

	@JsonProperty("village")
	private Object village;

	@JsonProperty("postalCode")
	private String postalCode;

	@JsonProperty("mobilePrefix")
	private String mobilePrefix;

	@JsonProperty("landLineNumber")
	private String landLineNumber;

	@JsonProperty("profitMargin")
	private Double profitMargin;

	@JsonProperty("averageMonthlySales")
	private Double averageMonthlySales;

	@JsonProperty("groupCompany")
	private String groupCompany;

	@JsonProperty("groupDescription")
	private String groupDescription;

	@JsonProperty("listOfPayor")
	private String listOfPayor;

	@JsonProperty("relationshipWithBank")
	private boolean relationshipWithBank;

	@JsonProperty("mailingAddressStatus")
	private Integer mailingAddressStatus;

	@JsonProperty("otherIncome")
	private Double otherIncome;

	@JsonProperty("generalProfitMargin")
	private Double generalProfitMargin;

	@JsonProperty("profitMarginFromPartner")
	private Double profitMarginFromPartner;

	@JsonProperty("totalNettMargin")
	private Double totalNettMargin;

	@JsonProperty("livingCost")
	private Double livingCost;
}
