package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CreateProductPrefRequest {
	@JsonProperty("companyName")
	private String companyName;

	@JsonProperty("legalEntity")
	private Integer legalEntity;

	@JsonProperty("userCategory")
	private Integer userCategory;

	@JsonProperty("productPreference")
	private Integer productPreference;

	@JsonProperty("productSelection")
	private Integer productSelection;
}
