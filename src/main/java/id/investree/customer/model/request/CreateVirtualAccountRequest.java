package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CreateVirtualAccountRequest {
	@JsonProperty("virtualAccountName")
	private String virtualAccountName;

	@JsonProperty("userType")
	private Integer userType;

	@JsonProperty("userPreference")
	private Integer userPreference;
}
