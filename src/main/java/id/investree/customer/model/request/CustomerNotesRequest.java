package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CustomerNotesRequest {
    @JsonProperty("customerNotesId")
    private Long customerNotesId;

	@JsonProperty("customerId")
    private Long customerId;

	@JsonProperty("customerType")
    private Long customerType;

	@JsonProperty("remarks")
    private String remarks;
}
