package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class DeletePicAnchorRequest {
	@JsonProperty("customerId")
	private Long customerId;

	@JsonProperty("pic")
	private PicAnchorRequest pic;
}
