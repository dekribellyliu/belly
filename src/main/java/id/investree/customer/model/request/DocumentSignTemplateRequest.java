package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class DocumentSignTemplateRequest {
	@JsonProperty("userRoleType")
	private Integer userRoleType;

	@JsonProperty("recipient")
	private List<String> recipient;

	@JsonProperty("ccUser")
	private List<String> ccUser;

	@JsonProperty("loanType")
	private String loanType;

	@JsonProperty("documentType")
	private String documentType;

	@JsonProperty("docPartnerId")
	private String docPartnerId;

	@JsonProperty("documentUrl")
	private String documentUrl;
}
