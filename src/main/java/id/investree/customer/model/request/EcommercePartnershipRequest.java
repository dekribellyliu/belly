package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.response.DefaultResponse;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class EcommercePartnershipRequest {
	@JsonProperty("id")
	private Long id;

	@JsonProperty("customerId")
	private Long customerId;

	@JsonProperty("name")
	private String name;

	@JsonProperty("agreementFile")
	private String agreementFile;

	@JsonFormat(pattern = "yyyy-MM-dd")
	@JsonProperty("agreementExpired")
	private Date agreementExpired;

	@JsonProperty("status")
	private Boolean status;

	@JsonProperty("rm")
	private List<DefaultResponse> rm;

	@JsonProperty("initial")
	private String initial;

	@JsonProperty("logo")
	private String logo;

	@JsonProperty("remarks")
	private String remarks;

}
