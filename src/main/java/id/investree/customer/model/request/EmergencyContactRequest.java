package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;
@Data
public class EmergencyContactRequest {
	@JsonProperty("emergencyContactId")
	private Long emergencyContactId;

	@JsonProperty("customerId")
	private Long customerId;

	@JsonProperty("relationship")
	private Long relationship;

	@JsonProperty("fullName")
	private String fullName;

	@JsonProperty("mobilePrefix")
	private Object mobilePrefix;

	@JsonProperty("mobileNumber")
	private String mobileNumber;

	@JsonProperty("emailAddress")
	private String emailAddress;

	@JsonProperty("address")
	private String address;

	@JsonProperty("province")
	private Object province;

	@JsonProperty("city")
	private Object city;

	@JsonProperty("district")
	private Object district;

	@JsonProperty("village")
	private Object village;

	@JsonProperty("postalCode")
	private String postalCode;

	@JsonProperty("identityCardUrl")
	private String identityCardUrl;

	@JsonProperty("identityCardNumber")
	private String identityCardNumber;

	@JsonProperty("isDelete")
	private boolean isDelete;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	@JsonProperty("identityExpiryDate")
	private Date identityExpiryDate;
}
