package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class FinancialInformationDetailRequest {
	@JsonProperty("eStatementDocuments")
	private List<FinancialInformationListRequest> eStatementDocuments;

	@JsonProperty("financialStatementDocuments")
	private List<FinancialInformationListRequest> financialStatementDocuments;

	@JsonProperty("profitMargin")
	private Double profitMargin;

	@JsonProperty("averageMonthlySales")
	private Double averageMonthlySales;

	@JsonProperty("salesTransactionData")
	private List<SalesTransactionRequest> salesTransactionData;

	@JsonProperty("customerNotesData")
	private List<CustomerNotesRequest> customerNotesData;

	@JsonProperty("financialStatement")
	private List<FinancialStatementRequest> financialStatement;

	@JsonProperty("financialTrend")
	private List<FinancialTrendRequest> financialTrend;

	@JsonProperty("financialInformation")
	private BusinessProfileRequest financialInformation;
}
