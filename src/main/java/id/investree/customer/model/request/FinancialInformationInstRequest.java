package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class FinancialInformationInstRequest {
	@JsonProperty("customerId")
	private Long customerId;

	@JsonProperty("profitMargin")
	private Double profitMargin;

	@JsonProperty("averageMonthlySales")
	private Double averageMonthlySales;

	@JsonProperty("financialStatement")
	private List<FinancialInformationListRequest> financialStatement;

	@JsonProperty("bankStatement")
	private List<FinancialInformationListRequest> bankStatement;

	@JsonProperty("financialStatementDetail")
	private List<FinancialStatementRequest> financialStatementDetail;

	@JsonProperty("salesTransaction")
	private List<SalesTransactionRequest> salesTransaction;

	@JsonProperty("financialTrend")
	private List<FinancialTrendRequest> financialTrend;

	@JsonProperty("financialRatio")
	private List<FinancialRatioRequest> financialRatio;

	@JsonProperty("balanceSheet")
	private List<BalanceSheetRequest> balanceSheet;

	@JsonProperty("customerNotes")
	private CustomerNotesRequest customerNotes;
}
