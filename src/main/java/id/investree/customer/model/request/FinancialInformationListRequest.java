package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class FinancialInformationListRequest {
	@JsonProperty("statementId")
	private Long statementId;

	@JsonProperty("customerId")
	private Long customerId;

	@JsonProperty("statementFileType")
	private Integer statementFileType;

	@JsonProperty("statementUrl")
	private String statementUrl;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	@JsonProperty("statementFileAt")
	private Date statementFileAt;

	@JsonProperty("isDelete")
	private boolean isDelete;
}
