package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class FinancialRatioRequest {
	@JsonProperty("financialRatioId")
	private Long financialRatioId;

	@JsonProperty("yearTo")
	private Long yearTo;

	@JsonProperty("gpm")
	private Double gpm;

	@JsonProperty("npm")
	private Double npm;

	@JsonProperty("ardoh")
	private Double ardoh;

	@JsonProperty("invdoh")
	private Double invdoh;

	@JsonProperty("apdoh")
	private Double apdoh;

	@JsonProperty("cashCycle")
	private Double cashCycle;

	@JsonProperty("cashRatio")
	private Double cashRatio;

	@JsonProperty("ebitda")
	private Double ebitda;

	@JsonProperty("leverage")
	private Double leverage;

	@JsonProperty("wiNeeds")
	private Double wiNeeds;

	@JsonProperty("tie")
	private Double tie;

	@JsonProperty("dscr")
	private Double dscr;
}
