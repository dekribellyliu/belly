package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class FinancialStatementRequest {
	@JsonProperty("financialStatementId")
	private Long financialStatementId;

	@JsonProperty("yearTo")
	private Long yearTo;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	@JsonProperty("fiscalYear")
	private Date fiscalYear;

	@JsonProperty("sales")
	private Double sales;

	@JsonProperty("cogs")
	private Double cogs;

	@JsonProperty("grossProfit")
	private Double grossProfit;

	@JsonProperty("sga")
	private Double sga;

	@JsonProperty("depreciation")
	private Double depreciation;

	@JsonProperty("operatingProfit")
	private Double operatingProfit;

	@JsonProperty("interestExpense")
	private Double interestExpense;

	@JsonProperty("otherIncome")
	private Double otherIncome;

	@JsonProperty("otherExpense")
	private Double otherExpense;

	@JsonProperty("profitBeforeTax")
	private Double profitBeforeTax;

	@JsonProperty("tax")
	private Double tax;

	@JsonProperty("profitAfterTax")
	private Double profitAfterTax;

	@JsonProperty("installment")
	private Double installment;
}
