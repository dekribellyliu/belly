package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class FinancialTrendRequest {
	@JsonProperty("financialTrendId")
	private Long financialTrendId;

	@JsonProperty("trendPeriod")
	private String trendPeriod;

	@JsonProperty("sales")
	private Double sales;

	@JsonProperty("cogs")
	private Double cogs;

	@JsonProperty("grossProfit")
	private Double grossProfit;

	@JsonProperty("sga")
	private Double sga;

	@JsonProperty("operatingProfit")
	private Double operatingProfit;

	@JsonProperty("installment")
	private Double installment;

	@JsonProperty("profitBeforeTax")
	private Double profitBeforeTax;

	@JsonProperty("profitAfterTax")
	private Double profitAfterTax;
}
