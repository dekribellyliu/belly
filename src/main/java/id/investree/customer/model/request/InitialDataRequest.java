package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.customer.model.data.PartnersCashflowHistoryData;
import id.investree.customer.model.enums.PartnershipTypeEnum;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class InitialDataRequest {

	@JsonProperty("customerId")
	private Long customerId;

	@JsonProperty("userType")
	private Integer userType;

	@JsonProperty("loginDataId")
	private Long loginDataId;

	@JsonProperty("nationality")
	private Long nationality;

	@JsonProperty("bankId")
	private Long bankId;

	@JsonProperty("bankName")
	private Long bankName;

	@JsonProperty("bankAccountHolder")
	private String bankAccountHolder;

	@JsonProperty("bankNumber")
	private String bankNumber;

	@JsonProperty("position")
	private Long position;

	@JsonProperty("ktpFile")
	private String ktpFile;

	@JsonProperty("ktpNumber")
	private String ktpNumber;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	@JsonProperty("ktpExpired")
	private Date ktpExpired;

	@JsonProperty("legalEntity")
	private Integer legalEntity;

	@JsonProperty("companyName")
	private String companyName;

	@JsonProperty("address")
	private String address;

	@JsonProperty("roles")
	private List<PicRolesRequest> roles;

	@JsonProperty("placeOfBirth")
	private Long placeOfBirth;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	@JsonProperty("dateOfBirth")
	private Date dateOfBirth;

	@JsonProperty("userCategory")
	private String userCategory;

	@JsonProperty("placeOfBirthExternal")
	private String placeOfBirthExternal;

	@JsonProperty("provinceName")
	private String provinceName;

	@JsonProperty("cityName")
	private String cityName;

	@JsonProperty("districtName")
	private String districtName;

	@JsonProperty("subDistrictName")
	private String subDistrictName;

	@JsonProperty("postalCode")
	private String postalCode;

	@JsonProperty("phoneNumber")
	private String phoneNumber;

	@JsonProperty("mobilePrefix")
	private String mobilePrefix;

	@JsonProperty("domicileAddress")
	private String domicileAddress;

	@JsonProperty("domicileProvinceName")
	private String domicileProvinceName;

	@JsonProperty("domicileCityName")
	private String domicileCityName;

	@JsonProperty("domicileDistrictName")
	private String domicileDistrictName;

	@JsonProperty("domicileSubDistrictName")
	private String domicileSubDistrictName;

	@JsonProperty("domicilePostalCode")
	private String domicilePostalCode;

	@JsonProperty("legalInfoDocFileType")
	private Integer legalInfoDocFileType;

	@JsonProperty("legalInfoDocNumber")
	private String legalInfoDocNumber;

	@JsonProperty("sellerLink")
	private String sellerLink;

	@JsonProperty("businessDescription")
	private String businessDescription;

	@JsonProperty("businessAddress")
	private String businessAddress;

	@JsonProperty("businessProvinceName")
	private String businessProvinceName;

	@JsonProperty("businessCityName")
	private String businessCityName;

	@JsonProperty("businessDistrictName")
	private String businessDistrictName;

	@JsonProperty("businessSubDistrictName")
	private String businessSubDistrictName;

	@JsonProperty("businessPostalCode")
	private String businessPostalCode;

	@JsonProperty("bankMasterId")
	private Long bankMasterId;

	@JsonProperty("bankAccountName")
	private String bankAccountName;

	@JsonProperty("bankAccountNumber")
	private String bankAccountNumber;

	@JsonProperty("isRegisterFromApi")
	private boolean isRegisterFromApi = false;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	@JsonProperty("businessDateOfEstablished")
	private Date businessDateOfEstablished;

	@JsonProperty("sixMonthsAverageCashflow")
	private Double sixMonthsAverageCashflow;

	@JsonProperty("profitMargin")
	private Double profitMargin;

	@JsonProperty("cashflowHistory")
	private List<PartnersCashflowHistoryData> cashflowHistory;

	@JsonProperty("transactionHistory")
	private List<Integer> transactionHistory;

	@JsonProperty(value = "referral")
	private String referral;

	@JsonProperty(value = "va_number")
	private String vaNumber;

	@JsonProperty(value = "bpdNumber")
	private String bpdNumber;

	@JsonProperty(value = "ciCreatedAt")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "Asia/Jakarta")
	private Date ciCreatedAt;

	@JsonProperty(value = "otpCreatedAt")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "Asia/Jakarta")
	private Date otpCreatedAt;

	@JsonProperty("productSelections")
	private List<Integer> productSelections;

	@JsonProperty("partnershipInformationRequest")
	private PartnershipInformationRequest partnershipInformationRequest;

	@JsonProperty("isRegisterFromRetailPartner")
	private boolean isRegisterFromRetailPartner = false;

	@JsonProperty("ecommercePartnershipRequest")
	private EcommercePartnershipRequest ecommercePartnershipRequest;

	@JsonProperty("partneshipType")
	private PartnershipTypeEnum partnershipType;

}
