package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class InternalAnchorRequest {
	@JsonProperty("userType")
	private Integer userType;

	@JsonProperty("customerId")
	private Long customerId;

	@JsonProperty("legalEntity")
	private Integer legalEntity;

	@JsonProperty("companyName")
	private String companyName;

	@JsonProperty("bankId")
	private Long bankId;

	@JsonProperty("masterBankId")
	private Long masterBankId;

	@JsonProperty("bankAccountHolderName")
	private String bankAccountHolderName;

	@JsonProperty("bankAccountNumber")
	private String bankAccountNumber;

	@JsonProperty("newAnchorBank")
	private Boolean newAnchorBank;

	@JsonProperty("anchorStatus")
	private Boolean anchorStatus;

	@JsonProperty("pic")
	private List<PicAnchorRequest> pic;
}
