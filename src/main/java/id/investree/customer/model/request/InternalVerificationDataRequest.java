package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class InternalVerificationDataRequest {
	@JsonProperty("fullname")
	private String fullname;

	@JsonProperty("userCategory")
	private Integer userCategory;
}
