package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class InvestmentProfileBulkRequest {
	@JsonProperty("investmentProfile")
	private InvestmentProfileRequest investmentProfile;

	@JsonProperty("productPreference")
	private UpdateProductPreferenceRequest productPreference;

	@JsonProperty("apuPptCheck")
	private ApuPptCheckRequest apuPptCheck;

	@JsonProperty("personalData")
	private PersonalDataRequest personalData;
}
