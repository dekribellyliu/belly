package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class InvestmentProfileRequest {
	@JsonProperty("investmentObjective")
	private Long investmentObjective;

	@JsonProperty("riskProfile")
	private Long riskProfile;
}
