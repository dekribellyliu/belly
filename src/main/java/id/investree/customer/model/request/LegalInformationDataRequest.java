package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.DefaultData;
import lombok.Data;

import java.util.Date;

@Data
public class LegalInformationDataRequest {
	@JsonProperty("documentType")
	private DefaultData documentType;

	@JsonProperty("documentFile")
	private String documentFile;

	@JsonProperty("documentNumber")
	private String documentNumber;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	@JsonProperty("documentExpiredDate")
	private Date documentExpiredDate;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	@JsonProperty("documentRegisterDate")
	private Date documentRegisterDate;
}
