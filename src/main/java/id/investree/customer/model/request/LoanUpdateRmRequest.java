package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class LoanUpdateRmRequest {
	@JsonProperty("borrowerId")
	private Long borrowerId;

	@JsonProperty("rmId")
	private Long rmId;

	public LoanUpdateRmRequest() {
	}

	public LoanUpdateRmRequest(Long borrowerId, Long rmId) {
		this.borrowerId = borrowerId;
		this.rmId = rmId;
	}
}
