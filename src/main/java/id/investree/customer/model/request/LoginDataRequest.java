package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class LoginDataRequest {
	@JsonProperty("id")
	private Long id;

	@JsonProperty("legacyId")
	private Long legacyId;

	@JsonProperty("userNumber")
	private String userNumber;

	@JsonProperty("userToken")
	private String userToken;

	@JsonProperty("fullname")
	private String fullname;

	@JsonProperty("referralCode")
	private String referralCode;

	@JsonProperty("username")
	private String username;

	@JsonProperty("userType")
	private Integer userType;

	@JsonProperty("salutation")
	private String salutation;

	@JsonProperty("emailAddress")
	private String emailAddress;

	@JsonProperty("password")
	private String password;

	@JsonProperty("mobilePrefix")
	private String mobilePrefix;

	@JsonProperty("phoneNumber")
	private String phoneNumber;

	@JsonProperty("gender")
	private String gender;

	@JsonProperty("slackId")
	private String slackId;

	@JsonProperty("passwordUpdateDate")
	private Date passwordUpdateDate;

	@JsonProperty("isResetPassword")
	private String isResetPassword;

	@JsonProperty("active")
	private Boolean active;

	@JsonProperty("picture")
	private String picture;

	@JsonProperty("createdBy")
	private Long createdBy = 0L;

	@JsonProperty("updateBy")
	private Long updateBy = 0L;

	@JsonProperty("agreeSubscribe")
	private Boolean agreeSubscribe;

	@JsonProperty("agreePrivacy")
	private Boolean agreePrivacy;

	@JsonProperty("otpVerificationStatus")
	private String otpVerificationStatus;

	@JsonProperty("loginAttempt")
	private int loginAttempt;

	@JsonProperty("emailStatus")
	private String emailStatus;
}