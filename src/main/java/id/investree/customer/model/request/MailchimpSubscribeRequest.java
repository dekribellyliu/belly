package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.customer.model.data.MailchimpEmailData;
import id.investree.customer.model.data.MailchimpVarsData;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MailchimpSubscribeRequest {

    @JsonProperty("apikey")
    private String apiKey;

    @JsonProperty("id")
    private String id;

    @JsonProperty("email")
    private MailchimpEmailData email;

    @JsonProperty("merge_vars")
    private MailchimpVarsData mergeVars;

    @JsonProperty("double_optin")
    @Builder.Default
    private Boolean doubleOption = false;

    @JsonProperty("update_existing")
    @Builder.Default
    private Boolean updateExisting = true;

    @JsonProperty("replace_interests")
    @Builder.Default
    private Boolean replaceInterests = false;

    @JsonProperty("send_welcome")
    @Builder.Default
    private Boolean sendWelcome = false;

}
