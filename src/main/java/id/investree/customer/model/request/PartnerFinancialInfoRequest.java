package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class PartnerFinancialInfoRequest {
	@JsonProperty("salesTransaction")
	private List<PartnerSalesTransactionRequest> salesTransaction;

	@JsonProperty("grossProfit")
	private Double grossProfit;
}
