package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class PartnerRegistrationRequest {
	@JsonProperty("loginDataId")
	private Long loginDataId;

	@JsonProperty("placeOfBirth")
	private Long placeOfBirth;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	@JsonProperty("dateOfBirth")
	private Date dateOfBirth;

	@JsonProperty("selfiePicture")
	private String selfiePicture;

	@JsonProperty("idCardPicture")
	private String idCardPicture;

	@JsonProperty("sameAsDomicileAddress")
	private Boolean sameAsDomicileAddress;

	@JsonProperty("address")
	private String address;

	@JsonProperty("province")
	private Long province;

	@JsonProperty("city")
	private Long city;

	@JsonProperty("district")
	private Long district;

	@JsonProperty("subDistrict")
	private Long subDistrict;

	@JsonProperty("postalCode")
	private String postalCode;

	@JsonProperty("domicileAddress")
	private String domicileAddress;

	@JsonProperty("domicileProvince")
	private Long domicileProvince;

	@JsonProperty("domicileCity")
	private Long domicileCity;

	@JsonProperty("domicileDistrict")
	private Long domicileDistrict;

	@JsonProperty("domicileSubDistrict")
	private Long domicileSubDistrict;

	@JsonProperty("domicilePostalCode")
	private String domicilePostalCode;

	@JsonProperty("idCardNumber")
	private String idCardNumber;

	@JsonProperty("idCardExpiredDate")
	private Date idCardExpiredDate;

	@JsonProperty("positionWithInstitution")
	private Long positionWithInstitution;
}
