package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class PartnerSalesTransactionRequest {
	@JsonProperty("salesDate")
	private Date salesDate;

	@JsonProperty("transaction")
	private Double transaction;

	@JsonProperty("partnerTransaction")
	private Double partnerTransaction;
}
