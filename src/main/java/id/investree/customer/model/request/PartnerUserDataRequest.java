package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
public class PartnerUserDataRequest extends UpdateUserDataRequest {

	@JsonProperty("fillFinishAt")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
	private Date fillFinishAt;

	@JsonProperty("domicileProvinceName")
	private String domicileProvinceName;

	@JsonProperty("domicileCityName")
	private String domicileCityName;

	@JsonProperty("domicileDistrictName")
	private String domicileDistrictName;

	@JsonProperty("domicileVillageName")
	private String domicileVillageName;
}
