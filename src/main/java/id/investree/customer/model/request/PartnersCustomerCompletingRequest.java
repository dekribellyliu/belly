package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PartnersCustomerCompletingRequest {
	@JsonProperty("productPrefRequest")
	private CreateProductPrefRequest productPrefRequest;

	@JsonProperty("businessProfile")
	private BusinessProfileRequest businessProfile;

	@JsonProperty("userData")
	private PartnerUserDataRequest userData;

	@JsonProperty("bankInformation")
	private BankInformationRequest bankInformation;

	@JsonProperty("legalInformation")
	private LegalInformationRequest legalInformation;

	@JsonProperty("financialInfo")
	private PartnerFinancialInfoRequest financialInfo;

	@JsonProperty("partnershipInfo")
	private PartnershipInformationRequest partnershipInformation;

	@JsonProperty("vaNumber")
	private String vaNumber;

	@JsonProperty("vaStatus")
	private String vaStatus;

}
