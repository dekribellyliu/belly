package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class PersonalDataRequest {
	@JsonProperty("nationality")
	private Long nationality;

	@JsonProperty("selfiePicture")
	private String selfiePicture;

	@JsonProperty("idCardPicture")
	private String idCardPicture;

	@JsonProperty("idCardNumber")
	private String idCardNumber;

	@JsonProperty("idCardExpiredDate")
	private String idCardExpiredDate;

	@JsonProperty("sameAsDomicileAddress")
	private Boolean sameAsDomicileAddress;

	@JsonProperty("address")
	private String address;

	@JsonProperty("province")
	private Long province;

	@JsonProperty("city")
	private Long city;

	@JsonProperty("district")
	private Long district;

	@JsonProperty("maritalStatus")
	private Long maritalStatus;

	@JsonProperty("subDistrict")
	private Long subDistrict;

	@JsonProperty("postalCode")
	private String postalCode;

	@JsonProperty("placeOfBirth")
	private Long placeOfBirth;

	@JsonProperty("placeOfBirthExternal")
	private String placeOfBirthExternal;

	@JsonProperty("dateOfBirth")
	private Date dateOfBirth;

	@JsonProperty("religion")
	private Long religion;

	@JsonProperty("education")
	private Long education;

	@JsonProperty("occupation")
	private Long occupation;

	@JsonProperty("domicileAddress")
	private String domicileAddress;

	@JsonProperty("domicileProvince")
	private Long domicileProvince;

	@JsonProperty("domicileCity")
	private Long domicileCity;

	@JsonProperty("domicileDistrict")
	private Long domicileDistrict;

	@JsonProperty("domicileSubdistrict")
	private Long domicileSubdistrict;

	@JsonProperty("domicilePostalCode")
	private String domicilePostalCode;

	@JsonProperty("motherName")
	private String motherName;

	@JsonProperty("fieldOfWork")
	private Long fieldOfWork;

	@JsonProperty("occupationAddress")
	private String occupationAddress;

	@JsonProperty("occupationCity")
	private Long occupationCity;

	@JsonProperty("occupationPostalCode")
	private String occupationPostalCode;

	@JsonProperty("occupationPhoneNumber")
	private String occupationPhoneNumber;

	@JsonProperty("mailingAddressStatus")
	private Integer mailingAddressStatus;

	@JsonProperty("annualIncome")
	private Long annualIncome;

	@JsonProperty("sourceOfFund")
	private Long sourceOfFund;

	@JsonProperty("mobilePrefix")
	private String mobilePrefix;
}
