package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.request.ParamRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
public class PersonalProfileIdentificationRequest extends ParamRequest {
	@JsonProperty("nationality")
	private Long nationality;

	@JsonProperty("selfiePicture")
	private String selfiePicture;

	@JsonProperty("idCardPicture")
	private String idCardPicture;

	@JsonProperty("idCardNumber")
	private String idCardNumber;

	@JsonProperty("idCardExpiredDate")
	private Date idCardExpiredDate;

	@JsonProperty("address")
	private String address;

	@JsonProperty("province")
	private Long province;

	@JsonProperty("city")
	private Long city;

	@JsonProperty("district")
	private Long district;

	@JsonProperty("subDistrict")
	private Long subDistrict;

	@JsonProperty("postalCode")
	private String postalCode;

	@JsonProperty("mailingAddressStatus")
	private Integer mailingAddressStatus;
}
