package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class PicAnchorRequest {
	@JsonProperty("loginId")
	private Long loginId;

	@JsonProperty("nationality")
	private Long nationality;

	@JsonProperty("position")
	private Long position;

	@JsonProperty("ktpFile")
	private String ktpFile;

	@JsonProperty("ktpNumber")
	private String ktpNumber;

	@JsonProperty("picFrom")
	private Long picFrom;

	@JsonProperty("address")
	private String address;

	@JsonProperty("status")
	private Boolean status;

	@JsonProperty("isDelete")
	private Boolean isDelete;

	@JsonProperty("changeTo")
	private AnchorPicChangesRequest changeTo;

	@JsonProperty("roles")
	private List<PicRolesRequest> roles;
}
