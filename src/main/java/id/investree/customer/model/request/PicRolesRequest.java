package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PicRolesRequest {
	@JsonProperty("role")
	private Integer role;
}
