package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RecreateVirtualAccountRequest {
	@JsonProperty("accountName")
	private String accountName;

	@JsonProperty("bin")
	private String bin;

	@JsonProperty("vaNumber")
	private String vaNumber;
}
