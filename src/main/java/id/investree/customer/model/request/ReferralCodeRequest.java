package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class ReferralCodeRequest {
	@JsonProperty("referrerCode")
	private String referrerCode;

	@JsonProperty("referralUserId")
	private Long referralUserId;

	@JsonProperty("referrerUserId")
	private Long referrerUserId;

	@JsonProperty("expiredDate")
	private Date expiredDate;

	@JsonProperty("knowInvestreeFrom")
	private Long knowInvestreeFrom;

	@JsonProperty("emailAddress")
	private String emailAddress;
}
