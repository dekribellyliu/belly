package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RegistrationStatusRequest {
	@JsonProperty("userType")
	private String userType;

	@JsonProperty("status")
	private String status;
}
