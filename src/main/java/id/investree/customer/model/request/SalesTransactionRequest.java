package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class SalesTransactionRequest {
    @JsonProperty("salesTransactionId")
    private Long salesTransactionId;

	@JsonProperty("customerId")
    private Long customerId;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
    @JsonProperty("dates")
    private Date dates;

	@JsonProperty("amount")
    private Double amount;

	@JsonProperty("transaction")
    private Double transaction;

	@JsonProperty("isDelete")
    private boolean isDelete;
}
