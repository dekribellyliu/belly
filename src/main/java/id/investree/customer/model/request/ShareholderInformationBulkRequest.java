package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ShareholderInformationBulkRequest {
	@JsonProperty("customerId")
	private Long customerId;

	@JsonProperty("data")
	private List<ShareholdersInformationRequest> data;
}
