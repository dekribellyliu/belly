package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class ShareholderInformationSyncLegacyRequest {

	@JsonProperty(value = "shi_id")
	private Long shareholderInfo_id;

	@JsonProperty(value = "shi_fullname")
	private String shareholderInfo_fullName;

	@JsonProperty(value = "shi_email_address")
	private String shareholderInfo_emailAddress;

	@JsonProperty(value = "shi_position")
	private Long shareholderInfo_position;

	@JsonProperty(value = "shi_mobile_prefix")
	private Long shareholderInfo_mobilePrefix;

	@JsonProperty(value = "shi_mobile_number")
	private String shareholderInfo_mobileNumber;

	@JsonProperty(value = "shi_dob")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	private Date shareholderInfo_dob;

	@JsonProperty(value = "shi_identity_card_file")
	private String shareholderInfo_identificationCardUrl;

	@JsonProperty(value = "shi_identity_card_number")
	private String shareholderInfo_identificationCardNumber;

	@JsonProperty(value = "shi_identity_card_expired")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	private Date shareholderInfo_identificationCardExpiryDate;

	@JsonProperty(value = "shi_tax_card_file")
	private String shareholderInfo_taxCardUrl;

	@JsonProperty(value = "shi_tax_card_number")
	private String shareholderInfo_taxCardNumber;

	@JsonProperty(value = "shi_stock_ownership_rate")
	private Float shareholderInfo_stockOwnership;

	@JsonProperty(value = "shi_deleted_at")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	private Date shareholderInfo_deletedAt;

	@JsonProperty(value = "shi_is_lss")
	private String shareholderInfo_isLss;

	@JsonProperty(value = "shi_is_pgs")
	private String shareholderInfo_isPgs;

	@JsonProperty(value = "shi_pg_file")
	private String shareholderInfo_pgFile;

	@JsonProperty(value = "shi_pg_no")
	private String shareholderInfo_pgNumber;

	@JsonProperty(value = "shi_pg_sd")
	private Date shareholderInfo_pgSignedDate;

	@JsonProperty(value = "shi_pg_amount")
	private Long shareholderInfo_pgAmount;
}
