package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;
@Data
public class ShareholdersInformationRequest {
	@JsonProperty("shareHolderId")
	private Long shareHolderId;

	@JsonProperty("customerId")
	private Long customerId;

	@JsonProperty("position")
	private Long position;

	@JsonProperty("fullName")
	private String fullName;

	@JsonProperty("mobilePrefix")
	private Long mobilePrefix;

	@JsonProperty("mobileNumber")
	private String mobileNumber;

	@JsonProperty("emailAddress")
	private String emailAddress;

	@JsonProperty("stockOwnership")
	private Float stockOwnership;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	@JsonProperty("dob")
	private Date dob;

	@JsonProperty("identificationCardUrl")
	private String identificationCardUrl;

	@JsonProperty("identificationCardNumber")
	private String identificationCardNumber;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	@JsonProperty("identificationCardExpiryDate")
	private Date identificationCardExpiryDate;

	@JsonProperty("selfieUrl")
	private String selfieUrl;

	@JsonProperty("taxCardUrl")
	private String taxCardUrl;

	@JsonProperty("taxCardNumber")
	private String taxCardNumber;

	@JsonProperty("isLss")
	private boolean isLss;

	@JsonProperty("isPgs")
	private boolean isPgs;

	@JsonProperty("isTss")
	private boolean isTss;

	@JsonProperty("isDelete")
	private boolean isDelete;

	@JsonProperty("apuPptCheck")
	private ApuPptCheckRequest apuPptCheck;

	@JsonProperty("pgNumber")
	private String pgNumber;

	@JsonProperty("pgAmount")
	private Long pgAmount;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	@JsonProperty("pgSignedDate")
	private Date pgSignedDate;

	@JsonProperty("pgType")
	private String pgType;

	@JsonProperty("pgFile")
	private String pgFile;
}
