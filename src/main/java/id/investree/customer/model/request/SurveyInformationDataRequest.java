package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class SurveyInformationDataRequest {

	@JsonProperty("surveyInformationId")
	private Long surveyInformationId;

	@JsonProperty("surveyBy")
	private Long surveyBy;

	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	@JsonProperty("surveyAt")
	private Date surveyAt;

	@JsonProperty("borrowerPosition")
	private Long borrowerPosition;

	@JsonProperty("borrowerName")
	private String borrowerName;

	@JsonProperty("numberOfEmployee")
	private Long numberOfEmployee;

	@JsonProperty("officeStatus")
	private Long officeStatus;

	@JsonProperty("lengthOfStay")
	private String lengthOfStay;

	@JsonProperty("filename")
	private List<String> filename;

	@JsonProperty("resultDescription")
	private String resultDescription;

	@JsonProperty("address")
	private String address;

	@JsonProperty("isDelete")
	private boolean isDelete;
}
