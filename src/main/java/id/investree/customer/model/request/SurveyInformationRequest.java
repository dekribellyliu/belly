package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class SurveyInformationRequest {
	@JsonProperty("customerId")
	private Long customerId;

	@JsonProperty("data")
	private List<SurveyInformationDataRequest> data;
}
