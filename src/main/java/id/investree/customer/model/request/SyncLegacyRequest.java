package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class SyncLegacyRequest {

	@JsonProperty(value = "ci_initial")
	private String customerInfo_Initial;

	@JsonProperty(value = "bd_description")
	private String businessData_Description;

	@JsonProperty(value = "bd_noe")
	private Integer businessData_numberOfEmployee;

	@JsonProperty(value = "bd_doe")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	private Date businessData_doe;

	private ShareholderInformationSyncLegacyRequest shareholderInformation;

	@JsonProperty(value = "bi_bank_id")
	private Long bankInfo_masterBankId;

	@JsonProperty(value = "bi_bank_account_number")
	private String bankInfo_bankAccountNumber;

	@JsonProperty(value = "bi_bank_account_holder")
	private String bankInfo_bankAccountHolderName;

	@JsonProperty(value = "pi_blo")
	private Long partnershipInfo_buildingLocOwnership;

	@JsonProperty(value = "pi_iecr")
	private String partnershipInfo_internalRating;

	@JsonProperty(value = "pi_eecr")
	private String partnershipInfo_externalRating;

	@JsonProperty(value = "ud_domicile_address")
	private String userData_domicileAddress;

	@JsonProperty(value = "ud_domicile_province")
	private Long userData_domicileProvince;

	@JsonProperty(value = "ud_domicile_city")
	private Long userData_domicileCity;

	@JsonProperty(value = "ud_domicile_district")
	private Long userData_domicileDistrict;

	@JsonProperty(value = "ud_domicile_village")
	private Long userData_domicileVillage;

	@JsonProperty(value = "ud_domicile_postal_code")
	private String userData_domicilePostalCode;

	@JsonProperty(value = "ud_identity_card_expired")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	private Date userData_idCardExpired;

	@JsonProperty(value = "ud_dob")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
	private Date userData_dateOfBirth;
}
