package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.utils.Utilities;
import lombok.Data;

import java.util.Optional;

@Data
public class TransactionHistoryRequest {

	@JsonProperty("partnershipId")
	private String partnershipId;

	@JsonProperty("page")
	private Integer page;

	@JsonProperty("size")
	private Integer size;

	@JsonProperty("sort")
	private String sort;

	@JsonProperty("buyerName")
	private String buyerName;

	public TransactionHistoryRequest setPartnershipId(String partnershiId) {
		this.partnershipId = partnershiId;
		return this;
	}

	public TransactionHistoryRequest setPage(Integer page) {
		this.page = page;
		return this;
	}

	public TransactionHistoryRequest setSize(Integer size) {
		this.size = size;
		return this;
	}

	public TransactionHistoryRequest setSort(String sort) {
		this.sort = sort;
		return this;
	}

	public TransactionHistoryRequest setBuyerName(String buyerName) {
		this.buyerName = buyerName;
		return this;
	}

	public TransactionHistoryRequest build() {
		TransactionHistoryRequest request = new TransactionHistoryRequest();

		Optional.ofNullable(partnershipId).filter(s -> !Utilities.isEmptyOrBlank(s)).ifPresent(request::setPartnershipId);
		Optional.ofNullable(page).ifPresent(request::setPage);
		Optional.ofNullable(size).ifPresent(request::setSize);
		Optional.ofNullable(sort).filter(s -> !Utilities.isEmptyOrBlank(s)).ifPresent(request::setSort);
		Optional.ofNullable(buyerName).filter(s -> !Utilities.isEmptyOrBlank(s)).ifPresent(request::setBuyerName);

		return request;
	}

}
