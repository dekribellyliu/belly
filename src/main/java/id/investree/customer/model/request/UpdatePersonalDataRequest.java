package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class UpdatePersonalDataRequest {
	@JsonProperty("basicData")
	private LoginDataRequest basicData;

	@JsonProperty("personalData")
	private UpdateUserDataRequest personalData;

	@JsonProperty("bankInformation")
	private List<BankInformationRequest> bankInformation;

	@JsonProperty("legalInformation")
	private List<LegalInformationDataRequest> legalInformation;

	@JsonProperty("emergencyContact")
	private List<EmergencyContactRequest> emergencyContact;

	@JsonProperty("annualIncome")
	private Long annualIncome;

	@JsonProperty("sourceOfFund")
	private Long sourceOfFund;
}
