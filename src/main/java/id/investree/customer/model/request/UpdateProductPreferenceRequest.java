package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class UpdateProductPreferenceRequest {
	@JsonProperty("productPreference")
	private Integer productPreference;
}
