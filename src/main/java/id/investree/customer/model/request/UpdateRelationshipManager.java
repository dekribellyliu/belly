package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class UpdateRelationshipManager {
	@JsonProperty("userRoleType")
	private Integer userRoleType;

	@JsonProperty("rmLoginId")
	private Long rmLoginId;
}
