package id.investree.customer.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.request.ParamRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
public class UpdateUserDataRequest extends ParamRequest {

	@JsonProperty("nationality")
	private Long nationality;

	@JsonProperty("positionWithInstitution")
	private Long positionWithInstitution;

	@JsonProperty("divisionWithInstitution")
	private Long divisionWithInstitution;

	@JsonProperty("maritalStatus")
	private Long maritalStatus;

	@JsonProperty("education")
	private Long education;

	@JsonProperty("occupation")
	private Long occupation;

	@JsonProperty("selfiePicture")
	private String selfiePicture;

	@JsonProperty("idCardPicture")
	private String idCardPicture;

	@JsonProperty("selfieKtpPicture")
	private String selfieKtpPicture;

	@JsonProperty("signaturePicture")
	private String signaturePicture;

	@JsonProperty("idCardNumber")
	private String idCardNumber;

	@JsonProperty("idCardExpiredDate")
	private Date idCardExpiredDate;

	@JsonProperty("placeOfBirth")
	private Long placeOfBirth;

	@JsonProperty("placeOfBirthExternal")
	private String placeOfBirthExternal;

	@JsonProperty("dateOfBirth")
	private Date dateOfBirth;

	@JsonProperty("religion")
	private Long religion;

	@JsonProperty("address")
	private String address;

	@JsonProperty("province")
	private Long province;

	@JsonProperty("city")
	private Long city;

	@JsonProperty("district")
	private Long district;

	@JsonProperty("subDistrict")
	private Long subDistrict;

	@JsonProperty("postalCode")
	private String postalCode;

	@JsonProperty("sameAsDomicileAddress")
	private Boolean sameAsDomicileAddress;

	@JsonProperty("domicileAddress")
	private String domicileAddress;

	@JsonProperty("domicileProvince")
	private Long domicileProvince;

	@JsonProperty("domicileCity")
	private Long domicileCity;

	@JsonProperty("domicileDistrict")
	private Long domicileDistrict;

	@JsonProperty("domicileSubDistrict")
	private Long domicileSubDistrict;

	@JsonProperty("domicileProvinceName")
	private String domicileProvinceName;

	@JsonProperty("domicileCityName")
	private String domicileCityName;

	@JsonProperty("domicileDistrictName")
	private String domicileDistrictName;

	@JsonProperty("domicileSubDistrictName")
	private String domicileSubDistrictName;

	@JsonProperty("domicilePostalCode")
	private String domicilePostalCode;

	@JsonProperty("poaFile")
	private String poaFile;

	@JsonProperty("mailingAddressStatus")
	private Integer mailingAddressStatus;

	@JsonProperty("motherMaidenName")
	private String motherMaidenName;

	@JsonProperty("occupationAddress")
	private String occupationAddress;

	@JsonProperty("occupationCity")
	private Long occupationCity;

	@JsonProperty("occupationPostalCode")
	private String occupationPostalCode;

	@JsonProperty("occupationPhoneNumber")
	private String occupationPhoneNumber;

	@JsonProperty("annualIncome")
	private Long annualIncome;

	@JsonProperty("sourceOfFund")
	private Long sourceOfFund;

	@JsonProperty("occupationMobilePrefix")
	private String occupationMobilePrefix;

	@JsonProperty("registerFromApi")
	private boolean registerFromApi = false;

	@JsonProperty("status")
	private Long status;
}
