package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.DefaultData;
import id.investree.customer.model.data.BankInformationData;
import id.investree.customer.model.data.PicInformationData;
import lombok.Data;

import java.util.List;

@Data
public class AnchorResponse {
	@JsonProperty("companyName")
	private String companyName;

	@JsonProperty("legalEntity")
	private DefaultData legalEntity;

	@JsonProperty("anchorStatus")
	private Boolean anchorStatus;

	@JsonProperty("anchor")
	private Boolean anchor;

	@JsonProperty("bankInformation")
	private List<BankInformationData> bankInformation;

	@JsonProperty("picInformation")
	private List<PicInformationData> picInformation;
}
