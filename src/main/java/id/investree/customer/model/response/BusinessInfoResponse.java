package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.customer.model.data.BusinessInfoData;
import id.investree.customer.model.data.CounterData;
import lombok.Data;

@Data
public class BusinessInfoResponse {
	@JsonProperty("businessInfo")
	private BusinessInfoData businessInfo;

	@JsonProperty("counter")
	private CounterData counter;
}
