package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.customer.model.data.*;
import lombok.Data;

import java.util.List;

@Data
public class BusinessInformationResponse {
	@JsonProperty("customerId")
	private Long customerId;

	@JsonProperty("businessInfoData")
	private BusinessInfoData businessInfoData;

	@JsonProperty("productPreferenceId")
	private Integer productPreferenceId;

	@JsonProperty("backgroundCheckData")
	private BackgroundCheckData backgroundCheckData;

	@JsonProperty("bankInformationData")
	private BankInformationParent bankInformationData;

	@JsonProperty("legalInfoGroup")
	private LegalInfoGroup legalInfoGroup;

	@JsonProperty("partnershipInformation")
	private List<PartnershipInformationData> partnershipInformation;

	@JsonProperty("shareholdersInformation")
	private List<ShareholdersInformationData> shareholdersInformation;
}
