package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class BusinessProfileResponse {
	@JsonProperty("averageMonthlySales")
	private Double averageMonthlySales;

	@JsonProperty("profitMargin")
	private Double profitMargin;

	@JsonProperty("otherIncome")
	private Double otherIncome;

	@JsonProperty("generalProfitMargin")
	private Double generalProfitMargin;

	@JsonProperty("profitMarginFromPartner")
	private Double profitMarginFromPartner;

	@JsonProperty("totalNettMargin")
	private Double totalNettMargin;

	@JsonProperty("livingCost")
	private Double livingCost;

	@JsonProperty("updatedBy")
	private Long updatedBy;

	@JsonProperty("updatedAt")
	private Date updatedAt;
}
