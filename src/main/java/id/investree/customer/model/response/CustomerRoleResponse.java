package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.customer.entity.CustomerRole;
import lombok.Data;

import java.util.Date;

@Data
public class CustomerRoleResponse {
	@JsonProperty("customerInformationId")
	private Long customerInformationId;

	@JsonProperty("type")
	private Integer type;

	@JsonProperty("registrationStatus")
	private RegisterStatusResponse registrationStatus;

	@JsonProperty("createdBy")
	private Long createdBy;

	@JsonProperty("createdAt")
	private Date createdAt;

	@JsonProperty("updatedBy")
	private Long updatedBy;

	@JsonProperty("updatedAt")
	private Date updatedAt;

	@JsonProperty("registeredAt")
	private Date registeredAt;

	@JsonProperty("poaFile")
	private String poaFile;

	@JsonProperty("rmBy")
	private Long rmBy;

	@JsonProperty("activeAt")
	private Date activeAt;

	@JsonProperty("activeBy")
	private Long activeBy;

	@JsonProperty("lsfBullet")
	private Double lsfBullet;

	@JsonProperty("installment")
	private Double installment;

	@JsonProperty("feeStatus")
	private String feeStatus;

	@JsonProperty("autoWithdrawalStatus")
	private String autoWithdrawalStatus;

	@JsonProperty("withHoldingTaxStatus")
	private String withHoldingTaxStatus;

	public CustomerRoleResponse(CustomerRole customerRole, RegisterStatusResponse status) {
		customerInformationId = customerRole.getCustomerInformationId();
		type = customerRole.getType();
		createdBy = customerRole.getCreatedBy();
		createdAt = customerRole.getCreatedAt();
		updatedAt = customerRole.getUpdatedAt();
		updatedBy = customerRole.getUpdatedBy();
		registeredAt = customerRole.getRegisteredAt();
		poaFile = customerRole.getPoaFile();
		rmBy = customerRole.getRmBy();
		activeAt = customerRole.getActiveAt();
		activeBy = customerRole.getActiveBy();
		lsfBullet = customerRole.getLsfBullet();
		installment = customerRole.getInstallment();
		feeStatus = customerRole.getFeeStatus();
		autoWithdrawalStatus = customerRole.getAutoWithdrawalStatus();
		withHoldingTaxStatus = customerRole.getWithHoldingTaxStatus();
		registrationStatus = status;
	}
}
