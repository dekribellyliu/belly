package id.investree.customer.model.response;

public class DataResponse {

	private Long currentPage;
	private Long totalPage;
	private Long totalData;
	
	public Long getCurrentPage() {
		return currentPage;
	}
	
	public void setCurrentPage(Long currentPage) {
		this.currentPage = currentPage;
	}
	
	public Long getTotalPage() {
		return totalPage;
	}
	
	public void setTotalPage(Long totalPage) {
		this.totalPage = totalPage;
	}
	
	public Long getTotalData() {
		return totalData;
	}
	
	public void setTotalData(Long totalData) {
		this.totalData = totalData;
	}
	
}
