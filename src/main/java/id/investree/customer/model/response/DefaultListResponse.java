package id.investree.customer.model.response;

import java.util.List;

public class DefaultListResponse<T> extends DataResponse {

	private List<T> data;

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}
}
