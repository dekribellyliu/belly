package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class DigitalSignatureResponse {
	@JsonProperty("pmpShariaFile")
	private String pmpShariaFile;

	@JsonProperty("pmpKonvensionalFile")
	private String pmpKonvensionalFile;

	@JsonProperty("poaFile")
	private String poaFile;
}
