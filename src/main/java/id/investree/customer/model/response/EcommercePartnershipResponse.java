package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import id.investree.core.model.response.DefaultResponse;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class EcommercePartnershipResponse {

	private Long id;
	private Long customerId;
	private String name;
	private String agreementFile;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date agreementExpired;
	private Boolean status;
	private List<DefaultResponse> rm;
	private String initial;
	private String logo;
	private String remarks;
	private RegisterStatusResponse registerStatusResponse;

}
