package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.customer.model.data.CustomerNotesData;
import lombok.Data;

import java.util.List;

@Data
public class FinancialInfoInstResponse {
	@JsonProperty("financialStatements")
	private List<Long> financialStatements;

	@JsonProperty("eStatements")
	private List<Long> eStatements;

	@JsonProperty("financialStatement")
	private List<Long> financialStatement;

	@JsonProperty("financialTrend")
	private List<Long> financialTrend;

	@JsonProperty("balanceSheet")
	private List<Long> balanceSheet;

	@JsonProperty("financialRatio")
	private List<Long> financialRatio;

	@JsonProperty("salesTransactionData")
	private List<Long> salesTransactionData;

	@JsonProperty("customerNotes")
	private CustomerNotesData customerNotes;
}
