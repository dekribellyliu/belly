package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.customer.model.data.CustomerNotesData;
import id.investree.customer.model.data.FinancialDataDetail;
import id.investree.customer.model.data.FinancialStatementData;
import id.investree.customer.model.data.FinancialTrendData;
import id.investree.customer.model.data.SalesTransactionData;
import lombok.Data;

import java.util.List;

@Data
public class FinancialInformationResponse {

    @JsonProperty("eStatementDocuments")
    private List<FinancialDataDetail> eStatementDocuments;

	@JsonProperty("financialStatementDocuments")
    private List<FinancialDataDetail> financialStatementDocuments;

	@JsonProperty("salesTransactionData")
    private List<SalesTransactionData> salesTransactionData;

	@JsonProperty("customerNotesData")
    private List<CustomerNotesData> customerNotesData;

	@JsonProperty("financialStatement")
    private List<FinancialStatementData> financialStatement;

	@JsonProperty("financialTrend")
    private List<FinancialTrendData> financialTrend;

	@JsonProperty("financialInformation")
    private BusinessProfileResponse financialInformation;
}
