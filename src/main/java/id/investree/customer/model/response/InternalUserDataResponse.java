package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.customer.model.data.BasicInformation;
import id.investree.customer.model.data.internal.InternalAdvancedInformation;
import lombok.Data;

@Data
public class InternalUserDataResponse {
	@JsonProperty("loginId")
	private Long loginId;

	@JsonProperty("basicInfo")
	private BasicInformation basicInfo;

	@JsonProperty("advancedInfo")
	private InternalAdvancedInformation advancedInfo;
}
