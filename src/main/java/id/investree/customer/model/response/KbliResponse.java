package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.customer.model.data.KbliData;
import lombok.Data;

import java.util.List;

@Data
public class KbliResponse {
	@JsonProperty("data")
	private List<KbliData> data;
}
