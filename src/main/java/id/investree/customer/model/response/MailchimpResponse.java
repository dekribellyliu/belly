package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class MailchimpResponse {

	// Success
	@JsonProperty("email")
	private String email;

	@JsonProperty("euid")
	private String euid;

	@JsonProperty("leid")
	private String leid;

	// Error
	@JsonProperty("status")
	private String status;

	@JsonProperty("code")
	private String code;

	@JsonProperty("name")
	private String name;

	@JsonProperty("error")
	private String error;

}
