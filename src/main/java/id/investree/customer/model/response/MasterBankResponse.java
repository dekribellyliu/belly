package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class MasterBankResponse {
	@JsonProperty("id")
	private Long id;

	@JsonProperty("name")
	private String name;
}
