package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class MasterInvarBankResponse {
	@JsonProperty("id")
	private Long id;

	@JsonProperty("bankId")
	private Long bankId;

	@JsonProperty("bankName")
	private String bankName;

	@JsonProperty("bankAccountName")
	private String bankAccountName;

	@JsonProperty("bankBin")
	private String bankBin;

	@JsonProperty("active")
	private String active;
}
