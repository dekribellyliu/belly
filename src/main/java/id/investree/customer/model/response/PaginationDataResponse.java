package id.investree.customer.model.response;

import lombok.Data;

import java.util.List;

@Data
public class PaginationDataResponse<Object> {
	private Long totalItem;
	private Long totalPages;
	private Long currentPage;
	private Long perPage;
	private List<Object> items;

}
