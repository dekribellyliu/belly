package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.customer.model.data.CounterData;
import id.investree.customer.model.data.PersonalProfileData;
import lombok.Data;


@Data
public class PersonalProfileResponse {
	@JsonProperty("personalProfile")
	private PersonalProfileData personalProfile;

	@JsonProperty("counter")
	private CounterData counter;
}
