package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.DefaultData;
import lombok.Data;

@Data
public class RegisterStatusResponse {
	@JsonProperty("borrower")
	private DefaultData borrower;

	@JsonProperty("lender")
	private DefaultData lender;

	@JsonProperty("anchor")
	private DefaultData anchor;

	@JsonProperty("partner")
	private DefaultData partner;
}
