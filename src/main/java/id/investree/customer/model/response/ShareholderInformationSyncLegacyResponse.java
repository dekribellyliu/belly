package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ShareholderInformationSyncLegacyResponse {
	@JsonProperty("shi_id")
	private Long shi_id;
}
