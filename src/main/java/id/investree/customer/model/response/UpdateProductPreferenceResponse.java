package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class UpdateProductPreferenceResponse {
	@JsonProperty("productPreference")
	private Integer productPreference;

	@JsonProperty("userCategory")
	private Integer userCategory;

	@JsonProperty("userPreferenceStatus")
	private boolean userPreferenceStatus;

	@JsonProperty("registrationStatus")
	private RegisterStatusResponse registrationStatus;
}
