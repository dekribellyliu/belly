package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.customer.model.data.AdvancedInformationBorrower;
import id.investree.customer.model.data.BasicInformationBorrower;
import lombok.Data;

@Data
public class UserDataBackofficeBorrowerResponse {
	@JsonProperty("basicInfo")
	private BasicInformationBorrower basicInfo;

	@JsonProperty("advancedInfo")
	private AdvancedInformationBorrower advancedInfo;
}

