package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.customer.model.data.AdvancedInformationLender;
import id.investree.customer.model.data.BasicInformationLender;
import lombok.Data;

@Data
public class UserDataBackofficeLenderResponse {
	@JsonProperty("basicInfo")
	private BasicInformationLender basicInfo;

	@JsonProperty("advancedInfo")
	private AdvancedInformationLender advancedInfo;
}

