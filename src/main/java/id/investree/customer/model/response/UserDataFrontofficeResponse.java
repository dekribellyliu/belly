package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.customer.model.data.AdvancedInformation;
import id.investree.customer.model.data.BasicInformationElementary;
import lombok.Data;

@Data
public class UserDataFrontofficeResponse {
	@JsonProperty("basicInfo")
	private BasicInformationElementary basicInfo;

	@JsonProperty("advancedInfo")
	private AdvancedInformation advancedInfo;
}

