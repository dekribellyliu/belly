package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.customer.model.data.AdvancedInformation;
import id.investree.customer.model.data.BasicInformation;
import lombok.Data;

@Data
public class UserDataResponse {
	@JsonProperty("basicInfo")
	private BasicInformation basicInfo;

	@JsonProperty("advancedInfo")
	private AdvancedInformation advancedInfo;
}
