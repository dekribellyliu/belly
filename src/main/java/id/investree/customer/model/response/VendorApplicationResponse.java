package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class VendorApplicationResponse {
	@JsonProperty("anchorId")
	private int anchorId;

	@JsonProperty("anchorRecipientId")
	private int anchorRecipientId;

	@JsonProperty("vendorId")
	private int vendorId;

	@JsonProperty("vendorPicRecipientId")
	private int vendorPicRecipientId;

	@JsonProperty("vendorShiRecipientId")
	private int vendorShiRecipientId;

	@JsonProperty("dsType")
	private int dsType;

	@JsonProperty("dsStatus")
	private String dsStatus;

	@JsonProperty("status")
	private String status;

	@JsonProperty("affiliate")
	private String affiliate;

	@JsonProperty("averageInvoice")
	private double averageInvoice;

	@JsonFormat(pattern = "yyyy-MM-dd")
	@JsonProperty("relationshipStart")
	private Date relationshipStart;

	@JsonProperty("documentNumber")
	private String documentNumber;

	@JsonProperty("dsFilename")
	private String dsFilename;

	@JsonProperty("docPartnerId")
	private String docPartnerId;

	@JsonProperty("sendDate")
	private Date sendDate;

	@JsonProperty("signedDate")
	private Date signedDate;

	@JsonProperty("sentBy")
	private int sentBy;

	@JsonProperty("updatedAt")
	private Date updatedAt;

	@JsonProperty("updatedBy")
	private int updatedBy;

	@JsonProperty("activeAt")
	private Date activeAt;

	@JsonProperty("activeBy")
	private int activeBy;

	@JsonProperty("inActiveAt")
	private Date inActiveAt;

	@JsonProperty("inActiveBy")
	private int inActiveBy;

	@JsonProperty("createdAt")
	private Date createdAt;

	@JsonProperty("createdBy")
	private int createdBy;
}
