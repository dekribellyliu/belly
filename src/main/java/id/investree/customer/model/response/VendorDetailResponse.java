package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.model.DefaultData;
import id.investree.customer.model.data.BankInformationParent;
import lombok.Data;

@Data
public class VendorDetailResponse {
	@JsonProperty("anchorId")
	private Long anchorId;

	@JsonProperty("legalEntity")
	private DefaultData legalEntity;

	@JsonProperty("companyName")
	private String companyName;

	@JsonProperty("contactName")
	private String contactName;

	@JsonProperty("email")
	private String email;

	@JsonProperty("mobilePrefix")
	private DefaultData mobilePrefix;

	@JsonProperty("mobileNumber")
	private String mobileNumber;

	@JsonProperty("city")
	private DefaultData city;

	@JsonProperty("poaFile")
	private String poaFile;

	@JsonProperty("bankInformation")
	private BankInformationParent bankInformation;
}
