package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.customer.model.data.BankInformationData;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class VendorResponse {
	@JsonProperty("anchorName")
	private String anchorName;

	@JsonProperty("vendorName")
	private String vendorName;

	@JsonProperty("vendorCity")
	private String vendorCity;

	@JsonProperty("picName")
	private String picName;

	@JsonProperty("picEmail")
	private String picEmail;

	@JsonProperty("virtualAccounts")
	private List<BankInformationData> virtualAccounts;

	@JsonProperty("digitalSignatureStatus")
	private String digitalSignatureStatus;

	@JsonProperty("agreementTemplate")
	private String agreementTemplate;

	@JsonProperty("digitalSignatureFile")
	private String digitalSignatureFile;

	@JsonProperty("relationshipStart")
	private Date relationshipStart;

	@JsonProperty("affiliate")
	private String affiliate;

	@JsonProperty("averageInvoice")
	private double averageInvoice;

	@JsonProperty("status")
	private String status;
}

