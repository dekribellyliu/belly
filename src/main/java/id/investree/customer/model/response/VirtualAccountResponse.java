package id.investree.customer.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class VirtualAccountResponse {
	@JsonProperty("status")
	private String status;

	@JsonProperty("binNumber")
	private String binNumber;

	@JsonProperty("virtualNumber")
	private String virtualNumber;
}
