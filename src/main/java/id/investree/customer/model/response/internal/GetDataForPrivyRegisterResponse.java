package id.investree.customer.model.response.internal;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.customer.entity.LegalInformation;
import id.investree.customer.entity.UserData;
import lombok.Data;

@Data
public class GetDataForPrivyRegisterResponse {
	@JsonProperty("legalInformation")
	private LegalInformation legalInformation;

	@JsonProperty("userData")
	private UserData userData;
}
