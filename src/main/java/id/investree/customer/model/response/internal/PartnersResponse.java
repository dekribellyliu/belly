package id.investree.customer.model.response.internal;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.model.DefaultData;
import id.investree.core.utils.DateUtils;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.CustomerInformation;
import id.investree.customer.entity.UserData;
import id.investree.customer.model.data.BankInformationData;
import id.investree.customer.model.data.BusinessInfoData;
import id.investree.customer.model.data.LegalInformationData;
import id.investree.customer.model.data.PartnershipInformationData;
import id.investree.customer.model.data.PersonalProfileData;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.StoreDataGlobalParam;
import lombok.Data;

import java.util.List;
import java.util.Optional;

@Data
public class PartnersResponse {
	@JsonProperty("customer_id")
	private Long customerId;

	@JsonProperty("login_id")
	private Long loginId;

	@JsonProperty("borrower_number")
	private String borrowerNumber;

	@JsonProperty("birth_place")
	private String birthPlace;

	@JsonProperty("birth_date")
	private String birthDate;
	private String title;

	@JsonProperty("loan_type")
	private String loanType;

	@JsonProperty("company_name")
	private String companyName;

	@JsonProperty("company_type")
	private String companyType;

	@JsonProperty("business_category")
	private String businessCategory;

	@JsonProperty("business_type")
	private String businessType;

	@JsonProperty("company_desc")
	private String companyDesc;

	@JsonProperty("employee_number")
	private String employeeNumber;

	@JsonProperty("year_of_estabilishment")
	private String yearOfEstabilishment;

	@JsonProperty("company_address")
	private String companyAddress;

	@JsonProperty("company_province")
	private String companyProvince;

	@JsonProperty("company_city")
	private String companyCity;

	@JsonProperty("company_district")
	private String companyDistrict;

	@JsonProperty("company_sub_district")
	private String companySubDistrict;

	@JsonProperty("company_postal_code")
	private String companyPostalCode;

	@JsonProperty("company_phone_number")
	private String companyPhoneNumber;

	@JsonProperty("fax_number")
	private String faxNumber;

	@JsonProperty("siup_number")
	private String siupNumber;

	@JsonProperty("company_bank")
	private String companyBank;

	@JsonProperty("company_account_number")
	private String companyAccountNumber;

	@JsonProperty("company_account_name")
	private String companyAccountName;

	@JsonProperty("customer_role_status")
	private Long crStatus;

	@JsonProperty("legalInfoData")
	private List<LegalInformationData> legalInfoDatas;

	@JsonProperty("partnerInfoData")
	private List<PartnershipInformationData> partnershipInformationDatas;

	public static class PartnersResponseBuilder {

		private String borrowerNumber;
		private Long customerId;
		private Long crStatus;
		private Integer productPreference;
		private UserData userData;
		private CustomerInformation customerInformation;
		private List<PartnershipInformationData> partnershipInformations;
		private PersonalProfileData personalProfileData;
		private BusinessInfoData businessInfoData;
		private BankInformationData bankInformationData;
		private List<LegalInformationData> legalInformationDataList;
		private StoreDataGlobalParam dataGlobalParam;

		public PartnersResponseBuilder setBorrowerNumber(String borrowerNumber) {
			this.borrowerNumber = borrowerNumber;
			return this;
		}

		public PartnersResponseBuilder setCustomerId(Long customerId) {
			this.customerId = customerId;
			return this;
		}

		public PartnersResponseBuilder setCrStatus(Long crStatus) {
			this.crStatus = crStatus;
			return this;
		}

		public PartnersResponseBuilder setProductPreference(Integer productPreference) {
			this.productPreference = productPreference;
			return this;
		}

		public PartnersResponseBuilder setCustomerInformation(CustomerInformation customerInformation) {
			this.customerInformation = customerInformation;
			return this;
		}

		public PartnersResponseBuilder setUserData(UserData userData, StoreDataGlobalParam dataGlobalParam) {
			this.userData = userData;
			this.dataGlobalParam = dataGlobalParam;
			return this;
		}

		public PartnersResponseBuilder setPersonalProfileData(PersonalProfileData personalProfileData) {
			this.personalProfileData = personalProfileData;
			return this;
		}

		public PartnersResponseBuilder setBusinessInfoData(BusinessInfoData businessInfoData) {
			this.businessInfoData = businessInfoData;
			return this;
		}

		public PartnersResponseBuilder setBankInformationData(BankInformationData bankInformationData) {
			this.bankInformationData = bankInformationData;
			return this;
		}

		public PartnersResponseBuilder setLegalInformationDataList(List<LegalInformationData> legalInformationDataList) {
			this.legalInformationDataList = legalInformationDataList;
			return this;
		}

		public PartnersResponseBuilder setPartnershipInformation(List<PartnershipInformationData> partnershipInformations) {
			this.partnershipInformations = partnershipInformations;
			return this;
		}

		public PartnersResponse build() {

			PartnersResponse partnersServiceResponse = new PartnersResponse();
			Optional.ofNullable(borrowerNumber).ifPresent(partnersServiceResponse::setBorrowerNumber);
			Optional.ofNullable(customerId).ifPresent(partnersServiceResponse::setCustomerId);
			Optional.ofNullable(personalProfileData).ifPresent(personalProfile -> {
				Optional.ofNullable(personalProfile.getDateOfBirth()).ifPresent(partnersServiceResponse::setBirthDate);
				Optional.ofNullable(personalProfile.getPlaceOfBirthExternal()).ifPresent(partnersServiceResponse::setBirthPlace);
			});
			Optional.ofNullable(userData).ifPresent(user -> {
				Optional.ofNullable(userData.getLoginDataId()).ifPresent(partnersServiceResponse::setLoginId);
				Optional.ofNullable(user.getCompanyPosition()).ifPresent(val -> {
					String position = dataGlobalParam.getPositionList().stream()
							.filter(v -> v.getId().equals(Math.toIntExact(user.getCompanyPosition())))
							.findFirst().map(DefaultData::getName).orElse(null);
					partnersServiceResponse.setTitle(position);
				});
			});

			Optional.ofNullable(productPreference).ifPresent(product -> {
				if (GlobalConstants.USER_PREFERENCE_SHARIA.equals(product)) {
					partnersServiceResponse.setLoanType(Constants.USER_PREFERENCE_SHARIA_NAME);

				} else if (GlobalConstants.USER_PREFERENCE_CONVENTIONAL.equals(product)) {
					partnersServiceResponse.setLoanType(Constants.USER_PREFERENCE_CONVENTIONAL_NAME);

				} else if (GlobalConstants.USER_PREFERENCE_ALL.equals(product)) {
					partnersServiceResponse.setLoanType(Constants.USER_PREFERENCE_ALL_NAME);
				}
			});

			Optional.ofNullable(businessInfoData).ifPresent(businessInfo -> {
				Optional.ofNullable(businessInfo.getCompanyName()).ifPresent(partnersServiceResponse::setCompanyName);partnersServiceResponse.setCompanyName(businessInfo.getCompanyName());
				Optional.ofNullable(businessInfo.getCompanyDescription()).ifPresent(partnersServiceResponse::setCompanyDesc);
				Optional.ofNullable(businessInfo.getNumberOfEmployee()).ifPresent(v -> partnersServiceResponse.setEmployeeNumber(String.valueOf(v)));
				Optional.ofNullable(businessInfo.getDateOfEstablishment()).ifPresent(v -> partnersServiceResponse.setYearOfEstabilishment(DateUtils.convertToString(v, "yyyy")));
				Optional.ofNullable(businessInfo.getCompanyAddress()).ifPresent(partnersServiceResponse::setCompanyAddress);
				Optional.ofNullable(businessInfo.getPostalCode()).ifPresent(partnersServiceResponse::setCompanyPostalCode);
				Optional.ofNullable(businessInfo.getLandLineNumber()).ifPresent(partnersServiceResponse::setCompanyPhoneNumber);
				partnersServiceResponse.setCompanyType(Utilities.isNull(businessInfo.getLegalEntity()) ? null : businessInfo.getLegalEntity().getName());
				partnersServiceResponse.setBusinessCategory(Utilities.isNull(businessInfo.getIndustry()) ? null : businessInfo.getIndustry().getName());
				partnersServiceResponse.setCompanyProvince(Utilities.isNull(businessInfo.getProvince()) ? null : businessInfo.getProvince().getName());
				partnersServiceResponse.setCompanyCity(Utilities.isNull(businessInfo.getCity()) ? null : businessInfo.getCity().getName());
				partnersServiceResponse.setCompanyDistrict(Utilities.isNull(businessInfo.getDistrict()) ? null : businessInfo.getDistrict().getName());
				partnersServiceResponse.setCompanySubDistrict(Utilities.isNull(businessInfo.getVillage()) ? null : businessInfo.getVillage().getName());
			});

			Optional.ofNullable(bankInformationData).ifPresent(bankInformation -> {
				Optional.ofNullable(bankInformation.getBankAccountNumber()).ifPresent(partnersServiceResponse::setCompanyAccountNumber);
				Optional.ofNullable(bankInformation.getBankAccountHolderName()).ifPresent(partnersServiceResponse::setCompanyAccountName);
				partnersServiceResponse.setCompanyBank(Utilities.isNull(bankInformation.getMasterBank()) ? null : bankInformation.getMasterBank().getName());
			});

			Optional.ofNullable(legalInformationDataList).ifPresent(partnersServiceResponse::setLegalInfoDatas);

			Optional.ofNullable(partnershipInformations).ifPresent(v -> {
				partnersServiceResponse.setCustomerId(customerId);
				partnersServiceResponse.setBorrowerNumber(borrowerNumber);
				partnersServiceResponse.setCompanyName(customerInformation.getName());
				partnersServiceResponse.setPartnershipInformationDatas(partnershipInformations);
			});

			Optional.ofNullable(crStatus).ifPresent(v -> {
				partnersServiceResponse.setCrStatus(v);
			});

			return partnersServiceResponse;
		}
	}
}
