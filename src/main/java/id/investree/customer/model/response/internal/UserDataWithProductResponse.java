package id.investree.customer.model.response.internal;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.investree.customer.entity.CustomerProductList;
import id.investree.customer.entity.UserData;
import lombok.Data;

import java.util.List;

@Data
public class UserDataWithProductResponse {
	@JsonProperty("userData")
	private UserData userData;

	@JsonProperty("products")
	private List<CustomerProductList> products;
}
