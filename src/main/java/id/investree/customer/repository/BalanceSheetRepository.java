package id.investree.customer.repository;

import id.investree.customer.entity.BalanceSheet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BalanceSheetRepository extends JpaRepository<BalanceSheet, Long> {

	Optional<BalanceSheet> findByIdAndCustomerId(Long id, Long customerId);

	Optional<List<BalanceSheet>> findByCustomerId(Long customerId);
}
