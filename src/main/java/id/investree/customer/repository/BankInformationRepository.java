package id.investree.customer.repository;

import id.investree.customer.entity.BankInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BankInformationRepository extends JpaRepository<BankInformation, Long> {

	Optional<List<BankInformation>> findByCustomerIdAndDeletedByIsNull(Long customerId);

	Optional<BankInformation> findByCustomerIdAndIdAndDeletedByIsNull(Long customerId, Long id);

	Optional<BankInformation> findByIdAndCustomerId(Long id, Long customerId);

	Optional<List<BankInformation>> findByCustomerIdAndIsAnchorDisbursementAndDeletedByIsNull(Long customerId, String isAnchorDisbursement);

	Optional<BankInformation> findByCustomerIdAndUseAsDisbursementAndDeletedByIsNull(Long customerId, String isUseAsDisbursement);

	Optional<List<BankInformation>> findByCustomerIdAndCustomerRoleIdAndDeletedByIsNull(Long customerId, Long customerRoleId);

	Optional<List<BankInformation>> findByCustomerIdAndBankTypeAndDeletedAtIsNull(Long id, Integer type);

	Optional<List<BankInformation>> findAllByCustomerId(Long customerId);

	Optional<BankInformation> findByCustomerIdAndBankTypeAndMasterBankIdAndDeletedAtIsNull(Long customerId, Integer type, Long masterBankId);
}
