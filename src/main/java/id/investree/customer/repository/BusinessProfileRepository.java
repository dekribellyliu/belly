package id.investree.customer.repository;

import id.investree.customer.entity.BusinessProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BusinessProfileRepository extends JpaRepository<BusinessProfile, Long> {

	Optional<BusinessProfile> findByCustomerId(Long customerId);
	
	void deleteByCustomerId(Long customerId);

}
