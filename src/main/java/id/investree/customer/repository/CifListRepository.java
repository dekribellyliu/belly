package id.investree.customer.repository;

import id.investree.customer.entity.CifList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CifListRepository extends JpaRepository<CifList, Long> {
	Optional<CifList> findByCustomerId(Long customerId);

	@Query(
		value = "insert into cif_list (cl_id, cl_ci_id, cl_created_by, cl_created_at) values (:#{#cif.id}, :#{#cif.customerId}, :#{#cif.createdBy}, :#{#cif.createdAt})",
		nativeQuery = true
	)
	void saveByQuery(@Param("cif") CifList save);
}
