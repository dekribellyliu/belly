package id.investree.customer.repository;

import id.investree.customer.entity.CustomerInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerInformationRepository extends JpaRepository<CustomerInformation, Long> {

	Optional<CustomerInformation> findByinitial(String initial);

}
