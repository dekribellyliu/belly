package id.investree.customer.repository;

import id.investree.customer.entity.CustomerNotes;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CustomerNotesRepository extends JpaRepository<CustomerNotes, Long> {

	Optional<List<CustomerNotes>>  findByCustomerId(Long customerId);

	Optional<CustomerNotes> findByCustomerIdAndCreatedBy(Long customerId, Long createdBy);
}
