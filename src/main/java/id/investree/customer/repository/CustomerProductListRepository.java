package id.investree.customer.repository;

import id.investree.customer.entity.CustomerProductList;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CustomerProductListRepository extends JpaRepository<CustomerProductList, Long> {

		Optional<List<CustomerProductList>> findByCustomerRoleIdInAndDeletedAtIsNull(Long... customerRoleId);

}
