package id.investree.customer.repository;

import id.investree.customer.entity.CustomerRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface CustomerRoleRepository extends JpaRepository<CustomerRole, Long> {

	Optional<List<CustomerRole>> findByCustomerInformationId(Long customerId);

	Optional<List<CustomerRole>> findByCustomerInformationIdAndTypeIn(Long customerId, Integer... type);

	Optional<CustomerRole> findByCustomerInformationIdAndType(Long customerId, Integer type);

	@Query("SELECT COUNT(*) FROM CustomerRole WHERE activeAt IS NOT NULL")
	Long countActiveAtIsNotNull();

	Optional<CustomerRole> findByCustomerNumber(String crNumber);

	@Transactional
	void deleteByCustomerInformationId(Long customerId);
}
