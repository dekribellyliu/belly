package id.investree.customer.repository;

import id.investree.customer.entity.EmergencyContact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmergencyContactRepository extends JpaRepository<EmergencyContact, Long> {

	Optional<List<EmergencyContact>> findByCustomerIdAndDeletedAtIsNull(Long customerId);

	Optional<EmergencyContact> findByIdentityCardNumberAndCustomerIdAndDeletedAtIsNull(String identityCardNumber, Long customerId);

	Optional<EmergencyContact> findByIdAndCustomerIdAndDeletedAtIsNull(Long id, Long customerId);
}
