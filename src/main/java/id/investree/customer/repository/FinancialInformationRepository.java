package id.investree.customer.repository;

import id.investree.customer.entity.FinancialInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FinancialInformationRepository extends JpaRepository<FinancialInformation, Long> {

	Optional<List<FinancialInformation>> findByCustomerIdAndDeletedAtIsNull(Long customerId);

	Optional<List<FinancialInformation>> findByCustomerIdAndStatementFileTypeAndDeletedAtIsNull(Long customerId, Integer statementFileType);

	Optional<FinancialInformation> findByIdAndCustomerIdAndDeletedAtIsNull(Long id, Long customerId);

	Optional<FinancialInformation> findByIdAndDeletedAtIsNull(Long id);

	Optional<FinancialInformation> findFirstByCustomerIdAndStatementFileTypeAndDeletedAtIsNullOrderByStatementFileDateDescCreatedAtDescIdDesc(Long customerId, Integer statementFileType);

	Optional<FinancialInformation> findFirstByCustomerIdAndStatementFileTypeAndDeletedAtIsNullOrderByStatementFileDateDescUpdatedAtDescCreatedAtDescIdDesc(Long customerId, Integer statementFileType);
}
