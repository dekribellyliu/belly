package id.investree.customer.repository;

import id.investree.customer.entity.FinancialRatio;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface FinancialRatioRepository extends JpaRepository<FinancialRatio, Long> {

	Optional<FinancialRatio> findByIdAndCustomerId(Long id, Long customerId);

	Optional<List<FinancialRatio>> findByCustomerId(Long customerId);
}
