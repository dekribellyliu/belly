package id.investree.customer.repository;

import id.investree.customer.entity.FinancialStatement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface FinancialStatementRepository extends JpaRepository<FinancialStatement, Long> {

	Optional<List<FinancialStatement>> findByCustomerId(Long customerId);

	Optional<FinancialStatement> findByCustomerIdAndYearTo(Long customerId, Long yearTo);

	Optional<FinancialStatement> findByIdAndCustomerId(Long id, Long customerId);
}
