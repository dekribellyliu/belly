package id.investree.customer.repository;

import id.investree.customer.entity.FinancialTrend;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface FinancialTrendRepository extends JpaRepository<FinancialTrend, Long> {

	Optional<FinancialTrend> findByIdAndCustomerId(Long id, Long customerId);

	Optional<List<FinancialTrend>> findByCustomerId(Long customerId);

	Optional<FinancialTrend> findByCustomerIdAndTrendPeriod(Long customerId, String yearPeriod);
}
