package id.investree.customer.repository;

import id.investree.customer.entity.LegalInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LegalInformationRepository extends JpaRepository<LegalInformation, Long> {

	Optional<List<LegalInformation>> findByCustomerId(Long customerId);

	Optional<LegalInformation> findByCustomerIdAndDocumentType(Long customerId, Integer documentType);

	Optional<List<LegalInformation>> findByDocumentTypeAndDocumentNumber(Integer documentType, String documentNumber);

}
