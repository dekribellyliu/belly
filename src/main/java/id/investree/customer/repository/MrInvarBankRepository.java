package id.investree.customer.repository;

import id.investree.customer.entity.MrInvarBank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MrInvarBankRepository extends JpaRepository<MrInvarBank, Long> {

	Optional<List<MrInvarBank>> findByIdIn(Long... type);

	Optional<MrInvarBank> findByBankIdAndTypeIn(Long bankId, Long... type);
}
