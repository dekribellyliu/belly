package id.investree.customer.repository;

import id.investree.customer.entity.PartnershipInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PartnershipInfromationRepository extends JpaRepository<PartnershipInformation, Long> {
	Optional<PartnershipInformation> findByCustomerId(Long customerId);

	Optional<List<PartnershipInformation>> findAllByCustomerId(Long customerId);

	Optional<PartnershipInformation> findByIdAndCustomerId(Long id, Long customerId);

	Optional<PartnershipInformation> findByCustomerIdAndPartnerId(Long customerId, Long partnerId);

}
