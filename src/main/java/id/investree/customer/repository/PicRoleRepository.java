package id.investree.customer.repository;

import id.investree.customer.entity.PicRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PicRoleRepository extends JpaRepository<PicRole, Long> {

	Optional<List<PicRole>> findByCustomerRoleIdInAndLoginIdAndRoleIsNotNullAndDeletedAtIsNull(List<Long> customerRoleId, Long loginId);

	Optional<List<PicRole>> findByCustomerRoleIdInAndLoginIdAndRoleIsNullAndDeletedAtIsNull(List<Long> customerRoleId, Long loginId);

	Optional<List<PicRole>> findByCustomerRoleIdAndLoginId(Long customerRoleId, Long loginId);

	Optional<List<PicRole>> findByCustomerRoleId(Long customerLongId);
	
	void deleteByLoginIdAndCustomerRoleId(Long LoginId, Long customerRoleId);
}
