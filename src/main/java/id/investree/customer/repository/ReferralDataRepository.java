package id.investree.customer.repository;

import id.investree.customer.entity.ReferralData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ReferralDataRepository extends JpaRepository<ReferralData, Long> {

	Optional<ReferralData> findByReferralUserId(Long referralUserId);

	Optional<ReferralData> findByReferrerCodeAndReferralUserIdIsNull(String referrerCode);
}
