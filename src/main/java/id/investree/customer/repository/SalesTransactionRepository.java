package id.investree.customer.repository;

import id.investree.customer.entity.SalesTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface SalesTransactionRepository extends JpaRepository<SalesTransaction, Long> {

	Optional<List<SalesTransaction>> findByCustomerIdAndDeletedAtIsNull(Long customerId);

	Optional<SalesTransaction> findByCustomerIdAndDateAndDeletedAtIsNull(Long customerId, Date date);

	Optional<SalesTransaction> findByIdAndCustomerIdAndDeletedAtIsNull(Long id, Long customerId);

	Optional<SalesTransaction> findByIdAndDeletedAtIsNull(Long id);

	@Query("SELECT COUNT(*) FROM SalesTransaction st WHERE st.customerId = ?1 and st.deletedAt = null")
	Long countCustomerIdAndDeletedAtIsNull(Long customerId);
}
