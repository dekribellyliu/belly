package id.investree.customer.repository;

import id.investree.customer.entity.ShareholdersInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface ShareholdersInformationRepository extends JpaRepository<ShareholdersInformation, Long> {

	Optional<List<ShareholdersInformation>> findByCustomerIdAndDeletedAtIsNull(Long customerId);

	Optional<ShareholdersInformation> findByIdAndDeletedAtIsNull(Long id);

	Optional<ShareholdersInformation> findByIdAndCustomerIdAndDeletedAtIsNull(Long id, Long customerId);

	Optional<List<ShareholdersInformation>> findByTaxCardNumberAndDeletedAtIsNull(String taxNumber);

	Optional<ArrayList<ShareholdersInformation>> findByCustomerIdAndDeletedAtIsNotNull(Long customerId);

	Optional<List<ShareholdersInformation>> findAllByCustomerId(Long customerId);
}
