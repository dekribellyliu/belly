package id.investree.customer.repository;

import id.investree.customer.entity.SurveyInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SurveyInformationRepository extends JpaRepository<SurveyInformation, Long> {

	Optional<SurveyInformation> findByIdAndDeletedAtIsNull(Long id);

	Optional<List<SurveyInformation>> findByCustomerIdAndDeletedAtIsNull(Long customerId);

	Optional<SurveyInformation> findByIdAndCustomerIdAndDeletedAtIsNull(Long id, Long customerId);

	Optional<SurveyInformation> findFirstByCustomerIdAndDeletedAtIsNullOrderBySurveyDateDescUpdateAtDescIdDesc(Long customerId);
}
