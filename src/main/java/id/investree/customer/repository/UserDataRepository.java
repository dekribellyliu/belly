package id.investree.customer.repository;

import id.investree.customer.entity.UserData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserDataRepository extends JpaRepository<UserData, Long> {

	Optional<UserData> findByLoginDataId(Long loginDataId);

	Optional<List<UserData>> findByCustomerId(Long customerId);

	Optional<UserData> findByIdCardNumber(String idCardNumber);

	@Transactional
	void deleteByCustomerId(Long customerId);

}
