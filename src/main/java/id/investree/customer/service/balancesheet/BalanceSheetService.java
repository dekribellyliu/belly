package id.investree.customer.service.balancesheet;

import id.investree.customer.entity.BalanceSheet;

import java.util.List;

public interface BalanceSheetService {

	List<BalanceSheet> findByCustomerId(Long customerId);

	BalanceSheet findByIdAndCustomerId(Long id, Long customerId);

	BalanceSheet saveOrUpdate(BalanceSheet data);
}
