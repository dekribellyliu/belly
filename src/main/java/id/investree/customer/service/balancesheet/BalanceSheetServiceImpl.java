package id.investree.customer.service.balancesheet;

import id.investree.customer.entity.BalanceSheet;
import id.investree.customer.repository.BalanceSheetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class BalanceSheetServiceImpl implements BalanceSheetService {

	@Autowired
	private BalanceSheetRepository repository;

	@Override
	public List<BalanceSheet> findByCustomerId(Long customerId) {
		return repository.findByCustomerId(customerId).orElse(new ArrayList<>());
	}

	@Override
	public BalanceSheet findByIdAndCustomerId(Long id, Long customerId) {
		return repository.findByIdAndCustomerId(id, customerId).orElse(null);
	}

	@Override
	@Transactional
	public BalanceSheet saveOrUpdate(BalanceSheet data) {
		return repository.saveAndFlush(data);
	}
}
