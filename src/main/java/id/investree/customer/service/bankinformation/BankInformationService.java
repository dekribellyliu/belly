package id.investree.customer.service.bankinformation;

import id.investree.customer.entity.BankInformation;

import java.util.List;

public interface BankInformationService {

	BankInformation saveOrUpdate(BankInformation request);

	BankInformation getBankInformationById(Long id, Long customerId);

	BankInformation findById(Long id);

	List<BankInformation> findAllByCustomerId(Long customerId);

	List<BankInformation> findByCustomerIdAndType(Long id, Integer type);

	BankInformation findByCustomerIdAndBankId(Long customerId, Long bankId);

	List<BankInformation> findByCustomerId(Long customerId);

	List<BankInformation> findByCustomerIdAndIsAnchorDisbursement(Long customerId);

	BankInformation findByCustomerIdAndIsUseAsDisbursement(Long customerId);

	List<BankInformation> findByCustomerIdAndCustomerRoleId(Long customerId, Long customerRoleId);

	BankInformation findByCustomerIdAndTypeAndMasterBankId(Long customerId, Integer type, Long masterBankId);
}
