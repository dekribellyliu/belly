package id.investree.customer.service.bankinformation;

import id.investree.core.constant.GlobalConstants;
import id.investree.customer.entity.BankInformation;
import id.investree.customer.repository.BankInformationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Component
public class BankInformationServiceImpl implements BankInformationService {

	@Autowired
	private BankInformationRepository bankInformationRepository;

	@Override
	@Transactional
	public BankInformation saveOrUpdate(BankInformation request) {
		return bankInformationRepository.saveAndFlush(request);
	}

	@Override
	public BankInformation getBankInformationById(Long id, Long customerId) {
		return bankInformationRepository.findByIdAndCustomerId(id, customerId).orElse(null);
	}

	@Override
	public BankInformation findById(Long id) {
		return bankInformationRepository.findById(id).orElse(null);
	}

	@Override
	public List<BankInformation> findAllByCustomerId(Long customerId) {
		return bankInformationRepository.findAllByCustomerId(customerId).orElse(new ArrayList<>());
	}

	@Override
	public List<BankInformation> findByCustomerIdAndType(Long id, Integer type) {
		return bankInformationRepository.findByCustomerIdAndBankTypeAndDeletedAtIsNull(id, type).orElse(null);
	}

	@Override
	public List<BankInformation> findByCustomerId(Long customerId) {
		return bankInformationRepository.findByCustomerIdAndDeletedByIsNull(customerId).orElse(new ArrayList<>());
	}

	@Override
	public BankInformation findByCustomerIdAndBankId(Long customerId, Long bankId) {
		return bankInformationRepository.findByCustomerIdAndIdAndDeletedByIsNull(customerId, bankId).orElse(null);

	}

	@Override
	public List<BankInformation> findByCustomerIdAndIsAnchorDisbursement(Long customerId) {
		return bankInformationRepository.findByCustomerIdAndIsAnchorDisbursementAndDeletedByIsNull(customerId, GlobalConstants.STATUS_YES)
			.orElse(new ArrayList<>());
	}

	@Override
	public BankInformation findByCustomerIdAndIsUseAsDisbursement(Long customerId) {
		return bankInformationRepository.findByCustomerIdAndUseAsDisbursementAndDeletedByIsNull(customerId, GlobalConstants.STATUS_YES)
			.orElse(null);
	}

	@Override
	public List<BankInformation> findByCustomerIdAndCustomerRoleId(Long customerId, Long customerRoleId) {
		return bankInformationRepository.findByCustomerIdAndCustomerRoleIdAndDeletedByIsNull(customerId, customerRoleId)
			.orElse(null);
	}

	@Override
	public BankInformation findByCustomerIdAndTypeAndMasterBankId(Long customerId, Integer type, Long masterBankId) {
		return bankInformationRepository.findByCustomerIdAndBankTypeAndMasterBankIdAndDeletedAtIsNull(customerId, type, masterBankId).orElse(null);
	}
}
