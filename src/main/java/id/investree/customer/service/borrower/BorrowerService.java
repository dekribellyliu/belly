package id.investree.customer.service.borrower;

import id.investree.customer.model.response.LoginDataResponse;

public interface BorrowerService {

	LoginDataResponse updateUserNumber(Long loginId, LoginDataResponse loginData, String accessToken);
}
