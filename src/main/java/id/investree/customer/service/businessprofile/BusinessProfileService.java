package id.investree.customer.service.businessprofile;

import id.investree.customer.entity.BusinessProfile;
import id.investree.customer.model.data.ApuPptCheckData;
import id.investree.customer.model.request.ApuPptCheckRequest;

public interface BusinessProfileService {

	BusinessProfile saveOrUpdate(BusinessProfile request);

	void deleteByCustomerId(Long id);

	BusinessProfile findByCustomerId(Long id);

	ApuPptCheckData checkByCustomerId(Long customerId, String accessToken);

	ApuPptCheckData saveBackgroundCheck(ApuPptCheckRequest request, String accessToken);

}
