package id.investree.customer.service.businessprofile;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.investree.core.base.ResultResponse;
import id.investree.core.exception.AppException;
import id.investree.core.json.JsonConverter;
import id.investree.core.rest.GenericRestClient;
import id.investree.core.rest.RequestDetails;
import id.investree.customer.entity.BusinessProfile;
import id.investree.customer.model.data.ApuPptCheckData;
import id.investree.customer.model.request.ApuPptCheckRequest;
import id.investree.customer.repository.BusinessProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

@Service
public class BusinessProfileServiceImpl implements BusinessProfileService {

	@Value("${microservice.background-check}")
	private String backgroundCheckUrl;

	@Autowired
	private BusinessProfileRepository businessProfileRepository;

	@Autowired
	private GenericRestClient genericRestClient;

	@Autowired
	private JsonConverter jsonConverter;

	@Override
	@Transactional
	public BusinessProfile saveOrUpdate(BusinessProfile request) {
		return businessProfileRepository.saveAndFlush(request);
	}

	@Override
	public void deleteByCustomerId(Long id) {
		businessProfileRepository.deleteByCustomerId(id);
	}

	@Override
	public BusinessProfile findByCustomerId(Long id) {
		return businessProfileRepository.findByCustomerId(id).orElse(new BusinessProfile());
	}

	@Override
	public ApuPptCheckData checkByCustomerId(Long customerId, String accessToken) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Investree-Token", accessToken);

		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(backgroundCheckUrl + "/internal/apu-checking?customerId=" + customerId);
		requestDetails.setRequestType(HttpMethod.GET);
		requestDetails.setHeaders(headers);

		ResultResponse result = genericRestClient.execute(requestDetails, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					throw new AppException(response.meta.message);
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.convertValue(result.data, new TypeReference<ApuPptCheckData>() {
		});
	}

	@Override
	public ApuPptCheckData saveBackgroundCheck(ApuPptCheckRequest apuPptCheckRequest, String accessToken) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Investree-Token", accessToken);

		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(backgroundCheckUrl + "/internal/apu-checking");
		requestDetails.setRequestType(HttpMethod.POST);
		requestDetails.setHeaders(headers);

		ResultResponse result = genericRestClient.execute(requestDetails, apuPptCheckRequest, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					throw new AppException(response.meta.message);
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.convertValue(result.data, new TypeReference<ApuPptCheckData>() {
		});
	}
}
