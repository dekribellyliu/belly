package id.investree.customer.service.ciflist;

import id.investree.customer.entity.CifList;

public interface CifListService {

	CifList findById(Long id);

	CifList findByCustomerId(Long customerId);

	CifList save(CifList data);

	CifList saveByQuery(CifList data);
}
