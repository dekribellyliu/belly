package id.investree.customer.service.ciflist;

import id.investree.customer.entity.CifList;
import id.investree.customer.repository.CifListRepository;
import io.sentry.Sentry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CifListServiceImpl implements CifListService {

	@Autowired
	private CifListRepository repository;

	@Override
	public CifList findById(Long id) {
		return repository.findById(id).orElse(null);
	}

	@Override
	public CifList findByCustomerId(Long customerId) {
		return repository.findByCustomerId(customerId).orElse(null);
	}

	@Override
	@Transactional
	public CifList save(CifList data) {
		return repository.saveAndFlush(data);
	}

	@Override
	public CifList saveByQuery(CifList data) {
		try {
			repository.saveByQuery(data);
			repository.flush();
			return data;
		} catch (Exception ex) {
			Sentry.capture(ex);
			return null;
		}
	}
}
