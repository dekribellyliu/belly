package id.investree.customer.service.customerinfo;

import id.investree.customer.entity.CustomerInformation;

public interface CustomerInformationService {

	CustomerInformation saveOrUpdate(CustomerInformation data);

	CustomerInformation findById(Long id);

	CustomerInformation findByInitial(String initial);

	void hardDelete(Long id);
}
