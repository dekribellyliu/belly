package id.investree.customer.service.customerinfo;

import id.investree.customer.entity.CustomerInformation;
import id.investree.customer.repository.CustomerInformationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomerInformationServiceImpl implements CustomerInformationService {

	@Autowired
	private CustomerInformationRepository customerInformationRepository;

	@Override
	@Transactional
	public CustomerInformation saveOrUpdate(CustomerInformation data) {
		return customerInformationRepository.save(data);
	}

	@Override
	public CustomerInformation findById(Long id) {
		return customerInformationRepository.findById(id).orElse(null);
	}

	@Override
	public CustomerInformation findByInitial(String initial) {
		return customerInformationRepository.findByinitial(initial).orElse(null);
	}

	@Override
	public void hardDelete(Long id) {
		customerInformationRepository.deleteById(id);
	}
}
