package id.investree.customer.service.customernotes;

import id.investree.customer.entity.CustomerNotes;

import java.util.List;

public interface CustomerNotesService {

    CustomerNotes findById(Long Id);

    CustomerNotes findByCustomerIdAndCreatedBy(Long customerId, Long createdBy);

    CustomerNotes internalFindByCustomerIdAndCreatedBy(Long customerId, Long createdBy, String accessToken);

    CustomerNotes saveOrUpdate(CustomerNotes customerNotesRequest);

    CustomerNotes internalSaveOrUpdate(CustomerNotes customerNotesRequest, String accessToken);

    List<CustomerNotes> findByCustomerId(Long customerId);

    List<CustomerNotes> internalFindByCustomerId(Long customerId, String accessToken);

    List<CustomerNotes> findAll();
}
