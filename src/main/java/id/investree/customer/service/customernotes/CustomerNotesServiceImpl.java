package id.investree.customer.service.customernotes;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.investree.core.base.ResultResponse;
import id.investree.core.exception.AppException;
import id.investree.core.json.JsonConverter;
import id.investree.core.rest.GenericRestClient;
import id.investree.core.rest.RequestDetails;
import id.investree.customer.entity.CustomerNotes;
import id.investree.customer.repository.CustomerNotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerNotesServiceImpl implements CustomerNotesService {

    @Value("${microservice.notes}")
    private String notesUrl;

    @Autowired
    private GenericRestClient genericRestClient;

    @Autowired
    private JsonConverter jsonConverter;

    @Autowired
    private CustomerNotesRepository customerNotesRepository;

    @Override
    public CustomerNotes findById(Long id) { return customerNotesRepository.findById(id).orElse(null); }

    @Override
    public CustomerNotes findByCustomerIdAndCreatedBy(Long customerId, Long createdBy) {
        return customerNotesRepository.findByCustomerIdAndCreatedBy(customerId, createdBy).orElse(null);
    }

    @Override
    public CustomerNotes internalFindByCustomerIdAndCreatedBy(Long customerId, Long createdBy, String accessToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-Investree-Token", accessToken);

        RequestDetails requestDetails = new RequestDetails();
        requestDetails.setUrl(notesUrl + "/internal/customer-notes?customerId=" + customerId + "&createdBy=" + createdBy);
        requestDetails.setRequestType(HttpMethod.GET);
        requestDetails.setHeaders(headers);

        ResultResponse result = genericRestClient.execute(requestDetails, new ResponseErrorHandler() {
            @Override
            public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
                return clientHttpResponse.getStatusCode().isError();
            }

            @Override
            public void handleError(ClientHttpResponse clientHttpResponse) {
                try {
                    ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
                    throw new AppException(response.meta.message);
                } catch (IOException e) {
                    throw new AppException(e.getMessage());
                }
            }
        }, ResultResponse.class);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper.convertValue(result.data, new TypeReference<CustomerNotes>() {
        });
    }

    @Override
    public CustomerNotes saveOrUpdate(CustomerNotes customerNotesData) { return customerNotesRepository.save(customerNotesData); }

    @Override
    public CustomerNotes internalSaveOrUpdate(CustomerNotes customerNotesRequest, String accessToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-Investree-Token", accessToken);

        RequestDetails requestDetails = new RequestDetails();
        requestDetails.setUrl(notesUrl + "/internal/customer-notes");
        requestDetails.setRequestType(HttpMethod.POST);
        requestDetails.setHeaders(headers);

        ResultResponse result = genericRestClient.execute(requestDetails, customerNotesRequest, new ResponseErrorHandler() {
            @Override
            public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
                return clientHttpResponse.getStatusCode().isError();
            }

            @Override
            public void handleError(ClientHttpResponse clientHttpResponse) {
                try {
                    ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
                    throw new AppException(response.meta.message);
                } catch (IOException e) {
                    throw new AppException(e.getMessage());
                }
            }
        }, ResultResponse.class);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper.convertValue(result.data, new TypeReference<CustomerNotes>() {
        });
    }

    @Override
    public List<CustomerNotes> findByCustomerId(Long customerId) { return  customerNotesRepository.findByCustomerId(customerId).orElse(new ArrayList<>()); }

    @Override
    public List<CustomerNotes> internalFindByCustomerId(Long customerId, String accessToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-Investree-Token", accessToken);

        RequestDetails requestDetails = new RequestDetails();
        requestDetails.setUrl(notesUrl + "/internal/customer-notes/" + customerId);
        requestDetails.setRequestType(HttpMethod.GET);
        requestDetails.setHeaders(headers);

        ResultResponse result = genericRestClient.execute(requestDetails, new ResponseErrorHandler() {
            @Override
            public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
                return clientHttpResponse.getStatusCode().isError();
            }

            @Override
            public void handleError(ClientHttpResponse clientHttpResponse) {
                try {
                    ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
                    throw new AppException(response.meta.message);
                } catch (IOException e) {
                    throw new AppException(e.getMessage());
                }
            }
        }, ResultResponse.class);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper.convertValue(result.data, new TypeReference<List<CustomerNotes>>() {
        });
    }

    @Override
    public  List<CustomerNotes> findAll() { return  customerNotesRepository.findAll();}
}
