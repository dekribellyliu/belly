package id.investree.customer.service.customernotification;

import id.investree.customer.model.request.InitialDataRequest;

public interface CustomerNotificationService {

	void setOtpPartners(String accessToken, InitialDataRequest request);

	void sendNotificationVerificationData(String accessToken, String fullname, String userCategory);

	void sendEmail(String accessToken, String slug, Long customerId);
}
