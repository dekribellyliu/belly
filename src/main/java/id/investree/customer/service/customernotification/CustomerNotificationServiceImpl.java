package id.investree.customer.service.customernotification;

import id.investree.core.base.ResultResponse;
import id.investree.core.exception.AppException;
import id.investree.core.json.JsonConverter;
import id.investree.core.rest.GenericRestClient;
import id.investree.core.rest.RequestDetails;
import id.investree.customer.model.request.InitialDataRequest;
import id.investree.customer.model.request.InternalVerificationDataRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

@Service
public class CustomerNotificationServiceImpl implements CustomerNotificationService {

	@Value("${microservice.notification}")
	private String notificationUrl;

	@Autowired
	private GenericRestClient genericRestClient;

	@Autowired
	private JsonConverter jsonConverter;

	@Override
	public void setOtpPartners(String accessToken, InitialDataRequest request) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Investree-Token", accessToken);

		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(notificationUrl + "/partners/otp-save-only/" + request.getLoginDataId());
		requestDetails.setRequestType(HttpMethod.POST);
		requestDetails.setHeaders(headers);

		genericRestClient.execute(requestDetails, request, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					throw new AppException(response.meta.message);
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);
	}

	@Override
	public void sendNotificationVerificationData(String accessToken, String fullname, String userCategory) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Investree-Token", accessToken);

		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(notificationUrl + "/slack/verification-data");
		requestDetails.setRequestType(HttpMethod.POST);
		requestDetails.setHeaders(headers);

		InternalVerificationDataRequest request = new InternalVerificationDataRequest();
		request.setFullname(fullname);
		request.setUserCategory(Integer.valueOf(userCategory));

		genericRestClient.execute(requestDetails, request, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					throw new AppException(response.meta.message);
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);
	}

	@Override
	public void sendEmail(String accessToken, String slug, Long customerId) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Investree-Token", accessToken);

		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(notificationUrl + "/email/send/" + slug + "/" + customerId);
		requestDetails.setRequestType(HttpMethod.POST);
		requestDetails.setHeaders(headers);

		genericRestClient.execute(requestDetails, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					throw new AppException(response.meta.message);
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);
	}
}
