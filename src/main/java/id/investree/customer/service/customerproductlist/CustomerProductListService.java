package id.investree.customer.service.customerproductlist;

import id.investree.customer.entity.CustomerProductList;

import java.util.List;

public interface CustomerProductListService {

	List<CustomerProductList> findByCustomerRoleIds(Long... customerRoleIds);

	CustomerProductList saveOrUpdate(CustomerProductList request);
}
