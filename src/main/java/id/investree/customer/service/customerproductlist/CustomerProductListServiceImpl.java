package id.investree.customer.service.customerproductlist;

import id.investree.customer.entity.CustomerProductList;
import id.investree.customer.repository.CustomerProductListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CustomerProductListServiceImpl implements CustomerProductListService {

	@Autowired
	private CustomerProductListRepository repository;

	@Override
	public List<CustomerProductList> findByCustomerRoleIds(Long... customerRoleIds) {
		return repository.findByCustomerRoleIdInAndDeletedAtIsNull(customerRoleIds).orElse(null);
	}

	@Override
	@Transactional
	public CustomerProductList saveOrUpdate(CustomerProductList request) {
		return repository.save(request);
	}
}
