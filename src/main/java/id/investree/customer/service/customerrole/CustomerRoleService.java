package id.investree.customer.service.customerrole;

import id.investree.customer.entity.CustomerRole;

import java.util.List;

public interface CustomerRoleService {

	CustomerRole saveOrUpdate(CustomerRole data);

	CustomerRole findById(Long id);

	CustomerRole findByCustomerId(Long customerId);

	List<CustomerRole> findByCustomerIdAndType(Long customerId, Integer... type);

	CustomerRole findByCustomerIdAndType(Long customerId, Integer type);

	List<CustomerRole> findAllByCustomerId(Long customerId);

	CustomerRole findByCrNumber(String crNumber);

	Long countActive();

	void hardDeleteByCustomerId(Long customerId);
}
