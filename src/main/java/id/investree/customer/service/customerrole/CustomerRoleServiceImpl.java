package id.investree.customer.service.customerrole;

import id.investree.customer.entity.CustomerRole;
import id.investree.customer.repository.CustomerRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerRoleServiceImpl implements CustomerRoleService {

	@Autowired
	private CustomerRoleRepository repository;

	@Override
	@Transactional
	public CustomerRole saveOrUpdate(CustomerRole data) {
		return repository.save(data);
	}

	@Override
	public CustomerRole findById(Long id) {
		return repository.findById(id).orElse(null);
	}

	@Override
	public CustomerRole findByCustomerId(Long customerId) {
		List<CustomerRole> roles = repository.findByCustomerInformationId(customerId).orElse(new ArrayList<>());
		return roles.isEmpty() ? null : roles.get(0);
	}

	@Override
	public List<CustomerRole> findByCustomerIdAndType(Long customerId, Integer... type) {
		return repository.findByCustomerInformationIdAndTypeIn(customerId, type).orElse(null);
	}

	@Override
	public CustomerRole findByCustomerIdAndType(Long customerId, Integer type) {
		return repository.findByCustomerInformationIdAndType(customerId, type).orElse(null);
	}


	public List<CustomerRole> findAllByCustomerId(Long customerId) {
		return repository.findByCustomerInformationId(customerId).orElse(new ArrayList<>());
	}

	@Override
	public CustomerRole findByCrNumber(String crNumber) {
		return repository.findByCustomerNumber(crNumber).orElse(null);
	}

	@Override
	public Long countActive() {
		return repository.countActiveAtIsNotNull();
	}

	@Override
	public void hardDeleteByCustomerId(Long customerId) {
		repository.deleteByCustomerInformationId(customerId);
	}
}
