package id.investree.customer.service.digitalsignature;

import id.investree.customer.model.response.DigitalSignatureResponse;

public interface DigitalSignatureService {

	DigitalSignatureResponse findDigitalSignatureFile(String accessToken);
}
