package id.investree.customer.service.emergencycontact;

import id.investree.customer.entity.EmergencyContact;

import java.util.List;

public interface EmergencyContactService {

	EmergencyContact saveOrUpdate(EmergencyContact entity);

	EmergencyContact findById(Long id);

	EmergencyContact findByIdAndCustomer(Long id, Long customerId);

	List<EmergencyContact> findByCustomerId(Long customerId);

	List<EmergencyContact> internalFindByCustomerId(Long customerId, String accessToken);

	EmergencyContact findByIdentityCardNumberAndCustomerIdAndDeletedIsNull(String identityCardNumber, Long customerId);

	List<EmergencyContact> saveAll(List<EmergencyContact> data);

}
