package id.investree.customer.service.emergencycontact;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.investree.core.base.ResultResponse;
import id.investree.core.exception.AppException;
import id.investree.core.json.JsonConverter;
import id.investree.core.rest.GenericRestClient;
import id.investree.core.rest.RequestDetails;
import id.investree.customer.entity.EmergencyContact;
import id.investree.customer.repository.EmergencyContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class EmergencyContactServiceImpl implements EmergencyContactService {

	@Value("${microservice.emergency}")
	private String emergencyUrl;

	@Autowired
	private GenericRestClient genericRestClient;

	@Autowired
	private JsonConverter jsonConverter;

	@Autowired
	private EmergencyContactRepository emergencyContactRepository;

	@Override
	public EmergencyContact findById(Long id) {
		return emergencyContactRepository.findById(id).orElse(null);
	}

	@Override
	public EmergencyContact findByIdAndCustomer(Long id, Long customerId) {
		return emergencyContactRepository.findByIdAndCustomerIdAndDeletedAtIsNull(id, customerId).orElse(null);
	}

	@Override
	public EmergencyContact saveOrUpdate(EmergencyContact entity) {
		return emergencyContactRepository.save(entity);
	}

	@Override
	public List<EmergencyContact> findByCustomerId(Long customerId) {
		return emergencyContactRepository.findByCustomerIdAndDeletedAtIsNull(customerId).orElse(new ArrayList<>());
	}

	@Override
	public List<EmergencyContact> internalFindByCustomerId(Long customerId, String accessToken) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Investree-Token", accessToken);

		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(emergencyUrl + "/internal/emergency/" + customerId);
		requestDetails.setRequestType(HttpMethod.GET);
		requestDetails.setHeaders(headers);

		ResultResponse result = genericRestClient.execute(requestDetails, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					throw new AppException(response.meta.message);
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.convertValue(result.data, new TypeReference<List<EmergencyContact>>() {
		});
	}

	@Override
	public EmergencyContact findByIdentityCardNumberAndCustomerIdAndDeletedIsNull(String identityCardNumber, Long customerId) {
		return emergencyContactRepository.findByIdentityCardNumberAndCustomerIdAndDeletedAtIsNull(identityCardNumber, customerId).orElse(null);
	}

	@Override
	public List<EmergencyContact> saveAll(List<EmergencyContact> data) {
		return emergencyContactRepository.saveAll(data);
	}

}
