package id.investree.customer.service.financialinformation;

import id.investree.customer.entity.FinancialInformation;

import java.util.List;

public interface FinancialInformationService {

	FinancialInformation findById(Long id);

	FinancialInformation findByIdAndDeleted(Long id);

	FinancialInformation findByIdAndCustomerId(Long id, Long customerId);

	FinancialInformation saveOrUpdate(FinancialInformation financialInformationRequest);

	List<FinancialInformation> findByCustomerId(Long customerId);

	List<FinancialInformation> findByCustomerIdAndStatementFileType(Long customerId, Integer statementFileType);

	FinancialInformation findFirstByCustomerIdAndStatementFileTypeOrderByStatementFileDateAndCreatedAt(Long customerId, Integer statementFileType);

	FinancialInformation findFirstByCustomerIdAndStatementFileTypeOrderByStatementFileDateAndUpdatedAt(Long customerId, Integer statementFileType);
}
