package id.investree.customer.service.financialinformation;

import id.investree.customer.entity.FinancialInformation;
import id.investree.customer.repository.FinancialInformationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class FinancialInformationServiceImpl implements FinancialInformationService {

	@Autowired
	private FinancialInformationRepository financialInformationRepository;

	@Override
	public FinancialInformation findById(Long id) {
		return financialInformationRepository.findById(id).orElse(null);
	}

	@Override
	public FinancialInformation findByIdAndDeleted(Long id) {
		return financialInformationRepository.findByIdAndDeletedAtIsNull(id).orElse(null);
	}

	@Override
	public FinancialInformation findByIdAndCustomerId(Long id, Long customerId) {
		return financialInformationRepository.findByIdAndCustomerIdAndDeletedAtIsNull(id, customerId).orElse(null);
	}

	@Override
	@Transactional
	public FinancialInformation saveOrUpdate(FinancialInformation financialData) {
		return financialInformationRepository.saveAndFlush(financialData);
	}

	@Override
	public List<FinancialInformation> findByCustomerId(Long customerId) {
		return financialInformationRepository.findByCustomerIdAndDeletedAtIsNull(customerId).orElse(new ArrayList<>());
	}

	@Override
	public List<FinancialInformation> findByCustomerIdAndStatementFileType(Long customerId, Integer statementFileType) {
		return financialInformationRepository
			.findByCustomerIdAndStatementFileTypeAndDeletedAtIsNull(customerId, statementFileType)
			.orElse(new ArrayList<>());
	}

	@Override
	public FinancialInformation findFirstByCustomerIdAndStatementFileTypeOrderByStatementFileDateAndCreatedAt(Long customerId, Integer statementFileType) {
		return financialInformationRepository
			.findFirstByCustomerIdAndStatementFileTypeAndDeletedAtIsNullOrderByStatementFileDateDescCreatedAtDescIdDesc(
				customerId, statementFileType).orElse(null);
	}

	@Override
	public FinancialInformation findFirstByCustomerIdAndStatementFileTypeOrderByStatementFileDateAndUpdatedAt(Long customerId, Integer statementFileType) {
		return financialInformationRepository
			.findFirstByCustomerIdAndStatementFileTypeAndDeletedAtIsNullOrderByStatementFileDateDescUpdatedAtDescCreatedAtDescIdDesc(
				customerId, statementFileType).orElse(null);
	}
}
