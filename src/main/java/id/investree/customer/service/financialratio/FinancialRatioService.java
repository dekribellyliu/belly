package id.investree.customer.service.financialratio;

import id.investree.customer.entity.FinancialRatio;

import java.util.List;

public interface FinancialRatioService {

	List<FinancialRatio> findByCustomerId(Long customerId);

	FinancialRatio findByIdAndCustomerId(Long id, Long customerId);

	FinancialRatio saveOrUpdate(FinancialRatio data);
}
