package id.investree.customer.service.financialratio;

import id.investree.customer.entity.FinancialRatio;
import id.investree.customer.repository.FinancialRatioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FinancialRatioServiceImpl implements FinancialRatioService {

	@Autowired
	private FinancialRatioRepository repository;

	@Override
	public List<FinancialRatio> findByCustomerId(Long customerId) {
		return repository.findByCustomerId(customerId).orElse(new ArrayList<>());
	}

	@Override
	public FinancialRatio findByIdAndCustomerId(Long id, Long customerId) {
		return repository.findByIdAndCustomerId(id, customerId).orElse(null);
	}

	@Override
	public FinancialRatio saveOrUpdate(FinancialRatio data) {
		return repository.saveAndFlush(data);
	}
}
