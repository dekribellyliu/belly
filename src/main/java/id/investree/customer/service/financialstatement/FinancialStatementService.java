package id.investree.customer.service.financialstatement;

import id.investree.customer.entity.FinancialStatement;

import java.util.List;

public interface FinancialStatementService {

	List<FinancialStatement> findByCustomerId(Long customerId);

	FinancialStatement findByCustomerIdAndYearTo(Long customerId, Long yearTo);

	FinancialStatement findByIdAndCustomerId(Long id, Long customerId);

	FinancialStatement saveOrUpdate(FinancialStatement data);
}
