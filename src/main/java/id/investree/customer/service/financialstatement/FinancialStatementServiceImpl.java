package id.investree.customer.service.financialstatement;

import id.investree.customer.entity.FinancialStatement;
import id.investree.customer.repository.FinancialStatementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FinancialStatementServiceImpl implements FinancialStatementService {

	@Autowired
	private FinancialStatementRepository repository;

	@Override
	public List<FinancialStatement> findByCustomerId(Long customerId) {
		return repository.findByCustomerId(customerId).orElse(new ArrayList<>());
	}

	@Override
	public FinancialStatement findByCustomerIdAndYearTo(Long customerId, Long yearTo) {
		return repository.findByCustomerIdAndYearTo(customerId, yearTo).orElse(null);
	}

	@Override
	public FinancialStatement findByIdAndCustomerId(Long id, Long customerId) {
		return repository.findByIdAndCustomerId(id, customerId).orElse(null);
	}

	@Override
	public FinancialStatement saveOrUpdate(FinancialStatement data) {
		return repository.saveAndFlush(data);
	}



}
