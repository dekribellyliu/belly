package id.investree.customer.service.financialtrend;

import id.investree.customer.entity.FinancialTrend;

import java.util.List;

public interface FinancialTrendService {

	List<FinancialTrend> findByCustomerId(Long customerId);

	FinancialTrend findByIdAndCustomerId(Long id, Long customerId);

	FinancialTrend findByCustomerIdAndTrendPeriod(Long customerId, String yearPeriod);

	FinancialTrend saveOrUpdate(FinancialTrend data);
}
