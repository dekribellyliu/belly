package id.investree.customer.service.financialtrend;

import id.investree.customer.entity.FinancialTrend;
import id.investree.customer.repository.FinancialTrendRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class FinancialTrendServiceImpl implements FinancialTrendService {

	@Autowired
	private FinancialTrendRepository repository;

	@Override
	public List<FinancialTrend> findByCustomerId(Long customerId) {
		return repository.findByCustomerId(customerId).orElse(new ArrayList<>());
	}

	@Override
	public FinancialTrend findByIdAndCustomerId(Long id, Long customerId) {
		return repository.findByIdAndCustomerId(id, customerId).orElse(null);
	}

	@Override
	public FinancialTrend findByCustomerIdAndTrendPeriod(Long customerId, String yearPeriod) {
		return repository.findByCustomerIdAndTrendPeriod(customerId, yearPeriod).orElse(null);
	}

	@Override
	@Transactional
	public FinancialTrend saveOrUpdate(FinancialTrend data) {
		return repository.saveAndFlush(data);
	}
}
