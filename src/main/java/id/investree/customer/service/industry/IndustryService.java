package id.investree.customer.service.industry;

import id.investree.customer.model.data.Industry;

public interface IndustryService {

	public Industry findByCode(String code);

}
