package id.investree.customer.service.integration;

import id.investree.customer.entity.MrInvarBank;
import id.investree.customer.model.data.TransactionHistoryData;
import id.investree.customer.model.data.VirtualAccountData;
import id.investree.customer.model.request.BankAccountValidationRequest;
import id.investree.customer.model.request.RecreateVirtualAccountRequest;
import id.investree.customer.model.request.TransactionHistoryRequest;
import id.investree.customer.model.response.PaginationDataResponse;
import id.investree.customer.model.response.VirtualAccountResponse;

import java.util.List;

public interface IntegrationService {

	void createBankVirtualAccount(VirtualAccountData data, List<MrInvarBank> mrInvarBanks);

	VirtualAccountResponse resendCreateVa(String accessToken, RecreateVirtualAccountRequest request);

	Boolean checkBankAccount(BankAccountValidationRequest request);

	PaginationDataResponse<TransactionHistoryData> getHistoryData(TransactionHistoryRequest data);
}
