package id.investree.customer.service.integration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.json.JsonConverter;
import id.investree.core.model.DefaultData;
import id.investree.core.rest.GenericRestClient;
import id.investree.core.rest.RequestDetails;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.BankInformation;
import id.investree.customer.entity.MrInvarBank;
import id.investree.customer.entity.UserData;
import id.investree.customer.model.data.TransactionHistoryData;
import id.investree.customer.model.data.VirtualAccountData;
import id.investree.customer.model.request.BankAccountValidationRequest;
import id.investree.customer.model.request.CreateVirtualAccountRequest;
import id.investree.customer.model.request.RecreateVirtualAccountRequest;
import id.investree.customer.model.request.TransactionHistoryRequest;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.model.response.PaginationDataResponse;
import id.investree.customer.model.response.VirtualAccountResponse;
import id.investree.customer.service.bankinformation.BankInformationService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.Constants;
import id.investree.customer.utils.LocalMessageUtils;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class IntegrationServiceImpl implements IntegrationService {

	@Value("${microservice.integration}")
	private String integrationUrl;

	@Autowired
	private GenericRestClient genericRestClient;

	@Autowired
	private JsonConverter jsonConverter;

	@Autowired
	private BankInformationService bankInformationService;

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private LoginDataService loginDataService;

	@Autowired
	private CustomerInformationService customerInformationService;

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	@Autowired
	private LocalMessageUtils localMessageUtils;

	@Override
	@Transactional
	public void createBankVirtualAccount(VirtualAccountData data, List<MrInvarBank> mrInvarBanks) {
		String accessToken = data.getAccessToken();

		UserData userData = Optional.ofNullable(userDataService.findByCustomerId(data.getCustomerInformation().getId()))
				.orElseThrow(() -> new DataNotFoundException("User Data"));

		String accountName = "";
		if (GlobalConstants.CATEGORY_INSTITUSI_ID.toString().equalsIgnoreCase(data.getCustomerInformation().getUserCategory())) {
			accountName = data.getCustomerInformation().getName();
			if (accountName.isEmpty()) {
				throw new AppException(localMessageUtils.errorFailCreateVa());
			}

			String legalEntity = "";
			if (!Utilities.isNull(data.getCustomerInformation().getLegalEntity())) {
				completingDataGlobalParam.getGlobalParam(GlobalConstants.GLOBAL_PARAM_LEGAL_ENTITY);
				legalEntity = completingDataGlobalParam.getLegalList().stream()
						.filter(v -> v.getId().equals(Math.toIntExact(data.getCustomerInformation().getLegalEntity())))
						.findFirst()
						.map(DefaultData::getName)
						.orElse("");
			}

			if (!accountName.startsWith(legalEntity)) {
				accountName = String.format("%s %s", accountName, legalEntity);
			}

		} else {
			accountName = Optional.ofNullable(loginDataService.findByLoginId(accessToken, userData.getLoginDataId()))
					.map(LoginDataResponse::getFullname)
					.orElseThrow(() -> new DataNotFoundException("Login Data"));
		}
		HttpHeaders headers = new HttpHeaders();
		headers.add("X-Investree-Token", accessToken);

		CreateVirtualAccountRequest request = new CreateVirtualAccountRequest();
		request.setUserType(data.getUserType());
		request.setUserPreference(data.getCustomerInformation().getProductPreference());
		request.setVirtualAccountName(accountName);

		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(integrationUrl + "/virtual-account/create-va");
		requestDetails.setRequestType(HttpMethod.POST);
		requestDetails.setHeaders(headers);

		AtomicReference<String> status = new AtomicReference<>("");
		AtomicReference<String> storedVirtualNumber = new AtomicReference<>("");
		Optional.ofNullable(genericRestClient.execute(requestDetails, request, null, ResultResponse.class)).ifPresent(result -> {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			List<VirtualAccountResponse> vaResponses = mapper.convertValue(result.data, new TypeReference<List<VirtualAccountResponse>>() {
			});

			if (!vaResponses.isEmpty()) {
				status.set(Optional.ofNullable(vaResponses.get(0))
						.map(VirtualAccountResponse::getStatus)
						.orElseThrow(() -> new DataNotFoundException("status")));

				storedVirtualNumber.set(Optional.ofNullable(vaResponses.get(0))
						.map(v -> v.getVirtualNumber().replace(v.getBinNumber(), ""))
						.orElseThrow(() -> new DataNotFoundException("virtual account")));
			}
		});

		String finalAccountName = accountName;
		mrInvarBanks.forEach(mrInvarBank -> {
			BankInformation bankInformation = new BankInformation();
			bankInformation.setCustomerId(data.getCustomerInformation().getId());
			bankInformation.setCustomerRoleId(data.getCustomerRoleId());
			bankInformation.setBankType(Constants.BANK_TYPE_VA);
			bankInformation.setBankAccountNumber(storedVirtualNumber.get());
			bankInformation.setMasterBankId(mrInvarBank.getBankId());
			bankInformation.setBankAccountHolderName(finalAccountName);
			bankInformation.setInvarBank(mrInvarBank.getId());

			if (mrInvarBank.getBankName().toLowerCase().contains("cimb")) {
				bankInformation.setVerifiedStatus("Verified");
			} else {
				bankInformation.setVerifiedStatus("SUCCESS".equalsIgnoreCase(status.get()) ? "Verified" : "Unverified");
			}
			bankInformationService.saveOrUpdate(bankInformation);
		});
	}

	public VirtualAccountResponse resendCreateVa(String accessToken, RecreateVirtualAccountRequest request) {
		HttpHeaders headers = new HttpHeaders();
		headers.add(GlobalConstants.HEADER_TOKEN, accessToken);

		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(integrationUrl + "/virtual-account/recreate-va");
		requestDetails.setRequestType(HttpMethod.POST);
		requestDetails.setHeaders(headers);

		ResultResponse result = genericRestClient.execute(requestDetails, request, null, ResultResponse.class);
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.convertValue(result.data, new TypeReference<VirtualAccountResponse>() {
		});

	}

	@Override
	public Boolean checkBankAccount(BankAccountValidationRequest request) {
		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(integrationUrl + "/bca/account-verification");
		requestDetails.setHeaders(listHttpHeaders());
		requestDetails.setRequestType(HttpMethod.POST);

		ResultResponse result = genericRestClient.execute(requestDetails, request, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					if (response.meta.code == 400 && response.meta.message == null) {
						throw new AppException("Invalid account. Please try again.");
					} else {
						throw new AppException(response.meta.message);
					}
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		if (!Utilities.isNull(result.data)) {
			LinkedHashMap data = (LinkedHashMap) result.data;
			return Constants.RESPONSE_BANK_ACCOUNT_IS_VALID.equals(data.get(Constants.RESPONSE_BANK_ACCOUNT_CODE_KEY));
		}

		return false;
	}

	@Override
	public PaginationDataResponse<TransactionHistoryData> getHistoryData(TransactionHistoryRequest data) {

		if (Utilities.isEmptyOrBlank(data.getPartnershipId())) {
			throw new DataNotFoundException();
		}

		String requestUrl = integrationUrl + "/transaction-history?partnershipId=" + data.getPartnershipId();
		if (Optional.ofNullable(data.getPage()).isPresent()) {
			requestUrl += "&page=" + data.getPage();
		}
		if (Optional.ofNullable(data.getSize()).isPresent()) {
			requestUrl += "&size=" + data.getSize();
		}
		if (!Utilities.isEmptyOrBlank(data.getSort())) {
			requestUrl += "&sort=" + data.getSort();
		}
		if (!Utilities.isEmptyOrBlank(data.getBuyerName())) {
			requestUrl += "&buyerName=" + data.getBuyerName();
		}
		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(requestUrl);
		requestDetails.setHeaders(listHttpHeaders());
		requestDetails.setRequestType(HttpMethod.GET);

		ResultResponse result = genericRestClient.execute(requestDetails, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					throw new AppException(response.meta.message);
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.convertValue(result.data, new TypeReference<PaginationDataResponse<TransactionHistoryData>>() {
		});
	}

	private HttpHeaders listHttpHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("X-Investree-Token", GlobalConstants.IGNORED);
		return headers;
	}
}
