package id.investree.customer.service.legacy;

import id.investree.core.model.legacy.LegacyModel;

public interface SyncLegacyService {

	void sync(String accessToken, LegacyModel legacyModel);
}
