package id.investree.customer.service.legacy;

import id.investree.core.integration.legacy.LegacyService;
import id.investree.core.model.legacy.LegacyModel;
import id.investree.core.utils.TokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SyncLegacyServiceImpl implements SyncLegacyService {

	@Value("${microservice.devapi-sync-legacy}")
	private String legacySyncUrl;

	@Autowired
	private LegacyService legacyService;

	@Autowired
	private TokenUtils tokenUtils;

	@Override
	public void sync(String accessToken, LegacyModel legacyModel) {
		legacyService.syncLegacy(legacySyncUrl, legacyModel);
	}
}
