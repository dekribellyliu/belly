package id.investree.customer.service.legalinformation;

import id.investree.customer.entity.LegalInformation;

import java.util.List;

public interface LegalInformationService {

	LegalInformation save(LegalInformation data);

	List<LegalInformation> saveAll(List<LegalInformation> data);

	List<LegalInformation> saveAllAndFlush(List<LegalInformation> data);

	List<LegalInformation> findByCustomerId(Long customerId);

	LegalInformation findByCustomerIdAndType(Long customerId, Integer type);

	List<LegalInformation> findByTypeAndDocNumber(Integer type, String number);

}
