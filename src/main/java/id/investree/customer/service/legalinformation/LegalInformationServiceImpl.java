package id.investree.customer.service.legalinformation;

import id.investree.customer.entity.LegalInformation;
import id.investree.customer.repository.LegalInformationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class LegalInformationServiceImpl implements LegalInformationService {

	@Autowired
	private LegalInformationRepository repository;

	@Override
	public LegalInformation save(LegalInformation data) {
		return repository.saveAndFlush(data);
	}

	@Override
	@Transactional
	public List<LegalInformation> saveAll(List<LegalInformation> data) {
		return repository.saveAll(data);
	}

	@Override
	@Transactional
	public List<LegalInformation> saveAllAndFlush(List<LegalInformation> data) {
		List<LegalInformation> saveResult = new ArrayList<>();
		data.forEach(v -> {
			saveResult.add(repository.saveAndFlush(v));
		});

		return saveResult;
	}

	@Override
	@Transactional(isolation = Isolation.READ_UNCOMMITTED)
	public List<LegalInformation> findByCustomerId(Long customerId) {
		return repository.findByCustomerId(customerId).orElse(new ArrayList<>());
	}

	@Override
	public LegalInformation findByCustomerIdAndType(Long customerId, Integer type) {
		return repository.findByCustomerIdAndDocumentType(customerId, type).orElse(new LegalInformation());
	}

	@Override
	public List<LegalInformation> findByTypeAndDocNumber(Integer type, String number) {
		return repository.findByDocumentTypeAndDocumentNumber(type, number).orElse(new ArrayList<>());
	}
}
