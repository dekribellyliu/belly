package id.investree.customer.service.loan;

import id.investree.customer.model.response.VendorApplicationResponse;

public interface LoanService {
	public boolean updateRmByBorrower(Long borrowerId, Long rmId);

	public VendorApplicationResponse getVendor(Long anchorId, Long vendorId);
}
