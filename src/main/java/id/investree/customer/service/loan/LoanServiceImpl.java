package id.investree.customer.service.loan;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.json.JsonConverter;
import id.investree.core.rest.GenericRestClient;
import id.investree.core.rest.RequestDetails;
import id.investree.core.utils.Utilities;
import id.investree.customer.model.request.LoanUpdateRmRequest;
import id.investree.customer.model.response.VendorApplicationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

@Service
public class LoanServiceImpl implements LoanService {

	@Value("${microservice.loan}")
	private String loanUrl;

	@Autowired
	private GenericRestClient genericRestClient;

	@Autowired
	private JsonConverter jsonConverter;

	@Override
	public boolean updateRmByBorrower(Long borrowerId, Long rmId) {
		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(loanUrl + "/corporateloanapplication/updateRmByBorrowerId");
		requestDetails.setHeaders(listHttpHeaders());
		requestDetails.setRequestType(HttpMethod.PUT);

		LoanUpdateRmRequest request = new LoanUpdateRmRequest(borrowerId, rmId);
		ResultResponse result = genericRestClient.execute(requestDetails, request, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					throw new AppException(response.meta.message);
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		if (!Utilities.isNull(result.data)) {
			return (Boolean) result.data;
		}

		return false;
	}

	@Override
	public VendorApplicationResponse getVendor(Long anchorId, Long vendorId) {
		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(loanUrl + "/corporateloanapplication/vendor/"+ anchorId + "/" + vendorId);
		requestDetails.setHeaders(listHttpHeaders());
		requestDetails.setRequestType(HttpMethod.GET);

		ResultResponse result = genericRestClient.execute(requestDetails, null, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					throw new AppException(response.meta.message);
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		VendorApplicationResponse response = objectMapper.convertValue(result.data, VendorApplicationResponse.class);

		return response;
	}



	private HttpHeaders listHttpHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("X-Investree-Token", GlobalConstants.IGNORED);
		return headers;
	}
}
