package id.investree.customer.service.logindata;

import id.investree.customer.model.request.LoginDataRequest;
import id.investree.customer.model.response.LoginDataResponse;

public interface LoginDataService {

	LoginDataResponse findByLoginId(String accessToken, Long loginId);

	LoginDataResponse updateLoginData(String accessToken, Long loginId, LoginDataRequest data);
}
