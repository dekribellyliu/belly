package id.investree.customer.service.logindata;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.investree.core.base.ResultResponse;
import id.investree.core.exception.AppException;
import id.investree.core.json.JsonConverter;
import id.investree.core.rest.GenericRestClient;
import id.investree.core.rest.RequestDetails;
import id.investree.customer.model.request.LoginDataRequest;
import id.investree.customer.model.response.LoginDataResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

@Service
public class LoginDataServiceImpl implements LoginDataService {

	@Value("${microservice.user}")
	private String userUrl;

	@Autowired
	private GenericRestClient genericRestClient;

	@Autowired
	private JsonConverter jsonConverter;

	@Override
	public LoginDataResponse findByLoginId(String accessToken, Long loginId) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Investree-Token", accessToken);

		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(userUrl + "/internal/user/" + loginId);
		requestDetails.setRequestType(HttpMethod.GET);
		requestDetails.setHeaders(headers);

		ResultResponse result = genericRestClient.execute(requestDetails, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					throw new AppException(response.meta.message);
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.convertValue(result.data, new TypeReference<LoginDataResponse>() {
		});
	}

	@Override
	public LoginDataResponse updateLoginData(String accessToken, Long loginId, LoginDataRequest data) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Investree-Token", accessToken);

		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(userUrl + "/internal/user/" + loginId);
		requestDetails.setRequestType(HttpMethod.PUT);
		requestDetails.setHeaders(headers);

		ResultResponse result = genericRestClient.execute(requestDetails, data, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					throw new AppException(response.meta.message);
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.convertValue(result.data, new TypeReference<LoginDataResponse>() {
		});
	}
}
