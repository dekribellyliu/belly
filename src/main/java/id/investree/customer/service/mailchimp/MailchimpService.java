package id.investree.customer.service.mailchimp;

public interface MailchimpService {

    boolean subscribe(String email, String fullname, String userType);
}
