package id.investree.customer.service.mailchimp;

import id.investree.core.json.JsonConverter;
import id.investree.core.rest.GenericRestClient;
import id.investree.core.rest.RequestDetails;
import id.investree.customer.model.data.MailchimpEmailData;
import id.investree.customer.model.data.MailchimpVarsData;
import id.investree.customer.model.request.MailchimpSubscribeRequest;
import id.investree.customer.model.response.MailchimpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

@Service
public class MailchimpServiceImpl implements MailchimpService {

	@Value("${mailchimp.apikey}")
	private String mailchimpApiKey;

	@Value("${mailchimp.id}")
	private String mailchimpId;

	private GenericRestClient genericRestClient;
	private JsonConverter jsonConverter;

	@Autowired
	public MailchimpServiceImpl(GenericRestClient genericRestClient, JsonConverter jsonConverter) {
		this.genericRestClient = genericRestClient;
		this.jsonConverter = jsonConverter;
	}

	@Override
	public boolean subscribe(String email, String fullname, String userType) {
		MailchimpEmailData emailData = MailchimpEmailData.builder()
				.email(email)
				.build();

		MailchimpVarsData varsData = MailchimpVarsData.builder()
				.fullname(fullname)
				.type(userType)
				.build();

		MailchimpSubscribeRequest model = MailchimpSubscribeRequest.builder()
				.apiKey(mailchimpApiKey)
				.id(mailchimpId)
				.email(emailData)
				.mergeVars(varsData)
				.build();

		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl("https://us12.api.mailchimp.com/2.0/lists/subscribe");
		requestDetails.setRequestType(HttpMethod.POST);

		MailchimpResponse response = genericRestClient.execute(requestDetails, model, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) {
				//ignored
			}
		}, MailchimpResponse.class);

		return null != response;
	}
}
