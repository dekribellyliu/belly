package id.investree.customer.service.masterbank;

import id.investree.customer.model.response.MasterBankResponse;

public interface MasterBankService {

	MasterBankResponse findById(Long masterBankId, String accessToken);
}
