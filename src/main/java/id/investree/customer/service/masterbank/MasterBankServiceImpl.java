package id.investree.customer.service.masterbank;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.investree.core.base.ResultResponse;
import id.investree.core.exception.AppException;
import id.investree.core.json.JsonConverter;
import id.investree.core.rest.GenericRestClient;
import id.investree.core.rest.RequestDetails;
import id.investree.core.utils.MessageUtils;
import id.investree.customer.model.response.MasterBankResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

@Service
public class MasterBankServiceImpl implements MasterBankService {

	@Value("${microservice.master-data}")
	private String masterDataUrl;

	@Autowired
	private GenericRestClient genericRestClient;

	@Autowired
	private MessageUtils messageUtils;

	@Autowired
	private JsonConverter jsonConverter;

	@Override
	public MasterBankResponse findById(Long masterBankId, String accessToken) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("X-Investree-Token", accessToken);

		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(masterDataUrl + "/banks/" + masterBankId);
		requestDetails.setRequestType(HttpMethod.GET);
		requestDetails.setHeaders(headers);

		ResultResponse result = genericRestClient.execute(requestDetails, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					throw new AppException(response.meta.message);
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.convertValue(result.data, new TypeReference<MasterBankResponse>() {
		});
	}
}
