package id.investree.customer.service.mrinvarbank;

import id.investree.customer.entity.MrInvarBank;

import java.util.List;

public interface MrInvarBankService {

	List<MrInvarBank> findById(Long... id);

	MrInvarBank findByBankIdAndType(Long bankId, Long... type);
}
