package id.investree.customer.service.mrinvarbank;

import id.investree.customer.entity.MrInvarBank;
import id.investree.customer.repository.MrInvarBankRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MrInvarBankServiceImpl implements MrInvarBankService {

	@Autowired
	private MrInvarBankRepository mrInvarBankRepository;

	@Override
	public List<MrInvarBank> findById(Long... type) {
		return mrInvarBankRepository.findByIdIn(type).orElse(new ArrayList<>());
	}

	@Override
	public MrInvarBank findByBankIdAndType(Long bankId, Long... type) {
		return mrInvarBankRepository.findByBankIdAndTypeIn(bankId, type).orElse(null);
	}

}
