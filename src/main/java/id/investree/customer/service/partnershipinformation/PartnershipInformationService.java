package id.investree.customer.service.partnershipinformation;

import id.investree.core.model.DefaultData;
import id.investree.customer.entity.PartnershipInformation;
import id.investree.customer.model.request.EcommercePartnershipRequest;
import id.investree.customer.model.response.DefaultListResponse;
import id.investree.customer.model.response.EcommercePartnershipResponse;

import java.util.List;

public interface PartnershipInformationService {

	List<PartnershipInformation> findAllByCustomerId(Long customerId);

	PartnershipInformation findByCustomerIdAndPartnerId(Long customerId, Long partnerId);

	PartnershipInformation findByCustomerId(Long customerId);

	PartnershipInformation findByIdAndCustomerId(Long id, Long customerId);

	PartnershipInformation saveOrUpdate(PartnershipInformation data);

	DefaultListResponse<DefaultData> jointVentureList(String accessToken);

	EcommercePartnershipResponse createEcommercePartnershipData(EcommercePartnershipRequest request, String accessToken);

	EcommercePartnershipResponse findEcommercePartnershipByCustomerId(Long customerId, String accessToken);

	EcommercePartnershipResponse findEcommercePartnershipByPartnerId(Long partnerId, String accessToken);
}
