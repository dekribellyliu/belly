package id.investree.customer.service.partnershipinformation;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.json.JsonConverter;
import id.investree.core.model.DefaultData;
import id.investree.core.rest.GenericRestClient;
import id.investree.core.rest.RequestDetails;
import id.investree.core.utils.Utilities;
import id.investree.customer.entity.PartnershipInformation;
import id.investree.customer.model.request.EcommercePartnershipRequest;
import id.investree.customer.model.response.DefaultListResponse;
import id.investree.customer.model.response.EcommercePartnershipResponse;
import id.investree.customer.repository.PartnershipInfromationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class PartnershipInformationServiceImpl implements PartnershipInformationService {

	@Value("${microservice.user-search}")
	private String userSearchrUrl;

	@Value("${microservice.partnership}")
	private String partnershipUrl;

	private final GenericRestClient genericRestClient;
	private final JsonConverter jsonConverter;
	private final PartnershipInfromationRepository repository;

	@Autowired
	public PartnershipInformationServiceImpl(
			PartnershipInfromationRepository repository,
			GenericRestClient genericRestClient,
			JsonConverter jsonConverter) {
		this.genericRestClient = genericRestClient;
		this.jsonConverter = jsonConverter;
		this.repository = repository;
	}

	@Override
	public List<PartnershipInformation> findAllByCustomerId(Long customerId) {
		return repository.findAllByCustomerId(customerId).orElse(new ArrayList<>());
	}

	@Override
	public PartnershipInformation findByCustomerIdAndPartnerId(Long customerId, Long partnerId) {
		return repository.findByCustomerIdAndPartnerId(customerId, partnerId).orElse(null);
	}

	@Override
	public PartnershipInformation findByCustomerId(Long customerId) {
		return repository.findByCustomerId(customerId).orElse(null);
	}

	@Override
	public PartnershipInformation findByIdAndCustomerId(Long id, Long customerId) {
		return repository.findByIdAndCustomerId(id, customerId).orElse(null);
	}

	@Override
	public PartnershipInformation saveOrUpdate(PartnershipInformation data) {
		return repository.saveAndFlush(data);
	}

	@Override
	public DefaultListResponse<DefaultData> jointVentureList(String accessToken) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Investree-Token", accessToken);

		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(userSearchrUrl + "/partner/list/jointventure?page=1&status=active&size=9999");
		requestDetails.setRequestType(HttpMethod.GET);
		requestDetails.setHeaders(headers);

		ResultResponse result = genericRestClient.execute(requestDetails, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					throw new AppException(response.meta.message);
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.convertValue(result.data, new TypeReference<DefaultListResponse<DefaultData>>() {
		});
	}

	@Override
	public EcommercePartnershipResponse createEcommercePartnershipData(EcommercePartnershipRequest request, String accessToken) {
		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(partnershipUrl + "/internal/ecommerce");
		requestDetails.setRequestType(HttpMethod.PUT);

		HttpHeaders headers = new HttpHeaders();
		headers.set(GlobalConstants.HEADER_TOKEN, Utilities.isNull(accessToken) ? GlobalConstants.IGNORED : accessToken);
		requestDetails.setHeaders(headers);

		ResultResponse result = genericRestClient.execute(requestDetails, request, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					throw new AppException(response.meta.message);
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.convertValue(result.data, new TypeReference<EcommercePartnershipResponse>() {
		});
	}

	@Override
	public EcommercePartnershipResponse findEcommercePartnershipByCustomerId(Long customerId, String accessToken) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Investree-Token", accessToken);

		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(partnershipUrl + "/internal/ecommerce/" + customerId);
		requestDetails.setRequestType(HttpMethod.GET);
		requestDetails.setHeaders(headers);

		ResultResponse result = genericRestClient.execute(requestDetails, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					throw new AppException(response.meta.message);
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.convertValue(result.data, new TypeReference<EcommercePartnershipResponse>() {
		});
	}

	@Override
	public EcommercePartnershipResponse findEcommercePartnershipByPartnerId(Long partnerId, String accessToken) {

		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(partnershipUrl + "/internal/ecommerce/partner/" + partnerId);
		requestDetails.setRequestType(HttpMethod.GET);

		ResultResponse result = genericRestClient.execute(requestDetails, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					throw new AppException(response.meta.message);
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.convertValue(result.data, new TypeReference<EcommercePartnershipResponse>() {
		});
	}
}
