package id.investree.customer.service.picrole;

import id.investree.customer.entity.PicRole;

import java.util.List;

public interface PicRoleService {
	
	List<PicRole> saveAllRole(List<PicRole> list);

	PicRole saveOrUpdate(PicRole data);

	List<PicRole> findByCustomerRoleIdAndLoginIdAndRoleIsNotNull(List<Long> customerId, Long loginId);

	List<PicRole> findByCustomerRoleIdAndLoginIdAndRoleIsNull(List<Long> customerId, Long loginId);

	List<PicRole> findByCustomerRoleIdAndLoginId(Long customerRoleId, Long loginId);

	List<PicRole> findByCustomerRoleId(Long customerRoleId);
	
	void deleteById(Long picRoleId);
	
	void deleteByLoginIdAndCustomerRole(Long loginId, Long customerRoleId);
	
}
