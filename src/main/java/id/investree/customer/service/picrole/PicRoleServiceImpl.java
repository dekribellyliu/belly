package id.investree.customer.service.picrole;

import id.investree.customer.entity.PicRole;
import id.investree.customer.repository.PicRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PicRoleServiceImpl implements PicRoleService {

	@Autowired
	private PicRoleRepository picRoleRepository;

	@Override
	public List<PicRole> saveAllRole(List<PicRole> list) {
		return picRoleRepository.saveAll(list);
	}

	@Override
	public PicRole saveOrUpdate(PicRole data) {
		return picRoleRepository.save(data);
	}

	@Override
	public List<PicRole> findByCustomerRoleIdAndLoginIdAndRoleIsNotNull(List<Long> customerId, Long loginId) {
		return picRoleRepository.findByCustomerRoleIdInAndLoginIdAndRoleIsNotNullAndDeletedAtIsNull(customerId, loginId).orElse(new ArrayList<>());
	}

	@Override
	public List<PicRole> findByCustomerRoleIdAndLoginIdAndRoleIsNull(List<Long> customerId, Long loginId) {
		return picRoleRepository.findByCustomerRoleIdInAndLoginIdAndRoleIsNullAndDeletedAtIsNull(customerId, loginId).orElse(new ArrayList<>());
	}

	@Override
	public List<PicRole> findByCustomerRoleIdAndLoginId(Long customerRoleId, Long loginId) {
		return picRoleRepository.findByCustomerRoleIdAndLoginId(customerRoleId, loginId).orElse(new ArrayList<>());
	}

	@Override
	public void deleteById(Long picRoleId) {
		picRoleRepository.deleteById(picRoleId);
	}

	@Override
	public void deleteByLoginIdAndCustomerRole(Long loginId, Long customerRoleId) {
		picRoleRepository.deleteByLoginIdAndCustomerRoleId(loginId, customerRoleId);

	}

	@Override
	public List<PicRole> findByCustomerRoleId(Long customerRoleId) {
		return picRoleRepository.findByCustomerRoleId(customerRoleId).orElse(new ArrayList<>());
	}

}
