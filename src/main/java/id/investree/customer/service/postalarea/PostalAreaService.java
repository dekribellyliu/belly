package id.investree.customer.service.postalarea;

import id.investree.core.model.DefaultData;

public interface PostalAreaService {

	DefaultData findProvince(String accessToken, Long id);

	DefaultData findCity(String accessToken, Long id);

	DefaultData findDistrict(String accessToken, Long id);

	DefaultData findVillage(String accessToken, Long id);

	DefaultData checkCity(String accessToken, Long id, Long provinceId);

	DefaultData checkDistrict(String accessToken, Long id, Long cityId);

	DefaultData checkVillage(String accessToken, Long id, Long districtId);
}
