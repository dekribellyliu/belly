package id.investree.customer.service.postalarea;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.investree.core.base.ResultResponse;
import id.investree.core.exception.AppException;
import id.investree.core.json.JsonConverter;
import id.investree.core.model.DefaultData;
import id.investree.core.rest.GenericRestClient;
import id.investree.core.rest.RequestDetails;
import id.investree.core.utils.MessageUtils;
import id.investree.core.utils.Utilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

@Service
public class PostalAreaServiceImpl implements PostalAreaService {

	@Value("${microservice.master-data}")
	private String masterDataUrl;

	@Autowired
	private GenericRestClient genericRestClient;

	@Autowired
	private MessageUtils messageUtils;

	@Autowired
	private JsonConverter jsonConverter;

	@Override
	public DefaultData findProvince(String accessToken, Long id) {
		return requestPostalArea(accessToken,"postal-area/province", id);
	}

	@Override
	public DefaultData findCity(String accessToken, Long id) {
		return requestPostalArea(accessToken,"postal-area/city", id);
	}

	@Override
	public DefaultData findDistrict(String accessToken, Long id) {
		return requestPostalArea(accessToken,"postal-area/district", id);
	}

	@Override
	public DefaultData findVillage(String accessToken, Long id) {
		return requestPostalArea(accessToken,"postal-area/village", id);
	}

	@Override
	public DefaultData checkCity(String accessToken, Long id, Long provinceId) {
		return checkPostalArea(accessToken, "postal-area/city", id, ("provinceId="+provinceId).toString());
	}
	@Override
	public DefaultData checkDistrict(String accessToken, Long id, Long cityId) {
		return checkPostalArea(accessToken, "postal-area/district", id, ("cityId="+cityId).toString());
	}
	@Override
	public DefaultData checkVillage(String accessToken, Long id, Long districtId) {
		return checkPostalArea(accessToken, "postal-area/village", id, ("districtId="+districtId).toString());
	}

	private DefaultData requestPostalArea(String accessToken, String path, Long id) {
		if (Utilities.isNull(id)) {
			return null;
		}
		String url = String.format("%s/%s/%d", masterDataUrl, path, id);

		HttpHeaders headers = new HttpHeaders();
		headers.add("X-Investree-Token", accessToken);

		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(url);
		requestDetails.setRequestType(HttpMethod.GET);
		requestDetails.setHeaders(headers);

		ResultResponse result = genericRestClient.execute(requestDetails, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					throw new AppException(response.meta.message);
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.convertValue(result.data, new TypeReference<DefaultData>() {
		});
	}

	private DefaultData checkPostalArea(String accessToken, String path, Long id, String parentId) {
		if (Utilities.isNull(id)) {
			return null;
		}
		String url = String.format("%s/%s/%d?%s", masterDataUrl, path, id, parentId);

		HttpHeaders headers = new HttpHeaders();
		headers.add("X-Investree-Token", accessToken);

		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(url);
		requestDetails.setRequestType(HttpMethod.GET);
		requestDetails.setHeaders(headers);

		ResultResponse result = genericRestClient.execute(requestDetails, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					throw new AppException(response.meta.message);
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.convertValue(result.data, new TypeReference<DefaultData>() {
		});
	}
}
