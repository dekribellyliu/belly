package id.investree.customer.service.referraldata;

import id.investree.customer.entity.ReferralData;

public interface ReferralDataService {

	ReferralData findByReferralUserId(Long referralUserId);

	ReferralData findByReferrerCode(String referrerCode);

	ReferralData saveOrUpdate(ReferralData data);
}
