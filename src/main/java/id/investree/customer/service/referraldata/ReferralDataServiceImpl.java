package id.investree.customer.service.referraldata;

import id.investree.customer.entity.ReferralData;
import id.investree.customer.repository.ReferralDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReferralDataServiceImpl implements ReferralDataService {

	@Autowired
	private ReferralDataRepository repository;

	@Override
	public ReferralData findByReferralUserId(Long referralUserId) {
		return repository.findByReferralUserId(referralUserId).orElse(null);
	}

	@Override
	public ReferralData findByReferrerCode(String referrerCode) {
		return repository.findByReferrerCodeAndReferralUserIdIsNull(referrerCode).orElse(null);
	}

	@Override
	public ReferralData saveOrUpdate(ReferralData data) {
		return repository.saveAndFlush(data);
	}
}
