package id.investree.customer.service.salestransaction;

import id.investree.customer.entity.SalesTransaction;

import java.util.Date;
import java.util.List;

public interface SalesTransactionService {

	SalesTransaction findById(Long id);

	List<SalesTransaction> findByCustomerId(Long customerId);

	SalesTransaction findByCustomerIdAndDate(Long customerId, Date date);

	SalesTransaction saveOrUpdate(SalesTransaction salesTransactionRequest);

	SalesTransaction findByIdAndCustomerId(Long id, Long customerId);

	Long countCustomerId(Long customerId);
}
