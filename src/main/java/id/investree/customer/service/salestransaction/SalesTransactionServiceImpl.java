package id.investree.customer.service.salestransaction;

import id.investree.customer.entity.SalesTransaction;
import id.investree.customer.repository.SalesTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class SalesTransactionServiceImpl implements SalesTransactionService {

	@Autowired
	private SalesTransactionRepository salesTransactionRepository;

	@Override
	public SalesTransaction findById(Long id) {
		return salesTransactionRepository.findByIdAndDeletedAtIsNull(id).orElse(null);
	}

	@Override
	public List<SalesTransaction> findByCustomerId(Long customerId) {
		return salesTransactionRepository.findByCustomerIdAndDeletedAtIsNull(customerId).orElse(new ArrayList<>());
	}

	@Override
	public SalesTransaction findByCustomerIdAndDate(Long customerId, Date date) {
		return salesTransactionRepository.findByCustomerIdAndDateAndDeletedAtIsNull(customerId, date).orElse(null);
	}

	@Override
	@Transactional
	public SalesTransaction saveOrUpdate(SalesTransaction salesTransactionData) {
		return salesTransactionRepository.saveAndFlush(salesTransactionData);
	}

	@Override
	public SalesTransaction findByIdAndCustomerId(Long id, Long customerId) {
		return salesTransactionRepository.findByIdAndCustomerIdAndDeletedAtIsNull(id, customerId).orElse(null);
	}

	@Override
	public Long countCustomerId(Long customerId) {
		return salesTransactionRepository.countCustomerIdAndDeletedAtIsNull(customerId);
	}


}
