package id.investree.customer.service.search;

import id.investree.customer.model.data.KbliData;

public interface SearchService {

	KbliData findByCode(String code, String accessToken);
}
