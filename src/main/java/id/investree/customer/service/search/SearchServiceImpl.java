package id.investree.customer.service.search;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import id.investree.core.json.JsonConverter;
import id.investree.core.rest.GenericRestClient;
import id.investree.core.rest.RequestDetails;
import id.investree.core.utils.MessageUtils;
import id.investree.customer.model.data.KbliData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

@Service
public class SearchServiceImpl implements SearchService {

	@Value("${microservice.user-search}")
	private String searchUserUrl;

	@Autowired
	private GenericRestClient genericRestClient;

	@Autowired
	private MessageUtils messageUtils;

	@Autowired
	private JsonConverter jsonConverter;

	@Override
	public KbliData findByCode(String code, String accessToken) {
		HttpHeaders headers = new HttpHeaders();
		headers.add(GlobalConstants.HEADER_TOKEN, accessToken);

		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(searchUserUrl + "/kbli/" + code);
		requestDetails.setRequestType(HttpMethod.GET);
		requestDetails.setHeaders(headers);

		ResultResponse result = genericRestClient.execute(requestDetails, null, ResultResponse.class);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.convertValue(result.data, new TypeReference<KbliData>() {
		});
	}
}
