package id.investree.customer.service.shareholdersinformation;

import id.investree.customer.entity.ShareholdersInformation;

import java.util.List;

public interface ShareholdersInformationService {

	List<ShareholdersInformation> findAllByCustomerId(Long customerId);

	ShareholdersInformation findById(Long id);

	ShareholdersInformation findByIdOrCreate(Long id);

	List<ShareholdersInformation> findByTaxNumber(String taxNumber);

	ShareholdersInformation findByIdAndCustomerId(Long id, Long customerId);

	List<ShareholdersInformation> findByCustomerIdAndNotDeleted(Long customerId);

	List<ShareholdersInformation> findByCustomerIdAndDeleted(Long customerId);

	List<ShareholdersInformation> internalFindByCustomerIdAndNotDeleted(Long customerId, String accessToken);

	ShareholdersInformation saveOrUpdate(ShareholdersInformation shareholderData);

}
