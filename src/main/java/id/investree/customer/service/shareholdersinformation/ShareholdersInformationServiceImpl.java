package id.investree.customer.service.shareholdersinformation;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.investree.core.base.ResultResponse;
import id.investree.core.exception.AppException;
import id.investree.core.json.JsonConverter;
import id.investree.core.rest.GenericRestClient;
import id.investree.core.rest.RequestDetails;
import id.investree.customer.entity.ShareholdersInformation;
import id.investree.customer.repository.ShareholdersInformationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ShareholdersInformationServiceImpl implements ShareholdersInformationService {

	@Value("${microservice.shareholder}")
	private String shareholderUrl;

	@Autowired
	private GenericRestClient genericRestClient;

	@Autowired
	private JsonConverter jsonConverter;

	@Autowired
	private ShareholdersInformationRepository shareholdersInformationRepository;

	@Override
	public List<ShareholdersInformation> findAllByCustomerId(Long customerId) {
		return shareholdersInformationRepository.findAllByCustomerId(customerId).orElse(new ArrayList<>());
	}

	@Override
	public ShareholdersInformation findById(Long id) {
		return shareholdersInformationRepository.findByIdAndDeletedAtIsNull(id).orElse(null);
	}

	@Override
	public ShareholdersInformation findByIdOrCreate(Long id) {
		return shareholdersInformationRepository.findByIdAndDeletedAtIsNull(id).orElse(new ShareholdersInformation());
	}

	@Override
	public List<ShareholdersInformation> findByTaxNumber(String taxNumber) {
		return shareholdersInformationRepository.findByTaxCardNumberAndDeletedAtIsNull(taxNumber).orElse(new ArrayList<>());
	}

	@Override
	public ShareholdersInformation findByIdAndCustomerId(Long id, Long customerId) {
		return shareholdersInformationRepository.findByIdAndCustomerIdAndDeletedAtIsNull(id, customerId).orElse(null);
	}

	@Override
	public List<ShareholdersInformation> findByCustomerIdAndNotDeleted(Long customerId) {
		return shareholdersInformationRepository.findByCustomerIdAndDeletedAtIsNull(customerId).orElse(new ArrayList<>());
	}

	@Override
	public List<ShareholdersInformation> findByCustomerIdAndDeleted(Long customerId) {
		return shareholdersInformationRepository.findByCustomerIdAndDeletedAtIsNotNull(customerId).orElse(new ArrayList<>());
	}

	@Override
	public List<ShareholdersInformation> internalFindByCustomerIdAndNotDeleted(Long customerId, String accessToken) {

		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Investree-Token", accessToken);

		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(shareholderUrl + "/internal/shareholder-info/" + customerId);
		requestDetails.setRequestType(HttpMethod.GET);
		requestDetails.setHeaders(headers);

		ResultResponse result = genericRestClient.execute(requestDetails, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					throw new AppException(response.meta.message);
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.convertValue(result.data, new TypeReference<List<ShareholdersInformation>>() {
		});
	}

	@Override
	@Transactional
	public ShareholdersInformation saveOrUpdate(ShareholdersInformation shareholderData) {
		return shareholdersInformationRepository.saveAndFlush(shareholderData);
	}

}
