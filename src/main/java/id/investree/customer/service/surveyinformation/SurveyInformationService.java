package id.investree.customer.service.surveyinformation;

import id.investree.core.model.DefaultData;
import id.investree.customer.entity.SurveyInformation;

import java.util.List;

public interface SurveyInformationService {
	SurveyInformation findByIdAndCustomerId(Long id, Long customerId);

	SurveyInformation findLastUpdateByCustomerId(Long customerId);

	SurveyInformation findSurveyInformationById(Long id);

	List<SurveyInformation> findAllSurveyByCustomerId(Long customerId);

	List<SurveyInformation> internalFindAllSurveyByCustomerId(Long customerId, String accessToken);

	SurveyInformation saveOrUpdate(SurveyInformation request);

	List<SurveyInformation> saveAll(List<SurveyInformation> data);

	DefaultData getSurveyOfficeStatus(Long id, String accessToken);
}
