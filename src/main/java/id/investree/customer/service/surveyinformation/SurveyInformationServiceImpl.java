package id.investree.customer.service.surveyinformation;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.investree.core.base.ResultResponse;
import id.investree.core.exception.AppException;
import id.investree.core.json.JsonConverter;
import id.investree.core.model.DefaultData;
import id.investree.core.rest.GenericRestClient;
import id.investree.core.rest.RequestDetails;
import id.investree.customer.entity.SurveyInformation;
import id.investree.customer.repository.SurveyInformationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class SurveyInformationServiceImpl implements SurveyInformationService {

	@Value("${microservice.master-data}")
	private String masterDataUrl;

	@Value("${microservice.survey}")
	private String surveyUrl;

	@Autowired
	private SurveyInformationRepository repository;

	@Autowired
	private GenericRestClient genericRestClient;

	@Autowired
	private JsonConverter jsonConverter;

	@Override
	public SurveyInformation findByIdAndCustomerId(Long id, Long customerId) {
		return repository.findByIdAndCustomerIdAndDeletedAtIsNull(id, customerId).orElse(null);
	}

	@Override
	public SurveyInformation findLastUpdateByCustomerId(Long customerId) {
		return repository.findFirstByCustomerIdAndDeletedAtIsNullOrderBySurveyDateDescUpdateAtDescIdDesc(customerId).orElse(null);
	}

	@Override
	public SurveyInformation findSurveyInformationById(Long id) {
		return repository.findByIdAndDeletedAtIsNull(id).orElse(null);
	}

	@Override
	public List<SurveyInformation> findAllSurveyByCustomerId(Long customerId) {
		return repository.findByCustomerIdAndDeletedAtIsNull(customerId).orElse(new ArrayList<>());
	}



	@Override
	public List<SurveyInformation> internalFindAllSurveyByCustomerId(Long customerId, String accessToken) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Investree-Token", accessToken);

		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(surveyUrl + "/internal/survey-info/" + customerId);
		requestDetails.setRequestType(HttpMethod.GET);
		requestDetails.setHeaders(headers);

		ResultResponse result = genericRestClient.execute(requestDetails, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					throw new AppException(response.meta.message);
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.convertValue(result.data, new TypeReference<List<SurveyInformation>>() {
		});
	}

	@Override
	public SurveyInformation saveOrUpdate(SurveyInformation request) {
		return repository.save(request);
	}

	@Override
	@Transactional
	public List<SurveyInformation> saveAll(List<SurveyInformation> data) {
		List<SurveyInformation> surveyInformations = repository.saveAll(data);
		repository.flush();
		return surveyInformations;
	}

	@Override
	public DefaultData getSurveyOfficeStatus(Long id, String accessToken) {

		HttpHeaders headers = new HttpHeaders();
		headers.set("X-Investree-Token", accessToken);

		RequestDetails requestDetails = new RequestDetails();
		requestDetails.setUrl(masterDataUrl + "/office-status/" + id);
		requestDetails.setRequestType(HttpMethod.GET);
		requestDetails.setHeaders(headers);

		ResultResponse result = genericRestClient.execute(requestDetails, new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
				return clientHttpResponse.getStatusCode().isError();
			}

			@Override
			public void handleError(ClientHttpResponse clientHttpResponse) {
				try {
					ResultResponse response = jsonConverter.mapFromJson(clientHttpResponse.getBody(), ResultResponse.class);
					throw new AppException(response.meta.message);
				} catch (IOException e) {
					throw new AppException(e.getMessage());
				}
			}
		}, ResultResponse.class);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.convertValue(result.data, new TypeReference<DefaultData>() {
		});
	}

}
