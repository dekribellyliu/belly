package id.investree.customer.service.userdata;

import id.investree.customer.entity.UserData;

import java.util.List;

public interface UserDataService {

	UserData saveOrUpdate(UserData data);

	UserData findByLoginId(Long loginId);

	List<UserData> findAllByCustomerId(Long customerId);

	UserData findByCustomerId(Long customerId);

	UserData findByIdCardNumber(String idCardNumber);

	void hardDeleteByCustomerId(Long customerId);
}
