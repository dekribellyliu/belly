package id.investree.customer.service.userdata;

import id.investree.customer.entity.UserData;
import id.investree.customer.repository.UserDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDataServiceImpl implements UserDataService {

	@Autowired
	private UserDataRepository repository;

	@Override
	@Transactional
	public UserData saveOrUpdate(UserData data) {
		return repository.save(data);
	}

	@Override
	public UserData findByLoginId(Long loginId) {
		return repository.findByLoginDataId(loginId).orElse(null);
	}

	@Override
	public List<UserData> findAllByCustomerId(Long customerId) {
		return repository.findByCustomerId(customerId).orElse(new ArrayList<>());
	}

	@Override
	public UserData findByCustomerId(Long customerId) {
		List<UserData> userData = repository.findByCustomerId(customerId).orElse(new ArrayList<>());
		if (userData.size() == 0) {
			return null;
		}
		return userData.get(0);
	}

	@Override
	public UserData findByIdCardNumber(String idCardNumber) {
		return repository.findByIdCardNumber(idCardNumber).orElse(null);
	}

	@Override
	public void hardDeleteByCustomerId(Long customerId) {
		repository.deleteByCustomerId(customerId);
	}
}
