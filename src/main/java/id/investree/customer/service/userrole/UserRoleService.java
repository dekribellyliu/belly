package id.investree.customer.service.userrole;

import id.investree.customer.entity.UserRole;

public interface UserRoleService {

	UserRole findByLdIdAndMurId(Long ldId);
}
