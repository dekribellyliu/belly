package id.investree.customer.service.userrole;

import id.investree.customer.entity.UserRole;
import id.investree.customer.repository.UserRoleRepository;
import id.investree.customer.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRoleServiceImpl implements UserRoleService {

	@Autowired
	private UserRoleRepository repository;

	@Override
	public UserRole findByLdIdAndMurId(Long ldId) {
		return repository.findByLdIdAndMurId(ldId, Constants.USER_ROLE_RELATIONSHIP_MANAGER).orElse(null);
	}
}
