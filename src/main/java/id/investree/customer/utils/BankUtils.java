package id.investree.customer.utils;

import id.investree.customer.model.request.BankAccountValidationRequest;
import id.investree.customer.model.request.BankInformationRequest;
import id.investree.customer.service.integration.IntegrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BankUtils {

	@Autowired
	private IntegrationService integrationService;

	public Boolean isValidBcaAccount(BankInformationRequest bankInformationRequest) {
		String accountNumber = bankInformationRequest.getBankAccountNumber();
		String accountName = bankInformationRequest.getBankAccountHolderName();

		BankAccountValidationRequest validationRequest = new BankAccountValidationRequest(accountName, accountNumber);
		return integrationService.checkBankAccount(validationRequest);
	}

}
