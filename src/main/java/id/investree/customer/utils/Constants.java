package id.investree.customer.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Constants {

	public static final Long EXISTING_BORROWER = 2L;

	public static final Integer BANK_TYPE_ALL = -1;
	public static final Integer BANK_TYPE_VA = 33;
	public static final Integer BANK_TYPE_ACCOUNT = 29;
	public static final Integer BANK_ACCOUNT_USED_DISBURSEMENT_COUNT = 1;
	public static final Long BANK_CENTRAL_ASIA_ID = 2L;

	public static final Integer LEGAL_INFO_NPWP = 4;
	public static final String LEGAL_INFO_NPWP_NAME = "NPWP";

	public static final Integer LEGAL_INFO_SIUP = 5;
	public static final String LEGAL_INFO_SIUP_NAME = "SIUP";

	public static final Integer LEGAL_INFO_AKTA_PENDIRIAN = 7;
	public static final String LEGAL_INFO_AKTA_PENDIRIAN_NAME = "Akta Pendirian";

	public static final Integer LEGAL_INFO_AKTA_TERBARU = 9;
	public static final String LEGAL_INFO_AKTA_TERBARU_NAME = "Akta Terbaru";

	public static final Integer LEGAL_INFO_SK_MENKUHAM = 8;
	public static final String LEGAL_INFO_SK_MENKUHAM_NAME = "MENKUHAM";

	public static final Integer LEGAL_INFO_SK_TDP = 6;
	public static final String LEGAL_INFO_SK_TDP_NAME = "TDP";

	public static final Integer LEGAL_INFO_SK_SKDU = 28;
	public static final String LEGAL_INFO_SK_SKDU_NAME = "SKDU";

	public static final Integer LEGAL_INFO_DGT = 44;
	public static final String LEGAL_INFO_DGT_NAME = "Directorate General Of Taxes";

	public static final Integer LEGAL_INFO_ARTICLE_ASSOCIATION = 45;
	public static final String LEGAL_INFO_ARTICLE_ASSOCIATION_NAME = "Article of Association";

	public static final Integer LEGAL_INFO_CERTIFICATE_INCUMBENCY = 46;
	public static final String LEGAL_INFO_CERTIFICATE_INCUMBENCY_NAME = "Certificate of Incumbency";

	public static final Integer FINANCIAL_ALL_STATEMENT = -1;
	public static final Integer FINANCIAL_STATEMENT = 10;
	public static final Integer FINANCIAL__E_STATEMENT = 30;

	public static final int IDENTITY_CARD_NUMBER_LENGTH = 16;

	public static final Integer RESPONSE_BANK_ACCOUNT_IS_VALID = 200;
	public static final String RESPONSE_BANK_ACCOUNT_CODE_KEY = "code";

	public static final String GLOBAL_PARAM_PIC_ROLE = "mr_pjr_anchor";
	public static final String GLOBAL_PARAM_INCOME_LEVEL = "mr_income_level";
	public static final String GLOBAL_PARAM_SOURCE_OF_FUND = "mr_ci_source_of_fund";

	public static final String USER_FEMALE_GENDER_TYPE = "F";
	public static final String USER_FEMALE_GENDER = "Female";
	public static final String USER_MALE_GENDER = "Male";

	public static final String IS_OBJECT_TRUE = "1";
	public static final Long USER_ROLE_RELATIONSHIP_MANAGER = 95L;
	public static final Long NATIONALITY_INDONESIA = 104L;

	private static ArrayList<Integer> legalInfoTypes = new ArrayList<Integer>(
		Arrays.asList(Constants.LEGAL_INFO_NPWP,
			Constants.LEGAL_INFO_SIUP,
			Constants.LEGAL_INFO_AKTA_PENDIRIAN,
			Constants.LEGAL_INFO_AKTA_TERBARU,
			Constants.LEGAL_INFO_SK_MENKUHAM,
			Constants.LEGAL_INFO_SK_TDP,
			Constants.LEGAL_INFO_SK_SKDU,
			Constants.LEGAL_INFO_DGT,
			Constants.LEGAL_INFO_ARTICLE_ASSOCIATION,
			Constants.LEGAL_INFO_CERTIFICATE_INCUMBENCY
		));

	public static ArrayList<Integer> getLegalInfoTypes() {
		return legalInfoTypes;
	}

	public static final Long STATUS_PRE_REGISTERED = 0L;
	public static final Long STATUS_ACTIVE = 4L;
	public static final Long STATUS_PENDING_VERIFICATION = 3L;
	public static final Long STATUS_INACTIVE = 6L;
	public static final String GLOBAL_PARAM_APPT_RESULT = "mr_appt_result";

	public static final String FILE_EXT_GIF = ".gif";

	public static final String USER_PREFERENCE_SHARIA_NAME = "SHARIA";
	public static final String USER_PREFERENCE_CONVENTIONAL_NAME = "CONVENTIONAL";
	public static final String USER_PREFERENCE_ALL_NAME = "ALL";

	public static final Integer USER_PREFERENCE_OSF = 1;
	public static final Integer USER_PREFERENCE_PROJECT_FINANCING = 2;
	public static final Integer USER_PREFERENCE_WCTL = 3;

	public static final String USER_PREFERENCE_OSF_NAME = "OSF";
	public static final String USER_PREFERENCE_PROJECT_FINANCING_NAME = "PROJECT FINANCING";
	public static final String USER_PREFERENCE_WCTL_NAME = "WCTL";

	public static final Integer PROJECT_FINANCING_INVOICE = 1;
	public static final Integer PROJECT_FINANCING_PRE_INVOICE = 38;
	public static final Integer PROJECT_FINANCING_BUYER = 15;

	public static final Integer WCTL_CBF = 42;
	public static final Integer WCTL_RTL = 41;
	public static final Integer WCTL_MCA = 40;

	public static final Integer OSF_PRODUCT = 13;
	public static final String VA_STATUS_VERIFIED = "Verified";
	public static final String VA_STATUS_UNVERIFIED = "Unverified";

	public static final Long PARTNERSHIP_ECOMMERCE_TYPE = 3L;

	public static final Long EXT_POB_ID = 99999L;
	public static final Long INVAR_BANK_TYPE_ALL = 3L;

	public static final Long INVAR_BANK_ID_DANAMON_LENDER = 1L;
	public static final Long INVAR_BANK_ID_CIMB = 2L;
	public static final Long INVAR_BANK_ID_CIMB_SYARIAH = 3L;
	public static final Long INVAR_BANK_ID_DANAMON_SYARIAH = 4L;
	public static final Long INVAR_BANK_ID_DANAMON_BORROWER = 5L;

	private static final Long[] INVAR_BANK_LENDER = {
		Constants.INVAR_BANK_ID_DANAMON_LENDER,
		Constants.INVAR_BANK_ID_CIMB,
		Constants.INVAR_BANK_ID_CIMB_SYARIAH,
		Constants.INVAR_BANK_ID_DANAMON_SYARIAH
	};

	private static final Integer[] GROUP_PRODUCT_FINANCING = {
			Constants.PROJECT_FINANCING_BUYER,
			Constants.PROJECT_FINANCING_INVOICE,
			Constants.PROJECT_FINANCING_PRE_INVOICE
	};

	private static final Integer[] GROUP_PRODUCT_WCTL = {
			Constants.WCTL_MCA,
			Constants.WCTL_CBF,
			Constants.WCTL_RTL
	};

	public static Long[] getInvarBankLender() {
		return INVAR_BANK_LENDER;
	}

	private static final Long[] INVAR_BANK_BORROWER = {
		Constants.INVAR_BANK_ID_DANAMON_SYARIAH,
		Constants.INVAR_BANK_ID_DANAMON_BORROWER
	};

	public static Long[] getInvarBankBorrower() {
		return INVAR_BANK_BORROWER;
	}

	public static List<Integer> getGroupProductFinancing() {
		return Arrays.asList(GROUP_PRODUCT_FINANCING);
	}

	public static List<Integer> getGroupProductWctl() {
		return Arrays.asList(GROUP_PRODUCT_WCTL);
	}

}
