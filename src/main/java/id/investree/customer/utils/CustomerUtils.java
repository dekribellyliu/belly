package id.investree.customer.utils;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.utils.Utilities;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Component
public class CustomerUtils {

	public Integer getAge(Date dateOfBirth) {
		if (Utilities.isNull(dateOfBirth)) {
			return 0;
		}
		Date now = new Date();

		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		int dateOfBirthInMilis = Integer.parseInt(dateFormat.format(dateOfBirth));
		int nowInMilis = Integer.parseInt(dateFormat.format(now));

		return (nowInMilis - dateOfBirthInMilis) / 10000;
	}

	public boolean isDateAfterSeventeenYearsAgo(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, -17);
		Date dateSeventeenYearAgo = cal.getTime();

		return date.before(dateSeventeenYearAgo);
	}

	public Integer getUserTypeIdentification(String userType) {
		if (GlobalConstants.USER_CATEGORY_BORROWER_NAME.equalsIgnoreCase(userType)) {
			return GlobalConstants.USER_CATEGORY_BORROWER_ID;
		} else if (GlobalConstants.USER_CATEGORY_LENDER_NAME.equalsIgnoreCase(userType)) {
			return GlobalConstants.USER_CATEGORY_LENDER_ID;
		} else {
			return GlobalConstants.USER_CATEGORY_ANCHOR_ID;
		}
	}
}
