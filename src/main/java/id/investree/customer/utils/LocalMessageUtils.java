package id.investree.customer.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public class LocalMessageUtils {

    @Autowired
    private MessageSource messageSource;

    public String statusRestricted() {
        return messageSource.getMessage("error.status.restricted", null, LocaleContextHolder.getLocale());
    }

    public String insertSuccess() {
        return messageSource.getMessage("insert.success", null, LocaleContextHolder.getLocale());
    }

    public String insertFailed(String name) {
        String message = messageSource.getMessage("insert.failed", null, LocaleContextHolder.getLocale());
        return String.format(message, name);
    }

    public String updateSuccess() {
        return messageSource.getMessage("update.success", null, LocaleContextHolder.getLocale());
    }

    public String updateFailed() {
        return messageSource.getMessage("update.failed", null, LocaleContextHolder.getLocale());
    }

    public String dataFetched() {
        return messageSource.getMessage("data.fetched", null, LocaleContextHolder.getLocale());
    }

    public String emptyToken() {
        return messageSource.getMessage("error.token.empty", null, LocaleContextHolder.getLocale());
    }

    public String dataNotValid(String name) {
        String message = messageSource.getMessage("data.not.valid", null, LocaleContextHolder.getLocale());
        return String.format(message, name);
    }

    public String nameOnlyAlphabet() {
        return messageSource.getMessage("error.fullname.alphabetonly", null, LocaleContextHolder.getLocale());
    }

    public String identityCardNumberDuplicate() {
        return messageSource.getMessage("error.identity.number.duplicate", null, LocaleContextHolder.getLocale());
    }

    public String identityCardNumberLength() {
        return messageSource.getMessage("error.identity.number.length", null, LocaleContextHolder.getLocale());
    }

    public String mobileNumberLength() {
        return messageSource.getMessage("error.mobile.number.length", null, LocaleContextHolder.getLocale());
    }

    public String deleteSuccess() {
        return messageSource.getMessage("delete.success", null, LocaleContextHolder.getLocale());
    }

    public String deleteFailed() {
        return messageSource.getMessage("delete.failed", null, LocaleContextHolder.getLocale());
    }

    public String identityCardExpiry() {
        return messageSource.getMessage("error.expirydate.less", null, LocaleContextHolder.getLocale());
    }

    public String dateMustBeOverSeventeenYears(String field) {
        String message = messageSource.getMessage("error.less.seventeen.years", null, LocaleContextHolder.getLocale());
        return String.format(message, field);
    }

    public String statementDate() {
        return messageSource.getMessage("error.statementdate.less", null, LocaleContextHolder.getLocale());
    }

    public String statementDateMinimunYear() {
        return messageSource.getMessage("error.statementdate.year", null, LocaleContextHolder.getLocale());
    }

    public String errorSelectedBankAccountForDisbursement() {
        return messageSource.getMessage("error.disbursement.account.must.one", null, LocaleContextHolder.getLocale());
    }

    public String errorSelectedBankAccountForWithdrawal() {
        return messageSource.getMessage("error.withdrawal.account.must.one", null, LocaleContextHolder.getLocale());
    }

    public String errorBankAccountNumberShouldContainsNumber() {
        return messageSource.getMessage("error.bankaccountnumber.number", null, LocaleContextHolder.getLocale());
    }

    public String errorNumberOnly(String field) {
        String message = messageSource.getMessage("error.numberonly", null, LocaleContextHolder.getLocale());
        return String.format(message, field);
    }

    public String errorBlank(String field) {
        String message = messageSource.getMessage("error.blank", null, LocaleContextHolder.getLocale());
        return String.format(message, field);
    }

    public String errorDocumentNotHaveExpireDate(String field) {
        String message = messageSource.getMessage("error.doc.nothave.exp", null, LocaleContextHolder.getLocale());
        return String.format(message, field);
    }

    public String errorNpwpNotValidDigit() {
        return messageSource.getMessage("error.npwp.exact.digit", null, LocaleContextHolder.getLocale());
    }

    public String lessLandLine() {
        return messageSource.getMessage("error.landline.less", null, LocaleContextHolder.getLocale());
    }

    public String moreDate(String field) {
        String message = messageSource.getMessage("error.more.date", null, LocaleContextHolder.getLocale());
        return String.format(message, field);
    }

    public String errorPostalCode() {
        return messageSource.getMessage("error.postalcode.less", null, LocaleContextHolder.getLocale());
    }

    public String errorExpiredDocument() {
        return messageSource.getMessage("error.doc.has.expired", null, LocaleContextHolder.getLocale());
    }

    public String lessDate(String field) {
        String message = messageSource.getMessage("error.date.less", null, LocaleContextHolder.getLocale());
        return String.format(message, field);
    }

    public String emptySection(String field) {
        String message = messageSource.getMessage("error.section.empty", null, LocaleContextHolder.getLocale());
        return String.format(message, field);
    }

    public String notCompletedSection(String field) {
        String message = messageSource.getMessage("error.section.not.completed", null, LocaleContextHolder.getLocale());
        return String.format(message, field);
    }

    public String specificBankAcccountNotValid(String accountNumber, String accountName) {
        String message = messageSource.getMessage("error.specific.bank.account.is.not.valid", null, LocaleContextHolder.getLocale());
        return String.format(message, accountNumber, accountName);
    }

    public String picBorrowerEmpty() {
        return messageSource.getMessage("error.pic.borrower.empty", null, LocaleContextHolder.getLocale());
    }

    public String errorFailCreateVa() {
        return messageSource.getMessage("error.fail.createva", null, LocaleContextHolder.getLocale());
    }
}
