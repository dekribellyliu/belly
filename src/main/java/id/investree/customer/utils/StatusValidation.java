package id.investree.customer.utils;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.TokenPayload;
import id.investree.customer.entity.CustomerRole;
import id.investree.customer.service.customerrole.CustomerRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class StatusValidation {

	@Autowired
	private CustomerRoleService customerRoleService;

	@Autowired
	private LocalMessageUtils messageUtils;

	public void statusRestrictionValidation(TokenPayload tokenPayload, Integer userRoleType) {
		if (GlobalConstants.USER_TYPE_FRONTOFFICE_ID.equals(tokenPayload.getUserType())) {
			CustomerRole checkingStatus = customerRoleService.findByCustomerIdAndType(Long.valueOf(tokenPayload.getCustomerId()), userRoleType);
			if (!Optional.ofNullable(checkingStatus).isPresent()) {
				throw new DataNotFoundException();
			}

			if (Constants.STATUS_ACTIVE.equals(checkingStatus.getStatus())
				|| Constants.STATUS_PRE_REGISTERED.equals(checkingStatus.getStatus())
				|| Constants.STATUS_PENDING_VERIFICATION.equals(checkingStatus.getStatus())
				|| Constants.STATUS_INACTIVE.equals(checkingStatus.getStatus())) {
				throw new AppException(messageUtils.statusRestricted());
			}
		}
	}
}
