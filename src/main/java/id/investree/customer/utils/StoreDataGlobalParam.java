package id.investree.customer.utils;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.integration.globalparam.GlobalParamUtilities;
import id.investree.core.model.DefaultData;
import id.investree.core.model.response.GlobalParamResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class StoreDataGlobalParam {

	@Value("${microservice.master-data}")
	private String masterDataUrl;

	@Autowired
	private GlobalParamUtilities globalParamUtilities;

	private List<DefaultData> nationalityList = new ArrayList<>();
	private List<DefaultData> positionList = new ArrayList<>();
	private List<DefaultData> divisionList = new ArrayList<>();
	private List<DefaultData> registrationStatusList = new ArrayList<>();
	private List<DefaultData> mobilePrefixList = new ArrayList<>();
	private List<DefaultData> maritalStatusList = new ArrayList<>();
	private List<DefaultData> occupationList = new ArrayList<>();
	private List<DefaultData> educationList = new ArrayList<>();
	private List<DefaultData> legalList = new ArrayList<>();
	private List<DefaultData> religionList = new ArrayList<>();
	private List<DefaultData> incomeLevelList = new ArrayList<>();
	private List<DefaultData> sourceOfFundList = new ArrayList<>();
	private List<DefaultData> relationshipList = new ArrayList<>();
	private List<DefaultData> kifList = new ArrayList<>();
	private List<DefaultData> picRoleList = new ArrayList<>();
	private List<DefaultData> apptResultList = new ArrayList<>();

	public void getGlobalParam(String... slugs) {
		List<GlobalParamResponse> globalParamResponses = globalParamUtilities.findByGlobalParam(masterDataUrl, Arrays.asList(slugs), "ID", GlobalConstants.IGNORED);
		globalParamResponses.forEach(globalParam -> {
			List<DefaultData> rawData = globalParamUtilities.parseDataBySlug(globalParamResponses, globalParam.getSlug());
			switch (globalParam.getSlug()) {
				case GlobalConstants.GLOBAL_PARAM_NATIONALITY: {
					nationalityList = rawData;
					break;
				}
				case GlobalConstants.GLOBAL_PARAM_POSITION: {
					positionList = rawData;
					break;
				}
				case GlobalConstants.GLOBAL_PARAM_DIVISION: {
					divisionList = rawData;
					break;
				}
				case GlobalConstants.GLOBAL_PARAM_REGISTRATION_STATUS: {
					registrationStatusList = rawData;
					break;
				}
				case GlobalConstants.GLOBAL_PARAM_MOBILE_PREFIX: {
					mobilePrefixList = rawData;
					break;
				}
				case GlobalConstants.GLOBAL_PARAM_MARITAL_STATUS: {
					maritalStatusList = rawData;
					break;
				}
				case GlobalConstants.GLOBAL_PARAM_OCCUPATION: {
					occupationList = rawData;
					break;
				}
				case GlobalConstants.GLOBAL_PARAM_EDUCATION: {
					educationList = rawData;
					break;
				}
				case GlobalConstants.GLOBAL_PARAM_LEGAL_ENTITY: {
					legalList = rawData;
					break;
				}
				case GlobalConstants.GLOBAL_PARAM_RELIGION: {
					religionList = rawData;
					break;
				}
				case Constants.GLOBAL_PARAM_INCOME_LEVEL: {
					incomeLevelList = rawData;
					break;
				}
				case Constants.GLOBAL_PARAM_SOURCE_OF_FUND: {
					sourceOfFundList = rawData;
					break;
				}
				case GlobalConstants.GLOBAL_PARAM_RELATIONSHIP: {
					relationshipList = rawData;
					break;
				}
				case GlobalConstants.GLOBAL_PARAM_KNOW_INVESTREE_FROM: {
					kifList = rawData;
					break;
				}
				case Constants.GLOBAL_PARAM_PIC_ROLE: {
					picRoleList = rawData;
					break;
				}
				case Constants.GLOBAL_PARAM_APPT_RESULT: {
					apptResultList = rawData;
					break;
				}
			}
		});
	}

	public List<DefaultData> getNationalityList() {
		return nationalityList;
	}

	public List<DefaultData> getPositionList() {
		return positionList;
	}

	public List<DefaultData> getDivisionList() {
		return divisionList;
	}

	public List<DefaultData> getRegistrationStatusList() {
		return registrationStatusList;
	}

	public List<DefaultData> getMobilePrefixList() {
		return mobilePrefixList;
	}

	public List<DefaultData> getMaritalStatusList() {
		return maritalStatusList;
	}

	public List<DefaultData> getOccupationList() {
		return occupationList;
	}

	public List<DefaultData> getEducationList() {
		return educationList;
	}

	public List<DefaultData> getLegalList() {
		return legalList;
	}

	public List<DefaultData> getReligionList() {
		return religionList;
	}

	public List<DefaultData> getIncomeLevelList() {
		return incomeLevelList;
	}

	public List<DefaultData> getSourceOfFundList() {
		return sourceOfFundList;
	}

	public List<DefaultData> getRelationshipList() {
		return relationshipList;
	}

	public List<DefaultData> getKifList() {
		return kifList;
	}

	public List<DefaultData> getPicRoleList() {
		return picRoleList;
	}

	public List<DefaultData> getApptResultList() {
		return apptResultList;
	}
}
