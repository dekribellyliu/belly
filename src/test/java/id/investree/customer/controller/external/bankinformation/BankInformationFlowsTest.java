package id.investree.customer.controller.external.bankinformation;

import id.investree.core.constant.GlobalConstants;
import id.investree.customer.model.request.BankInformationRequest;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BankInformationFlowsTest {

	@Autowired
	private BankInformationFlows flows;

	private BankInformationRequest request;
	private Long bankInformationId;
	private Long customerId;
	private Long masterBankId;
	private String bankAccountCoverFile;
	private String bankAccountNumber;
	private String bankAccountHolderName;
	private Boolean useAsDisbursement;
	private Boolean useAsWithdrawal;

	@Before
	public void init() {
		request = new BankInformationRequest();

		bankInformationId = -999L;
		customerId = -999L;
		masterBankId = 1L;
		bankAccountCoverFile = "lorem_ipsum.jpg";
		bankAccountNumber = "999999999";
		bankAccountHolderName = "LOREM IPSUM";
		useAsDisbursement = true;
		useAsWithdrawal = false;
	}

	@Test
	public void whenCreationBulkNewBankInfo_thenReturnValue() {
		request.setBankInformationId(bankInformationId);
		request.setCustomerId(customerId);
		request.setMasterBankId(masterBankId);
		request.setBankAccountCoverFile(bankAccountCoverFile);
		request.setBankAccountNumber(bankAccountNumber);
		request.setBankAccountHolderName(bankAccountHolderName);
		request.setUseAsDisbursement(useAsDisbursement);
		request.setUseAsWithdrawal(useAsWithdrawal);

		List<BankInformationRequest> requestList = new ArrayList<>();
		requestList.add(request);

		Assertions.assertThat(flows.bankCreationBulk(requestList, GlobalConstants.USER_CATEGORY_BORROWER_ID).size()).isEqualTo(requestList.size());
		Assertions.assertThat(flows.bankCreationBulk(requestList, GlobalConstants.USER_CATEGORY_BORROWER_ID).get(0)).isEqualToComparingFieldByField(requestList.get(0));
	}
}