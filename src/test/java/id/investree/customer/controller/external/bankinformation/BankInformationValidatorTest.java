package id.investree.customer.controller.external.bankinformation;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BankInformationValidatorTest {

	@Autowired
	private BankInformationValidator validator;

	@Test
	public void whenSetAccountNumberValid_thenReturnValue() {
		String accountNumber = "99999999";

		boolean isValid = validator.isBankAccountNumberValid(accountNumber);

		Assertions.assertThat(isValid).isTrue();
	}

	@Test
	public void whenSetAccountNumberInvalid_thenReturnValue() {
		String accountNumber = "abcdefghi";

		boolean isValid = validator.isBankAccountNumberValid(accountNumber);

		Assertions.assertThat(isValid).isFalse();
	}

	@Test
	public void whenSetAccountNumberSpecialCharacter_thenReturnValue() {
		String accountNumber = "!#@$#$$%%^&";

		boolean isValid = validator.isBankAccountNumberValid(accountNumber);

		Assertions.assertThat(isValid).isFalse();
	}

}