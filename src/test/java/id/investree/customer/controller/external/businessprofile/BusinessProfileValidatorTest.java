package id.investree.customer.controller.external.businessprofile;

import id.investree.core.exception.AppException;
import id.investree.customer.model.request.BusinessProfileRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BusinessProfileValidatorTest {

	@Autowired
	private BusinessProfileValidator validator;

	private BusinessProfileRequest request;

	private String accessToken = "ignored";

	@Before
	public void init() {
		request = new BusinessProfileRequest();

		request.setCompanyAddress("test valid");
		request.setCompanyName("test valid");
		request.setLegalEntity(1);
		request.setCustomerId(-123L);
		request.setIndustry("1");
		request.setDateOfEstablishment(new Date());
		request.setNumberOfEmployee(1);
		request.setCompanyDescription("test valid");
		request.setProvince(1);
		request.setCity(1);
		request.setDistrict(1);
		request.setVillage(1);
		request.setPostalCode("123");
		request.setLandLineNumber("12345");
		request.setProfitMargin(2.0);
		request.setAverageMonthlySales(2.0);
		request.setGroupCompany("test valid");
		request.setGroupDescription("test valid");
		request.setListOfPayor("test valid");
		request.setRelationshipWithBank(true);
		request.setMailingAddressStatus(1);
		request.setMobilePrefix("+62");
		request.setNationality(104L);
		request.setOtherIncome(2.0);
		request.setGeneralProfitMargin(2.0);
		request.setProfitMarginFromPartner(2.0);
		request.setTotalNettMargin(2.0);
		request.setLivingCost(2.0);
		request.setCompanyNarration("test valid");
	}

	@Test
	public void whenBusinessProfileValid_thenReturn() {
		validator.businessValidator(request, accessToken);
	}

	@Test(expected = AppException.class)
	public void whenCustomerIdNull_thenReturnException() {
		request.setCustomerId(null);
		validator.businessValidator(request, accessToken);
	}

	@Test(expected = AppException.class)
	public void whenPostalCodeNotValid_thenReturnException() {
		request.setPostalCode("avcd");
		validator.businessValidator(request, accessToken);
	}

	@Test(expected = AppException.class)
	public void whenLandNumberLessThanMinimum_thenReturnException() {
		request.setLandLineNumber("1234");
		validator.businessValidator(request, accessToken);
	}

	@Test(expected = AppException.class)
	public void whenLandNumberNotNumeric_thenReturnException() {
		request.setLandLineNumber("abcdef");
		validator.businessValidator(request, accessToken);
	}

	@Test(expected = AppException.class)
	public void whenDateOfEstablishMoreThanToday_thenReturnException() throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		request.setDateOfEstablishment(dateFormat.parse("2028-09-08"));
		validator.businessValidator(request, accessToken);
	}

	@Test(expected = AppException.class)
	public void whenNumberOfEmployeeNotValid_thenReturnException() {
		request.setNumberOfEmployee(0);
		validator.businessValidator(request, accessToken);
	}

	@Test(expected = AppException.class)
	public void whenCityFilledAndProvinceNull_thenReturnException() {
		request.setProvince(null);
		validator.businessValidator(request, accessToken);
	}

	@Test(expected = AppException.class)
	public void whenCityNotProvinceArea_thenReturnException() {
		request.setCity(25);
		validator.businessValidator(request, accessToken);
	}

	@Test(expected = AppException.class)
	public void whenDistrictFilledAndCityNull_thenReturnException() {
		request.setCity(null);
		validator.businessValidator(request, accessToken);
	}

	@Test(expected = AppException.class)
	public void whenDistrictNotCityArea_thenReturnException() {
		request.setDistrict(59);
		validator.businessValidator(request, accessToken);
	}

	@Test(expected = AppException.class)
	public void whenVillageFilledAndDistrictNull_thenReturnException() {
		request.setDistrict(null);
		validator.businessValidator(request, accessToken);
	}

	@Test(expected = AppException.class)
	public void whenVillageNotDistrictArea_thenReturnException() {
		request.setVillage(59);
		validator.businessValidator(request, accessToken);
	}
}
