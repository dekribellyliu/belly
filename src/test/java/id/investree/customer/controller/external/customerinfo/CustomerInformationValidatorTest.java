package id.investree.customer.controller.external.customerinfo;

import id.investree.core.constant.GlobalConstants;
import id.investree.customer.model.request.CreateProductPrefRequest;
import id.investree.customer.model.request.UpdateProductPreferenceRequest;
import id.investree.customer.utils.Constants;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerInformationValidatorTest {

	@Autowired
	private CustomerInformationValidator validator;

	@Test
	public void testCreateProductPreferenceEmptyField() {
		CreateProductPrefRequest createProductPrefRequest = new CreateProductPrefRequest();
		Assertions.assertThat(validator.isCreateProductPreferenceValid(createProductPrefRequest)).isNotEmpty();

		createProductPrefRequest.setUserCategory(-1);
		Assertions.assertThat(validator.isCreateProductPreferenceValid(createProductPrefRequest)).isNotEmpty();

		createProductPrefRequest.setUserCategory(GlobalConstants.CATEGORY_INSTITUSI_ID);
		Assertions.assertThat(validator.isCreateProductPreferenceValid(createProductPrefRequest)).isNotEmpty();

		createProductPrefRequest.setProductPreference(GlobalConstants.USER_PREFERENCE_CONVENTIONAL);
		Assertions.assertThat(validator.isCreateProductPreferenceValid(createProductPrefRequest)).isNotEmpty();

		createProductPrefRequest.setProductSelection(Constants.USER_PREFERENCE_WCTL);
		Assertions.assertThat(validator.isCreateProductPreferenceValid(createProductPrefRequest)).isNotEmpty();

		createProductPrefRequest.setCompanyName(null);
		Assertions.assertThat(validator.isCreateProductPreferenceValid(createProductPrefRequest)).isNotEmpty();

		createProductPrefRequest.setCompanyName("ABCD");
		Assertions.assertThat(validator.isCreateProductPreferenceValid(createProductPrefRequest)).isNotEmpty();
	}

	@Test
	public void testCreateProductPreferenceInstitution() {
		CreateProductPrefRequest createProductPrefRequest = new CreateProductPrefRequest();

		createProductPrefRequest.setUserCategory(GlobalConstants.CATEGORY_INSTITUSI_ID);
		Assertions.assertThat(validator.isCreateProductPreferenceValid(createProductPrefRequest)).isNotEmpty();

		createProductPrefRequest.setProductPreference(GlobalConstants.USER_PREFERENCE_CONVENTIONAL);
		Assertions.assertThat(validator.isCreateProductPreferenceValid(createProductPrefRequest)).isNotEmpty();

		createProductPrefRequest.setLegalEntity(1);
		Assertions.assertThat(validator.isCreateProductPreferenceValid(createProductPrefRequest)).isNotEmpty();

		createProductPrefRequest.setCompanyName("ABCD");
		Assertions.assertThat(validator.isCreateProductPreferenceValid(createProductPrefRequest)).isNotEmpty();

		createProductPrefRequest.setProductSelection(Constants.USER_PREFERENCE_OSF);
		Assertions.assertThat(validator.isCreateProductPreferenceValid(createProductPrefRequest)).isNotEmpty();

		createProductPrefRequest.setProductSelection(Constants.USER_PREFERENCE_WCTL);
		Assertions.assertThat(validator.isCreateProductPreferenceValid(createProductPrefRequest)).isEmpty();
	}

	@Test
	public void testCreateProductPreferenceIndividual() {
		CreateProductPrefRequest createProductPrefRequest = new CreateProductPrefRequest();

		createProductPrefRequest.setUserCategory(GlobalConstants.CATEGORY_INDIVIDU_ID);
		Assertions.assertThat(validator.isCreateProductPreferenceValid(createProductPrefRequest)).isNotEmpty();

		createProductPrefRequest.setProductPreference(GlobalConstants.USER_PREFERENCE_CONVENTIONAL);
		Assertions.assertThat(validator.isCreateProductPreferenceValid(createProductPrefRequest)).isNotEmpty();

		createProductPrefRequest.setProductSelection(Constants.USER_PREFERENCE_WCTL);
		Assertions.assertThat(validator.isCreateProductPreferenceValid(createProductPrefRequest)).isNotEmpty();

		createProductPrefRequest.setProductSelection(Constants.USER_PREFERENCE_OSF);
		Assertions.assertThat(validator.isCreateProductPreferenceValid(createProductPrefRequest)).isEmpty();
	}

	@Test
	public void testUpdateProductPreferenceValid() {
		UpdateProductPreferenceRequest updateProductPreferenceRequest = new UpdateProductPreferenceRequest();
		Assertions.assertThat(validator.isUpdateProductPreferenceValid(updateProductPreferenceRequest)).isNotEmpty();

		updateProductPreferenceRequest.setProductPreference(GlobalConstants.USER_PREFERENCE_CONVENTIONAL);
		Assertions.assertThat(validator.isUpdateProductPreferenceValid(updateProductPreferenceRequest)).isEmpty();
	}

	@Test
	public void testUserType() {
		Assertions.assertThat(validator.isUserTypeValid(-1)).isFalse();
		Assertions.assertThat(validator.isUserTypeValid(GlobalConstants.USER_CATEGORY_BORROWER_ID)).isTrue();
		Assertions.assertThat(validator.isUserTypeValid(GlobalConstants.USER_CATEGORY_LENDER_ID)).isTrue();
		Assertions.assertThat(validator.isUserTypeValid(GlobalConstants.USER_CATEGORY_ANCHOR_ID)).isTrue();
	}

}