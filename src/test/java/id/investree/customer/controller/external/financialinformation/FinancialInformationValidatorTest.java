package id.investree.customer.controller.external.financialinformation;

import id.investree.core.exception.AppException;
import id.investree.customer.model.request.FinancialInformationRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FinancialInformationValidatorTest {

	@Autowired
	private FinancialInformationValidator validator;

	private FinancialInformationRequest financialInformationRequest;

	@Before
	public void init() {
		financialInformationRequest = new FinancialInformationRequest();
		financialInformationRequest.setCustomerId(2991L);
		financialInformationRequest.setStatementFileType(30);
		financialInformationRequest.setStatementUrl("https://investree.com/pic.jpg");
		financialInformationRequest.setStatementFileDate(new Date());
	}

	@Test
	public void whenFinancialInformationValid_thenReturn() {
		validator.financialValidator(financialInformationRequest);
	}

	@Test(expected = AppException.class)
	public void whenCustomerIdNull_thenReturnException() {
		financialInformationRequest.setCustomerId(null);
		validator.financialValidator(financialInformationRequest);
	}

	@Test(expected = AppException.class)
	public void whenFinancialStatementUrlNull_thenReturnException() {
		financialInformationRequest.setStatementFileType(10);
		financialInformationRequest.setStatementUrl(null);
		validator.financialValidator(financialInformationRequest);
	}

	@Test(expected = AppException.class)
	public void whenEStatementUrlNull_thenReturnException() {
		financialInformationRequest.setStatementFileType(30);
		financialInformationRequest.setStatementUrl(null);
		validator.financialValidator(financialInformationRequest);
	}

	@Test(expected = AppException.class)
	public void whenStatementUrlNotValid_thenReturnException() {
		financialInformationRequest.setStatementUrl("http://investree.com/pic.php");
		validator.financialValidator(financialInformationRequest);
	}

	@Test(expected = AppException.class)
	public void whenStatementFileTypeNotValid_thenReturnExcption() {
		financialInformationRequest.setStatementFileType(18);
		validator.financialValidator(financialInformationRequest);
	}

	@Test(expected =  AppException.class)
	public void whenStatementDateMoreThanToday_thenReturnException() throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		financialInformationRequest.setStatementFileDate(dateFormat.parse("3001-09-08"));
		validator.financialValidator(financialInformationRequest);
	}

	@Test(expected =  AppException.class)
	public void whenStatementDateLessThanSixYears_thenReturnException() throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		financialInformationRequest.setStatementFileDate(dateFormat.parse("2008-09-08"));
		validator.financialValidator(financialInformationRequest);
	}


}
