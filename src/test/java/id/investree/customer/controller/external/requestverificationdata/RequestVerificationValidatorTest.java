package id.investree.customer.controller.external.requestverificationdata;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.core.IsEqual.equalTo;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RequestVerificationValidatorTest {

	@Autowired
	private RequestVerificationValidator validator;

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	private String accessToken = GlobalConstants.IGNORED;

	private Integer userTypeIdentification = GlobalConstants.USER_CATEGORY_BORROWER_ID;

	@Test
	public void whenCustomerRoleDataIsComplete_thenReturn() {
		Integer customerId = 12212;

		validator.checkData(userTypeIdentification, customerId, accessToken);
	}

	@Test
	public void whenCustomerRoleIsPendingVerification_thenReturnException() {
		Integer customerId = 25109;

		thrown.expect(AppException.class);
		thrown.expectMessage(equalTo("Borrower with status pending verification can not request data verification"));

		validator.checkData(userTypeIdentification, customerId, accessToken);
	}

	@Test
	public void whenCustomerRoleIsActive_thenReturnException() {
		Integer customerId = 25978;

		thrown.expect(AppException.class);
		thrown.expectMessage(equalTo("Borrower with status active can not request data verification"));

		validator.checkData(userTypeIdentification, customerId, accessToken);
	}

	@Test
	public void whenCustomerRoleIsInactive_thenReturnException() {
		Integer customerId = 25974;

		thrown.expect(AppException.class);
		thrown.expectMessage(equalTo("Borrower with status inactive can not request data verification"));

		validator.checkData(userTypeIdentification, customerId, accessToken);
	}

	@Test
	public void whenBusinessProfileIsNotComplete_thenReturnException() {
		Integer customerId = 12211;

		thrown.expect(AppException.class);
		thrown.expectMessage(equalTo("Business Profile is not completed"));

		validator.checkData(userTypeIdentification, customerId, accessToken);
	}

	@Test
	public void whenLegalInformationIsNotComplete_thenReturnException() {
		Integer customerId = 26344;

		thrown.expect(AppException.class);
		thrown.expectMessage(equalTo("Legal Information is not completed"));

		validator.checkData(userTypeIdentification, customerId, accessToken);
	}

	@Test
	public void whenPersonalProfileIsNotComplete_thenReturnException() {
		Integer customerId = 21288;

		thrown.expect(AppException.class);
		thrown.expectMessage(equalTo("Personal Profile is not completed"));

		validator.checkData(userTypeIdentification, customerId, accessToken);
	}

	@Test
	public void whenFinancialStatementIsNotComplete_thenReturnException() {
		Integer customerId = 25394;

		thrown.expect(AppException.class);
		thrown.expectMessage(equalTo("Please provide the last 2 years of your Financial Statements"));

		validator.checkData(userTypeIdentification, customerId, accessToken);
	}

	@Test
	public void whenFinancialEStatementIsNotComplete_thenReturnException() {
		Integer customerId = 24530;

		thrown.expect(AppException.class);
		thrown.expectMessage(equalTo("Please provide the last 6 months of your E-statements"));

		validator.checkData(userTypeIdentification, customerId, accessToken);
	}

	@Test
	public void whenBankInformationIsNotComplete_thenReturnException() {
		Integer customerId = 24083;

		thrown.expect(AppException.class);
		thrown.expectMessage(equalTo("BankInformation is Empty"));

		validator.checkData(userTypeIdentification, customerId, accessToken);
	}

	@Test
	public void whenEmergencyContactIsNotComplete_thenReturnException() {
		Integer customerId = 23890;

		thrown.expect(AppException.class);
		thrown.expectMessage(equalTo("EmergencyContact is Empty"));

		validator.checkData(userTypeIdentification, customerId, accessToken);
	}

	@Test
	public void whenShareholderInformationIsNotComplete_thenReturnException() {
		Integer customerId = 25075;

		thrown.expect(AppException.class);
		thrown.expectMessage(equalTo("ShareholdersInformation is Empty"));

		validator.checkData(userTypeIdentification, customerId, accessToken);
	}
}
