package id.investree.customer.controller.external.shareholdersinformation;

import id.investree.core.exception.AppException;
import id.investree.customer.model.request.ApuPptCheckRequest;
import id.investree.customer.model.request.ShareholdersInformationRequest;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ShareholdersInformationValidatorTest {

	@Autowired
	private ShareholdersInformationValidator validator;

	private ShareholdersInformationRequest shareholdersInformationRequest;
	private List<ShareholdersInformationRequest> shareholdersInformationRequestList;

	private Long shareHolderId;
	private Long customerId;
	private Long position;
	private String fullName;
	private Long mobilePrefix;
	private String mobileNumber;
	private String emailAddress;
	private Float stockOwnership;
	private Date dob;
	private String identificationCardUrl;
	private String identificationCardNumber;
	private Date identificationCardExpiryDate;
	private String selfieUrl;
	private String taxCardUrl;
	private String taxCardNumber;
	private boolean isLss;
	private boolean isPgs;
	private boolean isTss;
	private boolean isDelete;
	private ApuPptCheckRequest apuPptCheck;
	private String pgNumber;
	private Long pgAmount;
	private Date pgSignedDate;
	private String pgType;
	private String pgFile;

	private Long apuPptId;
	private Date checkingDate;
	private String checkingFile;
	private Object checkingResult;

	@Before
	public void init() {
		shareHolderId = -999L;
		customerId = -999L;
		position = -999L;
		fullName = "lorem ipsum";
		mobilePrefix = 1L;
		mobileNumber = "9999999999";
		emailAddress = "lorem@investree.id";
		stockOwnership = 90f;
		dob = new Date();
		identificationCardUrl = "lorem_ipsum.jpg";
		identificationCardNumber = RandomStringUtils.randomNumeric(16);
		identificationCardExpiryDate = new Date();
		selfieUrl = "lorem_ipsum.jpg";
		taxCardUrl = "lorem_ipsum.jpg";
		taxCardNumber = RandomStringUtils.randomNumeric(15);
		isLss = false;
		isPgs = false;
		isTss = false;
		isDelete = false;
		pgNumber = RandomStringUtils.randomNumeric(9);
		pgAmount = -999L;
		pgSignedDate = new Date();
		pgType = "lorem";
		pgFile = "lorem_ipsum.jpg";

		apuPptId = -999L;
		checkingDate = new Date();
		checkingFile = "lorem_ipsum.jpg";
		checkingResult = "good";

		apuPptCheck = new ApuPptCheckRequest();
		shareholdersInformationRequest = new ShareholdersInformationRequest();
		shareholdersInformationRequest.setLss(isLss);
		shareholdersInformationRequest.setPgs(isPgs);
		shareholdersInformationRequest.setTss(isTss);
		shareholdersInformationRequest.setCustomerId(customerId);
		shareholdersInformationRequest.setPosition(position);
		shareholdersInformationRequest.setFullName(fullName);
		shareholdersInformationRequest.setMobilePrefix(mobilePrefix);
		shareholdersInformationRequest.setMobileNumber(mobileNumber);
		shareholdersInformationRequest.setEmailAddress(emailAddress);
		shareholdersInformationRequest.setStockOwnership(stockOwnership);
		shareholdersInformationRequest.setDob(dob);
		shareholdersInformationRequest.setIdentificationCardUrl(identificationCardUrl);
		shareholdersInformationRequest.setIdentificationCardNumber(identificationCardNumber);
		shareholdersInformationRequest.setIdentificationCardExpiryDate(identificationCardExpiryDate);
		shareholdersInformationRequest.setSelfieUrl(selfieUrl);
		shareholdersInformationRequest.setTaxCardUrl(taxCardUrl);
		shareholdersInformationRequest.setTaxCardNumber(taxCardNumber);
		shareholdersInformationRequest.setDelete(isDelete);
		apuPptCheck.setApuPptId(apuPptId);
		apuPptCheck.setCheckingDate(checkingDate);
		apuPptCheck.setCheckingFile(checkingFile);
		apuPptCheck.setCheckingResult(checkingResult);
		shareholdersInformationRequest.setApuPptCheck(apuPptCheck);
		shareholdersInformationRequest.setShareHolderId(shareHolderId);
		shareholdersInformationRequest.setPgNumber(pgNumber);
		shareholdersInformationRequest.setPgAmount(pgAmount);
		shareholdersInformationRequest.setPgSignedDate(pgSignedDate);
		shareholdersInformationRequest.setPgType(pgType);
		shareholdersInformationRequest.setPgFile(pgFile);

		shareholdersInformationRequestList = new ArrayList<>();
	}

	@Test(expected = AppException.class)
	public void whenSetDob_thenReturnException() {
		shareholdersInformationRequest.setDob(new GregorianCalendar(3000, Calendar.DECEMBER, 30).getTime());
		shareholdersInformationRequestList.add(shareholdersInformationRequest);

		validator.backofficeBulkValidation(shareholdersInformationRequestList);
	}

	@Test(expected = AppException.class)
	public void whenSetDobBeforeSeventeen_thenReturnException() {
		shareholdersInformationRequest.setDob(new GregorianCalendar(2018, Calendar.DECEMBER, 30).getTime());
		shareholdersInformationRequestList.add(shareholdersInformationRequest);

		validator.backofficeBulkValidation(shareholdersInformationRequestList);
	}

	@Test(expected = AppException.class)
	public void whenSetIdentificationUrl_thenReturnException() {
		shareholdersInformationRequest.setIdentificationCardUrl("");
		shareholdersInformationRequestList.add(shareholdersInformationRequest);

		validator.backofficeBulkValidation(shareholdersInformationRequestList);
	}

	@Test(expected = AppException.class)
	public void whenSetIdentificationNumber_thenReturnException() {
		shareholdersInformationRequest.setIdentificationCardNumber("");
		shareholdersInformationRequestList.add(shareholdersInformationRequest);

		validator.backofficeBulkValidation(shareholdersInformationRequestList);
	}

	@Test(expected = AppException.class)
	public void whenSetIdentificationExpiryDate_thenReturnException() {
		shareholdersInformationRequest.setIdentificationCardExpiryDate(new GregorianCalendar(2000, Calendar.DECEMBER, 30).getTime());
		shareholdersInformationRequestList.add(shareholdersInformationRequest);

		validator.backofficeBulkValidation(shareholdersInformationRequestList);
	}

	@Test(expected = AppException.class)
	public void whenSetTaxCardNumber_thenReturnException() {
		shareholdersInformationRequest.setTaxCardNumber("47192550982546");
		shareholdersInformationRequestList.add(shareholdersInformationRequest);

		validator.backofficeBulkValidation(shareholdersInformationRequestList);
	}

	@Test(expected = AppException.class)
	public void whenSetTaxCardNumberDuplicate_thenReturnException() {
		shareholdersInformationRequest.setTaxCardNumber("471925509825467");
		shareholdersInformationRequestList.add(shareholdersInformationRequest);

		validator.backofficeBulkValidation(shareholdersInformationRequestList);
	}

	@Test(expected = AppException.class)
	public void whenSetStockOwnershipMoreThanOneHundred_thenReturnException() {
		shareholdersInformationRequest.setStockOwnership(101f);
		shareholdersInformationRequestList.add(shareholdersInformationRequest);

		validator.backofficeBulkValidation(shareholdersInformationRequestList);
	}

	@Test(expected = AppException.class)
	public void whenSetApuPptCheckFileNotPdf_thenReturnException() {
		apuPptCheck.setCheckingFile("lorem_ipsum.jpg");
		shareholdersInformationRequestList.add(shareholdersInformationRequest);

		validator.backofficeBulkValidation(shareholdersInformationRequestList);
	}

	@Test(expected = AppException.class)
	public void whenSetApuPptCheckDate_thenReturnException() {
		apuPptCheck.setCheckingDate(new GregorianCalendar(3000, Calendar.DECEMBER, 30).getTime());
		shareholdersInformationRequestList.add(shareholdersInformationRequest);

		validator.backofficeBulkValidation(shareholdersInformationRequestList);
	}
}