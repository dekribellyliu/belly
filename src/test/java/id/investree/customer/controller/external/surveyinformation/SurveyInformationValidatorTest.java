package id.investree.customer.controller.external.surveyinformation;

import id.investree.core.exception.AppException;
import id.investree.customer.model.request.SurveyInformationDataRequest;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SurveyInformationValidatorTest {

	@Autowired
	private SurveyInformationValidator validator;

	private SurveyInformationDataRequest surveyInformationDataRequest;

	@Before
	public void init() {
		surveyInformationDataRequest = new SurveyInformationDataRequest();
		surveyInformationDataRequest.setSurveyInformationId(-999L);
		surveyInformationDataRequest.setSurveyBy(-999L);
		surveyInformationDataRequest.setSurveyAt(new Date());
		surveyInformationDataRequest.setBorrowerPosition(-1L);
		surveyInformationDataRequest.setBorrowerName(RandomStringUtils.randomAlphabetic(6));
		surveyInformationDataRequest.setNumberOfEmployee(Long.valueOf(RandomStringUtils.randomNumeric(3)));
		surveyInformationDataRequest.setOfficeStatus(-1L);
		surveyInformationDataRequest.setLengthOfStay("0");
		surveyInformationDataRequest.setFilename(Collections.singletonList("lorem_ipsum.jpg"));
		surveyInformationDataRequest.setResultDescription("good");
		surveyInformationDataRequest.setAddress("lorem ipsum");
		surveyInformationDataRequest.setDelete(false);
	}

	@Test(expected = AppException.class)
	public void whenSetSurveyAt_thenReturnException() {
		surveyInformationDataRequest.setSurveyAt(new GregorianCalendar(3000, Calendar.DECEMBER, 30).getTime());

		validator.surveyValidator(surveyInformationDataRequest);
	}

	@Test(expected = AppException.class)
	public void whenSetFilename_thenReturnException() {
		surveyInformationDataRequest.setFilename(Collections.singletonList(""));

		validator.surveyValidator(surveyInformationDataRequest);
	}

	@Test(expected = AppException.class)
	public void whenSetNumberOfEmployee_thenReturnException() {
		surveyInformationDataRequest.setNumberOfEmployee(-1L);

		validator.surveyValidator(surveyInformationDataRequest);
	}

	@Test(expected = AppException.class)
	public void whenSetLengthOfStay_thenReturnException() {
		surveyInformationDataRequest.setLengthOfStay("-1");

		validator.surveyValidator(surveyInformationDataRequest);
	}
}