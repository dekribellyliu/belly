package id.investree.customer.controller.internal.completing;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.model.TokenPayload;
import id.investree.customer.entity.UserData;
import id.investree.customer.model.request.UpdateUserDataRequest;
import id.investree.customer.model.response.LoginDataResponse;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.service.userdata.UserDataService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InternalDetailDataFlowTest {

	@Autowired
	private InternalDetailDataFlow internalDetailDataFlow;

	@Autowired
	private LoginDataService loginDataService;

	@Autowired
	private UserDataService userDataService;

	@Test
	public void testUserDataByUserType() {
		LoginDataResponse loginDataResponse = loginDataService.findByLoginId(GlobalConstants.IGNORED, 1157473L);

		TokenPayload frontofficeTokenPayload = new TokenPayload();
		frontofficeTokenPayload.setUserId(loginDataResponse.getId());
		frontofficeTokenPayload.setUserType(loginDataResponse.getUserType());
		frontofficeTokenPayload.setUserEncryptedToken(loginDataResponse.getUserToken());

		UserData frontofficeUserData = userDataService.findByLoginId(loginDataResponse.getId());
		UpdateUserDataRequest frontofficeUpdateUserData = new UpdateUserDataRequest();
		frontofficeUpdateUserData.setNationality(104L);
		frontofficeUpdateUserData.setAddress("Yogyakarta");

		UserData validateFoUserData = internalDetailDataFlow.generateUserDataByUserType(frontofficeUserData, frontofficeUpdateUserData, frontofficeTokenPayload);
		Assertions.assertThat(validateFoUserData.getNationality()).isEqualTo(104L);
		Assertions.assertThat(validateFoUserData.getKtpAddress()).isEqualTo("Yogyakarta");
	}

}