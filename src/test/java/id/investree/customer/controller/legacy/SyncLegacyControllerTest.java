package id.investree.customer.controller.legacy;

import id.investree.customer.entity.BankInformation;
import id.investree.customer.entity.BusinessProfile;
import id.investree.customer.entity.CustomerInformation;
import id.investree.customer.entity.PartnershipInformation;
import id.investree.customer.entity.ShareholdersInformation;
import id.investree.customer.entity.UserData;
import id.investree.customer.model.request.ShareholderInformationSyncLegacyRequest;
import id.investree.customer.model.request.SyncLegacyRequest;
import id.investree.customer.service.bankinformation.BankInformationService;
import id.investree.customer.service.businessprofile.BusinessProfileService;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.partnershipinformation.PartnershipInformationService;
import id.investree.customer.service.shareholdersinformation.ShareholdersInformationService;
import id.investree.customer.service.userdata.UserDataService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SyncLegacyControllerTest {

	@Autowired
	private SyncLegacyController controller;

	@Autowired
	private CustomerInformationService customerInformationService;

	@Autowired
	private ShareholdersInformationService shareholdersInformationService;

	@Autowired
	private BusinessProfileService businessProfileService;

	@Autowired
	private BankInformationService bankInformationService;

	@Autowired
	private PartnershipInformationService partnershipInformationService;

	@Autowired
	private UserDataService userDataService;

	private Long customerId;
	private String initial;

	private String businessData_Desc;
	private Integer businessData_Noe;
	private Date businessData_doe;

	private String shareholderInfo_fullName;
	private Long shareholderInfo_id;
	private Long shareholderInfo_position;
	private Long shareholderInfo_mobilePrefix;
	private String shareholderInfo_mobileNumber;
	private Date shareholderInfo_dob;
	private String shareholderInfo_identificationCardUrl;
	private String shareholderInfo_identificationCardNumber;
	private String shareholderInfo_taxCardUrl;
	private String shareholderInfo_taxCardNumber;
	private Float shareholderInfo_stockOwnership;
	private String shareholderInfo_isLss;
	private String shareholderInfo_isPgs;
	private String shareholderInfo_pgFile;
	private String shareholderInfo_pgNumber;
	private Date shareholderInfo_pgSignedDate;
	private Long shareholderInfo_pgAmount;
	private Long shareholderInfo_bankInfo_Id;

	private Long bankInfo_id;
	private Long bankInfo_masterBankid;

	private Long partnershipInfo_buildingLocOwnership;
	private String partnershipInfo_internalRating;
	private String partnershipInfo_externalRating;

	private String userData_domicileAddress;
	private Long userData_domicileProvince;
	private Long userData_domicileCity;
	private Long userData_domicileDistrict;
	private Long userData_domicileVillage;
	private String userData_domicilePostalCode;
	private Date userData_dateOfBirth;

	@Before
	public void init() {
		initial = "ABCD";
		businessData_Desc = "this is description";
		businessData_Noe = 102;
		businessData_doe = new Date();

		shareholderInfo_id = 4829L;
		shareholderInfo_fullName = "Test A";
		shareholderInfo_position = 1L;
		shareholderInfo_mobilePrefix = 1L;
		shareholderInfo_mobileNumber = "57575757575";
		shareholderInfo_dob = new Date();
		shareholderInfo_identificationCardUrl = "http://google.com";
		shareholderInfo_identificationCardNumber = "9876543";
		shareholderInfo_taxCardUrl = "http://google.com";
		shareholderInfo_taxCardNumber = "123456789";
		shareholderInfo_stockOwnership = 30.0F;
		shareholderInfo_isLss = "Y";
		shareholderInfo_isPgs = "Y";
		shareholderInfo_pgFile = "http://google.com";
		shareholderInfo_pgNumber = "ABCD-567890123";
		shareholderInfo_pgSignedDate = new Date();
		shareholderInfo_pgAmount = 10000L;

		bankInfo_id = 9094L;
		bankInfo_masterBankid = 183L;

		partnershipInfo_buildingLocOwnership = 1L;
		partnershipInfo_internalRating = "10.0";
		partnershipInfo_externalRating = "11.0";

		userData_domicileAddress = "Kalimantan Timur";
		userData_domicileProvince = 1L;
		userData_domicileCity = 1L;
		userData_domicileDistrict = 1L;
		userData_domicileVillage = 1L;
		userData_domicilePostalCode = "55951";
		userData_dateOfBirth = new Date();
	}

	@Test
	public void test_individual() {
		customerId = 514L;

		SyncLegacyRequest request = new SyncLegacyRequest();
		request.setCustomerInfo_Initial(initial);
		request.setBusinessData_Description(businessData_Desc);
		request.setBusinessData_doe(businessData_doe);
		request.setBankInfo_masterBankId(bankInfo_masterBankid);
		request.setPartnershipInfo_buildingLocOwnership(partnershipInfo_buildingLocOwnership);
		request.setPartnershipInfo_internalRating(partnershipInfo_internalRating);
		request.setPartnershipInfo_externalRating(partnershipInfo_externalRating);
		request.setUserData_domicileAddress(userData_domicileAddress);
		request.setUserData_domicileProvince(userData_domicileProvince);
		request.setUserData_domicileCity(userData_domicileCity);
		request.setUserData_domicileDistrict(userData_domicileDistrict);
		request.setUserData_domicileVillage(userData_domicileVillage);
		request.setUserData_domicilePostalCode(userData_domicilePostalCode);
		request.setUserData_dateOfBirth(userData_dateOfBirth);

		// test ok
		ResponseEntity responseEntity = controller.saveLegacy(customerId, request);
		Assertions.assertNotNull(responseEntity);

		// test customer id
		CustomerInformation customerInformation = customerInformationService.findById(customerId);
		Assertions.assertEquals(customerInformation.getInitial(), request.getCustomerInfo_Initial());

		// test bank information
		BankInformation bankInformation = bankInformationService.findById(bankInfo_id);
		Assertions.assertEquals(bankInformation.getMasterBankId(), request.getBankInfo_masterBankId());

		// test partnership info
		PartnershipInformation partnershipInformation = partnershipInformationService.findByCustomerId(customerId);
		Assertions.assertEquals(partnershipInformation.getBuildingLocOwnership(), request.getPartnershipInfo_buildingLocOwnership());
		Assertions.assertEquals(partnershipInformation.getInternalRating(), request.getPartnershipInfo_internalRating());
		Assertions.assertEquals(partnershipInformation.getExternalRating(), request.getPartnershipInfo_externalRating());

		// test user data
		UserData userData = userDataService.findByCustomerId(customerId);
		Assertions.assertEquals(userData.getDomicileAddress(), request.getUserData_domicileAddress());
		Assertions.assertEquals(userData.getDomicileProvince(), request.getUserData_domicileProvince());
		Assertions.assertEquals(userData.getDomicileCity(), request.getUserData_domicileCity());
		Assertions.assertEquals(userData.getDomicileDistrict(), request.getUserData_domicileDistrict());
		Assertions.assertEquals(userData.getDomicileVillage(), request.getUserData_domicileVillage());
		Assertions.assertEquals(userData.getDomicilePostalCode(), request.getUserData_domicilePostalCode());
		Assertions.assertNotNull(userData.getDateOfBirth());
	}

	@Test
	public void test_institutional() {
		customerId = 2875L;

		SyncLegacyRequest request = new SyncLegacyRequest();
		ShareholderInformationSyncLegacyRequest shareholderInformationRequest = new ShareholderInformationSyncLegacyRequest();
		request.setCustomerInfo_Initial(initial);
		request.setBusinessData_Description(businessData_Desc);
		request.setBusinessData_numberOfEmployee(businessData_Noe);
		request.setBusinessData_doe(businessData_doe);
		shareholderInformationRequest.setShareholderInfo_id(4829L);
		shareholderInformationRequest.setShareholderInfo_fullName(shareholderInfo_fullName);
		shareholderInformationRequest.setShareholderInfo_position(shareholderInfo_position);
		shareholderInformationRequest.setShareholderInfo_mobilePrefix(shareholderInfo_mobilePrefix);
		shareholderInformationRequest.setShareholderInfo_mobileNumber(shareholderInfo_mobileNumber);
		shareholderInformationRequest.setShareholderInfo_dob(shareholderInfo_dob);
		shareholderInformationRequest.setShareholderInfo_identificationCardUrl(shareholderInfo_identificationCardUrl);
		shareholderInformationRequest.setShareholderInfo_identificationCardNumber(shareholderInfo_identificationCardNumber);
		shareholderInformationRequest.setShareholderInfo_taxCardUrl(shareholderInfo_taxCardUrl);
		shareholderInformationRequest.setShareholderInfo_taxCardNumber(shareholderInfo_taxCardNumber);
		shareholderInformationRequest.setShareholderInfo_stockOwnership(shareholderInfo_stockOwnership);

		shareholderInformationRequest.setShareholderInfo_isLss(shareholderInfo_isLss);
		shareholderInformationRequest.setShareholderInfo_isPgs(shareholderInfo_isPgs);
		shareholderInformationRequest.setShareholderInfo_pgFile(shareholderInfo_pgFile);
		shareholderInformationRequest.setShareholderInfo_pgNumber(shareholderInfo_pgNumber);
		shareholderInformationRequest.setShareholderInfo_pgSignedDate(shareholderInfo_pgSignedDate);
		shareholderInformationRequest.setShareholderInfo_pgAmount(shareholderInfo_pgAmount);

		// test ok
		ResponseEntity responseEntity = controller.saveLegacy(customerId, request);
		Assertions.assertNotNull(responseEntity);

		// test customer id
		CustomerInformation customerInformation = customerInformationService.findById(customerId);
		Assertions.assertEquals(customerInformation.getInitial(), request.getCustomerInfo_Initial());

		// test business profile
		BusinessProfile businessProfile = businessProfileService.findByCustomerId(customerId);
		Assertions.assertEquals(businessProfile.getCompanyDescription(), request.getBusinessData_Description());
		Assertions.assertEquals(businessProfile.getNumberOfEmployee(), request.getBusinessData_numberOfEmployee());

		// test shareholder
		ShareholdersInformation shareholdersInformation = shareholdersInformationService.findById(shareholderInfo_id);
		Assertions.assertEquals(shareholdersInformation.getId(), shareholderInformationRequest.getShareholderInfo_id());
		Assertions.assertEquals(shareholdersInformation.getFullName(), shareholderInformationRequest.getShareholderInfo_fullName());
		Assertions.assertEquals(shareholdersInformation.getPosition(), shareholderInformationRequest.getShareholderInfo_position());
		Assertions.assertEquals(shareholdersInformation.getMobilePrefix(), shareholderInformationRequest.getShareholderInfo_mobilePrefix());
		Assertions.assertEquals(shareholdersInformation.getMobileNumber(), shareholderInformationRequest.getShareholderInfo_mobileNumber());
		Assertions.assertNotNull(shareholdersInformation.getDob());
		Assertions.assertEquals(shareholdersInformation.getIdentificationCardUrl(), shareholderInformationRequest.getShareholderInfo_identificationCardUrl());
		Assertions.assertEquals(shareholdersInformation.getIdentificationCardNumber(), shareholderInformationRequest.getShareholderInfo_identificationCardNumber());
		Assertions.assertEquals(shareholdersInformation.getTaxCardUrl(), shareholderInformationRequest.getShareholderInfo_taxCardUrl());
		Assertions.assertEquals(shareholdersInformation.getTaxCardNumber(), shareholderInformationRequest.getShareholderInfo_taxCardNumber());
		Assertions.assertEquals(shareholdersInformation.getStockOwnership(), shareholderInformationRequest.getShareholderInfo_stockOwnership());
		Assertions.assertEquals(shareholdersInformation.getIsLss(), shareholderInformationRequest.getShareholderInfo_isLss());
		Assertions.assertEquals(shareholdersInformation.getIsPgs(), shareholderInformationRequest.getShareholderInfo_isPgs());
		Assertions.assertEquals(shareholdersInformation.getPgFile(), shareholderInformationRequest.getShareholderInfo_pgFile());
		Assertions.assertEquals(shareholdersInformation.getPgNumber(), shareholderInformationRequest.getShareholderInfo_pgNumber());
		Assertions.assertNotNull(shareholdersInformation.getPgSignedDate());
		Assertions.assertEquals(shareholdersInformation.getPgAmount(), shareholderInformationRequest.getShareholderInfo_pgAmount());
	}
}