package id.investree.customer.entity;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BalanceSheetTest {

	private Long id;
	private Long customerId;
	private Long yearTo;
	private Double accReceive;
	private Double investory;
	private Double accPayable;
	private Double bankDebt;
	private Double currentAssets;
	private Double currentLiabilities;
	private Double totalLiabilities;
	private Double equity;
	private Long createdBy;
	private Date createdAt;
	private Long updateBy;
	private Date updateAt;

	@Before
	public void init() {
		id = 1L;
		customerId = 1L;
		yearTo = 2015L;
		accReceive = 10.5;
		investory = 10.5;
		accPayable = 10.5;
		bankDebt = 10.5;
		currentAssets = 10.5;
		currentLiabilities = 10.5;
		totalLiabilities = 10.5;
		equity = 10.5;
		createdBy = 1L;
		createdAt = new Date();
		updateBy = 10L;
		updateAt = new Date();
	}

	@Test
	public void testBalanceSheetModel() {
		BalanceSheet balanceSheet = new BalanceSheet();

		balanceSheet.setId(id);
		Assertions.assertThat(balanceSheet.getId()).isNotNull();

		balanceSheet.setCustomerId(customerId);
		Assertions.assertThat(balanceSheet.getCustomerId()).isNotNull();

		balanceSheet.setYearTo(yearTo);
		Assertions.assertThat(balanceSheet.getYearTo()).isNotNull();

		balanceSheet.setAccReceive(accReceive);
		Assertions.assertThat(balanceSheet.getAccReceive()).isNotNull();

		balanceSheet.setInvestory(investory);
		Assertions.assertThat(balanceSheet.getInvestory()).isNotNull();

		balanceSheet.setAccPayable(accPayable);
		Assertions.assertThat(balanceSheet.getAccPayable()).isNotNull();

		balanceSheet.setBankDebt(bankDebt);
		Assertions.assertThat(balanceSheet.getBankDebt()).isNotNull();

		balanceSheet.setCurrentAssets(currentAssets);
		Assertions.assertThat(balanceSheet.getCurrentAssets()).isNotNull();

		balanceSheet.setCurrentLiabilities(currentLiabilities);
		Assertions.assertThat(balanceSheet.getCurrentLiabilities()).isNotNull();

		balanceSheet.setTotalLiabilities(totalLiabilities);
		Assertions.assertThat(balanceSheet.getTotalLiabilities()).isNotNull();

		balanceSheet.setEquity(equity);
		Assertions.assertThat(balanceSheet.getEquity()).isNotNull();

		balanceSheet.setCreatedBy(createdBy);
		Assertions.assertThat(balanceSheet.getCreatedBy()).isNotNull();

		balanceSheet.setCreatedAt(createdAt);
		Assertions.assertThat(balanceSheet.getCreatedAt()).isNotNull();

		balanceSheet.setUpdateBy(updateBy);
		Assertions.assertThat(balanceSheet.getUpdateBy()).isNotNull();

		balanceSheet.setUpdateAt(updateAt);
		Assertions.assertThat(balanceSheet.getUpdateAt()).isNotNull();
	}
}