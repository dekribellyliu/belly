package id.investree.customer.entity;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BankInformationTest {

	private BankInformation bankInformation;

	private Long id;
	private Long customerId;
	private Integer bankType;
	private String bankAccountCoverFile;
	private Long masterBankId;
	private String bankAccountNumber;
	private String bankAccountHolderName;
	private String useAsDisbursement;
	private String useAsWithdrawal;
	private String isAnchorDisbursement;
	private Long createdBy;
	private Long updateBy;
	private Long deletedBy;
	private Date createdAt;
	private Date updatedAt;
	private Date deletedAt;
	private Date fillFinishAt;
	private String verifiedStatus;
	private Long customerRole;
	private Long invarBank;

	@Before
	public void init() {
		bankInformation = new BankInformation();

		id = -999L;
		customerId = -999L;
		bankType = 1;
		bankAccountCoverFile = "lorem ipsum";
		masterBankId = 1L;
		bankAccountNumber = "999999";
		bankAccountHolderName = "LOREM IPSUM";
		useAsDisbursement = "Y";
		useAsWithdrawal = "N";
		isAnchorDisbursement = "N";
		createdBy = -999L;
		updateBy = -999L;
		deletedBy = -999L;
		createdAt = new Date();
		updatedAt = new Date();
		deletedAt = new Date();
		fillFinishAt = new Date();
		verifiedStatus = "Verified";
		customerRole = 1L;
		invarBank = 1L;
	}

	@Test
	public void whenSetBankInformation_thenReturnValue() {

		bankInformation.setId(id);
		Assertions.assertThat(bankInformation.getId()).isEqualTo(id);

		bankInformation.setCustomerId(customerId);
		Assertions.assertThat(bankInformation.getCustomerId()).isEqualTo(customerId);

		bankInformation.setBankType(bankType);
		Assertions.assertThat(bankInformation.getBankType()).isEqualTo(bankType);

		bankInformation.setBankAccountCoverFile(bankAccountCoverFile);
		Assertions.assertThat(bankInformation.getBankAccountCoverFile()).isEqualTo(bankAccountCoverFile);

		bankInformation.setBankAccountNumber(bankAccountNumber);
		Assertions.assertThat(bankInformation.getBankAccountNumber()).isEqualTo(bankAccountNumber);

		bankInformation.setBankAccountHolderName(bankAccountHolderName);
		Assertions.assertThat(bankInformation.getBankAccountHolderName()).isEqualTo(bankAccountHolderName);

		bankInformation.setMasterBankId(masterBankId);
		Assertions.assertThat(bankInformation.getMasterBankId()).isEqualTo(masterBankId);

		bankInformation.setUseAsDisbursement(useAsDisbursement);
		Assertions.assertThat(bankInformation.getUseAsDisbursement()).isEqualTo(useAsDisbursement);

		bankInformation.setUseAsWithdrawal(useAsWithdrawal);
		Assertions.assertThat(bankInformation.getUseAsWithdrawal()).isEqualTo(useAsWithdrawal);

		bankInformation.setIsAnchorDisbursement(isAnchorDisbursement);
		Assertions.assertThat(bankInformation.getIsAnchorDisbursement()).isEqualTo(isAnchorDisbursement);

		bankInformation.setCreatedBy(createdBy);
		Assertions.assertThat(bankInformation.getCreatedBy()).isEqualTo(createdBy);

		bankInformation.setCreatedAt(createdAt);
		Assertions.assertThat(bankInformation.getCreatedAt()).isEqualTo(createdAt);

		bankInformation.setUpdateBy(updateBy);
		Assertions.assertThat(bankInformation.getUpdateBy()).isEqualTo(updateBy);

		bankInformation.setUpdatedAt(updatedAt);
		Assertions.assertThat(bankInformation.getUpdatedAt()).isEqualTo(updatedAt);

		bankInformation.setFillFinishAt(fillFinishAt);
		Assertions.assertThat(bankInformation.getFillFinishAt()).isEqualTo(fillFinishAt);

		bankInformation.setDeletedBy(deletedBy);
		Assertions.assertThat(bankInformation.getDeletedBy()).isEqualTo(deletedBy);

		bankInformation.setDeletedAt(deletedAt);
		Assertions.assertThat(bankInformation.getDeletedAt()).isEqualTo(deletedAt);

		bankInformation.setVerifiedStatus(verifiedStatus);
		Assertions.assertThat(bankInformation.getVerifiedStatus()).isEqualTo(verifiedStatus);

		bankInformation.setCustomerRoleId(customerRole);
		Assertions.assertThat(bankInformation.getCustomerRoleId()).isEqualTo(customerRole);

		bankInformation.setInvarBank(invarBank);
		Assertions.assertThat(bankInformation.getInvarBank()).isEqualTo(invarBank);
	}

}