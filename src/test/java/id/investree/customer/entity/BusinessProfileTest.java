package id.investree.customer.entity;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BusinessProfileTest {
	private BusinessProfile businessProfile;

	private Long id;
	private Long customerId;
	private Date dateOfEstablishment;
	private Integer numberOfEmployee;
	private String companyDescription;
	private String companyAddress;
	private Long province;
	private String provinceName;
	private Long city;
	private String cityName;
	private Long district;
	private String districtName;
	private Long village;
	private String villageName;
	private String postalCode;
	private String landLineNumber;
	private String mobilePrefix;
	private Date createdAt;
	private Long createdBy;
	private Long updatedBy;
	private Date updatedAt;
	private Date fillFinishAt;
	private String groupCompany;
	private String groupDescription;
	private String listOfPayor;
	private String relationshipWithBank;
	private Double averageMonthlySales;
	private Double profitMargin;
	private Integer mailingAddressStatus;
	private Double otherIncome;
	private Double generalProfitMargin;
	private Double profitMarginFromPartner;
	private Double totalNettMargin;
	private Double livingCost;
	private String businessNarration;

	@Before
	public void init() {
		businessProfile = new BusinessProfile();

		id = -999L;
		customerId = -999L;
		dateOfEstablishment = new Date();
		numberOfEmployee = -999;
		companyDescription = "lorem ipsum";
		companyAddress = "lorem ipsum";
		province = 1L;
		provinceName = "lorem ipsum";
		city = 1L;
		cityName = "Lorem Ipsum";
		district = 1L;
		districtName = "Lorem Ipsum";
		village = 1L;
		villageName = "Lorem Ipsum";
		postalCode = "9999";
		landLineNumber = "9999999";
		mobilePrefix = "1";
		createdAt = new Date();
		createdBy = -999L;
		updatedBy = -999L;
		updatedAt = new Date();
		fillFinishAt = new Date();
		groupCompany = "ABCD";
		groupDescription = "Abcd";
		listOfPayor = "abcd";
		relationshipWithBank = "abcd";
		averageMonthlySales = 0.1;
		profitMargin = 1.0;
		mailingAddressStatus = 1;
		otherIncome = 10.5;
		generalProfitMargin = 10.5;
		profitMarginFromPartner = 10.5;
		totalNettMargin = 2D;
		livingCost = 1D;
		businessNarration = "Lorem Ipsum";
	}

	@Test
	public void whenSetBusinessData_thenReturnData() {

		businessProfile.setId(id);
		Assertions.assertThat(businessProfile.getId()).isEqualTo(id);

		businessProfile.setCustomerId(customerId);
		Assertions.assertThat(businessProfile.getCustomerId()).isEqualTo(customerId);

		businessProfile.setDateOfEstablishment(dateOfEstablishment);
		Assertions.assertThat(businessProfile.getDateOfEstablishment()).isEqualTo(dateOfEstablishment);

		businessProfile.setNumberOfEmployee(numberOfEmployee);
		Assertions.assertThat(businessProfile.getNumberOfEmployee()).isEqualTo(numberOfEmployee);

		businessProfile.setCompanyDescription(companyDescription);
		Assertions.assertThat(businessProfile.getCompanyDescription()).isEqualTo(companyDescription);

		businessProfile.setCompanyAddress(companyAddress);
		Assertions.assertThat(businessProfile.getCompanyAddress()).isEqualTo(companyAddress);

		businessProfile.setProvince(province);
		Assertions.assertThat(businessProfile.getProvince()).isEqualTo(province);

		businessProfile.setProvinceName(provinceName);
		Assertions.assertThat(businessProfile.getProvinceName()).isEqualTo(provinceName);

		businessProfile.setCity(city);
		Assertions.assertThat(businessProfile.getCity()).isEqualTo(city);

		businessProfile.setCityName(cityName);
		Assertions.assertThat(businessProfile.getCityName()).isEqualTo(cityName);

		businessProfile.setDistrict(district);
		Assertions.assertThat(businessProfile.getDistrict()).isEqualTo(district);

		businessProfile.setDistrictName(districtName);
		Assertions.assertThat(businessProfile.getDistrictName()).isEqualTo(districtName);

		businessProfile.setVillage(village);
		Assertions.assertThat(businessProfile.getVillage()).isEqualTo(village);

		businessProfile.setVillageName(villageName);
		Assertions.assertThat(businessProfile.getVillageName()).isEqualTo(villageName);

		businessProfile.setPostalCode(postalCode);
		Assertions.assertThat(businessProfile.getPostalCode()).isEqualTo(postalCode);

		businessProfile.setLandLineNumber(landLineNumber);
		Assertions.assertThat(businessProfile.getLandLineNumber()).isEqualTo(landLineNumber);

		businessProfile.setMobilePrefix(mobilePrefix);
		Assertions.assertThat(businessProfile.getMobilePrefix()).isEqualTo(mobilePrefix);

		businessProfile.setCreatedAt(createdAt);
		Assertions.assertThat(businessProfile.getCreatedAt()).isEqualTo(createdAt);

		businessProfile.setCreatedBy(createdBy);
		Assertions.assertThat(businessProfile.getCreatedBy()).isEqualTo(createdBy);

		businessProfile.setUpdatedAt(updatedAt);
		Assertions.assertThat(businessProfile.getUpdatedAt()).isEqualTo(updatedAt);

		businessProfile.setUpdatedBy(updatedBy);
		Assertions.assertThat(businessProfile.getUpdatedBy()).isEqualTo(updatedBy);

		businessProfile.setFillFinishAt(fillFinishAt);
		Assertions.assertThat(businessProfile.getFillFinishAt()).isEqualTo(fillFinishAt);

		businessProfile.setGroupCompany(groupCompany);
		Assertions.assertThat(businessProfile.getGroupCompany()).isEqualTo(groupCompany);

		businessProfile.setGroupDescription(groupDescription);
		Assertions.assertThat(businessProfile.getGroupDescription()).isEqualTo(groupDescription);

		businessProfile.setListOfPayor(listOfPayor);
		Assertions.assertThat(businessProfile.getListOfPayor()).isEqualTo(listOfPayor);

		businessProfile.setRelationshipWithBank(relationshipWithBank);
		Assertions.assertThat(businessProfile.getRelationshipWithBank()).isEqualTo(relationshipWithBank);

		businessProfile.setAverageMonthlySales(averageMonthlySales);
		Assertions.assertThat(businessProfile.getAverageMonthlySales()).isEqualTo(averageMonthlySales);

		businessProfile.setProfitMargin(profitMargin);
		Assertions.assertThat(businessProfile.getProfitMargin()).isEqualTo(profitMargin);

		businessProfile.setMailingAddressStatus(mailingAddressStatus);
		Assertions.assertThat(businessProfile.getMailingAddressStatus()).isEqualTo(mailingAddressStatus);

		businessProfile.setOtherIncome(otherIncome);
		Assertions.assertThat(businessProfile.getOtherIncome()).isEqualTo(otherIncome);

		businessProfile.setGeneralProfitMargin(generalProfitMargin);
		Assertions.assertThat(businessProfile.getGeneralProfitMargin()).isEqualTo(generalProfitMargin);

		businessProfile.setProfitMarginFromPartner(profitMarginFromPartner);
		Assertions.assertThat(businessProfile.getProfitMarginFromPartner()).isEqualTo(profitMarginFromPartner);

		businessProfile.setTotalNettMargin(totalNettMargin);
		Assertions.assertThat(businessProfile.getTotalNettMargin()).isEqualTo(totalNettMargin);

		businessProfile.setLivingCost(livingCost);
		Assertions.assertThat(businessProfile.getLivingCost()).isEqualTo(livingCost);

		businessProfile.setBusinessNarration(businessNarration);
		Assertions.assertThat(businessProfile.getBusinessNarration()).isEqualTo(businessNarration);
	}
}