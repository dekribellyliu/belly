package id.investree.customer.entity;

import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerInformationTest {

	private CustomerInformation customerInformation;

	private Long id;
	private Integer productPreference;
	private String userCategory;
	private String name;
	private Long nationality;
	private Integer legalEntity;
	private String industry;
	private Long annualIncome;
	private Long sourceOfFund;
	private Long investmentObjective;
	private Long riskProfile;
	private Long createdBy;
	private Date createdAt;
	private Long updatedBy;
	private Date updatedAt;
	private String initial;
	private String customerNumber;

	@Before
	public void init() {
		customerInformation = new CustomerInformation();

		id = -999L;
		productPreference = 1;
		userCategory = "1";
		name = "PT APA AJA";
		nationality = 104L;
		legalEntity = 1;
		industry = "A";
		annualIncome = 999L;
		sourceOfFund = 999L;
		investmentObjective = 999L;
		riskProfile = 999L;
		createdBy = -999L;
		createdAt = new Date();
		updatedBy = -999L;
		updatedAt = new Date();
		initial = RandomStringUtils.randomAlphabetic(4);
		customerNumber = "1.1.1019.000001";
	}

	@Test
	public void whenSetCustomerInfo_thenReturnValue() {

		customerInformation.setId(id);
		Assertions.assertThat(customerInformation.getId()).isEqualTo(id);

		customerInformation.setProductPreference(productPreference);
		Assertions.assertThat(customerInformation.getProductPreference()).isEqualTo(productPreference);

		customerInformation.setUserCategory(userCategory);
		Assertions.assertThat(customerInformation.getUserCategory()).isEqualTo(userCategory);

		customerInformation.setName(name);
		Assertions.assertThat(customerInformation.getName()).isEqualTo(name);

		customerInformation.setNationality(nationality);
		Assertions.assertThat(customerInformation.getNationality()).isEqualTo(nationality);

		customerInformation.setLegalEntity(legalEntity);
		Assertions.assertThat(customerInformation.getLegalEntity()).isEqualTo(legalEntity);

		customerInformation.setIndustry(industry);
		Assertions.assertThat(customerInformation.getIndustry()).isEqualTo(industry);

		customerInformation.setAnnualIncome(annualIncome);
		Assertions.assertThat(customerInformation.getAnnualIncome()).isEqualTo(annualIncome);

		customerInformation.setSourceOfFund(sourceOfFund);
		Assertions.assertThat(customerInformation.getSourceOfFund()).isEqualTo(sourceOfFund);

		customerInformation.setInvestmentObjective(investmentObjective);
		Assertions.assertThat(customerInformation.getInvestmentObjective()).isEqualTo(investmentObjective);

		customerInformation.setRiskProfile(riskProfile);
		Assertions.assertThat(customerInformation.getRiskProfile()).isEqualTo(riskProfile);

		customerInformation.setCreatedBy(createdBy);
		Assertions.assertThat(customerInformation.getCreatedBy()).isEqualTo(createdBy);

		customerInformation.setCreatedAt(createdAt);
		Assertions.assertThat(customerInformation.getCreatedAt()).isEqualTo(createdAt);

		customerInformation.setUpdatedBy(updatedBy);
		Assertions.assertThat(customerInformation.getUpdatedBy()).isEqualTo(updatedBy);

		customerInformation.setUpdatedAt(updatedAt);
		Assertions.assertThat(customerInformation.getUpdatedAt()).isEqualTo(updatedAt);

		customerInformation.setInitial(initial);
		Assertions.assertThat(customerInformation.getInitial()).isEqualTo(initial);

		customerInformation.setCustomerNumber(customerNumber);
		Assertions.assertThat(customerInformation.getCustomerNumber()).isEqualTo(customerNumber);
	}

}