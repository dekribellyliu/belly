package id.investree.customer.entity;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerProductListTest {
	private CustomerProductList customerProductList;

	private Long id;
	private Long customerRoleId;
	private Long legacyId;
	private Long type;
	private Long createdBy;
	private Date createdAt;
	private Long customerProductList_updatedBy;
	private Date customerProductList_updatedAt;
	private Long deletedBy;
	private Date deletedAt;

	@Before
	public void init() {
		customerProductList = new CustomerProductList();

		id = -999L;
		customerRoleId = -999L;
		legacyId = -999L;
		type = 1L;
		createdBy = -999L;
		createdAt = new Date();
		customerProductList_updatedBy = -999L;
		customerProductList_updatedAt = new Date();
		deletedBy = -999L;
		deletedAt = new Date();
	}

	@Test
	public void whenSetCustomerProductList_thenReturnValue() {

		customerProductList.setId(id);
		Assertions.assertThat(customerProductList.getId()).isEqualTo(id);

		customerProductList.setCustomerRoleId(customerRoleId);
		Assertions.assertThat(customerProductList.getCustomerRoleId()).isEqualTo(customerRoleId);

		customerProductList.setLegacyId(legacyId);
		Assertions.assertThat(customerProductList.getLegacyId()).isEqualTo(legacyId);

		customerProductList.setType(type);
		Assertions.assertThat(customerProductList.getType()).isEqualTo(type);

		customerProductList.setCreatedBy(createdBy);
		Assertions.assertThat(customerProductList.getCreatedBy()).isEqualTo(createdBy);

		customerProductList.setCreatedAt(createdAt);
		Assertions.assertThat(customerProductList.getCreatedAt()).isEqualTo(createdAt);

		customerProductList.setCustomerProductList_updatedBy(customerProductList_updatedBy);
		Assertions.assertThat(customerProductList.getCustomerProductList_updatedBy()).isEqualTo(customerProductList_updatedBy);

		customerProductList.setCustomerProductList_updatedAt(customerProductList_updatedAt);
		Assertions.assertThat(customerProductList.getCustomerProductList_updatedAt()).isEqualTo(customerProductList_updatedAt);

		customerProductList.setDeletedBy(deletedBy);
		Assertions.assertThat(customerProductList.getDeletedBy()).isEqualTo(deletedBy);

		customerProductList.setDeletedAt(deletedAt);
		Assertions.assertThat(customerProductList.getDeletedAt()).isEqualTo(deletedAt);
	}
}