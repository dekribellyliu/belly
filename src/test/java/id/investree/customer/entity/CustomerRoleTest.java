package id.investree.customer.entity;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerRoleTest {
	private CustomerRole customerRole;

	private Long id;
	private Long customerInformationId;
	private Integer type;
	private Long status;
	private Long createdBy;
	private Date createdAt;
	private Long updatedBy;
	private Date updatedAt;
	private Date registeredAt;
	private String poaFile;
	private Long rmBy;
	private Date activeAt;
	private Long activeBy;
	private Double lsfBullet;
	private Double installment;
	private String feeStatus;
	private String autoWithdrawalStatus;
	private String withHoldingTaxStatus;
	private String customerNumber;

	@Before
	public void init() {
		customerRole = new CustomerRole();

		id = -999L;
		customerInformationId = -123L;
		type = -1;
		status = 1L;
		createdBy = 123L;
		createdAt = new Date();
		updatedBy = -123L;
		updatedAt = new Date();
		registeredAt = new Date();
		poaFile = "lorem ipsum";
		rmBy = -1L;
		activeAt = new Date();
		activeBy = -123L;
		lsfBullet = 1D;
		installment = 1D;
		feeStatus = "Lorem Ipsum";
		autoWithdrawalStatus = "Lorem Ipsum";
		withHoldingTaxStatus = "lorem ipsum";
		customerNumber = "234";
	}

	@Test
	public void whenSetCustomerRole_thenReturnValue() {

		customerRole.setId(id);
		Assertions.assertThat(customerRole.getId()).isEqualTo(id);

		customerRole.setCustomerInformationId(customerInformationId);
		Assertions.assertThat(customerRole.getCustomerInformationId()).isEqualTo(customerInformationId);

		customerRole.setType(type);
		Assertions.assertThat(customerRole.getType()).isEqualTo(type);

		customerRole.setStatus(status);
		Assertions.assertThat(customerRole.getStatus()).isEqualTo(status);

		customerRole.setCreatedBy(createdBy);
		Assertions.assertThat(customerRole.getCreatedBy()).isEqualTo(createdBy);

		customerRole.setCreatedAt(createdAt);
		Assertions.assertThat(customerRole.getCreatedAt()).isEqualTo(createdAt);

		customerRole.setUpdatedBy(updatedBy);
		Assertions.assertThat(customerRole.getUpdatedBy()).isEqualTo(updatedBy);

		customerRole.setUpdatedAt(updatedAt);
		Assertions.assertThat(customerRole.getUpdatedAt()).isEqualTo(updatedAt);

		customerRole.setRegisteredAt(registeredAt);
		Assertions.assertThat(customerRole.getRegisteredAt()).isEqualTo(registeredAt);

		customerRole.setPoaFile(poaFile);
		Assertions.assertThat(customerRole.getPoaFile()).isEqualTo(poaFile);

		customerRole.setRmBy(rmBy);
		Assertions.assertThat(customerRole.getRmBy()).isEqualTo(rmBy);

		customerRole.setId(id);
		Assertions.assertThat(customerRole.getId()).isEqualTo(id);

		customerRole.setId(id);
		Assertions.assertThat(customerRole.getId()).isEqualTo(id);

		customerRole.setId(id);
		Assertions.assertThat(customerRole.getId()).isEqualTo(id);

		customerRole.setId(id);
		Assertions.assertThat(customerRole.getId()).isEqualTo(id);

		customerRole.setId(id);
		Assertions.assertThat(customerRole.getId()).isEqualTo(id);

		customerRole.setId(id);
		Assertions.assertThat(customerRole.getId()).isEqualTo(id);

		customerRole.setId(id);
		Assertions.assertThat(customerRole.getId()).isEqualTo(id);

		customerRole.setId(id);
		Assertions.assertThat(customerRole.getId()).isEqualTo(id);
	}
}
