package id.investree.customer.entity;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmergencyContactTest {
	private EmergencyContact emergencyContact;

	private Long id;
	private Long customerId;
	private Long relationship;
	private String fullname;
	private Long mobilePrefix;
	private String mobileNumber;
	private String emailAddress;
	private String address;
	private Long province;
	private Long city;
	private Long district;
	private Long village;
	private String postalCode;
	private String identityCardUrl;
	private String identityCardNumber;
	private Date identityCardExpired;
	private Long createdBy;
	private Long updateBy;
	private Date updateAt;
	private Date deletedAt;
	private Long deleteBy;

	@Before
	public void init() {
		emergencyContact = new EmergencyContact();

		id = -999L;
		customerId = -999L;
		relationship = 1L;
		fullname = "lorem ipsum";
		mobilePrefix = 1L;
		mobileNumber = "999999999999";
		emailAddress = "lorem@investree.id";
		address = "lorem ipsum";
		province = -999L;
		city = -999L;
		district = -999L;
		village = -999L;
		postalCode = "99999";
		identityCardUrl = "lorem_ipsum.jpg";
		identityCardNumber = "9999999999999999";
		identityCardExpired = new Date();
		createdBy = -999L;
		updateBy = -999L;
		updateAt = new Date();
		deletedAt = new Date();
		deleteBy = -999L;
	}

	@Test
	public void whenSetEmergencyContact_thenReturnValue() {
		emergencyContact.setId(id);
		Assertions.assertThat(emergencyContact.getId()).isEqualTo(id);

		emergencyContact.setCustomerId(customerId);
		Assertions.assertThat(emergencyContact.getCustomerId()).isEqualTo(customerId);

		emergencyContact.setRelationship(relationship);
		Assertions.assertThat(emergencyContact.getRelationship()).isEqualTo(relationship);

		emergencyContact.setFullname(fullname);
		Assertions.assertThat(emergencyContact.getFullname()).isEqualTo(fullname);

		emergencyContact.setMobilePrefix(mobilePrefix);
		Assertions.assertThat(emergencyContact.getMobilePrefix()).isEqualTo(mobilePrefix);

		emergencyContact.setMobileNumber(mobileNumber);
		Assertions.assertThat(emergencyContact.getMobileNumber()).isEqualTo(mobileNumber);

		emergencyContact.setEmailAddress(emailAddress);
		Assertions.assertThat(emergencyContact.getEmailAddress()).isEqualTo(emailAddress);

		emergencyContact.setAddress(address);
		Assertions.assertThat(emergencyContact.getAddress()).isEqualTo(address);

		emergencyContact.setProvince(province);
		Assertions.assertThat(emergencyContact.getProvince()).isEqualTo(province);

		emergencyContact.setCity(city);
		Assertions.assertThat(emergencyContact.getCity()).isEqualTo(city);

		emergencyContact.setDistrict(district);
		Assertions.assertThat(emergencyContact.getDistrict()).isEqualTo(district);

		emergencyContact.setVillage(village);
		Assertions.assertThat(emergencyContact.getVillage()).isEqualTo(village);

		emergencyContact.setPostalCode(postalCode);
		Assertions.assertThat(emergencyContact.getPostalCode()).isEqualTo(postalCode);

		emergencyContact.setIdentityCardUrl(identityCardUrl);
		Assertions.assertThat(emergencyContact.getIdentityCardUrl()).isEqualTo(identityCardUrl);

		emergencyContact.setIdentityCardNumber(identityCardNumber);
		Assertions.assertThat(emergencyContact.getIdentityCardNumber()).isEqualTo(identityCardNumber);

		emergencyContact.setIdentityCardExpired(identityCardExpired);
		Assertions.assertThat(emergencyContact.getIdentityCardExpired()).isEqualTo(identityCardExpired);

		emergencyContact.setCreatedBy(createdBy);
		Assertions.assertThat(emergencyContact.getCreatedBy()).isEqualTo(createdBy);

		emergencyContact.setUpdateBy(updateBy);
		Assertions.assertThat(emergencyContact.getUpdateBy()).isEqualTo(updateBy);

		emergencyContact.setUpdateAt(updateAt);
		Assertions.assertThat(emergencyContact.getUpdateAt()).isEqualTo(updateAt);

		emergencyContact.setDeletedAt(deletedAt);
		Assertions.assertThat(emergencyContact.getDeletedAt()).isEqualTo(deletedAt);

		emergencyContact.setDeleteBy(deleteBy);
		Assertions.assertThat(emergencyContact.getDeleteBy()).isEqualTo(deleteBy);
	}
}