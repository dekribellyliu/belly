package id.investree.customer.entity;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FinancialInformationTest {

	private FinancialInformation financialInformation;

	private Long id;
	private Long customerId;
	private Integer statementFileType;
	private String statementUrl;
	private Date statementFileDate;
	private Date createdAt;
	private Long createdBy;
	private Long updatedBy;
	private Date updatedAt;
	private Date deletedAt;
	private Long deleteBy;

	@Before
	public void init() {
		financialInformation = new FinancialInformation();

		id = -123L;
		customerId = -1234L;
		statementFileType = 10;
		statementUrl = "index.jpg";
		statementFileDate = new Date();
		createdAt = new Date();
		createdBy = -123L;
		updatedBy = -123L;
		updatedAt = new Date();
		deletedAt = new Date();
		deleteBy = -123L;
	}

	@Test
	public void whenSetFinancialInformation_thenReturn() {
		financialInformation.setId(id);
		Assertions.assertThat(financialInformation.getId()).isEqualTo(id);

		financialInformation.setCustomerId(customerId);
		Assertions.assertThat(financialInformation.getCustomerId()).isEqualTo(customerId);

		financialInformation.setStatementFileType(statementFileType);
		Assertions.assertThat(financialInformation.getStatementFileType()).isEqualTo(statementFileType);

		financialInformation.setStatementUrl(statementUrl);
		Assertions.assertThat(financialInformation.getStatementUrl()).isEqualTo(statementUrl);

		financialInformation.setStatementFileDate(statementFileDate);
		Assertions.assertThat(financialInformation.getStatementFileDate()).isEqualTo(statementFileDate);

		financialInformation.setCreatedAt(createdAt);
		Assertions.assertThat(financialInformation.getCreatedAt()).isEqualTo(createdAt);

		financialInformation.setCreatedBy(createdBy);
		Assertions.assertThat(financialInformation.getCreatedBy()).isEqualTo(createdBy);

		financialInformation.setUpdatedBy(updatedBy);
		Assertions.assertThat(financialInformation.getUpdatedBy()).isEqualTo(updatedBy);

		financialInformation.setUpdatedAt(updatedAt);
		Assertions.assertThat(financialInformation.getUpdatedAt()).isEqualTo(updatedAt);

		financialInformation.setDeletedAt(deletedAt);
		Assertions.assertThat(financialInformation.getDeletedAt()).isEqualTo(deletedAt);

		financialInformation.setDeleteBy(deleteBy);
		Assertions.assertThat(financialInformation.getDeleteBy()).isEqualTo(deleteBy);
	}
}
