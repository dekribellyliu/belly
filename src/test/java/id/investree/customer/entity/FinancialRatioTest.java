package id.investree.customer.entity;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FinancialRatioTest {
	private FinancialRatio financialRatio;

	private Long id;
	private Long customerId;
	private Long yearTo;
	private Double gpm;
	private Double npm;
	private Double ardoh;
	private Double invdoh;
	private Double apdoh;
	private Double cashCycle;
	private Double cashRatio;
	private Double ebitda;
	private Double leverage;
	private Double wiNeeds;
	private Double tie;
	private Double dscr;
	private Long createdBy;
	private Date createdAt;
	private Long updateBy;
	private Date updateAt;

	@Before
	public void init() {
		financialRatio = new FinancialRatio();

		id = -999L;
		customerId = -999L;
		yearTo = 1L;
		gpm = 99999d;
		npm = 99999d;
		ardoh = 99999d;
		invdoh = 99999d;
		apdoh = 99999d;
		cashCycle = 99999d;
		cashRatio = 99999d;
		ebitda = 99999d;
		leverage = 99999d;
		wiNeeds = 99999d;
		tie = 99999d;
		dscr = 99999d;
		createdBy = -999L;
		createdAt = new Date();
		updateBy = -999L;
		updateAt = new Date();
	}

	@Test
	public void whenSetFinancialRatio_thenReturnValue() {

		financialRatio.setId(id);
		Assertions.assertThat(financialRatio.getId()).isEqualTo(id);

		financialRatio.setCustomerId(customerId);
		Assertions.assertThat(financialRatio.getCustomerId()).isEqualTo(customerId);

		financialRatio.setYearTo(yearTo);
		Assertions.assertThat(financialRatio.getYearTo()).isEqualTo(yearTo);

		financialRatio.setGpm(gpm);
		Assertions.assertThat(financialRatio.getGpm()).isEqualTo(gpm);

		financialRatio.setNpm(npm);
		Assertions.assertThat(financialRatio.getNpm()).isEqualTo(npm);

		financialRatio.setArdoh(ardoh);
		Assertions.assertThat(financialRatio.getArdoh()).isEqualTo(ardoh);

		financialRatio.setInvdoh(invdoh);
		Assertions.assertThat(financialRatio.getInvdoh()).isEqualTo(invdoh);

		financialRatio.setApdoh(apdoh);
		Assertions.assertThat(financialRatio.getApdoh()).isEqualTo(apdoh);

		financialRatio.setCashCycle(cashCycle);
		Assertions.assertThat(financialRatio.getCashCycle()).isEqualTo(cashCycle);

		financialRatio.setCashRatio(cashRatio);
		Assertions.assertThat(financialRatio.getCashRatio()).isEqualTo(cashRatio);

		financialRatio.setEbitda(ebitda);
		Assertions.assertThat(financialRatio.getEbitda()).isEqualTo(ebitda);

		financialRatio.setLeverage(leverage);
		Assertions.assertThat(financialRatio.getLeverage()).isEqualTo(leverage);

		financialRatio.setWiNeeds(wiNeeds);
		Assertions.assertThat(financialRatio.getWiNeeds()).isEqualTo(wiNeeds);

		financialRatio.setTie(tie);
		Assertions.assertThat(financialRatio.getTie()).isEqualTo(tie);

		financialRatio.setCreatedBy(createdBy);
		Assertions.assertThat(financialRatio.getCreatedBy()).isEqualTo(createdBy);

		financialRatio.setCreatedAt(createdAt);
		Assertions.assertThat(financialRatio.getCreatedAt()).isEqualTo(createdAt);

		financialRatio.setUpdateBy(updateBy);
		Assertions.assertThat(financialRatio.getUpdateBy()).isEqualTo(updateBy);

		financialRatio.setUpdateAt(updateAt);
		Assertions.assertThat(financialRatio.getUpdateAt()).isEqualTo(updateAt);

		financialRatio.setDscr(dscr);
		Assertions.assertThat(financialRatio.getDscr()).isEqualTo(dscr);

	}
}