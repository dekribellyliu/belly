package id.investree.customer.entity;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FinancialStatementTest {
	private FinancialStatement financialStatement;

	private Long id;
	private Long customerId;
	private Long yearTo;
	private Date fiscalYear;
	private Double sales;
	private Double cogs;
	private Double grossProfit;
	private Double sga;
	private Double depreciation;
	private Double operatingProfit;
	private Double interestExpense;
	private Double otherIncome;
	private Double otherExpense;
	private Double profitBeforeTax;
	private Double tax;
	private Double profitAfterTax;
	private Long createdBy;
	private Date createdAt;
	private Long updateBy;
	private Date updateAt;
	private Double existingFacility;

	@Before
	public void init() {
		financialStatement = new FinancialStatement();

		id = -999L;
		customerId = -999L;
		yearTo = -999L;
		fiscalYear = new Date();
		sales = -999d;
		cogs = -999d;
		grossProfit = -999d;
		sga = -999d;
		depreciation = -999d;
		operatingProfit = -999d;
		interestExpense = -999d;
		otherIncome = -999d;
		otherExpense = -999d;
		profitBeforeTax = -999d;
		tax = -999d;
		profitAfterTax = -999d;
		createdBy = -999L;
		createdAt = new Date();
		updateBy = -999L;
		updateAt = new Date();
		existingFacility = -999d;
	}

	@Test
	public void whenSetFinancialStatement_thenReturnValue() {

		financialStatement.setId(id);
		Assertions.assertThat(financialStatement.getId()).isEqualTo(id);

		financialStatement.setCustomerId(customerId);
		Assertions.assertThat(financialStatement.getCustomerId()).isEqualTo(customerId);

		financialStatement.setYearTo(yearTo);
		Assertions.assertThat(financialStatement.getYearTo()).isEqualTo(yearTo);

		financialStatement.setFiscalYear(fiscalYear);
		Assertions.assertThat(financialStatement.getFiscalYear()).isEqualTo(fiscalYear);

		financialStatement.setSales(sales);
		Assertions.assertThat(financialStatement.getSales()).isEqualTo(sales);

		financialStatement.setCogs(cogs);
		Assertions.assertThat(financialStatement.getCogs()).isEqualTo(cogs);

		financialStatement.setGrossProfit(grossProfit);
		Assertions.assertThat(financialStatement.getGrossProfit()).isEqualTo(grossProfit);

		financialStatement.setSga(sga);
		Assertions.assertThat(financialStatement.getSga()).isEqualTo(sga);

		financialStatement.setDepreciation(depreciation);
		Assertions.assertThat(financialStatement.getDepreciation()).isEqualTo(depreciation);

		financialStatement.setOperatingProfit(operatingProfit);
		Assertions.assertThat(financialStatement.getOperatingProfit()).isEqualTo(operatingProfit);

		financialStatement.setInterestExpense(interestExpense);
		Assertions.assertThat(financialStatement.getInterestExpense()).isEqualTo(interestExpense);

		financialStatement.setOtherIncome(otherIncome);
		Assertions.assertThat(financialStatement.getOtherIncome()).isEqualTo(otherIncome);

		financialStatement.setOtherExpense(otherExpense);
		Assertions.assertThat(financialStatement.getOtherExpense()).isEqualTo(otherExpense);

		financialStatement.setProfitBeforeTax(profitBeforeTax);
		Assertions.assertThat(financialStatement.getProfitBeforeTax()).isEqualTo(profitBeforeTax);

		financialStatement.setTax(tax);
		Assertions.assertThat(financialStatement.getTax()).isEqualTo(tax);

		financialStatement.setProfitAfterTax(profitAfterTax);
		Assertions.assertThat(financialStatement.getProfitAfterTax()).isEqualTo(profitAfterTax);

		financialStatement.setCreatedBy(createdBy);
		Assertions.assertThat(financialStatement.getCreatedBy()).isEqualTo(createdBy);

		financialStatement.setCreatedAt(createdAt);
		Assertions.assertThat(financialStatement.getCreatedAt()).isEqualTo(createdAt);

		financialStatement.setUpdateBy(updateBy);
		Assertions.assertThat(financialStatement.getUpdateBy()).isEqualTo(updateBy);

		financialStatement.setUpdateAt(updateAt);
		Assertions.assertThat(financialStatement.getUpdateAt()).isEqualTo(updateAt);

		financialStatement.setExistingFacility(existingFacility);
		Assertions.assertThat(financialStatement.getExistingFacility()).isEqualTo(existingFacility);

	}
}