package id.investree.customer.entity;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FinancialTrendTest {
	private FinancialTrend financialTrend;

	private Long id;
	private Long customerId;
	private String trendPeriod;
	private Double sales;
	private Double cogs;
	private Double grossProfit;
	private Double sga;
	private Double operatingProfit;
	private Long createdBy;
	private Date createdAt;
	private Long updateBy;
	private Date updateAt;
	private Double existingFacility;
	private Double profitBeforeTax;
	private Double profitAfterTax;

	@Before
	public void init() {
		financialTrend = new FinancialTrend();

		id = -999L;
		customerId = -999L;
		trendPeriod = "year to year";
		sales = -999d;
		cogs = -999d;
		grossProfit = -999d;
		sga = -999d;
		operatingProfit = -999d;
		createdBy = -999L;
		createdAt = new Date();
		updateBy = -999L;
		updateAt = new Date();
		existingFacility = -999d;
		profitBeforeTax = -999d;
		profitAfterTax = -999d;
	}

	@Test
	public void whenSetFinancialTrend_thenReturnValue() {

		financialTrend.setId(id);
		Assertions.assertThat(financialTrend.getId()).isEqualTo(id);

		financialTrend.setCustomerId(customerId);
		Assertions.assertThat(financialTrend.getCustomerId()).isEqualTo(customerId);

		financialTrend.setTrendPeriod(trendPeriod);
		Assertions.assertThat(financialTrend.getTrendPeriod()).isEqualTo(trendPeriod);

		financialTrend.setSales(sales);
		Assertions.assertThat(financialTrend.getSales()).isEqualTo(sales);

		financialTrend.setCogs(cogs);
		Assertions.assertThat(financialTrend.getCogs()).isEqualTo(cogs);

		financialTrend.setGrossProfit(grossProfit);
		Assertions.assertThat(financialTrend.getGrossProfit()).isEqualTo(grossProfit);

		financialTrend.setSga(sga);
		Assertions.assertThat(financialTrend.getSga()).isEqualTo(sga);

		financialTrend.setOperatingProfit(operatingProfit);
		Assertions.assertThat(financialTrend.getOperatingProfit()).isEqualTo(operatingProfit);

		financialTrend.setCreatedBy(createdBy);
		Assertions.assertThat(financialTrend.getCreatedBy()).isEqualTo(createdBy);

		financialTrend.setCreatedAt(createdAt);
		Assertions.assertThat(financialTrend.getCreatedAt()).isEqualTo(createdAt);

		financialTrend.setUpdateBy(updateBy);
		Assertions.assertThat(financialTrend.getUpdateBy()).isEqualTo(updateBy);

		financialTrend.setUpdateAt(updateAt);
		Assertions.assertThat(financialTrend.getUpdateAt()).isEqualTo(updateAt);

		financialTrend.setExistingFacility(existingFacility);
		Assertions.assertThat(financialTrend.getExistingFacility()).isEqualTo(existingFacility);

		financialTrend.setProfitBeforeTax(profitBeforeTax);
		Assertions.assertThat(financialTrend.getProfitBeforeTax()).isEqualTo(profitBeforeTax);

		financialTrend.setProfitAfterTax(profitAfterTax);
		Assertions.assertThat(financialTrend.getProfitAfterTax()).isEqualTo(profitAfterTax);
	}
}