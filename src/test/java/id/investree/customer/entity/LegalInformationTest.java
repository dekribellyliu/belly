package id.investree.customer.entity;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LegalInformationTest {

	private LegalInformation legalInformation;

	private Long id;
	private Long customerId;
	private Integer documentType;
	private String documentFile;
	private String documentNumber;
	private Date documentExpired;
	private Date documentRegistered;
	private Long createdBy;
	private Long updateBy;
	private Date createdAt;
	private Date updatedAt;
	private Date fillFinishAt;
	private Date deletedAt;
	private Long deletedBy;

	@Before
	public void init() {
		legalInformation = new LegalInformation();

		id = -999L;
		customerId = -999L;
		documentType = 1;
		documentFile = "lorem ipsum";
		documentNumber = "9999999999";
		documentExpired = new Date();
		documentRegistered = new Date();
		createdBy = -999L;
		updateBy = -999L;
		createdAt = new Date();
		updatedAt = new Date();
		fillFinishAt = new Date();
		deletedAt = new Date();
		deletedBy = -999L;
	}

	@Test
	public void whenSetLegalInfo_theReturnValue() {
		legalInformation.setId(id);
		Assertions.assertThat(legalInformation.getId()).isEqualTo(id);

		legalInformation.setCustomerId(customerId);
		Assertions.assertThat(legalInformation.getCustomerId()).isEqualTo(customerId);

		legalInformation.setDocumentType(documentType);
		Assertions.assertThat(legalInformation.getDocumentType()).isEqualTo(documentType);

		legalInformation.setDocumentFile(documentFile);
		Assertions.assertThat(legalInformation.getDocumentFile()).isEqualTo(documentFile);

		legalInformation.setDocumentNumber(documentNumber);
		Assertions.assertThat(legalInformation.getDocumentNumber()).isEqualTo(documentNumber);

		legalInformation.setDocumentRegistered(documentRegistered);
		Assertions.assertThat(legalInformation.getDocumentRegistered()).isEqualTo(documentRegistered);

		legalInformation.setDocumentExpired(documentExpired);
		Assertions.assertThat(legalInformation.getDocumentExpired()).isEqualTo(documentExpired);

		legalInformation.setCreatedBy(createdBy);
		Assertions.assertThat(legalInformation.getCreatedBy()).isEqualTo(createdBy);

		legalInformation.setCreatedAt(createdAt);
		Assertions.assertThat(legalInformation.getCreatedAt()).isEqualTo(createdAt);

		legalInformation.setUpdateBy(updateBy);
		Assertions.assertThat(legalInformation.getUpdateBy()).isEqualTo(updateBy);

		legalInformation.setUpdatedAt(updatedAt);
		Assertions.assertThat(legalInformation.getUpdatedAt()).isEqualTo(updatedAt);

		legalInformation.setFillFinishAt(fillFinishAt);
		Assertions.assertThat(legalInformation.getFillFinishAt()).isEqualTo(fillFinishAt);

		legalInformation.setDeletedAt(deletedAt);
		Assertions.assertThat(legalInformation.getDeletedAt()).isEqualTo(deletedAt);

		legalInformation.setDeletedBy(deletedBy);
		Assertions.assertThat(legalInformation.getDeletedBy()).isEqualTo(deletedBy);
	}

}