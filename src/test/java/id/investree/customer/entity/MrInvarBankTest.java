package id.investree.customer.entity;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MrInvarBankTest {
	private MrInvarBank mrInvarBank;

	private Long id;
	private Long bankId;
	private String bankName;
	private String bankAccountName;
	private String bankNo;
	private String bankBin;
	private String bankVa;
	private Long type;

	@Before
	public void init() {
		mrInvarBank = new MrInvarBank();

		id = -999L;
		bankId = -999L;
		bankName = "lorem ipsum";
		bankAccountName = "lorem ipsum";
		bankNo = "-999";
		bankBin = "-999";
		bankVa = "-999";
		type = 1L;
	}

	@Test
	public void whenSetMrInvarBank_thenReturnValue() {

		mrInvarBank.setId(id);
		Assertions.assertThat(mrInvarBank.getId()).isEqualTo(id);

		mrInvarBank.setBankId(bankId);
		Assertions.assertThat(mrInvarBank.getBankId()).isEqualTo(bankId);

		mrInvarBank.setBankName(bankName);
		Assertions.assertThat(mrInvarBank.getBankName()).isEqualTo(bankName);

		mrInvarBank.setBankAccountName(bankAccountName);
		Assertions.assertThat(mrInvarBank.getBankAccountName()).isEqualTo(bankAccountName);

		mrInvarBank.setBankNo(bankNo);
		Assertions.assertThat(mrInvarBank.getBankNo()).isEqualTo(bankNo);

		mrInvarBank.setBankBin(bankBin);
		Assertions.assertThat(mrInvarBank.getBankBin()).isEqualTo(bankBin);

		mrInvarBank.setBankVa(bankVa);
		Assertions.assertThat(mrInvarBank.getBankVa()).isEqualTo(bankVa);

		mrInvarBank.setType(type);
		Assertions.assertThat(mrInvarBank.getType()).isEqualTo(type);

	}
}