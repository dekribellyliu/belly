package id.investree.customer.entity;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PartnershipInformationTest {
	private PartnershipInformation partnershipInformation;

	private Long id;
	private Long customerId;
	private Long type;
	private Long partnerId;
	private String name;
	private Long category;
	private Date startOfRelation;
	private String sellerId;
	private String sellerLink;
	private Long sellerLocation;
	private Long buildingLocOwnership;
	private String lengthOfOwnership;
	private String internalRating;
	private String externalRating;
	private Long createdBy;
	private Date createdAt;
	private Long updateBy;
	private Date updateAt;

	@Before
	public void init() {
		partnershipInformation = new PartnershipInformation();

		id = -999L;
		customerId = -999L;
		type = -999L;
		partnerId = -999L;
		name = "lorem ipsum";
		category = -999L;
		startOfRelation = new Date();
		sellerId = "lorem ipsum";
		sellerLink = "https://google.com";
		sellerLocation = -999L;
		buildingLocOwnership = -999L;
		lengthOfOwnership = "-999L";
		internalRating = "-999L";
		externalRating = "-999L";
		createdBy = -999L;
		createdAt = new Date();
		updateBy = -999L;
		updateAt = new Date();
	}

	@Test
	public void whenSetPartnershipInfo_thenReturnValue() {

		partnershipInformation.setId(id);
		Assertions.assertThat(partnershipInformation.getId()).isEqualTo(id);

		partnershipInformation.setCustomerId(customerId);
		Assertions.assertThat(partnershipInformation.getCustomerId()).isEqualTo(customerId);

		partnershipInformation.setType(type);
		Assertions.assertThat(partnershipInformation.getType()).isEqualTo(type);

		partnershipInformation.setPartnerId(partnerId);
		Assertions.assertThat(partnershipInformation.getPartnerId()).isEqualTo(partnerId);

		partnershipInformation.setName(name);
		Assertions.assertThat(partnershipInformation.getName()).isEqualTo(name);

		partnershipInformation.setCategory(category);
		Assertions.assertThat(partnershipInformation.getCategory()).isEqualTo(category);

		partnershipInformation.setStartOfRelation(startOfRelation);
		Assertions.assertThat(partnershipInformation.getStartOfRelation()).isEqualTo(startOfRelation);

		partnershipInformation.setSellerId(sellerId);
		Assertions.assertThat(partnershipInformation.getSellerId()).isEqualTo(sellerId);

		partnershipInformation.setSellerLink(sellerLink);
		Assertions.assertThat(partnershipInformation.getSellerLink()).isEqualTo(sellerLink);

		partnershipInformation.setSellerLocation(sellerLocation);
		Assertions.assertThat(partnershipInformation.getSellerLocation()).isEqualTo(sellerLocation);

		partnershipInformation.setBuildingLocOwnership(buildingLocOwnership);
		Assertions.assertThat(partnershipInformation.getBuildingLocOwnership()).isEqualTo(buildingLocOwnership);

		partnershipInformation.setLengthOfOwnership(lengthOfOwnership);
		Assertions.assertThat(partnershipInformation.getLengthOfOwnership()).isEqualTo(lengthOfOwnership);

		partnershipInformation.setInternalRating(internalRating);
		Assertions.assertThat(partnershipInformation.getInternalRating()).isEqualTo(internalRating);

		partnershipInformation.setExternalRating(externalRating);
		Assertions.assertThat(partnershipInformation.getExternalRating()).isEqualTo(externalRating);

		partnershipInformation.setCreatedBy(createdBy);
		Assertions.assertThat(partnershipInformation.getCreatedBy()).isEqualTo(createdBy);

		partnershipInformation.setCreatedAt(createdAt);
		Assertions.assertThat(partnershipInformation.getCreatedAt()).isEqualTo(createdAt);

		partnershipInformation.setUpdateBy(updateBy);
		Assertions.assertThat(partnershipInformation.getUpdateBy()).isEqualTo(updateBy);

		partnershipInformation.setUpdateAt(updateAt);
		Assertions.assertThat(partnershipInformation.getUpdateAt()).isEqualTo(updateAt);
	}
}