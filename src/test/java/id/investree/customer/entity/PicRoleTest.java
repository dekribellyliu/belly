package id.investree.customer.entity;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PicRoleTest {
	private PicRole picRole;

	private Long id;
	private Long customerRoleId;
	private Long loginId;
	private Integer role;
	private String status;
	private Long createdBy;
	private Date createdAt;
	private Long updateBy;
	private Date updateAt;
	private Long deletedBy;
	private Date deletedAt;

	@Before
	public void init() {
		picRole = new PicRole();

		id = -999L;
		customerRoleId = -999L;
		loginId = -999L;
		role = -999;
		status = "lorem";
		createdBy = -999L;
		createdAt = new Date();
		updateBy = -999L;
		updateAt = new Date();
		deletedBy = -999L;
		deletedAt = new Date();
	}

	@Test
	public void whenSetPicRole_thenReturnValue() {

		picRole.setId(id);
		Assertions.assertThat(picRole.getId()).isEqualTo(id);

		picRole.setCustomerRoleId(customerRoleId);
		Assertions.assertThat(picRole.getCustomerRoleId()).isEqualTo(customerRoleId);

		picRole.setLoginId(loginId);
		Assertions.assertThat(picRole.getLoginId()).isEqualTo(loginId);

		picRole.setRole(role);
		Assertions.assertThat(picRole.getRole()).isEqualTo(role);

		picRole.setStatus(status);
		Assertions.assertThat(picRole.getStatus()).isEqualTo(status);

		picRole.setCreatedBy(createdBy);
		Assertions.assertThat(picRole.getCreatedBy()).isEqualTo(createdBy);

		picRole.setCreatedAt(createdAt);
		Assertions.assertThat(picRole.getCreatedAt()).isEqualTo(createdAt);

		picRole.setUpdateBy(updateBy);
		Assertions.assertThat(picRole.getUpdateBy()).isEqualTo(updateBy);

		picRole.setUpdateAt(updateAt);
		Assertions.assertThat(picRole.getUpdateAt()).isEqualTo(updateAt);

		picRole.setDeletedBy(deletedBy);
		Assertions.assertThat(picRole.getDeletedBy()).isEqualTo(deletedBy);

		picRole.setDeletedAt(deletedAt);
		Assertions.assertThat(picRole.getDeletedAt()).isEqualTo(deletedAt);

	}
}