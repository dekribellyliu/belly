package id.investree.customer.entity;

import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ReferralDataTest {
	private ReferralData referralData;

	private Long id;
	private Long referralUserId;
	private Long referrerUserId;
	private String referrerCode;
	private Date referralExpiredDate;
	private Long knowInvestreeFrom;

	@Before
	public void init() {
		referralData = new ReferralData();

		id = -999L;
		referralUserId = -999L;
		referrerUserId = -999L;
		referrerCode = RandomStringUtils.randomAlphabetic(6);
		referralExpiredDate = Date.from(ZonedDateTime.now(ZoneId.of("Asia/Jakarta")).plusMonths(6).toInstant());
		knowInvestreeFrom = -999L;
	}

	@Test
	public void whenSetReferralData_thenReturnValue() {

		referralData.setId(id);
		Assertions.assertThat(referralData.getId()).isEqualTo(id);

		referralData.setReferralUserId(referralUserId);
		Assertions.assertThat(referralData.getReferralUserId()).isEqualTo(referralUserId);

		referralData.setReferrerUserId(referrerUserId);
		Assertions.assertThat(referralData.getReferrerUserId()).isEqualTo(referrerUserId);

		referralData.setReferralExpiredDate(referralExpiredDate);
		Assertions.assertThat(referralData.getReferralExpiredDate()).isEqualTo(referralExpiredDate);

		referralData.setReferrerCode(referrerCode);
		Assertions.assertThat(referralData.getReferrerCode()).isEqualTo(referrerCode);

		referralData.setKnowInvestreeFrom(knowInvestreeFrom);
		Assertions.assertThat(referralData.getKnowInvestreeFrom()).isEqualTo(knowInvestreeFrom);

	}
}