package id.investree.customer.entity;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ReferreralDataTest {

	private Long id;
	private Long referralUserId;
	private Long referrerUserId;
	private String referrerCode;
	private Date referralExpiredDate;
	private Long knowInvestreeFrom;

	@Before
	public void init() {
		id = -1L;
		referralUserId = -123L;
		referrerUserId = -999L;
		referrerCode = "abcd1234";
		referralExpiredDate = Date.from(ZonedDateTime.now(ZoneId.of("Asia/Jakarta")).plusMonths(6).toInstant());
		knowInvestreeFrom = -1L;
	}

	@Test
	public void testReferralData() {
		ReferralData referralData = new ReferralData();

		referralData.setId(id);
		Assertions.assertThat(referralData.getId()).isNotNull();

		referralData.setReferralUserId(referralUserId);
		Assertions.assertThat(referralData.getReferralUserId()).isNotNull();

		referralData.setReferrerUserId(referrerUserId);
		Assertions.assertThat(referralData.getReferrerUserId()).isNotNull();

		referralData.setReferrerCode(referrerCode);
		Assertions.assertThat(referralData.getReferrerCode()).isNotNull();

		referralData.setReferralExpiredDate(referralExpiredDate);
		Assertions.assertThat(referralData.getReferralExpiredDate()).isNotNull();

		referralData.setKnowInvestreeFrom(knowInvestreeFrom);
		Assertions.assertThat(referralData.getKnowInvestreeFrom()).isNotNull();
	}
}
