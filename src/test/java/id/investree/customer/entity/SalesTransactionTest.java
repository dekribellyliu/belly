package id.investree.customer.entity;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SalesTransactionTest {
	private SalesTransaction salesTransaction;

	private Long id;
	private Long customerId;
	private Date dates;
	private Double amount;
	private Double transaction;
	private Long createdBy;
	private Date createdAt;
	private Long updateBy;
	private Date updateAt;
	private Date deletedAt;
	private Long deletedBy;

	@Before
	public void init() {
		salesTransaction = new SalesTransaction();

		id = -999L;
		customerId = -999L;
		dates = new Date();
		amount = -999d;
		transaction = -999d;
		createdBy = -999L;
		createdAt = new Date();
		updateBy = -999L;
		updateAt = new Date();
		deletedAt = new Date();
		deletedBy = -999L;
	}

	@Test
	public void whenSetSalesTransaction_thenReturnValue() {

		salesTransaction.setId(id);
		Assertions.assertThat(salesTransaction.getId()).isEqualTo(id);

		salesTransaction.setCustomerId(customerId);
		Assertions.assertThat(salesTransaction.getCustomerId()).isEqualTo(customerId);

		salesTransaction.setDate(dates);
		Assertions.assertThat(salesTransaction.getDate()).isEqualTo(dates);

		salesTransaction.setAmount(amount);
		Assertions.assertThat(salesTransaction.getAmount()).isEqualTo(amount);

		salesTransaction.setTransaction(transaction);
		Assertions.assertThat(salesTransaction.getTransaction()).isEqualTo(transaction);

		salesTransaction.setCreatedBy(createdBy);
		Assertions.assertThat(salesTransaction.getCreatedBy()).isEqualTo(createdBy);

		salesTransaction.setCreatedAt(createdAt);
		Assertions.assertThat(salesTransaction.getCreatedAt()).isEqualTo(createdAt);

		salesTransaction.setUpdateBy(updateBy);
		Assertions.assertThat(salesTransaction.getUpdateBy()).isEqualTo(updateBy);

		salesTransaction.setUpdateAt(updateAt);
		Assertions.assertThat(salesTransaction.getUpdateAt()).isEqualTo(updateAt);

		salesTransaction.setDeletedAt(deletedAt);
		Assertions.assertThat(salesTransaction.getDeletedAt()).isEqualTo(deletedAt);

		salesTransaction.setDeletedBy(deletedBy);
		Assertions.assertThat(salesTransaction.getDeletedBy()).isEqualTo(deletedBy);

	}
}