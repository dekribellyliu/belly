package id.investree.customer.entity;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ShareholdersInformationTest {

	private ShareholdersInformation shareholdersInformation;
	private Long id;
	private Long customerId;
	private Long position;
	private String fullName;
	private Long mobilePrefix;
	private String mobileNumber;
	private String emailAddress;
	private Float stockOwnership;
	private Date dob;
	private String identificationCardUrl;
	private String identificationCardNumber;
	private Date identificationCardExpiryDate;
	private String selfieUrl;
	private String taxCardUrl;
	private String taxCardNumber;
	private String isLss;
	private String isPgs;
	private String isTss;
	private Date createdAt;
	private Long createdBy;
	private Long updatedBy;
	private Date updatedAt;
	private Date fillFinishAt;
	private Long deletedBy;
	private Date deletedAt;
	private Date apuPptDate;
	private String apuPptFile;
	private String apuPptResult;
	private String pgNumber;
	private Long pgAmount;
	private Date pgSignedDate;
	private String pgType;
	private String pgFile;

	@Before
	public void init() {
		shareholdersInformation = new ShareholdersInformation();

		id = -999L;
		customerId = -999L;
		position = -999L;
		fullName = "lorem ipsum";
		mobilePrefix = 1L;
		mobileNumber = "99999999";
		emailAddress = "lorem@investree.id";
		stockOwnership = 100f;
		dob = new Date();
		identificationCardUrl = "lorem_ipsum.jpg";
		identificationCardNumber = "999999999999";
		identificationCardExpiryDate = new Date();
		selfieUrl = "lorem_ipsum.jpg";
		taxCardUrl = "lorem_ipsum.jpg";
		taxCardNumber = "999999999";
		isLss = "Y";
		isPgs = "Y";
		isTss = "Y";
		createdAt = new Date();
		createdBy = -999L;
		updatedBy = -999L;
		updatedAt = new Date();
		fillFinishAt = new Date();
		deletedBy = -999L;
		deletedAt = new Date();
		apuPptDate = new Date();
		apuPptFile = "lorem_ipsum.jpg";
		apuPptResult = "lorem ipsum";
		pgNumber = "99999999";
		pgAmount = 999999L;
		pgSignedDate = new Date();
		pgType = "lorem";
		pgFile = "lorem_ipsum.jpg";
	}

	@Test
	public void whenSetShareholderInfo_thenReturnValue() {
		shareholdersInformation.setId(id);
		Assertions.assertThat(shareholdersInformation.getId()).isEqualTo(id);

		shareholdersInformation.setCustomerId(customerId);
		Assertions.assertThat(shareholdersInformation.getCustomerId()).isEqualTo(customerId);

		shareholdersInformation.setPosition(position);
		Assertions.assertThat(shareholdersInformation.getPosition()).isEqualTo(position);

		shareholdersInformation.setFullName(fullName);
		Assertions.assertThat(shareholdersInformation.getFullName()).isEqualTo(fullName);

		shareholdersInformation.setMobilePrefix(mobilePrefix);
		Assertions.assertThat(shareholdersInformation.getMobilePrefix()).isEqualTo(mobilePrefix);

		shareholdersInformation.setMobileNumber(mobileNumber);
		Assertions.assertThat(shareholdersInformation.getMobileNumber()).isEqualTo(mobileNumber);

		shareholdersInformation.setEmailAddress(emailAddress);
		Assertions.assertThat(shareholdersInformation.getEmailAddress()).isEqualTo(emailAddress);

		shareholdersInformation.setStockOwnership(stockOwnership);
		Assertions.assertThat(shareholdersInformation.getStockOwnership()).isEqualTo(stockOwnership);

		shareholdersInformation.setDob(dob);
		Assertions.assertThat(shareholdersInformation.getDob()).isEqualTo(dob);

		shareholdersInformation.setIdentificationCardUrl(identificationCardUrl);
		Assertions.assertThat(shareholdersInformation.getIdentificationCardUrl()).isEqualTo(identificationCardUrl);

		shareholdersInformation.setIdentificationCardNumber(identificationCardNumber);
		Assertions.assertThat(shareholdersInformation.getIdentificationCardNumber()).isEqualTo(identificationCardNumber);

		shareholdersInformation.setIdentificationCardExpiryDate(identificationCardExpiryDate);
		Assertions.assertThat(shareholdersInformation.getIdentificationCardExpiryDate()).isEqualTo(identificationCardExpiryDate);

		shareholdersInformation.setSelfieUrl(selfieUrl);
		Assertions.assertThat(shareholdersInformation.getSelfieUrl()).isEqualTo(selfieUrl);

		shareholdersInformation.setTaxCardUrl(taxCardUrl);
		Assertions.assertThat(shareholdersInformation.getTaxCardUrl()).isEqualTo(taxCardUrl);

		shareholdersInformation.setTaxCardNumber(taxCardNumber);
		Assertions.assertThat(shareholdersInformation.getTaxCardNumber()).isEqualTo(taxCardNumber);

		shareholdersInformation.setIsLss(isLss);
		Assertions.assertThat(shareholdersInformation.getIsLss()).isEqualTo(isLss);

		shareholdersInformation.setIsPgs(isPgs);
		Assertions.assertThat(shareholdersInformation.getIsPgs()).isEqualTo(isPgs);

		shareholdersInformation.setIsTss(isTss);
		Assertions.assertThat(shareholdersInformation.getIsTss()).isEqualTo(isTss);

		shareholdersInformation.setCreatedBy(createdBy);
		Assertions.assertThat(shareholdersInformation.getCreatedBy()).isEqualTo(createdBy);

		shareholdersInformation.setCreatedAt(createdAt);
		Assertions.assertThat(shareholdersInformation.getCreatedAt()).isEqualTo(createdAt);

		shareholdersInformation.setUpdatedBy(updatedBy);
		Assertions.assertThat(shareholdersInformation.getUpdatedBy()).isEqualTo(updatedBy);

		shareholdersInformation.setUpdatedAt(updatedAt);
		Assertions.assertThat(shareholdersInformation.getUpdatedAt()).isEqualTo(updatedAt);

		shareholdersInformation.setFillFinishAt(fillFinishAt);
		Assertions.assertThat(shareholdersInformation.getFillFinishAt()).isEqualTo(fillFinishAt);

		shareholdersInformation.setDeletedBy(deletedBy);
		Assertions.assertThat(shareholdersInformation.getDeletedBy()).isEqualTo(deletedBy);

		shareholdersInformation.setDeletedAt(deletedAt);
		Assertions.assertThat(shareholdersInformation.getDeletedAt()).isEqualTo(deletedAt);

		shareholdersInformation.setPgNumber(pgNumber);
		Assertions.assertThat(shareholdersInformation.getPgNumber()).isEqualTo(pgNumber);

		shareholdersInformation.setPgAmount(pgAmount);
		Assertions.assertThat(shareholdersInformation.getPgAmount()).isEqualTo(pgAmount);

		shareholdersInformation.setPgSignedDate(pgSignedDate);
		Assertions.assertThat(shareholdersInformation.getPgSignedDate()).isEqualTo(pgSignedDate);

		shareholdersInformation.setPgType(pgType);
		Assertions.assertThat(shareholdersInformation.getPgType()).isEqualTo(pgType);

		shareholdersInformation.setPgFile(pgFile);
		Assertions.assertThat(shareholdersInformation.getPgFile()).isEqualTo(pgFile);

		shareholdersInformation.setApuPptDate(apuPptDate);
		Assertions.assertThat(shareholdersInformation.getApuPptDate()).isEqualTo(apuPptDate);

		shareholdersInformation.setApuPptFile(apuPptFile);
		Assertions.assertThat(shareholdersInformation.getApuPptFile()).isEqualTo(apuPptFile);

		shareholdersInformation.setApuPptResult(apuPptResult);
		Assertions.assertThat(shareholdersInformation.getApuPptResult()).isEqualTo(apuPptResult);

	}
}