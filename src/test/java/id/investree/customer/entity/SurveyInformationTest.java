package id.investree.customer.entity;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SurveyInformationTest {
	private SurveyInformation surveyInformation;

	private Long id;
	private Long customerId;
	private Long loginId;
	private Date surveyDate;
	private Long borrowerPosition;
	private String borrowerName;
	private Long numberOfEmployees;
	private Long officeStatus;
	private String lengthOfStay;
	private String filename;
	private String resultDescription;
	private String address;
	private Long createdBy;
	private Date createdAt;
	private Long updateBy;
	private Date updateAt;
	private Long deletedBy;
	private Date deletedAt;

	@Before
	public void init() {
		surveyInformation = new SurveyInformation();

		id = -999L;
		customerId = -999L;
		loginId = -999L;
		surveyDate = new Date();
		borrowerPosition = -999L;
		borrowerName = "lorem ipsum";
		numberOfEmployees = -999L;
		officeStatus = -999L;
		lengthOfStay = "-999L";
		filename = "lorem_ipsum.jpg";
		resultDescription = "lorem ipsum";
		address = "lorem ipsum";
		createdBy = -999L;
		createdAt = new Date();
		updateBy = -999L;
		updateAt = new Date();
		deletedBy = -999L;
		deletedAt = new Date();
	}

	@Test
	public void whenSetSurveyInfo_thenReturnValue() {

		surveyInformation.setId(id);
		Assertions.assertThat(surveyInformation.getId()).isEqualTo(id);

		surveyInformation.setCustomerId(customerId);
		Assertions.assertThat(surveyInformation.getCustomerId()).isEqualTo(customerId);

		surveyInformation.setLoginId(loginId);
		Assertions.assertThat(surveyInformation.getLoginId()).isEqualTo(loginId);

		surveyInformation.setSurveyDate(surveyDate);
		Assertions.assertThat(surveyInformation.getSurveyDate()).isEqualTo(surveyDate);

		surveyInformation.setBorrowerPosition(borrowerPosition);
		Assertions.assertThat(surveyInformation.getBorrowerPosition()).isEqualTo(borrowerPosition);

		surveyInformation.setBorrowerName(borrowerName);
		Assertions.assertThat(surveyInformation.getBorrowerName()).isEqualTo(borrowerName);

		surveyInformation.setNumberOfEmployees(numberOfEmployees);
		Assertions.assertThat(surveyInformation.getNumberOfEmployees()).isEqualTo(numberOfEmployees);

		surveyInformation.setOfficeStatus(officeStatus);
		Assertions.assertThat(surveyInformation.getOfficeStatus()).isEqualTo(officeStatus);

		surveyInformation.setLengthOfStay(lengthOfStay);
		Assertions.assertThat(surveyInformation.getLengthOfStay()).isEqualTo(lengthOfStay);

		surveyInformation.setFilename(filename);
		Assertions.assertThat(surveyInformation.getFilename()).isEqualTo(filename);

		surveyInformation.setResultDescription(resultDescription);
		Assertions.assertThat(surveyInformation.getResultDescription()).isEqualTo(resultDescription);

		surveyInformation.setAddress(address);
		Assertions.assertThat(surveyInformation.getAddress()).isEqualTo(address);

		surveyInformation.setCreatedBy(createdBy);
		Assertions.assertThat(surveyInformation.getCreatedBy()).isEqualTo(createdBy);

		surveyInformation.setCreatedAt(createdAt);
		Assertions.assertThat(surveyInformation.getCreatedAt()).isEqualTo(createdAt);

		surveyInformation.setUpdateBy(updateBy);
		Assertions.assertThat(surveyInformation.getUpdateBy()).isEqualTo(updateBy);

		surveyInformation.setUpdateAt(updateAt);
		Assertions.assertThat(surveyInformation.getUpdateAt()).isEqualTo(updateAt);

		surveyInformation.setDeletedBy(deletedBy);
		Assertions.assertThat(surveyInformation.getDeletedBy()).isEqualTo(deletedBy);

		surveyInformation.setDeletedAt(deletedAt);
		Assertions.assertThat(surveyInformation.getDeletedAt()).isEqualTo(deletedAt);

	}
}