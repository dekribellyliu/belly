package id.investree.customer.entity;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserDataTest {
	private UserData userData;

	private Long id;
	private Long loginDataId;
	private Long customerId;
	private Long maritalStatus;
	private Long companyPosition;
	private Long companyDivision;
	private String selfieFile;
	private String idCardFile;
	private String selfieKtpFile;
	private String signatureFile;
	private String idCardNumber;
	private Date idCardExpired;
	private Long nationality;
	private String sameAddress;
	private String ktpAddress;
	private Long ktpProvince;
	private Long ktpCity;
	private Long ktpDistrict;
	private Long ktpVillage;
	private String ktpPostalCode;
	private Long placeOfBirth;
	private String placeOfBirthExternal;
	private Date dateOfBirth;
	private Long religion;
	private Long education;
	private Long occupation;
	private Long fieldOfWork;
	private String domicileAddress;
	private Long domicileProvince;
	private String domicileProvinceName;
	private Long domicileCity;
	private String domicileCityName;
	private Long domicileDistrict;
	private String domicileDistrictName;
	private Long domicileVillage;
	private String domicileVillageName;
	private String domicilePostalCode;
	private String partnerProvince;
	private String partnerCity;
	private String partnerDistrict;
	private String partnerVillage;
	private String partnerPostalCode;
	private String motherName;
	private Long createdBy;
	private Date createdAt;
	private Long updatedBy;
	private Date updatedAt;
	private Date fillFinishAt;

	@Before
	public void init() {
		userData = new UserData();

		id = -999L;
		loginDataId = -999L;
		customerId = -999L;
		maritalStatus = -999L;
		companyPosition = -999L;
		companyDivision = -999L;
		selfieFile = "lorem_ipsum.jpg";
		idCardFile = "lorem_ipsum.jpg";
		selfieKtpFile = "lorem_ipsum.jpg";
		signatureFile = "lorem_ipsum.jpg";
		idCardNumber = "-999L";
		idCardExpired = new Date();
		nationality = -999L;
		sameAddress = "Y";
		ktpAddress = "lorem ipsum";
		ktpProvince = -999L;
		ktpCity = -999L;
		ktpDistrict = -999L;
		ktpVillage = -999L;
		ktpPostalCode = "-99999";
		placeOfBirth = -999L;
		placeOfBirthExternal = "lorem ipsum";
		dateOfBirth = new Date();
		religion = -999L;
		education = -999L;
		occupation = -999L;
		fieldOfWork = -999L;
		domicileAddress = "lorem ipsum";
		domicileProvince = -999L;
		domicileProvinceName = "lorem ipsum";
		domicileCity = -999L;
		domicileCityName = "lorem ipsum";
		domicileDistrict = -999L;
		domicileDistrictName = "lorem ipsum";
		domicileVillage = -999L;
		domicileVillageName = "lorem ipsum";
		domicilePostalCode = "-99999";
		partnerProvince = "Test Partner Province";
		partnerCity = "Test partner City";
		partnerDistrict = "Test Partner District";
		partnerVillage = "Test partner village";
		partnerPostalCode = "Lorem ipsum";
		motherName = "lorem ipsum";
		createdBy = -999L;
		createdAt = new Date();
		updatedBy = -999L;
		updatedAt = new Date();
		fillFinishAt = new Date();
	}

	@Test
	public void whenSetUserData_thenReturnValue() {

		userData.setId(id);
		Assertions.assertThat(userData.getId()).isEqualTo(id);

		userData.setLoginDataId(loginDataId);
		Assertions.assertThat(userData.getLoginDataId()).isEqualTo(loginDataId);

		userData.setCustomerId(customerId);
		Assertions.assertThat(userData.getCustomerId()).isEqualTo(customerId);

		userData.setMaritalStatus(maritalStatus);
		Assertions.assertThat(userData.getMaritalStatus()).isEqualTo(maritalStatus);

		userData.setCompanyPosition(companyPosition);
		Assertions.assertThat(userData.getCompanyPosition()).isEqualTo(companyPosition);

		userData.setCompanyDivision(companyDivision);
		Assertions.assertThat(userData.getCompanyDivision()).isEqualTo(companyDivision);

		userData.setSelfieFile(selfieFile);
		Assertions.assertThat(userData.getSelfieFile()).isEqualTo(selfieFile);

		userData.setIdCardFile(idCardFile);
		Assertions.assertThat(userData.getIdCardFile()).isEqualTo(idCardFile);

		userData.setIdCardNumber(selfieKtpFile);
		Assertions.assertThat(userData.getIdCardNumber()).isEqualTo(selfieKtpFile);

		userData.setSignatureFile(signatureFile);
		Assertions.assertThat(userData.getSignatureFile()).isEqualTo(signatureFile);

		userData.setIdCardNumber(idCardNumber);
		Assertions.assertThat(userData.getIdCardNumber()).isEqualTo(idCardNumber);

		userData.setIdCardExpired(idCardExpired);
		Assertions.assertThat(userData.getIdCardExpired()).isEqualTo(idCardExpired);

		userData.setNationality(nationality);
		Assertions.assertThat(userData.getNationality()).isEqualTo(nationality);

		userData.setSameAddress(sameAddress);
		Assertions.assertThat(userData.getSameAddress()).isEqualTo(sameAddress);

		userData.setKtpAddress(ktpAddress);
		Assertions.assertThat(userData.getKtpAddress()).isEqualTo(ktpAddress);

		userData.setKtpProvince(ktpProvince);
		Assertions.assertThat(userData.getKtpProvince()).isEqualTo(ktpProvince);

		userData.setKtpCity(ktpCity);
		Assertions.assertThat(userData.getKtpCity()).isEqualTo(ktpCity);

		userData.setKtpDistrict(ktpDistrict);
		Assertions.assertThat(userData.getKtpDistrict()).isEqualTo(ktpDistrict);

		userData.setKtpVillage(ktpVillage);
		Assertions.assertThat(userData.getKtpVillage()).isEqualTo(ktpVillage);

		userData.setKtpPostalCode(ktpPostalCode);
		Assertions.assertThat(userData.getKtpPostalCode()).isEqualTo(ktpPostalCode);

		userData.setPlaceOfBirth(placeOfBirth);
		Assertions.assertThat(userData.getPlaceOfBirth()).isEqualTo(placeOfBirth);

		userData.setPlaceOfBirthExternal(placeOfBirthExternal);
		Assertions.assertThat(userData.getPlaceOfBirthExternal()).isEqualTo(placeOfBirthExternal);

		userData.setDateOfBirth(dateOfBirth);
		Assertions.assertThat(userData.getDateOfBirth()).isEqualTo(dateOfBirth);

		userData.setReligion(religion);
		Assertions.assertThat(userData.getReligion()).isEqualTo(religion);

		userData.setEducation(education);
		Assertions.assertThat(userData.getEducation()).isEqualTo(education);

		userData.setOccupation(occupation);
		Assertions.assertThat(userData.getOccupation()).isEqualTo(occupation);

		userData.setFieldOfWork(fieldOfWork);
		Assertions.assertThat(userData.getFieldOfWork()).isEqualTo(fieldOfWork);

		userData.setDomicileAddress(domicileAddress);
		Assertions.assertThat(userData.getDomicileAddress()).isEqualTo(domicileAddress);

		userData.setDomicileProvince(domicileProvince);
		Assertions.assertThat(userData.getDomicileProvince()).isEqualTo(domicileProvince);

		userData.setDomicileProvinceName(domicileProvinceName);
		Assertions.assertThat(userData.getDomicileProvinceName()).isEqualTo(domicileProvinceName);

		userData.setDomicileCity(domicileCity);
		Assertions.assertThat(userData.getDomicileCity()).isEqualTo(domicileCity);

		userData.setDomicileCityName(domicileCityName);
		Assertions.assertThat(userData.getDomicileCityName()).isEqualTo(domicileCityName);

		userData.setDomicileDistrict(domicileDistrict);
		Assertions.assertThat(userData.getDomicileDistrict()).isEqualTo(domicileDistrict);

		userData.setDomicileDistrictName(domicileDistrictName);
		Assertions.assertThat(userData.getDomicileDistrictName()).isEqualTo(domicileDistrictName);

		userData.setDomicileVillage(domicileVillage);
		Assertions.assertThat(userData.getDomicileVillage()).isEqualTo(domicileVillage);

		userData.setDomicileVillageName(domicileVillageName);
		Assertions.assertThat(userData.getDomicileVillageName()).isEqualTo(domicileVillageName);

		userData.setDomicilePostalCode(domicilePostalCode);
		Assertions.assertThat(userData.getDomicilePostalCode()).isEqualTo(domicilePostalCode);

		userData.setPartnerProvince(partnerProvince);
		Assertions.assertThat(userData.getPartnerProvince()).isEqualTo(partnerProvince);

		userData.setPartnerCity(partnerCity);
		Assertions.assertThat(userData.getPartnerCity()).isEqualTo(partnerCity);

		userData.setPartnerDistrict(partnerDistrict);
		Assertions.assertThat(userData.getPartnerDistrict()).isEqualTo(partnerDistrict);

		userData.setPartnerVillage(partnerVillage);
		Assertions.assertThat(userData.getPartnerVillage()).isEqualTo(partnerVillage);

		userData.setPartnerPostalCode(partnerPostalCode);
		Assertions.assertThat(userData.getPartnerPostalCode()).isEqualTo(partnerPostalCode);

		userData.setMotherName(motherName);
		Assertions.assertThat(userData.getMotherName()).isEqualTo(motherName);

		userData.setCreatedBy(createdBy);
		Assertions.assertThat(userData.getCreatedBy()).isEqualTo(createdBy);

		userData.setCreatedAt(createdAt);
		Assertions.assertThat(userData.getCreatedAt()).isEqualTo(createdAt);

		userData.setUpdatedBy(updatedBy);
		Assertions.assertThat(userData.getUpdatedBy()).isEqualTo(updatedBy);

		userData.setUpdatedAt(updatedAt);
		Assertions.assertThat(userData.getUpdatedAt()).isEqualTo(updatedAt);

		userData.setFillFinishAt(fillFinishAt);
		Assertions.assertThat(userData.getFillFinishAt()).isEqualTo(fillFinishAt);

	}
}