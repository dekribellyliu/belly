package id.investree.customer.entity;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRoleTest {
	private UserRole userRole;

	private Long id;
	private Long ldId;
	private Long murId;
	private String active;
	private String available;

	@Before
	public void init() {
		userRole = new UserRole();

		id = -999L;
		ldId = -999L;
		murId = -999L;
		active = "Active";
		available = "Not available";
	}

	@Test
	public void whenSetUserRole_thenReturnValue() {

		userRole.setId(id);
		Assertions.assertThat(userRole.getId()).isEqualTo(id);

		userRole.setLdId(ldId);
		Assertions.assertThat(userRole.getLdId()).isEqualTo(ldId);

		userRole.setMurId(murId);
		Assertions.assertThat(userRole.getMurId()).isEqualTo(murId);

		userRole.setActive(active);
		Assertions.assertThat(userRole.getActive()).isEqualTo(active);

		userRole.setAvailable(available);
		Assertions.assertThat(userRole.getAvailable()).isEqualTo(available);

	}
}