package id.investree.customer.service.balancesheet;


import id.investree.customer.entity.BalanceSheet;
import id.investree.customer.repository.BalanceSheetRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BalanceSheetServiceImplTest {

	private BalanceSheet balanceSheet;

	@Autowired
	private BalanceSheetServiceImpl service;

	@Autowired
	private BalanceSheetRepository repository;

	@Before
	public void init() {
		balanceSheet = new BalanceSheet();
		balanceSheet.setId(-999L);
		balanceSheet.setCustomerId(-999L);
		balanceSheet.setYearTo(2020L);
		balanceSheet.setAccReceive(999d);
		balanceSheet.setInvestory(999d);
		balanceSheet.setAccPayable(999d);
		balanceSheet.setBankDebt(999d);
		balanceSheet.setCurrentAssets(999d);
		balanceSheet.setCurrentLiabilities(999d);
		balanceSheet.setTotalLiabilities(999d);
		balanceSheet.setEquity(999d);
		balanceSheet.setCreatedAt(new Date());
		balanceSheet.setCreatedBy(-999L);
		balanceSheet.setUpdateAt(new Date());
		balanceSheet.setUpdateBy(-999L);
	}

	@Test
	public void whenSaveAndFindBalanceSheet_thenReturnExpectedResult() {
		BalanceSheet saved = service.saveOrUpdate(balanceSheet);

		assertThat(saved).isNotNull();

		List<BalanceSheet> balanceSheets = service.findByCustomerId(saved.getCustomerId());
		Optional<List<BalanceSheet>> repositoryData = repository.findByCustomerId(saved.getCustomerId());

		assertThat(repositoryData.isPresent()).isTrue();
		repositoryData.ifPresent(v -> {
			assertThat(balanceSheets).hasSameSizeAs(v);
			assertThat(balanceSheets.get(0)).isEqualToComparingFieldByField(v.get(0));
		});

		BalanceSheet balanceSheet = service.findByIdAndCustomerId(saved.getId(), saved.getCustomerId());
		Optional<BalanceSheet> repositoryBalanceSheet = repository.findByIdAndCustomerId(saved.getId(), saved.getCustomerId());

		repositoryBalanceSheet.ifPresent(v -> assertThat(balanceSheet).isEqualToComparingFieldByField(v));

		repository.delete(balanceSheet);
	}


}