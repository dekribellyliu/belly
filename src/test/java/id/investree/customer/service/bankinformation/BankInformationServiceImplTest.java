package id.investree.customer.service.bankinformation;

import id.investree.customer.entity.BankInformation;
import id.investree.customer.repository.BankInformationRepository;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BankInformationServiceImplTest {

	@Autowired
	private BankInformationRepository repository;

	@Autowired
	private BankInformationService service;

	private BankInformation bankInformation;

	private Long id;
	private Long customerId;
	private Integer bankType;
	private String bankAccountCoverFile;
	private Long masterBankId;
	private String bankAccountNumber;
	private String bankAccountHolderName;
	private String useAsDisbursement;
	private String useAsWithdrawal;
	private String isAnchorDisbursement;
	private Long createdBy;
	private Long updateBy;
	private Long deletedBy;
	private Date createdAt;
	private Date updatedAt;
	private Date deletedAt;
	private Date fillFinishAt;
	private String verifiedStatus;

	@Before
	public void init() {
		bankInformation = new BankInformation();

		id = -999L;
		customerId = -999L;
		bankType = 1;
		bankAccountCoverFile = "lorem ipsum";
		masterBankId = 1L;
		bankAccountNumber = "999999";
		bankAccountHolderName = "LOREM IPSUM";
		useAsDisbursement = "Y";
		useAsWithdrawal = "N";
		isAnchorDisbursement = "N";
		createdBy = -999L;
		updateBy = -999L;
		deletedBy = -999L;
		createdAt = new Date();
		updatedAt = new Date();
		deletedAt = new Date();
		fillFinishAt = new Date();
		verifiedStatus = "Verified";
	}

	@Test
	public void whenSaveOrUpdateAndFindBankInfo_thenReturnData() {
		bankInformation.setCustomerId(customerId);
		bankInformation.setBankType(bankType);
		bankInformation.setBankAccountCoverFile(bankAccountCoverFile);
		bankInformation.setBankAccountNumber(bankAccountNumber);
		bankInformation.setBankAccountHolderName(bankAccountHolderName);
		bankInformation.setMasterBankId(masterBankId);
		bankInformation.setUseAsDisbursement(useAsDisbursement);
		bankInformation.setUseAsWithdrawal(useAsWithdrawal);
		bankInformation.setIsAnchorDisbursement(isAnchorDisbursement);
		bankInformation.setCreatedBy(createdBy);
		bankInformation.setCreatedAt(createdAt);
		bankInformation.setUpdateBy(updateBy);
		bankInformation.setUpdatedAt(updatedAt);
		bankInformation.setFillFinishAt(fillFinishAt);
		bankInformation.setVerifiedStatus(verifiedStatus);

		BankInformation serviceSave = service.saveOrUpdate(bankInformation);
		Assertions.assertThat(serviceSave).isEqualToIgnoringGivenFields(bankInformation, "id", "updatedAt");

		BankInformation repositorySave = repository.save(bankInformation);
		Assertions.assertThat(repositorySave).isEqualToIgnoringGivenFields(bankInformation, "id", "updatedAt");

		Optional<BankInformation> repositoryFindByCustomerIdResult = repository.findByCustomerIdAndIdAndDeletedByIsNull(repositorySave.getCustomerId(), repositorySave.getId());
		BankInformation serviceFindByCustomerIdResult = service.findByCustomerIdAndBankId(serviceSave.getCustomerId(), serviceSave.getId());
		Assertions.assertThat(serviceFindByCustomerIdResult).isEqualToIgnoringGivenFields(repositoryFindByCustomerIdResult.get(), "createdAt", "updatedAt", "fillFinishAt");

		Optional<List<BankInformation>> repositoryFindByCustomerIdResultList = repository.findByCustomerIdAndDeletedByIsNull(repositorySave.getCustomerId());
		List<BankInformation> serviceFindByCustomerIdResultList = service.findByCustomerId(repositorySave.getCustomerId());

		Assertions.assertThat(serviceFindByCustomerIdResultList.size()).isEqualTo(repositoryFindByCustomerIdResultList.get().size());

		repository.deleteById(serviceSave.getId());
	}

}