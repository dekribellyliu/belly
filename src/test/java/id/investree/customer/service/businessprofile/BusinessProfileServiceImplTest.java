package id.investree.customer.service.businessprofile;

import id.investree.customer.entity.BusinessProfile;
import id.investree.customer.repository.BusinessProfileRepository;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BusinessProfileServiceImplTest {

	@Autowired
	private BusinessProfileService service;

	@Autowired
	private BusinessProfileRepository repository;

	private BusinessProfile businessProfile;
	private Long customerId;
	private Date dateOfEstablishment;
	private Integer numberOfEmployee;
	private String companyDescription;
	private String companyAddress;
	private Long province;
	private Long city;
	private Long district;
	private Long village;
	private String postalCode;
	private String landLineNumber;
	private String mobilePrefix;
	private Date createdAt;
	private Long createdBy;
	private Long updatedBy;
	private Date updatedAt;
	private Date fillFinishAt;
	private String businessNarration;

	@Before
	public void init() {
		businessProfile = new BusinessProfile();

		customerId = -999L;
		dateOfEstablishment = new Date();
		numberOfEmployee = 999;
		companyDescription = "lorem ipsum";
		companyAddress = "lorem ipsum";
		province = 1L;
		city = 1L;
		district = 1L;
		village = 1L;
		postalCode = "9999";
		landLineNumber = "9999999";
		mobilePrefix = "62";
		createdAt = new Date();
		createdBy = -999L;
		updatedBy = -999L;
		updatedAt = new Date();
		fillFinishAt = new Date();
		businessNarration = "lorem ipsum";
	}

	@Test
	public void whenSaveDataAndFindBusinessProfile_thenReturnData() {
		businessProfile.setCustomerId(customerId);
		businessProfile.setDateOfEstablishment(dateOfEstablishment);
		businessProfile.setNumberOfEmployee(numberOfEmployee);
		businessProfile.setCompanyDescription(companyDescription);
		businessProfile.setBusinessNarration(businessNarration);
		businessProfile.setCompanyAddress(companyAddress);
		businessProfile.setProvince(province);
		businessProfile.setCity(city);
		businessProfile.setDistrict(district);
		businessProfile.setVillage(village);
		businessProfile.setPostalCode(postalCode);
		businessProfile.setLandLineNumber(landLineNumber);
		businessProfile.setCreatedBy(createdBy);
		businessProfile.setCreatedAt(createdAt);
		businessProfile.setUpdatedBy(updatedBy);
		businessProfile.setUpdatedAt(updatedAt);
		businessProfile.setFillFinishAt(fillFinishAt);

		BusinessProfile repositoryResult = repository.save(businessProfile);
		Assertions.assertThat(repositoryResult).isNotNull();

		BusinessProfile serviceResult = service.saveOrUpdate(repositoryResult);
		Assertions.assertThat(serviceResult).isNotNull();

		Optional<BusinessProfile> repositoryFindByCustomerIdResult = repository.findByCustomerId(repositoryResult.getCustomerId());
		BusinessProfile serviceFindByCustomerIdResult = service.findByCustomerId(serviceResult.getCustomerId());

		Assertions.assertThat(serviceFindByCustomerIdResult).isEqualToComparingFieldByField(repositoryFindByCustomerIdResult.get());

		repository.deleteById(serviceResult.getId());
	}
}