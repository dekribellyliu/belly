package id.investree.customer.service.customerinfo;

import id.investree.customer.entity.CustomerInformation;
import id.investree.customer.repository.CustomerInformationRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerInformationServiceTest {

	@Autowired
	private CustomerInformationRepository repository;

	@Autowired
	private CustomerInformationService service;

	private CustomerInformation customerInformation;

	private Long id;
	private Integer productPreference;
	private String userCategory;
	private String name;
	private Long nationality;
	private Integer legalEntity;
	private String industry;
	private Long annualIncome;
	private Long sourceOfFund;
	private Long investmentObjective;
	private Long riskProfile;
	private Long createdBy;
	private Date createdAt;
	private Long updatedBy;
	private Date updatedAt;
	private String initial;
	private String customerNumber;

	@Before
	public void init() {
		customerInformation = new CustomerInformation();

		id = -999L;
		productPreference = 1;
		userCategory = "1";
		name = "PT APA AJA";
		nationality = 104L;
		legalEntity = 1;
		industry = "A";
		annualIncome = 999L;
		sourceOfFund = 999L;
		investmentObjective = 999L;
		riskProfile = 999L;
		createdBy = -999L;
		createdAt = new Date();
		updatedBy = -999L;
		updatedAt = new Date();
		initial = RandomStringUtils.randomAlphabetic(4);
		customerNumber = "X.X.XXYY.XXXXXX";
	}

	@Test
	public void whenSaveAndFindCustomerInfo_thenReturnData() {
		customerInformation.setProductPreference(productPreference);
		customerInformation.setUserCategory(userCategory);
		customerInformation.setName(name);
		customerInformation.setNationality(nationality);
		customerInformation.setLegalEntity(legalEntity);
		customerInformation.setIndustry(industry);
		customerInformation.setAnnualIncome(annualIncome);
		customerInformation.setSourceOfFund(sourceOfFund);
		customerInformation.setInvestmentObjective(investmentObjective);
		customerInformation.setRiskProfile(riskProfile);
		customerInformation.setCreatedBy(createdBy);
		customerInformation.setCreatedAt(createdAt);
		customerInformation.setUpdatedBy(updatedBy);
		customerInformation.setUpdatedAt(updatedAt);
		customerInformation.setInitial(initial);
		customerInformation.setCustomerNumber(customerNumber);

		CustomerInformation serviceSave = service.saveOrUpdate(customerInformation);
		Assertions.assertThat(serviceSave).isEqualToIgnoringGivenFields(customerInformation, "id", "updatedAt");

		CustomerInformation repositorySave = repository.save(customerInformation);
		Assertions.assertThat(repositorySave).isEqualToIgnoringGivenFields(customerInformation, "id", "updatedAt");

		CustomerInformation serviceFindByIdResult = service.findById(serviceSave.getId());
		Assertions.assertThat(serviceFindByIdResult).isEqualToIgnoringGivenFields(serviceSave,"createdAt", "updatedAt");

		Optional<CustomerInformation> repositoryFindByIdResult = repository.findById(repositorySave.getId());
		Assertions.assertThat(repositoryFindByIdResult.get()).isEqualToIgnoringGivenFields(repositorySave,"createdAt", "updatedAt");

		CustomerInformation serviceFindByInitialResult = service.findByInitial(serviceSave.getInitial());
		Assertions.assertThat(serviceFindByInitialResult).isEqualToIgnoringGivenFields(serviceSave,"createdAt", "updatedAt");

		Optional<CustomerInformation> repositoryFindByInitialResult = repository.findByinitial(repositorySave.getInitial());
		Assertions.assertThat(repositoryFindByInitialResult.get()).isEqualToIgnoringGivenFields(repositorySave,"createdAt", "updatedAt");

		repository.deleteById(serviceSave.getId());
	}

	@Test
	public void testHardDeleteById() {
		CustomerInformation ci = new CustomerInformation();
		ci.setName("T3st");
		CustomerInformation saved = service.saveOrUpdate(ci);

		Assertions.assertThat(service.findById(saved.getId())).isNotNull();

		service.hardDelete(saved.getId());
	}
}