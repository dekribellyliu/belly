package id.investree.customer.service.customerproductlist;

import id.investree.customer.entity.CustomerProductList;
import id.investree.customer.repository.CustomerProductListRepository;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerProductListServiceTest {

	private CustomerProductList customerProductList;

	@Autowired
	private CustomerProductListService service;

	@Autowired
	private CustomerProductListRepository repository;

	private Long customerRoleId;
	private Long legacyId;
	private Long type;
	private Long createdBy;
	private Date createdAt;
	private Long updatedBy;
	private Date updatedAt;

	@Before
	public void init() {
		customerProductList = new CustomerProductList();

		customerRoleId = -1234L;
		legacyId = -123L;
		type = 13L;
		createdBy = -123L;
		createdAt =new Date();
		updatedBy = -123L;
		updatedAt = new Date();
	}

	@Test
	public void whenSaveDataValid_thenReturn() {
		customerProductList.setCustomerRoleId(customerRoleId);
		customerProductList.setLegacyId(legacyId);
		customerProductList.setType(type);
		customerProductList.setCreatedBy(createdBy);
		customerProductList.setCreatedAt(createdAt);
		customerProductList.setCustomerProductList_updatedBy(updatedBy);
		customerProductList.setCustomerProductList_updatedAt(updatedAt);

		CustomerProductList serviceRequest = service.saveOrUpdate(customerProductList);
		Assertions.assertThat(serviceRequest.getId()).isNotEqualTo(0);

		List<CustomerProductList> serviceCustomerRole = service.findByCustomerRoleIds(serviceRequest.getCustomerRoleId());
		Optional<List<CustomerProductList>> repositoryCustomerRole = repository.findByCustomerRoleIdInAndDeletedAtIsNull(serviceRequest.getCustomerRoleId());
		Assertions.assertThat(serviceCustomerRole.size()).isEqualToComparingFieldByField(repositoryCustomerRole.get().size());

		repository.deleteById(serviceRequest.getId());
	}
}
