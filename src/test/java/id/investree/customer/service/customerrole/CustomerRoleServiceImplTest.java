package id.investree.customer.service.customerrole;

import id.investree.core.constant.GlobalConstants;
import id.investree.customer.entity.CustomerRole;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerRoleServiceImplTest {

	@Autowired
	private CustomerRoleService customerRoleService;

	@Test
	public void testHardDeleteByCustomerId() {
		long customerId = new Random().nextInt(999) - 999;
		String customerNumber = "1234";

		CustomerRole role = new CustomerRole();
		role.setCustomerInformationId(customerId);
		role.setCustomerNumber(customerNumber);
		role.setType(GlobalConstants.USER_CATEGORY_BORROWER_ID);
		CustomerRole saved = customerRoleService.saveOrUpdate(role);

		Assertions.assertThat(customerRoleService.findById(saved.getId())).isNotNull();
		Assertions.assertThat(customerRoleService.findByCustomerId(customerId)).isNotNull();
		Assertions.assertThat(customerRoleService.findAllByCustomerId(customerId)).isNotNull();
		Assertions.assertThat(customerRoleService.findByCustomerIdAndType(customerId, GlobalConstants.USER_CATEGORY_BORROWER_ID)).isNotNull();
		Assertions.assertThat(customerRoleService.findByCrNumber(customerNumber)).isNotNull();

		customerRoleService.hardDeleteByCustomerId(customerId);
		Assertions.assertThat(customerRoleService.findByCustomerId(customerId)).isNull();
	}
	@Test
	public void testFindByTypes() {
		long id = new Random().nextInt(999) - 999;

		CustomerRole roleBorrower = new CustomerRole();
		roleBorrower.setCustomerInformationId(id);
		roleBorrower.setType(GlobalConstants.USER_CATEGORY_BORROWER_ID);
		CustomerRole saved = customerRoleService.saveOrUpdate(roleBorrower);

		CustomerRole roleLender = new CustomerRole();
		roleLender.setCustomerInformationId(id);
		roleLender.setType(GlobalConstants.USER_CATEGORY_LENDER_ID);
		customerRoleService.saveOrUpdate(roleLender);

		Assertions.assertThat(customerRoleService.findByCustomerIdAndType(
			id,
			GlobalConstants.USER_CATEGORY_BORROWER_ID,
			GlobalConstants.USER_CATEGORY_LENDER_ID)).isNotNull();

		customerRoleService.hardDeleteByCustomerId(saved.getId());
	}
}