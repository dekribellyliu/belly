package id.investree.customer.service.financialinformation;

import id.investree.customer.entity.FinancialInformation;
import id.investree.customer.repository.FinancialInformationRepository;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FinancialInformationServiceTest {

	private FinancialInformation financialInformation;

	@Autowired
	private FinancialInformationService service;

	@Autowired
	private FinancialInformationRepository repository;

	private Long id;
	private Long customerId;
	private Integer statementFileType;
	private String statementUrl;
	private Date statementFileDate;
	private Long createdBy;
	private Long updatedBy;
	private Date updatedAt;

	@Before
	public void init() {
		financialInformation = new FinancialInformation();

		id = -123L;
		customerId = -1234L;
		statementFileType = 10;
		statementUrl = "index.jpg";
		statementFileDate = new Date();
		createdBy = -123L;
		updatedBy = -123L;
		updatedAt = new Date();
	}

	@Test
	public void whenSaveDataValid_thenReturn() {
		financialInformation.setCustomerId(customerId);
		financialInformation.setStatementFileType(statementFileType);
		financialInformation.setStatementUrl(statementUrl);
		financialInformation.setStatementFileDate(statementFileDate);
		financialInformation.setCreatedBy(createdBy);
		financialInformation.setUpdatedBy(updatedBy);
		financialInformation.setUpdatedAt(updatedAt);

		FinancialInformation serviceRequest = service.saveOrUpdate(financialInformation);
		Assertions.assertThat(serviceRequest.getId()).isNotEqualTo(0);

		FinancialInformation serviceFindById = service.findById(serviceRequest.getId());
		Optional<FinancialInformation> repositoryFindById = repository.findById(serviceRequest.getId());
		Assertions.assertThat(serviceFindById).isEqualToComparingFieldByField(repositoryFindById.get());

		FinancialInformation serviceFindByIdAndDeleted = service.findByIdAndDeleted(serviceRequest.getId());
		Optional<FinancialInformation> repositoryFindByIdAndDeleted = repository.findByIdAndDeletedAtIsNull(serviceRequest.getId());
		Assertions.assertThat(serviceFindByIdAndDeleted).isEqualToComparingFieldByField(repositoryFindByIdAndDeleted.get());

		FinancialInformation serviceFindByIdAndCustomerId = service.findByIdAndCustomerId(serviceRequest.getId(), customerId);
		Optional<FinancialInformation> repositoryFindByIdAndCustomerId = repository.findByIdAndCustomerIdAndDeletedAtIsNull(serviceRequest.getId(), customerId);
		Assertions.assertThat(serviceFindByIdAndCustomerId).isEqualToComparingFieldByField(repositoryFindByIdAndCustomerId.get());

		List<FinancialInformation> serviceListByCustomerId = service.findByCustomerId(customerId);
		Optional<List<FinancialInformation>> repositoryListByCustomerId = repository.findByCustomerIdAndDeletedAtIsNull(customerId);
		Assertions.assertThat(serviceListByCustomerId.size()).isEqualToComparingFieldByField(repositoryListByCustomerId.get().size());

		FinancialInformation serviceFindFirstByCustomerId = service
			.findFirstByCustomerIdAndStatementFileTypeOrderByStatementFileDateAndCreatedAt(customerId, statementFileType);
		Optional<FinancialInformation> repositoryFirstByCustomerId = repository
			.findFirstByCustomerIdAndStatementFileTypeAndDeletedAtIsNullOrderByStatementFileDateDescCreatedAtDescIdDesc(
				customerId,
				statementFileType
			);
		Assertions.assertThat(serviceFindFirstByCustomerId).isEqualToComparingFieldByField(repositoryFirstByCustomerId.get());

		repository.deleteById(serviceRequest.getId());
	}

}
