package id.investree.customer.service.financialratio;

import id.investree.customer.entity.FinancialRatio;
import id.investree.customer.repository.FinancialRatioRepository;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FinancialRatioServiceTest {

	private FinancialRatio financialRatio;

	@Autowired
	private FinancialRatioService service;

	@Autowired
	private FinancialRatioRepository repository;

	private Long customerId;
	private Long yearTo;
	private Double gpm;
	private Double npm;
	private Double ardoh;
	private Double invdoh;
	private Double apdoh;
	private Double cashCycle;
	private Double cashRatio;
	private Double ebitda;
	private Double leverage;
	private Double wiNeeds;
	private Double tie;
	private Double dscr;
	private Long createdBy;
	private Date createdAt;
	private Long updateBy;
	private Date updateAt;

	@Before
	public void init () {
		financialRatio = new FinancialRatio();

		customerId = 123L;
		yearTo = -123L;
		gpm = 2.0;
		npm = 2.0;
		ardoh = 2.0;
		invdoh = 2.0;
		apdoh = 2.0;
		cashCycle = 2.0;
		cashRatio = 2.0;
		ebitda = 2.0;
		leverage = 2.0;
		wiNeeds = 2.0;
		tie = 2.0;
		dscr = 2.0;
		createdBy = -123L;
		createdAt = new Date();
		updateBy = -123L;
		updateAt = new Date();
	}

	@Test
	public void whenSaveDataFinancialRatioValid_thenReturn() {
		financialRatio.setCustomerId(customerId);
		financialRatio.setYearTo(yearTo);
		financialRatio.setGpm(gpm);
		financialRatio.setNpm(npm);
		financialRatio.setArdoh(ardoh);
		financialRatio.setInvdoh(invdoh);
		financialRatio.setApdoh(apdoh);
		financialRatio.setCashCycle(cashCycle);
		financialRatio.setCashRatio(cashRatio);
		financialRatio.setEbitda(ebitda);
		financialRatio.setLeverage(leverage);
		financialRatio.setWiNeeds(wiNeeds);
		financialRatio.setTie(tie);
		financialRatio.setDscr(dscr);
		financialRatio.setCreatedBy(createdBy);
		financialRatio.setCreatedAt(createdAt);
		financialRatio.setUpdateBy(updateBy);
		financialRatio.setUpdateAt(updateAt);

		FinancialRatio serviceRequest = service.saveOrUpdate(financialRatio);
		Assertions.assertThat(serviceRequest.getId()).isNotEqualTo(0);

		List<FinancialRatio> serviceByCustomerId = service.findByCustomerId(customerId);
		Optional<List<FinancialRatio>> repositoryByCustomerId = repository.findByCustomerId(customerId);
		Assertions.assertThat(serviceByCustomerId.size()).isEqualToComparingFieldByField(repositoryByCustomerId.get().size());

		FinancialRatio serviceByIdAndCustomerId = service.findByIdAndCustomerId(serviceRequest.getId(), customerId);
		Optional<FinancialRatio> repositoryByIdAndCustomerId = repository.findByIdAndCustomerId(serviceRequest.getId(), customerId);
		Assertions.assertThat(serviceByIdAndCustomerId).isEqualToComparingFieldByField(repositoryByIdAndCustomerId.get());

		repository.deleteById(serviceRequest.getId());
	}
}
