package id.investree.customer.service.financialstatement;

import id.investree.customer.entity.FinancialStatement;
import id.investree.customer.repository.FinancialStatementRepository;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FinancialStatementServiceImplTest {

	private FinancialStatement financialStatement;

	@Autowired
	private FinancialStatementService service;

	@Autowired
	private FinancialStatementRepository repository;

	private Long id;
	private Long customerId;
	private Long yearTo;
	private Date fiscalYear;
	private Double sales;
	private Double cogs;
	private Double grossProfit;
	private Double sga;
	private Double depreciation;
	private Double operatingProfit;
	private Double interestExpense;
	private Double otherIncome;
	private Double otherExpense;
	private Double profitBeforeTax;
	private Double tax;
	private Double profitAfterTax;
	private Long createdBy;
	private Date createdAt;
	private Long updateBy;
	private Date updateAt;
	private Double existingFacility;

	@Before
	public void init() {
		financialStatement = new FinancialStatement();
		financialStatement.setId(-999L);
		financialStatement.setCustomerId(-999L);
		financialStatement.setYearTo(-999L);
		financialStatement.setFiscalYear(new Date());
		financialStatement.setSales(999d);
		financialStatement.setCogs(999d);
		financialStatement.setGrossProfit(999d);
		financialStatement.setSga(999d);
		financialStatement.setDepreciation(999d);
		financialStatement.setOperatingProfit(999d);
		financialStatement.setInterestExpense(999d);
		financialStatement.setOtherIncome(999d);
		financialStatement.setOtherExpense(999d);
		financialStatement.setProfitBeforeTax(999d);
		financialStatement.setTax(999d);
		financialStatement.setProfitAfterTax(999d);
		financialStatement.setCreatedBy(-999L);
		financialStatement.setCreatedAt(new Date());
		financialStatement.setUpdateBy(-999L);
		financialStatement.setUpdateAt(new Date());
		financialStatement.setExistingFacility(999d);

	}

	@Test
	public void whenSaveAndFindFinancialStatement_thenReturnResult() {

		FinancialStatement serviceSave = service.saveOrUpdate(financialStatement);
		Assertions.assertThat(serviceSave).isNotNull();

		List<FinancialStatement> serviceFindByCutomerId = service.findByCustomerId(serviceSave.getCustomerId());
		Optional<List<FinancialStatement>> repoFindByCustomerId = repository.findByCustomerId(serviceSave.getCustomerId());

		Assertions.assertThat(serviceFindByCutomerId.size()).isEqualTo(repoFindByCustomerId.get().size());
		Assertions.assertThat(serviceFindByCutomerId.get(0)).isEqualToComparingFieldByField(repoFindByCustomerId.get().get(0));

		FinancialStatement serviceFindByCustomerIdAndYear = service.findByCustomerIdAndYearTo(serviceSave.getCustomerId(), serviceSave.getYearTo());
		Optional<FinancialStatement> repoFindByCustomerIdAndYear = repository.findByCustomerIdAndYearTo(serviceSave.getCustomerId(), serviceSave.getYearTo());

		Assertions.assertThat(serviceFindByCustomerIdAndYear).isEqualToComparingFieldByField(repoFindByCustomerIdAndYear.get());

		FinancialStatement seviceFindByIdAndCustomerId = service.findByIdAndCustomerId(serviceSave.getId(), serviceSave.getCustomerId());
		Optional<FinancialStatement> repoFindByIdAndCustomerId = repository.findByIdAndCustomerId(serviceSave.getId(), serviceSave.getCustomerId());

		Assertions.assertThat(seviceFindByIdAndCustomerId).isEqualToComparingFieldByField(repoFindByIdAndCustomerId.get());

		repository.delete(serviceSave);
	}
}