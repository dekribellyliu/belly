package id.investree.customer.service.financialtrend;

import id.investree.customer.entity.FinancialTrend;
import id.investree.customer.repository.FinancialTrendRepository;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FinancialTrendServiceTest {

	private FinancialTrend financialTrend;

	@Autowired
	private FinancialTrendService service;

	@Autowired
	private FinancialTrendRepository repository;

	private Long customerId;
	private String trendPeriod;
	private Double sales;
	private Double cogs;
	private Double grossProfit;
	private Double sga;
	private Double operatingProfit;
	private Double existingFacility;
	private Long createdBy;
	private Date createdAt;
	private Long updateBy;
	private Date updateAt;

	@Before
	public void init() {
		financialTrend = new FinancialTrend();

		customerId = -123L;
		trendPeriod = "1 to 2";
		sales = 2.0;
		cogs = 2.0;
		grossProfit = 2.0;
		sga = 2.0;
		operatingProfit = 2.0;
		existingFacility = 2.0;
		createdBy = -123L;
		createdAt = new Date();
		updateBy = -123L;
		updateAt = new Date();
	}

	@Test
	public void whenSaveDataValid_thenReturn() {
		financialTrend.setCustomerId(customerId);
		financialTrend.setTrendPeriod(trendPeriod);
		financialTrend.setSales(sales);
		financialTrend.setCogs(cogs);
		financialTrend.setGrossProfit(grossProfit);
		financialTrend.setSga(sga);
		financialTrend.setOperatingProfit(operatingProfit);
		financialTrend.setExistingFacility(existingFacility);
		financialTrend.setCreatedBy(createdBy);
		financialTrend.setCreatedAt(createdAt);
		financialTrend.setUpdateBy(updateBy);
		financialTrend.setUpdateAt(updateAt);

		FinancialTrend serviceRequest = service.saveOrUpdate(financialTrend);
		Assertions.assertThat(serviceRequest.getId()).isNotEqualTo(0);

		FinancialTrend serviceByCustomerIdAndTrendPeriod = service.findByCustomerIdAndTrendPeriod(customerId, trendPeriod);
		Optional<FinancialTrend> repositoryByCustomerIdAndTrendPeriod = repository.findByCustomerIdAndTrendPeriod(customerId, trendPeriod);
		Assertions.assertThat(serviceByCustomerIdAndTrendPeriod).isEqualToComparingFieldByField(repositoryByCustomerIdAndTrendPeriod.get());

		FinancialTrend serviceByIdAndCustomerId = service.findByIdAndCustomerId(serviceRequest.getId(), customerId);
		Optional<FinancialTrend> repositoryByIdAndCustomerId = repository.findByIdAndCustomerId(serviceRequest.getId(), customerId);
		Assertions.assertThat(serviceByIdAndCustomerId).isEqualToComparingFieldByField(repositoryByIdAndCustomerId.get());

		List<FinancialTrend> serviceByCustomerId = service.findByCustomerId(customerId);
		Optional<List<FinancialTrend>> repositoryByCustomerId = repository.findByCustomerId(customerId);
		Assertions.assertThat(serviceByCustomerId.size()).isEqualToComparingFieldByField(repositoryByCustomerId.get().size());

		repository.deleteById(serviceRequest.getId());
	}
}
