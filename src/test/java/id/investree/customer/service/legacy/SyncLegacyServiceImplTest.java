package id.investree.customer.service.legacy;

import id.investree.core.model.TokenPayload;
import id.investree.core.model.legacy.LegacyModel;
import id.investree.core.utils.TokenUtils;
import id.investree.customer.entity.UserData;
import id.investree.customer.model.legacy.LegacyModelBuilder;
import id.investree.customer.service.customerinfo.CustomerInformationService;
import id.investree.customer.service.customerrole.CustomerRoleService;
import id.investree.customer.service.logindata.LoginDataService;
import id.investree.customer.service.userdata.UserDataService;
import id.investree.customer.utils.StoreDataGlobalParam;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SyncLegacyServiceImplTest {

	@Autowired
	private SyncLegacyService syncLegacyService;

	@Autowired
	private UserDataService userDataService;

	@Autowired
	private CustomerInformationService customerInformationService;

	@Autowired
	private CustomerRoleService customerRoleService;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private LoginDataService loginDataService;

	@Autowired
	private StoreDataGlobalParam completingDataGlobalParam;

	private String accessToken;

	@Before
	public void init() {
		TokenPayload tokenPayload = new TokenPayload();
		tokenPayload.setUserId(1150455L);
		tokenPayload.setUserType(1);
		tokenPayload.setUserEncryptedToken("84d400027d804d0007bd99f9fc7731dde0245575");
		tokenPayload.setUserEmail("swarawan.rio@gmail.com");
		accessToken = tokenUtils.generateToken("swarawan.rio@gmail.com", tokenPayload.toMap());
	}

	@Test
	public void testLegacyServiceModel() {

		UserData userData = userDataService.findByLoginId(1150455L);
		String emailAddress = loginDataService.findByLoginId(accessToken, userData.getLoginDataId()).getEmailAddress();
		LegacyModel legacyModel = new LegacyModelBuilder()
			.setGlobalParam(completingDataGlobalParam)
			.setUserData(userData)
			.setEmail(emailAddress)
			.build(1L);

		syncLegacyService.sync(accessToken, legacyModel);
	}
}