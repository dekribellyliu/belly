package id.investree.customer.service.legalinformation;

import id.investree.customer.entity.LegalInformation;
import id.investree.customer.repository.LegalInformationRepository;
import id.investree.customer.utils.Constants;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LegalInformationServiceImplTest {

	@Autowired
	private LegalInformationService service;

	@Autowired
	private LegalInformationRepository repository;

	private LegalInformation legalInformation;

	private Long id;
	private Long customerId;
	private Integer documentType;
	private String documentFile;
	private String documentNumber;
	private Date documentExpired;
	private Date documentRegistered;
	private Long createdBy;
	private Long updateBy;
	private Date createdAt;
	private Date updatedAt;
	private Date fillFinishAt;

	@Before
	public void init() {
		legalInformation = new LegalInformation();

		id = -999L;
		customerId = -999L;
		documentType = Constants.LEGAL_INFO_NPWP;
		documentFile = "lorem ipsum";
		documentNumber = "9999999999";
		documentExpired = new Date();
		documentRegistered = new Date();
		createdBy = -999L;
		updateBy = -999L;
		createdAt = new Date();
		updatedAt = new Date();
		fillFinishAt = new Date();
	}

	@Test
	public void whenSaveAndFindLegalInfo_thenReturnData() {
		legalInformation.setCustomerId(customerId);
		legalInformation.setDocumentType(documentType);
		legalInformation.setDocumentFile(documentFile);
		legalInformation.setDocumentNumber(documentNumber);
		legalInformation.setDocumentRegistered(documentRegistered);
		legalInformation.setDocumentExpired(documentExpired);
		legalInformation.setCreatedBy(createdBy);
		legalInformation.setCreatedAt(createdAt);
		legalInformation.setUpdateBy(updateBy);
		legalInformation.setUpdatedAt(updatedAt);
		legalInformation.setFillFinishAt(fillFinishAt);

		List<LegalInformation> legalInformationList = new ArrayList<>();
		legalInformationList.add(legalInformation);

		List<LegalInformation> serviceSave = service.saveAll(legalInformationList);
		Assertions.assertThat(serviceSave.get(0)).isEqualToIgnoringGivenFields(legalInformation, "id", "updateAt");

		LegalInformation repositorySave = repository.saveAll(legalInformationList).get(0);
		Assertions.assertThat(repositorySave).isEqualToIgnoringGivenFields(legalInformation, "id", "updateAt");

		List<LegalInformation> serviceFindByCustomerIdResult = service.findByCustomerId(serviceSave.get(0).getCustomerId());
		Optional<List<LegalInformation>> repositoryFindByCustomerIdResult = repository.findByCustomerId(repositorySave.getCustomerId());

		Assertions.assertThat(serviceFindByCustomerIdResult.size()).isEqualTo(repositoryFindByCustomerIdResult.get().size());
		Assertions.assertThat(serviceFindByCustomerIdResult.get(0)).isEqualToIgnoringGivenFields(repositoryFindByCustomerIdResult.get().get(0), "updateAt");

		LegalInformation serviceFindByCustomerIdAndTypeResult = service.findByCustomerIdAndType(serviceSave.get(0).getCustomerId(), Constants.LEGAL_INFO_NPWP);
		Optional<LegalInformation> repositoryFindByCustomerIdAndTypeResult = repository.findByCustomerIdAndDocumentType(repositorySave.getCustomerId(), Constants.LEGAL_INFO_NPWP);

		Assertions.assertThat(serviceFindByCustomerIdAndTypeResult).isEqualToComparingFieldByField(repositoryFindByCustomerIdAndTypeResult.get());

		List<LegalInformation> serviceResult = service.findByTypeAndDocNumber(Constants.LEGAL_INFO_NPWP, serviceSave.get(0).getDocumentNumber());
		Optional<List<LegalInformation>> repositoryResult = repository.findByDocumentTypeAndDocumentNumber(Constants.LEGAL_INFO_NPWP, repositorySave.getDocumentNumber());

		Assertions.assertThat(serviceResult.size()).isEqualTo(repositoryResult.get().size());

		repository.deleteAll(serviceSave);
	}
}