package id.investree.customer.service.mrinvarbank;

import id.investree.customer.entity.MrInvarBank;
import id.investree.customer.repository.MrInvarBankRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MrInvarBankServiceTest {

	@Autowired
	private MrInvarBankService service;

	@Autowired
	private MrInvarBankRepository repository;

	private MrInvarBank mrInvarBank;

	@Test
	public void whenDataValid_thenReturn() {

		List<MrInvarBank> serviceFindById = service.findById(1L, 4L, 2L, 3L);
		Optional<List<MrInvarBank>> repositoryFindById = repository.findByIdIn(1L, 4L, 2L, 3L);
		Assertions.assertThat(serviceFindById.size()).isEqualToComparingFieldByField(repositoryFindById.get().size());

		MrInvarBank serviceFindByBankIdAndType = service.findByBankIdAndType(6L, 1L);
		Optional<MrInvarBank> repositoryFindByBankIdAndType = repository.findByBankIdAndTypeIn(6L, 1L);
		Assertions.assertThat(serviceFindByBankIdAndType).isEqualToComparingFieldByField(repositoryFindByBankIdAndType.get());
	}

	@Test
	public void whenDataNotValid_thenReturn() {

		List<MrInvarBank> serviceFindById = service.findById(6L,7L);
		Assertions.assertThat(serviceFindById).isEmpty();

		MrInvarBank serviceFindByBankIdAndType = service.findByBankIdAndType(11L, 2L);
		Assertions.assertThat(serviceFindByBankIdAndType).isNull();
	}
}
