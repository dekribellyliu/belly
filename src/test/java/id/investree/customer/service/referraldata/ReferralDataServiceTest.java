package id.investree.customer.service.referraldata;

import id.investree.customer.entity.ReferralData;
import id.investree.customer.repository.ReferralDataRepository;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ReferralDataServiceTest {

	private ReferralData referralData;

	@Autowired
	private ReferralDataService service;

	@Autowired
	private ReferralDataRepository repository;

	private Long referralUserId;
	private Long referrerUserId;
	private String referrerCode;
	private Date referralExpiredDate;
	private Long knowInvestreeFrom;

	@Before
	public void init() {
		referralData = new ReferralData();

		referralUserId = -123L;
		referrerUserId = -999L;
		referrerCode = "abcd1234";
		referralExpiredDate = Date.from(ZonedDateTime.now(ZoneId.of("Asia/Jakarta")).plusMonths(6).toInstant());
		knowInvestreeFrom = -1L;
	}

	@Test
	public void whenSaveDataValid_thenReturn() {
		referralData.setReferralUserId(referralUserId);
		referralData.setReferrerUserId(referrerUserId);
		referralData.setReferrerCode(referrerCode);
		referralData.setReferralExpiredDate(referralExpiredDate);
		referralData.setKnowInvestreeFrom(knowInvestreeFrom);

		ReferralData serviceRequest = service.saveOrUpdate(referralData);
		Assertions.assertThat(serviceRequest.getId()).isNotEqualTo(0);

		ReferralData serviceReferralUserId = service.findByReferralUserId(serviceRequest.getReferralUserId());
		Optional<ReferralData> repositoryReferralUserId = repository.findByReferralUserId(serviceRequest.getReferralUserId());
		repositoryReferralUserId.ifPresent(v -> assertThat(serviceReferralUserId).isEqualToComparingFieldByField(v));

		ReferralData serviceReferrerCode = service.findByReferrerCode(serviceRequest.getReferrerCode());
		Optional<ReferralData> repositoryReferrerCode = repository.findByReferrerCodeAndReferralUserIdIsNull(serviceRequest.getReferrerCode());
		repositoryReferrerCode.ifPresent(v -> assertThat(serviceReferrerCode).isEqualToComparingFieldByField(v));

		repository.deleteById(serviceRequest.getId());
	}
}
