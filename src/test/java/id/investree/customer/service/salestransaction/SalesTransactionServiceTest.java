package id.investree.customer.service.salestransaction;

import id.investree.customer.entity.SalesTransaction;
import id.investree.customer.repository.SalesTransactionRepository;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SalesTransactionServiceTest {

	private SalesTransaction salesTransaction;

	@Autowired
	private SalesTransactionRepository repository;

	@Autowired
	private SalesTransactionService service;

	private Long customerId;
	private Double amount;
	private Double transaction;
	private Long createdBy;
	private Date createdAt;
	private Long updateBy;
	private Date updateAt;

	@Before
	public void init() {
		salesTransaction = new SalesTransaction();

		customerId = -123L;
		amount = 20D;
		transaction = 10D;
		createdBy = -123L;
		createdAt = new Date();
		updateBy = -123L;
		updateAt = new Date();
	}

	@Test
	public void whenSaveDataValid_thenReturn() throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		salesTransaction.setDate(dateFormat.parse("2008-09-08"));
		salesTransaction.setCustomerId(customerId);
		salesTransaction.setAmount(amount);
		salesTransaction.setTransaction(transaction);
		salesTransaction.setCreatedBy(createdBy);
		salesTransaction.setCreatedAt(createdAt);
		salesTransaction.setUpdateBy(updateBy);
		salesTransaction.setUpdateAt(updateAt);

		SalesTransaction serviceRequest = service.saveOrUpdate(salesTransaction);
		Assertions.assertThat(serviceRequest.getId()).isNotEqualTo(0);

		SalesTransaction serviceById = service.findById(serviceRequest.getId());
		Optional<SalesTransaction> repositoryById = repository.findById(serviceRequest.getId());
		Assertions.assertThat(serviceById).isEqualToComparingFieldByField(repositoryById.get());

		List<SalesTransaction> serviceByCustomerId = service.findByCustomerId(customerId);
		Optional<List<SalesTransaction>> repositoryByCustomerId = repository.findByCustomerIdAndDeletedAtIsNull(customerId);
		Assertions.assertThat(serviceByCustomerId.size()).isEqualToComparingFieldByField(repositoryByCustomerId.get().size());

		SalesTransaction serviceByIdAndCustomerId = service.findByIdAndCustomerId(serviceRequest.getId(), customerId);
		Optional<SalesTransaction> repositoryByIdAndCustomerId = repository.findByIdAndCustomerIdAndDeletedAtIsNull(serviceRequest.getId(), customerId);
		Assertions.assertThat(serviceByIdAndCustomerId).isEqualToComparingFieldByField(repositoryByIdAndCustomerId.get());

		SalesTransaction serviceByCustomerIdAndDate = service.findByCustomerIdAndDate(serviceRequest.getCustomerId(), serviceRequest.getDate());
		Optional<SalesTransaction> repositoryByCustomerIdAndDate = repository.findByCustomerIdAndDateAndDeletedAtIsNull(serviceRequest.getCustomerId(), serviceRequest.getDate());
		Assertions.assertThat(serviceByCustomerIdAndDate).isEqualToComparingFieldByField(repositoryByCustomerIdAndDate.get());

		Long totalSalesTransactionByCustomerId = service.countCustomerId(customerId);
		Long countSalesTransactionByCustomerId = repository.countCustomerIdAndDeletedAtIsNull(customerId);
		Assertions.assertThat(totalSalesTransactionByCustomerId).isEqualTo(countSalesTransactionByCustomerId);

		repository.deleteById(serviceRequest.getId());
	}
}
