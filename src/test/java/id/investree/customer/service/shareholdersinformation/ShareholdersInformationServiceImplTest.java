package id.investree.customer.service.shareholdersinformation;

import id.investree.customer.entity.ShareholdersInformation;
import id.investree.customer.repository.ShareholdersInformationRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ShareholdersInformationServiceImplTest {

	@Autowired
	private ShareholdersInformationRepository repository;

	@Autowired
	private ShareholdersInformationService service;

	private ShareholdersInformation shareholdersInformation;
	private Long id;
	private Long customerId;
	private Long position;
	private String fullName;
	private Long mobilePrefix;
	private String mobileNumber;
	private String emailAddress;
	private Float stockOwnership;
	private Date dob;
	private String identificationCardUrl;
	private String identificationCardNumber;
	private Date identificationCardExpiryDate;
	private String selfieUrl;
	private String taxCardUrl;
	private String taxCardNumber;
	private String isLss;
	private String isPgs;
	private String isTss;
	private Date createdAt;
	private Long createdBy;
	private Long updatedBy;
	private Date updatedAt;
	private Date fillFinishAt;
	private Long deletedBy;
	private Date deletedAt;
	private Date apuPptDate;
	private String apuPptFile;
	private String apuPptResult;
	private String pgNumber;
	private Long pgAmount;
	private Date pgSignedDate;
	private String pgType;
	private String pgFile;

	@Before
	public void init() {
		shareholdersInformation = new ShareholdersInformation();

		Date date = new Calendar.Builder()
				.setDate(2019, 12, 12)
				.setTimeOfDay(0, 0, 0)
				.build().getTime();

		id = -999L;
		customerId = -999L;
		position = -1L;
		fullName = "lorem ipsum";
		mobilePrefix = 1L;
		mobileNumber = "99999999";
		emailAddress = "lorem@investree.id";
		stockOwnership = 100f;
		dob = date;
		identificationCardUrl = "lorem_ipsum.jpg";
		identificationCardNumber = RandomStringUtils.randomNumeric(16);
		identificationCardExpiryDate = date;
		selfieUrl = "lorem_ipsum.jpg";
		taxCardUrl = "lorem_ipsum.jpg";
		taxCardNumber = RandomStringUtils.randomNumeric(15);
		isLss = "Y";
		isPgs = "Y";
		isTss = "Y";
		createdAt = date;
		createdBy = -999L;
		updatedBy = -999L;
		updatedAt = date;
		fillFinishAt = date;
		deletedBy = -999L;
		deletedAt = date;
		apuPptDate = date;
		apuPptFile = "lorem_ipsum.jpg";
		apuPptResult = "lorem ipsum";
		pgNumber = RandomStringUtils.randomNumeric(6);
		pgAmount = 999999L;
		pgSignedDate = date;
		pgType = "with spouse";
		pgFile = "lorem_ipsum.jpg";
	}

	@Test
	public void whenSaveAndFindShareholderInfo_thenReturnData() {
		ShareholdersInformation shareholdersInformation = seedData();

		ShareholdersInformation serviceSave = service.saveOrUpdate(shareholdersInformation);
		Assertions.assertThat(serviceSave).isEqualToIgnoringGivenFields(shareholdersInformation, "id", "updatedAt");

		ShareholdersInformation repositorySave = repository.save(shareholdersInformation);
		Assertions.assertThat(repositorySave).isEqualToIgnoringGivenFields(shareholdersInformation, "id", "updatedAt");

		ShareholdersInformation serviceFindByIdResult = service.findById(serviceSave.getId());
		Optional<ShareholdersInformation> repositoryFindByIdResult = repository.findByIdAndDeletedAtIsNull(repositorySave.getId());
		Assertions.assertThat(serviceFindByIdResult).isEqualToIgnoringGivenFields(repositoryFindByIdResult.get(),
				"createdAt",
				"updatedAt",
				"dob",
				"identificationCardExpiryDate",
				"fillFinishAt",
				"apuPptDate",
				"pgSignedDate");

		ShareholdersInformation serviceFindByIdAndCustomerIdResult = service.findByIdAndCustomerId(serviceSave.getId(), serviceSave.getCustomerId());
		Optional<ShareholdersInformation> repositoryFindByIdAndCustomerIdResult = repository.findByIdAndCustomerIdAndDeletedAtIsNull(repositorySave.getId(), repositorySave.getCustomerId());
		Assertions.assertThat(serviceFindByIdAndCustomerIdResult).isEqualToIgnoringGivenFields(repositoryFindByIdAndCustomerIdResult.get(),
				"createdAt",
				"updatedAt",
				"dob",
				"identificationCardExpiryDate",
				"fillFinishAt",
				"apuPptDate",
				"pgSignedDate");

		List<ShareholdersInformation> serviceFindByTaxResult = service.findByTaxNumber(serviceSave.getTaxCardNumber());
		Optional<List<ShareholdersInformation>> repositoryFindByTaxResult = repository.findByTaxCardNumberAndDeletedAtIsNull(repositorySave.getTaxCardNumber());

		Assertions.assertThat(serviceFindByTaxResult.size()).isEqualTo(repositoryFindByTaxResult.get().size());
		Assertions.assertThat(serviceFindByTaxResult.get(0)).isEqualToIgnoringGivenFields(repositoryFindByTaxResult.get().get(0),
				"createdAt",
				"updatedAt",
				"dob",
				"identificationCardExpiryDate",
				"fillFinishAt",
				"apuPptDate",
				"pgSignedDate");

		List<ShareholdersInformation> serviceFindByCustomerIdResultList = service.findByCustomerIdAndNotDeleted(serviceSave.getCustomerId());
		Optional<List<ShareholdersInformation>> repositoryFindByCustomerIdResultList = repository.findByCustomerIdAndDeletedAtIsNull(repositorySave.getCustomerId());

		Assertions.assertThat(serviceFindByCustomerIdResultList.size()).isEqualTo(repositoryFindByCustomerIdResultList.get().size());
		Assertions.assertThat(serviceFindByCustomerIdResultList.get(0)).isEqualToIgnoringGivenFields(repositoryFindByCustomerIdResultList.get().get(0),
				"createdAt",
				"updatedAt",
				"dob",
				"identificationCardExpiryDate",
				"fillFinishAt",
				"apuPptDate",
				"pgSignedDate");

		repository.deleteById(serviceSave.getId());
	}

	@BeforeAll
	private ShareholdersInformation seedData() {
		shareholdersInformation.setCustomerId(customerId);
		shareholdersInformation.setPosition(position);
		shareholdersInformation.setFullName(fullName);
		shareholdersInformation.setMobilePrefix(mobilePrefix);
		shareholdersInformation.setMobileNumber(mobileNumber);
		shareholdersInformation.setEmailAddress(emailAddress);
		shareholdersInformation.setStockOwnership(stockOwnership);
		shareholdersInformation.setDob(dob);
		shareholdersInformation.setIdentificationCardUrl(identificationCardUrl);
		shareholdersInformation.setIdentificationCardNumber(identificationCardNumber);
		shareholdersInformation.setIdentificationCardExpiryDate(identificationCardExpiryDate);
		shareholdersInformation.setSelfieUrl(selfieUrl);
		shareholdersInformation.setTaxCardUrl(taxCardUrl);
		shareholdersInformation.setTaxCardNumber(taxCardNumber);
		shareholdersInformation.setIsLss(isLss);
		shareholdersInformation.setIsPgs(isPgs);
		shareholdersInformation.setIsTss(isTss);
		shareholdersInformation.setCreatedBy(createdBy);
		shareholdersInformation.setCreatedAt(createdAt);
		shareholdersInformation.setUpdatedBy(updatedBy);
		shareholdersInformation.setUpdatedAt(updatedAt);
		shareholdersInformation.setFillFinishAt(fillFinishAt);
		shareholdersInformation.setPgNumber(pgNumber);
		shareholdersInformation.setPgAmount(pgAmount);
		shareholdersInformation.setPgSignedDate(pgSignedDate);
		shareholdersInformation.setPgType(pgType);
		shareholdersInformation.setPgFile(pgFile);
		shareholdersInformation.setApuPptDate(apuPptDate);
		shareholdersInformation.setApuPptFile(apuPptFile);
		shareholdersInformation.setApuPptResult(apuPptResult);

		return shareholdersInformation;
	}


}