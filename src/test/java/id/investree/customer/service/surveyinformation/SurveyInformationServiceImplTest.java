package id.investree.customer.service.surveyinformation;

import id.investree.customer.entity.SurveyInformation;
import id.investree.customer.repository.SurveyInformationRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SurveyInformationServiceImplTest {

	@Autowired
	private SurveyInformationService service;

	@Autowired
	private SurveyInformationRepository repository;

	private SurveyInformation surveyInformation;
	private List<SurveyInformation> surveyInformationList;

	@Before
	public void init() {
		surveyInformation = new SurveyInformation();
		surveyInformation.setCustomerId(-999L);
		surveyInformation.setLoginId(-999L);
		surveyInformation.setSurveyDate(new Date());
		surveyInformation.setBorrowerPosition(-1L);
		surveyInformation.setBorrowerName(RandomStringUtils.randomAlphabetic(6));
		surveyInformation.setNumberOfEmployees(0L);
		surveyInformation.setOfficeStatus(-1L);
		surveyInformation.setLengthOfStay("0");
		surveyInformation.setFilename("lorem_ipsum.jpg");
		surveyInformation.setResultDescription("lorem ipsum");
		surveyInformation.setAddress("lorem ipsum");
		surveyInformation.setCreatedBy(-999L);
		surveyInformation.setCreatedAt(new Date());
		surveyInformation.setUpdateBy(-999L);
		surveyInformation.setUpdateAt(new Date());

		surveyInformationList = new ArrayList<>();
	}

	@Test
	public void whenSaveAndFind_thenReturnValue() {
		surveyInformationList.add(surveyInformation);

		List<SurveyInformation> serviceSave = service.saveAll(surveyInformationList);
		Assertions.assertThat(serviceSave.get(0)).isEqualToIgnoringGivenFields(surveyInformation, "updateAt", "createdAt");

		SurveyInformation serviceFindByIdAndCustomerId = service.findByIdAndCustomerId(serviceSave.get(0).getId(), serviceSave.get(0).getCustomerId());
		Optional<SurveyInformation> repoFindByIdAndCustomerId = repository.findByIdAndCustomerIdAndDeletedAtIsNull(serviceSave.get(0).getId(), serviceSave.get(0).getCustomerId());
		Assertions.assertThat(serviceFindByIdAndCustomerId).isEqualToComparingFieldByField(repoFindByIdAndCustomerId.get());

		SurveyInformation serviceFindById = service.findSurveyInformationById(serviceSave.get(0).getId());
		Optional<SurveyInformation> repoFindById = repository.findById(serviceSave.get(0).getId());
		Assertions.assertThat(serviceFindById).isEqualToComparingFieldByField(repoFindById.get());

		SurveyInformation serviceFindByLastUpdate = service.findLastUpdateByCustomerId(serviceSave.get(0).getCustomerId());
		Optional<SurveyInformation> repoFindByLastUpdate = repository.findFirstByCustomerIdAndDeletedAtIsNullOrderBySurveyDateDescUpdateAtDescIdDesc(serviceSave.get(0).getCustomerId());
		Assertions.assertThat(serviceFindByLastUpdate).isEqualToComparingFieldByField(repoFindByLastUpdate.get());

		List<SurveyInformation> serviceFindAllByCustomerId = service.findAllSurveyByCustomerId(serviceSave.get(0).getCustomerId());
		Optional<List<SurveyInformation>> repoFindAllByCustomerId = repository.findByCustomerIdAndDeletedAtIsNull(serviceSave.get(0).getCustomerId());
		Assertions.assertThat(serviceFindAllByCustomerId.get(0)).isEqualToComparingFieldByField(repoFindAllByCustomerId.get().get(0));

		repository.delete(serviceSave.get(0));
	}
}