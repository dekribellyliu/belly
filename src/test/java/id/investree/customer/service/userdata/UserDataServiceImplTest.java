package id.investree.customer.service.userdata;

import id.investree.customer.entity.UserData;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserDataServiceImplTest {

	@Autowired
	private UserDataService userDataService;

	@Test
	public void testFindByCustomerId_butEmpty() {
		long id = new Random().nextInt(999) - 999;
		Assertions.assertThat(userDataService.findByCustomerId(id)).isNull();
	}

	@Test
	public void testFindByIdCardNumbe() {
		long id = new Random().nextInt(999) - 999;
		String idCardNumber = "101010101010101";

		UserData userData = new UserData();
		userData.setCustomerId(id);
		userData.setIdCardNumber(idCardNumber);

		UserData saved = userDataService.saveOrUpdate(userData);
		Assertions.assertThat(userDataService.findAllByCustomerId(id)).isNotNull();
		Assertions.assertThat(userDataService.findByIdCardNumber(idCardNumber)).isNotNull();

		userDataService.hardDeleteByCustomerId(saved.getCustomerId());
	}

	@Test
	public void testHardDeleteByCustomer() {
		long id = new Random().nextInt(999) - 999;

		UserData userData = new UserData();
		userData.setCustomerId(id);

		UserData saved = userDataService.saveOrUpdate(userData);

		Assertions.assertThat(userDataService.findByCustomerId(id)).isNotNull();

		userDataService.hardDeleteByCustomerId(saved.getCustomerId());
		UserData userDataAfterDelete = userDataService.findByCustomerId(id);
		Assertions.assertThat(userDataAfterDelete).isNull();
	}
}