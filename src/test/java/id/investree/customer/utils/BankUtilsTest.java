package id.investree.customer.utils;

import id.investree.core.exception.AppException;
import id.investree.customer.model.request.BankInformationRequest;
import id.investree.customer.service.bankinformation.BankInformationService;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BankUtilsTest {

	@Autowired
	private BankUtils bankUtils;

	@Autowired
	private BankInformationService bankInformationService;

	private BankInformationRequest request;

	@Before
	public void init() {
		request = new BankInformationRequest();
		request.setBankInformationId(1L);
		request.setCustomerId(1L);
		request.setMasterBankId(1L);
		request.setBankAccountCoverFile("http://file.com");
		request.setUseAsDisbursement(false);
		request.setUseAsWithdrawal(false);
		request.setDelete(false);
	}

	@Test
	public void testBankUtils_valid() {
		request.setBankAccountHolderName("Tahapan");
		request.setBankAccountNumber("0611104579");
		Assertions.assertThat(bankUtils.isValidBcaAccount(request)).isTrue();
	}

	@Test(expected = AppException.class)
	public void testBankUtils_invalid() {
		request.setBankAccountHolderName("Wrong");
		request.setBankAccountNumber("0101010101");
		Assertions.assertThat(bankUtils.isValidBcaAccount(request)).isFalse();
	}
}