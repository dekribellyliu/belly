package id.investree.customer.utils;

import id.investree.core.constant.GlobalConstants;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerUtilsTest {

	@Autowired
	private CustomerUtils customerUtils;

	private Calendar calendar = Calendar.getInstance();

	@Test
	public void testGetAge() {
		Assertions.assertThat(customerUtils.getAge(null)).isZero().isNotNegative();

		calendar.set(1989, Calendar.DECEMBER, 6);
		Date date = calendar.getTime();
		Assertions.assertThat(customerUtils.getAge(date)).isNotZero().isNotNegative();

		Calendar thisYear = Calendar.getInstance();
		calendar.set(thisYear.get(Calendar.YEAR), Calendar.DECEMBER, 6);
		date = calendar.getTime();
		Assertions.assertThat(customerUtils.getAge(date)).isZero().isNotNegative();
	}

	@Test
	public void testDateAfterSeventeenYearsAgo() {
		Calendar thisYear = Calendar.getInstance();

		calendar.set(thisYear.get(Calendar.YEAR) - 20, Calendar.DECEMBER, 6);
		Date date = calendar.getTime();
		Assertions.assertThat(customerUtils.isDateAfterSeventeenYearsAgo(date)).isTrue();

		calendar.set(thisYear.get(Calendar.YEAR) - 15, Calendar.DECEMBER, 6);
		date = calendar.getTime();
		Assertions.assertThat(customerUtils.isDateAfterSeventeenYearsAgo(date)).isFalse();
	}

	@Test
	public void testGetUserTypeIdentification() {
		Assertions.assertThat(customerUtils.getUserTypeIdentification("borrower")).isEqualTo(GlobalConstants.USER_CATEGORY_BORROWER_ID);
		Assertions.assertThat(customerUtils.getUserTypeIdentification("lender")).isEqualTo(GlobalConstants.USER_CATEGORY_LENDER_ID);
		Assertions.assertThat(customerUtils.getUserTypeIdentification("anchor")).isEqualTo(GlobalConstants.USER_CATEGORY_ANCHOR_ID);
	}
}