package id.investree.customer.utils;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LocalMessageUtilsTest {

	@Autowired
	private LocalMessageUtils localMessageUtils;

	@Test
	public void testMessage() {
		String staticField = "field";

		Assertions.assertThat(localMessageUtils.statusRestricted()).isEqualTo("Your status is restricted to update/add");
		Assertions.assertThat(localMessageUtils.insertSuccess()).isEqualTo("Data successfully saved");
		Assertions.assertThat(localMessageUtils.insertFailed(staticField)).isEqualTo("Data failed to save: " + staticField);
		Assertions.assertThat(localMessageUtils.updateSuccess()).isEqualTo("Data successfully updated");
		Assertions.assertThat(localMessageUtils.updateFailed()).isEqualTo("Data failed to update");
		Assertions.assertThat(localMessageUtils.emptyToken()).isEqualTo("Access Token must not be empty");
		Assertions.assertThat(localMessageUtils.dataNotValid(staticField)).isEqualTo("Data not valid. Please check following field: " + staticField);
		Assertions.assertThat(localMessageUtils.nameOnlyAlphabet()).isEqualTo("Fullname cannot contains numbers and special characters");
		Assertions.assertThat(localMessageUtils.identityCardNumberDuplicate()).isEqualTo("IdentityCardNumber had been used");
		Assertions.assertThat(localMessageUtils.identityCardNumberLength()).isEqualTo("IdentityCardNumber must be 16 characters long");
		Assertions.assertThat(localMessageUtils.mobileNumberLength()).isEqualTo("MobileNumber must be must be 9 - 12 digits");
		Assertions.assertThat(localMessageUtils.deleteSuccess()).isEqualTo("Data has been deleted");
		Assertions.assertThat(localMessageUtils.deleteFailed()).isEqualTo("Data failed to delete");
		Assertions.assertThat(localMessageUtils.identityCardExpiry()).isEqualTo("IdentityCardExpiryDate cannot less than today");
		Assertions.assertThat(localMessageUtils.dateMustBeOverSeventeenYears(staticField)).isEqualTo(staticField + " must be less than 17 years ago");
		Assertions.assertThat(localMessageUtils.statementDate()).isEqualTo("Statement Date cannot more than today");
		Assertions.assertThat(localMessageUtils.statementDateMinimunYear()).isEqualTo("Statement Date cannot more than 5 years");
		Assertions.assertThat(localMessageUtils.errorSelectedBankAccountForDisbursement()).isEqualTo("Please set one account to be used for disbursement");
		Assertions.assertThat(localMessageUtils.errorSelectedBankAccountForWithdrawal()).isEqualTo("Please set one account to be used for withdrawal");
		Assertions.assertThat(localMessageUtils.errorBankAccountNumberShouldContainsNumber()).isEqualTo("BankAccountNumber should be number");
		Assertions.assertThat(localMessageUtils.errorNumberOnly(staticField)).isEqualTo(staticField + " must be number");
		Assertions.assertThat(localMessageUtils.errorBlank(staticField)).isEqualTo(staticField + " cannot be blank");
		Assertions.assertThat(localMessageUtils.errorDocumentNotHaveExpireDate(staticField)).isEqualTo(staticField + " does not have expire date");
		Assertions.assertThat(localMessageUtils.errorNpwpNotValidDigit()).isEqualTo("NPWP must be 15 digits");
		Assertions.assertThat(localMessageUtils.lessLandLine()).isEqualTo("Landline cannot less than 5 digits");
		Assertions.assertThat(localMessageUtils.moreDate(staticField)).isEqualTo(staticField + " cannot more than today");
		Assertions.assertThat(localMessageUtils.errorPostalCode()).isEqualTo("Postal code must be exact 5 digits");
		Assertions.assertThat(localMessageUtils.errorExpiredDocument()).isEqualTo("Document has been expired");
		Assertions.assertThat(localMessageUtils.lessDate(staticField)).isEqualTo(staticField + " cannot less than today");
		Assertions.assertThat(localMessageUtils.emptySection(staticField)).isEqualTo(staticField + " is Empty");
		Assertions.assertThat(localMessageUtils.notCompletedSection(staticField)).isEqualTo(staticField + " is not completed");
		Assertions.assertThat(localMessageUtils.specificBankAcccountNotValid(staticField, staticField)).isEqualTo("Bank account number " + staticField + " with holder name " + staticField + " is not valid");
		Assertions.assertThat(localMessageUtils.picBorrowerEmpty()).isEqualTo("Borrower PIC is empty");
	}
}