package id.investree.customer.utils;

import id.investree.core.constant.GlobalConstants;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;
import id.investree.core.model.TokenPayload;
import id.investree.customer.entity.CustomerRole;
import id.investree.customer.service.customerrole.CustomerRoleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StatusValidationTest {

	@Autowired
	private StatusValidation statusValidation;

	@Autowired
	private CustomerRoleService customerRoleService;

	@Test(expected = DataNotFoundException.class)
	public void testStatusRestrictionValidation_DataNotFound() {
		TokenPayload tokenPayload = new TokenPayload();
		tokenPayload.setUserType(GlobalConstants.USER_TYPE_FRONTOFFICE_ID);
		tokenPayload.setCustomerId(-21291);

		statusValidation.statusRestrictionValidation(tokenPayload, GlobalConstants.USER_CATEGORY_BORROWER_ID);
	}

	@Test(expected = AppException.class)
	public void testStatusRestrictionValidation_restricted() {
		TokenPayload tokenPayload = new TokenPayload();
		tokenPayload.setUserType(GlobalConstants.USER_TYPE_FRONTOFFICE_ID);
		tokenPayload.setCustomerId(165); // user `aquagelas`

		CustomerRole customerRole = customerRoleService.findByCustomerIdAndType(165L, GlobalConstants.USER_CATEGORY_BORROWER_ID);
		customerRole.setStatus(4L);
		customerRoleService.saveOrUpdate(customerRole);

		statusValidation.statusRestrictionValidation(tokenPayload, GlobalConstants.USER_CATEGORY_BORROWER_ID);
	}
}