package id.investree.customer.utils;

import id.investree.core.constant.GlobalConstants;
import id.investree.customer.service.customerrole.CustomerRoleService;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StoreDataGlobalParamTest {

	@Autowired
	private StoreDataGlobalParam storeDataGlobalParam;

	@Autowired
	private CustomerRoleService customerRoleService;

	@Test
	public void testGlobalParam() {
		storeDataGlobalParam.getGlobalParam(
			GlobalConstants.GLOBAL_PARAM_NATIONALITY,
			GlobalConstants.GLOBAL_PARAM_POSITION,
			GlobalConstants.GLOBAL_PARAM_DIVISION,
			GlobalConstants.GLOBAL_PARAM_REGISTRATION_STATUS,
			GlobalConstants.GLOBAL_PARAM_MOBILE_PREFIX,
			GlobalConstants.GLOBAL_PARAM_MARITAL_STATUS,
			GlobalConstants.GLOBAL_PARAM_OCCUPATION,
			GlobalConstants.GLOBAL_PARAM_EDUCATION,
			GlobalConstants.GLOBAL_PARAM_LEGAL_ENTITY,
			GlobalConstants.GLOBAL_PARAM_RELIGION,
			GlobalConstants.GLOBAL_PARAM_RELATIONSHIP,
			GlobalConstants.GLOBAL_PARAM_KNOW_INVESTREE_FROM,
			Constants.GLOBAL_PARAM_INCOME_LEVEL,
			Constants.GLOBAL_PARAM_SOURCE_OF_FUND,
			Constants.GLOBAL_PARAM_PIC_ROLE
		);

		Assertions.assertNotNull(storeDataGlobalParam.getNationalityList());
		Assertions.assertNotNull(storeDataGlobalParam.getPositionList());
		Assertions.assertNotNull(storeDataGlobalParam.getDivisionList());
		Assertions.assertNotNull(storeDataGlobalParam.getRegistrationStatusList());
		Assertions.assertNotNull(storeDataGlobalParam.getMobilePrefixList());
		Assertions.assertNotNull(storeDataGlobalParam.getMaritalStatusList());
		Assertions.assertNotNull(storeDataGlobalParam.getOccupationList());
		Assertions.assertNotNull(storeDataGlobalParam.getEducationList());
		Assertions.assertNotNull(storeDataGlobalParam.getLegalList());
		Assertions.assertNotNull(storeDataGlobalParam.getReligionList());
		Assertions.assertNotNull(storeDataGlobalParam.getIncomeLevelList());
		Assertions.assertNotNull(storeDataGlobalParam.getSourceOfFundList());
		Assertions.assertNotNull(storeDataGlobalParam.getRelationshipList());
		Assertions.assertNotNull(storeDataGlobalParam.getKifList());
		Assertions.assertNotNull(storeDataGlobalParam.getPicRoleList());
		Assertions.assertNotNull(storeDataGlobalParam.getApptResultList());
	}
}